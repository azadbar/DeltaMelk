package ir.delta.delta.pushNotification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ir.delta.delta.R;
import ir.delta.delta.activity.StartActivity;

import static ir.delta.delta.util.Constants.FCM_ACTION_CLICK_NOTIFICATION;
import static ir.delta.delta.util.Constants.FCM_ACTION_NEW_NOTIFICATION;

public class FCMService extends FirebaseMessagingService {




    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("remoteMessage", remoteMessage.getFrom() + "");

        String postId = "0";
        String link = null;
        String imageUri;
        Bitmap bitmap;
        if (remoteMessage.getData().size() > 0) {
            Log.d("TAG", "Message data payload: " + remoteMessage.getData());
            postId = remoteMessage.getData().get("postId");
            link = remoteMessage.getData().get("link");
            imageUri = remoteMessage.getData().get("image");
            bitmap = getBitmapfromUrl(imageUri);
            notificationMessageReceived(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"),
                    postId, link,bitmap);
        }


        if (remoteMessage.getNotification() != null) {

        }

//        if (remoteMessage.getData().size() > 0) {
//            dataMessageReceived(remoteMessage.getData());
//        }
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
    private void notificationMessageReceived(String title, String body, String postId, String link, Bitmap bitmap) {

        NotificationHelper notificationHelper=new NotificationHelper(this);
        Intent intent=new Intent(this, StartActivity.class);
        intent.putExtra("postId",postId);
        intent.putExtra("link",link);
        notificationHelper.createNotification(title,body,intent,bitmap);

//        Intent intent = new Intent(getApplicationContext(), SplashConsultantActivity.class); //Open activity if clicked on notification
//        // Set the Activity to start in a new, empty task
//        intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.putExtra("postId", "1");
//        intent.putExtra("link", "111");
////        intent.putExtra("selectedIndex", position);
//
//
//        Bundle bundle = new Bundle();
//        bundle.putString("postId", "1");
//
//
//        intent.putExtras(bundle);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
////        long[] pattern = {500, 500, 500, 500, 500}; //Notification vibration pattern
////
////        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); //Notification alert sound
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setContentTitle(title)
//                .setContentText(body)
//                .setAutoCancel(true)
//                .setLights(Color.BLUE, 1, 1)
//                .setContentIntent(pendingIntent)
//                .setSmallIcon(getNotificationIcon())
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_mag))
//                .setColor(ContextCompat.getColor(this, R.color.colorPrimary));
//
//
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(getNotify(), notificationBuilder.build());

    }

    private void dataMessageReceived(java.util.Map<java.lang.String, java.lang.String> body) {
        try {

            JSONObject bodyObjects = new JSONObject(body);

            if (bodyObjects.getString("type").equals("banner")) {
                showNotificationWithBanner(bodyObjects.getString("title"), bodyObjects.getString("message"), bodyObjects.getString("banner_url"));
            } else if (bodyObjects.getString("type").equals("dialog_message")) {
                broadcastTheMessage(bodyObjects.getString("title"), bodyObjects.getString("message"));
            }

        } catch (Exception e) {
            Log.e("There is a problem", "Exception: " + e);
        }
    }

    private void showNotificationWithBanner(String title, String message, String bannerURL) {

        Intent intent = new Intent(this, StartActivity.class); //Open activity if clicked on notification
        PendingIntent pendingIntent;

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setAction(FCM_ACTION_CLICK_NOTIFICATION);
        pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long[] pattern = {500, 500, 500, 500, 500}; //Notification vibration pattern

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION); //Notification alert sound

        //Style for showing notification With Banner
        Bitmap remote_picture = BitmapFactory.decodeResource(getResources(), R.drawable.coins_bg);
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(bannerURL).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        notiStyle.bigPicture(remote_picture);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setLights(Color.BLUE, 1, 1)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSmallIcon(getNotificationIcon())
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_mag))
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setStyle(notiStyle);
//                .setStyle(new NotificationCompat.BigTextStyle().bigText());

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(getNotify(), notificationBuilder.build());

    }


    private void broadcastTheMessage(String title, String message) {

        Intent notification = new Intent(FCM_ACTION_NEW_NOTIFICATION);
        notification.putExtra("title", title);
        notification.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(notification);
    }


    //Random and unique notification ID
    public int getNotify() {
        return Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(new Date()));
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher_mag : R.mipmap.ic_launcher_mag;
    }


}