package ir.delta.delta.payment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import ir.delta.delta.R;
import ir.delta.delta.activity.MainActivity;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.service.ResponseModel.PaymentResult;

/**
 * Created by R.taghizadeh on 11/27/2017.
 */

public class ActivityListenerResultPayment extends BaseActivity implements FinishActivityListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_result);
    }


    @Override
    protected void onStart() {
        super.onStart();
        Uri data = getIntent().getData();
        PaymentResult paymentResult = null;
        if (data != null && data.isHierarchical()) {
            if (data.getQueryParameter("resultType") != null) {
                paymentResult = new PaymentResult();
                paymentResult.setResultType(!TextUtils.equals(data.getQueryParameter("resultType"), "false"));
                paymentResult.setWalletPayment(TextUtils.equals(data.getQueryParameter("isWalletPayment"), "True"));
                paymentResult.setInvoice(data.getQueryParameter("invoice"));
                paymentResult.setOrderId(data.getQueryParameter("orderId"));
                paymentResult.setEnOrderId(data.getQueryParameter("enOrderId"));
                paymentResult.setPrice(data.getQueryParameter("price"));
                paymentResult.setPayDate(data.getQueryParameter("payDate"));
            }
        }
        if (paymentResult != null) {
            PaymentResultDialog paymentResultDialog = new PaymentResultDialog(this);
            paymentResultDialog.paymentResult = paymentResult;
            paymentResultDialog.finishListener = this;
            paymentResultDialog.show();
        }
    }


    @Override
    public void onFinishActivity() {
        finish();
    }

    @Override
    public void onGoTOHomeActivity() {
        Intent intent = new Intent(this, BottomBarActivity.class);
        intent.putExtra("itemsIndex", 3);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
