package ir.delta.delta.payment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.PaymentService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.PaymentReq;
import ir.delta.delta.service.ResponseModel.PaymentResponse;
import ir.delta.delta.service.ResponseModel.PaymentResult;
import ir.delta.delta.util.Constants;

/**
 * Created by m.azadbar on 10/7/2017.
 */

public class PaymentResultDialog extends Dialog {

    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;
    @BindView(R.id.tvPayementStatus)
    BaseTextView tvPayementStatus;
    @BindView(R.id.rlPaymentStatus)
    BaseRelativeLayout rlPaymentStatus;
    @BindView(R.id.tvPaymentType)
    BaseTextView tvPaymentType;
    @BindView(R.id.tvWarningReturn)
    BaseTextView tvWarningReturn;
    @BindView(R.id.tvTrackingCode)
    BaseTextView tvTrackingCode;
    @BindView(R.id.tvFactorNumber)
    BaseTextView tvFactorNumber;
    @BindView(R.id.tvPaymentAmount)
    BaseTextView tvPaymentAmount;
    @BindView(R.id.tvDateAmount)
    BaseTextView tvDateAmount;
    @BindView(R.id.btnTryAgain)
    BaseTextView btnTryAgain;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    public PaymentResult paymentResult;
    public ServerErrorListener listener;
    public FinishActivityListener finishListener;

    PaymentResultDialog(@NonNull Context context) {
        super(context);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.payment_result_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);

        Resources res = tvPayementStatus.getResources();
        tvPayementStatus.setTextColor(res.getColor(R.color.white));
        if (paymentResult.isResultType()) {
            tvPayementStatus.setText(res.getString(R.string.payment_was_successful));
            rlPaymentStatus.setBackgroundColor(res.getColor(R.color.green));
            tvWarningReturn.setVisibility(View.GONE);
            tvDateAmount.setVisibility(View.VISIBLE);
            tvDateAmount.setText(res.getString(R.string.payment_date) + ": " + paymentResult.getPayDate());
            tvPaymentAmount.setText(res.getString(R.string.amount_paid) + ": " +paymentResult.getPrice() + " " + res.getString(R.string.toman));
            btnCancel.setVisibility(View.GONE);
            btnTryAgain.setText(res.getString(R.string.retrun_main_activity));
        } else {
            tvPayementStatus.setText(res.getString(R.string.payment_was_error));
            rlPaymentStatus.setBackgroundColor(res.getColor(R.color.redColor));
            tvWarningReturn.setVisibility(View.VISIBLE);
            tvWarningReturn.setTextColor(res.getColor(R.color.redColor));
            tvWarningReturn.setText(res.getString(R.string.warning_payment));
            tvDateAmount.setVisibility(View.GONE);
            tvPaymentAmount.setText(res.getString(R.string.the_amount_payable) + ": "+  paymentResult.getPrice()+  " " + res.getString(R.string.toman));
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(res.getString(R.string.cancel));
            btnTryAgain.setVisibility(View.VISIBLE);
            btnTryAgain.setText(res.getString(R.string.payment_again));
        }

       /* if (paymentResult.isDemandPayment()) {
            tvPaymentType.setText(res.getString(R.string.invoice_for_property_registration));
        } else if (paymentResult.isSpecialPayment()) {
            tvPaymentType.setText(res.getString(R.string.invoice_for_stair_property));
        } else {
            tvPaymentType.setText(res.getString(R.string.property_factor_no_photo));
        }*/
        if (!TextUtils.isEmpty(paymentResult.getInvoice())) {
            tvTrackingCode.setText(res.getString(R.string.traking_code) + ": " + paymentResult.getInvoice());
            tvTrackingCode.setTextColor(res.getColor(R.color.secondaryTextColor));
        }

        if (!TextUtils.isEmpty(paymentResult.getOrderId())) {
            tvFactorNumber.setText(res.getString(R.string.number_factor_payment) + ": " + paymentResult.getOrderId());
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }


    @OnClick({R.id.btnTryAgain, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnTryAgain:
                if (finishListener != null) {
                    dismiss();
                    finishListener.onGoTOHomeActivity();
                }
                break;
            case R.id.btnCancel:
                if (finishListener != null) {
                    dismiss();
                    finishListener.onFinishActivity();
                }
                break;
        }
    }


   /* private void payementSent(Resources res) {
        showLoding();
        PaymentReq req = new PaymentReq();
        req.setEnOrderId(paymentResult.getEnOrderId());
        PaymentService.getInstance().getPaymentResponse(res, req, new ResponseListener<PaymentResponse>() {
            @Override
            public void onGetError(String error) {
                stopLoading();
                Toast.makeText(getContext(), res.getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                getContext().startActivity(intent);
                dismiss();
            }

            @Override
            public void onSuccess(PaymentResponse response) {
                stopLoading();
                dismiss();
                if (response.isSuccessed() && response.getRedirectUrl() != null) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(response.getRedirectUrl()));
                        getContext().startActivity(i);
                    } catch (ActivityNotFoundException act) {

                    }
                } else {
                    if (listener != null) {
                        listener.onShowErrorListener(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });
    }

    private void showLoding() {
        if (progressDialog != null) {
            progressDialog.setVisibility(View.VISIBLE);
            btnTryAgain.setClickable(false);
            btnCancel.setClickable(false);
        }

    }

    private void stopLoading() {
        if (progressDialog != null) {
            progressDialog.setVisibility(View.INVISIBLE);
            btnTryAgain.setClickable(true);
            btnCancel.setClickable(true);
        }

    }*/


}
