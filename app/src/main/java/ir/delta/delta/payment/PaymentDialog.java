package ir.delta.delta.payment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.enums.PayerIdEnum;
import ir.delta.delta.enums.PaymentTypeEnum;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.PaymentService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.wallet.PaymentSendService;
import ir.delta.delta.service.Request.wallet.RegisterEsatetWalletService;
import ir.delta.delta.service.RequestModel.PaymentReq;
import ir.delta.delta.service.ResponseModel.PaymentResponse;
import ir.delta.delta.service.ResponseModel.wallet.PaymentSendWalletResponse;
import ir.delta.delta.service.ResponseModel.wallet.RegisterWalletAmountResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

/**
 * Created by  on 10/7/2017.
 */

public class PaymentDialog extends Dialog {

    @BindView(R.id.titlePayment)
    BaseTextView titlePayment;
    @BindView(R.id.tvNumberFactor)
    BaseTextView tvNumberFactor;
    @BindView(R.id.tvAmountPayable)
    BaseTextView tvAmountPayable;
    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;
    @BindView(R.id.tvWalletAmount)
    BaseTextView tvWalletAmount;
    @BindView(R.id.btnPayment)
    BaseRelativeLayout btnPayment;
    @BindView(R.id.btnCancel)
    BaseRelativeLayout btnCancel;
    @BindView(R.id.tvPayment)
    BaseTextView tvPayment;

    public PaymentTypeEnum paymentTypeEnum;
    public int orderId;
    public String enOrderId;
    public String orderPrice;
    public String orderTitle;
    public String walletAmount;
    public ServerErrorListener listener;

    public PaymentDialog(@NonNull Context context) {
        super(context);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.payment_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawableProgress = DrawableCompat.wrap(progressDialog.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(getContext(), R.color.white));
            progressDialog.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));

        } else {
            progressDialog.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
        }

        Resources res = titlePayment.getResources();
        if (paymentTypeEnum == PaymentTypeEnum.DEPOSIT_REGISTER || paymentTypeEnum == PaymentTypeEnum.STAIRS_REGISTER
                || paymentTypeEnum == PaymentTypeEnum.DEMAND_REGISTER) {
            tvPayment.setText(res.getString(R.string.payment_from_wallet));
        } else {
            tvPayment.setText(res.getString(R.string.payment_online));
        }
        titlePayment.setText(orderTitle);
        tvNumberFactor.setText(res.getString(R.string.number_factor_payment) + ": " + orderId);
        tvAmountPayable.setText(res.getString(R.string.the_amount_payable) + ": " + orderPrice + " " + res.getString(R.string.toman));
        if (!TextUtils.isEmpty(walletAmount)) {
            tvWalletAmount.setVisibility(View.VISIBLE);
            tvWalletAmount.setText(res.getString(R.string.inventory) + " " + res.getString(R.string.wallet) + ": " + walletAmount + " " +
                    res.getString(R.string.toman));
        } else {
            tvWalletAmount.setVisibility(View.GONE);
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
        btnPayment.setOnClickListener(v -> {
            if (paymentTypeEnum == PaymentTypeEnum.DEPOSIT_REGISTER || paymentTypeEnum == PaymentTypeEnum.STAIRS_REGISTER
            || paymentTypeEnum == PaymentTypeEnum.DEMAND_REGISTER ) {
                registerEstateRequest(res);
            } else {
                paymentSent(res);
            }
        });
        btnCancel.setOnClickListener(view1 -> dismiss());
    }

    private void registerEstateRequest(Resources res) {
        showLoding();
        PaymentReq req = new PaymentReq();
        req.setEnOrderId(enOrderId);
        req.setAtUserId(Constants.getUser(getContext()).getUserId() + "");
        RegisterEsatetWalletService.getInstance().registerEstateWallet(getContext(), req, res, new ResponseListener<RegisterWalletAmountResponse>() {
            @Override
            public void onGetError(String error) {

                stopLoading();
                Toast.makeText(getContext(), res.getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                getContext().startActivity(intent);
                dismiss();
            }

            @Override
            public void onSuccess(RegisterWalletAmountResponse response) {
                stopLoading();
                dismiss();
                if (response.isSuccessed()) {
                    CustomDialog successDialog = new CustomDialog(getContext());
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                    successDialog.setLottieAnim("tick.json", 0);
                    successDialog.setDialogTitle(response.getMessage());
                    successDialog.setColorTitle(getContext().getResources().getColor(R.color.applyFore));
                    successDialog.setOkListener(getContext().getString(R.string.ok), view -> successDialog.dismiss());
                    successDialog.show();
                } else {
                    if (listener != null) {
                        listener.onShowErrorListener(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });
    }


    private void paymentSent(Resources res) {
        showLoding();
        PaymentReq req = new PaymentReq();
        req.setEnOrderId(enOrderId);
        req.setAtUserId(Constants.getUser(getContext()).getUserId() + "");
        PaymentSendService.getInstance().paymentSendWallet(getContext(), req, res, new ResponseListener<PaymentSendWalletResponse>() {
            @Override
            public void onGetError(String error) {

                stopLoading();
                Toast.makeText(getContext(), res.getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                getContext().startActivity(intent);
                dismiss();
            }

            @Override
            public void onSuccess(PaymentSendWalletResponse response) {
                stopLoading();
                dismiss();
                if (response.isSuccessed() && response.getRedirectUrl() != null) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(response.getRedirectUrl()));
                        getContext().startActivity(i);
//                        listener.payemntInBillingBazar(response.getReferralCode());
                    } catch (ActivityNotFoundException act) {

                    }

                } else {
                    if (listener != null) {
                        listener.onShowErrorListener(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });
    }

    private void showLoding() {
        progressDialog.setVisibility(View.VISIBLE);
        btnPayment.setClickable(false);
        btnCancel.setClickable(false);
    }

    private void stopLoading() {
        if (progressDialog != null) {
            progressDialog.setVisibility(View.INVISIBLE);
            btnPayment.setClickable(true);
            btnCancel.setClickable(true);
        }
    }
}
