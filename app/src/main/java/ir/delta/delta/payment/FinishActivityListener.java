package ir.delta.delta.payment;

public interface FinishActivityListener {

    void onFinishActivity();
    void onGoTOHomeActivity();

}
