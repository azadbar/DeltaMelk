package ir.delta.delta.baseView;


import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatDelegate;

import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ReplacementSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.bottomNavigation.registerEstate.InfoListDialog;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.location.LocationActivity;
import ir.delta.delta.util.Constants;

import static android.content.Context.WINDOW_SERVICE;

public class BaseFragment extends Fragment {

    public int currentCityId = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        adjustFontScale(getResources().getConfiguration());
        try {
            Configuration configuration = getResources().getConfiguration();
            configuration.locale = Constants.getLanguage().getLocale();
            configuration.setLayoutDirection(configuration.locale);
            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        } catch (Exception ignored) {
        }
    }

    public void finishFragment(View view) {
        if (getActivity() != null) {
            view.setOnClickListener(view1 -> getActivity().finish());
        }
    }

    public void startCityActivity() {
        Intent intent = new Intent(getContext(), LocationActivity.class);
        intent.putExtra("cityEnum", CityEnum.MAINCITY.getId());
        startActivity(intent);
    }

    public void checkArrowRtlORLtr(View view) {
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            view.setBackgroundResource(R.drawable.ic_back_english);
        } else {
            view.setBackgroundResource(R.drawable.ic_back);
        }
    }

    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        if (getActivity() != null) {
            InfoListDialog infoListDialog = new InfoListDialog(getActivity());
            infoListDialog.errorMsg = errorMsgList;
            infoListDialog.title = title;
            infoListDialog.show();
        }
    }


    public void showErrorFromServer(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        if (modelStateErrors != null && modelStateErrors.size() > 0) {
            ArrayList<String> errorList = new ArrayList<>();
            for (ModelStateErrors error : modelStateErrors) {
                if (!TextUtils.isEmpty(error.getMessage()))
                    errorList.add(error.getMessage());
            }
            if (errorList.size() > 0) {
                showInfoDialog(getString(R.string.error), errorList);
                return;
            }
        }
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void loadFragment(Fragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.replace(R.id.frameLayout, fragment, fragmentTag);
            fragTrans.commit();
        }
    }

    // for override

    public void resume() {
    }

    public boolean onPopBackStack() {
        return false;
    }

    public boolean isHiddenNavigation() {
        return false;
    }


    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

//    public void openGalleryPhotos() {
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "انتخاب عکس"), Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY);
//    }

//    public Uri openCameraTake(Uri imageUri) {
//        if (getActivity() != null) {
//            ContentValues values = new ContentValues();
//            values.put(MediaStore.Images.Media.TITLE, "دلتا");
//            values.put(MediaStore.Images.Media.DESCRIPTION, "عکس خود را انتخاب کنید");
//            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//            startActivityForResult(intent, Constants.REQUEST_IMAGE_CAPTURE);
//
//        }
//        return imageUri;
//    }

//
//    public void getImageFromGallery(Intent data) {
//        if (getActivity() != null) {
//            Uri selectedImage = data.getData();
//            String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
//            if (selectedImage != null)
//                UCrop.of(selectedImage, Uri.fromFile(new File(getActivity().getCacheDir(), destinationFileName))).withAspectRatio(5, 4)
//                        .withMaxResultSize(280, 280)
//                        .start(getActivity());
//        }
//    }


//    public void getImageFromCamera(Uri imageUri) {
//        if (getActivity() == null) {
//            return;
//        }
//        String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
//        UCrop.of(imageUri, Uri.fromFile(new File(getActivity().getCacheDir(), destinationFileName))).withAspectRatio(5, 4)
//                .withMaxResultSize(280, 280)
//                .start(getActivity());
//    }


    public Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }


    protected void seStatusBarColor(int color, View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            window.setStatusBarColor(color);
        }

    }

    protected SpannableStringBuilder getTextDescription(Context context, String desc) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(desc + "\n\n");
        int start = desc_two.length();
        desc_two.append("متوجه شدم");
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RoundedBackgroundSpan(context), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return desc_two;
    }

    public static class RoundedBackgroundSpan extends ReplacementSpan {

        private static final int CORNER_RADIUS = 80;
        private static final int PADDING_X = 35;
        private static final int PADDING_y = 20;
        private int backgroundColor = 0;
        private int textColor = 0;

        public RoundedBackgroundSpan(Context context) {
            super();
            backgroundColor = context.getResources().getColor(R.color.white);
            textColor = context.getResources().getColor(R.color.redColor);
        }

        @Override
        public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
            float width = paint.measureText(text.subSequence(start, end).toString());
            RectF rect = new RectF(x, top , x + width + 2 * PADDING_X, bottom);
            paint.setColor(backgroundColor);
            canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS, paint);
            paint.setColor(textColor);
            canvas.drawText(text, start, end, x + PADDING_X, y , paint);
        }

        @Override
        public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
            return (int) (PADDING_X + paint.measureText(text.subSequence(start, end).toString()) + PADDING_X);
        }

        private float measureText(Paint paint, CharSequence text, int start, int end) {
            return paint.measureText(text, start, end);
        }
    }

    public static void ellipsizeText(TextView tv, int lenght){
        int maxLength = lenght;
        if(tv.getText().length() > maxLength){
            String currentText = tv.getText().toString();
            String ellipsizedText =  currentText.substring(0, maxLength - 3) + "...";
            tv.setText(ellipsizedText);
        }
    }

    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > 1.20) {
            configuration.fontScale = 1.20f;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getContext().getSystemService(WINDOW_SERVICE);
            if (wm != null)
                wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getContext().getResources().updateConfiguration(configuration, metrics);
        }
    }
}
