package ir.delta.delta.baseView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;

import ir.delta.delta.R;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.util.Constants;

public class BaseTextView extends AppCompatTextView {


    public BaseTextView(Context context) {
        this(context, null);
    }

    public BaseTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }


    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        this.setTextDirection(Constants.getLanguage().setTextDirec());
        setFontType(FontTypeEnum.REGULAR);
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseTextView, 0, 0);
            if (a.hasValue(R.styleable.BaseTextView_fontType)) {
                int value = a.getInt(R.styleable.BaseTextView_fontType, 0);
                if (value >= 0 && value < FontTypeEnum.values().length) {
                    setFontType(FontTypeEnum.values()[value]);
                }
            }
            a.recycle();
        }
    }


    public void setFontType(FontTypeEnum fontType) {
        if (fontType == FontTypeEnum.BOLD) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"));
        } else if (fontType == FontTypeEnum.MEDIUM) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanLightMobile(FaNum).ttf"));
        } else if (fontType == FontTypeEnum.Light) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanLightMobile(FaNum).ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"));
        }
    }


    public void setText(String text) {
        if (text == null) {
            text = "";
        }
        super.setText(text);
    }

    public void textAlign(String value) {//TODO check text align
        if (TextUtils.equals(value, ("right"))) {
//            this.setTextDirection(View.TEXT_DIRECTION_RTL);
            this.setGravity(Gravity.RIGHT);
        } else if (TextUtils.equals(value, ("left"))) {
//            this.setTextDirection(View.TEXT_DIRECTION_LTR);
            this.setGravity(Gravity.LEFT);
        } else if (TextUtils.equals(value, ("center"))) {
//            this.setTextDirection(View.TEXT_DIRECTION_LOCALE);
            this.setGravity(Gravity.CENTER);
        } else {
//            this.setTextDirection(View.TEXT_DIRECTION_RTL);
            this.setGravity(Gravity.NO_GRAVITY);
        }
    }

    @Override
    public void setTextSize(float size) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }
}