package ir.delta.delta.baseView;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.delta.delta.R;
import ir.delta.delta.bottomNavigation.registerEstate.InfoListDialog;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.location.LocationActivity;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.util.Constants;

public abstract class BaseActivity extends AppCompatActivity {

    public int currentCityId = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        try {
            Configuration configuration = getResources().getConfiguration();
            configuration.locale = Constants.getLanguage().getLocale();
            configuration.setLayoutDirection(configuration.locale);
            getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        } catch (Exception ignored) {
        }
    }

    protected void seStatusBarColor(int color, View view) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            window.setStatusBarColor(color);
        }

    }

    public void seStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public void seStatusBarColor(View view, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = view.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            view.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(color);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        newConfig.locale = Constants.getLanguage().getLocale();
        getApplicationContext().getResources().updateConfiguration(newConfig, null);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
    }


    protected void checkArrowRtlORLtr(View view) {
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            view.setBackgroundResource(R.drawable.ic_back_english);
        } else {
            view.setBackgroundResource(R.drawable.ic_back);
        }
    }

    public void setCityTitle(BaseTextView textView) {
        if (Constants.getCity(this) != null) {
            textView.setText(Constants.getCity(this).getName());
        }
    }

    public void showChangeCityView(View view) {
        view.setVisibility(View.VISIBLE);
        view.setOnClickListener(view1 -> {
            Intent intent = new Intent(BaseActivity.this, LocationActivity.class);
            intent.putExtra("cityEnum", CityEnum.MAINCITY.getId());
            startActivity(intent);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            List<Fragment> frags = getSupportFragmentManager().getFragments();
            if (frags != null) {
                for (Fragment f : frags) {
                    if (f != null && f.isVisible())
                        f.onActivityResult(requestCode, resultCode, data);
                }
            }

        }
    }

    public int getStatusBarHeight() {
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    protected void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        InfoListDialog infoListDialog = new InfoListDialog(this);
        infoListDialog.errorMsg = errorMsgList;
        infoListDialog.title = title;
        infoListDialog.show();
    }

    protected void showInfoDialog(String title, ArrayList<String> errorMsgList, int colorTitle) {
        InfoListDialog infoListDialog = new InfoListDialog(this);
        infoListDialog.errorMsg = errorMsgList;
        infoListDialog.title = title;
        infoListDialog.colorTitle = colorTitle;
        infoListDialog.show();
    }

    public void changeStatusBar(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    //ihsisa

    //asdad
    public int getToolBarHeight() {

        TypedValue tv = new TypedValue();
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            return TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        return 0;
    }

    protected static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    protected SpannableStringBuilder getTextDescription(Context context, String desc) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(desc + "\n\n");
        int start = desc_two.length();
        desc_two.append("متوجه شدم");
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new BaseFragment.RoundedBackgroundSpan(context), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return desc_two;
    }

    public void showErrorFromServer(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        if (modelStateErrors != null && modelStateErrors.size() > 0) {
            ArrayList<String> errorList = new ArrayList<>();
            for (ModelStateErrors error : modelStateErrors) {
                if (!TextUtils.isEmpty(error.getMessage()))
                    errorList.add(error.getMessage());
            }
            if (errorList.size() > 0) {
                showInfoDialog(getString(R.string.error), errorList);
                return;
            }
        }
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    public void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    public void ellipsizeText(TextView tv) {
        int maxLength = 40;
        if (tv.getText().length() > maxLength) {
            String currentText = tv.getText().toString();
            String ellipsizedText = currentText.substring(0, maxLength - 3) + "...";
            tv.setText(ellipsizedText);
        }
    }

    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > 1.20) {
            configuration.fontScale = 1.20f;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            if (wm != null)
                wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }
}
