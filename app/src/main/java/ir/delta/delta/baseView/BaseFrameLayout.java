package ir.delta.delta.baseView;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import ir.delta.delta.util.Constants;

public class BaseFrameLayout extends FrameLayout {

    public BaseFrameLayout(Context context) {
        super(context);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public BaseFrameLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());

    }

    public BaseFrameLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }


}
