package ir.delta.delta.baseView;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;

import ir.delta.delta.util.Constants;

public class BaseToolbar extends Toolbar {

    public BaseToolbar(Context context) {
        super(context);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public BaseToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());

    }

    public BaseToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }


}
