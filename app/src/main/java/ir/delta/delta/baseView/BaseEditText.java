package ir.delta.delta.baseView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;

import androidx.appcompat.widget.AppCompatEditText;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;

import java.text.DecimalFormat;
import java.text.ParseException;

import ir.delta.delta.R;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.enums.InputTypeEnum;
import ir.delta.delta.listener.OnEditTextChangeListener;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.Formatter;
import ir.delta.delta.util.Validation;

public class BaseEditText extends AppCompatEditText implements TextWatcher {

    public OnEditTextChangeListener onEditTextChangeListener;
    public String title;
    public String error = null;
    public String unit = null;
    private InputTypeEnum inputType = InputTypeEnum.DESCRIPTION;
    private Boolean isRequired = false;

    public BaseEditText(Context context) {
        super(context, null);
        initView(context, null, 0);
    }

    public BaseEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, 0);
    }

    public BaseEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        this.setTextDirection(Constants.getLanguage().getLayoutDirection());
        setFontType(FontTypeEnum.REGULAR);
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BaseEditText, 0, 0);
            if (a.hasValue(R.styleable.BaseEditText_fontTypeEditText)) {
                int value = a.getInt(R.styleable.BaseEditText_fontTypeEditText, 0);
                if (value >= 0 && value < FontTypeEnum.values().length) {
                    setFontType(FontTypeEnum.values()[value]);
                }
            }
            a.recycle();
        }
        this.addTextChangedListener(this);
        this.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
    }

    public String getTrimedText() {
        if (this.getText() != null) {
            return Constants.convertToEnglishDigits(this.getText().toString().trim());
        }
        return "";
    }

    public void setInputTypeEnum(InputTypeEnum inputType) {
        this.inputType = inputType;
        this.setFilters(new InputFilter[]{new InputFilter.LengthFilter(inputType.getCharCount())});
        this.setInputType(inputType.getKeyBoardType());
        this.setMaxLines(inputType.getMaxLine());
    }


    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (onEditTextChangeListener != null) {
            String text = "";
            if (charSequence != null) {
                text = charSequence.toString().trim().replaceAll(",", "");
            }
            switch (inputType) {
                case DESCRIPTION:
                    error = Validation.getDescription(text, title, isRequired, getResources());
                    break;
                case NAME:
                    error = Validation.getNameError(text, title, isRequired, getResources());
                    break;
                case PHONE:
                    error = Validation.getPhoneErrore(text, title, isRequired, getResources());
                    break;
                case MOBILE:
                    error = Validation.getMobileError(text, isRequired, getResources());
                    break;
                case EMAIL:
                    error = Validation.getEmailError(text, title, isRequired, getResources());
                    break;
                case ADDRESS:
                    error = Validation.getAddressError(text, title, isRequired, getResources());
                    break;
                case RENT_PRICE:
                case MORTGAGE_PRICE:
                    error = Validation.getPriceValidation(text, title, unit, isRequired, true, getResources());
                    break;
                case TOTAL_PRICE:
                case LOAN:
                    error = Validation.getPriceValidation(text, title, unit, isRequired, false, getResources());
                    break;
                case FLOOR:
                case FLOOR_COUNT:
                    error = Validation.getFloor(text, title, isRequired, getResources());
                    break;
                case ROOM_COUNT:
                    error = Validation.getCountRoom(text, title, isRequired, getResources());
                    break;
                case YEAR_BUILT:
                    error = Validation.getAge(text, title, unit, isRequired, getResources());
                    break;
                case GROUND_WIDTH:
                case AREA:
                case TOTAL_AREA:
                    error = Validation.getArea(text, title, unit, isRequired, getResources());
                    break;
                case NUMBER:
                    error = Validation.getNumberValidation(text, title, isRequired, getResources());
                    break;
                case PASSWORD:
                    error = Validation.getPasswordError(text, title, isRequired, getResources());
                    break;
            }
            if (onEditTextChangeListener != null) {
                onEditTextChangeListener.onGetError(error);
            }


        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (inputType.isNumeric()) {
            this.removeTextChangedListener(this);
            String text = editable.toString();
            DecimalFormat formatrer = Formatter.getInstance();

            try {
                Formatter formatter = new Formatter();
                int inilen, endlen;
                inilen = text.length();
                char separator = formatrer.getDecimalFormatSymbols().getGroupingSeparator();
                String v = text.replace(String.valueOf(separator), "");
                Number n = formatter.df().parse(v);

                int cp = this.getSelectionStart();
                if (formatter.isHasFractionalPart()) {
                    this.setText(formatter.df().format(n));
                } else {
                    this.setText(formatter.dfnf().format(n));
                }
//                this.setText(Formatter.getInstance().format(n));
                endlen = this.getText().length();
                int sel = (cp + (endlen - inilen));
                if (sel > 0 && sel <= this.getText().length()) {
                    this.setSelection(sel);
                } else {
                    // place cursor at the end?
                    this.setSelection(this.getText().length() - 1);
                }
            } catch (NumberFormatException nfe) {
                // do nothing?
            } catch (ParseException e) {
                // do nothing?
            }

            this.addTextChangedListener(this);
        }

    }


    private void setFontType(FontTypeEnum fontType) {
        if (fontType == FontTypeEnum.BOLD) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"));
        } else if (fontType == FontTypeEnum.MEDIUM) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanLightMobile(FaNum).ttf"));
        } else if (fontType == FontTypeEnum.Light) {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanLightMobile(FaNum).ttf"));
        } else {
            this.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"));
        }
    }

    public String getValueString() {
        String text = getTrimedText();
        if (text.isEmpty()) {
            return null;
        }
        return getTrimedText();
    }

    public int getValueInt() {
        if (getTrimedText().isEmpty()) {
            return 0;
        }
        DecimalFormat formatter = Formatter.getInstance();
        char separator = formatter.getDecimalFormatSymbols().getGroupingSeparator();
        String v = getTrimedText().replace(String.valueOf(separator), "");
        try {
            return formatter.parse(v).intValue();
        } catch (ParseException e) {
            return 0;
        }
    }

    public long getValueLong() {
        if (getTrimedText().isEmpty()) {
            return 0;
        }
        DecimalFormat formatter = Formatter.getInstance();
        char separator = formatter.getDecimalFormatSymbols().getGroupingSeparator();
        String v = getTrimedText().replace(String.valueOf(separator), "");
        try {
            return formatter.parse(v).longValue();
        } catch (ParseException e) {
            return 0;
        }
    }


    public void setBody(String text){
        this.setText(text);
    }

    @Override
    public void setTextSize(float size) {
        setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
    }
}
