package ir.delta.delta.baseView;

import android.content.Context;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

import ir.delta.delta.util.Constants;

public class BaseButton extends AppCompatButton {

    public BaseButton(Context context) {
        super(context);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public BaseButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public BaseButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }
}
