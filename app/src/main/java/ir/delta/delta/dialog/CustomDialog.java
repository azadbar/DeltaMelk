package ir.delta.delta.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.airbnb.lottie.LottieAnimationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class CustomDialog extends Dialog {

    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.tvDescriptioError)
    BaseTextView tvDescriptioError;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    @BindView(R.id.lottieAnim)
    LottieAnimationView lottieAnim;

    private String okTitle;
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private String cancelTitle;
    private String description;
    private String title;
    private int image;
    private int color;
    private int colorIcon;
    private boolean cancelable;
    private String lottieFile;
    private int repeatCount;

    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.custom_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(cancelable);

        if (cancelListener != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(cancelTitle);
            btnCancel.setOnClickListener(cancelListener);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        if (okTitle != null) {
            btnOk.setText(okTitle);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(okListener);
        } else {
            btnOk.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        tvTitle.setTextColor(color);
        tvDescriptioError.setText(description);

        if (image > 0) {
            icon.setVisibility(View.VISIBLE);
            lottieAnim.setVisibility(View.INVISIBLE);
            icon.setImageResource(image);
            icon.setColorFilter(colorIcon, PorterDuff.Mode.SRC_ATOP);
        }

        if (!TextUtils.isEmpty(lottieFile)) {
            icon.setVisibility(View.INVISIBLE);
            lottieAnim.setVisibility(View.VISIBLE);
            lottieAnim.setAnimation(lottieFile);
            lottieAnim.playAnimation();
            lottieAnim.setRepeatCount(repeatCount);
        }

        Window window = getWindow();
        if (window != null) {
            window.setWindowAnimations(R.style.DialogTheme);
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    public void setIcon(int image, int color) {
        this.image = image;
        this.colorIcon = color;
    }

    public void setLottieAnim(String lottieFile, int repeatCount) {
        this.lottieFile = lottieFile;
        this.repeatCount = repeatCount;
    }

    public void setCancelListener(String cancelTitle, View.OnClickListener cancelListener) {
        this.cancelTitle = cancelTitle;
        this.cancelListener = cancelListener;
    }

    public void setOkListener(String okTitle, View.OnClickListener okListener) {
        this.okTitle = okTitle;
        this.okListener = okListener;
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }

    public void setColorTitle(int color) {
        this.color = color;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }
}
