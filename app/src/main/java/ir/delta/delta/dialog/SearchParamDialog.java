package ir.delta.delta.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.search.SearchMaxParamAdapter;
import ir.delta.delta.bottomNavigation.search.SearchMinParamAdapter;
import ir.delta.delta.enums.SearchParamType;
import ir.delta.delta.service.ResponseModel.SearchParam;
import ir.delta.delta.util.Constants;

import static android.app.Activity.RESULT_OK;


public class SearchParamDialog extends DialogFragment implements SearchMinParamAdapter.OnItemClickListener, SearchMaxParamAdapter.OnItemClickListener {


    @BindView(R.id.rvMinPrice)
    RecyclerView rvMinPrice;
    @BindView(R.id.rvMaxPrice)
    RecyclerView rvMaxPrice;
    @BindView(R.id.btnDone)
    BaseTextView btnDone;
    Unbinder unbinder;
    @BindView(R.id.tvMin)
    BaseTextView tvMin;
    @BindView(R.id.tvMax)
    BaseTextView tvMax;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;

    private ArrayList<SearchParam> searchParams;
    private String minTitle;
    private String maxTitle;
    private SearchParam minSearchParam;
    private SearchParam maxSearchParam;
    private SearchParamType searchParamType;
    private SearchMinParamAdapter minAdapter;
    private SearchMaxParamAdapter maxAdapter;
    private ArrayList<SearchParam> listMax = new ArrayList<>();
    private ArrayList<SearchParam> listMin = new ArrayList<>();
    private String title;


    public SearchParamDialog() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (getActivity() != null && window != null && getContext() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int height = (int) Math.min(size.y * 0.70, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_height));
                window.setLayout(width, height);
                window.setGravity(Gravity.CENTER);
            }
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_param, container, false);
        unbinder = ButterKnife.bind(this, v);
        setCancelable(true);
        Bundle b = getArguments();
        if (b != null) {
            searchParams = b.getParcelableArrayList("searchParams");
            minSearchParam = b.getParcelable("minSearchParam");
            maxSearchParam = b.getParcelable("maxSearchParam");
            searchParamType = (SearchParamType) b.getSerializable("searchParamType");
            minTitle = b.getString("minTitle");
            maxTitle = b.getString("maxTitle");
            title = b.getString("title");

        }
        tvMin.setText(minTitle);
        tvMax.setText(maxTitle);
        tvTitle.setText(title);
        if (searchParams.size() > 2) {
            listMax = new ArrayList<SearchParam>(searchParams.subList(1, searchParams.size() - 1));
        }
        listMin = searchParams;
        setMinAdapterData();
        setMaxAdapterData();
        return v;
    }


    private void setMaxAdapterData() {

        maxAdapter = new SearchMaxParamAdapter(getResources(), listMax, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvMaxPrice.setLayoutManager(layoutManager);
        rvMaxPrice.setAdapter(maxAdapter);
        maxAdapter.setSearchParam(maxSearchParam);
        maxAdapter.setMinSearchParam(minSearchParam);
        int index = getSearchParamIndex(maxSearchParam, listMax);
        if (index >= 0) {
            rvMaxPrice.scrollToPosition(index);
        }
//        Constants.scrollRecycler(searchParams.size(), rvMaxPrice, 10);
    }

    private void setMinAdapterData() {
        minAdapter = new SearchMinParamAdapter(getResources(), listMin, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvMinPrice.setLayoutManager(layoutManager);
        rvMinPrice.setAdapter(minAdapter);
        minAdapter.setSearchParam(minSearchParam);
        minAdapter.setMaxSearchParam(maxSearchParam);

        int index = getSearchParamIndex(minSearchParam, listMin);
        if (index >= 0) {
            rvMinPrice.scrollToPosition(index);
        }

    }

    private int getSearchParamIndex(SearchParam search, ArrayList<SearchParam> searchParams) {
        if (search != null) {
            for (int i = 0; i < searchParams.size(); i++) {
                if (search.getValue() == searchParams.get(i).getValue()) {
                    return i;
                }
            }
        }
        return -1;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnDone, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                Intent intent = new Intent(getContext(), SearchParamDialog.class);
//                if (minSearchParam == null) {
//                    minSearchParam = listMin.get(0);
//                }
//                if (maxSearchParam == null) {
//                    maxSearchParam = listMax.get(listMax.size() - 1);
//                }
                intent.putExtra("minSearchParam", minSearchParam);
                intent.putExtra("maxSearchParam", maxSearchParam);
                intent.putExtra("searchParamType", searchParamType);

                if (getTargetFragment() != null) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                }
                dismiss();

                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    @Override
    public void onItemClickMin(int position) {
        SearchParam sp = listMin.get(position);
        maxSearchParam = null;
        minSearchParam = sp;
        minAdapter.setSearchParam(minSearchParam);
        minAdapter.setMaxSearchParam(maxSearchParam);
        maxAdapter.setMinSearchParam(minSearchParam);
        maxAdapter.setSearchParam(maxSearchParam);
        int index = getSearchParamIndex(sp, listMin);
        if (index >= 0) {
            rvMaxPrice.scrollToPosition(index);
        }
    }

    @Override
    public void onItemClickMax(int position) {
        SearchParam sp = listMax.get(position);
        if (minSearchParam == null || minSearchParam.getValue() < sp.getValue()) {
            maxSearchParam = sp;
            maxAdapter.setSearchParam(maxSearchParam);
            minAdapter.setMaxSearchParam(maxSearchParam);
        }

    }


}
