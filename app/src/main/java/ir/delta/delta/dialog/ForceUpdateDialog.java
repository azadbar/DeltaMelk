package ir.delta.delta.dialog;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by m.Azadbar on 10/7/2017.
 */

public class ForceUpdateDialog extends Dialog {


    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.tvDescriptionUpdate)
    BaseTextView tvDescriptionUpdate;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.ProgressBar)
    ProgressBar ProgressBar;
    @BindView(R.id.rlBtn)
    BaseRelativeLayout rlBtn;
    @BindView(R.id.tvWating)
    BaseTextView tvWating;
    @BindView(R.id.cvGooglePlay)
    CardView cvGooglePlay;
    @BindView(R.id.cvCafeBazar)
    CardView cvCafeBazar;
    @BindView(R.id.cvDirectDownload)
    CardView cvDirectDownload;
    @BindView(R.id.rlBtnDl)
    BaseLinearLayout rlBtnDl;
    @BindView(R.id.rlProgress)
    BaseRelativeLayout rlProgress;
    @BindView(R.id.progress)
    android.widget.ProgressBar progress;
    @BindView(R.id.btnDownload)
    BaseRelativeLayout btnDownload;

    private View.OnClickListener onClickListener;
    private boolean isCancelabe;
    private String versionName;
    private boolean isForce;
    private ArrayList<String> features;
    private String downloadUrl;

    public ForceUpdateDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.force_update_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        Constants.setBackgroundProgress(getContext(), ProgressBar);
        setCancelable(isCancelabe);
        setDialogSize();
        if (this.features != null) {
            ArrayList<String> features = this.features;
            StringBuilder feature = new StringBuilder();
            for (int i = 0; i < features.size(); i++) {
                feature.append(features.get(i)).append("\n");
            }
            if (feature.length() > 0) {
                tvDescriptionUpdate.setText(feature);
            } else {
                tvDescriptionUpdate.setText(null);
            }
        }
        tvTitle.setText(versionName);

//        if (hasBazaar()) {
//            cvCafeBazar.setVisibility(View.VISIBLE);
//        } else {
//            cvCafeBazar.setVisibility(View.GONE);
//        }

        //relase for cafebazar then update palystore cansel//TODO relase for cafebazar
//        if (TextUtils.equals(BuildConfig.Market, MarketEnum.BAZAR.getMethodString())){
        cvGooglePlay.setVisibility(View.GONE);
        rlBtnDl.setVisibility(View.GONE);
        btnDownload.setVisibility(View.VISIBLE);
        btnDownload.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (TextUtils.equals(BuildConfig.Market, "bazar")) {
                if (hasBazaar()) {
                    downloadCafeBazar();
                } else {
//                    Toast.makeText(getContext(), "برای آپدیت لطفا بازار را نصب نمایید", Toast.LENGTH_SHORT).show();
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=ir.delta.delta"));
                }
            } else {

                if (BuildConfig.isCounsultant) {
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=ir.delta.delta.counsultant"));
                } else {
                    intent.setData(Uri.parse(downloadUrl));
                }
            }
            getContext().startActivity(intent);
        });
//        }
//        else if (TextUtils.equals(BuildConfig.Market, MarketEnum.GOOGLE.getMethodString())){
//            cvGooglePlay.setVisibility(View.VISIBLE);
//            rlBtnDl.setVisibility(View.VISIBLE);
//            btnDownload.setVisibility(View.GONE);
//            cvCafeBazar.setVisibility(View.GONE);
//        }


//        if (BuildConfig.isCounsultant) {
//            cvCafeBazar.setVisibility(View.GONE);
//        } else {
//            cvCafeBazar.setVisibility(View.VISIBLE);
//        }


        if (isForce) {
            rlBtnDl.setVisibility(View.GONE);
            rlBtn.setVisibility(View.GONE);
            btnDownload.setVisibility(View.VISIBLE);
        } else {
            btnCancel.setText(getContext().getString(R.string.continue_version));
            if (BuildConfig.isCounsultant)
                rlBtn.setVisibility(View.VISIBLE);
            else
                rlBtn.setVisibility(View.GONE);
            rlBtn.setOnClickListener(view12 -> {
                dismiss();
                if (onClickListener != null) {
                    onClickListener.onClick(view12);
                }
            });
        }

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        getContext().registerReceiver(downloadReceiver, filter);
    }

    private boolean hasBazaar() {
        try {
            PackageManager pm = getContext().getPackageManager();
            pm.getPackageInfo("com.farsitel.bazaar", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (SecurityException e) {
            return true;
        }
    }

    private void setDialogSize() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.80, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    public void setForceUpdate(String versionName, boolean isForce, ArrayList<String> features,
                               String downloadUrl, View.OnClickListener onClickListener, boolean isCancelabe) {
        this.versionName = versionName;
        this.isForce = isForce;
        this.features = features;
        this.downloadUrl = downloadUrl;
        this.onClickListener = onClickListener;
        this.isCancelabe = isCancelabe;
    }

    private void autoUpdate() {
        String urlDownloadApk = "";
        if (!downloadUrl.isEmpty()) {
            urlDownloadApk = Constants.Download_Base_Url + downloadUrl;
        }
        if (!TextUtils.isEmpty(urlDownloadApk) && URLUtil.isValidUrl(urlDownloadApk)) {
            new DownloadFileFromURL(getContext(), rlBtn, rlBtnDl, ProgressBar, rlProgress, divider, tvWating).execute(urlDownloadApk);
        }

    }

    @OnClick({R.id.cvGooglePlay, R.id.cvCafeBazar, R.id.cvDirectDownload})
    public void onViewClicked(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.cvGooglePlay:
                intent = new Intent(Intent.ACTION_VIEW);
                if (BuildConfig.isCounsultant) {
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=ir.delta.delta.counsultant"));
                } else {
                    intent.setData(Uri.parse(downloadUrl));
                }
                getContext().startActivity(intent);
                break;
            case R.id.cvCafeBazar:
                downloadCafeBazar();
                break;
            case R.id.cvDirectDownload:
//                autoUpdate();
//                intent = new Intent(getContext(), MagPostContentWebViewActivity.class);
//                intent.putExtra("link", "https://delta.ir/downloads/deltaapis/deltamag.apk");
//                getContext().startActivity(intent);
                DownloadManager downloadManager = (DownloadManager) getContext().getSystemService(DOWNLOAD_SERVICE);
                Uri uri;
                String subPath;
                String title;
                if (BuildConfig.isCounsultant) {
                    uri = Uri.parse(Constants.Download_Base_Url + downloadUrl);
                    subPath = "delta.apk";
                    title = "دلتاملک";
                } else {
                    uri = Uri.parse("https://delta.ir/downloads/deltaapis/deltamag.apk");
                    subPath = "deltaMag.apk";
                    title = "مجله دلتا";
                }
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(title);
                request.setDescription("لطفا منتظر بمانید...");
                request.setDestinationInExternalFilesDir(getContext(), Environment.DIRECTORY_DOWNLOADS, subPath);
                downloadManager.enqueue(request);
                progress.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void downloadCafeBazar() {
        try {

            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(downloadUrl));
            intent.setPackage("com.farsitel.bazaar");
            getContext().startActivity(intent);
        } catch (ActivityNotFoundException ac) {
//                autoUpdate();
        }
    }

    private final BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            progress.setVisibility(View.GONE);
            Toast.makeText(context, "دانلود به صورت کامل انجام شد و از پوشه دانلود آن را نصب کنید", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> getContext().startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS)), 1000);
        }
    };

    private static class DownloadFileFromURL extends AsyncTask<String, String, String> {

        private final WeakReference<View> dividerWeak;
        private final WeakReference<Context> contextWeak;
        private final WeakReference<View> rlBtnDl;
        private final WeakReference<View> rlbtn;
        private final WeakReference<ProgressBar> progresBarWeak;
        private final WeakReference<View> rlProgres;
        private final WeakReference<View> tvWatingWeak;

        DownloadFileFromURL(Context context, View rlbtn, View rlBtnDl, ProgressBar ProgresBar, View rlProgress, View divider, View waiting) {
            this.contextWeak = new WeakReference<>(context);
            this.rlBtnDl = new WeakReference<>(rlBtnDl);
            this.rlbtn = new WeakReference<>(rlbtn);
            progresBarWeak = new WeakReference<>(ProgresBar);
            this.rlProgres = new WeakReference<>(rlProgress);
            dividerWeak = new WeakReference<>(divider);
            tvWatingWeak = new WeakReference<>(waiting);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            android.widget.ProgressBar progress = progresBarWeak.get();
            View divider = dividerWeak.get();
            View waiting = tvWatingWeak.get();
            View btnDl = rlBtnDl.get();
            View rlBtn = rlbtn.get();
            View rlProgre = rlProgres.get();
            if (progress != null) {
                progress.setProgress(0);
                btnDl.setVisibility(View.INVISIBLE);
                rlBtn.setVisibility(View.INVISIBLE);
                divider.setVisibility(View.INVISIBLE);
                progress.setVisibility(View.VISIBLE);
                waiting.setVisibility(View.VISIBLE);
                rlProgre.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/delta.apk";
                File file = new File(destination);
                if (file.exists()) {
                    file.delete();
                }
                URL url = new URL(f_url[0]);
                HttpURLConnection conection = (HttpURLConnection) url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(destination);
                byte[] data = new byte[4096];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100.0) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return destination;
            } catch (Exception e) {
                return null;
            }

        }


        protected void onProgressUpdate(String... progress) {
            android.widget.ProgressBar progressBar = progresBarWeak.get();
            if (progressBar != null) {
                progressBar.setProgress(Integer.parseInt(progress[0]));
            }

        }

        protected void onPostExecute(String destination) {
            Context context = contextWeak.get();
            if (context == null) {
                return;
            }
            android.widget.ProgressBar progress = progresBarWeak.get();
            View rlbt = rlbtn.get();
            View btnDl = rlBtnDl.get();
            View divider = dividerWeak.get();
            View waiting = tvWatingWeak.get();
            View rlProgress = rlProgres.get();
            if (progress != null && rlbtn != null) {
                progress.setProgress(0);
                rlbt.setVisibility(View.VISIBLE);
                btnDl.setVisibility(View.VISIBLE);
                divider.setVisibility(View.VISIBLE);
                progress.setVisibility(View.INVISIBLE);
                waiting.setVisibility(View.INVISIBLE);
                rlProgress.setVisibility(View.INVISIBLE);
            }
            if (destination != null) {
                File file = new File(destination);
//                if (file.exists()) {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        Uri apkUri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);
//                        Intent i = new Intent(Intent.ACTION_INSTALL_PACKAGE);
//                        i.setData(apkUri);
//                        i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        context.startActivity(i);
//                    } else {
//                        try {
//                            Intent i = new Intent(Intent.ACTION_VIEW);
//                            Uri uri = Uri.parse("file://" + destination);
//                            i.setDataAndType(uri, "application/vnd.android.package-suspend");
//                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            context.startActivity(i);
//                        } catch (ActivityNotFoundException act) {
//
//                        }
//                    }
//                }


            } else {
                Toast.makeText(context, "خطایی رخ داده لطفا مجددا تلاش کنید", Toast.LENGTH_SHORT).show();
            }
        }

    }
}

