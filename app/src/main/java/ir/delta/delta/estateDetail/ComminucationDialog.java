package ir.delta.delta.estateDetail;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class ComminucationDialog extends Dialog {


    @BindView(R.id.profile_image)
    BaseImageView profileImage;
    @BindView(R.id.tvRealtorName)
    BaseTextView tvRealtorName;
    @BindView(R.id.btnCall)
    BaseTextView btnCall;
    @BindView(R.id.btnMessage)
    BaseTextView btnMessage;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.view2)
    View view2;
    @BindView(R.id.view3)
    View view3;

    private String logoSrc;
    private String name;
    private String mobile;
    private boolean isIsOfficeActive;
    private String userAgencyName;
    private boolean isHiddenMessageBtn;


    public void setName(String name) {
        this.name = name;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setIsOfficeActive(boolean isOfficeActive) {
        isIsOfficeActive = isOfficeActive;
    }

    public void setUserAgencyName(String userAgencyName) {
        this.userAgencyName = userAgencyName;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }


    public ComminucationDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.custom_cominucation_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);

        setDefultImage();

        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                double width = size.x * 0.7;
                if (width >= getContext().getResources().getDimensionPixelSize(R.dimen.dialog_width_comminucatio)) {
                    width = getContext().getResources().getDimensionPixelSize(R.dimen.dialog_width_comminucatio);
                } else {
                    width = size.x * 0.7;
                }
                window.setLayout((int) width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }

        if (isHiddenMessageBtn){
            btnMessage.setVisibility(View.GONE);
        }else {
            btnMessage.setVisibility(View.VISIBLE);
        }
    }


    private void setDefultImage() {

        if (isIsOfficeActive) {
            if (!name.trim().equals("")) {
                tvRealtorName.setText(getContext().getString(R.string.consultant) + ": " + name.trim());
            } else if (!userAgencyName.trim().equals("")) {
                tvRealtorName.setText(getContext().getString(R.string.agency_text) + ": " + userAgencyName.trim());
            } else {
                tvRealtorName.setText(null);
            }
            if (TextUtils.isEmpty(logoSrc)) {
                Glide.with(getContext()).load(R.drawable.ic_profile).into(profileImage);
                profileImage.setColorFilter(getContext().getResources().getColor(R.color.light_gray), PorterDuff.Mode.SRC_ATOP);
            } else {
                Glide.with(getContext()).load(logoSrc).into(profileImage);
            }
        } else {
            if (!TextUtils.isEmpty(name.trim())) {
                tvRealtorName.setText(getContext().getString(R.string.advertiser) + ": " + name.trim());
            } else {
                tvRealtorName.setText(null);
            }
            Glide.with(getContext()).load(R.drawable.ic_profile).into(profileImage);
            profileImage.setColorFilter(getContext().getResources().getColor(R.color.light_gray), PorterDuff.Mode.SRC_ATOP);
        }
    }


    @OnClick({R.id.btnCall, R.id.btnMessage, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCall:
                try {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + mobile.trim()));
                    getContext().startActivity(intent);
                } catch (ActivityNotFoundException act) {

                }

                break;
            case R.id.btnMessage:
                try {
                    getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", mobile, null)));
                } catch (ActivityNotFoundException act) {

                }
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    public void isHiddenMessageBtn(boolean isShow){
        this.isHiddenMessageBtn = isShow;
    }
}
