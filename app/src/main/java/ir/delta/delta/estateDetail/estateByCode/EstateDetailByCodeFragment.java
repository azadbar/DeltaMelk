package ir.delta.delta.estateDetail.estateByCode;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.DetailOtherRowView;
import ir.delta.delta.customView.DetailRowView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.estateDetail.BugReportDialog;
import ir.delta.delta.estateDetail.ComminucationDialog;
import ir.delta.delta.estateDetail.SliderAdapter;
import ir.delta.delta.favorite.Favorite;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DetailEstateByCodeService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.DepositDetailByCodeReq;
import ir.delta.delta.service.ResponseModel.DetailEstateData;
import ir.delta.delta.service.ResponseModel.DetailEstateDisplayOtherFacilities;
import ir.delta.delta.service.ResponseModel.DetailEstateDisplayText;
import ir.delta.delta.service.ResponseModel.DetailEstateResponse;
import ir.delta.delta.service.ResponseModel.DetailListItem;
import ir.delta.delta.service.ResponseModel.FailureBugReport;
import ir.delta.delta.service.ResponseModel.FavoritePropertyResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.Validation;
import me.relex.circleindicator.CircleIndicator;

public class EstateDetailByCodeFragment extends BaseFragment implements Favorite.OnFavoriteListener {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgShare)
    BaseImageView imgShare;
    @BindView(R.id.imgLike)
    BaseImageView imgLike;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.tvPrice)
    BaseTextView tvPrice;
    @BindView(R.id.tvRent)
    BaseTextView tvRent;
    @BindView(R.id.tvTextDetail)
    BaseTextView tvTextDetail;
    @BindView(R.id.tvDescription)
    BaseTextView tvDescription;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    @BindView(R.id.tvCodeEstate)
    BaseTextView tvCodeEstate;
    @BindView(R.id.tvMobile)
    BaseTextView tvMobile;
    @BindView(R.id.tvAgencyTell)
    BaseTextView tvAgencyTell;
    @BindView(R.id.imgAgencyLogo)
    BaseImageView imgAgencyLogo;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.addRow)
    LinearLayout addRow;
    @BindView(R.id.llDetail)
    BaseLinearLayout llDetail;
    @BindView(R.id.tvTextContacts)
    BaseTextView tvTextContacts;
    @BindView(R.id.tvTxtNameAgency)
    BaseTextView tvTxtNameAgency;
    @BindView(R.id.tvNameAgency)
    BaseTextView tvNameAgency;
    @BindView(R.id.tvTxtAgencyTell)
    BaseTextView tvTxtAgencyTell;
    @BindView(R.id.tvTxtCodeEstate)
    BaseTextView tvTxtCodeEstate;
    @BindView(R.id.llCodeEstate)
    BaseLinearLayout llCodeEstate;
    @BindView(R.id.tvTxtMobile)
    BaseTextView tvTxtMobile;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.llDescription)
    BaseLinearLayout llDescription;
    @BindView(R.id.llLocation)
    BaseLinearLayout llLocation;
    @BindView(R.id.cvTitle)
    CardView cvTitle;
    @BindView(R.id.cvDetail)
    CardView cvDetail;
    @BindView(R.id.cvDescription)
    CardView cvDescription;
    @BindView(R.id.cvLocation)
    CardView cvLocation;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.llButton)
    BaseLinearLayout llButton;
    @BindView(R.id.rlPager)
    BaseRelativeLayout rlPager;

    Unbinder unbinder;
    @BindView(R.id.rlComminucation)
    BaseRelativeLayout rlComminucation;
    @BindView(R.id.rlBugReport)
    BaseRelativeLayout rlBugReport;
    @BindView(R.id.icComminucation)
    BaseImageView icComminucation;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.edtEstateCode)
    BaseEditText edtEstateCode;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.rlLike)
    BaseRelativeLayout rlLike;
    @BindView(R.id.rlShare)
    BaseRelativeLayout rlShare;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;

    private DetailEstateData estateDetail;
    private ArrayList<FailureBugReport> failureItemList;
    private boolean disLike;
    private boolean isFavoriteDeposit;
    private boolean isSearch;
    private String callNumber;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_estate_details_by_code, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            imgBack.setImageResource(R.drawable.ic_back_english);
        } else {
            imgBack.setImageResource(R.drawable.ic_back);
        }
        HideShowViewInit(View.GONE);

        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        icComminucation.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

        edtEstateCode.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                getDetailData();
            }
            return false;
        });

        /*edtEstateCode.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 2000;

            @Override
            public void afterTextChanged(Editable s) {
                imgSearch.setVisibility(View.VISIBLE);
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty()) {
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(() -> {
                                            getDetailData();
                                        });
                                    }
                                }
                            },
                            DELAY);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty()) {
                    if (timer != null) {
                        timer.cancel();
                    }
                }
            }
        });*/

        loadingView.setOnClickListener(view1 -> {
            if (estateDetail != null) {
                getDetailData();
            }
        });
        if (getActivity() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                rlPager.getLayoutParams().height = (int) (size.x * 12 / 16.0);
            }
        }
        return view;
    }


    @OnClick({R.id.imgShare, R.id.imgLike, R.id.rlBugReport, R.id.rlBack, R.id.rlComminucation, R.id.imgSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgShare:
                if (getContext() != null && !TextUtils.isEmpty(estateDetail.getEnOfficeId())) {
                    Constants.shareTextUrl(getContext(), Constants.Download_Base_Url + estateDetail.getEnOfficeId());
                }
                break;
            case R.id.imgLike:
                Favorite favorite = new Favorite(getContext(), this);
                if (isFavoriteDeposit) {
                    if (Constants.getUser(getActivity()) != null) {
                        roundedLoadingView.setVisibility(View.VISIBLE);
                        enableDisableViewGroup(root, false);
                        if (Constants.getUser(getActivity()).isGeneral()) {
                            favorite.unFavoriteUserRequest(estateDetail.getDeposiId());
                        } else {
                            favorite.unFavoriteConsultantProperty(estateDetail.getDeposiId());
                        }
                    } else {
                        login();
                        disLike = true;
                    }

                } else {
                    if (Constants.getUser(getActivity()) != null) {
                        roundedLoadingView.setVisibility(View.VISIBLE);
                        enableDisableViewGroup(root, false);
                        if (Constants.getUser(getActivity()).isGeneral()) {
                            favorite.favoriteUserRequest(estateDetail.getDeposiId());
                        } else {
                            favorite.favoriteConsultantProperty(estateDetail.getDeposiId());
                        }
                    } else {
                        login();
                        disLike = false;
                    }
                }

                break;
            case R.id.rlBugReport:
                if (failureItemList != null && estateDetail.getEnDepositId() != null && getActivity() != null) {
                    BugReportDialog bugReportDialog = new BugReportDialog(getActivity());
                    bugReportDialog.setFailureBugReports(failureItemList);
                    bugReportDialog.setEncodedId(estateDetail.getEnDepositId());
                    bugReportDialog.show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.not_find_bug_report), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rlComminucation:
                if (estateDetail != null && !TextUtils.isEmpty(callNumber)) {
                    if (getActivity() != null) {
                        ComminucationDialog comminucationDialog = new ComminucationDialog(getActivity());
                        comminucationDialog.setName(estateDetail.getName());
                        comminucationDialog.setMobile(callNumber);
                        comminucationDialog.setUserAgencyName(estateDetail.getUserAgencyName());
                        comminucationDialog.setIsOfficeActive(estateDetail.isOfficeActive());
                        comminucationDialog.setLogoSrc(estateDetail.getUserAgencyLogo());
                        comminucationDialog.isHiddenMessageBtn(estateDetail.isMobileHidden());
                        comminucationDialog.show();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.not_find_mobile), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.rlBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.imgSearch:
                if (TextUtils.isEmpty(edtEstateCode.getValueString())){
                    edtEstateCode.setText("");
//                    edtEstateCode.setHint(getResources().getString(R.string.search_mag));
                    imgSearch.setImageResource(R.drawable.ic_search_gray);
                    imgSearch.setColorFilter(getResources().getColor(R.color.black),PorterDuff.Mode.SRC_ATOP);
                    Toast.makeText(getContext(), "لطفا کد ملک مورد نظر را وارد کنید.", Toast.LENGTH_SHORT).show();
                    isSearch = false;
                }else {
                    if (isSearch){
                        edtEstateCode.setText("");
                        edtEstateCode.setHint(getResources().getString(R.string.search_mag));
                        imgSearch.setImageResource(R.drawable.ic_search_gray);
                        imgSearch.setColorFilter(getResources().getColor(R.color.black),PorterDuff.Mode.SRC_ATOP);
                        isSearch = false;
                    }else {
                        getDetailData();
                        imgSearch.setImageResource(R.drawable.ic_clear_black_24dp);
                        imgSearch.setColorFilter(getResources().getColor(R.color.black),PorterDuff.Mode.SRC_ATOP);
                        isSearch = true;
                    }
                }

                break;
        }
    }


    private void login() {
        if (getActivity() != null) {
            CustomDialog dialog = new CustomDialog(getActivity());
            dialog.setIcon(R.drawable.ic_lock, getResources().getColor(R.color.redColor));
            dialog.setDialogTitle(getResources().getString(R.string.login_account));
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setDescription(getString(R.string.desc__login_to_account_for_like));
            dialog.setOkListener(getString(R.string.login), v -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivityForResult(intent, Constants.LOGIN_LIKE);
                dialog.dismiss();
            });
            dialog.setCancelListener(getString(R.string.cancel), v -> {
                dialog.dismiss();
            });
            dialog.setCancelable(true);
            dialog.show();
        }
    }


    private void getDetailData() {
        if (getActivity() == null) {
            return;
        }
        Constants.hideKeyboard(getActivity());
        rootEmptyView.setVisibility(View.GONE);
        addRow.removeAllViewsInLayout();
        HideShowViewInit(View.GONE);
        loadingView.showLoading(false);
        DepositDetailByCodeReq req = new DepositDetailByCodeReq();
        req.setId(edtEstateCode.getValueInt());
        if (Constants.getUser(getActivity()) != null) {
            if (Constants.getUser(getActivity()).isGeneral()) {
                req.setLogedInUserTokenOrId(Constants.getUser(getActivity()).getUserToken());
            } else {
                req.setLogedInUserTokenOrId(String.valueOf(Constants.getUser(getActivity()).getUserId()));
            }
        } else {
            req.setLogedInUserTokenOrId("");
        }
        DetailEstateByCodeService.getInstance().getDepositDetailByCode(getResources(), req, new ResponseListener<DetailEstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    loadingView.showError(error);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(DetailEstateResponse response) {
                if (getView() == null || getActivity() == null) {
                    return;
                }

                loadingView.stopLoading();
                if (response.isSuccessed()) {
                    estateDetail = response.getDetailEstateData();
                    failureItemList = response.getFailureItemList();
                    isFavoriteDeposit = response.isFavoriteDeposit();
                    setDataInView();
                } else {
                    rootEmptyView.setVisibility(View.VISIBLE);
                    if (TextUtils.isEmpty(response.getMessage())) {
                        tvEmptyView.setText(getResources().getString(R.string.not_found_estate));
                    } else {
                        tvEmptyView.setText(response.getMessage());
                    }
                }
            }
        });
    }

    @SuppressLint({"SetTextI18n", "SetJavaScriptEnabled"})
    private void setDataInView() {
        HideShowViewInit(View.VISIBLE);
        makeSlider(estateDetail.getImagesList());
        if (isFavoriteDeposit) {
            imgLike.setImageResource(R.drawable.ic_favorite_deep_white);
            imgLike.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        } else {
            imgLike.setImageResource(R.drawable.ic_favorite_border);
            imgLike.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        }
        tvTitle.setText(estateDetail.getDepositTitle());
        if (!TextUtils.isEmpty(estateDetail.getDisplayMortgageOrTotal()) && estateDetail.getMortgageOrTotal() >= 1000) {
            tvPrice.setText(createHtmlText("<font color='#999999'>" + estateDetail.getDisplayMortgageOrTotal() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(estateDetail.getMortgageOrTotal(), getResources()) + "</strong></font>"));
        } else {
            tvPrice.setText(null);
        }

        if (!TextUtils.isEmpty(estateDetail.getDisplayRentOrMetric()) && estateDetail.getRentOrMetric() >= 1000) {
            tvRent.setText(createHtmlText("<font color='#999999'>" + estateDetail.getDisplayRentOrMetric() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(estateDetail.getRentOrMetric(), getResources()) + "</strong></font>"));
        } else {
            tvRent.setText(null);
        }

        if (estateDetail.getDisplayText() == null || estateDetail.getDisplayText().size() == 0 || estateDetail.getDisplayOtherFacilities().size() == 0 ||
                estateDetail.getDisplayOtherFacilities() == null) {
            cvDetail.setVisibility(View.GONE);
        } else {
            cvDetail.setVisibility(View.VISIBLE);
            for (int i = 0; i < estateDetail.getDisplayText().size(); i++) {
                DetailEstateDisplayText display = estateDetail.getDisplayText().get(i);
                addRowView(display, i);

            }

            int listCount = estateDetail.getDisplayText().size();
            for (int i = 0; i < estateDetail.getDisplayOtherFacilities().size(); i++) {
                DetailEstateDisplayOtherFacilities other = estateDetail.getDisplayOtherFacilities().get(i);
                addOtherRowView(other, listCount + i);
            }
        }

        if (!estateDetail.getDescription().equals("")) {
            cvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(estateDetail.getDescription());
        } else {
            cvDescription.setVisibility(View.GONE);
        }

        cvLocation.setVisibility(View.GONE);

        if (!estateDetail.getName().trim().equals("") && estateDetail.isIsOfficeActive()) {
            tvNameAgency.setVisibility(View.VISIBLE);
            if (!estateDetail.getUserAgencyName().equals("")) {
                tvNameAgency.setText(getResources().getString(R.string.consultant) + ": " + estateDetail.getName().trim() + "، " + estateDetail.getUserAgencyName().trim());
            } else {
                tvNameAgency.setText(getResources().getString(R.string.consultant) + ": " + estateDetail.getName().trim());
            }
        } else {
            tvNameAgency.setText(getString(R.string.advertiser) + ": " + estateDetail.getName().trim());
            tvCodeEstate.setText(getResources().getString(R.string.code_estate) + ": " + estateDetail.getDeposiId());
            tvMobile.setText(getResources().getString(R.string.mobile_text) + ": " + estateDetail.getMobile());
            tvAgencyTell.setVisibility(View.GONE);
        }
        if (estateDetail.getDeposiId() > 0) {
            tvCodeEstate.setVisibility(View.VISIBLE);
            tvCodeEstate.setText(getResources().getString(R.string.code_estate) + ": " + estateDetail.getDeposiId());
        } else {
            tvCodeEstate.setVisibility(View.GONE);
        }

        callNumber = estateDetail.getUserAgencyPhone();
        if (!estateDetail.isMobileHidden()) {
            if (!TextUtils.isEmpty(estateDetail.getMobile())) {
                tvMobile.setVisibility(View.VISIBLE);
                tvMobile.setText(getResources().getString(R.string.mobile_text) + ": " + estateDetail.getMobile());
                callNumber = estateDetail.getMobile();
            } else {
                tvMobile.setVisibility(View.GONE);
            }
        } else {
            tvMobile.setVisibility(View.GONE);
        }


        if (!estateDetail.getUserAgencyPhone().equals("")) {
            tvAgencyTell.setVisibility(View.VISIBLE);
            tvAgencyTell.setText(getResources().getString(R.string.agency_tell_text) + ": " + estateDetail.getUserAgencyPhone());
        } else {
            tvAgencyTell.setVisibility(View.GONE);
        }
        if (estateDetail.isIsOfficeActive()) {
            if (getContext() != null) {
                imgAgencyLogo.setVisibility(View.VISIBLE);
                Glide.with(getContext()).load(estateDetail.getUserAgencyLogo()).into(imgAgencyLogo);
            }
        } else {
            imgAgencyLogo.setVisibility(View.GONE);
        }
        if (estateDetail.getOfficeStarsCnt() > 0) {
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar.setRating(estateDetail.getOfficeStarsCnt());
        } else {
            ratingBar.setVisibility(View.GONE);
        }
    }

    private void addRowView(DetailEstateDisplayText detailEstateDisplayText, int index) {
        DetailRowView detailRowView = new DetailRowView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        detailRowView.setLayoutParams(lp);
        addRow.addView(detailRowView);
        detailRowView.setTextTitle(detailEstateDisplayText.getText(), detailEstateDisplayText.getValue());
        detailRowView.setBackgroundColor(index % 2 == 0 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.oddW));
    }


    private void addOtherRowView(DetailEstateDisplayOtherFacilities other, int index) {
        DetailOtherRowView detailOtherRowView = new DetailOtherRowView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        detailOtherRowView.setLayoutParams(lp);
        addRow.addView(detailOtherRowView);
        detailOtherRowView.setTextTitle(other.getText());
        if (other.isValue()) {
            detailOtherRowView.setImageIcon(R.drawable.ic_check_circle_green, R.color.applyFore);
            detailOtherRowView.setTextStatus(getString(R.string.exist));
            detailOtherRowView.setTextStatusColor(getResources().getColor(R.color.primaryTextColor));
        } else {
            detailOtherRowView.setImageIcon(R.drawable.ic_cancel, R.color.primaryBack);
            detailOtherRowView.setTextStatus(getString(R.string.no_exist));
            detailOtherRowView.setTextStatusColor(getResources().getColor(R.color.primaryTextColor));
        }
        detailOtherRowView.setBackgroundColor(index % 2 == 0 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.oddW));

    }

    private void makeSlider(List<DetailListItem> imagesList) {
        final ArrayList<String> images = new ArrayList<>();
        for (int i = 0; i < imagesList.size(); i++) {
            images.add(imagesList.get(i).getPath());
        }

        pager.setAdapter(new SliderAdapter(images));
        if (images.size() == 0) {
            pager.setBackgroundResource(R.drawable.placehoder_big);
        }
        if (images.size() == 1) {
            indicator.setVisibility(View.GONE);
        } else {
            indicator.setViewPager(pager);
        }
    }

    private void HideShowViewInit(int visiblity) {
        llButton.setVisibility(visiblity);
        scrollView.setVisibility(visiblity);
        rlLike.setVisibility(visiblity);
        rlShare.setVisibility(visiblity);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.LOGIN_LIKE && resultCode == Activity.RESULT_OK) {
            Favorite favorite = new Favorite(getContext(), this);
            roundedLoadingView.setVisibility(View.VISIBLE);
            enableDisableViewGroup(root, false);
            if (Constants.getUser(getActivity()).isGeneral()) {
                if (disLike) {
                    favorite.unFavoriteUserRequest(estateDetail.getDeposiId());
                } else {
                    favorite.favoriteUserRequest(estateDetail.getDeposiId());
                }
            } else {
                if (disLike) {
                    favorite.unFavoriteConsultantProperty(estateDetail.getDeposiId());
                } else {
                    favorite.favoriteConsultantProperty(estateDetail.getDeposiId());
                }
            }
        }
    }


    @Override
    public void favoriteUserGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void favoriteUserOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                imgLike.setImageResource(R.drawable.ic_bookmark);
                imgLike.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));
                imgLike.setImageResource(R.drawable.ic_bookmark);
                imgLike.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
            isFavoriteDeposit = true;
        }
    }

    @Override
    public void unFavoriteUserGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void unFavoriteUserOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                imgLike.setImageResource(R.drawable.ic_favorite_border);
                imgLike.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
                isFavoriteDeposit = false;
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));

            }
        }
    }

    @Override
    public void favoriteConsultantPropertyGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void favoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                imgLike.setImageResource(R.drawable.ic_bookmark);
                imgLike.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));

                imgLike.setImageResource(R.drawable.ic_bookmark);
                imgLike.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
            }
            isFavoriteDeposit = true;
        }
    }

    @Override
    public void unFavoriteConsultantPropertyGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void unFavoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                imgLike.setImageResource(R.drawable.ic_favorite_border);
                imgLike.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
                isFavoriteDeposit = false;
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));
                imgLike.setImageResource(R.drawable.ic_favorite_deep_white);
                imgLike.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }
}
