package ir.delta.delta.estateDetail;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.FailureBugReport;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class BugReportAdapter extends RecyclerView.Adapter<BugReportAdapter.ViewHolder> {

    private final ArrayList<FailureBugReport> list;
    private final BugReportAdapter.OnItemClickListener listener;
    private FailureBugReport failureBugReport;

    BugReportAdapter(ArrayList<FailureBugReport> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bug_report, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        FailureBugReport object = list.get(position);
        holder.tvTitle.setText(object.getText());
        if (failureBugReport != null && TextUtils.equals(failureBugReport.getValue(),object.getValue())) {
            setSelcted(res, holder);
        } else {
            setNormal(res, holder);
        }
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgSelect)
        BaseImageView imgSelect;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.row)
        BaseRelativeLayout row;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickSingle(position));
        }

    }

    public void setFailureBugReport(FailureBugReport failureBugReport) {
        this.failureBugReport = failureBugReport;
        notifyDataSetChanged();
    }

    private void setSelcted(Resources res, ViewHolder holder) {
        holder.imgSelect.setImageResource(R.drawable.ic_check_circle_red);
        holder.tvTitle.setTextColor(res.getColor(R.color.redColor));
    }

    private void setNormal(Resources res, ViewHolder holder) {
        holder.imgSelect.setImageResource(R.drawable.ic_circle);
        holder.tvTitle.setTextColor(res.getColor(R.color.secondaryTextColor));
    }

    public interface OnItemClickListener {
        void onItemClickSingle(int position);
    }
}
