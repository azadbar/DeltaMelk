package ir.delta.delta.estateDetail;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.service.ResponseModel.Estate;

public class EstateDetailActivity extends BaseActivity {

    @BindView(R.id.frameLayout_estate)
    LinearLayout frameLayoutEstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estate_detail);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String encodedId = bundle.getString("encodedId");
            if (encodedId != null) {
                EstateDetailFragment estateDetailFragment = new EstateDetailFragment();
                FragmentManager fragMgr = getSupportFragmentManager();
                FragmentTransaction fragTrans = fragMgr.beginTransaction();
                Bundle bundle1 = new Bundle();
                bundle1.putString("encodedId", encodedId);
                estateDetailFragment.setArguments(bundle1);
                fragTrans.replace(R.id.frameLayout_estate, estateDetailFragment);
                fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragTrans.commit();
            } else {
                finish();
            }
        }
    }
}
