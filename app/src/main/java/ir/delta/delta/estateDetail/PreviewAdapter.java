package ir.delta.delta.estateDetail;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {

    private final ArrayList<String> list;
    private final OnItemClickListener listener;
    private int currentPosition = 0;


    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
        notifyDataSetChanged();
    }

    PreviewAdapter(ArrayList<String> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_thumb_image, parent, false);
        return new ViewHolder(itemView);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        if (position == currentPosition) {
//            holder.card.setBackgroundResource(R.drawable.select_preview_image);
//        } else {
//            holder.card.setBackgroundResource(R.drawable.unselect_preview_image);
//        }
        Glide.with(holder.imgCards.getContext()).load("" + list.get(position)).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(holder.imgCards);
        holder.bind(position, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgDisplay)
        ImageView imgCards;
        @BindView(R.id.card)
        CardView card;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
