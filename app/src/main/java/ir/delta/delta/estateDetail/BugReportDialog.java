package ir.delta.delta.estateDetail;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.registerEstate.InfoListDialog;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomMultiLineEditText;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.BugReportService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.BugReportReq;
import ir.delta.delta.service.ResponseModel.BugReportResponse;
import ir.delta.delta.service.ResponseModel.FailureBugReport;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.util.Constants;


public class BugReportDialog extends Dialog implements BugReportAdapter.OnItemClickListener {

    @BindView(R.id.tvTxtBugReport)
    BaseTextView tvTxtBugReport;
    @BindView(R.id.rvBugReport)
    RecyclerView rvBugReport;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    @BindView(R.id.edtBugReport)
    CustomMultiLineEditText edtBugReport;
    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtName)
    CustomEditText edtName;

    private ArrayList<FailureBugReport> failureBugReports;
    private FailureBugReport failurCode;
    public String encodedId;
    private BugReportAdapter bugReportAdapter;

    public void setEncodedId(String encodedId) {
        this.encodedId = encodedId;
    }

    public void setFailureBugReports(ArrayList<FailureBugReport> failureBugReports) {
        this.failureBugReports = failureBugReports;
    }

    public BugReportDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.bug_report_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        Window window = getWindow();
        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null && window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Point size = Constants.getScreenSize(windowManager);
            int width = (int) Math.min(size.x * 0.90, Objects.requireNonNull(getContext()).getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
            int height = (int) Math.min(size.y * 0.60, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_bug_report_height));
            window.setLayout(width, height);
            window.setGravity(Gravity.CENTER);
        }
        edtBugReport.setTextsTitle(getContext().getString(R.string.description_text));
        setDataAdapter();
    }


    private void setDataAdapter() {
        bugReportAdapter = new BugReportAdapter(failureBugReports, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvBugReport.setLayoutManager(layoutManager);
        rvBugReport.setAdapter(bugReportAdapter);
    }


    @OnClick({R.id.btnOk, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                sendBugReport();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    private void sendBugReport() {
        if (failurCode != null && edtName.getError() == null && edtMobile.getError() == null) {
            progressDialog.setVisibility(View.VISIBLE);
            BugReportReq req = new BugReportReq();
            req.setEnDepositId(encodedId);
            req.setTypeId(Integer.parseInt(failurCode.getValue()));
            req.setSenderName(edtName.getValueString());
            req.setMobile(edtMobile.getValueString());
            req.setDescription(edtBugReport.getTextVal());
            BugReportService.getInstance().BugReportService(getContext().getResources(), req, new ResponseListener<BugReportResponse>() {
                @Override
                public void onGetError(String error) {
                    if (progressDialog != null) {
                        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                        progressDialog.setVisibility(View.GONE);
                        dismiss();
                    }
                }

                @Override
                public void onAuthorization() {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getContext());
                    getContext().startActivity(intent);
                    dismiss();
                }

                @Override
                public void onSuccess(BugReportResponse response) {
                    if (progressDialog != null) {
                        progressDialog.setVisibility(View.GONE);
                        if (response.isSuccessed()) {
                            Toast.makeText(getContext(), getContext().getResources().getString(R.string.message_was_sucees_record), Toast.LENGTH_SHORT).show();
                        } else {
                            ArrayList<String> error = new ArrayList<>();
                            for (ModelStateErrors m : response.getModelStateErrors()) {
                                error.add(m.getMessage());
                            }
                            showInfoDialog("موارد زیر را بررسی کنید", error);
                        }
                        dismiss();
                    }
                }
            });
        } else {
            progressDialog.setVisibility(View.GONE);
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.enter_name_family), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onItemClickSingle(int position) {
        failurCode = failureBugReports.get(position);
        bugReportAdapter.setFailureBugReport(failurCode);
    }

    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        InfoListDialog infoListDialog = new InfoListDialog(getContext());
        infoListDialog.errorMsg = errorMsgList;
        infoListDialog.title = title;
        infoListDialog.show();
    }
}
