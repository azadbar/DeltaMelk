package ir.delta.delta.estateDetail;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import ir.delta.delta.R;

/**
 * create by m.azadbar
 */
public class SliderAdapter extends PagerAdapter {

    private final ArrayList<String> images;

    public SliderAdapter(ArrayList<String> images) {
        this.images = images;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        String urlAddress = images.get(position);
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View myImageLayout = inflater.inflate(R.layout.slide, view, false);
        ImageView myImage = myImageLayout.findViewById(R.id.image);
        myImage.setOnClickListener(view1 -> {
            if (images.size() > 0) {
                Intent intent = new Intent(view1.getContext(), PreviewActivity.class);
                intent.putStringArrayListExtra("images", images);
                view1.getContext().startActivity(intent);
            }
        });

        final ProgressBar pr = myImageLayout.findViewById(R.id.progress);

        Glide.with(myImage.getContext())
                .load(urlAddress)
                .fitCenter()
                .placeholder(R.drawable.no_photo_big)
                .addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        pr.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        pr.setVisibility(View.GONE);
                        return false;
                    }
                }).error(R.drawable.no_photo_big).into(myImage);

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
