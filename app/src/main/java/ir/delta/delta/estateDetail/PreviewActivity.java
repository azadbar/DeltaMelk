package ir.delta.delta.estateDetail;

import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.util.Constants;

public class PreviewActivity extends BaseActivity implements PreviewAdapter.OnItemClickListener {


    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    public static ArrayList<String> images;
    int currentPosition = 0;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.left_nav)
    ImageButton leftNav;
    @BindView(R.id.right_nav)
    ImageButton rightNav;
    private static PreviewAdapter adapter;
    ImageFragmentPagerAdapter imageFragmentPagerAdapter;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            imgBack.setImageResource(R.drawable.ic_back_english);
        } else {
            imgBack.setImageResource(R.drawable.ic_back);
        }
        imgLogo.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);

        changeStatusBar(getResources().getColor(R.color.black));

        imgBack.setColorFilter(getResources().getColor(R.color.primaryFore), PorterDuff.Mode.SRC_ATOP);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            images = bundle.getStringArrayList("images");
        }
//        imageFragmentPagerAdapter = new ImageFragmentPagerAdapter(getSupportFragmentManager(), images);
//        pager.setAdapter(imageFragmentPagerAdapter);

        if (images != null && images.size() == 1) {
            recyclerview.setVisibility(View.GONE);
        } else {
            recyclerview.setVisibility(View.VISIBLE);
        }
        pager = findViewById(R.id.pager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(images);

        pager.setAdapter(viewPagerAdapter);

//        setImageUri();
        setImageAdapter();
    }


    public static class ImageFragmentPagerAdapter extends FragmentPagerAdapter {
        private final ArrayList<String> images;

        ImageFragmentPagerAdapter(FragmentManager fm, ArrayList<String> images) {
            super(fm);
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public Fragment getItem(int position) {
            return SwipeFragment.newInstance(position);
        }
    }

    public static class SwipeFragment extends Fragment {
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View swipeView = inflater.inflate(R.layout.swipe_fragment, container, false);
            DecomTouchImageView imageView = swipeView.findViewById(R.id.imageView);
            Bundle bundle = getArguments();
            if (bundle != null) {
                int position = bundle.getInt("position");
                String imageFileName = images.get(position);
                Glide.with(getActivity()).load(imageFileName).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);
                adapter.setCurrentPosition(position);
            }

//            int imgResId = getResources().getIdentifier(imageFileName, "drawable", "ir.delta.delta.estateDetail");
//            imageView.setImageResource(imgResId);
            return swipeView;
        }

        static SwipeFragment newInstance(int position) {
            SwipeFragment swipeFragment = new SwipeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }
    }

    public void setImageUri(int position) {
        pager.setCurrentItem(position);
    }

    private void setImageAdapter() {
        adapter = new PreviewAdapter(images, this);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerview.setLayoutManager(horizontalLayoutManagaer);
        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(int position) {
        currentPosition = position;
        setImageUri(currentPosition);
        if (adapter != null) {
            adapter.setCurrentPosition(currentPosition);
        }
    }

    @OnClick({R.id.left_nav, R.id.right_nav, R.id.rlBack})
    public void onViewClicked(View view) {
        int tab;
        switch (view.getId()) {
            case R.id.left_nav:
                tab = pager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    pager.setCurrentItem(tab);
                } else if (tab == 0) {
                    pager.setCurrentItem(tab);
                }
                break;
            case R.id.right_nav:
                tab = pager.getCurrentItem();
                tab++;
                pager.setCurrentItem(tab);
                break;
            case R.id.rlBack:
                finish();
                break;
        }
    }


}
