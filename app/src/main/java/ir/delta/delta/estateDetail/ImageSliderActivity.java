package ir.delta.delta.estateDetail;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;

public class ImageSliderActivity extends BaseActivity {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.left_nav)
    ImageButton leftNav;
    @BindView(R.id.right_nav)
    ImageButton rightNav;
    @BindView(R.id.recyclerviewFrag)
    RecyclerView recyclerviewFrag;
    ArrayList<String> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide3);
        ButterKnife.bind(this);
        loadArray(this);
    }

    private void loadArray(ImageSliderActivity imageSliderActivity) {
        SharedPreferences mSharedPreference1 = PreferenceManager.getDefaultSharedPreferences(imageSliderActivity);
        images.clear();
        int size = mSharedPreference1.getInt("Status_size", 0);
        for (int i = 0; i < size; i++) {
            images.add(mSharedPreference1.getString("Status_" + i, null));
        }
        GalleryPagerAdapter adapter = new GalleryPagerAdapter(this);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(4); // how many images to load into memory
        adapter.notifyDataSetChanged();
        HorizontalAdapter horizontalAdapter = new HorizontalAdapter(images);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerviewFrag.setLayoutManager(horizontalLayoutManagaer);
        recyclerviewFrag.setAdapter(horizontalAdapter);
        horizontalAdapter.notifyDataSetChanged();
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    pager.setCurrentItem(tab);
                } else if (tab == 0) {
                    pager.setCurrentItem(tab);
                }
            }
        });
        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                tab++;
                pager.setCurrentItem(tab);
            }
        });
        // MenuScroll.setSmoothScrollingEnabled(true);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //MenuScroll.smoothScrollTo(position*302, 0);
                /*int offset=(position*300)+(position+1)*4;
                if(offset<(304*_adapter.getCount()-2))
                MenuScroll.smoothScrollBy(offset, 0);*/

            }
            @Override
            public void onPageSelected(int position) {
                /*  int offset=(position*300)+(position+1)*4;
                if(offset<(304*_adapter.getCount()-4))
                MenuScroll.smoothScrollTo((position*300)+(position+1)*4, 0);
                */
                recyclerviewFrag.scrollToPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private class GalleryPagerAdapter extends PagerAdapter {
        Context _context;
        LayoutInflater _inflater;

        GalleryPagerAdapter(Context context) {
            _context = context;
            _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {

            return images.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View itemView = _inflater.inflate(R.layout.pager_gallery_item, container, false);
            container.addView(itemView);

            // Get the border size to show around each image
            //int borderSize = _thumbnails.getPaddingTop();

            // Get the size of the actual thumbnail image
            // int thumbnailSize = ((RelativeLayout.LayoutParams)
            //        _pager.getLayoutParams()).bottomMargin - (borderSize*2);

            // Set the thumbnail layout parameters. Adjust as required
            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(300, 260);
            params.setMargins(3, 3, 3, 3);

            // You could also set like so to remove borders
           /* ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                   ViewGroup.LayoutParams.WRAP_CONTENT,
                   ViewGroup.LayoutParams.WRAP_CONTENT);*/
            //   bmArray = new ArrayList<Bitmap>();
            final ImageView thumbView = new ImageView(_context);
            thumbView.setTag(position);
            thumbView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            thumbView.setLayoutParams(params);
            thumbView.setMinimumHeight(260);

            thumbView.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {
                    // Set the pager position when thumbnail clicked
                    //v.setSelected(true);
                   /* for (int j=0;j<_thumbnails.getChildCount();j++)
                    {
                        if (_thumbnails.getChildAt(j).getBackground()!=null) {
                            _thumbnails.getChildAt(j).setBackground(null);
                        }
                    }

                    Drawable highlight = getResources().getDrawable( R.drawable.border);
                    _thumbnails.getChildAt(position).setPadding(4,4,4,4);
                    _thumbnails.getChildAt(position).setBackground(highlight);*/
                    pager.setCurrentItem(position);
                    //int offset=(position*300)+(position+1)*4;
                    //if(offset<(304*_adapter.getCount()-2))
                    //MenuScroll.smoothScrollTo(offset, 0);
                }
            });

            //  _thumbnails.addView(thumbView);

            final ImageView imageView = itemView.findViewById(R.id.image);
            /*final ImageView myCustomIcon = (ImageView) LayoutInflater.
                    from(tabL.getContext()).inflate(R.layout.my_custom_tab, null);
            myCustomIcon.setLayoutParams(params);
            myCustomIcon.setPadding(0,0,0,0);*/
            // Asynchronously load the image and set the thumbnail and pager view
            // final View view = getLayoutInflater().inflate(R.layout.my_custom_tab,null);
            // _thumbnails.addView(imageView);
            Glide.with(ImageSliderActivity.this).load("" + images.get(position)).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(imageView);
            imageView.setOnClickListener(new OnImageClickListener(position));
            return itemView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((LinearLayout) object);
        }
    }

    private class OnImageClickListener implements View.OnClickListener {
        int _postion;

        // constructor
        OnImageClickListener(int position) {
            this._postion = position;
        }

        @Override
        public void onClick(View v) {
            // on selecting grid view image
            // launch full screen activity
//            ArrayList<String> list = new ArrayList<>(images);
           /* Intent i = new Intent(BoligerDetailsActivity.this, FullScreenViewActivity.class);
            i.putExtra("position", _postion);
            i.putStringArrayListExtra("stock_list",list);
            startActivity(i);*/
        }
    }

    class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
        private final List<String> horizontalList;

        class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imgCards;
            MyViewHolder(View view) {
                super(view);
                imgCards = view.findViewById(R.id.imgDisplay);
            }
        }

        HorizontalAdapter(List<String> horizontalList) {
            this.horizontalList = horizontalList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_thumb_image, parent, false);

            /*LinearLayout l2 = (LinearLayout) itemView.findViewById(R.id.mainLin);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth-90, ViewGroup.LayoutParams.WRAP_CONTENT );
            //  layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(8,4,0,0);
            l2.setLayoutParams(layoutParams);*/
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            Glide.with(ImageSliderActivity.this).load("" + horizontalList.get(position)).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(holder.imgCards);
            holder.imgCards.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pager.setCurrentItem(position);
                }
            });
        }

        @Override
        public int getItemCount() {
            return horizontalList.size();
        }
    }

}
