package ir.delta.delta.showAdvertising;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.airbnb.lottie.LottieAnimationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;

import static android.view.View.VISIBLE;

public class ShowAdvertisingActivity extends BaseActivity {


    @BindView(R.id.lottieDevelop)
    LottieAnimationView lottieDevelop;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;

    public ShowAdvertisingActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_show_advertising);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.show_adss);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);
        Toast.makeText(this, "این امکان به زودی اضافه خواهد شد.", Toast.LENGTH_SHORT).show();

        lottieDevelop.setAnimation("develop.json");
        lottieDevelop.playAnimation();
        lottieDevelop.loop(true);
    }


    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }
}
