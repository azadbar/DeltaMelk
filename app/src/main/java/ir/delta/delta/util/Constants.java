package ir.delta.delta.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ir.delta.delta.Model.ContractObjectList;
import ir.delta.delta.Model.Language;
import ir.delta.delta.R;
import ir.delta.delta.customView.CustomDrawable;
import ir.delta.delta.database.TransactionAdsCity;
import ir.delta.delta.database.TransactionCampaignUser;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.database.TransactionContractObject;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.service.ResponseModel.Campaign.CampaignUser;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.profile.User;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

import static java.lang.Math.log10;

public class Constants {


    public static final int REQUEST_REFRESH_DELTA_NET = 599;
    public static Language language;
    private static ContractObjectList contractObjects;
    private static City currentCity;
    private static City currentAdsCity;
    private static User currentUser;
    private static CampaignUser currentCampaignUser;

    public static String URL_API = "https://api.delta.ir/api/";
    //        public static String URL_API = "http://172.16.25.116:2323/api/";
    public static String IMAGE_URL_ADS = "https://api.delta.ir/";
    public static String Download_Base_Url = "https://delta.ir/";
    public static String URL_BASE_STATIC = "https://static.delta.ir";
    //    public static String URL_API_MAG = "http://169.254.19.133:8888/wordpress/wp-json/deltapress/v2/";
//    public static String URL_API_MAG = "http://172.16.25.110:8888/wordpress/wp-json/deltapress/";
    public static String URL_API_MAG = "https://deltapayam.com/wp-json/deltapress/";
    public static String URL_SITE = "https://deltapayam.com/";
    public static final int REQUEST_AREA_CODE = 100;
    public static final int REQUEST_REGION_CODE = 101;
    public static final int REQUEST_PROPERTYTYPE_CODE = 102;
    public static final int REQUEST_ESTATE_STATUS = 103;
    public static final int REQUEST_CODE_SEECTED_IMAGE_GALLERY = 600;
    public static final int REQUEST_IMAGE_CAPTURE = 601;
    public static final int RESULT_LOAD_IMAGE = 602;
    public static final int FILTER_DEMAND_LIST = 620;
    public static final int FILTER_ESTATE_STATUS_LIST = 621;
    public static final int LOGIN_LIKE = 700;
    public static final int DATABASE_VERSION = 28;//TODO check version database
    public static final float imageRatio = 5 / 4.0f;
    final public static String FCM = "firebase_service";
    final public static String FCM_Registered = "firebase_service_registered";
    final public static String FCM_ACTION_NEW_NOTIFICATION = "NEW_NOTIFICATION";
    final public static String FCM_ACTION_CLICK_NOTIFICATION = "CLICK_NOTIFICATION";
    public static String isChangePassword = "isChangePassword";
    public static final String isChangeMobileNumber = "CHANGE_MOBILE_NUMBER";
    public static String isRetryCode = "IS_RETRY_CODE";
    public static final String SEND_VERIFICATION_TIME = "SEND_VERIFICATION_TIME";

    public static Language getLanguage() {
        if (language == null) {
            return new Language("فارسی", "fa", DirectionEnum.RTL);
        }
        return language;
    }

    public static City getCity(Context context) {
        if (currentCity != null) {
            return currentCity;
        }
        int cityId = PreferencesData.getCityId(context);
        currentCity = TransactionCity.getInstance().getCityById(context, cityId);
        return currentCity;
    }

    public static void setCurrentCity(City currentCity) {
        Constants.currentCity = currentCity;
    }

    public static City getAdsCity(Context context) {
        if (currentAdsCity != null) {
            return currentAdsCity;
        }
        int cityId = PreferencesData.getCityId(context);
        currentAdsCity = TransactionAdsCity.getInstance().getCityById(context, cityId);
        return currentAdsCity;
    }

    public static void setCurrentCityAds(City currentCity) {
        Constants.currentAdsCity = currentCity;
    }

    public static User getUser(Context context) {
        if (currentUser != null) {
            return currentUser;
        }
        currentUser = TransactionUser.getInstance().getLastUser(context);
        return currentUser;
    }

    public static void setCurrentCampaignUser(CampaignUser campaignUser) {
        Constants.currentCampaignUser = campaignUser;
    }


    public static CampaignUser getCampaignUser(Context context) {

        if (currentCampaignUser != null) {
            return currentCampaignUser;
        }
        currentCampaignUser = TransactionCampaignUser.getInstance().getLastCampaignUser(context);
        return currentCampaignUser;
    }

    public static void setCurrentUser(User currentUser) {
        Constants.currentUser = currentUser;
    }

    public static ContractObjectList getContractObjects(Context context) {
        if (contractObjects != null) {
            return contractObjects;
        }
        contractObjects = TransactionContractObject.getInstance().getContractObject(context);
        return contractObjects;
    }

    public static void setContractObjects(ContractObjectList contractObjects) {
        Constants.contractObjects = contractObjects;
    }

    public static Point getScreenSize(WindowManager windowManager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return new Point(width, height);
    }

    public static Point getScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return new Point(width, height);
    }


    public static String convertNumberToDecimal(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###.###");
        return formatter.format(d);
    }


    public static void shareTextUrl(Context context, String text) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(share, "این مطلب را به اشتراک بگذارید"));
    }

    public static void setBackgroundProgress(Context context, ProgressBar progressBar) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawableProgress = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(context, R.color.redColor));
            progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));

        } else {
            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context, R.color.redColor), PorterDuff.Mode.SRC_IN);
        }
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static void showKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }
    }

    public static String priceConvertor(long number, Resources res) {

        double num = number / 10.0;
        if (num <= 0) {
            return "";
        }
        int digitCount = (int) Math.floor(log10(num) + 1);
        if (digitCount <= 6) {
            return Constants.convertNumberToDecimal(num) + " " + res.getString(R.string.toman);
        } else if (digitCount < 10) {
            double result = num / Math.pow(10, 6);
            return Constants.convertNumberToDecimal(result) + " " + res.getString(R.string.million) + " " + res.getString(R.string.toman);
        }
        double result = num / Math.pow(10, 9);
        return Constants.convertNumberToDecimal(result) + " " + res.getString(R.string.billion) + " " + res.getString(R.string.toman);
    }

    public static int parseColor(String color, int defaultColor) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            return defaultColor;
        }
    }

    public static String convertToEnglishDigits(String value) {
        return value.replace("١", "1").replace("٢", "2").replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8").replace("٩", "9").replace("٠", "0")
                .replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5")
                .replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9").replace("۰", "0");
    }

    public static String convertToPersianDigits(String value) {
        return value.replace("1", "١").replace("2", "٢").replace("3", "٣").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "٧").replace("8", "٨").replace("9", "٩").replace("0", "٠");
    }

    public static float getHeightFactr(String width, String height) {
        if (!TextUtils.isEmpty(width) && !TextUtils.isEmpty(height)) {
            float w = extractInt(width);
            int h = extractInt(height);
            if (w > 0 && h > 0) {
                return h / w;
            }
        }

        return 0.7f;
    }


    private static int extractInt(String text) {
        return Integer.parseInt(text.replaceAll("[^0-9]", ""));

    }

    public static boolean isPackageInstalled(PackageManager packageManager) {
        try {
            packageManager.getPackageInfo("com.farsitel.bazaar", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String convertImageFileToBase64(Bitmap bitmap) {

        try {
            byte[] bitmapData = convertBitmapToByte(bitmap);
            return android.util.Base64.encodeToString(bitmapData, Base64.DEFAULT);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] convertBitmapToByte(Bitmap bitmap) {
        bitmap = scaleDown(bitmap);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int quality = 100;
        try {
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            while (bos.toByteArray().length / 1024.0 > 800 && quality > 30) {
                quality -= 10;
                bos.reset();
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            }
        } catch (OutOfMemoryError error) {
            bos.reset();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bos);
            while (bos.toByteArray().length / 1024.0 > 700) {
                quality -= 5;
                bos.reset();
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
            }
        }
        return bos.toByteArray();
    }


    private static Bitmap scaleDown(Bitmap bitmap) {
        float maxImageSize = 800f;
        if ((bitmap.getWidth() > maxImageSize || bitmap.getHeight() > maxImageSize)) {
            float ratio = Math.min(maxImageSize / bitmap.getWidth(), maxImageSize / bitmap.getHeight());
            int width = Math.round(ratio * bitmap.getWidth());
            int height = Math.round(ratio * bitmap.getHeight());
            return Bitmap.createScaledBitmap(bitmap, width, height, true);
        }
        return bitmap;

    }

    public static void setRippleView(Context context, View view, int height, int pStartColor, int pCenterColor, int pEndColor, int radius) {
        CustomDrawable drawable = new CustomDrawable(
                ContextCompat.getColor(context, pStartColor),
                ContextCompat.getColor(context, pCenterColor),
                ContextCompat.getColor(context, pEndColor),
                1,
                Color.BLACK,
                radius);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setBackground(drawable.getRipple(context, drawable, radius));
            view.setBackground(drawable.getRipple(context, drawable, radius));
        } else {
            view.setBackground(drawable);
            view.setBackground(drawable);
        }
    }

    public static void setToastFont(Context context, String text, int size) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile(FaNum)_Medium.ttf");
        SpannableString efr = new SpannableString(text);
        efr.setSpan(new TypefaceSpan(font), 0, efr.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        Toast toast = Toast.makeText(context, efr, Toast.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(size);
        toast.show();
    }


    public static void sharePostLink(Context context, int id, String link, String title, String termName) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);

        String share;
        if (!TextUtils.isEmpty(title)) {
            share = title;
        } else {
            share = "";
        }
        if (!TextUtils.isEmpty(link)) {
            if (link.contains("%")) {
                Constants.shareTextUrl(context, "مجله دلتا:" + "\n"
                        + share + "\n" + link.substring(0, link.indexOf(("%")) - 1)
                        + "\n" + PreferencesData.getLinDownload(context));
            } else {
                Constants.shareTextUrl(context, "مجله دلتا:" + "\n" +
                        share + "\n" + link
                        + "\n" + PreferencesData.getLinDownload(context));
            }
        } else {
            Constants.shareTextUrl(context, "مجله دلتا:" + "\n" +
                    share + "\n" + Constants.URL_SITE + id
                    + PreferencesData.getLinDownload(context));
        }

        Bundle params = new Bundle();
        params.putString("share_mag", String.valueOf(termName));
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, params);
    }


}
