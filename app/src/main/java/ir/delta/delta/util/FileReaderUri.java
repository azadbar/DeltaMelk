package ir.delta.delta.util;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;

public class FileReaderUri {

    public static ArrayList<Uri> getImagePaths( final Intent data) {
        ArrayList<Uri> list = new ArrayList<>();
        if (data.getData() != null) {
            list.add(data.getData());

        } else if (data.getClipData() != null) {
            ClipData mClipData = data.getClipData();
            for (int i = 0; i < mClipData.getItemCount(); i++) {
                Uri uri = mClipData.getItemAt(i).getUri();
//                String path = getPath(context, data.getData());
                list.add(uri);

            }
        }
        return list;
    }

}
