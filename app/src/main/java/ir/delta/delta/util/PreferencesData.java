package ir.delta.delta.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.UUID;

import ir.delta.delta.R;

public class PreferencesData {

    public static void setSearchTextAnimationTime(Context context, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("SearchTextAnimation", value).apply();
    }

    public static int getSearchTextAnimationTime(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("SearchTextAnimation", 0);
    }

    public static void saveCityId(Context context, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("cityId", value).apply();
    }

    public static int getCityId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("cityId", 0);
    }

    public static void saveToSharedPreferences(Context context, int depositRegisterPrice, int demandRegisterPrice, int depositRegisterImageCount) {
        if (depositRegisterPrice <= 0) {
            depositRegisterPrice = 20000;
        }
        if (demandRegisterPrice <= 0) {
            demandRegisterPrice = 50000;
        }
        if (depositRegisterImageCount < 0 || depositRegisterImageCount > 30) {
            depositRegisterImageCount = 8;
        }
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt("depositRegisterPrice", depositRegisterPrice);
        editor.putInt("demandRegisterPrice", demandRegisterPrice);
        editor.putInt("depositRegisterImageCount", depositRegisterImageCount);
        editor.apply();
    }

    public static int getDepositRegisterPrice(Context context) {
        int price = PreferenceManager.getDefaultSharedPreferences(context).getInt("depositRegisterPrice", 20000);
        if (price > 0) {
            return price;
        }
        return 20000;
    }

    public static int getDemandRegisterPrice(Context context) {
        int price = PreferenceManager.getDefaultSharedPreferences(context).getInt("demandRegisterPrice", 50000);
        if (price > 0) {
            return price;
        }
        return 50000;
    }

    public static int getMaxImageCount(Context context) {
        int imageCount = PreferenceManager.getDefaultSharedPreferences(context).getInt("depositRegisterImageCount", 8);
        if (imageCount > 0 && imageCount <= 30) {
            return imageCount;
        }
        return 8;
    }


    public static boolean isFilterTableUpdate(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("filterTableUpdate", false);
    }

    public static void setFilterTableUpdate(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("filterTableUpdate", true).apply();
    }

    public static void saveIsOnBoarding(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("isOnBoarding", value).apply();
    }


    public static void setFirstSearchOpened(Context context, String isFirstSearchOpened, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isFirstSearchOpened, value).apply();
    }

    public static boolean isFirstSearchOpened(Context context, String isFirstSearchOpened) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isFirstSearchOpened, true);
    }


    public static void setChangeCity(Context context, String isChangeCity, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isChangeCity, value).apply();
    }

    public static boolean isChangeCity(Context context, String isChangeCity) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isChangeCity, true);
    }

    public static void setClear(Context context, String isClear, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isClear, value).apply();
    }

    public static boolean isClear(Context context, String isClear) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isClear, true);
    }

    public static void setMagFragmentOpend(Context context, String isClear, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isClear, value).apply();
    }

    public static boolean isMagFragmentOpend(Context context, String isClear) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isClear, true);
    }

    public static boolean isBottomBarOpened(Context context, String isBottomBar) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isBottomBar, true);
    }

    public static void setBottomBarOpend(Context context, String isBottomBar, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isBottomBar, value).apply();
    }

    public static boolean isSearchMagOpend(Context context, String isSearchMag) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(isSearchMag, true);
    }

    public static void setSearchMagOpend(Context context, String isSearchMag, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(isSearchMag, value).apply();
    }


    public static void setOrderPriceStair(Context context, String orderStairPrice) {
        if (TextUtils.equals(orderStairPrice, "0")) {
            orderStairPrice = "20000";
        }

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString("orderStairPrice", orderStairPrice);
        editor.apply();
    }

    public static String getOrderPriceStair(Context context) {
        String price = PreferenceManager.getDefaultSharedPreferences(context).getString("orderStairPrice", "20000");
        if (!TextUtils.equals(price, "0")) {
            return price;
        }
        return "20000";
    }

    public static void saveToken(Context context, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("GUID", value).apply();
    }

    public static String getToken(Context context) {
        if (TextUtils.isEmpty(PreferenceManager.getDefaultSharedPreferences(context).getString("GUID", ""))) {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putString("GUID", UUID.randomUUID().toString()).apply();
            return PreferenceManager.getDefaultSharedPreferences(context).getString("GUID", "");
        } else {
            return PreferenceManager.getDefaultSharedPreferences(context).getString("GUID", "");
        }
    }


    public static void saveLinDownload(Context context, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("downloadLink", "\n\n"+context.getResources().getString(R.string.share_text) + "\n\n" + value).apply();
    }

    public static String getLinDownload(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("downloadLink", "");
    }

    public static void setMobileNumber(Context context, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("mobileCampaign", value).apply();
    }

    public static String getMobileNumber(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("mobileCampaign", "");
    }

    public static void setNameCampaign(Context context, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("nameCampaign", value).apply();
    }
    public static String getNameCampaign(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("nameCampaign", "");
    }

    public static void setlongParam(Context context, String key, long value) {
        final SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static long getLongParam(Context context, String key, long def) {
        final SharedPreferences settings = context.getSharedPreferences("UserInfo", 0);
        return settings.getLong(key, def);

    }


    public static void setRegionAdsText(Context context, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString("regionAds", value).apply();
    }
    public static String getRegionAdsText(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString("regionAds", "");
    }

    public static void setRegionAdsId(Context context, int value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt("regionAdsId", value).apply();
    }

    public static int getRegionAdsId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt("regionAdsId", 0);
    }

    public static boolean isShowRequirementBtnShowInMenu(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("isShowRequirementBtnShowInMenu", false);
    }

    public static void setShowRequirementBtnShowInMenu(Context context, boolean value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean("isShowRequirementBtnShowInMenu", value).apply();
    }

}
