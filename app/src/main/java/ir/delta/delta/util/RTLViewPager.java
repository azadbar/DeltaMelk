package ir.delta.delta.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class RTLViewPager extends ViewPager {

    private boolean swipeable = true;
    // variables
    private boolean mIsRtlOriented;

    // constructors
    public RTLViewPager(Context context){
        this(context, null);
    }

    public RTLViewPager(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    /**
     * To use this method call first android.support.v4.view.ViewPager#setAdapter(android.support.v4.view.PagerAdapter)
     */
    public void setRtlOriented(boolean isRtlOriented){
        mIsRtlOriented = isRtlOriented;
        if(mIsRtlOriented && getAdapter() != null){
            setCurrentItem(getAdapter().getCount() - 1);
        }else{
            setCurrentItem(0);
        }
    }

    // Call this method in your motion events when you want to disable or enable
    // It should work as desired.
    public void setSwipeable(boolean swipeable) {
        this.swipeable = swipeable;
    }

    public boolean isSwipeable() {
        return swipeable;
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent arg0) {
        return (this.swipeable) ? super.onInterceptTouchEvent(arg0) : false;
    }

}