package ir.delta.delta.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import java.util.ArrayList;

public class ImageUploader {

    public interface OnImageUploader {
        void onSuccess(String finalImageNames);

        void onFailure(String error);
    }

    private String pathImageName;
    private ArrayList<Uri> uriList;
    private final OnImageUploader listener;
    private final Context context;
    private final Gson gson = new Gson();
    private ArrayList<String> imageNames;

    public ImageUploader(Context context, OnImageUploader listener) {
        this.context = context;
        this.listener = listener;
    }

    public void start(ArrayList<Uri> list, String pathImageName) {
        this.pathImageName = pathImageName;
        this.uriList = list;
        this.imageNames = new ArrayList<>();
        if (uriList == null || uriList.size() == 0) {
            listener.onFailure("list is Empty");
            return;
        }
        loadImage(0);
    }

    private void loadImage(int index) {
        Glide.with(context).asBitmap().load(String.valueOf(uriList.get(index))).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                String base64 = null;
                if (resource != null) {
                    base64 = Constants.convertImageFileToBase64(resource);
                }
                if (base64 != null) {

                } else {
                    listener.onFailure("base64 convert error in index: " + index);
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }

        });
    }

}
