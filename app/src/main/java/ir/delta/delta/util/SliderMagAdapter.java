package ir.delta.delta.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class SliderMagAdapter extends
        SliderViewAdapter<SliderMagAdapter.SliderAdapterVH> {

    private final Context context;
    private final ArrayList<MagPost> list;
    private final onItemClickSlider listener;
    private final ArrayList<Integer> readPost;

    public SliderMagAdapter(Context context, ArrayList<MagPost> list, onItemClickSlider lisener, ArrayList<Integer> readPost) {
        this.context = context;
        this.list = list;
        this.listener = lisener;
        this.readPost = readPost;
    }


    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {


        MagPost magPost = list.get(position);

        Glide.with(viewHolder.itemView.getContext())
                .load(magPost.getImage())
                .fitCenter()
                .addListener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                viewHolder.progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                viewHolder.progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(viewHolder.iv_auto_image_slider);
        viewHolder.textViewDescription.setText(magPost.getTitle());


        viewHolder.itemView.setOnClickListener(v -> {
//            readPost.add(list.get(position).getId());
//            TransactionReadPost.getInstance().saveMagPost(context, list.get(position).getId());
//            Intent intent = new Intent(context, PostContentActivity.class);
//            intent.putExtra("magPosts", list);
//            intent.putExtra("termId", magPost.getTermId());
//            intent.putExtra("selectedIndex", position);
////        list.get(position).setOpen(true);
//            context.startActivity(intent);
            viewHolder.bind(position, list);
        });


    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return list.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        TextView textViewDescription;
        ImageView iv_auto_image_slider;
        ProgressBar progressBar;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider);
            iv_auto_image_slider = itemView.findViewById(R.id.iv_auto_image_slider);
            progressBar = itemView.findViewById(R.id.progress);
            this.itemView = itemView;
        }

        public void bind(int position, ArrayList<MagPost> list) {
            listener.onItemClick(position,list);
        }
    }

    public interface onItemClickSlider {
        void onItemClick(int position, ArrayList<MagPost> list);
    }

}
