package ir.delta.delta.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.HashMap;

import ir.delta.delta.baseView.BaseImageView;

public class DownloadImageThumbnail extends AsyncTask<String, Void, Bitmap> {
    private final ProgressBar progressBar;
    private final BaseImageView play;
    ImageView bmImage;
    Activity activity;


    public DownloadImageThumbnail(ImageView bmImage, ProgressBar progressBar, BaseImageView play) {
        this.bmImage = bmImage;
        this.progressBar = progressBar;
        this.play = play;
    }

    protected Bitmap doInBackground(String... urls) {
        Bitmap myBitmap = null;
        MediaMetadataRetriever mMRetriever = null;
        try {
            progressBar.setVisibility(View.VISIBLE);
            play.setVisibility(View.GONE);
            bmImage.setVisibility(View.GONE);
            mMRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mMRetriever.setDataSource(urls[0], new HashMap<String, String>());
            else
                mMRetriever.setDataSource(urls[0]);
            myBitmap = mMRetriever.getFrameAtTime();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mMRetriever != null) {
                mMRetriever.release();
            }
        }

        return myBitmap;
    }

    protected void onPostExecute(Bitmap result) {
        progressBar.setVisibility(View.GONE);
        play.setVisibility(View.VISIBLE);
        bmImage.setVisibility(View.VISIBLE);
        bmImage.setImageBitmap(result);


    }
}