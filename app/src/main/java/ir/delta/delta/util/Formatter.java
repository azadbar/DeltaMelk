package ir.delta.delta.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import ir.delta.delta.Model.Language;
import ir.delta.delta.enums.DirectionEnum;

public class Formatter {

    private static Formatter sFormatter;
    private final DecimalFormat df;
    private final boolean hasFractionalPart;
    private final DecimalFormat dfnd;

    public Formatter() {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Constants.getLanguage().getLocale());
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        df = new DecimalFormat("#,###.##", otherSymbols);
        df.setDecimalSeparatorAlwaysShown(true);
        dfnd = new DecimalFormat("#,###",otherSymbols);
        hasFractionalPart = false;
    }


    public static DecimalFormat getInstance() {
        if (sFormatter == null) {
            sFormatter = new Formatter();
        }
        return sFormatter.df;
    }

    public DecimalFormat df() {
        return df;
    }

    public DecimalFormat dfnf() {
        return dfnd;
    }

    public boolean isHasFractionalPart() {
        return hasFractionalPart;
    }
}
