package ir.delta.delta.enums;

import android.content.res.Resources;

public enum MarketEnum {

    BAZAR(0,"bazar"),
    GOOGLE(1,"google");

    private final int methodCode;
    private final String methodString;


    MarketEnum(int methodCode, String methodString) {
        this.methodCode = methodCode;
        this.methodString = methodString;
    }

    public int getMethodCode() {
        return methodCode;
    }

    public String getMethodString() {
        return methodString;
    }
}
