package ir.delta.delta.enums;

import android.text.TextUtils;

import java.io.Serializable;

public enum PostRowTypeEnum implements Serializable {

    header(1),
    htmlTag(2),
    img(3),
    video(4),
    tablePress(5),
    gallery(6),
    script(7),

    author(8),
    relatedPostTitle(9),
    relatedPost(10),
    commentHeader(11),
    comment(12),
    otherCommentButton(13),
    addCommentButton(14);

    public static PostRowTypeEnum getHtmlTag(String tagName) {
        if (TextUtils.isEmpty(tagName)) {
            return htmlTag;
        }
        switch (tagName) {
            case "img":
                return img;
            case "video":
                return video;
            case "tablePress":
                return tablePress;
            case "gallery":
                return gallery;
            case "script":
                return script;
            default:
                return htmlTag;
        }
    }

    private final int methodCode;

    PostRowTypeEnum(int methodCode) {
        this.methodCode = methodCode;
    }

    public int getMethodCode() {
        return methodCode;
    }

}
