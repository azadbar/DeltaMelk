package ir.delta.delta.enums;

import android.content.res.Resources;

import ir.delta.delta.R;

public enum EstateStatusEnum {

    NONE(0),
    WIAT_FOR_CONFIRM(1),
    CONFIRMED(2),
    ARCHIVE(3),
    EDITED(7),
    BAYEGANI(14);

    private final int methodCode;

    EstateStatusEnum(int i) {
        this.methodCode = i;
    }

    public int getMethodCode() {
        return methodCode;
    }

    public int getBackColor(Resources res) {
        switch (this) {
            case CONFIRMED:
                return res.getColor(R.color.ovalGreen);
            case WIAT_FOR_CONFIRM:
                return res.getColor(R.color.ovalYellow);
            case ARCHIVE:
                return res.getColor(R.color.ovalArchive);
            case EDITED:
                return res.getColor(R.color.light_gray);
            case BAYEGANI:
                return res.getColor(R.color.mainGray);
            default:
                return res.getColor(R.color.redColor);
        }
    }

    public static EstateStatusEnum getEnum(int statusLocalId) {
        switch (statusLocalId) {
            case 2:
                return EstateStatusEnum.CONFIRMED;
            case 1:
                return EstateStatusEnum.WIAT_FOR_CONFIRM;
            case 3:
                return EstateStatusEnum.ARCHIVE;
            case 7:
                return EstateStatusEnum.EDITED;
            case 14:
                return EstateStatusEnum.BAYEGANI;
            default:
                return EstateStatusEnum.NONE;
        }
    }

    public String getText(Resources res) {
        switch (this) {
            case NONE:
                return res.getString(R.string.all_items);
            case CONFIRMED:
                return res.getString(R.string.confirmed);
            case WIAT_FOR_CONFIRM:
                return res.getString(R.string.wait_for_confirmed);
            case ARCHIVE:
                return res.getString(R.string.archive);
            case EDITED:
                return res.getString(R.string.edited);
            case BAYEGANI:
                return res.getString(R.string.baygani_shode);
            default:
                return "";
        }
    }


}
