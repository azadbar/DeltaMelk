package ir.delta.delta.enums;

import java.io.Serializable;

public enum SearchParamType implements Serializable {

    PRICE,
    RENT,
    METER,
    TOTALMETER
}
