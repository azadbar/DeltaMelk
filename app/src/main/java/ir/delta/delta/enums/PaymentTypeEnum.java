package ir.delta.delta.enums;

public enum PaymentTypeEnum {

    DEPOSIT_REGISTER(1),
    DEMAND_REGISTER(2),
    STAIRS_REGISTER(3);
    private final int methodCode;

    PaymentTypeEnum(int methodCode) {
        this.methodCode = methodCode;
    }

    public int getMethodCode() {
        return methodCode;
    }
}
