package ir.delta.delta.enums;

public enum TypeCommunityAdsEnum {
    isVideo(0),
    isTelegram(1),
    isInstagram(2),
    isWebsite(3),
    isPhone1(4),
    isPhone2(5),
    isPhone3(6),
    isMobile(7);

    private final int methodCode;

    TypeCommunityAdsEnum(int methodCode) {
        this.methodCode = methodCode;
    }

    public int getMethodCode() {
        return methodCode;
    }
}
