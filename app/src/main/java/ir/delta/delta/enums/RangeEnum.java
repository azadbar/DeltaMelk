package ir.delta.delta.enums;

public enum RangeEnum {

    FIRST,
    MIDDLE,
    END;

    public static RangeEnum getEnum(int index, int listSize) {
        if (index == 0) {
            return FIRST;
        }
        if (index == listSize - 1) {
            return END;
        }
        return MIDDLE;
    }
}
