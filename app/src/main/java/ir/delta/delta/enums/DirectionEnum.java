package ir.delta.delta.enums;

public enum DirectionEnum {

    RTL("راست به چپ", 1),
    LTR("چپ به راست", 2);

    private final String methodName;
    private final int methodCode;

    DirectionEnum(String methodName, int methodCode) {
        this.methodName = methodName;
        this.methodCode = methodCode;
    }

    public String getMethodName() {
        return methodName;
    }

    public int getMethodCode() {
        return methodCode;
    }
}
