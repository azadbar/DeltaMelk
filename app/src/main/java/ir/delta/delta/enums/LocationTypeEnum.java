package ir.delta.delta.enums;

public enum LocationTypeEnum {
    AREA,
    REGION,
    PROPERTYTYPE,
    ESTATE_STATUS
}
