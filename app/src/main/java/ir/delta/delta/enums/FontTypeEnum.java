package ir.delta.delta.enums;

public enum FontTypeEnum {
    REGULAR(1),
    Light(2),
    MEDIUM(3),
    BOLD(4);

    private final int methodCode;

    FontTypeEnum(int i) {
        this.methodCode = i;
    }

    public int getMethodCode() {
        return methodCode;
    }


}
