package ir.delta.delta.enums;

public enum PropertyTypeEnum {

    Apartment(1),//آپارتمان مسکونی
    ApartmentOffice(2),//آپارتمان-اداری
    ApartmentBoth(3),//آپارتمان-موقعیت-اداری
    Villa(4),//خانه-ویلا
    KaneVilaKolangi(5),//خانه-ویلا-کلنگی
    MojtamaApatmaniMostaghelat(6),//مجتمع-آپارتمانی-مستغلات
    Ground(7),//زمین
    Shop(8),//تجاری-مغازه
    Factory(9),//کارگاه-کارخانه-سوله
    Livestock(10),//دامداری-دامپروری
    Cultivation(11),//کشت-صنعت
    //    Industrial(12),//املاک-صنعتی-زراعی
    Garden(13);//باغ-باغچه
//    Others(14); //سایر موارد صنعتی

    public static PropertyTypeEnum propertyTypeEnum(int propertyTypeLocalId) {
        switch (propertyTypeLocalId) {
            case 1:
                return Apartment;
            case 2:
                return ApartmentOffice;
            case 3:
                return ApartmentBoth;
            case 4:
                return Villa;
            case 5:
                return KaneVilaKolangi;
            case 6:
                return MojtamaApatmaniMostaghelat;
            case 7:
                return Ground;
            case 8:
                return Shop;
            case 9:
                return Factory;
            case 10:
                return Livestock;
            case 11:
                return Cultivation;
//            case 12:
//                return Industrial;
            case 13:
                return Garden;
//            case 14:
//                return Others;
        }
        return Apartment;
    }


    private final int methodCode;

    PropertyTypeEnum(int i) {
        this.methodCode = i;
    }


    public int getMethodCode() {
        return methodCode;
    }



}
