package ir.delta.delta.enums;

import java.io.Serializable;

public enum HtmlTagEnum implements Serializable {

    //HEADER, FOOTER,HR

    div(1),
    p(2),
    h1(3),
    h2(4),
    h3(5),
    h4(6),
    h5(7),
    h6(8),

    a(9),
    img(10),
    script(11),

    hr(12),
    other(13),

    table(14),
    gallery(15),
    strong(16),

    relatedHeader(17),
    related(18),
    comment(19),
    author(20),
    video(21);


    private final int methodCode;

    HtmlTagEnum(int methodCode) {
        this.methodCode = methodCode;
    }

    public int getMethodCode() {
        return methodCode;
    }


    public static HtmlTagEnum getHtmlTag(String tagName) {
        switch (tagName) {
            case "div":
                return div;
            case "h1":
                return h1;
            case "h2":
                return h2;
            case "h3":
                return h3;
            case "h4":
                return h4;
            case "h5":
                return h5;
            case "h6":
                return h6;
            case "a":
                return a;
            case "img":
                return img;
            case "script":
                return script;
            case "hr":
                return hr;
            case "p":
                return p;
            case "gallery":
                return gallery;
            case "strong":
                return strong;
            case "relatedHeader":
                return relatedHeader;
            case "related":
                return related;
            case "comment":
                return comment;
            case "author":
                return author;
            case "video":
                return video;
            default:
                return other;
        }
    }


    public boolean isTextOrHr() {
        switch (this) {
            case img:
            case script:
            case table:
            case gallery:
            case comment:
            case author:
            case related:
            case relatedHeader:
            case video:
                return false;
            default:
                return true;
        }
    }


}
