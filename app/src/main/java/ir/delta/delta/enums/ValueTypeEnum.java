package ir.delta.delta.enums;

public enum ValueTypeEnum {
    INT(0),
    LONG(1),
    STRING(2);

    private final int methodCode;

    ValueTypeEnum(int methodCode) {
        this.methodCode = methodCode;
    }

    public int getMethodCode() {
        return methodCode;
    }
}
