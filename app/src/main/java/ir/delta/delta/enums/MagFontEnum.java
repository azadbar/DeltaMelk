package ir.delta.delta.enums;

import android.content.res.Resources;
import android.graphics.Typeface;

import java.io.Serializable;

import ir.delta.delta.R;

public enum MagFontEnum implements Serializable {

    SMALL,
    NORMAL,
    LARGE,
    XLARGE;

    public int getSize(Resources res, HtmlTagEnum htmlTag) {
        switch (htmlTag) {
            case p:
            case div:
                return res.getDimensionPixelSize(R.dimen.text_size_1) + getFontSize(res);

            case h1:
            case h2:
                return res.getDimensionPixelSize(R.dimen.text_size_h1) + getFontSize(res);

            case h3:
            case h4:
            case relatedHeader:
                return res.getDimensionPixelSize(R.dimen.text_size_h2) + getFontSize(res);

            case h5:
            case h6:
                return res.getDimensionPixelSize(R.dimen.text_size_h3) + getFontSize(res);

            default:
                return res.getDimensionPixelSize(R.dimen.text_size_2) + getFontSize(res);
        }
    }


    public Typeface getTypeFace(HtmlTagEnum htmlTag, Typeface boldFont, Typeface mediumFont, Typeface regularFont) {
        switch (htmlTag) {
            case h1:
            case h2:
            case h3:
            case h4:
            case h5:
            case h6:
            case relatedHeader:
                return boldFont;
//            case p:
//            case div:
//            case a:
//                return mediumFont;//TODO medium
            case strong:
                return boldFont;

            default:
                return mediumFont;
        }
    }

    public MagFontEnum decrease() {
        switch (this) {

            case LARGE:
                return NORMAL;
            case XLARGE:
                return LARGE;
            default:
                return SMALL;
        }
    }

    public MagFontEnum increase() {
        switch (this) {
            case SMALL:
                return NORMAL;
            case NORMAL:
                return LARGE;
            default:
                return XLARGE;
        }
    }

    private int getFontSize(Resources res) {
        switch (this) {
            case NORMAL:
                return res.getDimensionPixelSize(R.dimen.normal);
            case LARGE:
                return res.getDimensionPixelSize(R.dimen.large);
            case XLARGE:
                return res.getDimensionPixelSize(R.dimen.xlarge);
            default:
                return 0;
        }
    }


}
