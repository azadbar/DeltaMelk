package ir.delta.delta.enums;

import android.content.res.Resources;

import ir.delta.delta.R;

public enum ContactDeltaTypeEnum {

    PHONE(0),
    EMAIL(1),
    WATS_APP(2),
    INSTAGRAM(3),
    LINKEDEN(4),
    APARAT(5),
    WEB(6);


    private final int methodCode;

    ContactDeltaTypeEnum(int i) {
        this.methodCode = i;
    }

    public int getMethodCode() {
        return methodCode;
    }





}
