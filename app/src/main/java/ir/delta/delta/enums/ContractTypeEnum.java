package ir.delta.delta.enums;

import android.content.res.Resources;

import ir.delta.delta.R;

public enum ContractTypeEnum {

    PURCHASE(1),
    RENT(2);


    private final int methodCode;

    ContractTypeEnum(int i) {
        this.methodCode = i;
    }

    public int getMethodCode() {
        return methodCode;
    }


    public static ContractTypeEnum getContractType(int id) {
        switch (id) {
            case 2:
                return RENT;
            default:
                return PURCHASE;
        }
    }

    public String getTitle(Resources res) {
        switch (this) {
            case RENT:
                return res.getString(R.string.rent_text);
            default:
                return res.getString(R.string.buy_text);
        }
    }

}
