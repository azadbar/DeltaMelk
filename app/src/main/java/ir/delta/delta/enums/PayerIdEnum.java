package ir.delta.delta.enums;

public enum PayerIdEnum {
    online(0),
    cafe_bazar(1);

    PayerIdEnum(int code) {
        methodCode = code;
    }

    int methodCode;

    public int getMethodCode() {
        return methodCode;
    }

    public void setMethodCode(int methodCode) {
        this.methodCode = methodCode;
    }
}
