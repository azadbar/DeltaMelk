package ir.delta.delta.enums;

import java.io.Serializable;

public enum LoginState implements Serializable {

    inputMobile,
    loginUser,
    consultantLogin,
    forgetPassword


}
