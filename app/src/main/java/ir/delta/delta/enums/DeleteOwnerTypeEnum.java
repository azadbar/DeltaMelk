package ir.delta.delta.enums;

import android.content.res.Resources;

import ir.delta.delta.R;

public enum DeleteOwnerTypeEnum {

    DeleteSaled(1),
    DeleteMortgage(2),
    DeleteSiteNotGood(3),
    DeleteOwnerGiveUp(4),
    DeleteOthers(5),
    DeleteRealtorMistake(6),
    DeleteOwnerMistake(7);

    private final int code;

    DeleteOwnerTypeEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getString(Resources res) {
        switch (this) {
            case DeleteSaled:
                return res.getString(R.string.deleteSaled);
            case DeleteMortgage:
                return res.getString(R.string.deleteMortgage);
            case DeleteSiteNotGood:
                return res.getString(R.string.deleteSiteNotGood);
            case DeleteOwnerGiveUp:
                return res.getString(R.string.deleteOwnerGiveUp);
            case DeleteOthers:
                return res.getString(R.string.deleteOthers);
            case DeleteRealtorMistake:
                return res.getString(R.string.deleteRealtorMistake);
            case DeleteOwnerMistake:
                return res.getString(R.string.deleteOwnerMistake);
            default:
                return "";
        }
    }
}
