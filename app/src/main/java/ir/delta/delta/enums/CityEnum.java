package ir.delta.delta.enums;

public enum CityEnum {
    MAINCITY,
    COASTALCITY,
    CITYINSTATE;


    public int getId() {
        switch (this) {
            case COASTALCITY:
                return 1;
            case CITYINSTATE:
                return 2;
        }
        return 0;

    }

    public static CityEnum fromId(int id) {
        switch (id) {
            case 1:
                return COASTALCITY;
            case 2:
                return CITYINSTATE;

        }
        return MAINCITY;
    }

}
