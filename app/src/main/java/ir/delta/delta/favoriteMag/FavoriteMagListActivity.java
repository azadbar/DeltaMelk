package ir.delta.delta.favoriteMag;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.mag.PostAdapter;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.mag.main_page.OnRefreshMainPageList;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

import static android.view.View.VISIBLE;

public class FavoriteMagListActivity extends BaseActivity implements PostAdapter.onItemClick, OnRefreshMainPageList {

    @BindView(R.id.rvFavorite)
    RecyclerView rvFavorite;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;
    private ArrayList<MagPost> favoriteList;
    private PostAdapter postAdapter;
    private LinearLayoutManager layoutManager;

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    public static OnRefreshMainPageList refreshMainPageListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_mag_list);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.contents_shown);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);

        favoriteList = TransactionFavoriteMag.getInstance().getFavoriteMag(this);

        layoutManager = new LinearLayoutManager(getBaseContext());
        rvFavorite.setLayoutManager(layoutManager);
        rvFavorite.setHasFixedSize(true);

        checkEmptyList();
    }

    private void checkEmptyList() {
        if (favoriteList.size() > 0) {
            rvFavorite.setVisibility(View.VISIBLE);
            empty.setVisibility(View.GONE);
            setAdapter();
        } else {
            rvFavorite.setVisibility(View.GONE);
            empty.setVisibility(View.VISIBLE);
        }
    }

    private void setAdapter() {
        if (postAdapter == null) {
            postAdapter = new PostAdapter(favoriteList, new ArrayList<>(), this, TransactionFavoriteMag.getInstance().getReadPostsId(this));
            rvFavorite.setLayoutManager(layoutManager);
            postAdapter.setFavorite(true);
            postAdapter.setRefreshMainPageListener(refreshMainPageListener);
            rvFavorite.setAdapter(postAdapter);
        } else {
            postAdapter.updateReceiptsList(favoriteList);
        }
    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        Intent intent = new Intent(this, PostContentActivity.class);
        PostContentActivity.magPosts = list;//becuse too large list
        Bundle bundle = new Bundle();
        bundle.putString("termId", "0");
        bundle.putInt("selectedIndex", position);
        intent.putExtras(bundle);
        PostContentActivity.refreshMainPageListener = this;
//        PostContentActivity.listener = this;
//        list.get(position).setOpen(true);
//        postAdapter.notifyDataSetChanged();
        startActivity(intent);
    }

    @Override
    public void onItemShare(MagPost object) {
        Constants.sharePostLink(this,object.getId(), object.getLink(), object.getTitle(), object.getTerm_name());
    }

    @OnClick({R.id.rlBack})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.rlBack) {
            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
            finish();
        }

    }

    @Override
    public void onRefresh() {
        favoriteList = TransactionFavoriteMag.getInstance().getFavoriteMag(this);
        checkEmptyList();
    }
}