package ir.delta.delta.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.LoginState;
import ir.delta.delta.service.Request.LoginConsultantService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.LoginConsultantReq;
import ir.delta.delta.service.ResponseModel.profile.ConsultantUserResponse;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;

public class LoginConsultantFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.btnLogin)
    BaseTextView btnLogin;
    @BindView(R.id.btnForgetPassword)
    BaseTextView btnForgetPassword;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private String mobile;


    public LoginConsultantFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_consultant, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();

        Bundle b = getArguments();
        if (b != null) {
            mobile = b.getString("mobile");
        }
        return view;
    }

    private void initView() {
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.password));
        if (getActivity() != null) {
            Window window = getActivity().getWindow();
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null && window != null) {
                Point size = Constants.getScreenSize(windowManager);
                if (size.x > getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile)) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) second.getLayoutParams();
                    layoutParams.width = getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile);
                    second.setLayoutParams(layoutParams);
                }
            }
        }
        if (this.edtPassword.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private boolean isValidPassword() {
        if (edtPassword.getError() != null) {
            Toast.makeText(getActivity(), edtPassword.getError(), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @OnClick({R.id.rlBack, R.id.btnForgetPassword, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((LoginActivity) getActivity()).backPress();
                }
                break;
            case R.id.btnForgetPassword:

                InputMobileFragment inputMobileFragment = new InputMobileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("mobile", mobile);
                bundle.putSerializable("loginState", LoginState.forgetPassword);
                inputMobileFragment.setArguments(bundle);
                loadFragment(inputMobileFragment, InputMobileFragment.class.getName());
                break;
            case R.id.btnLogin:
                if (isValidPassword()) {
                    getLoginConsultant();
                }
                break;
        }
    }

    private void getLoginConsultant() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        LoginConsultantReq req = new LoginConsultantReq();
        req.setUserName(mobile);
        req.setPassword(edtPassword.getValueString());
        LoginConsultantService.getInstance().getLoginConsultant(getResources(), req, new ResponseListener<ConsultantUserResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(ConsultantUserResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    //TODO save user consultant
                    if (response.getSuccessed()) {
                        saveUser(response);
                        if (BuildConfig.isCounsultant) {
                            Intent intent = new Intent(getActivity(), BottomBarActivity.class);
                            intent.putExtra("itemsIndex", 3);
                            getActivity().startActivity(intent);
                            getActivity().finish();
                        } else {
                            Intent intent = new Intent();
                            intent.putExtra("result", response.getLogedInUserInfo().getUserId());
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void saveUser(ConsultantUserResponse response) {
        User user = new User();
        user.setGeneral(false);
        user.setUserId(response.getLogedInUserInfo().getUserId());
        user.setLastName(response.getLogedInUserInfo().getLastname());
        if (!TextUtils.isEmpty(response.getLogedInUserInfo().getAlias())) {
            user.setDisplayName(response.getLogedInUserInfo().getAlias());
        } else {
            user.setDisplayName(response.getLogedInUserInfo().getLastname());
        }
        user.setShowDeltaNetMenu(response.getLogedInUserInfo().isShowDeltaNetMenu());
        user.setMobile(response.getLogedInUserInfo().getMobile());
        user.setActivityLocation(response.getLogedInUserInfo().getActivityLocation());
        user.setOfficeTitle(response.getLogedInUserInfo().getOfficeTitle());
        user.setOfficeLogo(response.getLogedInUserInfo().getOfficeLogo());
        user.setImagePath(response.getLogedInUserInfo().getImagePath());
        user.setLocationId(response.getLogedInUserInfo().getLocationId());
        TransactionUser.getInstance().save(getActivity(), user);
        Constants.setCurrentUser(user);
    }


    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

}
