package ir.delta.delta.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.service.Request.CheckUserCodeConsultantService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.CheckUserConsultantReq;
import ir.delta.delta.service.ResponseModel.profile.CheckUserConsultantResponse;
import ir.delta.delta.util.Constants;

public class VerificationCodeConsultantFragment extends BaseFragment {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.edtSubmitCode)
    CustomEditText edtSubmitCode;
    @BindView(R.id.btnRegisterCode)
    BaseTextView btnRegisterCode;
    Unbinder unbinder;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.tvCounter)
    BaseTextView tvCounter;
    @BindView(R.id.btnResendCode)
    BaseTextView btnResendCode;
    @BindView(R.id.rlCounter)
    BaseRelativeLayout rlCounter;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private final int count = 300000;
    private CountDownTimer timer;
    private String mobile;
    private String userId;


    public VerificationCodeConsultantFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_verification_code_consultant, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        Bundle b = getArguments();
        if (b != null) {
            mobile = b.getString("mobile");
            userId = b.getString("userId");
        }
        return view;
    }

    private void initView() {
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.verify_identity));
        if (getActivity() != null) {
            Window window = getActivity().getWindow();
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null && window != null) {
                Point size = Constants.getScreenSize(windowManager);
                if (size.x > getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile)) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) second.getLayoutParams();
                    layoutParams.width = getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile);
                    second.setLayoutParams(layoutParams);
                }
            }
        }
        startTimer();
        hideResendCode();
    }

    private void stopTimer() {
        rlCounter.setVisibility(View.GONE);
        btnResendCode.setVisibility(View.VISIBLE);
//        timer.cancel();
//        timer = null;

    }

    private void hideResendCode() {
        rlCounter.setVisibility(View.VISIBLE);
        btnResendCode.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rlBack, R.id.btnRegisterCode, R.id.btnResendCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((LoginActivity) getActivity()).backPress();
                }
                break;
            case R.id.btnRegisterCode:
                if (isValidCode()) {
                    getVerificationCode();
                }

                break;
            case R.id.btnResendCode:
                InputMobileFragment inputMobileFragment = new InputMobileFragment();
                inputMobileFragment.text = mobile;
                loadFragment(inputMobileFragment, InputMobileFragment.class.getName());
                hideResendCode();

                break;
        }
    }

    private void getVerificationCode() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        CheckUserConsultantReq req = new CheckUserConsultantReq();
        req.setUserToken(userId);
        req.setActivationCode(edtSubmitCode.getValueString());
        CheckUserCodeConsultantService.getInstance().getCheckUserCode(getResources(), req, new ResponseListener<CheckUserConsultantResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);

                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(CheckUserConsultantResponse response) {
                if (getActivity() == null) {
                    return;
                }
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);

                    if (response.isSuccessed()) {
                        ResetPasswordConsultantFragment resetFragment = new ResetPasswordConsultantFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("userId", userId);
                        bundle.putString("mobile", mobile);
                        resetFragment.setArguments(bundle);
                        loadFragment(resetFragment, ResetPasswordConsultantFragment.class.getName());
                    } else {
                        Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void startTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        timer = new CountDownTimer(count, 1000) {
            public void onTick(long millisUntilFinished) {
                if (getView() != null) {
                    tvCounter.setText("" + millisUntilFinished / 1000);
                    Log.e("Counter: ", "" + millisUntilFinished / 1000);
                }
            }

            public void onFinish() {
                if (getView() != null) {
                    stopTimer();

                }
            }
        }.start();
    }

    private boolean isValidCode() {
        if (edtSubmitCode.getError() != null) {
            Toast.makeText(getActivity(), edtSubmitCode.getError(), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        stopTimer();
//    }
}
