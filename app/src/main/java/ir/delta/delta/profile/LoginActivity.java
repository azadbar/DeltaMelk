package ir.delta.delta.profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;

public class LoginActivity extends BaseActivity {


    private boolean isFirstActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFirstActivity = bundle.getBoolean("isFirstActivity");
        }
        InputMobileFragment inputMobileFragment = new InputMobileFragment();
        Bundle b = new Bundle();
        b.putBoolean("isFirstActivity", isFirstActivity);
        inputMobileFragment.setArguments(bundle);
        loadFragment(inputMobileFragment, InputMobileFragment.class.getName());
    }


    private void loadFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.replace(R.id.frameLayout, fragment, fragmentTag);
        fragTrans.commit();
    }

    @Override
    public void onBackPressed() {
        backPress();
    }

    public void backPress() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
