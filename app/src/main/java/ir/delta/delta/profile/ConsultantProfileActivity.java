package ir.delta.delta.profile;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.changePassword.ChangePasswordActivity;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.service.Request.CheckProfileImageValidService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.UploadAgentImageService;
import ir.delta.delta.service.RequestModel.EmptyReq;
import ir.delta.delta.service.RequestModel.ProfileImageReq;
import ir.delta.delta.service.ResponseModel.CheckProfileImageValidResponse;
import ir.delta.delta.service.ResponseModel.profile.ProfileImageResponse;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PermissionHandler;

public class ConsultantProfileActivity extends BaseActivity {

    @BindView(R.id.ivPerson)
    CircleImageView ivPerson;
    @BindView(R.id.tvName)
    BaseTextView tvName;
    @BindView(R.id.tvPostName)
    BaseTextView tvPostName;
    @BindView(R.id.tvMobile)
    BaseTextView tvMobile;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    @BindView(R.id.btnChangePassword)
    BaseTextView btnChangePassword;
    @BindView(R.id.btnExit)
    BaseTextView btnExit;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.close)
    BaseImageView close;
    @BindView(R.id.Pending)
    BaseTextView pending;

    private Uri imageUri;
    private final int REQUEST_CODE_PERMISSION = 2;
    private boolean doubleBackToExitPressedOnce;


    @Nullable
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_up, R.anim.no_anim);
        setContentView(R.layout.fragment_profile_consultant);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.redColor));
        }


        initView();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (!Constants.getUser(this).getImagePath().isEmpty())
            checkProfileImage();
    }

    private void checkProfileImage() {

        CheckProfileImageValidService.getInstance().checkProfileImageValid(this, getResources(), new EmptyReq(), new ResponseListener<CheckProfileImageValidResponse>() {
            @Override
            public void onGetError(String error) {

            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantProfileActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(pending.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(CheckProfileImageValidResponse response) {
                if (response.isSuccessed()) {
                    pending.setVisibility(View.GONE);
                } else {
                    pending.setVisibility(View.VISIBLE);
                    pending.setText(getResources().getString(R.string.pending));
                }
            }
        });
    }


    private void initView() {

        User user = Constants.getUser(this);
        if (user != null) {
            loadImage(user.getImagePath());

            if (!TextUtils.isEmpty(user.getLastName())) {
                if (!TextUtils.isEmpty(user.getDisplayName())) {
                    tvName.setText(user.getLastName() + " (" + user.getDisplayName() + ")");
                } else {
                    tvName.setText(user.getLastName());
                }
            } else {
                tvName.setText(null);
            }


            if (!TextUtils.isEmpty(user.getOfficeTitle())) {
                tvPostName.setText(getResources().getString(R.string.realtor) + " " + user.getOfficeTitle());
            } else {
                tvPostName.setText(null);
            }


            if (!TextUtils.isEmpty(user.getMobile())) {
                String mobile = "<font color='#777777'>" + getResources().getString(R.string.mobile_text) + ": </font> " +
                        "<font color='#000000' <strong> " + user.getMobile() + "</strong></font>";
                tvMobile.setText(createHtmlText(mobile));
            } else {
                tvMobile.setText(null);
            }

            if (!TextUtils.isEmpty(user.getActivityLocation())) {
                String location = "<font color='#777777'>" + getResources().getString(R.string.location_agency) + ": </font> " +
                        "<font color='#000000' <strong> " + user.getActivityLocation() + "</strong></font>";
                tvLocation.setText(createHtmlText(location));
            } else {
                tvLocation.setText(null);
            }

        }

    }

    private void loadImage(String userImage) {
        if (!TextUtils.isEmpty(userImage)) {
            ivPerson.setBorderWidth(4);
            ivPerson.setBorderColor(getResources().getColor(R.color.white));
        }
        Glide.with(this).load(userImage).error(R.drawable.ic_person).into(ivPerson);
    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }


    @OnClick({R.id.close, R.id.ivPerson, R.id.btnChangePassword, R.id.btnExit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close:
                finish();
                closeAnimation();
                break;
            case R.id.ivPerson:
                updateProfilePicture();
                break;

            case R.id.btnChangePassword:
                Intent intent = new Intent(this, ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.btnExit:
                if (Constants.getUser(this) != null) {
                    if (!Constants.getUser(this).isGeneral()) {
                        CustomDialog dialog = new CustomDialog(this);
                        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                            dialog.dismiss();
                            Constants.setCurrentUser(null);
                            TransactionUser.getInstance().deleteUsr(this);
                            if (BuildConfig.isCounsultant) {
                                Intent intent1 = new Intent(ConsultantProfileActivity.this, LoginActivity.class);
                                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent1);
                            } else {
                                finish();
                            }
                        });
                        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
                        dialog.setDialogTitle(getResources().getString(R.string.are_you_sure_you_want_to_leave_the_account));
                        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                        dialog.setIcon(R.drawable.ic_logout, getResources().getColor(R.color.redColor));
                        dialog.show();

                    }
                }
                break;
        }
    }

    private void updateProfilePicture() {
        if (PermissionHandler.hasAllPermissions(this)) {
            showDialogForImageSelection();
        } else {
            PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
        }
    }

    private void showDialogForImageSelection() {
        BottomSheetMenuDialog dialog = new BottomSheetBuilder(this, R.style.AppTheme_BottomSheetDialog)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_bottom_sheet)
                .setItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.btnTakePhoto:
                            openCameraTake();
                            break;
                        case R.id.btnImageGallery:
                            openGalleryPhotos();
                            break;
                    }
                })
                .createDialog();
        dialog.show();
    }

    private void openGalleryPhotos() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.RESULT_LOAD_IMAGE);
    }

    private void openCameraTake() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "دلتا");
        values.put(MediaStore.Images.Media.DESCRIPTION, "عکس خود را انتخاب کنید");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, Constants.REQUEST_IMAGE_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
                String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
                UCrop.of(imageUri, Uri.fromFile(new File(getCacheDir(), destinationFileName))).withAspectRatio(1, 1)
                        .withMaxResultSize(280, 280)
                        .start(this);
            } else if (requestCode == Constants.RESULT_LOAD_IMAGE && data != null) {
                getImageFromGallery(data);
            }
        }
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            uploadImageCameraRequest(bitmapImage(data));

        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private String bitmapImage(Intent data) {
        final Uri resultUri = UCrop.getOutput(data);
        InputStream imageStream = null;
        try {
            if (resultUri != null)
                imageStream = getContentResolver().openInputStream(resultUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        return encodeImage(selectedImage);
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }


    private void getImageFromGallery(Intent data) {
        if (data != null) {
//            try {
            Uri selectedImage = data.getData();
            String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
            if (selectedImage != null)
                UCrop.of(selectedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName))).withAspectRatio(1, 1)
                        .withMaxResultSize(280, 280)
                        .start(this);
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
//                uploadImageCameraRequest((encodeImage(bitmap)));
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean hasAllPermission = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    hasAllPermission = false;
                    break;
                }
            }
            if (hasAllPermission) {
                showDialogForImageSelection();
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                PermissionHandler.showSettingPermissionPage(this, false);
            } else {
                if (this.doubleBackToExitPressedOnce) {

                } else {
                    this.doubleBackToExitPressedOnce = true;
                    PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
                }
            }
        }
    }

    private void uploadImageCameraRequest(String base64ImageCamera) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        ProfileImageReq req = new ProfileImageReq();
        req.setImageBase64(base64ImageCamera);
        req.setThmbHeight(0);
        req.setThmbWidth(0);

        UploadAgentImageService.getInstance().getUploadProfileImage(this, getResources(), req, new ResponseListener<ProfileImageResponse>() {
            @Override
            public void onGetError(String error) {
                if (tvPostName != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    showErrorDialog(getResources().getString(R.string.error_sending_photo));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantProfileActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvPostName.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(ProfileImageResponse response) {
                if (tvPostName != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        TransactionUser.getInstance().updateUserImage(ConsultantProfileActivity.this, response.getFullImagePath());
                        Constants.getUser(ConsultantProfileActivity.this).setImagePath(response.getFullImagePath());
                        loadImage(Constants.getUser(ConsultantProfileActivity.this).getImagePath());
                        Toast.makeText(ConsultantProfileActivity.this, !TextUtils.isEmpty(response.getMessage()) ? response.getMessage() : getResources().getString(R.string.edit_success), Toast.LENGTH_SHORT).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.error_sending_photo));
                    }
                }
            }
        });
    }

    public void showErrorDialog(String description) {

        CustomDialog customDialog = new CustomDialog(this);
        customDialog.setOkListener(getString(R.string.retry_text), view -> {
            customDialog.dismiss();
            showDialogForImageSelection();
        });
        customDialog.setCancelListener(getString(R.string.cancel), view -> customDialog.dismiss());
//        customDialog.setIcon(R.drawable.ic_bug_repoart, getResources().getColor(R.color.redColor));
        customDialog.setLottieAnim("error.json", 0);
        if (description != null) {
            customDialog.setDescription(description);
        }

        customDialog.setDialogTitle(getString(R.string.communicationError));
        customDialog.show();

    }

    private void closeAnimation() {
        overridePendingTransition(0, R.anim.slide_out_up);
    }

    @Override
    public void onBackPressed() {
        closeAnimation();
        super.onBackPressed();

    }
}
