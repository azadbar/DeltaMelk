package ir.delta.delta.profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;

public class UserProfileActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estate_edit);

        UserProfileFragment userProfileFragment = new UserProfileFragment();
        FragmentManager fragMng = getSupportFragmentManager();
        FragmentTransaction frgTrans = fragMng.beginTransaction();
        frgTrans.replace(R.id.frameLayout, userProfileFragment);
        frgTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        frgTrans.commit();

    }


}
