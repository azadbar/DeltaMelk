package ir.delta.delta.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.Model.Language;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.service.Request.EditProfileUserService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.EditProfileUserReq;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.EditProfileUserResponse;
import ir.delta.delta.util.Constants;

import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends BaseFragment {

    private final String AREAS = "areas";
    @BindView(R.id.rlChangeName)
    BaseRelativeLayout rlChangeName;
    @BindView(R.id.area_layout)
    LocationView areaLayout;
    @BindView(R.id.rlExit)
    BaseTextView rlExit;
    @BindView(R.id.tvAccount)
    BaseTextView tvAccount;
    @BindView(R.id.btnRegister)
    BaseTextView btnRegister;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.text)
    BaseTextView text;
    @BindView(R.id.imgAccountNextPage)
    BaseImageView imgAccountNextPage;
    private String name;
    private Area selectedArea;
    private ArrayList<Area> areas;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_user_profile, container, false);
        ButterKnife.bind(this, view);


        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.edit_name));
        checkArrowRtlORLtr(imgBack);

        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            imgAccountNextPage.setBackgroundResource(R.drawable.ic_keyboard_english);
        } else {
            imgAccountNextPage.setBackgroundResource(R.drawable.ic_keyboard_arrow);
        }
        areaLayout.setImgInfo(R.drawable.ic_location_gray);
        areaLayout.setTxtTitle(getString(R.string.area_text));
        areaLayout.setHint(getString(R.string.your_estat));
        areaSelected(null);
        areas = TransactionArea.getInstance().getAreasBy(getActivity(), Constants.getCity(getActivity()).getId());
        if (areas.size() > 0) {
            areaLayout.setVisibility(View.VISIBLE);
            text.setVisibility(View.VISIBLE);
            Area areaById = TransactionArea.getInstance().getAreaById(getActivity(), Constants.getUser(getActivity()).getLocationId());
            if (areaById != null) {
                for (int i = 0; i < areas.size(); i++) {
                    if (areaById.getId() == areas.get(i).getId()) {
                        areaLayout.setValue(areaById.getName(), areaById.getDescription());
                        selectedArea = areaById;
                    }
                }
            }
        } else {
            areaLayout.setVisibility(View.GONE);
            text.setVisibility(View.GONE);
            selectedArea = null;
        }

        if (Constants.getUser(getActivity()) != null) {
            if (Constants.getUser(getActivity()).isGeneral()) {
                tvAccount.setText(Constants.getUser(getActivity()).getDisplayName());
                name = Constants.getUser(getActivity()).getDisplayName();
            }
        }


        return view;
    }

    @OnClick({R.id.rlChangeName, R.id.area_layout, R.id.rlExit, R.id.btnRegister, R.id.imgBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlChangeName:
                if (getActivity() != null) {
                    ChangeNameDialog dialog = new ChangeNameDialog(getActivity());
                    dialog.setIcon(R.drawable.ic_person, getResources().getColor(R.color.redColor));
                    dialog.setOkListener(getResources().getString(R.string.register), v -> {
                        name = dialog.getEditText();
                        tvAccount.setText(name);
                        dialog.dismiss();
                    });
                    dialog.setDialogTitle(getString(R.string.enter_name));
                    dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                    dialog.setCancelListener(getResources().getString(R.string.cancel), v -> dialog.dismiss());
                    dialog.setEditTextName(Constants.getUser(getActivity()).getDisplayName());
                    dialog.show();
                }
                break;
            case R.id.area_layout:

                if (areas.size() > 0) {
                    LocationSelectFragment locationSelectFragment = new LocationSelectFragment();
                    locationSelectFragment.setTargetFragment(UserProfileFragment.this, Constants.REQUEST_AREA_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_area));
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", false);
                    bundle.putString("extraName", AREAS);
                    bundle.putSerializable("locationType", LocationTypeEnum.AREA);
                    bundle.putParcelableArrayList("list", new ArrayList<>(areas));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationSelectFragment.setArguments(bundle);
                    loadFragment(locationSelectFragment, LocationSelectFragment.class.getName());
                }
                break;

            case R.id.rlExit:
                if (getActivity() != null) {
                    CustomDialog dialog1 = new CustomDialog(getActivity());
                    dialog1.setOkListener(getResources().getString(R.string.ok), view1 -> {
                        dialog1.dismiss();
                        Constants.setCurrentUser(null);
                        TransactionUser.getInstance().deleteUsr(getActivity());
                        getActivity().finish();
                    });
                    dialog1.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog1.dismiss());
                    dialog1.setDialogTitle(getResources().getString(R.string.are_you_sure_you_want_to_leave_the_account));
                    dialog1.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                    dialog1.setIcon(R.drawable.ic_logout, getResources().getColor(R.color.redColor));
                    dialog1.show();
                }


                break;
            case R.id.btnRegister:
                if (isValidData()) {
                    editProfileUser();
                }
                break;
            case R.id.imgBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }

                break;
        }
    }

    private boolean isValidData() {

        ArrayList<String> errorMsgList = new ArrayList<>();
        if (tvAccount.getText() == null) {
            String message = getString(R.string.enter_title, "نام ");
            errorMsgList.add(message);
        }

        if (selectedArea == null && TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) > 0) {
            String message = getResources().getString(R.string.select_area);
            areaLayout.setError(message);
            errorMsgList.add(message);
        }


        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

    private void editProfileUser() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        EditProfileUserReq req = new EditProfileUserReq();
        req.setFullName(name);
        req.setLocationId(selectedArea != null ? selectedArea.getId() : Constants.getCity(getActivity()).getId());
        EditProfileUserService.getInstance().editProfileUser(getContext(), getResources(), req, new ResponseListener<EditProfileUserResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }
            @Override
            public void onSuccess(EditProfileUserResponse response) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed() && getActivity() != null) {
                        TransactionUser.getInstance().updateUserName(getActivity(), name);
                        TransactionUser.getInstance().updateLocationIdUser(getActivity(), selectedArea != null ? selectedArea.getId() : Constants.getUser(getActivity()).getLocationId());
                        Constants.getUser(getActivity()).setDisplayName(name);
                        Constants.getUser(getActivity()).setLocationId(selectedArea != null ? selectedArea.getId() : Constants.getUser(getActivity()).getLocationId());
                        CustomDialog successDialog = new CustomDialog(getActivity());
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                        successDialog.setLottieAnim("tick.json", 0);
                        successDialog.setDescription(response.getMessage());
                        successDialog.setOkListener(getString(R.string.ok), view -> successDialog.dismiss());
                        successDialog.show();
                    } else {
                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });
    }

    private void areaSelected(Area area) {
        areaLayout.setVisibility(View.VISIBLE);
        selectedArea = area;
        if (selectedArea != null) {
            areaLayout.setValue(selectedArea.getName(), selectedArea.getDescription());
        } else {
            if (TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) > 0) {
                areaLayout.reset();
            } else {
                areaLayout.setVisibility(View.GONE);
//                areaLayout.notExist();
            }
        }
    }


    public void loadFragment(LocationSelectFragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
            fragTrans.addToBackStack(fragmentTag);
            fragTrans.commit();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == Constants.REQUEST_AREA_CODE) {
                areaSelected(data.getParcelableExtra(AREAS));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
