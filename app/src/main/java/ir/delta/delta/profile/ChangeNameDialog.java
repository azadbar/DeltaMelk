package ir.delta.delta.profile;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.util.Constants;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class ChangeNameDialog extends Dialog {


    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    private String okTitle;
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private String cancelTitle;
    private String title;
    private int image;
    private int color;
    private int colorIcon;
    private String name;

    public ChangeNameDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.custom_change_name_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);
        if (cancelListener != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(cancelTitle);
            btnCancel.setOnClickListener(cancelListener);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        if (okTitle != null) {
            btnOk.setText(okTitle);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(okListener);
        } else {
            btnOk.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        tvTitle.setTextColor(color);

        if (image > 0) {
            icon.setVisibility(View.VISIBLE);
            icon.setImageResource(image);
            icon.setColorFilter(colorIcon, PorterDuff.Mode.SRC_ATOP);
        }

        if (name != null) {
            edtName.setTextBody(name);
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }

    public void setIcon(int image, int color) {
        this.image = image;
        this.colorIcon = color;
    }

    public void setCancelListener(String cancelTitle, View.OnClickListener cancelListener) {
        this.cancelTitle = cancelTitle;
        this.cancelListener = cancelListener;
    }

    public void setOkListener(String okTitle, View.OnClickListener okListener) {
        this.okTitle = okTitle;
        this.okListener = okListener;
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }

    public void setColorTitle(int color) {
        this.color = color;
    }

    public void setEditTextName(String name) {
        this.name = name;
    }

    public String getEditText() {
        return edtName.getValueString();
    }
}
