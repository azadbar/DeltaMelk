package ir.delta.delta.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.aboutDelta.AboutDeltaActivity;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.enums.LoginState;
import ir.delta.delta.service.Request.LoginMobileService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.SendActivationCodeService;
import ir.delta.delta.service.RequestModel.LoginMobileReq;
import ir.delta.delta.service.RequestModel.SendActivationCodeReq;
import ir.delta.delta.service.ResponseModel.profile.LoginMobileResponse;
import ir.delta.delta.service.ResponseModel.profile.SendActivationCodeResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class InputMobileFragment extends BaseFragment {

    public String text;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.btnSend)
    BaseTextView btnSend;
    Unbinder unbinder;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.description_login)
    BaseTextView descriptionLogin;
    private LoginState loginState;
    private String mobile;
    private boolean isFirstActivity;


    public InputMobileFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_input_mobile, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle b = getArguments();
        if (b != null) {
            mobile = b.getString("mobile");
            loginState = (LoginState) b.get("loginState");
            isFirstActivity = b.getBoolean("isFirstActivity");
        }

        initView();
        return view;
    }

    private void initView() {
        if (text != null) {
            edtMobile.setTextBody(text);
        }
        checkArrowRtlORLtr(imgBack);
        if (isFirstActivity) {
            rlBack.setVisibility(View.GONE);
        } else {
            rlBack.setVisibility(View.VISIBLE);
        }
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        if (getActivity() != null) {
            Window window = getActivity().getWindow();
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null && window != null) {
                Point size = Constants.getScreenSize(windowManager);
                if (size.x > getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile)) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) second.getLayoutParams();
                    layoutParams.width = getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile);
                    second.setLayoutParams(layoutParams);
                }
            }
            if (this.edtMobile.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }


        edtMobile.setTextBody(mobile);
        if (BuildConfig.isCounsultant) {
            if (loginState == LoginState.forgetPassword) {
                btnSend.setText(getResources().getString(R.string.send_activation_code));
                tvFilterTitle.setText(getResources().getString(R.string.send_activation_code));
            } else {
                btnSend.setText(getResources().getString(R.string.next_level));
                tvFilterTitle.setText(getResources().getString(R.string.login_consultant));
            }
            descriptionLogin.setText(getResources().getString(R.string.description_login_cnsultant));

        } else {
            tvFilterTitle.setText(getResources().getString(R.string.login));
            descriptionLogin.setText(getResources().getString(R.string.mobile_sample));
            btnSend.setText(getResources().getString(R.string.send));
        }


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rlBack, R.id.btnSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((LoginActivity) getActivity()).backPress();
                }
                break;
            case R.id.btnSend:
                if (isMobileValid()) {
                    roundedLoadingView.setVisibility(View.VISIBLE);
                    enableDisableViewGroup(root, false);
                    if (loginState == LoginState.forgetPassword) {
                        getActivateCodeForConsultant();
                    } else {
                        getLoginMobileResponse();
                    }
                }
                break;
        }
    }

    private void getActivateCodeForConsultant() {

        SendActivationCodeReq req = new SendActivationCodeReq();
        req.setUsername(mobile);
        SendActivationCodeService.getInstance().getSendActivationCode(getResources(), req, new ResponseListener<SendActivationCodeResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(SendActivationCodeResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        VerificationCodeConsultantFragment vfCodeFragment = new VerificationCodeConsultantFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("userId", response.getUserToken());
                        bundle.putString("mobile", mobile);
                        vfCodeFragment.setArguments(bundle);
                        loadFragment(vfCodeFragment, VerificationCodeConsultantFragment.class.getName());
                    } else {
                        if (!TextUtils.isEmpty(response.getMessage())) {
                            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void getLoginMobileResponse() {
        LoginMobileReq req = new LoginMobileReq();
        req.setMobile(edtMobile.getValueString());
        req.setLocationId(PreferencesData.getCityId(getContext()));
        LoginMobileService.getInstance().getLoginMobile(getResources(), req, new ResponseListener<LoginMobileResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getActivity());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(LoginMobileResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);

                    if (response.isSuccessed()) {
                        if (response.isUseVerificationCode()) {
                            LoginUserFragment inputSubmittedCodeFragment = new LoginUserFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("mobile", edtMobile.getValueString());
                            inputSubmittedCodeFragment.setArguments(bundle);
                            loadFragment(inputSubmittedCodeFragment, LoginUserFragment.class.getName());
                        } else {
                            LoginConsultantFragment loginConsultantFragment = new LoginConsultantFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("mobile", edtMobile.getValueString());
                            loginConsultantFragment.setArguments(bundle);
                            loadFragment(loginConsultantFragment, LoginConsultantFragment.class.getName());
                        }
                    } else {
                        if (response.isUseVerificationCode()) {
                            CustomDialog dialog = new CustomDialog(getActivity());
                            dialog.setIcon(R.drawable.ic_lock, getResources().getColor(R.color.redColor));
                            dialog.setDialogTitle(getResources().getString(R.string.unauthorized_user));
                            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                            dialog.setDescription(getString(R.string.desc_unauthorized_user));
                            dialog.setOkListener(getString(R.string.membership), v -> {
//                                AboutDeltaActivity aboutDeltaFragment = new AboutDeltaActivity();
//                                Bundle b = new Bundle();
//                                b.putString("urlSite", "https://delta.ir/%D8%AB%D8%A8%D8%AA-%D9%86%D8%A7%D9%85-%D8%A2%DA%98%D8%A7%D9%86%D8%B3");
//                                aboutDeltaFragment.setArguments(b);
//                                loadFragment(aboutDeltaFragment, AboutDeltaActivity.class.getName());
                                Intent intent = new Intent(getActivity(), AboutDeltaActivity.class);
                                intent.putExtra("urlSite", "https://delta.ir/%D8%AB%D8%A8%D8%AA-%D9%86%D8%A7%D9%85-%D8%A2%DA%98%D8%A7%D9%86%D8%B3");
                                startActivity(intent);
                                dialog.dismiss();
                            });
                            dialog.setCancelListener(getString(R.string.cancel), v -> dialog.dismiss());
                            dialog.setCancelable(true);
                            dialog.show();

                        } else {
                            if (!TextUtils.isEmpty(response.getMessage())) {
                                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                }
            }
        });
    }


    private boolean isMobileValid() {
        if (edtMobile.getError() != null) {
            Toast.makeText(getActivity(), edtMobile.getError(), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }
}
