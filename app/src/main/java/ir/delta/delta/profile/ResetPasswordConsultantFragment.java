package ir.delta.delta.profile;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.service.Request.ForgetPassConsultantService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.ForgetPassReq;
import ir.delta.delta.service.ResponseModel.profile.ForgetPassResponse;
import ir.delta.delta.util.Constants;

public class ResetPasswordConsultantFragment extends BaseFragment {


    Unbinder unbinder;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.edtConfirmPass)
    CustomEditText edtConfirmPass;
    @BindView(R.id.btnRegisterCode)
    BaseTextView btnRegisterCode;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.second)
    BaseLinearLayout second;
    private String userId;
    private String mobile;


    public ResetPasswordConsultantFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_reset_password_consultant, container, false);
        unbinder = ButterKnife.bind(this, view);

        initView();
        Bundle b = getArguments();
        if (b != null) {
            userId = b.getString("userId");
            mobile = b.getString("mobile");
        }
        return view;
    }

    private void initView() {
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.password_recovery));
        if (getActivity() != null) {
            Window window = getActivity().getWindow();
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null && window != null) {
                Point size = Constants.getScreenSize(windowManager);
                if (size.x > getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile)) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) second.getLayoutParams();
                    layoutParams.width = getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile);
                    second.setLayoutParams(layoutParams);
                }
            }
        }
        if (this.edtConfirmPass.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        if (this.edtPassword.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rlBack, R.id.btnRegisterCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((LoginActivity) getActivity()).backPress();
                }
                break;
            case R.id.btnRegisterCode:
                if (isValidPassword()) {
                    getForgetPassword();
                }
                break;

        }
    }

    private void getForgetPassword() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        ForgetPassReq req = new ForgetPassReq();
        req.setUserToken(userId);
        req.setPass(edtPassword.getValueString());
        req.setConfirmPass(edtConfirmPass.getValueString());
        ForgetPassConsultantService.getInstance().getForgetPasswrd(getResources(), req, new ResponseListener<ForgetPassResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }
            @Override
            public void onSuccess(ForgetPassResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        LoginConsultantFragment loginConsultantFragment = new LoginConsultantFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("mobile", mobile);
                        loginConsultantFragment.setArguments(bundle);
                        loadFragment(loginConsultantFragment, LoginConsultantFragment.class.getName());
                    }
                }
            }
        });
    }


    private boolean isValidPassword() {

        ArrayList<String> errorMsgList = new ArrayList<>();

        if (edtPassword.getError() != null) {
            errorMsgList.add(edtPassword.getError());
        }

        if (edtConfirmPass.getError() != null) {
            errorMsgList.add(edtConfirmPass.getError());
        }
        if (errorMsgList.size() > 0) {
            showInfoDialog((getString(R.string.fill_following)), errorMsgList);
            return false;
        }

        return true;
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

}
