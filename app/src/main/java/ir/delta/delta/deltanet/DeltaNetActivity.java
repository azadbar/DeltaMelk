package ir.delta.delta.deltanet;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dision.android.rtlviewpager.RTLPagerAdapter;
import com.dision.android.rtlviewpager.RTLViewPager;
import com.dision.android.rtlviewpager.Tab;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;


public class DeltaNetActivity extends BaseActivity implements DeltaNetListChangeInteface {

    private static final int TAB_DELTA_NET = 1;
    private static final int TAB_SELECTED_DELTA_NET = 2;

    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tool)
    AppBarLayout tool;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    RTLViewPager viewpager;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    private Tab[] mTabs;
    private boolean isDataChangeInDeltaNet = false;
    private boolean isDataChangeInDeltaNetSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deltanet);
        ButterKnife.bind(this);
        toolbar = findViewById(R.id.toolbar);
        checkArrowRtlORLtr(imgBack);
        imgLogo.setImageResource(R.drawable.ic_logo_delta);
        initTabs();
        setAdapters();
        setCustomFont();
        isDataChangeInDeltaNet = true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    private void initTabs() {

        mTabs = new Tab[]{
                new Tab(TAB_DELTA_NET, getString(R.string.deltanet)) {
                    @Override
                    public Fragment getFragment() {
                        DeltaNetFragment deltanet = new DeltaNetFragment();
                        deltanet.setListener(DeltaNetActivity.this);
                        return deltanet;
                    }
                },
                new Tab(TAB_SELECTED_DELTA_NET, getString(R.string.selected_deltanet)) {

                    @Override
                    public Fragment getFragment() {
                        SelectedDeltaNetFragment deltaNetFragment = new SelectedDeltaNetFragment();
                        deltaNetFragment.setListener(DeltaNetActivity.this);
                        return deltaNetFragment;
                    }
                }

        };
    }


    private void setAdapters() {
        RTLPagerAdapter mTabsAdapter = new RTLPagerAdapter(getFragmentManager(), mTabs, true);
        viewpager.setAdapter(mTabsAdapter);
        viewpager.setRtlOriented(true);
        tabs.setupWithViewPager(viewpager);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public AppBarLayout getToolbar() {
        return tool;
    }


    @OnClick(R.id.imgBack)
    public void onViewClicked() {
        finish();
    }

    public void setCustomFont() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
                }
            }
        }
    }

    @Override
    public void onChangeDataInDeltanet(boolean isChange) {
        isDataChangeInDeltaNet = isChange;
    }

    @Override
    public void onChangeDataInDeltanetSelected(boolean isChange) {
        isDataChangeInDeltaNetSelected = isChange;
    }

    @Override
    public boolean getChangeDeltanet() {
        return isDataChangeInDeltaNet;
    }

    @Override
    public boolean getChangeDeltanetSelected() {
        return isDataChangeInDeltaNetSelected;
    }

    @Override
    public boolean onIsFirst(boolean isFirst) {
        return isFirst;
    }
}