package ir.delta.delta.deltanet;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.service.ResponseModel.deltanet.SelectedDeltaNetEstate;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SelectedDeltaNetAdapter extends RecyclerView.Adapter<SelectedDeltaNetAdapter.ViewHolder> {


    private final ArrayList<SelectedDeltaNetEstate> list;
    private final OnItemClick listener;


    SelectedDeltaNetAdapter(ArrayList<SelectedDeltaNetEstate> list, OnItemClick listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_deltanet, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        SelectedDeltaNetEstate object = list.get(position);

        if (!TextUtils.isEmpty(object.getTitle())) {
            holder.tvDepositId.setVisibility(View.VISIBLE);
            holder.tvTitle.setText(object.getTitle());
        } else
            holder.tvTitle.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(object.getTitleDescription())) {
            holder.tvDepositId.setVisibility(View.VISIBLE);
            holder.tvDetail.setText(object.getTitleDescription());
        } else
            holder.tvDetail.setVisibility(View.GONE);


        if (!TextUtils.isEmpty(object.getDescription().trim())) {
            if (object.getDescription().length() >= 100) {
                holder.tvDepositId.setVisibility(View.VISIBLE);
                String description = "<font color='#777777'>" + object.getDescription().substring(0, 30).replace("</br>", "\n").trim() + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
                holder.tvDescription.setText(createHtmlText(description));
            } else {
                holder.tvDescription.setText(object.getDescription().replace("</br>", "\n").trim());
            }
        } else {
            holder.tvDescription.setVisibility(View.GONE);
        }


        if (object.getContractTypeLocalId() == ContractTypeEnum.PURCHASE.getMethodCode()) {
            holder.tvRent.setVisibility(View.GONE);
            holder.tvPrice.setVisibility(View.VISIBLE);
            if (object.getMortgageOrTotal() >= 1000) {
                holder.tvDepositId.setVisibility(View.VISIBLE);
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.price_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setVisibility(View.GONE);
            }
        } else {
            if (object.getMortgageOrTotal() >= 1000) {
                holder.tvPrice.setVisibility(View.VISIBLE);
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.mortgage_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setText(res.getString(R.string.no_deposit));
            }

            if (object.getRentOrMetric() >= 1000) {
                holder.tvDepositId.setVisibility(View.VISIBLE);
                holder.tvRent.setVisibility(View.VISIBLE);
                holder.tvRent.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.rent_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getRentOrMetric(), res) + "</strong></font>"));
            } else
                holder.tvRent.setText(res.getString(R.string.no_rent));
        }


        if (!TextUtils.isEmpty(object.getFullName())) {
            holder.tvDepositId.setVisibility(View.VISIBLE);
            holder.tvAdvertiser.setText(res.getString(R.string.advertiser) + ": " + object.getFullName());
        } else {
            holder.tvAdvertiser.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(object.getMobile())) {
            holder.tvDepositId.setVisibility(View.VISIBLE);
            ArrayList<String> myList = new ArrayList<String>(Arrays.asList(object.getMobile().split("-")));
            if (myList.size() > 0) {
                for (int i = 0; i < myList.size(); i++) {
                    holder.tvMobile.setText(res.getString(R.string.mobile) + ": " + myList.get(i));
                }
            } else {
                holder.tvMobile.setText(res.getString(R.string.mobile) + ": " + object.getMobile());
            }

        } else {
            holder.tvMobile.setVisibility(View.GONE);
        }

        if (object.getDepositId() > 0) {
            holder.tvDepositId.setVisibility(View.VISIBLE);
            holder.tvDepositId.setText(res.getString(R.string.estate_code) + ": " + object.getDepositId());
        } else {
            holder.tvDepositId.setVisibility(View.GONE);
        }

        if (object.getSendToSiteDate() != null && !object.getSendToSiteDate().isEmpty()) {
            holder.tvSendToSiteDate.setVisibility(View.VISIBLE);
            holder.tvSendToSiteDate.setText(res.getString(R.string.request_send) + ": " + object.getSendToSiteDate());
        } else {
            holder.tvSendToSiteDate.setVisibility(View.GONE);
        }

        if (object.isSendToSite()) {
            holder.btnSendToSite.setEnabled(false);
            holder.btnSendToSite.setClickable(false);
            holder.tvSendToSite.setText(res.getString(R.string.sended));
            holder.tvSendToSite.setTextColor(res.getColor(R.color.white));
            holder.btnSendToSite.setBackgroundColor(res.getColor(R.color.send_to_site));
        } else {
            holder.btnSendToSite.setEnabled(true);
            holder.btnSendToSite.setClickable(true);
            holder.tvSendToSite.setText(res.getString(R.string.send_to_site));
            holder.tvSendToSite.setTextColor(res.getColor(R.color.primaryTextColor));
            holder.btnSendToSite.setBackground(res.getDrawable(R.drawable.ripplewhite_no_radius));
        }


        holder.bind(object, position, listener);
        holder.tvDescription.setOnClickListener(view -> clickDescription(res, holder.tvDescription, object));
    }

    private void clickDescription(Resources res, BaseTextView tvDescription, SelectedDeltaNetEstate deltaNet) {

        if (deltaNet.getDescription().length() >= 100) {
            String description;
            if (deltaNet.isExpanded()) {
                description = "<font color='#777777'>" + deltaNet.getDescription().replace("</br>", "\n").substring(0, 20) + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
            } else {
                description = "<font color='#777777'>" + deltaNet.getDescription().replace("</br>", "\n") + " </font> " +
                        "<font color='#E30427' <strong> " + res.getString(R.string.less) + "</strong></font>";
            }
            deltaNet.setExpanded(!deltaNet.isExpanded());
            tvDescription.setText(createHtmlText(description));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDetail)
        BaseTextView tvDetail;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.tvPrice)
        BaseTextView tvPrice;
        @BindView(R.id.tvRent)
        BaseTextView tvRent;
        @BindView(R.id.tvAdvertiser)
        BaseTextView tvAdvertiser;
        @BindView(R.id.tvMobile)
        BaseTextView tvMobile;
        @BindView(R.id.img)
        BaseImageView img;
        @BindView(R.id.btnComminucation)
        BaseLinearLayout btnComminucation;
        @BindView(R.id.imgSend)
        BaseImageView imgSend;
        @BindView(R.id.btnSendToSite)
        BaseLinearLayout btnSendToSite;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.btnDelete)
        BaseLinearLayout btnDelete;
        @BindView(R.id.llButtons)
        BaseLinearLayout llButtons;
        @BindView(R.id.tvSendToSite)
        BaseTextView tvSendToSite;
        @BindView(R.id.tvDepositId)
        BaseTextView tvDepositId;
        @BindView(R.id.tvSendToSiteDate)
        BaseTextView tvSendToSiteDate;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(SelectedDeltaNetEstate object, int position, final OnItemClick listener) {
            btnSendToSite.setOnClickListener(v -> listener.onSendToSite(position, object));

            btnDelete.setOnClickListener(v -> listener.onDelete(position, object));

            btnComminucation.setOnClickListener(v -> listener.onCommunications(position, object));

        }


    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public interface OnItemClick {
        void onSendToSite(int position, SelectedDeltaNetEstate object);

        void onDelete(int position, SelectedDeltaNetEstate object);

        void onCommunications(int position, SelectedDeltaNetEstate object);
    }

}
