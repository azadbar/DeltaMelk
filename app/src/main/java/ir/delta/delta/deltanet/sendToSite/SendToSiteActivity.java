package ir.delta.delta.deltanet.sendToSite;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.edit.EditEstateFragment;

public class SendToSiteActivity extends BaseActivity {

    private long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to_site_delta_net);
        SendToSiteDeltaNetFragment sendToSiteDeltaNetFragment = new SendToSiteDeltaNetFragment();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getLong("id");
            if (id > 0) {
                Bundle b = new Bundle();
                b.putLong("id", id);
                sendToSiteDeltaNetFragment.setArguments(b);
            } else {
                finish();
            }
        }
        FragmentManager frgMgr = getSupportFragmentManager();
        FragmentTransaction frgTrans = frgMgr.beginTransaction();
        frgTrans.replace(R.id.frameLayout, sendToSiteDeltaNetFragment);
        frgTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        frgTrans.commit();
    }
}
