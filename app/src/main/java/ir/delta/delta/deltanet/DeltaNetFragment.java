package ir.delta.delta.deltanet;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.registerEstate.InfoListDialog;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DeltaNetService;
import ir.delta.delta.service.Request.DeltanetPropertyDeleteEstateService;
import ir.delta.delta.service.Request.DeltanetPropertySelectEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.DeltaNetReq;
import ir.delta.delta.service.RequestModel.DeltanetPropertySelectReq;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.deltanet.DeltaNetEstate;
import ir.delta.delta.service.ResponseModel.deltanet.DeltanetActionResponse;
import ir.delta.delta.service.ResponseModel.deltanet.DeltanetResponse;
import ir.delta.delta.util.Constants;

public class DeltaNetFragment extends Fragment implements DeltaNetAdapter.OnItemClick {


    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rvDeltaNet)
    RecyclerView rvDeltaNet;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;

    private Unbinder unbinder;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<DeltaNetEstate> list = new ArrayList<>();
    private DeltaNetAdapter adapter;
    private DeltaNetListChangeInteface listener;

    public void setListener(DeltaNetListChangeInteface listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_deltanet, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingView.setButtonClickListener(view1 -> getList());
        getList();
        return view;
    }


    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (list.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getDeltaNetList();
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null && listener != null && listener.getChangeDeltanet()) {
            endOfList = false;
            list.clear();
            getList();
        }
    }


    private void getDeltaNetList() {

        DeltaNetReq req = new DeltaNetReq();
        req.setStartIndex(list.size());
        req.setOffset(offset);
        DeltaNetService.getInstance().deltaNetRequest(getActivity(), getResources(), req, new ResponseListener<DeltanetResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    onError(error);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(DeltanetResponse response) {
                if (getActivity() != null && isAdded()) {
                    enableDisableViewGroup(root, true);
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getDeltanetDeposits() != null) {
                        if (response.getDeltanetDeposits().size() < offset) {
                            endOfList = true;
                        }
                        list.addAll(response.getDeltanetDeposits());
                        if (list.size() > 0) {
                            setDataAdapter();
                        } else {
                            rootEmptyView.setVisibility(View.VISIBLE);
                        }
                        if (listener != null) {
                            listener.onChangeDataInDeltanet(false);
                        }
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }


    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new DeltaNetAdapter(list, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvDeltaNet.setHasFixedSize(true);
            rvDeltaNet.setLayoutManager(layoutManager);
            rvDeltaNet.setAdapter(adapter);
            rvDeltaNet.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvDeltaNet.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvDeltaNet.setVisibility(View.GONE);
        loadingView.showLoading(false);
        rootEmptyView.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvDeltaNet.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && list != null) {
            rlLoding.setVisibility(View.GONE);
            if (list.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

    @Override
    public void onItemClick(int position, DeltaNetEstate deltanet) {
        if (deltanet.isSelected()) {
            deleteDeltaNet(deltanet);
        } else {
            selectedDeltaNet(deltanet);
        }

    }

    private void selectedDeltaNet(DeltaNetEstate deltanet) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        DeltanetPropertySelectReq req = new DeltanetPropertySelectReq();
        req.setDeltanetDepositId(deltanet.getDataId());

        DeltanetPropertySelectEstateService.getInstance().deltanetPropertySelectEstate(getActivity(), getResources(), req, new ResponseListener<DeltanetActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }


            @Override
            public void onSuccess(DeltanetActionResponse response) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.success_selected));
                        getDeltaNetDelete(response.getDeltanetDepositId(), true);
                        if (listener != null) {
                            listener.onChangeDataInDeltanetSelected(true);
                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        if (response.getModelStateErrors() !=null &&response.getModelStateErrors().size() > 0) {
                            showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                        } else {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                        }
                    }
                }
            }
        });
    }

    public void showErrorFromServer(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        if (modelStateErrors != null && modelStateErrors.size() > 0) {
            ArrayList<String> errorList = new ArrayList<>();
            for (ModelStateErrors error : modelStateErrors) {
                if (!TextUtils.isEmpty(error.getMessage()))
                    errorList.add(error.getMessage());
            }
            if (errorList.size() > 0) {
                showInfoDialog(getString(R.string.error), errorList);
                return;
            }
        }
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        if (getActivity() != null) {
            InfoListDialog infoListDialog = new InfoListDialog(getActivity());
            infoListDialog.errorMsg = errorMsgList;
            infoListDialog.title = title;
            infoListDialog.show();
        }
    }

    private void deleteDeltaNet(DeltaNetEstate deltanet) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        DeltanetPropertySelectReq req = new DeltanetPropertySelectReq();
        req.setDeltanetDepositId(deltanet.getDataId());

        DeltanetPropertyDeleteEstateService.getInstance().deltanetPropertyDeleteEstate(getActivity(), getResources(), req, new ResponseListener<DeltanetActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(DeltanetActionResponse response) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        if (response.isSuccessed()) {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.success_delete));
                            getDeltaNetDelete(response.getDeltanetDepositId(), false);
                            if (listener != null) {
                                listener.onChangeDataInDeltanetSelected(true);
                            }
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                        }

                    }
                }
            }
        });
    }

    private void showSuccessToast(String message, String defualt) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), defualt, Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeltaNetDelete(int id, boolean isSelected) {
        for (DeltaNetEstate estate : list) {
            if (estate.getDataId() == id) {
                estate.setSelected(isSelected);
            }
        }
    }
}
