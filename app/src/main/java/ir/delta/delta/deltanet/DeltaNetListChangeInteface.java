package ir.delta.delta.deltanet;

public interface DeltaNetListChangeInteface {
    void onChangeDataInDeltanet(boolean isChange);

    void onChangeDataInDeltanetSelected(boolean isChange);

    boolean getChangeDeltanet();

    boolean getChangeDeltanetSelected();

    boolean onIsFirst(boolean isFirst);
}
