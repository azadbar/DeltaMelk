package ir.delta.delta.deltanet;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.service.ResponseModel.deltanet.DeltaNetEstate;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class DeltaNetAdapter extends RecyclerView.Adapter<DeltaNetAdapter.ViewHolder> {


    private final ArrayList<DeltaNetEstate> list;
    private final OnItemClick listener;


    DeltaNetAdapter(ArrayList<DeltaNetEstate> list, OnItemClick listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_deltanet, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        DeltaNetEstate deltanet = list.get(position);

        if (!TextUtils.isEmpty(deltanet.getTitle()))
            holder.tvTitle.setText(deltanet.getTitle());
        else
            holder.tvTitle.setText(null);

        if (!TextUtils.isEmpty(deltanet.getTitleDescription()))
            holder.tvDetail.setText(deltanet.getTitleDescription());
        else
            holder.tvDetail.setText(null);


        if (!TextUtils.isEmpty(deltanet.getDescription().trim())) {
            if (deltanet.getDescription().length() >= 100) {
                String description = "<font color='#777777'>" + deltanet.getDescription().substring(0, 30).replace("</br>", "\n").trim() + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
                holder.tvDescription.setText(createHtmlText(description));
            } else {
                holder.tvDescription.setText(deltanet.getDescription().replace("</br>", "\n").trim());
            }
        } else {
            holder.tvDescription.setText(null);
        }


        if (deltanet.getContractTypeLocalId() == ContractTypeEnum.PURCHASE.getMethodCode()) {
            holder.tvRent.setVisibility(View.GONE);
            holder.tvPrice.setVisibility(View.VISIBLE);
            if (deltanet.getMortgageOrTotal() >= 1000) {
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.price_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(deltanet.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setText(null);
            }
        } else {
            if (deltanet.getMortgageOrTotal() >= 1000) {
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.mortgage_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(deltanet.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setText(res.getString(R.string.no_deposit));
            }

            if (deltanet.getRentOrMetric() >= 1000)
                holder.tvRent.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.rent_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(deltanet.getRentOrMetric(), res) + "</strong></font>"));
            else
                holder.tvRent.setText(res.getString(R.string.no_rent));
        }


        if (!TextUtils.isEmpty(deltanet.getPersianDate())) {
            holder.tvDate.setText(res.getString(R.string.date) + ": " + deltanet.getPersianDate());
        } else {
            holder.tvDate.setText(null);
        }

        if (deltanet.isSelected()) {
            holder.btnAddDeltaNet.setText(res.getString(R.string.delete_deltanet_selected));
            holder.rlBtn.setBackgroundColor(res.getColor(R.color.primaryBack));
            holder.image.setImageResource(R.drawable.ic_delete_red);
            holder.image.setColorFilter(res.getColor(R.color.primaryFore), PorterDuff.Mode.SRC_ATOP);
        } else {
            holder.btnAddDeltaNet.setText(res.getString(R.string.add_deltanet_selected));
            holder.rlBtn.setBackgroundColor(res.getColor(R.color.applyFore));
            holder.image.setImageResource(R.drawable.ic_add_circle);
            holder.image.setColorFilter(res.getColor(R.color.primaryFore), PorterDuff.Mode.SRC_ATOP);
        }
        holder.bind(deltanet, position, listener);
        holder.tvDescription.setOnClickListener(view -> clickDescription(res, holder.tvDescription, deltanet));
    }


    private void clickDescription(Resources res, BaseTextView tvDescription, DeltaNetEstate deltaNet) {

        if (deltaNet.getDescription().length() >= 100) {
            String description;
            if (deltaNet.isExpanded()) {
                description = "<font color='#777777'>" + deltaNet.getDescription().replace("</br>", "\n").substring(0, 20) + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
            } else {
                description = "<font color='#777777'>" + deltaNet.getDescription().replace("</br>", "\n") + " </font> " +
                        "<font color='#E30427' <strong> " + res.getString(R.string.less) + "</strong></font>";
            }
            deltaNet.setExpanded(!deltaNet.isExpanded());
            tvDescription.setText(createHtmlText(description));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDetail)
        BaseTextView tvDetail;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.tvPrice)
        BaseTextView tvPrice;
        @BindView(R.id.tvRent)
        BaseTextView tvRent;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.btnAddDeltaNet)
        BaseTextView btnAddDeltaNet;
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.rlBtn)
        BaseLinearLayout rlBtn;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(DeltaNetEstate deltanet, int position, final OnItemClick listener) {
            rlBtn.setOnClickListener(v -> listener.onItemClick(position, deltanet));
        }


    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public interface OnItemClick {
        void onItemClick(int position, DeltaNetEstate deltanet);
    }

}
