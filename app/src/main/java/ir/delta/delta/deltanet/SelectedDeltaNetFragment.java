package ir.delta.delta.deltanet;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.registerEstate.InfoListDialog;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.deltanet.sendToSite.SendToSiteActivity;
import ir.delta.delta.deltanet.sendToSite.SendToSiteDeltaNetFragment;
import ir.delta.delta.estateDetail.ComminucationDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DeltanetPropertyDeleteEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.SelectedDeltaNetService;
import ir.delta.delta.service.RequestModel.DeltanetPropertySelectReq;
import ir.delta.delta.service.RequestModel.SelectedDeltaNetReq;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.deltanet.DeltanetActionResponse;
import ir.delta.delta.service.ResponseModel.deltanet.SelectedDeltaNetEstate;
import ir.delta.delta.service.ResponseModel.deltanet.SelectedDeltanetResponse;
import ir.delta.delta.util.Constants;

import static android.app.Activity.RESULT_OK;

public class SelectedDeltaNetFragment extends Fragment implements SelectedDeltaNetAdapter.OnItemClick, OnRefreshDeltaNetSelectedListener {


    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rvDeltaNet)
    RecyclerView rvDeltaNet;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;

    private Unbinder unbinder;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<SelectedDeltaNetEstate> list = new ArrayList<>();
    private SelectedDeltaNetAdapter adapter;
    private DeltaNetListChangeInteface listener;
    private boolean isFirst = true;


    public void setListener(DeltaNetListChangeInteface listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_selected_deltanet, container, false);
        unbinder = ButterKnife.bind(this, view);

        loadingView.setButtonClickListener(view1 -> getList());

        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getView() != null && listener != null) {

            if (listener.getChangeDeltanetSelected()) {
                endOfList = false;
                list.clear();
                getList();
            } else {
                if (listener.onIsFirst(isFirst)) {
                    getList();
                    isFirst = false;
                }
            }

        }
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (list.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getSelectedDeltaNetList();
        }
    }

    private void getSelectedDeltaNetList() {

        SelectedDeltaNetReq req = new SelectedDeltaNetReq();
        req.setStartIndex(list.size());
        req.setOffset(offset);
        SelectedDeltaNetService.getInstance().selectedDeltaNetRequest(getActivity(), getResources(), req, new ResponseListener<SelectedDeltanetResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    onError(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getActivity());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(SelectedDeltanetResponse response) {
                if (getActivity() != null && isAdded()) {
                    enableDisableViewGroup(root, true);
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getDeltanetSelectedDeposits() != null) {
                        if (response.getDeltanetSelectedDeposits().size() < offset) {
                            endOfList = true;
                        }
                        list.addAll(response.getDeltanetSelectedDeposits());
                        if (list.size() > 0) {
                            setDataAdapter();
                        } else {
                            rootEmptyView.setVisibility(View.VISIBLE);
                        }

                        if (listener != null) {
                            listener.onChangeDataInDeltanetSelected(false);
                        }
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new SelectedDeltaNetAdapter(list, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvDeltaNet.setHasFixedSize(true);
            rvDeltaNet.setLayoutManager(layoutManager);
            rvDeltaNet.setAdapter(adapter);
            rvDeltaNet.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvDeltaNet.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvDeltaNet.setVisibility(View.GONE);
        loadingView.showLoading(false);
        rootEmptyView.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvDeltaNet.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && list != null) {
            rlLoding.setVisibility(View.GONE);
            if (list.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }


    @Override
    public void onSendToSite(int position, SelectedDeltaNetEstate object) {
        Intent intent = new Intent(getActivity(), SendToSiteActivity.class);
        intent.putExtra("id", object.getDataId());
        intent.putExtra("title", object.getTitle());
        SendToSiteDeltaNetFragment.listener = this;
        startActivity(intent);
//        Toast.makeText(getActivity(), "این امکان به زودی اضافه خواهد شد", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDelete(int position, SelectedDeltaNetEstate object) {

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        DeltanetPropertySelectReq req = new DeltanetPropertySelectReq();
        req.setDeltanetDepositId(object.getDataId());

        DeltanetPropertyDeleteEstateService.getInstance().deltanetPropertyDeleteEstate(getActivity(), getResources(), req, new ResponseListener<DeltanetActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(DeltanetActionResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.success_delete));
                        removeFromList(response.getDeltanetDepositId());
                        if (listener != null) {
                            listener.onChangeDataInDeltanet(true);
                        }
                        if (adapter != null) {
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        if (response.getModelStateErrors() !=null &&response.getModelStateErrors().size() > 0) {
                            showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                        } else {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                        }
                    }
                }
            }
        });
    }

    public void showErrorFromServer(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        if (modelStateErrors != null && modelStateErrors.size() > 0) {
            ArrayList<String> errorList = new ArrayList<>();
            for (ModelStateErrors error : modelStateErrors) {
                if (!TextUtils.isEmpty(error.getMessage()))
                    errorList.add(error.getMessage());
            }
            if (errorList.size() > 0) {
                showInfoDialog(getString(R.string.error), errorList);
                return;
            }
        }
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        if (getActivity() != null) {
            InfoListDialog infoListDialog = new InfoListDialog(getActivity());
            infoListDialog.errorMsg = errorMsgList;
            infoListDialog.title = title;
            infoListDialog.show();
        }
    }
    @Override
    public void onCommunications(int position, SelectedDeltaNetEstate object) {
        if (!TextUtils.isEmpty(object.getMobile())) {
//            if (TextUtils.isEmpty(Validation.getMobileError(object.getMobile(), true, getResources()))) {
            ComminucationDialog comminucationDialog = new ComminucationDialog(getActivity());
            comminucationDialog.setName(object.getFullName());
            comminucationDialog.setMobile(object.getMobile());
            comminucationDialog.setIsOfficeActive(false);
            comminucationDialog.setUserAgencyName("");
            comminucationDialog.setLogoSrc("");
            comminucationDialog.show();
        }
//        else {
//                Toast.makeText(getActivity(), "شماره تماس اشتباه است", Toast.LENGTH_SHORT).show();
//            }
//        }
        else {
            Toast.makeText(getActivity(), getResources().getString(R.string.not_find_mobile), Toast.LENGTH_SHORT).show();
        }
    }

    private void removeFromList(int id) {
        for (SelectedDeltaNetEstate estate : list) {
            if (estate.getDataId() == id) {
                list.remove(estate);
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (list == null || list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }
    }


    private void showSuccessToast(String message, String defualt) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), defualt, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRefreshList() {
        endOfList = false;
        list.clear();
        getList();
    }
}
