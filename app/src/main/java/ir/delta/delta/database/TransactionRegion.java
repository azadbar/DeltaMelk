package ir.delta.delta.database;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.Region;

public class TransactionRegion extends BaseTransaction {

    private static TransactionRegion transactionRegion;


    public static TransactionRegion getInstance() {
        if (transactionRegion == null) {
            transactionRegion = new TransactionRegion();
        }
        return transactionRegion;
    }

    private TransactionRegion() {
        super("REGION");
    }


    public ArrayList<Region> getRegions(Context context, ArrayList<Integer> regions) {
        ArrayList<Region> list = new ArrayList<>();
        String strList = TextUtils.join(",", regions);
        String query = "SELECT * FROM " + tableName + " WHERE id in (" + strList + ")";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getRegion(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return list;
    }

    public ArrayList<Region> getRegionsBy(Context context, int areaId) {
        ArrayList<Region> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE parentId = " + areaId + " ORDER BY name";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getRegion(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public Region getRegionBy(Context context, int regionId) {
        Region region = null;
        String query = "SELECT * FROM " + tableName + " WHERE id = " + regionId;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                region = getRegion(cursor);
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return region;
    }

    public int getRegionsCount(Context context, int areaId) {
        int count = 0;
        String query = "SELECT Count(*) AS count from " + tableName + " WHERE parentId ='" + areaId + "'";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            count = cursor.getInt(cursor.getColumnIndex("count"));
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return count;
    }

    private Region getRegion(Cursor cursor) {
        Region item = new Region();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setOrder(cursor.getInt(cursor.getColumnIndex("orderIndex")));
        item.setParentId(cursor.getInt(cursor.getColumnIndex("parentId")));
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        item.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        item.setLocationFeatureLocalId(cursor.getInt(cursor.getColumnIndex("locationFeatureLocalId")));
        return item;
    }


}
