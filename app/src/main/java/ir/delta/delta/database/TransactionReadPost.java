package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ir.delta.delta.service.ResponseModel.Estate;
import ir.delta.delta.service.ResponseModel.mag.ReadPost;
import ir.delta.delta.service.ResponseModel.profile.User;

public class TransactionReadPost extends BaseTransaction {

    private static TransactionReadPost readPost;


    public static TransactionReadPost getInstance() {
        if (readPost == null) {
            readPost = new TransactionReadPost();
        }
        return readPost;
    }

    private TransactionReadPost() {
        super("READ_POST");
    }


    public void saveMagPost(Context context, int id) {
        ContentValues values = new ContentValues();
        values.put("id", id);
        insert(context, "READ_POST", values);

    }

    public ArrayList<Integer> getReadPostsId(Context context) {
        ArrayList<Integer> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(cursor.getColumnIndex("id")));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }




}
