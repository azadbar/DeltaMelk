package ir.delta.delta.database;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.City;

public class TransactionCity extends BaseTransaction {

    private static TransactionCity transactionCity;

    public static TransactionCity getInstance() {
        if (transactionCity == null) {
            transactionCity = new TransactionCity();
        }
        return transactionCity;
    }

    private TransactionCity() {
        super("CITY");
    }

    public City getCityById(Context context, int id) {
        City item = null;
        String query = "SELECT * FROM " + tableName + " WHERE id =" + id;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getCity(cursor);
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return item;
    }

    public ArrayList<City> getMainCities(Context context) {
        ArrayList<City> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE isMainCity = 1 ORDER BY orderIndex , parentId";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getCity(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public ArrayList<City> getCoastalCities(Context context) {
        ArrayList<City> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE isCoastalCity = 1 ORDER BY orderIndex , parentId";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getCity(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public ArrayList<City> getCitiesBy(Context context,int stateId) {
        ArrayList<City> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE parentId = " + stateId + " ORDER BY orderIndex , parentId";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getCity(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    private City getCity(Cursor cursor) {
        City item = new City();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setOrder(cursor.getInt(cursor.getColumnIndex("orderIndex")));
        item.setParentId(cursor.getInt(cursor.getColumnIndex("parentId")));
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        item.setMainCity(cursor.getInt(cursor.getColumnIndex("isMainCity")) == 1);
        item.setCoastalCity(cursor.getInt(cursor.getColumnIndex("isCoastalCity")) == 1);
        item.setLocationFeatureLocalId(cursor.getInt(cursor.getColumnIndex("locationFeatureLocalId")));
        return item;
    }

}
