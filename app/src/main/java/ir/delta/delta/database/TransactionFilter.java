package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import ir.delta.delta.Model.Filter;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class TransactionFilter extends BaseTransaction {

    private static final int LIMIT_COUNT = 20;
    private static TransactionFilter transactionFilter;

    public static TransactionFilter getInstance() {
        if (transactionFilter == null) {
            transactionFilter = new TransactionFilter();
        }
        return transactionFilter;
    }

    private TransactionFilter() {
        super("FILTER");
    }


    public void saveFilter(Context context, Filter filter) {
        deleteLimit(context, filter.getCityId());
        ContentValues values = new ContentValues();
        Gson gson = new Gson();
        values.put("cityId", filter.getCityId());
        values.put("filterId", UUID.randomUUID().toString());
        values.put("areaList", gson.toJson(filter.getAreaIdList()));
        values.put("regionList", gson.toJson(filter.getRegionListFilter()));
        values.put("contractTypeLocalId", filter.getContractTypeLocalId());
        values.put("propertyTypeLocalId", filter.getPropertyTypeLocalId());
        values.put("minMortgageOrTotalId", filter.getMinMortgageOrTotalId());
        values.put("maxMortgageOrTotalId", filter.getMaxMortgageOrTotalId());
        values.put("minRentOrMetricId", filter.getMinRentOrMetricId());
        values.put("maxRentOrMetricId", filter.getMaxRentOrMetricId());
        values.put("minPropertyAreaId", filter.getMinPropertyAreaId());
        values.put("maxPropertyAreaId", filter.getMaxPropertyAreaId());
        values.put("minPropertyTotalAreaId", filter.getMinPropertyTotalAreaId());
        values.put("maxPropertyTotalAreaId", filter.getMaxPropertyTotalAreaId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        values.put("date", simpleDateFormat.format(new Date()));
        insert(context, tableName, values);
    }


    public Filter getLastFilter(Context context, int cityId) {
        Filter item = null;
        String query = "SELECT * FROM " + tableName + " WHERE cityId  = " + cityId + " order by date Desc";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getFilter(cursor, new Gson());
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return item;
    }


    public Filter getSameFilter(Context context, Filter filter) {
        Filter selectedFilter = null;
        String whereText = "cityId = " + filter.getCityId() +
                " AND contractTypeLocalId = " + filter.getContractTypeLocalId() +
                " AND propertyTypeLocalId = " + filter.getPropertyTypeLocalId() +
                " AND minMortgageOrTotalId = " + filter.getMinMortgageOrTotalId() +
                " AND maxMortgageOrTotalId = " + filter.getMaxMortgageOrTotalId() +
                " AND minRentOrMetricId = " + filter.getMinRentOrMetricId() +
                " AND maxRentOrMetricId = " + filter.getMaxRentOrMetricId() +
                " AND minPropertyTotalAreaId = " + filter.getMinPropertyTotalAreaId() +
                " AND maxPropertyTotalAreaId = " + filter.getMaxPropertyTotalAreaId() +
                " AND minPropertyAreaId = " + filter.getMinPropertyAreaId() +
                " AND maxPropertyAreaId = " + filter.getMaxPropertyAreaId();
        String query = "SELECT * FROM " + tableName + " WHERE " + whereText + " ORDER BY date Desc";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            Gson gson = new Gson();
            int locationFeatureLocalId = Constants.getCity(context).getLocationFeatureLocalId();
            do {
                Filter f = getFilter(cursor, gson);
                ContractObject c = Constants.getContractObjects(context).getContract(f.getContractTypeLocalId(), locationFeatureLocalId);
                if (c != null) {
                    f.setContractTypeEnum(c.getType());
                    f.setPropertyType(c.getPropertyTypeBy(f.getPropertyTypeLocalId()));
                }
                if (isSameFilter(filter, f)) {
                    if (f.getAreaIdList() != null && f.getAreaIdList().size() > 0) {
                        ArrayList<String> areaNameList = TransactionArea.getInstance().getAreaNames(context, f.getAreaIdList());
                        f.setAreaNameList(areaNameList);
                    }
                    selectedFilter = f;
                    break;
                }
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return selectedFilter;
    }

    public ArrayList<Filter> getListOfFilters(Context context, int cityId) {
        ArrayList<Filter> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE cityId  = " + cityId + " order by date Desc";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            Gson gson = new Gson();
            do {
                Filter filter = getFilter(cursor, gson);
                ContractObject c = Constants.getContractObjects(context).getContract(filter.getContractTypeLocalId(), Constants.getCity(context).getLocationFeatureLocalId());
                if (c != null) {
                    filter.setContractTypeEnum(c.getType());
                    filter.setPropertyType(c.getPropertyTypeBy(filter.getPropertyTypeLocalId()));
                }
                if (filter.getAreaIdList() != null && filter.getAreaIdList().size() > 0) {
                    ArrayList<String> areaNameList = TransactionArea.getInstance().getAreaNames(context, filter.getAreaIdList());
                    filter.setAreaNameList(areaNameList);
                }
                list.add(filter);
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public void updateFilter(Context context, Filter filter, String filterId) {
        ContentValues values = new ContentValues();
        Gson gson = new Gson();
        values.put("cityId", filter.getCityId());
        values.put("areaList", gson.toJson(filter.getAreaIdList()));
        values.put("regionList", gson.toJson(filter.getRegionListFilter()));
        values.put("contractTypeLocalId", filter.getContractTypeLocalId());
        values.put("propertyTypeLocalId", filter.getPropertyTypeLocalId());
        values.put("minMortgageOrTotalId", filter.getMinMortgageOrTotalId());
        values.put("maxMortgageOrTotalId", filter.getMaxMortgageOrTotalId());
        values.put("minRentOrMetricId", filter.getMinRentOrMetricId());
        values.put("maxRentOrMetricId", filter.getMaxRentOrMetricId());
        values.put("minPropertyAreaId", filter.getMinPropertyAreaId());
        values.put("maxPropertyAreaId", filter.getMaxPropertyAreaId());
        values.put("minPropertyTotalAreaId", filter.getMinPropertyTotalAreaId());
        values.put("maxPropertyTotalAreaId", filter.getMaxPropertyTotalAreaId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        values.put("date", simpleDateFormat.format(new Date()));
        update(context, tableName, values, "filterId = \"" + filterId + "\"");
    }

    public void updateFilterDate(Context context, String filterId) {
        ContentValues values = new ContentValues();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        values.put("date", simpleDateFormat.format(new Date()));
        update(context, tableName, values, "filterId = \"" + filterId + "\"");
    }

    public void updateFilterTable(Context context) {
        ArrayList<Filter> filters = getAllFilters(context);
        try {
            getDB(context).beginTransaction();
            SimpleDateFormat oldFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            ContentValues values = new ContentValues();
            for (Filter f : filters) {
                String oldDateString = f.getDate();
                String newDateString;
                try {
                    Date date = oldFormat.parse(oldDateString);
                    newDateString = newFormat.format(date);

                } catch (ParseException e) {
                    newDateString = newFormat.format(new Date());
                }
                values.clear();
                values.put("date", newDateString);
                update(context, tableName, values, "filterId = \"" + f.getFilterId() + "\"");
            }
            PreferencesData.setFilterTableUpdate(context);
            getDB(context).setTransactionSuccessful();
        } finally {
            getDB(context).endTransaction();
        }


    }

    private ArrayList<Filter> getAllFilters(Context context) {
        ArrayList<Filter> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " order by date Desc";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            Gson gson = new Gson();
            do {
                Filter filter = getFilter(cursor, gson);
                ContractObject c = Constants.getContractObjects(context).getContract(filter.getContractTypeLocalId(), Constants.getCity(context).getLocationFeatureLocalId());
                if (c != null) {
                    filter.setContractTypeEnum(c.getType());
                    filter.setPropertyType(c.getPropertyTypeBy(filter.getPropertyTypeLocalId()));
                }
                if (filter.getAreaIdList() != null && filter.getAreaIdList().size() > 0) {
                    ArrayList<String> areaNameList = TransactionArea.getInstance().getAreaNames(context, filter.getAreaIdList());
                    filter.setAreaNameList(areaNameList);
                }
                list.add(filter);
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    private Filter getFilter(Cursor cursor, Gson gson) {
        Filter item = new Filter();
        item.setCityId(cursor.getInt(cursor.getColumnIndex("cityId")));
        item.setFilterId(cursor.getString(cursor.getColumnIndex("filterId")));
        String json = (cursor.getString(cursor.getColumnIndex("areaList")));
        if (json != null) {
            item.setAreaIdList(gson.fromJson(json, new TypeToken<List<Integer>>() {
            }.getType()));
        }
        json = (cursor.getString(cursor.getColumnIndex("regionList")));
        if (json != null) {
            item.setRegionListFilter(gson.fromJson(json, new TypeToken<List<Integer>>() {
            }.getType()));
        }
        item.setContractTypeLocalId(cursor.getInt(cursor.getColumnIndex("contractTypeLocalId")));
        item.setPropertyTypeLocalId(cursor.getInt(cursor.getColumnIndex("propertyTypeLocalId")));
        item.setMinMortgageOrTotalId(cursor.getInt(cursor.getColumnIndex("minMortgageOrTotalId")));
        item.setMaxMortgageOrTotalId(cursor.getInt(cursor.getColumnIndex("maxMortgageOrTotalId")));
        item.setMinRentOrMetricId(cursor.getInt(cursor.getColumnIndex("minRentOrMetricId")));
        item.setMaxRentOrMetricId(cursor.getInt(cursor.getColumnIndex("maxRentOrMetricId")));
        item.setMinPropertyAreaId(cursor.getInt(cursor.getColumnIndex("minPropertyAreaId")));
        item.setMaxPropertyAreaId(cursor.getInt(cursor.getColumnIndex("maxPropertyAreaId")));
        item.setMinPropertyTotalAreaId(cursor.getInt(cursor.getColumnIndex("minPropertyTotalAreaId")));
        item.setMaxPropertyTotalAreaId(cursor.getInt(cursor.getColumnIndex("maxPropertyTotalAreaId")));
        item.setDate(cursor.getString(cursor.getColumnIndex("date")));
        return item;
    }

    private void deleteLimit(Context context, int cityId) {
        String where = " cityId = " + cityId + " AND filterId NOT IN ( SELECT filterId FROM  " + tableName + " order by date desc limit " + LIMIT_COUNT + " )";
        delete(context, tableName, where);
    }

    private boolean isSameFilter(Filter f1, Filter f2) {
        return equalLists(f1.getAreaIdList(), f2.getAreaIdList()) && equalLists(f1.getRegionListFilter(), f2.getRegionListFilter());
    }

    private boolean equalLists(ArrayList<Integer> list1, ArrayList<Integer> list2){
        if(list1.size() != list2.size()) {
            return false;
        }
        Collections.sort(list1);
        Collections.sort(list2);
        return list1.equals(list2);
    }
}
