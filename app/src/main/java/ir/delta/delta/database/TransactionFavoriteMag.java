package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ir.delta.delta.service.ResponseModel.Province;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class TransactionFavoriteMag extends BaseTransaction {

    private static TransactionFavoriteMag favoriteMag;


    public static TransactionFavoriteMag getInstance() {
        if (favoriteMag == null) {
            favoriteMag = new TransactionFavoriteMag();
        }
        return favoriteMag;
    }

    private TransactionFavoriteMag() {
        super("FAVORITE_MAG_ITEM");
    }


    public void saveMagPost(Context context, MagPost magPost) {
        ContentValues values = new ContentValues();
        values.put("id", magPost.getId());
        values.put("title", magPost.getTitle());
        values.put("image", magPost.getImage());
        values.put("author", magPost.getAuthor());
        values.put("term_name", magPost.getTerm_name());
        values.put("termId", magPost.getTermId());
        values.put("commentStatus", magPost.getCommentStatus());
        values.put("viewCount", magPost.getViewCount());
        values.put("date", magPost.getDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String date = simpleDateFormat.format(new Date());
        values.put("dateSort", date);
        if (tableIsExist(context, magPost.getId())) {
            update(context, tableName, values, "id = " + magPost.getId());
        } else {
            insert(context, "FAVORITE_MAG_ITEM", values);
        }

    }

    private boolean tableIsExist(Context context, int id) {
        boolean isExist = false;
        String query = "SELECT * FROM " + tableName + " WHERE id = " + id;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.getCount() > 0) {
            isExist = true;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return isExist;
    }

    public ArrayList<Integer> getReadPostsId(Context context) {
        ArrayList<Integer> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(cursor.getColumnIndex("id")));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public ArrayList<MagPost> getFavoriteMag(Context context) {
        ArrayList<MagPost> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " ORDER BY dateSort DESC";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getMag(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    private MagPost getMag(Cursor cursor) {
        MagPost item = new MagPost();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setTitle(cursor.getString(cursor.getColumnIndex("title")));
        item.setDate(cursor.getString(cursor.getColumnIndex("date")));
        item.setImage(cursor.getString(cursor.getColumnIndex("image")));
        item.setAuthor(cursor.getString(cursor.getColumnIndex("author")));
        item.setTerm_name(cursor.getString(cursor.getColumnIndex("term_name")));
        item.setTermId(cursor.getString(cursor.getColumnIndex("termId")));
        item.setCommentStatus(cursor.getString(cursor.getColumnIndex("commentStatus")));
        item.setViewCount(cursor.getString(cursor.getColumnIndex("viewCount")));
        item.setDateSort(cursor.getString(cursor.getColumnIndex("dateSort")));
        return item;
    }

    public void delete(Context context, int favoriteId) {
        delete(context, tableName, " id = " + favoriteId);
    }

}
