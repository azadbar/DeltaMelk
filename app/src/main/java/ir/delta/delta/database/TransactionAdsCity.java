package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.profile.User;

public class TransactionAdsCity extends BaseTransaction {

    private static TransactionAdsCity transactionAdsCity;


    public static TransactionAdsCity getInstance() {
        if (transactionAdsCity == null) {
            transactionAdsCity = new TransactionAdsCity();
        }
        return transactionAdsCity;
    }

    private TransactionAdsCity() {
        super("CITY_ADS");
    }

    public void saveCityAdsTable(Context context, List<City> itemList) {
        for (City item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("title", item.getTitle());
            values.put("longitude", item.getLongitude());
            values.put("latitude", item.getLatitude());
            insert(context, "CITY_ADS", values);
        }
    }

    public City getCityById(Context context, int id) {
        City item = null;
        String query = "SELECT * FROM " + tableName + " WHERE id =" + id;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getCity(cursor);
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return item;
    }

    public ArrayList<City> getCitiesBy(Context context,int stateId) {
        ArrayList<City> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE parentId = " + stateId + " ORDER BY orderIndex , parentId";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getCity(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    private City getCity(Cursor cursor) {
        City item = new City();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setOrder(cursor.getInt(cursor.getColumnIndex("orderIndex")));
        item.setParentId(cursor.getInt(cursor.getColumnIndex("parentId")));
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        item.setMainCity(cursor.getInt(cursor.getColumnIndex("isMainCity")) == 1);
        item.setCoastalCity(cursor.getInt(cursor.getColumnIndex("isCoastalCity")) == 1);
        item.setLocationFeatureLocalId(cursor.getInt(cursor.getColumnIndex("locationFeatureLocalId")));
        item.setTitle(cursor.getString(cursor.getColumnIndex("title")));
        item.setLongitude(cursor.getDouble(cursor.getColumnIndex("longitude")));
        item.setLatitude(cursor.getDouble(cursor.getColumnIndex("latitude")));
        return item;

    }

}
