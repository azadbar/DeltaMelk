package ir.delta.delta.database;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.Area;

public class TransactionArea extends BaseTransaction {

    private static TransactionArea transactionArea;

    public static TransactionArea getInstance() {
        if (transactionArea == null) {
            transactionArea = new TransactionArea();
        }
        return transactionArea;
    }

    private TransactionArea() {
        super("AREA");
    }


    public ArrayList<String> getAreaNames(Context context, ArrayList<Integer> listId) {
        ArrayList<String> list = new ArrayList<>();
        String strList = TextUtils.join(",", listId);
        String query = "SELECT * FROM " + tableName + " WHERE id in (" + strList + ")";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(cursor.getString(cursor.getColumnIndex("name")));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return list;
    }

    public ArrayList<Area> getAreasBy(Context context,int cityId) {
        ArrayList<Area> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " WHERE parentId = " + cityId + " ORDER BY orderIndex";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getArea(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public ArrayList<Area> getAreas(Context context,ArrayList<Integer> areaIdList) {
        ArrayList<Area> list = new ArrayList<>();
        String strList = TextUtils.join(",", areaIdList);
        String query = "SELECT * FROM " + tableName + " WHERE id in (" + strList + ")";
        Cursor cursor = rawQuery(context,query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getArea(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return list;
    }

    public int getAreasCount(Context context,int cityId) {
        int count = 0;
        String query = "SELECT Count(*) AS count from " + tableName + " WHERE parentId ='" + cityId + "'";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            count = cursor.getInt(cursor.getColumnIndex("count"));
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return count;
    }


    public Area getAreaById(Context context, int id) {
        Area item = null;
        String query = "SELECT * FROM " + tableName + " WHERE id  = " + id;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getArea(cursor);
        if (cursor != null && !cursor.isClosed())
            cursor.close();
        return item;
    }

    public boolean hasArea(Context context, int cityId) {
        boolean hasArea = false;
        String query = "SELECT Count(*) AS count FROM " + tableName + " WHERE parentId  = " + cityId;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            hasArea = cursor.getInt(cursor.getColumnIndex("count")) > 0;
        if (cursor != null && !cursor.isClosed())
            cursor.close();

        return hasArea;
    }

    private Area getArea(Cursor cursor) {
        Area item = new Area();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setOrder(cursor.getInt(cursor.getColumnIndex("orderIndex")));
        item.setParentId(cursor.getInt(cursor.getColumnIndex("parentId")));
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        item.setDescription(cursor.getString(cursor.getColumnIndex("description")));
        item.setLocationFeatureLocalId(cursor.getInt(cursor.getColumnIndex("locationFeatureLocalId")));
        return item;
    }

}
