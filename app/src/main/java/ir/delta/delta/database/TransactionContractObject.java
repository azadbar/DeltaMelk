package ir.delta.delta.database;

import android.content.Context;
import android.database.Cursor;

import com.google.gson.Gson;

import ir.delta.delta.Model.ContractObjectList;

public class TransactionContractObject extends BaseTransaction {

    private static TransactionContractObject transactionContractObject;

    public static TransactionContractObject getInstance() {
        if (transactionContractObject == null) {
            transactionContractObject = new TransactionContractObject();
        }
        return transactionContractObject;
    }

    private TransactionContractObject() {
        super("CONTRACT_OBJECT");
    }


    public ContractObjectList getContractObject(Context context) {
        ContractObjectList list = new ContractObjectList();
        String query = "SELECT contractObjectJson FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            String json = (cursor.getString(cursor.getColumnIndex("contractObjectJson")));
            if (json != null) {
                list = new Gson().fromJson(json, ContractObjectList.class);
            }
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }


    public int getCacheVersion(Context context) {
        int version = 0;
        String query = "SELECT cacheVersion FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            version = (cursor.getInt(cursor.getColumnIndex("cacheVersion")));

        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return version;
    }
}
