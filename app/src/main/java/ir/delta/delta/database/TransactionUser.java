package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import androidx.fragment.app.FragmentActivity;

import ir.delta.delta.service.ResponseModel.profile.User;

public class TransactionUser extends BaseTransaction {

    private static TransactionUser transactionUser;


    public static TransactionUser getInstance() {
        if (transactionUser == null) {
            transactionUser = new TransactionUser();
        }
        return transactionUser;
    }

    private TransactionUser() {
        super("USER");
    }


    public void save(Context context, User user) {

        ContentValues values = new ContentValues();
        values.put("isGeneral", user.isGeneral() ? 1 : 0);
        values.put("userId", user.getUserId());
        values.put("userToken", user.getUserToken());
        values.put("activityLocation", user.getActivityLocation());
        values.put("displayName", user.getDisplayName());
        values.put("officeTitle", user.getOfficeTitle());
        values.put("mobile", user.getMobile());
        values.put("officeLogo", user.getOfficeLogo());
        values.put("imagePath", user.getImagePath());
        values.put("showDeltaNetMenu", user.isShowDeltaNetMenu() ? 1 : 0);
        values.put("locationId", user.getLocationId());
        values.put("lastName", user.getLastName());
        insert(context, tableName, values);
    }


    public User getLastUser(Context context) {
        User item = null;
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getUser(cursor);
        if (cursor != null && !cursor.isClosed())
            cursor.close();

        return item;
    }


    public void deleteUsr(Context context) {
        delete(context, tableName, null);
    }

    public void updateUserImage(Context context, String userImage) {
        ContentValues values = new ContentValues();
        values.put("imagePath", userImage);
        update(context, tableName, values, null);
    }

    public void updateUserName(Context context, String name) {
        ContentValues values = new ContentValues();
        values.put("displayName", name);
        update(context, tableName, values, null);
    }

    public void updateLocationIdUser(Context context, int loactionId) {
        ContentValues values = new ContentValues();
        values.put("locationId", loactionId);
        update(context, tableName, values, null);
    }

    private User getUser(Cursor cursor) {
        User item = new User();
        item.setGeneral(cursor.getInt(cursor.getColumnIndex("isGeneral")) == 1);
        item.setUserId(cursor.getInt(cursor.getColumnIndex("userId")));
        item.setUserToken(cursor.getString(cursor.getColumnIndex("userToken")));
        item.setActivityLocation(cursor.getString(cursor.getColumnIndex("activityLocation")));
        item.setDisplayName(cursor.getString(cursor.getColumnIndex("displayName")));
        item.setOfficeTitle(cursor.getString(cursor.getColumnIndex("officeTitle")));
        item.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
        item.setOfficeLogo(cursor.getString(cursor.getColumnIndex("officeLogo")));
        item.setImagePath(cursor.getString(cursor.getColumnIndex("imagePath")));
        item.setShowDeltaNetMenu(cursor.getInt(cursor.getColumnIndex("showDeltaNetMenu")) == 1);
        item.setLocationId(cursor.getInt(cursor.getColumnIndex("locationId")));
        item.setLastName(cursor.getString(cursor.getColumnIndex("lastName")));
        return item;
    }


}
