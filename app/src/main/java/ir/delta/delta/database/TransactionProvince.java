package ir.delta.delta.database;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.Province;

public class TransactionProvince extends BaseTransaction {

    private static TransactionProvince transactionState;

    public static TransactionProvince getInstance() {
        if (transactionState == null) {
            transactionState = new TransactionProvince();
        }
        return transactionState;
    }


    private TransactionProvince() {
        super("PROVINCE");
    }


    public ArrayList<Province> getStates(Context context) {
        ArrayList<Province> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " ORDER BY orderIndex";
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getState(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }


    private Province getState(Cursor cursor) {
        Province item = new Province();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setOrder(cursor.getInt(cursor.getColumnIndex("orderIndex")));
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        return item;
    }
}
