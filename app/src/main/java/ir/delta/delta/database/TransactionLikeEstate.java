package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import ir.delta.delta.service.ResponseModel.Estate;

public class TransactionLikeEstate extends BaseTransaction {

    private static TransactionLikeEstate transactionLikeEstate;


    public static TransactionLikeEstate getInstance() {
        if (transactionLikeEstate == null) {
            transactionLikeEstate = new TransactionLikeEstate();
        }
        return transactionLikeEstate;
    }

    private TransactionLikeEstate() {
        super("LIKEESTATE");
    }


    public void saveLikeItem(Context context, Estate estate) {
        ContentValues values = new ContentValues();
        values.put("id", estate.getId());
        values.put("encodedId", estate.getEncodedId());
        values.put("srcImage", estate.getSrcImage());
        values.put("imageCount", estate.getImageCount());
        values.put("showOfficeLogo", estate.isShowOfficeLogo());
        values.put("logoSrc", estate.getLogoSrc());
        values.put("propertyType", estate.getPropertyType());
        values.put("contractType", estate.getContractType());
        values.put("displayText", estate.getDisplayText());
        values.put("displayTitle", estate.getDisplayTitle());
        values.put("rentOrMetric", estate.getRentOrMetric());
        values.put("rentOrMetricDisplayTitle", estate.getRentOrMetricDisplayTitle());
        values.put("mortgageOrTotal", estate.getMortgageOrTotal());
        values.put("mortgageOrTotalDisplayTitle", estate.getMortgageOrTotalDisplayTitle());
        values.put("address", estate.getAddress());
        values.put("location", estate.getLocation());
        values.put("area", estate.getArea());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String date = simpleDateFormat.format(new Date());
        values.put("date", date);
        if (tableIsExist(context, estate.getId())) {
            update(context, tableName, values, "id = " + estate.getId());
        } else {
            insert(context, "LIKEESTATE", values);
        }
    }


    public ArrayList<Estate> getEstateList(Context context, int from, int offset) {
        ArrayList<Estate> list = new ArrayList<>();
        String query = "SELECT * FROM " + tableName + " ORDER BY date DESC limit " + from + " , " + offset;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(getEstate(cursor));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    public void delete(Context context, int estateId) {
        delete(context, tableName, " id = " + estateId);
    }

    public ArrayList<Integer> getEstateIdList(Context context) {
        ArrayList<Integer> list = new ArrayList<>();
        String query = "SELECT id FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(cursor.getColumnIndex("id")));
            }
            while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return list;
    }

    private boolean tableIsExist(Context context, int id) {
        boolean isExist = false;
        String query = "SELECT * FROM " + tableName + " WHERE id = " + id;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.getCount() > 0) {
            isExist = true;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return isExist;
    }

    private Estate getEstate(Cursor cursor) {
        Estate item = new Estate();
        item.setId(cursor.getInt(cursor.getColumnIndex("id")));
        item.setEncodedId(cursor.getString(cursor.getColumnIndex("encodedId")));
        item.setSrcImage(cursor.getString(cursor.getColumnIndex("srcImage")));
        item.setImageCount(cursor.getInt(cursor.getColumnIndex("imageCount")));
        item.setShowOfficeLogo(cursor.getInt(cursor.getColumnIndex("showOfficeLogo")) == 1);
        item.setLogoSrc(cursor.getString(cursor.getColumnIndex("logoSrc")));
        item.setPropertyType(cursor.getString(cursor.getColumnIndex("propertyType")));
        item.setContractType(cursor.getString(cursor.getColumnIndex("contractType")));
        item.setDisplayText(cursor.getString(cursor.getColumnIndex("displayText")));
        item.setDisplayTitle(cursor.getString(cursor.getColumnIndex("displayTitle")));
        item.setRentOrMetric(cursor.getLong(cursor.getColumnIndex("rentOrMetric")));
        item.setRentOrMetricDisplayTitle(cursor.getString(cursor.getColumnIndex("rentOrMetricDisplayTitle")));
        item.setMortgageOrTotal(cursor.getLong(cursor.getColumnIndex("mortgageOrTotal")));
        item.setMortgageOrTotalDisplayTitle(cursor.getString(cursor.getColumnIndex("mortgageOrTotalDisplayTitle")));
        item.setAddress(cursor.getString(cursor.getColumnIndex("address")));
        item.setLocation(cursor.getString(cursor.getColumnIndex("location")));
        item.setArea(cursor.getString(cursor.getColumnIndex("area")));
        return item;
    }
}
