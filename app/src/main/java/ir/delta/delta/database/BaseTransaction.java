package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import ir.delta.delta.Model.ContractObjectList;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.Location;
import ir.delta.delta.service.ResponseModel.Province;
import ir.delta.delta.service.ResponseModel.Region;
import ir.delta.delta.util.Constants;

/**
 * Created by m.azadbar on 07/29/2018.
 */
public class BaseTransaction {

    String tableName;
    private static SQLiteDatabase dbDeltaMelk;

    public BaseTransaction(String tableName) {
        this.tableName = tableName;
    }

    public static SQLiteDatabase getDB(Context context) {
        if (dbDeltaMelk == null || !dbDeltaMelk.isOpen()) {
            DatabaseOpenHelper db1 = new DatabaseOpenHelper(context, context.getFilesDir().getAbsolutePath() + "/DELTA_MELK.db3", Constants.DATABASE_VERSION);
            dbDeltaMelk = db1.getWritableDatabase();
        }
        return dbDeltaMelk;
    }

    public static void openDb(Context context) {
        if (dbDeltaMelk == null || !dbDeltaMelk.isOpen()) {
            DatabaseOpenHelper db1 = new DatabaseOpenHelper(context, context.getFilesDir().getAbsolutePath() + "/DELTA_MELK.db3",  Constants.DATABASE_VERSION);
            dbDeltaMelk = db1.getWritableDatabase();
        }
    }

    public static void closeDb() {
        if (dbDeltaMelk != null && dbDeltaMelk.isOpen()) {
            dbDeltaMelk.close();
        }
        dbDeltaMelk = null;
    }
    public static boolean isDbOpen() {
        return dbDeltaMelk != null && dbDeltaMelk.isOpen();
    }

    Cursor rawQuery(Context context, String sql) {
        try {
            return getDB(context).rawQuery(sql, null);
        } catch (Exception e) {
            return null;
        }
    }


    void insert(Context context, String table, ContentValues values) {
        try {
            getDB(context).insert(table, null, values);
        } catch (Exception ignored) {
            Log.e("ignored",ignored.getMessage());
        }
    }


    void update(Context context, String table, ContentValues values, String whereClause) {
        try {
            getDB(context).update(table, values, whereClause, null);
        } catch (Exception ignored) {

        }
    }

    void delete(Context context, String table, String whereClause) {
        try {
            getDB(context).delete(table, whereClause, null);
        } catch (Exception ignored) {
        }
    }

    public void updateTable(Context context, Location location, int cacheVersion) {
        if (location != null) {
            try {
                getDB(context).beginTransaction();
                if (location.getProvinces().size() > 0) {
                    updateStateTable(context,location.getProvinces());
                }
                if (location.getCities().size() > 0) {
                    updateCityTable(context,location.getCities());
                }
                if (location.getAreas().size() > 0) {
                    updateAreaTable(context,location.getAreas());
                }
                if (location.getRegions().size() > 0) {
                    updateRegionTable(context,location.getRegions());
                }
                if (location.getContractObjects().size() > 0) {
                    updateContractTypeObjectTable(context, location.getContractObjects(), cacheVersion);
                }
                getDB(context).setTransactionSuccessful();
            } finally {
                getDB(context).endTransaction();
            }
        }
    }

    private void updateContractTypeObjectTable(Context context, ContractObjectList contractObjects, int cacheVersion) {
        delete(context, "CONTRACT_OBJECT", null);
        ContentValues values = new ContentValues();
        Gson gson = new Gson();
        String contractJson = gson.toJson(contractObjects);
        values.put("contractObjectJson", contractJson);
        values.put("cacheVersion", cacheVersion);
        insert(context, "CONTRACT_OBJECT", values);
    }


    private void updateStateTable(Context context, List<Province> itemList) {
        delete(context, "PROVINCE", null);
        for (Province item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("orderIndex", item.getOrder());
            values.put("name", item.getName());
            insert(context, "PROVINCE", values);
        }
    }

    private void updateCityTable(Context context, List<City> itemList) {
        delete(context, "CITY", null);
        for (City item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("orderIndex", item.getOrder());
            values.put("parentId", item.getParentId());
            values.put("name", item.getName());
            values.put("isMainCity", item.isMainCity() ? 1 : 0);
            values.put("isCoastalCity", item.isCoastalCity() ? 1 : 0);
            values.put("locationFeatureLocalId", item.getLocationFeatureLocalId());
            insert(context, "CITY", values);
        }
    }

    private void updateAreaTable(Context context, List<Area> itemList) {
        delete(context, "AREA", null);
        for (Area item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("orderIndex", item.getOrder());
            values.put("parentId", item.getParentId());
            values.put("name", item.getName());
            values.put("description", item.getDescription());
            values.put("locationFeatureLocalId", item.getLocationFeatureLocalId());
            insert(context, "AREA", values);
        }
    }

    private void updateRegionTable(Context context, List<Region> itemList) {
        delete(context, "REGION", null);
        for (Region item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("orderIndex", item.getOrder());
            values.put("parentId", item.getParentId());
            values.put("name", item.getName());
            values.put("description", item.getDescription());
            values.put("locationFeatureLocalId", item.getLocationFeatureLocalId());
            insert(context, "REGION", values);
        }
    }

    public void updateCityAdsTable(Context context, List<City> itemList) {
        delete(context, "CITY_ADS", null);
        for (City item : itemList) {
            ContentValues values = new ContentValues();
            values.put("id", item.getId());
            values.put("orderIndex", item.getOrder());
            values.put("parentId", item.getParentId());
            values.put("name", item.getName());
            values.put("isMainCity", item.isMainCity() ? 1 : 0);
            values.put("isCoastalCity", item.isCoastalCity() ? 1 : 0);
            values.put("locationFeatureLocalId", item.getLocationFeatureLocalId());

            values.put("title", item.getTitle());
            values.put("longitude", item.getLongitude());
            values.put("latitude", item.getLatitude());
            insert(context, "CITY_ADS", values);
        }
    }


}
