package ir.delta.delta.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import ir.delta.delta.service.ResponseModel.Campaign.CampaignUser;
import ir.delta.delta.service.ResponseModel.profile.User;

public class TransactionCampaignUser extends BaseTransaction {

    private static TransactionCampaignUser transactionCampaignUser;


    public static TransactionCampaignUser getInstance() {
        if (transactionCampaignUser == null) {
            transactionCampaignUser = new TransactionCampaignUser();
        }
        return transactionCampaignUser;
    }

    private TransactionCampaignUser() {
        super("CAMPAIGN_USER");
    }


    public void save(Context context, CampaignUser user) {

        ContentValues values = new ContentValues();
        values.put("name", user.getName());
        values.put("mobile", user.getMobile());
        values.put("refCode", user.getRefCode());
        values.put("friendsCount", user.getFriendsCount());
        values.put("points", user.getPoints());
        values.put("hasActiveQuiz", user.isHasActiveQuiz() ? 1 : 0);
        values.put("isNotActivated", user.isNotActivated() ? 1 : 0);
        values.put("isNotRegistered", user.isNotRegistered() ? 1 : 0);
        insert(context, tableName, values);
    }


    public CampaignUser getLastCampaignUser(Context context) {
        CampaignUser item = null;
        String query = "SELECT * FROM " + tableName;
        Cursor cursor = rawQuery(context, query);
        if (cursor != null && cursor.moveToFirst())
            item = getUser(cursor);
        if (cursor != null && !cursor.isClosed())
            cursor.close();

        return item;
    }


    public void deleteUsr(Context context) {
        delete(context, tableName, null);
    }

    private CampaignUser getUser(Cursor cursor) {
        CampaignUser item = new CampaignUser();
        item.setName(cursor.getString(cursor.getColumnIndex("name")));
        item.setMobile(cursor.getString(cursor.getColumnIndex("mobile")));
        item.setRefCode(cursor.getString(cursor.getColumnIndex("refCode")));
        item.setFriendsCount(cursor.getInt(cursor.getColumnIndex("friendsCount")));
        item.setPoints(cursor.getInt(cursor.getColumnIndex("points")));
        item.setHasActiveQuiz(cursor.getInt(cursor.getColumnIndex("hasActiveQuiz")) == 1);
        item.setNotActivated(cursor.getInt(cursor.getColumnIndex("isNotActivated")) == 1);
        item.setNotRegistered(cursor.getInt(cursor.getColumnIndex("isNotRegistered")) == 1);
        return item;
    }


}
