package ir.delta.delta.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.annotations.SerializedName;

/**
 * Created by M.azadbar on 07/28/2015.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper {

    public DatabaseOpenHelper(Context context, String dbPath, int version) {
        super(context, dbPath, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE PROVINCE (id  INTEGER, orderIndex INTEGER, name TEXT);";
        db.execSQL(query);
        query = "CREATE TABLE CITY (id  INTEGER, orderIndex INTEGER,parentId INTEGER, name TEXT, isMainCity INTEGER, isCoastalCity INTEGER, locationFeatureLocalId INTEGER);";
        db.execSQL(query);
        query = "CREATE TABLE AREA (id  INTEGER, orderIndex INTEGER,parentId INTEGER, name TEXT,description TEXT,locationFeatureLocalId INTEGER);";
        db.execSQL(query);
        query = "CREATE TABLE REGION (id  INTEGER, orderIndex INTEGER,parentId INTEGER, name TEXT,description TEXT,locationFeatureLocalId INTEGER);";
        db.execSQL(query);
        query = "CREATE TABLE CONTRACT_OBJECT (contractObjectJson TEXT, cacheVersion INTEGER);";
        db.execSQL(query);
        query = "CREATE TABLE FILTER (cityId INTEGER,filterId TEXT,areaList INTEGER, regionList INTEGER," +
                "contractTypeLocalId INTEGER, propertyTypeLocalId INTEGER, minMortgageOrTotalId INTEGER, maxMortgageOrTotalId INTEGER," +
                "minRentOrMetricId INTEGER, maxRentOrMetricId INTEGER, minPropertyAreaId INTEGER, maxPropertyAreaId INTEGER," +
                "minPropertyTotalAreaId INTEGER, maxPropertyTotalAreaId INTEGER,date TEXT);";
        db.execSQL(query);
        query = "CREATE TABLE LIKEESTATE (id INTEGER,encodedId TEXT,srcImage TEXT,imageCount INTEGER,showOfficeLogo INTEGER," +
                " logoSrc TEXT,propertyType TEXT,contractType  TEXT,displayText TEXT,displayTitle TEXT,rentOrMetric  INTEGER" +
                ", rentOrMetricDisplayTitle  TEXT, mortgageOrTotal INTEGER, mortgageOrTotalDisplayTitle TEXT, " +
                "address TEXT,location  TEXT, area TEXT,date TEXT);";
        db.execSQL(query);

        db.execSQL(getUserTable());

        db.execSQL(getReadPostTable());

        db.execSQL(getCampaignUser());

        db.execSQL(getCityAds());

        db.execSQL(getFavoriteListMag());

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion < 21) {
            db.execSQL("ALTER TABLE CONTRACT_OBJECT ADD COLUMN cacheVersion INTEGER");
        }
        if (oldVersion < 22) {
            db.execSQL(getUserTable());
        }

        if (oldVersion < 23) {
            db.execSQL(getReadPostTable());
        }

        if (oldVersion < 24) {
            db.execSQL(getCampaignUser());
        }

        if (oldVersion < 25) {
            db.execSQL(getCityAds());
        }

        if (oldVersion < 26) {
            db.execSQL(getFavoriteListMag());
        }

        if (oldVersion < 27) {
            db.execSQL("ALTER TABLE USER ADD COLUMN lastName TEXT");
        }

        if (oldVersion < 28) {
            db.execSQL("ALTER TABLE FAVORITE_MAG_ITEM ADD COLUMN dateSort TEXT");
        }

    }

    private String getCityAds() {
        return "CREATE TABLE IF NOT EXISTS CITY_ADS (id  INTEGER, orderIndex INTEGER,parentId INTEGER, name TEXT," +
                " isMainCity INTEGER, isCoastalCity INTEGER, locationFeatureLocalId INTEGER,title TEXT,longitude REAL,latitude REAL)";
    }

    private String getUserTable() {
        return "CREATE TABLE IF NOT EXISTS USER (isGeneral INTEGER, userId INTEGER, userToken TEXT, activityLocation TEXT,displayName TEXT," +
                "officeTitle TEXT, mobile TEXT, officeLogo TEXT, imagePath TEXT, showDeltaNetMenu INTEGER,locationId  INTEGER,lastName TEXT);";
    }

    private String getReadPostTable() {
        return "CREATE TABLE READ_POST (id INTEGER);";
    }

    private String getCampaignUser() {
        return "CREATE TABLE IF NOT EXISTS CAMPAIGN_USER (name TEXT, mobile TEXT, refCode TEXT, friendsCount INTEGER,points INTEGER," +
                "hasActiveQuiz INTEGER, isNotActivated INTEGER, isNotRegistered INTEGER);";
    }

    private String getFavoriteListMag() {
        return "CREATE TABLE FAVORITE_MAG_ITEM (id INTEGER, title TEXT, date TEXT, image TEXT, author TEXT,term_name TEXT," +
                "termId INTEGER,commentStatus TEXT,viewCount  INTEGER,dateSort TEXT );";
    }
}
