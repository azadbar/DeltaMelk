package ir.delta.delta.aboutDelta;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

/**
 * Created by a.azadbar on 9/27/2017.
 */

public class AboutDeltaActivity extends BaseActivity {


    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.progreesBar)
    ProgressBar progreesBar;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    private String urlSite;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_delta);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.about_delta);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);

        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            urlSite = b.getString("urlSite");
        }
        Constants.setBackgroundProgress(AboutDeltaActivity.this, progreesBar);
        waitingStart();
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                // Return the app name after finish loading
                if (progress == 100)
                    waitingStop();
            }
        });
        webview.loadUrl(urlSite);

    }


    private void waitingStop() {
        progreesBar.setVisibility(View.GONE);
        webview.setVisibility(VISIBLE);

    }

    private void waitingStart() {
        webview.setVisibility(View.GONE);
        progreesBar.setVisibility(VISIBLE);
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }
}
