package ir.delta.delta.aboutDelta;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import androidx.annotation.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.GetAboutMagTextService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.AboutMagResponse;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

/**
 * Created by a.azadbar on 9/27/2017.
 */

public class AboutMagDeltaActivity extends BaseActivity {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.textAbout)
    BaseTextView textAbout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_mag_delta);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.about_delta_mag);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);

        loadingView.setButtonClickListener(view -> getAboutMagUsRequest());
        getAboutMagUsRequest();



    }

    
    private void getAboutMagUsRequest() {
        loadingView.showLoading(false);

        GetAboutMagTextService.getInstance().getAboutMagText(getResources(), new ResponseListener<AboutMagResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
//                    Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(AboutMagDeltaActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(AboutMagResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    if (response.isSuccess())
                        textAbout.setText(response.getAboutUs());
                    else
                        textAbout.setText(getResources().getString(R.string.about_delta_mag_txt));
                }
            }
        });
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
        finish();
    }
}
