package ir.delta.delta.listener;

/**
 * Created by R.taghizadeh on 11/6/2017.
 */

public interface OnNetworkResponse {
     void onConnectionState(boolean isConnected);
}
