package ir.delta.delta.edit;

import android.content.Context;
import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.Image;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ImageType;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int cellWidth;
    private final int maxImageCount;

    private final Context context;
    private final ArrayList<Image> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;
    private final AddOriginalImage addOriginalImage;


    public ImageAdapter(Context context, ArrayList<Image> list, int cellWidth, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener, AddOriginalImage addOriginalImage) {
        this.list = list;
        this.listener = listener;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
        this.addOriginalImage = addOriginalImage;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_image, parent, false);
                itemView.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new addImageViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_estate_new, parent, false);
                itemView1.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new ViewHolderImageList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addImageViewHolder viewHolder0 = (addImageViewHolder) holder;
                ((addImageViewHolder) holder).bind(position, listener);
                break;

            default:
                Image imagPath;
                Image image;
                if (list.size() == maxImageCount) {
                    imagPath = list.get(position);
                    image = list.get(position);
                    if (list.get(position).getImageType() == ImageType.url) {
                        Glide.with(context).load(imagPath.getUrl().replace("$", ""))
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .override(200, 200)
                                .into(((ViewHolderImageList) holder).imgAdd);
                    } else {
                        Glide.with(context).load(imagPath.getUri())
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .override(200, 200)
                                .into(((ViewHolderImageList) holder).imgAdd);
                    }
                } else {
                    imagPath = list.get(position - 1);
                    image = list.get(position - 1);
                    if (list.get(position - 1).getImageType() == ImageType.url) {
                        Glide.with(context).load(imagPath.getUrl().replace("$", ""))
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .override(200, 200)
                                .into(((ViewHolderImageList) holder).imgAdd);
                    } else {
                        Glide.with(context).load(imagPath.getUri())
                                .centerCrop()
                                .skipMemoryCache(true)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .override(200, 200)
                                .into(((ViewHolderImageList) holder).imgAdd);
                    }
                }

                Resources res = holder.itemView.getResources();
                ViewHolderImageList viewHolder2 = (ViewHolderImageList) holder;
                ((ViewHolderImageList) holder).bind(position, image, deleteOnItemClickListener);
                ((ViewHolderImageList) holder).add(position, image, addOriginalImage);

                if (list.size() == maxImageCount) {
                    setMainImageTextView(position == 0, (ViewHolderImageList) holder, res);
                } else {
                    setMainImageTextView(position == 1, (ViewHolderImageList) holder, res);
                }


                break;
        }

    }

    private void setMainImageTextView(boolean isFirstItem, ViewHolderImageList holder, Resources res) {
        if (isFirstItem) {
            holder.tvText.setVisibility(View.VISIBLE);
            (holder).tvText.setText(res.getString(R.string.original_photo));
            (holder).tvText.setTextColor(res.getColor(R.color.secondaryBack1));
        } else {
            holder.tvText.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }


    static class addImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;

        addImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderImageList extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;
        @BindView(R.id.tvText)
        BaseTextView tvText;

        ViewHolderImageList(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Image image, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position, image));
        }

        public void add(int position, Image image, final AddOriginalImage listener) {
            itemView.setOnClickListener(view -> listener.onItemAddOriginalImage(position, image));
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position, Image image);


    }

    public interface AddOriginalImage {
        void onItemAddOriginalImage(int position, Image image);
    }
}
