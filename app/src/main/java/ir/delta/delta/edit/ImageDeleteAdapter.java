package ir.delta.delta.edit;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.Image;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.enums.ImageType;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImageDeleteAdapter extends RecyclerView.Adapter<ImageDeleteAdapter.ViewHolder> {


    private final ArrayList<Image> list;
    private final int cellWidth;
    private final int maxImageCount;
    private final onItemClick listener;

    public ImageDeleteAdapter(ArrayList<Image> list, int cellWidth, int maxImageCount, onItemClick listener) {
        this.list = list;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_estate_delete_recycle, parent, false);
        itemView.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Image image = list.get(position);


        if (image.getImageType() == ImageType.local) {
            Glide.with(holder.imageAdd.getContext()).load(image.getUri())
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .override(200, 200)
                    .into(holder.image);
        } else {
            Glide.with(holder.imageAdd.getContext()).load(image.getUrl().replace("$", ""))
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .override(200, 200)
                    .into(holder.image);
        }


        holder.bind(position, image, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageAdd)
        BaseImageView imageAdd;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;
        @BindView(R.id.image)
        BaseImageView image;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Image image, final onItemClick listener) {
            imageAdd.setOnClickListener(v -> listener.onAddImage(position, image));
        }

    }

    public interface onItemClick {
        void onAddImage(int position, Image image);
    }
}
