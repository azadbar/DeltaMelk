package ir.delta.delta.myEstate;

import android.annotation.SuppressLint;
import android.content.res.Resources;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.EstateStatusEnum;
import ir.delta.delta.service.ResponseModel.myEstate.ConsultantEstate;
import ir.delta.delta.service.ResponseModel.myEstate.FilterMyEstateReq;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ConsultantEstateAdapter extends RecyclerView.Adapter<ConsultantEstateAdapter.ViewHolder> {


    private final ArrayList<ConsultantEstate> list;
    private final OnItemClickListener listener;
    private int methodCode;


    ConsultantEstateAdapter(ArrayList<ConsultantEstate> list, OnItemClickListener listener, int methodCode) {
        this.list = list;
        this.listener = listener;
        this.methodCode = methodCode;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_consultant_estate, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        ConsultantEstate object = list.get(position);

        int width = res.getDimensionPixelOffset(R.dimen.height_size_item_search_estate);
        ViewGroup.LayoutParams lp = holder.imageEstate.getLayoutParams();
        lp.height = (int) (width / Constants.imageRatio);
        holder.imageEstate.setLayoutParams(lp);

        Glide.with(holder.imageEstate.getContext()).load(object.getImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .into(holder.imageEstate);

        holder.tvTitleEstate.setText(object.getContractType() + " " + object.getPropertyType() + "،" + object.getLocation() + "، " + object.getAddress());

        holder.tvDetailsEstate.setText(object.getShowDetail());
        holder.tvDuration.setText(object.getDurationDate());

        if (object.getStatusEnum() == null) {
            object.setStatusEnum(EstateStatusEnum.getEnum(object.getStatusLocalId()));
        }

        setPrice(object, holder, res);
        setButtons(object, holder, res);
        if (object.getViewCount() > 0) {
            holder.tvViewCount.setText("\uD83D\uDC41 " + Constants.convertNumberToDecimal(object.getViewCount()));
        } else {
            holder.tvViewCount.setText(null);
        }

        if (methodCode == EstateStatusEnum.NONE.getMethodCode()) {
            holder.tvStatus.setVisibility(View.VISIBLE);
            holder.tvStatus.setText(object.getStatusName());
            holder.tvStatus.setBackgroundColor(object.getStatusEnum().getBackColor(res));
        } else {
            holder.tvStatus.setVisibility(View.GONE);
        }


        if (object.isSpecial()) {
            holder.tvExtended.setVisibility(View.VISIBLE);
            holder.tvExtended.setText(res.getString(R.string.staired));
        } else {
            holder.tvExtended.setVisibility(View.GONE);
        }
        holder.bind(object, position, listener);

    }


    private void setPrice(ConsultantEstate object, ViewHolder holder, Resources res) {
        if (object.getContractTypeLocalId() == ContractTypeEnum.PURCHASE.getMethodCode()) {
            holder.tvRentPrice.setVisibility(View.GONE);
            holder.tvBuyPrice.setVisibility(View.VISIBLE);
            if (object.getMortgageOrTotal() >= 1000) {
                holder.tvBuyPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.price_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvBuyPrice.setText(null);
            }
        } else {
            if (object.getMortgageOrTotal() >= 1000) {
                holder.tvBuyPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.mortgage_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvBuyPrice.setText(res.getString(R.string.no_deposit));
            }

            if (object.getRentOrMetric() >= 1000)

                holder.tvRentPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.rent_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(object.getRentOrMetric(), res) + "</strong></font>"));

            else
                holder.tvRentPrice.setText(res.getString(R.string.no_rent));
        }
    }

    public Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    private void setButtons(ConsultantEstate object, ViewHolder holder, Resources res) {
        switch (object.getStatusEnum()) {
            case CONFIRMED:
                holder.btnOne.setVisibility(View.VISIBLE);
                holder.btnTwo.setVisibility(View.VISIBLE);
                holder.btnOne.setText(res.getString(R.string.baygani));
                holder.btnTwo.setText(res.getString(R.string.stair));//comment for stairs
                if (object.isExtendable()) {
                    holder.btnThree.setVisibility(View.VISIBLE);
                    holder.btnThree.setText(res.getString(R.string.extended));
                    holder.btnThree.setTag("extended");
                } else {
                    holder.btnThree.setVisibility(View.GONE);
                    holder.btnThree.setTag(null);
                }
                break;
            case ARCHIVE:
                holder.btnTwo.setVisibility(View.GONE);
                holder.btnThree.setVisibility(View.GONE);
                if (object.isExtendable()) {
                    holder.btnOne.setVisibility(View.VISIBLE);
                    holder.btnOne.setText(res.getString(R.string.extended));
                    holder.btnOne.setTag("extended");
                } else {
                    holder.btnOne.setVisibility(View.GONE);
                    holder.btnOne.setTag(null);
                }
                break;
            case BAYEGANI:
                holder.btnOne.setVisibility(View.VISIBLE);
                holder.btnThree.setVisibility(View.GONE);
                holder.btnOne.setText(res.getString(R.string.exit_from_archive));
                holder.btnTwo.setVisibility(View.GONE);

                break;
            default:
                holder.btnTwo.setVisibility(View.GONE);
                holder.btnThree.setVisibility(View.GONE);
                holder.btnOne.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setFilter(FilterMyEstateReq filter) {
        methodCode = filter != null ? filter.getStatusLocalId() : EstateStatusEnum.NONE.getMethodCode();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageEstate)
        BaseImageView imageEstate;
        @BindView(R.id.tvDuration)
        BaseTextView tvDuration;
        @BindView(R.id.tvStatus)
        BaseTextView tvStatus;
        @BindView(R.id.tvTitleEstate)
        BaseTextView tvTitleEstate;
        @BindView(R.id.tvDetailsEstate)
        BaseTextView tvDetailsEstate;
        @BindView(R.id.tvBuyPrice)
        BaseTextView tvBuyPrice;
        @BindView(R.id.tvRentPrice)
        BaseTextView tvRentPrice;
        @BindView(R.id.tvViewCount)
        BaseTextView tvViewCount;
        @BindView(R.id.btnDelete)
        BaseTextView btnDelete;
        @BindView(R.id.btnEdit)
        BaseTextView btnEdit;
        @BindView(R.id.btnOne)
        BaseTextView btnOne;
        @BindView(R.id.btnTwo)
        BaseTextView btnTwo;
        @BindView(R.id.btnThree)
        BaseTextView btnThree;
        @BindView(R.id.tvExtended)
        BaseTextView tvExtended;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(ConsultantEstate estate, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, estate));

            btnEdit.setOnClickListener(v -> listener.edit(position, estate));
            btnDelete.setOnClickListener(v -> listener.delete(position, estate));

            btnOne.setOnClickListener(view -> {
                if (TextUtils.equals((CharSequence) view.getTag(), "extended")) {
                    listener.extended(position, estate);
                } else if (estate.getStatusEnum() == EstateStatusEnum.CONFIRMED) {
                    listener.suspend(position, estate);
                } else if (estate.getStatusEnum() == EstateStatusEnum.BAYEGANI) {
                    listener.exitFromArchive(position, estate);
                }
            });

            btnTwo.setOnClickListener(view -> {
                if (TextUtils.equals((CharSequence) view.getTag(), "extended")) {
                    listener.extended(position, estate);
                } else if (estate.getStatusEnum() == EstateStatusEnum.CONFIRMED) {
                    listener.stairs(position, estate);
                }
            });

            btnThree.setOnClickListener(view -> {
                if (TextUtils.equals((CharSequence) view.getTag(), "extended")) {
                    listener.extended(position, estate);
                }
            });
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int position, ConsultantEstate depositItems);

        void delete(int position, ConsultantEstate depositItems);

        void edit(int position, ConsultantEstate depositItems);

        void extended(int position, ConsultantEstate depositItems);

        void exitFromArchive(int position, ConsultantEstate depositItems);

        void suspend(int position, ConsultantEstate depositItems);

        void stairs(int position, ConsultantEstate depositItems);


    }


}
