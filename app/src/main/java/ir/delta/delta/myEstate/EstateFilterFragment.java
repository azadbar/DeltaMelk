package ir.delta.delta.myEstate;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.service.ResponseModel.myEstate.EstateStatus;
import ir.delta.delta.service.ResponseModel.myEstate.FilterMyEstateReq;
import ir.delta.delta.util.Constants;

import static android.app.Activity.RESULT_OK;

public class EstateFilterFragment extends BaseFragment {

    private final String STATUS = "status";
    private final String PROPERTY = "property";
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;

    @BindView(R.id.edtDepositId)
    CustomEditText edtDepositId;
    @BindView(R.id.propertyType_layout)
    LocationView propertyTypeLayout;
    @BindView(R.id.status_layout)
    LocationView statusLayout;
    @BindView(R.id.btnSearch)
    BaseTextView btnSearch;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;

    private PropertyType selectedPropertyType;
    private EstateStatus selectedStatus;
    private FilterMyEstateReq estateFilter;
    private City consultantCity;
    private ArrayList<EstateStatus> statusList;
    private ContractObject selectedContractType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_consultant_estate_filter, container, false);
        ButterKnife.bind(this, view);
        hideInputKeyboard();
        initView();
        Bundle b = getArguments();
        if (b != null) {
            estateFilter = (FilterMyEstateReq) b.getSerializable("filter");
            statusList = b.getParcelableArrayList("statusList");

        }


        statusLayout.setVisibility(statusList == null || statusList.size() == 0 ? View.GONE : View.VISIBLE);

        setFilter();
        return view;
    }

    private void setFilter() {
        Area ConsultantArea = TransactionArea.getInstance().getAreaById(getActivity(), Constants.getUser(getActivity()).getLocationId());

        if (ConsultantArea == null) {
            consultantCity = TransactionCity.getInstance().getCityById(getActivity(), Constants.getUser(getActivity()).getLocationId());
        } else {
            consultantCity = TransactionCity.getInstance().getCityById(getActivity(), ConsultantArea.getParentId());
        }

        if (estateFilter != null && estateFilter.getDepositId() > 0) {
            edtDepositId.setTextBody(estateFilter.getDepositId() + "");
        } else {
            edtDepositId.setTextsTitle(getResources().getString(R.string.code_estate));
        }

        if (consultantCity != null)
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(ContractTypeEnum.PURCHASE.getMethodCode(), consultantCity.getLocationFeatureLocalId());
        else
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(ContractTypeEnum.PURCHASE.getMethodCode(), 1);

        if (estateFilter != null && estateFilter.getPropertyTypeLocalId() > 0) {
            propertyTypeSelected(selectedContractType.getPropertyTypeBy(estateFilter.getPropertyTypeLocalId()));
        } else {
            propertyTypeLayout.setTxtTitle(getString(R.string.property_text));
            propertyTypeLayout.setHint(getString(R.string.click_to_select));
            propertyTypeLayout.reset();
        }

        if (estateFilter != null) {
            EstateStatus status = getStatus(estateFilter.getStatusLocalId());
            if (status != null) {
                statusSelected(status);
            }
        }
    }

    private EstateStatus getStatus(int id) {
        for (EstateStatus status : statusList) {
            if (Integer.parseInt(status.getValue()) == id) {
                return status;
            }
        }
        return statusList.size() > 0 ? statusList.get(0) : null;
    }

    private void initView() {
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);
        rlFilterChange.setVisibility(View.VISIBLE);
        tvFilterChnage.setText(getResources().getString(R.string.delete_filter));

        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.search_request));

        propertyTypeLayout.setTxtTitle(getString(R.string.property_text));
        propertyTypeLayout.setHint(getString(R.string.click_to_select));
        propertyTypeLayout.reset();

        statusLayout.setTxtTitle(getString(R.string.status_estate));
        statusLayout.setHint(getString(R.string.click_to_select));
        statusLayout.reset();

    }


    private void setContractType(ContractTypeEnum contractType) {
        if (consultantCity != null) {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractType.getMethodCode(), consultantCity.getLocationFeatureLocalId());
        } else {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractType.getMethodCode(), 1);
        }
        propertyTypeSelected(null);

    }

    private void propertyTypeSelected(PropertyType propertyType) {
        if (propertyTypeLayout == null) {
            return;
        }
        selectedPropertyType = propertyType;
        if (selectedPropertyType != null) {
            propertyTypeLayout.setValue(selectedPropertyType.getTitle(), null);
        } else {
            propertyTypeLayout.reset();
        }
    }

    private void statusSelected(EstateStatus status) {
        selectedStatus = status;
        if (selectedStatus != null) {
            statusLayout.setValue(selectedStatus.getText(), null);
        } else {
            statusLayout.reset();
        }
    }


    @OnClick({R.id.rlBack, R.id.btnSearch, R.id.btnCancel, R.id.propertyType_layout, R.id.status_layout,
            R.id.rlFilterChange})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.btnSearch:
                fillFilter();
                if (getActivity() != null) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", Constants.FILTER_ESTATE_STATUS_LIST);
                    returnIntent.putExtra("estateFilter", estateFilter);
                    getActivity().setResult(RESULT_OK, returnIntent);
                    getActivity().finish();
                }

                break;
            case R.id.btnCancel:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.propertyType_layout:
                if (selectedContractType != null && selectedContractType.getPropertyTypes() != null && selectedContractType.getPropertyTypes().size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(EstateFilterFragment.this, Constants.REQUEST_PROPERTYTYPE_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", PROPERTY);
                    bundle.putSerializable("locationType", LocationTypeEnum.PROPERTYTYPE);
                    bundle.putParcelableArrayList("list", new ArrayList<>(selectedContractType.getPropertyTypes()));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    loadFragment(locationFragment, LocationSelectFragment.class.getName());
                }
                break;
            case R.id.status_layout:
                if (statusList.size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(EstateFilterFragment.this, Constants.REQUEST_ESTATE_STATUS);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", STATUS);
                    bundle.putSerializable("locationType", LocationTypeEnum.ESTATE_STATUS);
                    bundle.putParcelableArrayList("list", new ArrayList<>(statusList));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    loadFragment(locationFragment, LocationSelectFragment.class.getName());
                }

                break;
            case R.id.rlFilterChange:
                if (getActivity() != null) {
                    estateFilter = null;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", Constants.FILTER_DEMAND_LIST);
                    returnIntent.putExtra("estateFilter", estateFilter);
                    getActivity().setResult(RESULT_OK, returnIntent);
                    getActivity().finish();
                }
                break;

        }
    }

    private void fillFilter() {

        if (estateFilter == null)
            estateFilter = new FilterMyEstateReq();


        estateFilter.setDepositId(edtDepositId.getValueInt());


        if (selectedPropertyType != null)
            estateFilter.setPropertyTypeLocalId(selectedPropertyType.getPropertyTypeLocalId());

        if (selectedStatus != null)
            estateFilter.setStatusLocalId(Integer.parseInt(selectedStatus.getValue()));

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_PROPERTYTYPE_CODE) {
                propertyTypeSelected(data.getParcelableExtra(PROPERTY));
            } else if (requestCode == Constants.REQUEST_ESTATE_STATUS) {
                statusSelected(data.getParcelableExtra(STATUS));
            }
        }
    }

    public void loadFragment(LocationSelectFragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
            fragTrans.addToBackStack(fragmentTag);
            fragTrans.commit();
        }
    }

    private void hideInputKeyboard() {
        if (getActivity() != null) {
            if (this.edtDepositId.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }
    }

}
