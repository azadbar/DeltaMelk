package ir.delta.delta.myEstate;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.service.ResponseModel.myEstate.EstateStatus;
import ir.delta.delta.service.ResponseModel.myEstate.FilterMyEstateReq;

public class EstateFilterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultant_estate_filter);
        EstateFilterFragment demandFilterFragment = new EstateFilterFragment();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            FilterMyEstateReq demandFilter = (FilterMyEstateReq) bundle.getSerializable("filter");
            ArrayList<EstateStatus> statusList = bundle.getParcelableArrayList("statusList");
            if (statusList != null) {
                Bundle b = new Bundle();
                b.putSerializable("filter", demandFilter);
                b.putParcelableArrayList("statusList", statusList);
                demandFilterFragment.setArguments(b);
            }
        }
        FragmentManager frgMgr = getSupportFragmentManager();
        FragmentTransaction frgTrans = frgMgr.beginTransaction();
        frgTrans.replace(R.id.frameLayout, demandFilterFragment);
        frgTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        frgTrans.commit();
    }
}
