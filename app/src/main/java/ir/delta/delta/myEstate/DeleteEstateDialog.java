package ir.delta.delta.myEstate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.DeleteOwnerTypeEnum;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DeleteEstateService;
import ir.delta.delta.service.Request.DeleteUserEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.EstateActionReq;
import ir.delta.delta.service.ResponseModel.myEstate.DeleteEstateAdapter;
import ir.delta.delta.service.ResponseModel.myEstate.EstateActionResponse;
import ir.delta.delta.util.Constants;


public class DeleteEstateDialog extends Dialog implements DeleteEstateAdapter.OnItemClickListener {


    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;
    @BindView(R.id.tvTxtBugReport)
    BaseTextView tvTxtBugReport;
    @BindView(R.id.rlTitle)
    BaseRelativeLayout rlTitle;
    @BindView(R.id.rvBugReport)
    RecyclerView rvBugReport;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    private final ArrayList<DeleteOwnerTypeEnum> list = new ArrayList<>();
    private DeleteEstateAdapter adapter;
    private DeleteOwnerTypeEnum selected = DeleteOwnerTypeEnum.DeleteSaled;
    private long id;
    private OnDeleteEstateItem listener;

    public void setId(long id) {
        this.id = id;
    }

    public void setListener(OnDeleteEstateItem listener) {
        this.listener = listener;
    }

    public DeleteEstateDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.delete_estate_dialog, null);
        ButterKnife.bind(this, view);
        setContentView(view);
        tvTxtBugReport.setText(getContext().getResources().getString(R.string.delete_reason));
        btnOk.setText(getContext().getResources().getString(R.string.delete));
        btnCancel.setText(getContext().getResources().getString(R.string.cancel));
        list.add(DeleteOwnerTypeEnum.DeleteSaled);
        list.add(DeleteOwnerTypeEnum.DeleteMortgage);
        list.add(DeleteOwnerTypeEnum.DeleteSiteNotGood);
        list.add(DeleteOwnerTypeEnum.DeleteOwnerGiveUp);
        list.add(DeleteOwnerTypeEnum.DeleteOthers);
        if (!Constants.getUser(getContext()).isGeneral()) {
            list.add(DeleteOwnerTypeEnum.DeleteRealtorMistake);
            list.add(DeleteOwnerTypeEnum.DeleteOwnerMistake);
        }


        if (getWindow() != null) {
            Window window = getWindow();
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int maxHeight = getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_show_info);
                int cellHeight = getContext().getResources().getDimensionPixelSize(R.dimen.defult_height_dilog_info);
                int heightDialog = (cellHeight) * (4 + list.size());
                if (heightDialog > maxHeight) {
                    heightDialog = maxHeight;
                } else if (heightDialog <= (cellHeight * 4)) {
                    heightDialog = cellHeight * 5;
                }
                window.setLayout(width, heightDialog);
                window.setGravity(Gravity.CENTER);
            }

        }

        setDataAdapter();
    }


    private void setDataAdapter() {
        adapter = new DeleteEstateAdapter(list, selected, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rvBugReport.setLayoutManager(layoutManager);
        rvBugReport.setAdapter(adapter);
    }


    @OnClick({R.id.btnOk, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                if (Constants.getUser(getContext()).isGeneral()) {
                    deleteUserEstate();
                } else {
                    deleteConsultantatEstate();
                }

                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    private void deleteConsultantatEstate() {
        if (selected != null) {
            progressDialog.setVisibility(View.VISIBLE);
            EstateActionReq req = new EstateActionReq();
            req.setId(id);
            req.setDeleteMode(selected.getCode());
            DeleteEstateService.getInstance().deleteEstate(getContext(), getContext().getResources(), req, new ResponseListener<EstateActionResponse>() {
                @Override
                public void onGetError(String error) {
                    if (progressDialog != null) {
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                        progressDialog.setVisibility(View.GONE);
                        dismiss();
                    }
                }

                @Override
                public void onAuthorization() {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getContext());
                    getContext().startActivity(intent);
                    dismiss();
                }

                @Override
                public void onSuccess(EstateActionResponse response) {
                    if (progressDialog != null) {
                        progressDialog.setVisibility(View.GONE);
                        if (response.isSuccessed()) {
                            showSuccessToast(response.getMessage(), getContext().getResources().getString(R.string.your_property_has_been_deleted));
                            listener.onDeleteEstate(id);
                        } else {
                            showSuccessToast(response.getMessage(), getContext().getResources().getString(R.string.operation_encountered_error));
                        }
                        dismiss();
                    }
                }
            });
        } else {
            progressDialog.setVisibility(View.GONE);
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.error_bug_report), Toast.LENGTH_SHORT).show();

        }
    }

    private void deleteUserEstate() {
        if (selected != null) {
            progressDialog.setVisibility(View.VISIBLE);
            EstateActionReq req = new EstateActionReq();
            req.setId(id);
            req.setDeleteMode(selected.getCode());
            DeleteUserEstateService.getInstance().deleteUserEstate(getContext(), getContext().getResources(), req, new ResponseListener<EstateActionResponse>() {
                @Override
                public void onGetError(String error) {
                    if (progressDialog != null) {
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                        progressDialog.setVisibility(View.GONE);
                        dismiss();
                    }
                }

                @Override
                public void onAuthorization() {
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getContext());
                    getContext().startActivity(intent);
                    dismiss();
                }

                @Override
                public void onSuccess(EstateActionResponse response) {
                    if (progressDialog != null) {
                        progressDialog.setVisibility(View.GONE);
                        if (response.isSuccessed()) {
                            showSuccessToast(response.getMessage(), getContext().getResources().getString(R.string.your_property_has_been_deleted));
                            listener.onDeleteEstate(id);
                        } else {
                            showSuccessToast(response.getMessage(), getContext().getResources().getString(R.string.operation_encountered_error));
                        }
                        dismiss();
                    }
                }
            });
        } else {
            progressDialog.setVisibility(View.GONE);
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.error_bug_report), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onItemClickSingle(int position) {
        selected = list.get(position);
        adapter.setDeleteEstate(selected);
    }

    private void showSuccessToast(String message, String defualt) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getContext(), defualt, Toast.LENGTH_SHORT).show();
        }
    }

    public interface OnDeleteEstateItem {
        void onDeleteEstate(long id);
    }
}
