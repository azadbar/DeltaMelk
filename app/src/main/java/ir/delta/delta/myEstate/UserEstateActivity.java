package ir.delta.delta.myEstate;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.deltanet.sendToSite.SendToSiteDeltaNetFragment;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.editUser.EditUserEstateActivity;
import ir.delta.delta.enums.EstateStatusEnum;
import ir.delta.delta.enums.PaymentTypeEnum;
import ir.delta.delta.payment.PaymentDialog;
import ir.delta.delta.payment.ServerErrorListener;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ExtendedEstateUserService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.StairsService;
import ir.delta.delta.service.Request.SuspendUserEstateService;
import ir.delta.delta.service.Request.UnSuspendEstateUserService;
import ir.delta.delta.service.Request.UserEstateService;
import ir.delta.delta.service.RequestModel.EstateActionReq;
import ir.delta.delta.service.RequestModel.StairsReq;
import ir.delta.delta.service.ResponseModel.DepositResponse;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.myEstate.ConsultantEstate;
import ir.delta.delta.service.ResponseModel.myEstate.MyEstateResponse;
import ir.delta.delta.service.ResponseModel.myEstate.EstateActionResponse;
import ir.delta.delta.service.ResponseModel.myEstate.FilterMyEstateReq;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import ir.delta.delta.wallet.WalletDialog;

public class UserEstateActivity extends BaseActivity implements UserEstateAdapter.OnItemClickListener, DeleteEstateDialog.OnDeleteEstateItem, ServerErrorListener {

    private static final int REQUEST_EDIT_CODE = 108;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.loadingView)
    LoadingView loadingView;


    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.rvUser)
    RecyclerView rvUser;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private UserEstateAdapter adapter;
    //    private FilterMyEstateReq filter;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<ConsultantEstate> userEstates = new ArrayList<>();
    //    private ArrayList<EstateStatus> statusList;
    private int resultCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_estate);
        ButterKnife.bind(this);


        intView();
        getList();
    }

    private void intView() {
        tvFilterTitle.setText(getResources().getString(R.string.my_file));
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);


        tvFilterChnage.setText(getResources().getString(R.string.filter));
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvFilterChnage.getLayoutParams();
        params.width = getResources().getDimensionPixelSize(R.dimen.minwidth);
        tvFilterChnage.setLayoutParams(params);
        tvFilterChnage.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
        tvFilterChnage.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        loadingView.setButtonClickListener(view -> getList());
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (userEstates.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getUserListRequest();
        }
    }

    private void getUserListRequest() {
        Map<String, Object> map;

        map = FilterMyEstateReq.getParamsWithoutFilter(userEstates.size(), offset, EstateStatusEnum.NONE.getMethodCode());

        UserEstateService.getInstance().getUserEstate(this, getResources(), map, new ResponseListener<MyEstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    onError(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(UserEstateActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(MyEstateResponse response) {
                if (imgBack != null) {
                    if (rlLoding == null) {
                        return;
                    }
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getDepositItems() != null) {
                        resultCount = response.getSearchResultCount();

                        if (response.getDepositItems().size() < offset) {
                            endOfList = true;
                        }

                        handleToolbarFragment(response);
                        userEstates.addAll(response.getDepositItems());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    public void handleToolbarFragment(MyEstateResponse response) {
        if (response.getDepositItems().size() > 0 || userEstates.size() > 0) {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setVisibility(View.VISIBLE);
            setResultCount();
        } else {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setText(null);
            tvFilterDetail.setVisibility(View.GONE);
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }

    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new UserEstateAdapter(userEstates, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvUser.setHasFixedSize(true);
            rvUser.setLayoutManager(layoutManager);
            rvUser.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvUser.setAdapter(adapter);
            rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    @OnClick({R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
        }

    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvUser.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvUser.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && userEstates != null) {
            rlLoding.setVisibility(View.GONE);
            if (userEstates.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


    @Override
    public void onItemClick(int position, ConsultantEstate depositItems) {
        Intent intent = new Intent(this, EstateUserDetailActivity.class);
        intent.putExtra("id", depositItems.getId());
        intent.putExtra("notShowBugReportAndCall", true);
        startActivity(intent);
//        Toast.makeText(this, "open detail", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void delete(int position, ConsultantEstate depositItems) {
        DeleteEstateDialog deleteEstateDialog = new DeleteEstateDialog(this);
        deleteEstateDialog.setId(depositItems.getId());
        deleteEstateDialog.setListener(this);
        deleteEstateDialog.show();
    }


    @Override
    public void edit(int position, ConsultantEstate depositItems) {
        redirectToEdit(depositItems);
    }


    @Override
    public void extended(int position, ConsultantEstate depositItems) {

        if (depositItems.isExtendedRedirectToEdit()) {
            //TODO redirect editActivity
            //TODO pass extandable
            redirectToEdit(depositItems);
        } else {
            EstateActionReq req = new EstateActionReq();
            req.setId(depositItems.getId());

            roundedLoadingView.setVisibility(View.VISIBLE);
            enableDisableViewGroup(root, false);

            ExtendedEstateUserService.getInstance().extendedEstateUser(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
                @Override
                public void onGetError(String error) {
                    if (tvFilterTitle != null) {
                        roundedLoadingView.setVisibility(View.GONE);
                        enableDisableViewGroup(root, true);
                        Toast.makeText(UserEstateActivity.this, getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAuthorization() {
                    Intent intent = new Intent(UserEstateActivity.this, LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onSuccess(EstateActionResponse response) {
                    if (tvFilterTitle != null) {
                        roundedLoadingView.setVisibility(View.GONE);
                        enableDisableViewGroup(root, true);
                        if (response.isSuccessed()) {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_extended));
                            removeFromList(depositItems.getId());
                        } else {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                        }
                    }
                }
            });
        }


    }


    @Override
    public void exitFromArchive(int position, ConsultantEstate depositItems) {

        EstateActionReq req = new EstateActionReq();
        req.setId(depositItems.getId());

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);

        UnSuspendEstateUserService.getInstance().unSuspendEstateUserEstate(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(UserEstateActivity.this, getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(UserEstateActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(EstateActionResponse response) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_has_been_exit_from_archived));
                        removeFromList(depositItems.getId());
                    } else {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                    }
                }
            }
        });

    }

    @Override
    public void suspend(int position, ConsultantEstate depositItems) {
        EstateActionReq req = new EstateActionReq();
        req.setId(depositItems.getId());

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);

        SuspendUserEstateService.getInstance().suspendUserEstate(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(UserEstateActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(UserEstateActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                startActivity(intent);
                finish();
            }


            @Override
            public void onSuccess(EstateActionResponse response) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_has_been_archived));
                        removeFromList(depositItems.getId());
                    } else {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                    }
                }
            }
        });
    }


    @Override
    public void stairs(int position, ConsultantEstate depositItems) {
        stairsRequest(depositItems.getId());

    }

    private void stairsRequest(long id) {

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        StairsReq req = new StairsReq();
        req.setId(id);
        StairsService.getInstance().staireProperty(this, getResources(), req, new ResponseListener<DepositResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
//                    showErrorDialog(error, 0);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(UserEstateActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(UserEstateActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(DepositResponse response) {
                if (imgBack != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {

                        if (response.isWalletPaymentNeed()) {
                            User user = Constants.getUser(UserEstateActivity.this);
                            if (user != null) {
                                if (response.isWalletAmountOk()) {
                                    PaymentDialog paymentDialog = new PaymentDialog(UserEstateActivity.this);
                                    paymentDialog.enOrderId = response.getEnOrderId();
                                    paymentDialog.orderId = response.getOrderId();
                                    paymentDialog.orderPrice = response.getOrderPrice();
                                    paymentDialog.orderTitle = response.getOrderTitle();
                                    paymentDialog.walletAmount = response.getWalletAmount();
                                    paymentDialog.paymentTypeEnum = PaymentTypeEnum.STAIRS_REGISTER;
                                    paymentDialog.listener = UserEstateActivity.this;
                                    paymentDialog.show();
                                } else {
                                    CustomDialog successDialog = new CustomDialog(UserEstateActivity.this);
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                                    successDialog.setLottieAnim("error.json", 0);
                                    successDialog.setDialogTitle(response.getOrderTitle());
                                    successDialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                                    successDialog.setDescription("برای ادامه فعالیت، لطفا کیف پول خود را شارژ کنید");
                                    successDialog.setCancelListener(getResources().getString(R.string.cancel), view -> successDialog.dismiss());
                                    successDialog.setOkListener(getString(R.string.increase_wallet), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            successDialog.dismiss();
                                            WalletDialog dialog = new WalletDialog(UserEstateActivity.this);
                                            dialog.setCancelable(true);
                                            dialog.show();
                                        }
                                    });
                                    successDialog.show();
                                }
                            }

                        } else {
                            CustomDialog successDialog = new CustomDialog(UserEstateActivity.this);
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                            successDialog.setLottieAnim("tick.json", 0);
                            successDialog.setDialogTitle(getString(R.string.property_registered_with_the_code, response.getDepositId()));
                            successDialog.setColorTitle(getResources().getColor(R.color.applyFore));
                            successDialog.setDescription(getString(R.string.property_renewed_least_day, response.getDuration()));
                            successDialog.setOkListener(getString(R.string.ok), view -> successDialog.dismiss());
                            successDialog.show();
                        }

                    } else {
                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });

    }

    private void redirectToEdit(ConsultantEstate depositItems) {
        Intent intent = new Intent(this, EditUserEstateActivity.class);
        intent.putExtra("id", depositItems.getId());
        intent.putExtra("title", depositItems.getContractType() + " " + depositItems.getPropertyType() + " " + "در " + depositItems.getLocation());
        startActivityForResult(intent, REQUEST_EDIT_CODE);
    }


    private void showSuccessToast(String message, String defualt) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, defualt, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDeleteEstate(long id) {
        removeFromList(id);
    }

    //TODO remove list delete
    private void removeFromList(long id) {
        for (ConsultantEstate consultantEstate : userEstates) {
            if (consultantEstate.getId() == id) {
                userEstates.remove(consultantEstate);
                if (resultCount > 0) {
                    resultCount = resultCount - 1;
                    setResultCount();
                }
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (userEstates == null || userEstates.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }
    }


    private void setResultCount() {
        if (resultCount > 0) {
            tvFilterDetail.setVisibility(View.VISIBLE);
            tvFilterDetail.setText(resultCount + " " + getResources().getString(R.string.estate));
        } else {
            tvFilterDetail.setText(null);
            tvFilterDetail.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShowErrorListener(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        showErrorFromServer(modelStateErrors, message);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_EDIT_CODE) {
                String result = data.getStringExtra("result");
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                endOfList = false;
                userEstates.clear();
                getList();
            }
        }
    }

}
