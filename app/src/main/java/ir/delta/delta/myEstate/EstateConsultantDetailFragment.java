package ir.delta.delta.myEstate;

import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.Model.Language;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.DetailOtherRowView;
import ir.delta.delta.customView.DetailRowView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.estateDetail.BugReportDialog;
import ir.delta.delta.estateDetail.ComminucationDialog;
import ir.delta.delta.estateDetail.SliderAdapter;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DetailConsultantEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.DepositConsultantDetailReq;
import ir.delta.delta.service.ResponseModel.DetailEstateData;
import ir.delta.delta.service.ResponseModel.DetailEstateDisplayOtherFacilities;
import ir.delta.delta.service.ResponseModel.DetailEstateDisplayText;
import ir.delta.delta.service.ResponseModel.DetailEstateResponse;
import ir.delta.delta.service.ResponseModel.DetailListItem;
import ir.delta.delta.service.ResponseModel.FailureBugReport;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.Validation;
import me.relex.circleindicator.CircleIndicator;

import static android.content.Context.CLIPBOARD_SERVICE;

public class EstateConsultantDetailFragment extends BaseFragment {
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgShare)
    BaseImageView imgShare;
    @BindView(R.id.imgLike)
    BaseImageView imgLike;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.tvPrice)
    BaseTextView tvPrice;
    @BindView(R.id.tvRent)
    BaseTextView tvRent;
    @BindView(R.id.tvTextDetail)
    BaseTextView tvTextDetail;
    @BindView(R.id.tvDescription)
    BaseTextView tvDescription;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    @BindView(R.id.tvCodeEstate)
    BaseTextView tvCodeEstate;
    @BindView(R.id.tvMobile)
    BaseTextView tvMobile;
    @BindView(R.id.tvAgencyTell)
    BaseTextView tvAgencyTell;
    @BindView(R.id.imgAgencyLogo)
    BaseImageView imgAgencyLogo;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.addRow)
    LinearLayout addRow;
    @BindView(R.id.llDetail)
    BaseLinearLayout llDetail;
    @BindView(R.id.tvTextContacts)
    BaseTextView tvTextContacts;
    @BindView(R.id.tvTxtNameAgency)
    BaseTextView tvTxtNameAgency;
    @BindView(R.id.tvNameAgency)
    BaseTextView tvNameAgency;
    @BindView(R.id.tvTxtAgencyTell)
    BaseTextView tvTxtAgencyTell;
    @BindView(R.id.tvTxtCodeEstate)
    BaseTextView tvTxtCodeEstate;
    @BindView(R.id.llCodeEstate)
    BaseLinearLayout llCodeEstate;
    @BindView(R.id.tvTxtMobile)
    BaseTextView tvTxtMobile;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.llDescription)
    BaseLinearLayout llDescription;
    @BindView(R.id.llLocation)
    BaseLinearLayout llLocation;
    @BindView(R.id.cvTitle)
    CardView cvTitle;
    @BindView(R.id.cvDetail)
    CardView cvDetail;
    @BindView(R.id.cvDescription)
    CardView cvDescription;
    @BindView(R.id.cvLocation)
    CardView cvLocation;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.llButton)
    BaseLinearLayout llButton;
    @BindView(R.id.rlPager)
    BaseRelativeLayout rlPager;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.parentDetail)
    BaseLinearLayout parentDetail;

    private Unbinder unbinder;
    @BindView(R.id.rlComminucation)
    BaseRelativeLayout rlComminucation;
    @BindView(R.id.rlBugReport)
    BaseRelativeLayout rlBugReport;
    @BindView(R.id.icComminucation)
    BaseImageView icComminucation;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    //    private Estate estate;
    //    private ArrayList<Integer> likeEstateList;
    private DetailEstateData estateDetail;
    private ArrayList<FailureBugReport> failureItemList;
    private long id;
    private boolean notShowBugReportAndCall;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_estate_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        scrollView.requestFocus();
        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_UP));
        scrollView.scrollTo(0, 0);
        scrollView.smoothScrollTo(0, 0);
        hideShowView(View.GONE);
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            imgBack.setImageResource(R.drawable.ic_back_english);
        } else {
            imgBack.setImageResource(R.drawable.ic_back);
        }
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
//        likeEstateList = TransactionLikeEstate.getInstance().getEstateIdList(getActivity());

        Bundle b = getArguments();
        if (b != null) {
            id = b.getLong("id");
            notShowBugReportAndCall = b.getBoolean("notShowBugReportAndCall");
        }

        if (id > 0) {
            getDetailData();
        }

        loadingView.setButtonClickListener(view1 -> {
            if (id > 0) {
                getDetailData();
            }
        });
        if (getActivity() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                rlPager.getLayoutParams().height = (int) (size.x * 12 / 16.0);
            }
        }


        return view;
    }

    private void hideShowView(int visibilty) {
        toolbar.setVisibility(visibilty);
        scrollView.setVisibility(visibilty);
        llButton.setVisibility(visibilty);

    }


    private void getDetailData() {
        loadingView.showLoading(false);
        DepositConsultantDetailReq req = new DepositConsultantDetailReq();
        req.setId(id);
        DetailConsultantEstateService detailEstateService = new DetailConsultantEstateService();
        detailEstateService.getDepositConsultantDetail(getContext(), getResources(), req, new ResponseListener<DetailEstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    loadingView.showError(error);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(DetailEstateResponse response) {
                if (getView() == null || getActivity() == null) {
                    return;
                }

                llButton.setVisibility(View.VISIBLE);
                if (response.isSuccessed() && response.getDetailEstateData() != null) {
                    loadingView.stopLoading();
                    estateDetail = response.getDetailEstateData();
                    failureItemList = response.getFailureItemList();
                    setDataInView();
                } else {
                    loadingView.showError(response.getMessage());
                }
            }
        });
    }

    @SuppressLint({"SetTextI18n", "SetJavaScriptEnabled"})
    private void setDataInView() {
        hideShowView(View.VISIBLE);
        if (notShowBugReportAndCall) {
            llButton.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 0);
            parentDetail.setLayoutParams(params);
        } else {
            llButton.setVisibility(View.VISIBLE);
        }
        makeSlider(estateDetail.getImagesList());
        tvTitle.setText(estateDetail.getDepositTitle());
        if (!TextUtils.isEmpty(estateDetail.getDisplayMortgageOrTotal()) && estateDetail.getMortgageOrTotal() >= 1000) {
            tvPrice.setText(createHtmlText("<font color='#999999'>" + estateDetail.getDisplayMortgageOrTotal() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(estateDetail.getMortgageOrTotal(), getResources()) + "</strong></font>"));
        } else {
            tvPrice.setText(null);
        }

        if (!TextUtils.isEmpty(estateDetail.getDisplayRentOrMetric()) && estateDetail.getRentOrMetric() >= 1000) {
            tvRent.setText(createHtmlText("<font color='#999999'>" + estateDetail.getDisplayRentOrMetric() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(estateDetail.getRentOrMetric(), getResources()) + "</strong></font>"));
        } else {
            tvRent.setText(null);
        }
        if (estateDetail.getDisplayText() == null && estateDetail.getDisplayOtherFacilities() == null) {
            cvDetail.setVisibility(View.GONE);
        } else {
            cvDetail.setVisibility(View.VISIBLE);
            for (int i = 0; i < estateDetail.getDisplayText().size(); i++) {
                DetailEstateDisplayText display = estateDetail.getDisplayText().get(i);
                addRowView(display, i);
            }

            int listCount = estateDetail.getDisplayText().size();
            for (int i = 0; i < estateDetail.getDisplayOtherFacilities().size(); i++) {
                DetailEstateDisplayOtherFacilities other = estateDetail.getDisplayOtherFacilities().get(i);
                addOtherRowView(other, listCount + i);
            }
        }

        if (!TextUtils.isEmpty(estateDetail.getDescription())) {
            cvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(estateDetail.getDescription());
        } else {
            cvDescription.setVisibility(View.GONE);
        }

        cvLocation.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(estateDetail.getName().trim()) && estateDetail.isIsOfficeActive()) {
            tvNameAgency.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(estateDetail.getUserAgencyName())) {
                tvNameAgency.setText(getResources().getString(R.string.consultant) + ": " + estateDetail.getName().trim() + "، " + estateDetail.getUserAgencyName().trim());
            } else {
                tvNameAgency.setText(getResources().getString(R.string.consultant) + ": " + estateDetail.getName().trim());
            }
        } else {

            tvNameAgency.setText(getString(R.string.advertiser) + ": " + estateDetail.getName().trim());
            tvCodeEstate.setText(getResources().getString(R.string.code_estate) + ": " + estateDetail.getDeposiId());
            tvMobile.setText(getResources().getString(R.string.mobile_text) + ": " + estateDetail.getMobile());
            tvAgencyTell.setVisibility(View.GONE);
        }
        if (estateDetail.getDeposiId() > 0) {
            tvCodeEstate.setVisibility(View.VISIBLE);
            tvCodeEstate.setText(getResources().getString(R.string.code_estate) + ": " + estateDetail.getDeposiId());
        } else {
            tvCodeEstate.setVisibility(View.GONE);
        }

        String stringYouExtracted = tvCodeEstate.getText().toString();
        int startIndex = tvCodeEstate.getSelectionStart();
        int endIndex = tvCodeEstate.getSelectionEnd();
        stringYouExtracted = stringYouExtracted.substring(startIndex, endIndex);
        if (getActivity() != null) {
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
            clipboard.setText(stringYouExtracted);
        }


        if (!TextUtils.isEmpty(estateDetail.getMobile())) {
            tvMobile.setVisibility(View.VISIBLE);
            tvMobile.setText(getResources().getString(R.string.mobile_text) + ": " + estateDetail.getMobile());
        } else {
            tvMobile.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(estateDetail.getUserAgencyPhone())) {
            tvAgencyTell.setVisibility(View.VISIBLE);
            tvAgencyTell.setText(getResources().getString(R.string.agency_tell_text) + ": " + estateDetail.getUserAgencyPhone());
        } else {
            tvAgencyTell.setVisibility(View.GONE);
        }
        if (estateDetail.isIsOfficeActive()) {
            if (getContext() != null) {
                imgAgencyLogo.setVisibility(View.VISIBLE);
                Glide.with(getContext()).load(estateDetail.getUserAgencyLogo()).into(imgAgencyLogo);
            }
        } else {
            imgAgencyLogo.setVisibility(View.GONE);
        }
        if (estateDetail.getOfficeStarsCnt() > 0) {
            ratingBar.setVisibility(View.VISIBLE);
            ratingBar.setRating(estateDetail.getOfficeStarsCnt());
        } else {
            ratingBar.setVisibility(View.GONE);
        }
    }


    private void addRowView(DetailEstateDisplayText detailEstateDisplayText, int index) {
        DetailRowView detailRowView = new DetailRowView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        detailRowView.setLayoutParams(lp);
        addRow.addView(detailRowView);
        detailRowView.setTextTitle(detailEstateDisplayText.getText(), detailEstateDisplayText.getValue());
        detailRowView.setBackgroundColor(index % 2 == 0 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.oddW));
    }


    private void addOtherRowView(DetailEstateDisplayOtherFacilities other, int index) {
        DetailOtherRowView detailOtherRowView = new DetailOtherRowView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        detailOtherRowView.setLayoutParams(lp);
        addRow.addView(detailOtherRowView);
        detailOtherRowView.setTextTitle(other.getText());
        if (other.isValue()) {
            detailOtherRowView.setImageIcon(R.drawable.ic_check_circle_green, R.color.green);
            detailOtherRowView.setTextStatus(getString(R.string.exist));
            detailOtherRowView.setTextStatusColor(getResources().getColor(R.color.primaryTextColor));
        } else {
            detailOtherRowView.setImageIcon(R.drawable.ic_cancel, R.color.primaryBack);
            detailOtherRowView.setTextStatus(getString(R.string.no_exist));
            detailOtherRowView.setTextStatusColor(getResources().getColor(R.color.primaryTextColor));
        }
        detailOtherRowView.setBackgroundColor(index % 2 == 0 ? getResources().getColor(R.color.white) : getResources().getColor(R.color.oddW));

    }

    private void makeSlider(List<DetailListItem> imagesList) {
        final ArrayList<String> images = new ArrayList<>();
        for (int i = 0; i < imagesList.size(); i++) {
            images.add(imagesList.get(i).getPath());
        }

        pager.setAdapter(new SliderAdapter(images));
        if (images.size() == 0) {
            pager.setBackgroundResource(R.drawable.placehoder_big);
        }
        if (images.size() == 1) {
            indicator.setVisibility(View.GONE);
        } else {
            indicator.setViewPager(pager);
        }
    }

    @OnClick({R.id.imgShare, R.id.rlBugReport, R.id.rlBack, R.id.rlComminucation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgShare:
                if (getContext() != null && !TextUtils.isEmpty(estateDetail.getEnDepositId())) {
                    Constants.shareTextUrl(getContext(), Constants.Download_Base_Url + estateDetail.getEnDepositId());
                }
                break;
            case R.id.rlBugReport:
                if (failureItemList != null && estateDetail.getEnDepositId() != null && getActivity() != null) {
                    BugReportDialog bugReportDialog = new BugReportDialog(getActivity());
                    bugReportDialog.setFailureBugReports(failureItemList);
                    bugReportDialog.setEncodedId(estateDetail.getEnDepositId());
                    bugReportDialog.show();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.not_find_bug_report), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.rlComminucation:
                if (estateDetail != null && !TextUtils.isEmpty(estateDetail.getMobile())) {
                    if (TextUtils.isEmpty(Validation.getMobileError(estateDetail.getMobile(), true, getResources()))) {
                        if (getActivity() != null) {
                            ComminucationDialog comminucationDialog = new ComminucationDialog(getActivity());
                            comminucationDialog.setName(estateDetail.getName());
                            comminucationDialog.setMobile(estateDetail.getMobile());
                            comminucationDialog.setUserAgencyName(estateDetail.getUserAgencyName());
                            comminucationDialog.setIsOfficeActive(estateDetail.isOfficeActive());
                            comminucationDialog.setLogoSrc(estateDetail.getUserAgencyLogo());
                            comminucationDialog.show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "شماره تماس اشتباه است", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.not_find_mobile), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.rlBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
