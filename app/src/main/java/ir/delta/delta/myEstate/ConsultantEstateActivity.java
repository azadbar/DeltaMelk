package ir.delta.delta.myEstate;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.deltanet.sendToSite.SendToSiteDeltaNetFragment;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.edit.EditEstateActivity;
import ir.delta.delta.enums.EstateStatusEnum;
import ir.delta.delta.enums.PaymentTypeEnum;
import ir.delta.delta.payment.PaymentDialog;
import ir.delta.delta.payment.ServerErrorListener;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ConsultantEstateService;
import ir.delta.delta.service.Request.ExtendedEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.StairsCounsultantService;
import ir.delta.delta.service.Request.SuspendEstateService;
import ir.delta.delta.service.Request.UnSuspendEstateService;
import ir.delta.delta.service.RequestModel.EstateActionReq;
import ir.delta.delta.service.RequestModel.StairsReq;
import ir.delta.delta.service.ResponseModel.DepositResponse;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.myEstate.ConsultantEstate;
import ir.delta.delta.service.ResponseModel.myEstate.MyEstateResponse;
import ir.delta.delta.service.ResponseModel.myEstate.EstateActionResponse;
import ir.delta.delta.service.ResponseModel.myEstate.EstateStatus;
import ir.delta.delta.service.ResponseModel.myEstate.FilterMyEstateReq;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import ir.delta.delta.wallet.WalletDialog;

public class ConsultantEstateActivity extends BaseActivity implements ConsultantEstateAdapter.OnItemClickListener, DeleteEstateDialog.OnDeleteEstateItem, ServerErrorListener {

    private static final int REQUEST_EDIT_CODE = 108;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.loadingView)
    LoadingView loadingView;


    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.rvConsultantEstateList)
    RecyclerView rvConsultantEstateList;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private ConsultantEstateAdapter adapter;
    private FilterMyEstateReq filter;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<ConsultantEstate> consultantEstates = new ArrayList<>();
    private ArrayList<EstateStatus> statusList;
    private int resultCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultant_estate);
        ButterKnife.bind(this);

        intView();
        getList();
    }

    private void intView() {
        tvFilterTitle.setText(EstateStatusEnum.NONE.getText(getResources()));
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);

        tvFilterChnage.setText(getResources().getString(R.string.filter));
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvFilterChnage.getLayoutParams();
        params.width = getResources().getDimensionPixelSize(R.dimen.minwidth);
        tvFilterChnage.setLayoutParams(params);
        tvFilterChnage.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
        tvFilterChnage.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        loadingView.setButtonClickListener(view -> getList());
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (consultantEstates.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getConsultantListRequest();
        }
    }

    private void getConsultantListRequest() {
        Map<String, Object> map;
        if (filter == null) {
            map = FilterMyEstateReq.getParamsWithoutFilter(consultantEstates.size(), offset, EstateStatusEnum.NONE.getMethodCode());
        } else {
            map = filter.getParams(consultantEstates.size(), offset);
        }

        ConsultantEstateService.getInstance().getCosultantEstate(this, getResources(), map, new ResponseListener<MyEstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    onError(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                intent.putExtra("itemsIndex", 3);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(MyEstateResponse response) {
                if (imgBack != null) {
                    if (rlLoding == null) {
                        return;
                    }
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getDepositItems() != null) {
                        resultCount = response.getSearchResultCount();

                        if (response.getDepositItems().size() < offset) {
                            endOfList = true;
                        }

                        if (statusList == null) {
                            statusList = response.getStatusList();
                            rlFilterChange.setVisibility(View.VISIBLE);
                        }
                        handleToolbarFragment(response);
                        consultantEstates.addAll(response.getDepositItems());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    public void handleToolbarFragment(MyEstateResponse response) {
        if (response.getDepositItems().size() > 0 || consultantEstates.size() > 0) {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setVisibility(View.VISIBLE);
            setResultCount();
        } else {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setText(null);
            tvFilterDetail.setVisibility(View.GONE);
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }

    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new ConsultantEstateAdapter(consultantEstates, this, filter != null ? filter.getStatusLocalId() : EstateStatusEnum.NONE.getMethodCode());
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvConsultantEstateList.setHasFixedSize(true);
            rvConsultantEstateList.setLayoutManager(layoutManager);
            rvConsultantEstateList.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvConsultantEstateList.setAdapter(adapter);
            rvConsultantEstateList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.setFilter(filter);
        }
    }


    @OnClick({R.id.rlBack, R.id.rlFilterChange})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlFilterChange:
                Intent intent = new Intent(this, EstateFilterActivity.class);
                intent.putExtra("filter", filter);
                intent.putExtra("statusList", statusList);
                startActivityForResult(intent, Constants.FILTER_ESTATE_STATUS_LIST);
                break;
        }

    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvConsultantEstateList.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvConsultantEstateList.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && consultantEstates != null) {
            rlLoding.setVisibility(View.GONE);
            if (consultantEstates.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.FILTER_ESTATE_STATUS_LIST) {
                filter = (FilterMyEstateReq) data.getSerializableExtra("estateFilter");
                if (filter == null || statusList == null || statusList.size() == 0) {
                    tvFilterTitle.setText(EstateStatusEnum.NONE.getText(getResources()));
                } else {
                    tvFilterTitle.setText(getStatusName(filter.getStatusLocalId()));
                }
                tvFilterDetail.setText("");
                consultantEstates.clear();
                if (adapter != null)
                    adapter.notifyDataSetChanged();
                endOfList = false;
                inLoading = false;
                rootEmptyView.setVisibility(View.GONE);
                getList();
            } else if (requestCode == REQUEST_EDIT_CODE) {
                String result = data.getStringExtra("result");
                Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                endOfList = false;
                consultantEstates.clear();
                getList();
            }
        }
    }

    private String getStatusName(int id) {
        for (EstateStatus status : statusList) {
            if (Integer.parseInt(status.getValue()) == id) {
                return status.getText();
            }
        }
        return statusList.size() > 0 ? statusList.get(0).getText() : null;
    }

    @Override
    public void onItemClick(int position, ConsultantEstate depositItems) {
        Intent intent = new Intent(this, EstateConsultantDetailActivity.class);
        intent.putExtra("id", depositItems.getId());
        intent.putExtra("notShowBugReportAndCall", true);
        startActivity(intent);

    }

    @Override
    public void delete(int position, ConsultantEstate depositItems) {
        DeleteEstateDialog deleteEstateDialog = new DeleteEstateDialog(this);
        deleteEstateDialog.setId(depositItems.getId());
        deleteEstateDialog.setListener(this);
        deleteEstateDialog.show();
    }

    @Override
    public void edit(int position, ConsultantEstate depositItems) {
        redirectToEdit(depositItems);
    }


    @Override
    public void extended(int position, ConsultantEstate depositItems) {

        if (depositItems.isExtendedRedirectToEdit()) {
            //TODO redirect editActivity
            //TODO pass extandable
            redirectToEdit(depositItems);
        } else {
            EstateActionReq req = new EstateActionReq();
            req.setId(depositItems.getId());

            roundedLoadingView.setVisibility(View.VISIBLE);
            enableDisableViewGroup(root, false);

            ExtendedEstateService.getInstance().extendedEstateEstate(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
                @Override
                public void onGetError(String error) {
                    if (tvFilterTitle != null) {
                        roundedLoadingView.setVisibility(View.GONE);
                        enableDisableViewGroup(root, true);
                        Toast.makeText(ConsultantEstateActivity.this, error, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onAuthorization() {
                    Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                    intent.putExtra("itemsIndex", 3);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                    startActivity(intent);
                    finish();
                }

                @Override
                public void onSuccess(EstateActionResponse response) {
                    if (tvFilterTitle != null) {
                        roundedLoadingView.setVisibility(View.GONE);
                        enableDisableViewGroup(root, true);
                        if (response.isSuccessed()) {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_extended));
                            removeFromList(depositItems.getId());
                        } else {
                            showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                        }
                    }
                }
            });
        }


    }


    @Override
    public void exitFromArchive(int position, ConsultantEstate depositItems) {

        EstateActionReq req = new EstateActionReq();
        req.setId(depositItems.getId());

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);

        UnSuspendEstateService.getInstance().unSuspendEstateEstate(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(ConsultantEstateActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                intent.putExtra("itemsIndex", 3);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(EstateActionResponse response) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_has_been_exit_from_archived));
                        removeFromList(depositItems.getId());
                    } else {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                    }
                }
            }
        });

    }

    @Override
    public void suspend(int position, ConsultantEstate depositItems) {
        EstateActionReq req = new EstateActionReq();
        req.setId(depositItems.getId());

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);

        SuspendEstateService.getInstance().suspendEstate(this, getResources(), req, new ResponseListener<EstateActionResponse>() {
            @Override
            public void onGetError(String error) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(ConsultantEstateActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                intent.putExtra("itemsIndex", 3);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvCenterTitle.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(EstateActionResponse response) {
                if (tvFilterTitle != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.your_property_has_been_archived));
                        removeFromList(depositItems.getId());
                    } else {
                        showSuccessToast(response.getMessage(), getResources().getString(R.string.operation_encountered_error));
                    }
                }
            }
        });
    }


    @Override
    public void stairs(int position, ConsultantEstate depositItems) {
        stairsRequest(depositItems.getId());

    }

    private void stairsRequest(long id) {

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        StairsReq req = new StairsReq();
        req.setId(id);
        StairsCounsultantService.getInstance().staireCounsultantProperty(this, getResources(), req, new ResponseListener<DepositResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
//                    showErrorDialog(error, 0);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(ConsultantEstateActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(DepositResponse response) {
                if (imgBack != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {

                        if (response.isWalletPaymentNeed()) {
                            User user = Constants.getUser(ConsultantEstateActivity.this);
                            if (user != null) {
                                if (response.isWalletAmountOk()) {
                                    PaymentDialog paymentDialog = new PaymentDialog(ConsultantEstateActivity.this);
                                    paymentDialog.enOrderId = response.getEnOrderId();
                                    paymentDialog.orderId = response.getOrderId();
                                    paymentDialog.orderPrice = response.getOrderPrice();
                                    paymentDialog.orderTitle = response.getOrderTitle();
                                    paymentDialog.walletAmount = response.getWalletAmount();
                                    paymentDialog.paymentTypeEnum = PaymentTypeEnum.STAIRS_REGISTER;
                                    paymentDialog.listener = ConsultantEstateActivity.this;
                                    paymentDialog.show();
                                } else {
                                    CustomDialog successDialog = new CustomDialog(ConsultantEstateActivity.this);
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                                    successDialog.setLottieAnim("error.json", 0);
                                    successDialog.setDialogTitle(response.getOrderTitle());
                                    successDialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                                    successDialog.setDescription("برای ادامه فعالیت، لطفا کیف پول خود را شارژ کنید");
                                    successDialog.setCancelListener(getResources().getString(R.string.cancel), view -> successDialog.dismiss());
                                    successDialog.setOkListener(getString(R.string.increase_wallet), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            successDialog.dismiss();
                                            WalletDialog dialog = new WalletDialog(ConsultantEstateActivity.this);
                                            dialog.setCancelable(true);
                                            dialog.show();
                                        }
                                    });
                                    successDialog.show();
                                }
                            } else {
                                Intent intent = new Intent(ConsultantEstateActivity.this, LoginActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Toast.makeText(ConsultantEstateActivity.this, "در حال حاضر امکان پلکان کردن آگهی وجود ندارد", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });

    }

    private void redirectToEdit(ConsultantEstate depositItems) {
        Intent intent = new Intent(this, EditEstateActivity.class);
        intent.putExtra("id", depositItems.getId());
        intent.putExtra("title", depositItems.getContractType() + " " + depositItems.getPropertyType() + " " + "در " + depositItems.getLocation());
        startActivityForResult(intent, REQUEST_EDIT_CODE);
    }


    private void showSuccessToast(String message, String defualt) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, defualt, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDeleteEstate(long id) {
        removeFromList(id);
    }

    private void removeFromList(long id) {
        for (ConsultantEstate consultantEstate : consultantEstates) {
            if (consultantEstate.getId() == id) {
                consultantEstates.remove(consultantEstate);
                if (resultCount > 0) {
                    resultCount = resultCount - 1;
                    setResultCount();
                }
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (consultantEstates == null || consultantEstates.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }
    }


    private void setResultCount() {
        if (resultCount > 0) {
            tvFilterDetail.setVisibility(View.VISIBLE);
            tvFilterDetail.setText(resultCount + " " + getResources().getString(R.string.estate));
        } else {
            tvFilterDetail.setText(null);
            tvFilterDetail.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShowErrorListener(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        showErrorFromServer(modelStateErrors, message);
    }


}
