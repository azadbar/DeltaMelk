package ir.delta.delta.myEstate;

import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;

public class EstateUserDetailActivity extends BaseActivity {

    @BindView(R.id.frameLayout_estate)
    LinearLayout frameLayoutEstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estate_detail);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            long id = bundle.getLong("id");
            boolean isShowBugReportAndCall = bundle.getBoolean("notShowBugReportAndCall");
            if (id > 0) {
                EstateUserDetailFragment estateDetailFragment = new EstateUserDetailFragment();
                FragmentManager fragMgr = getSupportFragmentManager();
                FragmentTransaction fragTrans = fragMgr.beginTransaction();
                Bundle bundle1 = new Bundle();
                bundle1.putLong("id", id);
                bundle1.putBoolean("notShowBugReportAndCall", isShowBugReportAndCall);
                estateDetailFragment.setArguments(bundle1);
                fragTrans.replace(R.id.frameLayout_estate, estateDetailFragment);
                fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragTrans.commit();
            } else {
                finish();
            }
        }
    }
}
