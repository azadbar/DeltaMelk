package ir.delta.delta.ads;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.TypeCommunityAdsEnum;
import ir.delta.delta.service.ResponseModel.ads.ValueItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CommunityAdsAdapter extends RecyclerView.Adapter<CommunityAdsAdapter.ViewHolder> {


    private final ArrayList<ValueItem> list;
    private final OnItemClickListener listener;

    public CommunityAdsAdapter(ArrayList<ValueItem> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads_community, parent, false);

        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        ValueItem object = list.get(position);

        Glide.with(holder.itemView.getContext()).load(Constants.IMAGE_URL_ADS + object.getImage()).placeholder(R.drawable.common_full_open_on_phone).into(holder.image);
        holder.mainText.setText(object.getKey());
        if (!TextUtils.isEmpty(object.getValue()) && (object.getType().equals(TypeCommunityAdsEnum.isPhone1.name()) ||
                object.getType().equals(TypeCommunityAdsEnum.isPhone1.name()) ||
                object.getType().equals(TypeCommunityAdsEnum.isPhone2.name()) ||
                object.getType().equals(TypeCommunityAdsEnum.isPhone3.name()) ||
                object.getType().equals(TypeCommunityAdsEnum.isMobile.name()))) {
            holder.secondText.setVisibility(View.VISIBLE);
            holder.secondText.setText(object.getValue().trim());
        } else {
            holder.secondText.setVisibility(View.GONE);
        }

        holder.bind(position, object, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.mainText)
        BaseTextView mainText;
        @BindView(R.id.secondText)
        BaseTextView secondText;
        @BindView(R.id.rlText)
        BaseRelativeLayout rlText;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        public void bind(int position, ValueItem object, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, object));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ValueItem object);
    }
}
