package ir.delta.delta.ads;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionAdsCity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetSelectedCityListAdsService;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.SelectedListAdsResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.EqualSpacingItemDecoration;
import ir.delta.delta.util.PreferencesData;

import static androidx.navigation.Navigation.findNavController;

public class SelectedCityListAdsFragment extends BaseFragment implements SelectedCityListAdsAdapter.OnItemClickListener {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rvSubGroupAds)
    RecyclerView rvSubGroupAds;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rlComingSoonCity)
    BaseRelativeLayout rlComingSoonCity;
    @BindView(R.id.rlAddImage)
    CardView rlAddImage;
    private SelectedListAdsResponse response;
    private SelectedCityListAdsAdapter adapter;
    private View v;
    private boolean isMelki;
    private int parentId;
    private String groupTitle;
    private boolean changeCity;
    private String subGroupTitle;
    private int groupid;
    private boolean isShowButtonEstate;
    private boolean isFromSubGroup;
    private int parentGroupId;
    private boolean showList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                onBackPress();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_selected_list_ads, container, false);
            ButterKnife.bind(this, v);

            Bundle bundle = getArguments();
            if (bundle != null) {
                isMelki = bundle.getBoolean("isMelki");
                showList = bundle.getBoolean("showList");
                parentId = bundle.getInt("parentId");
                groupid = bundle.getInt("groupid");
                groupTitle = bundle.getString("groupTitle");
                changeCity = bundle.getBoolean("changeCity");
                subGroupTitle = bundle.getString("subGroupTitle");
                isShowButtonEstate = bundle.getBoolean("isShowButtonEstate");
                isFromSubGroup = bundle.getBoolean("isFromSubGroup");
                parentGroupId = bundle.getInt("parentGroupId");
            }
            getSubGroupRequest();
//            creatList();
        }
        initView();
        return v;

    }

  /*  private void creatList() {
        adsItems = new ArrayList<>();
        adsItems.add(new AdsItemTest(33, "تهران", false, R.drawable.tehran));
        adsItems.add(new AdsItemTest(33, "مشهد", false, R.drawable.mashhad));
        adsItems.add(new AdsItemTest(172, "اصفهان", false, R.drawable.isfehan));
        adsItems.add(new AdsItemTest(33, "کرج", false, R.drawable.karaj));
        adsItems.add(new AdsItemTest(317, "شیراز", false, R.drawable.shiraz));
        adsItems.add(new AdsItemTest(111, "تبریز", false, R.drawable.tabriz));
        adsItems.add(new AdsItemTest(33, "قم", false, R.drawable.qhoam));
        adsItems.add(new AdsItemTest(33, "اهواز", false, R.drawable.ahvaz));
        adsItems.add(new AdsItemTest(33, "کرمانشاه", false, R.drawable.kermanshah));
        int coulumCount = 3;
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        TestAdsAdapter testAdsAdapter = new TestAdsAdapter(adsItems, cellWidth, this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvSubGroupAds.setHasFixedSize(true);
        rvSubGroupAds.setLayoutManager(layoutManager);
        rvSubGroupAds.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvSubGroupAds.setAdapter(testAdsAdapter);
    }*/


    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(setTextSpannable());
    }

    private SpannableStringBuilder setTextSpannable() {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append("انتخاب شهر");
   /*     int start = desc_two.length();
        desc_two.append(" (به ترتیب جمعیت) ");
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(0.7f), start, end, 0);*/
        return desc_two;
    }

    private void getSubGroupRequest() {
        showLoading();
        GetSelectedCityListAdsService.getInstance().getSelectedCityListAds(getResources(), new ResponseListener<SelectedListAdsResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
//                    Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(requireContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(SelectedListAdsResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    SelectedCityListAdsFragment.this.response = response;
                    TransactionAdsCity.getInstance().saveCityAdsTable(requireContext(), response.getAdslist());

                    setDataAdapter();
                }
            }
        });

    }

    private void setDataAdapter() {
        int coulumCount = 3;
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }

        adapter = new SelectedCityListAdsAdapter(response.getAdslist(), cellWidth, this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvSubGroupAds.setHasFixedSize(true);
        rvSubGroupAds.setLayoutManager(layoutManager);
        rvSubGroupAds.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvSubGroupAds.setAdapter(adapter);
    }


    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvSubGroupAds.setVisibility(View.GONE);
        loadingView.showLoading(false);
        rlAddImage.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvSubGroupAds.setVisibility(View.VISIBLE);
        rlAddImage.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.rlBack)
    public void onViewClicked() {
//        getActivity().onBackPressed();
        onBackPress();

    }

    private void onBackPress() {
        int cityId = PreferencesData.getCityId(requireActivity());
        City city = TransactionAdsCity.getInstance().getCityById(requireActivity(), cityId);
        if (city != null) {
            if (isFromSubGroup) {
                findNavController(rlBack).popBackStack(R.id.subGroupAdsFragment, false);
            } else {
                findNavController(rlBack).popBackStack(R.id.listAdsFragment, false);
            }

        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onItemShare(int position, ArrayList<City> city) {
        PreferencesData.saveCityId(requireContext(), city.get(position).getId());
        Constants.setCurrentCityAds(city.get(position));

        Bundle b = new Bundle();
        b.putInt("parentId", parentId);
        b.putInt("groupid", groupid);
        b.putBoolean("isMelki", isMelki);
        b.putBoolean("showList", showList);
        b.putBoolean("isShowButtonEstate", isShowButtonEstate);
        b.putString("groupTitle", groupTitle);
        b.putInt("parentGroupId", parentGroupId);
        if (isMelki || changeCity || showList) {
            if (isMelki) {
                b.putString("subGroupTitle", groupTitle);
            } else {
                b.putString("subGroupTitle", subGroupTitle);
            }
            findNavController(rvSubGroupAds).navigate(R.id.action_selected_city_to_sub_list, b);
        } else {
            findNavController(rvSubGroupAds).navigate(R.id.action_selected_city_to_sub_group, b);
        }

    }


}
