package ir.delta.delta.ads;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.AdsItemTest;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SelectedCityListAdsAdapter extends RecyclerView.Adapter<SelectedCityListAdsAdapter.ViewHolder> {



    private final ArrayList<City> list;
    private final int cellWidth;
    private final OnItemClickListener listener;


    public SelectedCityListAdsAdapter(ArrayList<City> list, int cellWidth, OnItemClickListener listener) {
        this.list = list;
        this.cellWidth = cellWidth;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_city, parent, false);
        itemView.getLayoutParams().height = cellWidth;
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        City sub = list.get(position);
        holder.title.setText(sub.getTitle());
        Glide.with(holder.itemView.getContext()).load(Constants.URL_BASE_STATIC +sub.getImagePath()).into(holder.image);
//        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));

        holder.bind(list, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.title)
        BaseTextView title;
        @BindView(R.id.cvRoot)
        CardView cvRoot;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(ArrayList<City> adsItem, int position, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemShare(position, adsItem));
        }
    }


    public interface OnItemClickListener {

        void onItemShare(int position, ArrayList<City> demand);
    }

}
