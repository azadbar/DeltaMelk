package ir.delta.delta.ads;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetRegionListAdsService;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.ListRegionItem;
import ir.delta.delta.service.ResponseModel.ads.RegionResponse;
import ir.delta.delta.util.Constants;

public class RegionAdsActivity extends BaseActivity implements RegionAdsAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rlRoot)
    BaseRelativeLayout rlRoot;
    @BindView(R.id.rvProvince)
    RecyclerView rvProvince;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.edtSearchBar)
    BaseEditText edtSearchBar;
    private RegionResponse response;
    private RegionAdsAdapter adapter;
    private int cityId;
    private int groupId;
    private final ArrayList<ListRegionItem> filterList = new ArrayList<>();

    public RegionAdsActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_province_ads);
        ButterKnife.bind(this);
        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            cityId = arguments.getInt("cityId");
            groupId = arguments.getInt("groupId");
        }
        initView();
        getProvinceRequest();

        edtSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence == null || charSequence.toString().trim().isEmpty()) {
                    filterList.clear();
                    filterList.addAll(response.getListRegion());
                } else {
                    filterList.clear();
                    String searchText = charSequence.toString().trim();
                    for (ListRegionItem item : response.getListRegion()) {
                        if (item.getTitle().contains(searchText)) {
                            filterList.add(item);
                        }
                    }
                }
                adapter.setList(filterList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        loadingView.setButtonClickListener(view -> getProvinceRequest());
    }


    private void getProvinceRequest() {
        showLoading();

        GetRegionListAdsService.getInstance().getRegionListAds(getResources(), cityId, groupId, new ResponseListener<RegionResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    Toast.makeText(RegionAdsActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(RegionAdsActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(RegionResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    RegionAdsActivity.this.response = response;
                    filterList.clear();
                    filterList.addAll(response.getListRegion());
                    setAdapter();
                }
            }
        });
    }

    private void setAdapter() {
        adapter = new RegionAdsAdapter(filterList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(RegionAdsActivity.this, RecyclerView.VERTICAL, false);
        rvProvince.setHasFixedSize(true);
        rvProvince.setLayoutManager(layoutManager);
        rvProvince.setAdapter(adapter);
    }

    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.select_region));
        tvFilterTitle.setTextColor(getResources().getColor(R.color.black));
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.white));
        edtSearchBar.setHint("جستجوی محله");
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvProvince.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }


    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvProvince.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemClick(int position, ListRegionItem object) {
//        Bundle result = new Bundle();
////        result.putString("locationTitle",  object.getTitle());
////        result.putInt("locationId",  object.getId());
////        getParentFragmentManager().setFragmentResult("requestKey", result);
////        findNavController(rlRoot).navigate(R.id.action_region_to_filter);
        Intent resultIntent = new Intent();
        resultIntent.putExtra("locationTitle", object.getTitle());
        resultIntent.putExtra("locationId", object.getId());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
//        getActivity().onBackPressed();
//        Navigation.findNavController(rlBack).navigateUp();
        finish();
    }
}
