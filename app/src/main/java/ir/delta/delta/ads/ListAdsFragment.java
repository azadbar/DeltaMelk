package ir.delta.delta.ads;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionAdsCity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetAllListAdsService;
import ir.delta.delta.service.Request.ads.GetFilterListAdsService;
import ir.delta.delta.service.RequestModel.FilterAdsReq;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.service.ResponseModel.ads.AllListAdsResponse;
import ir.delta.delta.service.ResponseModel.ads.AllListItem;
import ir.delta.delta.splash.SplashConsultantActivity;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import ir.metrix.sdk.Metrix;

import static androidx.navigation.Navigation.findNavController;

public class ListAdsFragment extends BaseFragment implements AllListAdsAdapter.onItemClick {

    private static final int REQUEST_FILTER = 5;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rvAddsList)
    RecyclerView rvAddsList;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.rlLoading)
    ProgressBar rlLoading;
    @BindView(R.id.tvContractType)
    BaseTextView tvContractType;
    @BindView(R.id.countAds)
    BaseTextView countAds;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.imgFilter)
    BaseImageView imgFilter;
    @BindView(R.id.tvChangeCity)
    BaseTextView tvChangeCity;
    @BindView(R.id.changeCity)
    BaseRelativeLayout changeCity;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    @BindView(R.id.real_estate_sector)
    BaseRelativeLayout realEstateSector;
    @BindView(R.id.rlContractType)
    BaseRelativeLayout rlContractType;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.edtEstateCode)
    BaseEditText edtEstateCode;
    @BindView(R.id.btnClear)
    BaseImageView btnClear;
    @BindView(R.id.btnSearch)
    BaseImageView btnSearch;
    @BindView(R.id.rlSearch)
    BaseRelativeLayout rlSearch;
    @BindView(R.id.horizontal_progress)
    ProgressBar horizontalProgress;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;


    private int groupid;
    private AllListAdsResponse response;
    private AllListAdsAdapter adapter;
    private boolean inLoading;
    private boolean endOfList;
    private int pageNumber = 1;
    private boolean isCountServer = true;
    private final ArrayList<AllListItem> list = new ArrayList<>();
    private int parentId;
    private LinearLayoutManager layoutManager;
    private ArrayList<AdsItem> groupList;
    private String groupTitle;
    private String subGroupTitle;
    private FilterAdsReq filterReq;
    private boolean isMelki;
    private View v;
    private Bundle bundle;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Bundle params;
    private boolean isShowButtonEstate;
    private boolean isSwipeRefresh;
    private TextWatcher textWatcher;
    private int parentGroupId;
    private boolean showList;
    private long mLastClickTime = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                onBackPress();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_list_ads, container, false);
            ButterKnife.bind(this, v);

            bundle = getArguments();
            if (bundle != null) {
                groupid = bundle.getInt("groupid", 0);
                isMelki = bundle.getBoolean("isMelki");
                showList = bundle.getBoolean("showList");
                parentId = bundle.getInt("parentId", 0);
                groupTitle = bundle.getString("groupTitle");
                subGroupTitle = bundle.getString("subGroupTitle");
                isShowButtonEstate = bundle.getBoolean("isShowButtonEstate");
                parentGroupId = bundle.getInt("parentGroupId");
            }

            if (isMelki) {//because melki is group and dont have subgroup
                groupid = parentId;
                parentId = 0;
            }
            getList();

        }
        initView();
        return v;
    }

    private void getFilterListRequest() {
        if (pageNumber == 1)
            showLoading();
        filterReq.setPageNumber(pageNumber);
        filterReq.setCount(isCountServer);
        GetFilterListAdsService.getInstance().getFilterListAds(getResources(), filterReq, new ResponseListener<AllListAdsResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(AllListAdsResponse response) {
                stopLoading();
                horizontalProgress.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    rvAddsList.setVisibility(View.VISIBLE);
                    rvAddsList.animate().alpha(1.0f);
                    isSwipeRefresh = false;
                    swipeRefresh.setRefreshing(false);
                    ListAdsFragment.this.response = response;
                    setCountList();
                    setDataLoading(response.getAdslist().size());
                    setList(response.getAdslist());

                }
            }
        });
    }


    private void getList() {
        if (!inLoading && !endOfList) {
            disableEmptyView();
            inLoading = true;
            if (list.isEmpty()) {
                if (isSwipeRefresh) {
                    rvAddsList.animate().alpha(0.0f).setStartDelay(700);
//                    recyclerView.setVisibility(View.GONE);
//                    realEstateSector.setVisibility(View.GONE);
//                    imageSlider.setVisibility(View.GONE);
                } else {
                    showLoading();
                }
            } else {
                rlLoading.setVisibility(View.VISIBLE);
            }
            if (filterReq != null) {
                getFilterListRequest();
            } else {
                getSubGroupRequest();
            }

        }
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.statusBarRedTrans));
        }
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.white));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);

        tvFilterTitle.setTextColor(getResources().getColor(R.color.white));
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.redColor));
        rlFilterChange.setVisibility(View.VISIBLE);
        tvFilterChnage.setText(getResources().getString(R.string.filter) + " " + getResources().getString(R.string.advertis));
        rlFilterChange.setBackground(getResources().getDrawable(R.drawable.strock_white));
        rlFilterChange.setPadding(16, 8, 16, 8);
        imgFilter.setVisibility(View.VISIBLE);
        horizontalProgress.setVisibility(View.GONE);
        if (this.edtEstateCode.requestFocus()) {
            requireActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }

        rvAddsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    int visibleItemCount = layoutManager.getChildCount();
                    int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                    int totalItemCount = layoutManager.getItemCount();
                    if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                        if (firstVisibleItem != 0 || visibleItemCount != 0) {
                            getList();
                        }
                }
            }
        });

        if (isMelki && isShowButtonEstate) {
            realEstateSector.setVisibility(View.GONE);
        } else {
            realEstateSector.setVisibility(View.GONE);
        }

        rvAddsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
                    slideDown();
                } else {
                    slideUp();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        swipeRefresh.setOnRefreshListener(() -> getRefreshData(true));

        textWatcher = new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 2000; // milliseconds

            @Override
            public void afterTextChanged(Editable s) {

                btnClear.setVisibility(View.VISIBLE);
                btnSearch.setVisibility(View.GONE);
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty()) {
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(() -> {
                                            if (TextUtils.isEmpty(s.toString().trim())) {
                                                filterReq = null;
                                                visibleOrGoneSearchUi();
                                                getRefreshData(false);
                                            } else {
                                                list.clear();
                                                endOfList = false;
                                                inLoading = false;
//                                            empty.setVisibility(View.GONE);
                                                getSearchTextRequest();
                                            }
                                            Constants.hideKeyboard(requireActivity());
                                        });
                                    }
                                }
                            },
                            DELAY
                    );
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty()) {
                    horizontalProgress.setVisibility(View.VISIBLE);
                    if (timer != null) {
                        timer.cancel();
                    }
                }

            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();
        edtEstateCode.addTextChangedListener(textWatcher);
    }

    @Override
    public void onPause() {
        super.onPause();
        edtEstateCode.removeTextChangedListener(textWatcher);
    }

    private void visibleOrGoneSearchUi() {
        btnClear.setVisibility(View.GONE);
        btnSearch.setVisibility(View.VISIBLE);
        horizontalProgress.setVisibility(View.GONE);
    }

    private void getRefreshData(boolean isSwipe) {
        list.clear();
        endOfList = false;
        inLoading = false;
        isSwipeRefresh = isSwipe;
        pageNumber = 1;
        isCountServer = true;
        if (adapter != null)
            adapter.notifyDataSetChanged();
        getList();
    }

    private void getSearchTextRequest() {
        horizontalProgress.setVisibility(View.VISIBLE);
        int cityId = PreferencesData.getCityId(requireContext());
        City city = TransactionAdsCity.getInstance().getCityById(requireContext(), cityId);
        filterReq = new FilterAdsReq();
        filterReq.setLocationId(city.getId());
        filterReq.setGroupId(groupid);
        filterReq.setParentGroupId(parentId);
        filterReq.setTextSearch(edtEstateCode.getValueString());
        pageNumber = 1;
        isCountServer = true;
        disableEmptyView();
        getFilterListRequest();
    }

    private void getSubGroupRequest() {
        GetAllListAdsService.getInstance().getAllListAds(getResources(), groupid, pageNumber, isCountServer, parentId, Constants.getAdsCity(requireContext()).getId(), new ResponseListener<AllListAdsResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
//                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(AllListAdsResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    rvAddsList.setVisibility(View.VISIBLE);
                    rvAddsList.animate().alpha(1.0f);
                    isSwipeRefresh = false;
                    swipeRefresh.setRefreshing(false);
                    ListAdsFragment.this.response = response;
                    setCountList();
                    setDataLoading(response.getAdslist().size());
                    setList(response.getAdslist());

                }
            }
        });

    }

    private void setCountList() {
        tvFilterTitle.setText("نیازمندی های " + groupTitle);
        if (isMelki || showList) {
            tvContractType.setText(groupTitle);
            edtEstateCode.setHint(getResources().getString(R.string.search) + " "
                    + getResources().getString(R.string.bar_asas) + " " + groupTitle);
        } else {
            tvContractType.setText(subGroupTitle);
            edtEstateCode.setHint(getResources().getString(R.string.search) + " "
                    + getResources().getString(R.string.bar_asas) + " " + subGroupTitle);
        }

        if (isCountServer)
            countAds.setText(response.getCount() + " " + getResources().getString(R.string.advertis));
        tvLocation.setText(setTextSpannable());
    }

    private SpannableStringBuilder setTextSpannable() {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(getResources().getString(R.string.selected_your_city)).append(": ");
        int start = desc_two.length();
        desc_two.append(Constants.getAdsCity(requireContext()).getTitle());
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.2f), start, end, 0);
        return desc_two;
    }

    private void setList(ArrayList<AllListItem> adslist) {
        this.list.addAll(adslist);
        if (list.size() > 0) {
            rvAddsList.setVisibility(View.VISIBLE);
            setDataAdapter();
        } else {
            emptyView();
        }

    }

    private void emptyView() {
        empty.setVisibility(View.VISIBLE);
    }

    private void disableEmptyView() {
        empty.setVisibility(View.GONE);
    }

    private void setDataLoading(int size) {
        if (size < 30)
            endOfList = true;
        inLoading = false;
        isCountServer = false;
        pageNumber++;
        rlLoading.setVisibility(View.GONE);
    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new AllListAdsAdapter(list, this);
            layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvAddsList.setHasFixedSize(true);
            rvAddsList.setLayoutManager(layoutManager);
            rvAddsList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvAddsList.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvAddsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position, AllListItem allListItem) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){//In my case, if the user clicks the same view twice very very quickly ,this crash will occur.
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        Bundle bundle = new Bundle();
        bundle.putInt("infoId", allListItem.getId());
        bundle.putBoolean("isMelki", isMelki);
        bundle.putBoolean("showList", showList);
        bundle.putInt("parentGroupId", parentGroupId);
//        findNavController(rlFilterChange).navigate(R.id.action_list_to_detail, bundle);
        Navigation.findNavController(getView()).navigate(R.id.action_list_to_detail, bundle);
//        clearView();
    }

    private void clearView() {
        endOfList = false;
        list.clear();
        isCountServer = true;
        pageNumber = 1;
    }

    @OnClick({R.id.rlBack, R.id.rlFilterChange, R.id.changeCity, R.id.real_estate_sector,
            R.id.rlContractType, R.id.btnClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
            case R.id.rlContractType:
                onBackPress();
                break;
            case R.id.rlFilterChange:
                Intent intent = new Intent(getActivity(), FilterAdsActivity.class);
                intent.putExtra("parentId", parentId);
                intent.putExtra("groupid", groupid);
                intent.putExtra("isMelki", isMelki);
                intent.putExtra("filterReq", filterReq);
                startActivityForResult(intent, REQUEST_FILTER);
//                clearView();
                break;
            case R.id.changeCity:
                /*NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_ads_fragment);
                NavGraph navGraph = navController.getNavInflater().inflate(R.navigation.nav_graph);
                navGraph.setStartDestination(R.id.selectedCityList);*/
                Bundle bundle = new Bundle();
                bundle.putBoolean("changeCity", true);
                if (isMelki) {
                    bundle.putInt("parentId", groupid);
                } else {
                    bundle.putInt("parentId", parentId);
                    bundle.putInt("groupid", groupid);
                }

                bundle.putBoolean("isMelki", isMelki);
                bundle.putBoolean("showList", showList);
                bundle.putString("groupTitle", groupTitle);
                bundle.putString("subGroupTitle", subGroupTitle);
                bundle.putInt("parentGroupId", parentGroupId);
//                navController.setGraph(navGraph,bundle);
                findNavController(changeCity).navigate(R.id.selectedCityList, bundle);
                break;
            case R.id.real_estate_sector:
                intent = new Intent(requireActivity(), SplashConsultantActivity.class);
                intent.putExtra("isFirst", false);
                startActivity(intent);

                params = new Bundle();
                params.putString("realEstate", "selectRealStateSection");
                mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireActivity());
                mFirebaseAnalytics.logEvent("section_real_estate", params);
                Metrix.getInstance().newEvent("xozon");
                break;
            case R.id.btnClear:
                filterReq = null;
                edtEstateCode.setBody("");
                visibleOrGoneSearchUi();
                getRefreshData(false);
                break;
        }
    }


    private void onBackPress() {
        if (isMelki || showList) {
            getActivity().finish();
        } else {
            findNavController(rlBack).popBackStack(R.id.subGroupAdsFragment, false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_FILTER) {
                if (data != null) {
                    clearView();
                    filterReq = (FilterAdsReq) data.getSerializableExtra("filterReq");
                    pageNumber = 1;
                    isCountServer = true;
                    disableEmptyView();
                    getFilterListRequest();
                } else {
                    Toast.makeText(getActivity(), "دوبتره تلاش کنید", Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    private void slideUp() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(realEstateSector, "translationY", 0);
        animation2.setDuration(500);
        animation2.start();
    }

    private void slideDown() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(realEstateSector, "translationY", +realEstateSector.getHeight() + 200);
        animation2.setDuration(500);
        animation2.start();
    }
}
