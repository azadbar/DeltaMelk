package ir.delta.delta.ads;

import android.annotation.SuppressLint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.slider.CardSliderAdapter;
import ir.delta.delta.util.Constants;

import static ir.delta.delta.baseView.BaseFragment.ellipsizeText;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SlideListMainAdsAdapter extends CardSliderAdapter<SlideListMainAdsAdapter.ViewHolder> {


    private final ArrayList<AdsItem> list;
    private final int cellWidth;
    private final OnItemClickListener listener;


    public SlideListMainAdsAdapter(ArrayList<AdsItem> list, int cellWidth, OnItemClickListener listener) {
        this.list = list;
        this.cellWidth = cellWidth;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slide_main_ads_, parent, false);
        return new ViewHolder(itemView, cellWidth);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AdsItem sub = list.get(position);
        holder.title.setText(sub.getTitle());
        Glide.with(holder.itemView.getContext()).load(Constants.URL_BASE_STATIC + sub.getFirstImage()).into(holder.image);
//        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));
        ellipsizeText(holder.title,40);
        holder.bind(sub, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void bindVH(@NotNull ViewHolder holder, int position) {
        AdsItem sub = list.get(position);
        holder.title.setText(sub.getTitle());
        Glide.with(holder.itemView.getContext()).load(Constants.URL_BASE_STATIC + sub.getFirstImage()).into(holder.image);
//        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));
        ellipsizeText(holder.title,40);
        holder.bind(sub, position, listener);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.title)
        BaseTextView title;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;

        ViewHolder(View view, int cellWidth) {
            super(view);
            ButterKnife.bind(this, view);
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(cellWidth, cellWidth * 2 / 3);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(0, 0, itemView.getResources().getDimensionPixelOffset(R.dimen.margin_2), 0);
            itemView.setLayoutParams(layoutParams);
        }

        public void bind(AdsItem adsItem, int position, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onSlideListItem(position, adsItem));
        }
    }


    public interface OnItemClickListener {

        void onSlideListItem(int position, AdsItem demand);
    }

}
