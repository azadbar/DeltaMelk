package ir.delta.delta.ads;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;

import android.os.Bundle;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.database.TransactionAdsCity;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class AdsActivity extends BaseActivity {


    private boolean isMelki;
    private int infoId;
    private boolean isMainPage;
    private boolean isOpenDetail;
    private boolean showList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_ads_fragment);
        NavGraph navGraph = navController.getNavInflater().inflate(R.navigation.nav_graph);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isMelki = bundle.getBoolean("isMelki");
            showList = bundle.getBoolean("showList");
            infoId = bundle.getInt("infoId");
            isMainPage = bundle.getBoolean("infoId", false);
            isOpenDetail = bundle.getBoolean("isOpenDetail", false);
        }

        int cityId = PreferencesData.getCityId(this);
        City city = TransactionAdsCity.getInstance().getCityById(this, cityId);
        if (isOpenDetail) {
            navGraph.setStartDestination(R.id.detailAdsFragment);
        }else {
            if (city != null) {
                //go to melk or other fragment
                Constants.setCurrentCityAds(city);
                if (isMelki || showList) {
                    navGraph.setStartDestination(R.id.listAdsFragment);
                } else {
                    navGraph.setStartDestination(R.id.subGroupAdsFragment);
                }
            } else {
                //go to selected city fragment
                navGraph.setStartDestination(R.id.selectedCityList);
            }
        }

        navController.setGraph(navGraph, getIntent().getExtras());
    }
}
