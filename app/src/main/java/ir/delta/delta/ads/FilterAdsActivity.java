package ir.delta.delta.ads;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.service.RequestModel.FilterAdsReq;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class FilterAdsActivity extends BaseActivity {


    private static final int REGION_CODE = 3;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.locationTitle)
    BaseTextView locationTitle;
    @BindView(R.id.selectedLocation)
    BaseRelativeLayout selectedLocation;
    @BindView(R.id.rlLocation)
    BaseLinearLayout rlLocation;
    @BindView(R.id.typeAdsTitle)
    BaseTextView typeAdsTitle;
    @BindView(R.id.selectedRegion)
    BaseRelativeLayout selectedRegion;
    @BindView(R.id.edtMinMeter)
    CustomEditText edtMinMeter;
    @BindView(R.id.rlMinMeter)
    BaseRelativeLayout rlMinMeter;
    @BindView(R.id.edtMaxMeter)
    CustomEditText edtMaxMeter;
    @BindView(R.id.rlMaxMeter)
    BaseRelativeLayout rlMaxMeter;
    @BindView(R.id.edtMinPrice)
    CustomEditText edtMinPrice;
    @BindView(R.id.rlMinPrice)
    BaseRelativeLayout rlMinPrice;
    @BindView(R.id.edtMaxPrice)
    CustomEditText edtMaxPrice;
    @BindView(R.id.rlMaxPrice)
    BaseRelativeLayout rlMaxPrice;
    @BindView(R.id.rlParameter)
    BaseLinearLayout rlParameter;
    @BindView(R.id.tvApplyFilters)
    BaseTextView tvApplyFilters;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rlRegion)
    BaseLinearLayout rlRegion;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.my_location)
    BaseImageView myLocation;
    @BindView(R.id.regionText)
    BaseTextView regionText;
    @BindView(R.id.tvFilter)
    BaseRelativeLayout tvFilter;
    @BindView(R.id.locationText)
    BaseTextView locationText;
    @BindView(R.id.clear)
    BaseTextView clear;


    private String locationTitleTxt;
    private int selectedSubGroupId;
    private boolean isMelki;
    private FilterAdsReq filterReq;
    private String typeAdsTitleTxt;

    // location last updated time
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates;

    private int parentId;
    private int groupId;
    private int locationId;
    private final boolean firstTimeLocationFound = false;
    private int beforRegion;


    public FilterAdsActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_filter_ads);
        ButterKnife.bind(this);
        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            parentId = arguments.getInt("parentId");//منظور beforGroupId  است سر منشا آیدی ها
            groupId = arguments.getInt("groupid");//منظور groupid  است
            isMelki = arguments.getBoolean("isMelki");
            filterReq = (FilterAdsReq) arguments.getSerializable("filterReq");

        }

        initView();
        restoreValuesFromBundle(savedInstanceState);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }
    }


    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.filter_ads));
        tvFilterTitle.setTextColor(getResources().getColor(R.color.black));
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.white));
        tvFilter.setEnabled(false);
        clear.setVisibility(View.VISIBLE);
        if (PreferencesData.getRegionAdsId(this) > 0) {
            setRegionText(getString(R.string.region_text) + " " + PreferencesData.getRegionAdsText(this));
            beforRegion = PreferencesData.getRegionAdsId(this);
        } else {
            setRegionText(getString(R.string.region_text));
        }


        if (Constants.getAdsCity(this).getId() == 33) {
            rlRegion.setVisibility(View.VISIBLE);
        } else {
            rlRegion.setVisibility(View.GONE);
        }
        if (isMelki) {
            rlParameter.setVisibility(View.GONE);

        } else {
            rlParameter.setVisibility(View.GONE);
        }


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                disableRegion();
            }
        };

        mRequestingLocationUpdates = false;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void setRegionText(String text) {
        typeAdsTitle.setText(text);
    }

    /**
     * Restoring values from saved instance state
     */
    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

        }

    }


    @OnClick({R.id.selectedLocation, R.id.selectedRegion, R.id.rlBack, R.id.tvFilter, R.id.clear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.selectedLocation:
                Dexter.withActivity(this)
                        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                mRequestingLocationUpdates = true;
                                visibleLoadingProgress();
                                startLocationUpdates();
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {
                                if (response.isPermanentlyDenied()) {
                                    openSettings();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
                break;
            case R.id.selectedRegion:
                Intent intent = new Intent(this, RegionAdsActivity.class);
                intent.putExtra("cityId", Constants.getAdsCity(this).getId());
                intent.putExtra("groupId", groupId);
                startActivityForResult(intent, REGION_CODE);
                break;
            case R.id.rlBack:
                if (beforRegion == 0) {
                    filterRequest();
                } else {
                    finish();
                }
                break;
            case R.id.tvFilter:
                filterRequest();
                break;
            case R.id.clear:
                mCurrentLocation = null;
                mRequestingLocationUpdates = false;
                stopLocationUpdates();
                setRegionText(getString(R.string.region_text));
                PreferencesData.setRegionAdsText(this, null);
                PreferencesData.setRegionAdsId(this, Constants.getAdsCity(this).getId());//clear locationId
                enableView();
                beforRegion = 0;
                break;
        }
    }

    /**
     * Starting location updates
     * Check whether location settings are satisfied and then
     * location updates will be requested
     */
    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    /*    Toast.makeText(FilterAdsActivity.this, "Lat: " + mCurrentLocation.getLatitude() + ", " +
                                "Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_SHORT).show();*/

                    }
                })
                .addOnFailureListener(this, e -> {
                    int statusCode = ((ApiException) e).getStatusCode();
                    switch (statusCode) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException rae = (ResolvableApiException) e;
                                rae.startResolutionForResult(FilterAdsActivity.this, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sie) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            String errorMessage = "Location settings are inadequate, and cannot be " +
                                    "fixed here. Fix in Settings.";
                            Toast.makeText(FilterAdsActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                    }

                });
    }

    private void visibleLoadingProgress() {
        progress.setVisibility(View.VISIBLE);
        myLocation.setVisibility(View.INVISIBLE);
    }

    private void disableLocationSection() {
        selectedLocation.setEnabled(false);
        locationTitle.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        locationText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        mCurrentLocation = null;
        selectedRegion.setBackground(getResources().getDrawable(R.drawable.strock_red_low_raduis));
        disableEnableBtnFilter();
    }

    private void enableView() {
        selectedLocation.setBackground(getResources().getDrawable(R.drawable.strock_gray_low_raduis));
        selectedRegion.setBackground(getResources().getDrawable(R.drawable.strock_gray_low_raduis));
        selectedLocation.setEnabled(true);
        selectedRegion.setEnabled(true);
        myLocation.setVisibility(View.VISIBLE);
        myLocation.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_baseline_my_location_24));
        myLocation.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
        tvFilter.setBackground(getResources().getDrawable(R.drawable.ripple_disable_radius));
        typeAdsTitle.setTextColor(getResources().getColor(R.color.primaryTextColor));
        locationTitle.setTextColor(getResources().getColor(R.color.primaryTextColor));
        tvFilter.setFocusable(false);
        tvFilter.setEnabled(false);
    }

    private void disableRegion() {
        selectedLocation.setBackground(getResources().getDrawable(R.drawable.strock_red_low_raduis));
        progress.setVisibility(View.GONE);
        myLocation.setVisibility(View.VISIBLE);
        myLocation.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
        myLocation.setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_ATOP);
        selectedRegion.setEnabled(false);
        typeAdsTitle.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        PreferencesData.setRegionAdsId(this, Constants.getAdsCity(this).getId());
        PreferencesData.setRegionAdsText(this, null);
        regionText.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        setRegionText(getString(R.string.region_text));
        disableEnableBtnFilter();
    }

    private void disableEnableBtnFilter() {
        tvFilter.setBackground(getResources().getDrawable(R.drawable.ripple_red_raduis));
        tvFilter.setFocusable(true);
        tvFilter.setEnabled(true);
    }


    private void filterRequest() {
        FilterAdsReq req = new FilterAdsReq();
        if (mCurrentLocation != null) {
            req.setLatitude(mCurrentLocation.getLatitude());
            req.setLongitude(mCurrentLocation.getLongitude());
        }
        req.setGroupId(groupId);
        req.setParentGroupId(parentId);
        req.setLocationId(PreferencesData.getRegionAdsId(this));
        req.setTextSearch("");
        Intent resultIntent = new Intent();
        resultIntent.putExtra("filterReq", req);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
     /*   Bundle bundle = new Bundle();
        bundle.putSerializable("filterReq", req);
        Navigation.findNavController(selectedLocation).navigate(R.id.action_filter_to_list, bundle);*/

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        break;
                    case Activity.RESULT_CANCELED:
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
            case REGION_CODE:
                if (data != null) {
                    setRegionText(getString(R.string.region_text) + getString(R.string.dots) + " " + makeText(data.getStringExtra("locationTitle")));
                    disableLocationSection();
                    locationId = data.getIntExtra("locationId", 0);
                    PreferencesData.setRegionAdsText(this, data.getStringExtra("locationTitle"));
                    PreferencesData.setRegionAdsId(this, locationId);
                }

                break;
        }
    }

    private SpannableStringBuilder makeText(String text) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        int start = desc_two.length();
        desc_two.append(text);
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.2f), start, end, 0);
        return desc_two;
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void stopLocationUpdates() {
        // Removing location updates
        mFusedLocationClient
                .removeLocationUpdates(mLocationCallback)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                    }
                });
    }


}
