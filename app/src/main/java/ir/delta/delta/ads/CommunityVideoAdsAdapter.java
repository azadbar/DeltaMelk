package ir.delta.delta.ads;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CommunityVideoAdsAdapter extends RecyclerView.Adapter<CommunityVideoAdsAdapter.ViewHolder> {


    private final ArrayList<String> list;
    private final String key;
    private final String image;
    private final OnItemClickListener listener;


    public CommunityVideoAdsAdapter(ArrayList<String> list, String key, String image,OnItemClickListener listener) {
        this.list = list;
        this.key = key;
        this.image = image;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads_community, parent, false);

        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        String object = list.get(position);

        Glide.with(holder.itemView.getContext()).load(Constants.IMAGE_URL_ADS + image).placeholder(R.drawable.telegram).into(holder.image);
        holder.mainText.setText(key);

        holder.bind(position, object, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.mainText)
        BaseTextView mainText;
        @BindView(R.id.secondText)
        BaseTextView secondText;
        @BindView(R.id.rlText)
        BaseRelativeLayout rlText;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        public void bind(int position, String object, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, object));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, String  object);
    }
}
