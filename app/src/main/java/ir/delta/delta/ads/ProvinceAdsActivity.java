package ir.delta.delta.ads;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetProvinceListAdsService;
import ir.delta.delta.service.ResponseModel.ads.ListProvinceItem;
import ir.delta.delta.service.ResponseModel.ads.ProvinceResponse;
import ir.delta.delta.util.Constants;

import static androidx.navigation.Navigation.findNavController;

public class ProvinceAdsActivity extends BaseActivity implements ProvinceAdsAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rlRoot)
    BaseRelativeLayout rlRoot;
    @BindView(R.id.rvProvince)
    RecyclerView rvProvince;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.loadingView)
    LoadingView loadingView;

    private ProvinceResponse response;
    private ProvinceAdsAdapter adapter;
    private int groupId;

    public ProvinceAdsActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_province_ads);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            groupId = extras.getInt("groupid");
        }
        initView();
        getProvinceRequest();
    }

 /*   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_province_ads, container, false);
        ButterKnife.bind(this, v);
        initView();
        getProvinceRequest();

        loadingView.setButtonClickListener(view -> getProvinceRequest());
        return v;
    }*/

    private void getProvinceRequest() {
        showLoading();

        GetProvinceListAdsService.getInstance().getProvinceListAds(getResources(),groupId,new ResponseListener<ProvinceResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    Toast.makeText(ProvinceAdsActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ProvinceAdsActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(ProvinceResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    ProvinceAdsActivity.this.response = response;
                    setAdapter();
                }
            }
        });
    }

    private void setAdapter() {
        adapter = new ProvinceAdsAdapter(response.getListProvince(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ProvinceAdsActivity.this, RecyclerView.VERTICAL, false);
        rvProvince.setHasFixedSize(true);
        rvProvince.setLayoutManager(layoutManager);
        rvProvince.setAdapter(adapter);
    }

    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white));
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.select_province));
        tvFilterTitle.setTextColor(getResources().getColor(R.color.black));
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvProvince.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }


    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvProvince.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position, ListProvinceItem object) {
      /*  Bundle bundle = new Bundle();
        bundle.putInt("provinceId", object.getId());
        Navigation.findNavController(rlRoot).navigate(R.id.action_province_to_city, bundle);*/
      Intent intent = new Intent(ProvinceAdsActivity.this, CityAdsActivity.class);
      intent.putExtra("provinceId", object.getId());
      intent.putExtra("groupId", groupId);
      startActivityForResult(intent, 1);
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
//        getActivity().onBackPressed();
       finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == 1){
                Intent resultIntent = new Intent();
                resultIntent.putExtra("locationTitle",data.getStringExtra("locationTitle"));
                resultIntent.putExtra("locationId",  data.getIntExtra("locationId",0));
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }
    }
}
