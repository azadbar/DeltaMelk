package ir.delta.delta.ads;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;

public class SecondAdsActivity extends BaseActivity {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgeLeftAds)
    BaseImageView imgeLeftAds;
    @BindView(R.id.imgAds)
    BaseImageView imgAds;
    @BindView(R.id.imgeRightAds)
    BaseImageView imgeRightAds;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_second);
        ButterKnife.bind(this);
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        imgAds.setVisibility(View.VISIBLE);
        imgeLeftAds.setVisibility(View.VISIBLE);
        imgeRightAds.setVisibility(View.VISIBLE);


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        MainAdsFragment containerFragment = new MainAdsFragment();
        transaction.replace(R.id.container, containerFragment);
        transaction.commit();
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        finish();
    }
}
