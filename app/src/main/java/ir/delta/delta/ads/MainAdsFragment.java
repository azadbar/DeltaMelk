package ir.delta.delta.ads;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionAdsCity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetAdsListService;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.service.ResponseModel.ads.AdsResponse;
import ir.delta.delta.slider.CardSliderIndicator;
import ir.delta.delta.slider.CardSliderViewPager;
import ir.delta.delta.splash.SplashConsultantActivity;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.EqualSpacingItemDecoration;
import ir.delta.delta.util.PreferencesData;
import ir.metrix.sdk.Metrix;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;

import static androidx.navigation.Navigation.findNavController;

public class MainAdsFragment extends BaseFragment implements AdsAdapter.OnItemClickListener,
        SlideListMainAdsAdapter.OnItemClickListener, ScrollListMainAdsAdapter.OnItemClickListener {


    @BindView(R.id.loadingView)
    LoadingView loadingView;

    @BindView(R.id.linearLayout)
    BaseLinearLayout linearLayout;

    @BindView(R.id.rvGroupList)
    RecyclerView rvGroupList;

    @BindView(R.id.cardSliderViewPager)
    CardSliderViewPager cardSliderViewPager;

    @BindView(R.id.indicator)
    CardSliderIndicator indicator;

    @BindView(R.id.rvScrollList)
    RecyclerView rvScrollList;

    @BindView(R.id.slidTitle)
    BaseTextView slidTitle;
    private int dotscount = 0;
    private int currentPage = 0;
    private View v;
    private AdsResponse response;
    private AdsAdapter adsAdapter;
    private boolean isShowButtonEstate;
    private SlideListMainAdsAdapter slidAdapter;
    private ScrollListMainAdsAdapter scrollListAdapter;


    private FirebaseAnalytics mFirebaseAnalytics;
    private Bundle params;
    private SlideListMainAdsAdapter slideListMainAdsAdapter;
    private boolean isVisibleToUser;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                getActivity().finish();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_main_ads, container, false);
            ButterKnife.bind(this, v);
//            Intent intent = requireActivity().getIntent();
            Bundle bundle = getArguments();
            if (bundle != null) {
                isShowButtonEstate = bundle.getBoolean("isShowButtonEstate", false);
            }
            getAdsDataRequest();
        }


        loadingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getAdsDataRequest();
            }
        });
        return v;
    }

    private void getAdsDataRequest() {
        loadingView.showLoading(false);
        slidTitle.setVisibility(View.GONE);
        rvGroupList.setVisibility(View.GONE);
        rvScrollList.setVisibility(View.GONE);
        cardSliderViewPager.setVisibility(View.GONE);
        GetAdsListService.getInstance().getAdsList(getResources(), new ResponseListener<AdsResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    loadingView.showError(error);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(AdsResponse response) {
                if (getView() != null && isAdded()) {
                    MainAdsFragment.this.response = response;
                    if (response.isSuccess()) {
                        loadingView.stopLoading();
                        slidTitle.setVisibility(View.VISIBLE);
                        rvGroupList.setVisibility(View.VISIBLE);
                        rvScrollList.setVisibility(View.VISIBLE);
                        cardSliderViewPager.setVisibility(View.VISIBLE);
                        setDataInView();
                    }
                }
            }
        });
    }

    private void setDataInView() {
        if (response != null) {
            if (response.getGroupList() != null)
                fillListGroup();

            if (response.getAdsSlideList() != null && response.getAdsSlideList().size() > 0) {
                fillSlideList();
            }

            if (response.getAdsScrollList() != null && response.getAdsScrollList().size() > 0) {
                fillScrollList();
            }
        }


    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        this.isVisibleToUser = isVisibleToUser;
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            ContainerFragment.swipeOff();
        } else {
            ContainerFragment.swipeOn();
        }
    }

    private void fillScrollList() {
//        BaseTextView scrollTitle = new BaseTextView(getContext());
////        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
////        lp.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_16), getResources().getDimensionPixelOffset(R.dimen.margin_3), 0);
////        scrollTitle.setLayoutParams(lp);
////        scrollTitle.setText(response.getAdsScrollTitle());
////        scrollTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_3));
////        scrollTitle.setTextColor(getResources().getColor(R.color.primaryTextColor));
////        scrollTitle.setFontType(FontTypeEnum.BOLD);
////        linearLayout.addView(scrollTitle);

       /* RecyclerView rvScrollList = new RecyclerView(requireContext());
        LinearLayout.LayoutParams scrollLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        scrollLp.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_16), 0, 0);
        rvScrollList.setLayoutParams(scrollLp);*/
        int coulumCount = response.getColumnCount() > 0 ? response.getColumnCount(): 2;
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvScrollList.setHasFixedSize(true);
        scrollListAdapter = new ScrollListMainAdsAdapter(getActivity(), cellWidth, coulumCount, response.getAdsScrollList(), this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvScrollList.setLayoutManager(layoutManager);
        rvScrollList.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvScrollList.setAdapter(scrollListAdapter);
        rvScrollList.setNestedScrollingEnabled(false);
        layoutManager.isAutoMeasureEnabled();
//        linearLayoutTotlal.addView(rvScrollList);
    }

    private void fillSlideList() {

       /* nestedScrollView = new NestedScrollView(getContext());
        LinearLayout.LayoutParams nestedLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        nestedScrollView.setLayoutParams(nestedLp);
        linearLayout.addView(nestedScrollView);*/

        /*linearLayoutTotlal = new BaseLinearLayout(getContext());
        LinearLayout.LayoutParams linearLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayoutTotlal.setLayoutParams(linearLp);
        linearLayoutTotlal.setOrientation(LinearLayout.VERTICAL);
        nestedScrollView.addView(linearLayoutTotlal);*/

        /*BaseTextView slidTitle = new BaseTextView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_16), getResources().getDimensionPixelOffset(R.dimen.margin_3), 0);
        slidTitle.setLayoutParams(lp);
        slidTitle.setText("");
        slidTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_3));
        slidTitle.setTextColor(getResources().getColor(R.color.primaryTextColor));
        slidTitle.setFontType(FontTypeEnum.BOLD);
        linearLayoutTotlal.addView(slidTitle);*/

        /*RecyclerView rvSlideList = new RecyclerView(requireContext());
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp1.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_3), 0, 0);
        rvSlideList.setLayoutParams(lp1);
        rvSlideList.setHasFixedSize(true);
        int cellWidth = 0;
        if (getContext() != null) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = screenWidth * 3 / 4;
            }
        }
        slidAdapter = new SlideListMainAdsAdapter(response.getAdsSlideList(), cellWidth, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
        rvSlideList.setLayoutManager(layoutManager);
        rvSlideList.setAdapter(slidAdapter);
        rvSlideList.setNestedScrollingEnabled(false);
        layoutManager.isAutoMeasureEnabled();*/

        /*CardSliderIndicator cardSliderIndicator = new CardSliderIndicator(getContext());
        LinearLayout.LayoutParams cardIndicatorParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        cardIndicatorParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_3), 0, 0);
        cardIndicatorParams.gravity = Gravity.CENTER_HORIZONTAL;
        cardSliderIndicator.setLayoutParams(cardIndicatorParams);
        cardSliderIndicator.setId(R.id.cardIndicator);
        cardSliderIndicator.setIndicatorsToShow(5);*/

        /*CardSliderViewPager cardSliderViewPager = new CardSliderViewPager(getContext());
        LinearLayout.LayoutParams cardSliderParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        cardSliderParams.setMargins(0, getResources().getDimensionPixelOffset(R.dimen.margin_3), 0, 0);
        cardSliderViewPager.setLayoutParams(cardSliderParams);
        cardSliderViewPager.setId(R.id.cardSlider);
        cardSliderViewPager.setOtherPagesWidth(getResources().getDimensionPixelOffset(R.dimen.margin_3));
        cardSliderViewPager.setSliderPageMargin(getResources().getDimensionPixelOffset(R.dimen.margin_2));
        cardSliderViewPager.setNestedScrollingEnabled(false);
        cardSliderViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });*/

        int cellWidth = 0;
        if (getContext() != null) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = screenWidth * 3 / 4;
            }
        }

        ArrayList<AdsItem> items = new ArrayList<>();
        items.addAll(response.getAdsSlideList());
        items.addAll(response.getAdsSlideList());
        items.addAll(response.getAdsSlideList());
        items.addAll(response.getAdsSlideList());
        items.addAll(response.getAdsSlideList());
        items.addAll(response.getAdsSlideList());

        cardSliderViewPager.setAdapter(new SlideListMainAdsAdapter(items, cellWidth, this));
        cardSliderViewPager.setSmallScaleFactor(0.7f);
        cardSliderViewPager.setSmallAlphaFactor(0.7f);
        cardSliderViewPager.setAutoSlideTime(4);
        cardSliderViewPager.setCurrentItem(0);
        cardSliderViewPager.setSliderPageMargin(1f);
        cardSliderViewPager.setNestedScrollingEnabled(false);
        indicator.setIndicatorsToShow(5);




      /*  cardSliderViewPager.setCurrentItem((int) items.size() / 2, true);
        cardSliderViewPager.setCurrentItem((int) items.size() / 2);
        indicator.setIndicatorsToShow(items.size());*/

      /*  DemoInfiniteAdapter demoInfiniteAdapter = new DemoInfiniteAdapter(getContext(),
                response.getAdsScrollList(),
                true,
                cellWidth);
        cardSliderViewPager.setAdapter(demoInfiniteAdapter);

        indicator.setHighlighterViewDelegate(new Function1<FrameLayout, View>() {
            @Override
            public View invoke(FrameLayout frameLayout) {
                View highlighter = new View(getContext());
                highlighter.setLayoutParams(new ViewGroup.LayoutParams(
                        16,
                        2));
                highlighter.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                return highlighter;
            }
        });

        indicator.setUnselectedViewDelegate(new Function1<LinearLayout, View>() {
            @Override
            public View invoke(LinearLayout linearLayout) {
                View unselected = new View(getContext());
                unselected.setLayoutParams(new ViewGroup.LayoutParams(
                        16,
                        2));
                unselected.setBackgroundColor(getContext().getResources().getColor(android.R.color.darker_gray));
                unselected.setAlpha(0.4F);
                return unselected;
            }
        });

        cardSliderViewPager.setOnIndicatorProgress(new Function2<Integer, Float, Unit>() {
            @Override
            public Unit invoke(Integer integer, Float aFloat) {
                indicator.onPageScrolled(integer, aFloat);
                return Unit.INSTANCE;
            }
        });
        indicator.updateIndicatorCounts(cardSliderViewPager.getIndicatorCount());*/

        /*linearLayoutTotlal.addView(cardSliderViewPager);
        linearLayoutTotlal.addView(cardSliderIndicator);*/


    }

    private void fillListGroup() {
       /* nestedScrollView = new NestedScrollView(getContext());
        LinearLayout.LayoutParams nestedLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        nestedScrollView.setLayoutParams(nestedLp);
        linearLayout.addView(nestedScrollView);

        linearLayoutTotlal = new BaseLinearLayout(getContext());
        LinearLayout.LayoutParams linearLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        linearLayoutTotlal.setLayoutParams(linearLp);
        linearLayoutTotlal.setOrientation(LinearLayout.VERTICAL);
        nestedScrollView.addView(linearLayoutTotlal);*/


        int coulumCount = response.getColumnCountGroup() > 0 ? response.getColumnCountGroup(): 3;//get khanom akhtari
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvGroupList.setHasFixedSize(true);
        adsAdapter = new AdsAdapter(getActivity(), cellWidth, coulumCount, response.getGroupList(), this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvGroupList.setNestedScrollingEnabled(true);
        rvGroupList.setLayoutManager(layoutManager);
        rvGroupList.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvGroupList.setAdapter(adsAdapter);
        double height = Double.valueOf(response.getGroupList().size()) / coulumCount;
        LinearLayout.LayoutParams nestedLp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                (int) ((coulumCount > 3 ? cellWidth : cellWidth * coulumCount / 4) * Math.ceil(height))
                        + getResources().getDimensionPixelOffset(R.dimen.margin_16));
        rvGroupList.setLayoutParams(nestedLp);
//        linearLayoutTotlal.addView(rvGroupList);


    }


    @Override
    public void onItemClick(AdsItem adsItem, int position) {
       /* NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_ads_fragment);
        NavGraph navGraph = navController.getNavInflater().inflate(R.navigation.nav_graph);
        int cityId = PreferencesData.getCityId(requireContext());
        City city = TransactionAdsCity.getInstance().getCityById(requireContext(), cityId);
        if (city != null) {
            //go to melk or other fragment
            Constants.setCurrentCityAds(city);
            if (adsItem.isMelki()) {
                navGraph.setStartDestination(R.id.listAdsFragment);
            } else {
                navGraph.setStartDestination(R.id.subGroupAdsFragment);
            }
        } else {
            //go to selected city fragment
            navGraph.setStartDestination(R.id.selectedCityList);
        }*/
        Intent intent;
//        if (adsItem.isMelki()) {
//           intent = new Intent(requireActivity(), SplashConsultantActivity.class);
//           intent.putExtra("isFirst", false);
//           startActivity(intent);
//

//       }else {
        intent = new Intent(getActivity(), AdsActivity.class);
        intent.putExtra("parentGroupId", adsItem.getParent_id());
        intent.putExtra("parentId", adsItem.getId());
        intent.putExtra("groupTitle", adsItem.getTitle());
        intent.putExtra("isMelki", adsItem.isMelki());
        intent.putExtra("showList", adsItem.isShowList());
        intent.putExtra("isShowButtonEstate", isShowButtonEstate);
        startActivity(intent);
        Metrix.getInstance().newEvent("ypojp");
        Map<String, String> attributes = new HashMap<>();
        attributes.put("ypojp", adsItem.getTitle());
        Metrix.getInstance().addUserAttributes(attributes);
//       }

       /* Bundle bundle = new Bundle();
        bundle.putInt("parentId", adsItem.getId());
        bundle.putString("groupTitle", adsItem.getTitle());
        bundle.putBoolean("isMelki", adsItem.isMelki());
        bundle.putBoolean("isShowButtonEstate", isShowButtonEstate);//for show button estate for cafe bazar payment
        navController.setGraph(navGraph, bundle);*/
    }

    @Override
    public void onSlideListItem(int position, AdsItem allListItem) {
        startDetail(allListItem);
    }

    private void startDetail(AdsItem allListItem) {
        Intent intent = new Intent(getActivity(), AdsActivity.class);
        intent.putExtra("infoId", allListItem.getId());
        intent.putExtra("isMelki", allListItem.isMelki());
        intent.putExtra("isMainPage", true);
        intent.putExtra("isOpenDetail", true);
        startActivity(intent);
//        findNavController(loadingView).navigate(R.id.action_main_to_detail, bundle);
    }

    @Override
    public void onAdsScrollItemClick(AdsItem adsItem, int position) {
        startDetail(adsItem);
    }

    @Override
    public void onDestroy() {
        cardSliderViewPager.setAutoSlideTime(-1);
        super.onDestroy();
    }

    @Override
    public void onPause() {
        cardSliderViewPager.setAutoSlideTime(-1);
        super.onPause();
    }

}
