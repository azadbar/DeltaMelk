package ir.delta.delta.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.util.Constants;

import static ir.delta.delta.baseView.BaseFragment.ellipsizeText;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ScrollListMainAdsAdapter extends RecyclerView.Adapter<ScrollListMainAdsAdapter.ViewHolder> {


    private final int cellWidth;
    private final int coulumCount;
    private final ArrayList<AdsItem> list;
    private final OnItemClickListener listener;
    private final Context context;

    public ScrollListMainAdsAdapter(Context context, int cellWidth, int coulumCount, ArrayList<AdsItem> list, OnItemClickListener listener) {
        this.cellWidth = cellWidth;
        this.coulumCount = coulumCount;
        this.list = list;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scroll_main_ads, parent, false);
        return new ViewHolder(itemView, cellWidth);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Resources res = holder.itemView.getContext().getResources();
        AdsItem object = list.get(position);
        Glide.with(context).load(Constants.URL_BASE_STATIC + object.getFirstImage()).into(holder.image);
        holder.title.setText(object.getTitle());
        if (coulumCount <=2){
            holder.title.setMaxLines(2);
            holder.title.setTextSize(res.getDimensionPixelSize(R.dimen.text_size_2));
            holder.title.setFontType(FontTypeEnum.BOLD);
            ellipsizeText(holder.title,50);
        }else {
            holder.title.setMaxLines(1);
            holder.title.setTextSize(res.getDimensionPixelSize(R.dimen.text_size_1));
            holder.title.setFontType(FontTypeEnum.MEDIUM);
            ellipsizeText(holder.title,20);
        }
        holder.bind(object, position, listener);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.title)
        BaseTextView title;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;

        ViewHolder(View view, int cellWidth) {
            super(view);
            ButterKnife.bind(this, view);
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(cellWidth, cellWidth);
            layoutParams.gravity = Gravity.CENTER;
            itemView.setLayoutParams(layoutParams);
        }

        public void bind(AdsItem addItem, int position, final OnItemClickListener listener) {
            rlAddImage.setOnClickListener(v -> listener.onAdsScrollItemClick(addItem, position));
        }


    }


    public interface OnItemClickListener {
        void onAdsScrollItemClick(AdsItem adsItem, int position);

    }


}
