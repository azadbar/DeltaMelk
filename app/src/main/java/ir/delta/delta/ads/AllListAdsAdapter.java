package ir.delta.delta.ads;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.ads.AllListItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class AllListAdsAdapter extends RecyclerView.Adapter<AllListAdsAdapter.ViewHolder> {


    private final ArrayList<AllListItem> list;
    private final onItemClick listener;

    public AllListAdsAdapter(ArrayList<AllListItem> list, onItemClick listener) {
        this.list = list;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads_list, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AllListItem object = list.get(position);
        holder.location.setVisibility(View.VISIBLE);
        holder.location.setColorFilter(holder.imageMag.getContext().getResources().getColor(R.color.gray_100));
        holder.tvTitle.setText(object.getTitle());
        holder.tvDate.setText(object.getLocationName());
        holder.tvDate.setTextColor(holder.tvDate.getResources().getColor(R.color.gray_100));
        Glide.with(holder.itemView.getContext()).load(Constants.URL_BASE_STATIC + object.getBaseMultimediaPath()).placeholder(R.drawable.no_photo).into(holder.imageMag);
        if (object.getLogo() != null) {
            holder.logoRealEstate.setVisibility(View.VISIBLE);
            Glide.with(holder.itemView.getContext()).load( object.getLogo()).placeholder(R.drawable.no_photo).into(holder.logoRealEstate);
        } else
            holder.logoRealEstate.setVisibility(View.GONE);

        holder.bind(object, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        BaseImageView imageMag;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvAuthor)
        BaseTextView tvAuthor;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.location)
        BaseImageView location;
        @BindView(R.id.logoRealEstate)
        BaseImageView logoRealEstate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(AllListItem adsItem, int position, onItemClick listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, adsItem));
        }

    }


    public interface onItemClick {
        void onItemClick(int position, AllListItem allListItem);
    }
}
