package ir.delta.delta.ads;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.ads.ListProvinceItem;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ProvinceAdsAdapter extends RecyclerView.Adapter<ProvinceAdsAdapter.ViewHolder> {



    private final ArrayList<ListProvinceItem> list;
    private final OnItemClickListener listener;

    public ProvinceAdsAdapter(ArrayList<ListProvinceItem> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_province_ads, parent, false);

        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ListProvinceItem object = list.get(position);

        holder.title.setText(object.getTitle());

//        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));

        holder.bind(position, object, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        BaseTextView title;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, ListProvinceItem object, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, object));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, ListProvinceItem object);
    }
}
