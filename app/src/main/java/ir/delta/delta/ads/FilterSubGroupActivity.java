package ir.delta.delta.ads;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetSubGroupListAdsService;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.service.ResponseModel.ads.SubGroupListResponse;
import ir.delta.delta.util.Constants;

import static androidx.navigation.Navigation.findNavController;

public class FilterSubGroupActivity extends BaseActivity implements SubGroupListAdapter.OnItemClickListener {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rvSubGroupAds)
    RecyclerView rvSubGroupAds;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    private int groupid;
    private SubGroupListResponse response;
    private SubGroupListAdapter adapter;
    private boolean isFilter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sub_group_ads);
        ButterKnife.bind(this);
        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            groupid = arguments.getInt("groupid", 0);
        }
        initView();
        getSubGroupRequest();
    }

   /* @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_sub_group_ads, container, false);
        ButterKnife.bind(this, v);


        Bundle arguments = getArguments();
        if (arguments != null) {
            groupid = arguments.getInt("groupid", 0);
            isFilter = arguments.getBoolean("isFilter");
        }
        initView();
        getSubGroupRequest();

        return v;

    }*/


    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white));
        imgBack.setImageResource(R.drawable.ic_back);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.selected_grouping));
    }

    private void getSubGroupRequest() {
        showLoading();
        GetSubGroupListAdsService.getInstance().getSubGroupAdsList(getResources(), groupid, new ResponseListener<SubGroupListResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    Toast.makeText(FilterSubGroupActivity.this, error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(FilterSubGroupActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(SubGroupListResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    FilterSubGroupActivity.this.response = response;
                    setDataAdapter();
                }
            }
        });

    }

    private void setDataAdapter() {
        adapter = new SubGroupListAdapter(response.getAdsItems(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(FilterSubGroupActivity.this, RecyclerView.VERTICAL, false);
        rvSubGroupAds.setHasFixedSize(true);
        rvSubGroupAds.setLayoutManager(layoutManager);
        rvSubGroupAds.setAdapter(adapter);
    }


    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvSubGroupAds.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvSubGroupAds.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemShare(int position, AdsItem adsItem) {
           /* Bundle bundle = new Bundle();
            bundle.putInt("groupid", adsItem.getId());
            bundle.putInt("beforId", groupid);
            bundle.putString("title", adsItem.getTitle());
            Navigation.findNavController(rvSubGroupAds).navigate(R.id.action_subGroupAdsFragment_to_listAdsFragment,bundle);*/

        Intent resultIntent = new Intent();
        resultIntent.putExtra("subGroupTitle", adsItem.getTitle());
        resultIntent.putExtra("subGroupId", adsItem.getId());
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        finish();
//        findNavController(rlBack).navigateUp();
    }
}
