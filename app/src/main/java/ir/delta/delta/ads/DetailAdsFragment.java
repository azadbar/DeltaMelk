package ir.delta.delta.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.bottomNavigation.moreItems.WebSiteDeltaActivity;
import ir.delta.delta.customView.AdsTextView;
import ir.delta.delta.customView.DetailRowAdsView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.enums.TypeCommunityAdsEnum;
import ir.delta.delta.estateDetail.SliderAdapter;
import ir.delta.delta.listener.AppBarStateChangeListener;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetDetailAdsService;
import ir.delta.delta.service.ResponseModel.ads.DetailAdsItem;
import ir.delta.delta.service.ResponseModel.ads.DetailResponse;
import ir.delta.delta.service.ResponseModel.ads.ResultThreeItem;
import ir.delta.delta.service.ResponseModel.ads.ValueItem;
import ir.delta.delta.util.Constants;
import me.relex.circleindicator.CircleIndicator;

import static androidx.navigation.Navigation.findNavController;

public class DetailAdsFragment extends BaseFragment implements CommunityAdsAdapter.OnItemClickListener, CommunityVideoAdsAdapter.OnItemClickListener {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.rlPager)
    BaseRelativeLayout rlPager;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    @BindView(R.id.rlLocation)
    BaseRelativeLayout rlLocation;
    @BindView(R.id.viewPrice)
    View viewPrice;
    @BindView(R.id.rlPrice)
    BaseLinearLayout rlPrice;
    @BindView(R.id.resultOneA)
    BaseLinearLayout resultOneA;
    @BindView(R.id.btnCall)
    BaseTextView btnCall;
    @BindView(R.id.rlCall)
    BaseRelativeLayout rlCall;
    @BindView(R.id.btnMessage)
    BaseTextView btnMessage;
    @BindView(R.id.rlMessage)
    BaseRelativeLayout rlMessage;
    @BindView(R.id.llButton)
    BaseLinearLayout llButton;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.resultOneATitle)
    BaseTextView resultOneATitle;
    @BindView(R.id.tvHeaderDetail)
    BaseTextView tvHeaderDetail;
    @BindView(R.id.resultOneBFacilityTitle)
    BaseTextView resultOneBFacilityTitle;
    @BindView(R.id.resultOneBFacility)
    BaseLinearLayout resultOneBFacility;
    @BindView(R.id.resultTwoDescription)
    BaseLinearLayout resultTwoDescription;
    //    @BindView(R.id.cvTwoDescription)
//    CardView cvTwoDescription;
    @BindView(R.id.resultThreeVideoAds)
    BaseLinearLayout resultThreeVideoAds;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.anim_toolbar)
    BaseToolbar animToolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.imgShare)
    BaseImageView imgShare;
    @BindView(R.id.scroolView)
    NestedScrollView scroolView;
    @BindView(R.id.rlMap)
    BaseRelativeLayout rlMap;
    @BindView(R.id.view)
    View view;


    private int infoId;
    private DetailResponse response;
    private View rlResultTwoDescription;
    private View rlResultOneA;
    private boolean isMelki;
    private String isMobileValue;
    private int currentPage;
    private boolean isMainPage;
    private int parentGroupId;
    private boolean showList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                backPress();

            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    private void backPress() {
        if (isMainPage) {
            getActivity().finish();
        } else {
            findNavController(rlBack).popBackStack(R.id.listAdsFragment, false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detial_ads, container, false);
        ButterKnife.bind(this, v);
        rlResultTwoDescription = v.findViewById(R.id.rlResultTwoDescription);
        rlResultOneA = v.findViewById(R.id.rlResultOneA);
        Bundle bu = getArguments();
        if (bu != null) {
            infoId = bu.getInt("infoId", 0);
            isMelki = bu.getBoolean("isMelki");
            showList = bu.getBoolean("showList");
            isMainPage = bu.getBoolean("isMainPage");
            parentGroupId = bu.getInt("parentGroupId");
        }
        initView();
        getDetailRequest();
        loadingView.setButtonClickListener(view -> getDetailRequest());
        return v;
    }


    private void initView() {
        hideShowView(View.GONE);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black));
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        imgShare.setVisibility(View.GONE);

        ViewTreeObserver viewTreeObserver = rlPager.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    rlPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    rlPager.getLayoutParams().height = (int) (rlPager.getWidth() / 1.75);
                }
            });
        }

        appbar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if (getActivity() == null) {
                    return;
                }
                if (state == State.COLLAPSED) {
                    imgBack.setColorFilter(getActivity().getResources().getColor(R.color.black));
                    imgShare.setColorFilter(getActivity().getResources().getColor(R.color.black));
                    imgBack.setBackground(null);
                    imgShare.setBackground(null);
                    tvTitle.setVisibility(View.VISIBLE);
                    tvTitle.setText(response != null ? response.getResultZeroA().getTitle() : "");
                    ellipsizeText(tvTitle, 40);
                } else if (state == State.EXPANDED) {
                    imgBack.setColorFilter(getActivity().getResources().getColor(R.color.white));
                    imgShare.setColorFilter(getActivity().getResources().getColor(R.color.white));
                    imgBack.setBackground(getActivity().getResources().getDrawable(R.drawable.background_transparent_view));
                    imgShare.setBackground(getActivity().getResources().getDrawable(R.drawable.background_transparent_view));
                    tvTitle.setVisibility(View.GONE);

                }
            }
        });

    }

    private void getDetailRequest() {
        loadingView.showLoading(false);
        scroolView.setVisibility(View.GONE);
        llButton.setVisibility(View.GONE);
//        appbar.setVisibility(View.GONE);
        GetDetailAdsService.getInstance().getDetailAds(getResources(), infoId,parentGroupId, new ResponseListener<DetailResponse>() {
            @Override
            public void onGetError(String error) {
                loadingView.showError(error);
            }

            @Override
            public void onSuccess(DetailResponse response) {
                llButton.setVisibility(View.VISIBLE);
                scroolView.setVisibility(View.VISIBLE);
//                appbar.setVisibility(View.VISIBLE);
                loadingView.stopLoading();
                if (response.isSuccess()) {
                    DetailAdsFragment.this.response = response;
                    setDataInView();
                } else {
                    scroolView.setVisibility(View.GONE);
                    llButton.setVisibility(View.GONE);
//                    appbar.setVisibility(View.GONE);
                    loadingView.showError(response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(requireContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(requireContext());
                startActivity(intent);
                getActivity().finish();
            }
        });
    }

    private void setDataInView() {
        hideShowView(View.VISIBLE);
        makeSlider(response.getResultZeroA().getImageList());
        tvHeaderDetail.setText(response.getResultZeroA().getTitle().replace("\n", ""));
        tvLocation.setText(response.getResultZeroA().getLocationName());
        if (response.getResultZeroB() != null) {
            rlPrice.setVisibility(View.VISIBLE);
            viewPrice.setVisibility(View.VISIBLE);
            rlPrice.setOrientation(LinearLayout.HORIZONTAL);
            for (int i = 0; i < response.getResultZeroB().size(); i++) {
                DetailAdsItem price = response.getResultZeroB().get(i);
                if (!price.getValue().equals("0")) {
                    addPriceRow(price, 2, rlPrice, getResources().getString(R.string.toman), true);
                }

            }
        } else {
            rlPrice.setVisibility(View.GONE);
            viewPrice.setVisibility(View.GONE);
        }

        if (((response.getResultOneA() != null && response.getResultOneA().getValue() != null && response.getResultOneA().getValue().size() > 0) ||
                (response.getResultOneA1() != null && !TextUtils.isEmpty(response.getResultOneA1().getValue())))
                || (response.getResultOneB() != null && response.getResultOneB().getValue() != null)) {
            rlResultOneA.setVisibility(View.VISIBLE);
        } else {
            rlResultOneA.setVisibility(View.GONE);
        }
        if (response.getResultOneA() != null && response.getResultOneA().getValue() != null && response.getResultOneA().getValue().size() > 0) {
            resultOneA.setVisibility(View.VISIBLE);
            resultOneATitle.setText(response.getResultOneA().getKey());
            resultOneA.setOrientation(LinearLayout.HORIZONTAL);
            for (int i = 0; i < response.getResultOneA().getValue().size(); i++) {
                DetailAdsItem price = response.getResultOneA().getValue().get(i);
                addPriceRow(price, 3, resultOneA, "", false);
            }
        } else {
            resultOneA.setVisibility(View.GONE);
        }

        if (response.getResultOneA1() != null && !TextUtils.isEmpty(response.getResultOneA1().getValue())) {
            resultOneA.setVisibility(View.VISIBLE);
            resultOneATitle.setVisibility(View.VISIBLE);
            resultOneATitle.setText(response.getResultOneA1().getKey());
            resultOneA.setOrientation(LinearLayout.HORIZONTAL);
            addRowText(response.getResultOneA1().getValue(), resultOneA, getResources().getColor(R.color.primaryTextColor), FontTypeEnum.MEDIUM);
        } else {
            resultOneATitle.setVisibility(View.GONE);
        }

        if (response.getResultOneB() != null) {
            if (response.getResultOneB().getValue() != null && response.getResultOneB().getValue().size() > 0) {
                resultOneBFacilityTitle.setVisibility(View.VISIBLE);
                resultOneBFacilityTitle.setText(response.getResultOneB().getKey());
                addResultOneB();
            } else {
                resultOneBFacilityTitle.setVisibility(View.GONE);
//                addRowText(getResources().getString(R.string.nothing), resultOneBFacility, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }

        if (response.getResultTwo() != null) {
            if (!TextUtils.isEmpty(response.getResultTwo().getValue())) {
                rlResultTwoDescription.setVisibility(View.VISIBLE);
                addRowText(response.getResultTwo().getKey() + ":", resultTwoDescription, getResources().getColor(R.color.redColor), FontTypeEnum.BOLD);
                addRowText(response.getResultTwo().getValue(), resultTwoDescription, getResources().getColor(R.color.primaryTextColor), FontTypeEnum.BOLD);
            } else {
                rlResultTwoDescription.setVisibility(View.GONE);
//                addRowText(getResources().getString(R.string.nothing), resultTwoDescription, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }

        if (response.getResultThree() != null) {
            if (response.getResultThree().getValue() != null && response.getResultThree().getValue().size() > 0) {
                addRowThreeRecycle(response.getResultThree().getKey(), response.getResultThree());
            } else {
//                addRowText(getResources().getString(R.string.nothing), resultThreeVideoAds, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }

        if (response.getResultFour() != null) {
            if (response.getResultFour().getValue() != null && response.getResultFour().getValue().size() > 0) {
                addRowThreeRecycle(response.getResultFour().getKey(), response.getResultFour().getValue());
            } else {
//                addRowText(getResources().getString(R.string.nothing), resultThreeVideoAds, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }


        if (response.getResultFive() != null) {
            if (response.getResultFive().getValue() != null && response.getResultFive().getValue().size() > 0) {
                addRowThreeRecycle(response.getResultFive().getKey().trim(), response.getResultFive().getValue());
            } else {
//                addRowText(getResources().getString(R.string.nothing), resultThreeVideoAds, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }

        //for check show rlMessage Visbile or not visible
        if (TextUtils.isEmpty(isMobileValue)) {
            rlMessage.setVisibility(View.GONE);
            view.setVisibility(View.GONE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    10.0f
            );
            rlCall.setLayoutParams(param);
        } else {
            rlMessage.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    0,
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    4.98f
            );
            rlMessage.setLayoutParams(param);
        }

        if (response.getResultSix() != null) {
            if (!TextUtils.isEmpty(response.getResultSix().getValue().trim())) {
                addRowText(response.getResultSix().getKey().trim() + ":", resultThreeVideoAds, getResources().getColor(R.color.redColor), FontTypeEnum.BOLD);
                addRowText(response.getResultSix().getValue().trim(), resultThreeVideoAds, getResources().getColor(R.color.primaryTextColor), FontTypeEnum.MEDIUM);
            } else {

//                addRowText(getResources().getString(R.string.nothing), resultThreeVideoAds, getResources().getColor(R.color.secondaryTextColor), FontTypeEnum.MEDIUM);
            }
        }

        if (response.getResultSeven() != null) {
            String valueGroup; //if 1 = isMelk and 2 other
            if (isMelki) {
                valueGroup = "1";
            } else {
                valueGroup = "2";
            }
            webView.loadUrl(Constants.IMAGE_URL_ADS + "map.html?lat=" + response.getResultSeven().getLatitude()
                    + "&lng=" + response.getResultSeven().getLongitude() + "&isMelki=" + valueGroup);
        }
    }

    private void addRowThreeRecycle(String key, ResultThreeItem value) {
        if (value.getValue() != null && value.getValue().size() > 0) {
            addRowText(key + ":", resultThreeVideoAds, getResources().getColor(R.color.redColor), FontTypeEnum.BOLD);
            RecyclerView rv = new RecyclerView(requireContext());
            rv.setNestedScrollingEnabled(false);
            resultThreeVideoAds.addView(rv);
            rv.setHasFixedSize(true);
            CommunityVideoAdsAdapter adapter = new CommunityVideoAdsAdapter(value.getValue(), value.getKey(), value.getImage(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            rv.setAdapter(adapter);
        }
    }


    private void addRowThreeRecycle(String key, ArrayList<ValueItem> value) {
        ArrayList<ValueItem> list = new ArrayList<>();
        for (int i = 0; i < value.size(); i++) {
            if (!TextUtils.isEmpty(value.get(i).getValue().trim())) {
                list.add(value.get(i));
                if (value.get(i).getType().equals(TypeCommunityAdsEnum.isMobile.name()) && !TextUtils.isEmpty(value.get(i).getValue())) {
                    isMobileValue = value.get(i).getValue();
                }
            }
        }


        if (list.size() > 0) {
            addRowText(key + ":", resultThreeVideoAds, getResources().getColor(R.color.redColor), FontTypeEnum.BOLD);
            RecyclerView rv = new RecyclerView(requireContext());
            rv.setNestedScrollingEnabled(false);
            resultThreeVideoAds.addView(rv);
            rv.setHasFixedSize(true);
            CommunityAdsAdapter adapter = new CommunityAdsAdapter(list, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            rv.setAdapter(adapter);
        }

    }

    private void addResultOneB() {
        RecyclerView rv = new RecyclerView(requireContext());
        resultOneBFacility.addView(rv);
        rv.setHasFixedSize(true);
        ResultOneBAdsAdapter adapter = new ResultOneBAdsAdapter(response.getResultOneB().getValue());
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setAdapter(adapter);
    }

    private void addRowText(String value, BaseLinearLayout resultOneBFacility, int color, FontTypeEnum fontTypeEnum) {
        AdsTextView priceRow = new AdsTextView(requireContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        priceRow.setLayoutParams(lp);
        resultOneBFacility.addView(priceRow);
        priceRow.setTextTitle(value);
        priceRow.setStyleText(color, fontTypeEnum);
    }


    private void addPriceRow(DetailAdsItem price, int columnSize, BaseLinearLayout view, String postfix, boolean isConvertNumberToDecimal) {
        DetailRowAdsView priceRow = new DetailRowAdsView(requireContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setWeightSum(columnSize);
        lp.weight = 1f;
        priceRow.setLayoutParams(lp);
        view.addView(priceRow);
        priceRow.setTextTitle(price.getKey(), price.getValue(), postfix, isConvertNumberToDecimal);
    }


    private void makeSlider(ArrayList<String> imageList) {
        final ArrayList<String> images = new ArrayList<>();
        if (imageList != null) {
            for (int i = 0; i < imageList.size(); i++) {
                images.add(Constants.URL_BASE_STATIC + imageList.get(i));
            }
        }

//        images.add("https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.gettyimages.com%2Fphotos%2Fimage&psig=AOvVaw3j3UO5R-hivRs51I3mXrgR&ust=1591700083046000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCPCkp9aG8ukCFQAAAAAdAAAAABAI");

        pager.setAdapter(new SliderAdapter(images));
        if (images.size() == 0) {
            pager.setBackgroundResource(R.drawable.no_photo_big);
        }
        if (images.size() == 1) {
            indicator.setVisibility(View.GONE);
        } else {
            indicator.setViewPager(pager);
        }

        Handler handler = new Handler();

        Runnable update = () -> {
            if (currentPage == images.size()) {
                currentPage = 0;
            }
            pager.setCurrentItem(currentPage++, true);
        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 1000, 4000);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {
                // your code here
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void hideShowView(int visibilty) {
//        toolbar.setVisibility(visibilty);
        llButton.setVisibility(visibilty);
    }

    @OnClick({R.id.rlCall, R.id.rlMessage, R.id.imgShare, R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlCall:
                if (!TextUtils.isEmpty(isMobileValue)) {
                    openCallIntent(isMobileValue.trim());
                } else {
                    openCallIntent(response.getResultFive().getValue().get(0).getValue().trim());
                }
                break;
            case R.id.rlMessage:
                openMessageIntent(isMobileValue);
                break;
            case R.id.imgShare:
                Toast.makeText(requireContext(), "share", Toast.LENGTH_SHORT).show();
                break;
            case R.id.rlBack:
                backPress();
                break;
        }
    }

    private void openMessageIntent(String message) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", message, null)));
    }

    private void openCallIntent(String call) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + call));
        startActivity(intent);
    }

    @Override
    public void onItemClick(int position, ValueItem object) {

        Intent intent = null;
        if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isVideo.name())) {
            openWebSite(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isTelegram.name())) {
            intentMessageTelegram(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isInstagram.name())) {
            openInstagram(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isWebsite.name())) {
            openWebSite(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isPhone1.name())) {
            openCallIntent(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isPhone2.name())) {
            openCallIntent(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isPhone3.name())) {
            openCallIntent(object.getValue());
        } else if (object.getType() != null && object.getType().equals(TypeCommunityAdsEnum.isMobile.name())) {
            openCallIntent(object.getValue());
        }
    }

    private void openWebSite(String value) {
        if (URLUtil.isValidUrl(value)) {
            Intent intent = new Intent(requireContext(), WebSiteDeltaActivity.class);
            intent.putExtra("url", value);
            startActivity(intent);
        } else {
            Toast.makeText(requireContext(), "متاسفانه خطایی رخ داده است", Toast.LENGTH_SHORT).show();
        }

    }

    private void openInstagram(String value) {
        Uri uri = Uri.parse("http://instagram.com/_u/" + value);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + value)));
        }
    }

    private void intentMessageTelegram(String id) {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(getActivity().getApplicationContext(), appName);
        if (isAppInstalled) {
//            Intent myIntent = new Intent(Intent.ACTION_SEND);
//            myIntent.setType("text/plain");
//            myIntent.setPackage(appName);
//            myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
//            startActivity(Intent.createChooser(myIntent, "Share with"));
            Intent telegram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/" + id));
            startActivity(telegram);
        } else {
            Toast.makeText(requireContext(), "Telegram not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onItemClick(int position, String object) {
//        openWebSite(object);
        if (URLUtil.isValidUrl(object)) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(object));
            startActivity(i);
        } else {
            Toast.makeText(requireContext(), "متاسفانه خطایی رخ داده است", Toast.LENGTH_SHORT).show();
        }
    }


}
