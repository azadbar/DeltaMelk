package ir.delta.delta.ads;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.ads.GetSubGroupListAdsService;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.service.ResponseModel.ads.SubGroupListResponse;
import ir.delta.delta.util.Constants;

import static androidx.navigation.Navigation.findNavController;

public class SubGroupFragment extends BaseFragment implements SubGroupListAdapter.OnItemClickListener {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rvSubGroupAds)
    RecyclerView rvSubGroupAds;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.tvChangeCity)
    BaseTextView tvChangeCity;
    @BindView(R.id.changeCity)
    BaseRelativeLayout changeCity;
    @BindView(R.id.tvLocation)
    BaseTextView tvLocation;
    private int parentId;
    private SubGroupListResponse response;
    private SubGroupListAdapter adapter;
    private boolean isMelki;
    private String groupTitle;
    private boolean title;
    private View v;
    private long mLastClickTime = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                onBackPress();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    private void onBackPress() {
        getActivity().finish();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (v == null) {
            v = inflater.inflate(R.layout.fragment_sub_group_ads, container, false);
            ButterKnife.bind(this, v);
            Bundle arguments = getArguments();
            if (arguments != null) {
                parentId = arguments.getInt("parentId", 0);
                groupTitle = arguments.getString("groupTitle");
                isMelki = arguments.getBoolean("isMelki");
            }

            getSubGroupRequest();
        }
        initView();
        return v;

    }


    private void initView() {
        seStatusBarColor(getResources().getColor(R.color.white), rlBack);
        imgBack.setImageResource(R.drawable.ic_back);
        rlBack.setVisibility(View.VISIBLE);
        tvTitle.setText(getResources().getString(R.string.back));
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText("نیازمندی های " + groupTitle);
        tvLocation.setText(setTextSpannable());
    }

    private void getSubGroupRequest() {
        showLoading();
        GetSubGroupListAdsService.getInstance().getSubGroupAdsList(getResources(), parentId, new ResponseListener<SubGroupListResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
//                    Toast.makeText(requireContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(requireContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(SubGroupListResponse response) {
                stopLoading();
                if (response.isSuccess()) {
                    SubGroupFragment.this.response = response;
                    setDataAdapter();
                }
            }
        });

    }

    private void setDataAdapter() {
        adapter = new SubGroupListAdapter(response.getAdsItems(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false);
        rvSubGroupAds.setHasFixedSize(true);
        rvSubGroupAds.setLayoutManager(layoutManager);
        rvSubGroupAds.setAdapter(adapter);
    }


    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvSubGroupAds.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvSubGroupAds.setVisibility(View.VISIBLE);
    }


    @Override
    public void onItemShare(int position, AdsItem adsItem) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){//In my case, if the user clicks the same view twice very very quickly ,this crash will occur.
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        Bundle bundle = new Bundle();
        bundle.putInt("groupid", adsItem.getId());
        bundle.putInt("parentId", parentId);
        bundle.putBoolean("isMelki", isMelki);
        bundle.putString("groupTitle", groupTitle);
        bundle.putString("subGroupTitle", adsItem.getTitle());
        findNavController(rvSubGroupAds).navigate(R.id.action_subGroupAdsFragment_to_listAdsFragment, bundle);

    }

    @OnClick({R.id.rlBack, R.id.changeCity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
//                getActivity().onBackPressed();
                onBackPress();
                break;
            case R.id.changeCity:
                /*NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_ads_fragment);
                NavGraph navGraph = navController.getNavInflater().inflate(R.navigation.nav_graph);
                navGraph.setStartDestination(R.id.selectedCityList);*/
                Bundle bundle = new Bundle();
                bundle.putInt("parentId", parentId);
                bundle.putString("groupTitle", groupTitle);
                bundle.putBoolean("isMelki", isMelki);
                bundle.putBoolean("isFromSubGroup", true);
                findNavController(changeCity).navigate(R.id.selectedCityList, bundle);
                break;
        }

    }

    private SpannableStringBuilder setTextSpannable() {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(getResources().getString(R.string.selected_your_city)).append(": ");
        int start = desc_two.length();
        desc_two.append(Constants.getAdsCity(requireContext()) != null ? Constants.getAdsCity(requireContext()).getTitle() : "انتخاب نشده");
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.2f), start, end, 0);
        return desc_two;
    }
}
