package ir.delta.delta.ads;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SubGroupListAdapter extends RecyclerView.Adapter<SubGroupListAdapter.ViewHolder> {


    private final ArrayList<AdsItem> list;
    private final OnItemClickListener listener;


    public SubGroupListAdapter(ArrayList<AdsItem> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_group, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        AdsItem sub = list.get(position);
        holder.title.setText(sub.getTitle());
        Glide.with( holder.itemView.getContext()).load(Constants.URL_BASE_STATIC + sub.getImage()).into(holder.image);

//        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));

        holder.bind(sub, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        BaseTextView title;
        @BindView(R.id.image)
        BaseImageView image;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(AdsItem adsItem, int position, OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemShare(position, adsItem));
        }
    }


    public interface OnItemClickListener {

        void onItemShare(int position, AdsItem demand);
    }

}
