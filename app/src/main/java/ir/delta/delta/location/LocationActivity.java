package ir.delta.delta.location;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.Province;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class LocationActivity extends BaseActivity implements LocationSelectListener {

    @BindView(R.id.frameLayout)
    LinearLayout frameLayout;
    private boolean isFirstLaunch;//اگر بار اول بود برنامه اجرا شد
    private CityEnum cityEnum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_in_up, 0);
        getDataBundle();
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        CityFragment cityFragment = new CityFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("cityEnum", CityEnum.MAINCITY.getId());
        cityFragment.setArguments(bundle);
        loadFragmentCityFragment(cityFragment, "mainCityFragment", true);
    }

    private void getDataBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isFirstLaunch = bundle.getBoolean("isFirstLaunch");
        }
    }

    @Override
    public void OnSelectCity(City city, CityEnum cityEnum) {
        this.cityEnum = cityEnum;
        if (cityEnum == CityEnum.MAINCITY) {
            selectCity(city);
        } else {
            if (cityEnum == CityEnum.COASTALCITY) {
                CityFragment cityFragment = new CityFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("cityEnum", CityEnum.COASTALCITY.getId());
                cityFragment.setArguments(bundle);
                loadFragmentCityFragment(cityFragment, "coastalFragment", false);
            } else {
                ProvinceFragment provinceFragment = new ProvinceFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("cityEnum", CityEnum.CITYINSTATE.getId());
                provinceFragment.setArguments(bundle);
                loadFragmentProvinceFragment(provinceFragment);
            }
        }
    }

    public void selectCity(City city) {
        PreferencesData.saveCityId(LocationActivity.this, city.getId());
        Constants.setCurrentCity(city);
        if (isFirstLaunch) {
            Intent intent;
//            if (BuildConfig.isCounsultant) {
//                intent = new Intent(LocationActivity.this, LoginActivity.class);
//                intent.putExtra("isFirstActivity", true);
//            } else {
                intent = new Intent(LocationActivity.this, BottomBarActivity.class);
                intent.putExtra("itemsIndex", 0);
//            }
            startActivity(intent);
        }
        finish();
    }

    private void loadFragmentCityFragment(CityFragment fragment, String fragmentTag, boolean isFirstFragment) {
        fragment.setLocationSelectListener(this);
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        if (cityEnum == CityEnum.COASTALCITY || cityEnum == CityEnum.CITYINSTATE) {
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        } else {
            fragTrans.setCustomAnimations(R.anim.slide_in_up, 0, 0, R.anim.slide_out_up);
        }
        if (!isFirstFragment)
            fragTrans.addToBackStack(fragmentTag);
        fragTrans.replace(R.id.frameLayout, fragment);
        fragTrans.commit();
    }

    private void loadFragmentProvinceFragment(ProvinceFragment fragment) {
        fragment.setLocationSelectListener(this);
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.addToBackStack("provinceFragment");
        fragTrans.replace(R.id.frameLayout, fragment);
        fragTrans.commit();
    }

    @Override
    public void OnSelectProvince(Province province, CityEnum cityEnum) {
        CityFragment cityFragment = new CityFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("cityEnum", CityEnum.CITYINSTATE.getId());
        bundle.putParcelable("province", province);
        bundle.putString("cityTitle", province.getName());
        cityFragment.setArguments(bundle);
        loadFragmentCityFragment(cityFragment, "cityInEstate", false);
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
        overridePendingTransition(0, R.anim.slide_out_up);

    }
}
