package ir.delta.delta.location;

import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.Province;

public interface LocationSelectListener {

    void OnSelectCity(City city, CityEnum cityEnum);
    void OnSelectProvince(Province province, CityEnum cityEnum);
}
