package ir.delta.delta.location;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.service.ResponseModel.Province;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.ViewHolder> {

    private ArrayList<Province> list;
    private final ProvinceAdapter.OnItemClickListener listener;

    ProvinceAdapter(ArrayList<Province> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Province object = list.get(position);
        holder.tvTitle.setText(object.getName());
        holder.bind(position, object, listener);
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            holder.imgMore.setBackgroundResource(R.drawable.ic_keyboard_english);
        } else {
            holder.imgMore.setBackgroundResource(R.drawable.ic_keyboard_arrow);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.imgMore)
        BaseImageView imgMore;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Province province, final OnItemClickListener listener) {
            itemView.setOnClickListener(view -> listener.onItemClick(position, province));
        }
    }

    public void setList(ArrayList<Province> provinces) {
        list = provinces;
        notifyDataSetChanged();

    }

    public interface OnItemClickListener {
        void onItemClick(int position, Province province);
    }
}
