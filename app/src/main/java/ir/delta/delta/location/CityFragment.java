package ir.delta.delta.location;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.Province;

public class CityFragment extends BaseFragment implements CityAdapter.OnItemClickListener {

    @BindView(R.id.tvTxtInputCity)
    BaseTextView tvTxtInputCity;
    @BindView(R.id.edtSearchBar)
    BaseEditText edtSearchBar;
    @BindView(R.id.icSearch)
    BaseImageView icSearch;
    @BindView(R.id.searchBar)
    CardView searchBar;
    @BindView(R.id.rvResultSearch)
    RecyclerView rvResultSearch;
    @BindView(R.id.llSearchResult)
    BaseLinearLayout llSearchResult;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;

    private ArrayList<City> mainList;
    private final ArrayList<City> filterList = new ArrayList<>();
    private CityAdapter adapter;
    Unbinder unbinder;
    private LocationSelectListener locationSelectListener;
    private String cityTitle;
    private CityEnum cityEnum;
    private Province province;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_city, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle b = getArguments();
        if (b != null) {
            int cityEnumInt = b.getInt("cityEnum");
            cityEnum = CityEnum.fromId(cityEnumInt);
            province = b.getParcelable("province");
            cityTitle = b.getString("cityTitle");
        } else {
            cityEnum = CityEnum.MAINCITY;
        }

        if (cityEnum == CityEnum.COASTALCITY) {
            searchBar.setVisibility(View.GONE);
            tvTxtInputCity.setText(getResources().getString(R.string.slecet_city));
            setTextTitle(getString(R.string.north_coastal_city));
        } else if (cityEnum == CityEnum.CITYINSTATE) {
            searchBar.setVisibility(View.VISIBLE);
            tvTxtInputCity.setText(getResources().getString(R.string.enter_city_or_choose_from_the_list_below));
            edtSearchBar.setHint(getResources().getString(R.string.search_city));
            setTextTitle(cityTitle);
        } else {
            searchBar.setVisibility(View.GONE);
            tvTxtInputCity.setText(getResources().getString(R.string.slecet_city));
            setTextTitle(getString(R.string.select_city));
        }

        fillCityList();
        edtSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence == null || charSequence.toString().trim().isEmpty()) {
                    filterList.clear();
                    filterList.addAll(mainList);
                } else {
                    filterList.clear();
                    String searchText = charSequence.toString().trim();
                    for (City city : mainList) {
                        if (city.getName().contains(searchText)) {
                            filterList.add(city);
                        }
                    }
                }
                adapter.setList(filterList);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    private void setTextTitle(String text) {
        tvFilterTitle.setText(text);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);
    }


    private void fillCityList() {
        switch (cityEnum) {
            case COASTALCITY:
                mainList = TransactionCity.getInstance().getCoastalCities(getActivity());
                break;
            case CITYINSTATE:
                mainList = TransactionCity.getInstance().getCitiesBy(getActivity(), province.getId());
                break;
            default:
                mainList = TransactionCity.getInstance().getMainCities(getActivity());
                City northCity = new City();
                northCity.setName(getString(R.string.north_coastal_city));
                mainList.add(northCity);
                City otherCity = new City();
                otherCity.setName(getString(R.string.other_city));
                mainList.add(otherCity);
        }
        filterList.clear();
        filterList.addAll(mainList);
        setDataAdapter();
    }

    private void setDataAdapter() {
        adapter = new CityAdapter(filterList, this, cityEnum);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvResultSearch.setLayoutManager(layoutManager);
        rvResultSearch.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position, City city) {
        if (locationSelectListener != null) {
            if (cityEnum == CityEnum.COASTALCITY || cityEnum == CityEnum.CITYINSTATE) {
                locationSelectListener.OnSelectCity(city, CityEnum.MAINCITY);
            } else {
                if (position == filterList.size() - 1) {
                    locationSelectListener.OnSelectCity(city, CityEnum.CITYINSTATE);
                } else if (position == filterList.size() - 2) {
                    locationSelectListener.OnSelectCity(city, CityEnum.COASTALCITY);
                } else {
                    locationSelectListener.OnSelectCity(city, CityEnum.MAINCITY);
                }
            }
        }

    }

    public void setLocationSelectListener(LocationSelectListener locationSelectListener) {
        this.locationSelectListener = locationSelectListener;
    }


    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                getActivity().onBackPressed();
            }
        }

    }
}
