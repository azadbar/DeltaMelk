package ir.delta.delta.location;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.Language;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private ArrayList<City> list;
    private final CityEnum cityEnum;
    private final OnItemClickListener listener;

    CityAdapter(ArrayList<City> list, OnItemClickListener listener, CityEnum cityEnum) {
        this.list = list;
        this.listener = listener;
        this.cityEnum = cityEnum;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        City object = list.get(position);
        holder.tvTitle.setText(object.getName());
        holder.bind(position, object, listener);

        holder.tvCostalCity.setText(object.getName());
        holder.tvCostalDescription.setText(object.getDescription());
        if (cityEnum == CityEnum.COASTALCITY) {
            holder.tvTitle.setVisibility(View.INVISIBLE);
            holder.tvCostalCity.setVisibility(View.VISIBLE);
            holder.tvCostalDescription.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.VISIBLE);
            holder.tvCostalCity.setVisibility(View.GONE);
            holder.tvCostalDescription.setVisibility(View.GONE);
        }
        if (cityEnum == CityEnum.COASTALCITY || cityEnum == CityEnum.CITYINSTATE) {
            holder.imgMore.setVisibility(View.GONE);
        } else {
            if (position < list.size() - 2) {
                holder.imgMore.setVisibility(View.GONE);
            } else {
                holder.imgMore.setVisibility(View.VISIBLE);
            }
        }
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            holder.imgMore.setBackgroundResource(R.drawable.ic_keyboard_english);
        } else {
            holder.imgMore.setBackgroundResource(R.drawable.ic_keyboard_arrow);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.imgMore)
        BaseImageView imgMore;
        @BindView(R.id.row)
        BaseRelativeLayout row;
        @BindView(R.id.tvCostalCity)
        BaseTextView tvCostalCity;
        @BindView(R.id.tvCostalDescription)
        BaseTextView tvCostalDescription;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, City city, final OnItemClickListener listener) {
            itemView.setOnClickListener(view -> {
                listener.onItemClick(position, city);
            });
        }
    }

    public void setList(ArrayList<City> cities) {
        list = cities;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(int position, City city);
    }
}
