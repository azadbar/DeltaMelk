package ir.delta.delta.location;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionProvince;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.service.ResponseModel.Province;

public class ProvinceFragment extends BaseFragment implements ProvinceAdapter.OnItemClickListener {

    @BindView(R.id.tvTxtInputCity)
    BaseTextView tvTxtInputCity;
    @BindView(R.id.edtSearchBar)
    BaseEditText edtSearchBar;
    @BindView(R.id.icSearch)
    BaseImageView icSearch;
    @BindView(R.id.searchBar)
    CardView searchBar;
    @BindView(R.id.rvResultSearch)
    RecyclerView rvResultSearch;
    @BindView(R.id.llSearchResult)
    BaseLinearLayout llSearchResult;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    private ArrayList<Province> provinces;
    private ArrayList<Province> filterList = new ArrayList<>();
    private ProvinceAdapter adapter;
    Unbinder unbinder;
    private LocationSelectListener locationSelectListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_province, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle b = getArguments();
        CityEnum cityEnum;
        if (b != null) {
            int cityEnumInt = b.getInt("cityEnum");
            cityEnum = CityEnum.fromId(cityEnumInt);
        } else {
            cityEnum = CityEnum.CITYINSTATE;
        }
        provinces = TransactionProvince.getInstance().getStates(getActivity());
        setDataStateAdapter();

        if (cityEnum == CityEnum.CITYINSTATE) {
            tvTxtInputCity.setText(getResources().getString(R.string.enter_province_or_choose_from_the_list_below));
            edtSearchBar.setHint(R.string.search_state);
            setTextTitle(getString(R.string.select_province));
        }
        edtSearchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence == null || charSequence.toString().trim().isEmpty()) {
                    filterList = provinces;
                } else {
                    filterList = new ArrayList<>();
                    String searchText = charSequence.toString().trim();
                    for (Province province : provinces) {
                        if (province.getName().contains(searchText)) {
                            filterList.add(province);
                        }
                    }
                }
                adapter.setList(filterList);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }


    private void setDataStateAdapter() {
        adapter = new ProvinceAdapter(provinces, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvResultSearch.setLayoutManager(layoutManager);
        rvResultSearch.setAdapter(adapter);
    }

    @Override
    public void onItemClick(int position, Province province) {
        if (locationSelectListener != null) {
            locationSelectListener.OnSelectProvince(province, CityEnum.CITYINSTATE);
        }
    }

    public void setLocationSelectListener(LocationSelectListener locationSelectListener) {
        this.locationSelectListener = locationSelectListener;
    }


    private void setTextTitle(String text) {
        tvFilterTitle.setText(text);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);

    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        if (getActivity() != null) {
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                getActivity().onBackPressed();
            }
        }
    }
}
