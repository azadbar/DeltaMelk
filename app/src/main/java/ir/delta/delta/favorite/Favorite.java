package ir.delta.delta.favorite;

import android.content.Context;
import android.content.Intent;

import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.FavoritConsultantPropertyService;
import ir.delta.delta.service.Request.FavoritUserPropertyService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.UnFavoriteConsultantPropertyService;
import ir.delta.delta.service.Request.UnFavoriteUserPropertyService;
import ir.delta.delta.service.RequestModel.FavoritePropertyReq;
import ir.delta.delta.service.ResponseModel.FavoritePropertyResponse;
import ir.delta.delta.util.Constants;

public class Favorite {

    private final OnFavoriteListener listener;
    private final Context context;

    public Favorite(Context context, OnFavoriteListener listener) {
        this.listener = listener;
        this.context = context;
    }

    public void favoriteUserRequest(int id) {
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);
        FavoritUserPropertyService.getInstance().favoritUserProperty(context, context.getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                listener.favoriteUserGetError(error);
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(context, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(context);
                context.startActivity(intent);

            }
            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                listener.favoriteUserOnSuccess(response);
            }
        });
    }

    public void unFavoriteUserRequest(int id) {
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);

        UnFavoriteUserPropertyService.getInstance().unFavoritUserProperty(context, context.getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                listener.unFavoriteUserGetError(error);
            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(context, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(context);
                context.startActivity(intent);
            }
            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                listener.unFavoriteUserOnSuccess(response);
            }
        });
    }


    public void favoriteConsultantProperty(int id) {
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);
        FavoritConsultantPropertyService.getInstance().favoritConsultantProperty(context, context.getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                listener.favoriteConsultantPropertyGetError(error);
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(context, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(context);
                context.startActivity(intent);
            }
            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                listener.favoriteConsultantPropertyOnSuccess(response);
            }
        });
    }

    public void unFavoriteConsultantProperty(int id) {
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);
        UnFavoriteConsultantPropertyService.getInstance().unFavoriteConsultantProperty(context, context.getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                listener.unFavoriteConsultantPropertyGetError(error);
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(context, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(context);
                context.startActivity(intent);
            }
            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                listener.unFavoriteConsultantPropertyOnSuccess(response);
            }
        });

    }

    public interface OnFavoriteListener {
        void favoriteUserGetError(String error);

        void favoriteUserOnSuccess(FavoritePropertyResponse response);

        void unFavoriteUserGetError(String error);

        void unFavoriteUserOnSuccess(FavoritePropertyResponse response);

        void favoriteConsultantPropertyGetError(String error);

        void favoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response);

        void unFavoriteConsultantPropertyGetError(String error);

        void unFavoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response);

    }
}
