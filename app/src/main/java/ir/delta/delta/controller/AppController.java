package ir.delta.delta.controller;


import androidx.multidex.MultiDexApplication;


import com.adivery.sdk.Adivery;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import io.sentry.android.core.SentryAndroid;
import ir.delta.delta.R;
import ir.metrix.sdk.Metrix;
import ir.metrix.sdk.MetrixConfig;



public class AppController extends MultiDexApplication {

    public void onCreate() {
        super.onCreate();
        // OPTIONAL: If crash reporting has been explicitly disabled previously, add:
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);

        MetrixConfig metrixConfig = new MetrixConfig(this, "fgpuplvyboqcrfc");
        metrixConfig.setScreenFlowsAutoFill(true);
        metrixConfig.setFirebaseId("1:921373624684:android:c3bab977a434b7dcd50b49", "deltaorginal", "AIzaSyC36BCs5NGOhoP1I0Gpq3IzNxE1vfMadLU");
        Metrix.onCreate(metrixConfig);

        SentryAndroid.init(this, options -> options.setDsn("http://a4aabe1358c54be28b93db6ef97080e1@handler.delta.ir/1"));
        Adivery.configure(this,  getString(R.string.adivery_token));

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}
