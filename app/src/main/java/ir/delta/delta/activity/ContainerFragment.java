package ir.delta.delta.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ir.delta.delta.R;
import ir.delta.delta.ads.MainAdsFragment;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.MagPostContentWebViewActivity;
import ir.delta.delta.mag.SinglePostContentActivity;
import ir.delta.delta.mag.main_page.MainPageMagFragment;
import ir.delta.delta.mag.telegram.TelegramContainerFragment;
import ir.delta.delta.mag.video.VideoFragment;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.RTLViewPager;
import ir.metrix.sdk.Metrix;


public class ContainerFragment extends BaseFragment {

    private Context context;
    private static TabLayoutSetupCallback mToolbarSetupCallback;
    private List<MagMenuResponse> response;
    private OnGetRecentList listener;
    private OnScroolListener scrollListener;
    private boolean isRequirement;
    private static RTLViewPager viewPager;
    private String postId;
    private String link;

   /* public ContainerFragment(Context context) {
        // Required empty public constructor
        this.context = context;
    }*/

    public static ContainerFragment newInstance(Context context) {
        ContainerFragment myFragment = new ContainerFragment();
        myFragment.context = context;
        return myFragment;
    }

    public void setResponse(List<MagMenuResponse> response) {
        this.response = response;
    }

    public void setNotification(String postId, String link) {
        this.postId = postId;
        this.link = link;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof MainActivity) {
            mToolbarSetupCallback = (TabLayoutSetupCallback) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement TabLayoutSetupCallback");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for notification
        Intent intent = null;
        if (!TextUtils.isEmpty(postId) && TextUtils.isDigitsOnly(postId)) {
            intent = new Intent(getActivity(), SinglePostContentActivity.class);
            intent.putExtra("id", postId);
            startActivity(intent);
        } else if (!TextUtils.isEmpty(link)) {
            intent = new Intent(getActivity(), MagPostContentWebViewActivity.class);
            intent.putExtra("link", link);
            startActivity(intent);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_container, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        if (response != null) {
            viewPager.setAdapter(new ItemsPagerAdapter(getChildFragmentManager(), response, listener,
                    scrollListener, true, context, isRequirement,postId, link));
            viewPager.setRtlOriented(true);
            viewPager.setBackgroundColor(getResources().getColor(R.color.backColor));
            mToolbarSetupCallback.setupTabLayout(viewPager);
        }

        return view;
    }

    public void setListener(OnGetRecentList listener) {
        this.listener = listener;
    }

    public void setScrollListener(OnScroolListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    public void setRequirement(boolean isRequirement) {
        this.isRequirement = isRequirement;
    }




    public static class ItemsPagerAdapter extends FragmentStatePagerAdapter {

        private final OnScroolListener scrollListener;
        private final boolean mIsRtlOrientated;
        private final Context context;
        private final boolean isRequirement;
        private String postId;
        private String link;
        private final List<MagMenuResponse> mTabs;
        private final OnGetRecentList listener;
        private final boolean isVideo = false;

        public ItemsPagerAdapter(FragmentManager fm, List<MagMenuResponse> tabNames,
                                 OnGetRecentList listener,
                                 OnScroolListener scrollListener,
                                 boolean isRtlOriented, Context context, boolean isRequirement, String postId, String link) {
            super(fm);
            this.mTabs = tabNames;
            this.listener = listener;
            this.scrollListener = scrollListener;
            this.mIsRtlOrientated = isRtlOriented;
            this.context = context;
            this.isRequirement = isRequirement;
            this.postId = postId;
            this.link = link;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {

            if (MagPostContentAdapter.stopVideoScrollListener != null)
                MagPostContentAdapter.stopVideoScrollListener.onScroll();

            if (mTabs != null && TextUtils.equals(mTabs.get(mTabs.size() - position - 1).getMenuType(), "Telegram")) {
                if (scrollListener != null)
                    scrollListener.onScrool(2);//for GONE realEstate Button
                Bundle params = new Bundle();
                params.putString("click_menu", "Telegram");
                FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
                mFirebaseAnalytics.logEvent("Click_in_menu", params);
                //analytics for metrix
                Metrix.getInstance().newEvent("xdtyo");
                Map<String, String> attributes = new HashMap<>();
                attributes.put("xdtyo", "Telegram");
                Metrix.getInstance().addUserAttributes(attributes);
                TelegramContainerFragment telegramContainerFragment = new TelegramContainerFragment();
                telegramContainerFragment.setScrollListener(scrollListener);
                return telegramContainerFragment;
            } else if (mTabs != null && (TextUtils.equals(mTabs.get(mTabs.size() - position - 1).getTermId(), "1609") ||
                    TextUtils.equals(mTabs.get(mTabs.size() - position - 1).getTermId(), "28"))) {//for video
                if (scrollListener != null)
                    scrollListener.onScrool(2);//for GONE realEstate Button
                return VideoFragment.newInstance(mTabs.size() - position - 1,
                        mTabs.get(mTabs.size() - position - 1).getTermId(),
                        mTabs.get(mTabs.size() - position - 1).getTitle(), listener, scrollListener);
            } else if (mTabs != null && (TextUtils.equals(mTabs.get(mTabs.size() - position - 1).getTermId(), "99999999"))) {//for main page
                return MainPageMagFragment.newInstance(mTabs.size() - position - 1,
                        mTabs.get(mTabs.size() - position - 1).getTermId(),
                        mTabs.get(mTabs.size() - position - 1).getTitle(), listener, scrollListener, postId, link);
            } else if (mTabs != null
                    && mTabs.get(mTabs.size() - position - 1).getMenuType() != null
                    && mTabs.get(mTabs.size() - position - 1).getMenuType().equals("Ads")) {//for show requirement in menu
                //analytics for metrix
                Metrix.getInstance().newEvent("xdtyo");
                Map<String, String> attributes = new HashMap<>();
                attributes.put("xdtyo", context.getResources().getString(R.string.essential_requirements));
                Metrix.getInstance().addUserAttributes(attributes);
                MainAdsFragment mainAdsFragment = new MainAdsFragment();
                Bundle bundle = new Bundle();
                mainAdsFragment.setArguments(bundle);
                return mainAdsFragment;
            } else if (mIsRtlOrientated && mTabs != null) {
                if (scrollListener != null)
                    scrollListener.onScrool(0);//for GONE realEstate Button
                return MagFragment.newInstance(mTabs.size() - position - 1,
                        mTabs.get(mTabs.size() - position - 1).getTermId(), mTabs.get(mTabs.size() - position - 1).getTitle(),
                        listener, scrollListener);
            } else {
                if (mTabs != null)
                    return MagFragment.newInstance(position, mTabs.get(position).getTermId(), mTabs.get(position).getTitle(), listener, scrollListener);
                else
                    return MagFragment.newInstance(position, "0", "", listener, scrollListener);
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            if (mTabs != null)
                return mTabs.size();
            else
                return 0;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (mIsRtlOrientated && mTabs != null) {
                return mTabs.get(mTabs.size() - position - 1).getTitle();
            } else {
                if (mTabs != null)
                    return mTabs.get(position).getTitle();
                else
                    return "";
            }
        }
    }

    public static void currentPage() {
        viewPager.setCurrentItem(0);
    }

    public interface TabLayoutSetupCallback {
        void setupTabLayout(ViewPager viewPager);
    }


    public interface OnGetRecentList {
        void onGetList(ArrayList<MagPost> sliderList);
    }

    public interface OnScroolListener {
        void onScrool(int i);
    }

    public static void swipeOff() {
        viewPager.setSwipeable(false);
    }

    public static void swipeOn() {
        viewPager.setSwipeable(true);
    }
}
