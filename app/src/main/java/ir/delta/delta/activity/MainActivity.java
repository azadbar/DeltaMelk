package ir.delta.delta.activity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.adivery.sdk.Adivery;
import com.adivery.sdk.AdiveryBannerCallback;
import com.adivery.sdk.AdiveryNativeAd;
import com.adivery.sdk.AdiveryNativeAdView;
import com.adivery.sdk.AdiveryNativeCallback;
import com.adivery.sdk.BannerType;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.aboutDelta.AboutMagDeltaActivity;
import ir.delta.delta.ads.SecondAdsActivity;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.moreItems.ContactDeltaActivity;
import ir.delta.delta.campaign.CampaignActivity;
import ir.delta.delta.campaign.RegisterCampaignActivity;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.ForceUpdateDialog;
import ir.delta.delta.favoriteMag.FavoriteMagListActivity;
import ir.delta.delta.feeback.FeedbackActivity;
import ir.delta.delta.mag.MagPostContentWebViewActivity;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.mag.SinglePostContentActivity;
import ir.delta.delta.mag.searchMag.SearchMagActivity;
import ir.delta.delta.orderAdvertising.OrderAdvertisingActivity;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.mag.MagHomeService;
import ir.delta.delta.service.RequestCallback;
import ir.delta.delta.service.RequestListener;
import ir.delta.delta.service.ResponseModel.mag.AppConfigResponse;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.splash.SplashConsultantActivity;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.CustomTypefaceSpan;
import ir.delta.delta.util.PreferencesData;
import ir.delta.delta.util.SliderMagAdapter;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements ContainerFragment.TabLayoutSetupCallback,
        MagFragment.OnListItemClickListener, NavigationView.OnNavigationItemSelectedListener, SliderMagAdapter.onItemClickSlider,
        ContainerFragment.OnGetRecentList, ContainerFragment.OnScroolListener {

    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.navigation_drawer)
    NavigationView navigationDrawer;
    @BindView(R.id.menu)
    BaseImageView menu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.coordinator)
    CoordinatorLayout coordinator;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.real_estate_sector)
    BaseRelativeLayout realEstateSector;
    @BindView(R.id.rlRoot)
    BaseRelativeLayout rlRoot;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rlRequirement)
    BaseRelativeLayout rlRequirement;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.tabProgress)
    ProgressBar tabProgress;
    @BindView(R.id.adContainer)
    BaseRelativeLayout adContainer;


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private String postId;
    private String link;
    private AppConfigResponse appConfigResponse;
    private TabLayout tabLayout;
    private ArrayList<MagPost> sliderList;
    private ArrayList<Integer> readPost;
    private SliderMagAdapter adapter;
    private SliderView view;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Bundle params;
    private boolean doubleBackToExitPressedOnce = false;
    private final int seed = 30;
    private boolean isFromPostContentMag;
    private List<MagMenuResponse> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_main);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_outt);
//        seStatusBarColor(getResources().getColor(R.color.transparent));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = rlRoot.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            rlRoot.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
        rlRoot.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setColorFilter(getResources().getColor(R.color.magToolbarFore), PorterDuff.Mode.SRC_ATOP);
        imgSearch.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        menu.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);

        tabLayout = findViewById(R.id.tab_layout);
        menu.setVisibility(View.VISIBLE);
        if (PreferencesData.isShowRequirementBtnShowInMenu(this)) {
            rlRequirement.setVisibility(View.GONE);
        } else {
            rlRequirement.setVisibility(View.GONE);
        }
        readPost = TransactionReadPost.getInstance().getReadPostsId(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //for notification action
            postId = bundle.getString("id");
//            postId = "250795";
            link = bundle.getString("link");
            appConfigResponse = bundle.getParcelable("appConfigResponse");
            isFromPostContentMag = bundle.getBoolean("isFromPostContentMag");
        }

        //request
        getMenusRequest();

        //for show or hide campaign in drawer mag"
        Menu menuItem = navigationDrawer.getMenu();
        if (appConfigResponse != null) {
            menuItem.findItem(R.id.nav_game).setVisible(appConfigResponse.isCampaign());
            menuItem.findItem(R.id.nav_requirement).setVisible(true);
        } else {
            //because onResume call return from PostContent Mag
            menuItem.findItem(R.id.nav_game).setVisible(false);
            menuItem.findItem(R.id.nav_requirement).setVisible(false);
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        setFont();//for drawer


        if (appConfigResponse != null && appConfigResponse.isHasChanged()) {
            new Handler().postDelayed(() -> {
                ForceUpdateDialog forceUpdateDialog = new ForceUpdateDialog(MainActivity.this);
                forceUpdateDialog.setForceUpdate(appConfigResponse.getVersionName(), appConfigResponse.isForce(),
                        appConfigResponse.getFeatures(),
                        appConfigResponse.getDownloadUrl(), view -> forceUpdateDialog.dismiss(), !appConfigResponse.isForce());
                try {
                    forceUpdateDialog.show();
                } catch (WindowManager.BadTokenException e) {

                }

            }, 4000);

        }

        mDrawerLayout = findViewById(R.id.drawer_layout);

        // Code here will be triggered once the drawer closes as we don't want anything to happen so we leave this blank
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);


        navigationDrawer.setNavigationItemSelectedListener(this);

        menu.setOnClickListener(v -> {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
        });

        //when response not found and try again
        loadingView.setButtonClickListener(v -> getMenusRequest());

        //show ads banner adivery
        showAdsAdivery();

    }


    private void showAdsAdivery() {
        AdiveryBannerCallback callback = new AdiveryBannerCallback() {
            @Override
            public void onAdLoaded(View ad) {
                /** Add this ad object to whatever layout you have set up for this placement */
                adContainer.addView(ad);
            }
        };
        Adivery.requestBannerAd(this, "9dbc0836-24ec-48e7-99ac-dc858010ae9c", BannerType.BANNER, callback);
    }

    private void getMenusRequest() {
        tabProgress.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
        new RequestCallback<>(this, ApiClient.getClient_MAG_Timeout().create(ReqInterface.class).getMenus(), new RequestListener<List<MagMenuResponse>>() {
            @Override
            public void onResponse(@NonNull Response<List<MagMenuResponse>> response) {
                tabProgress.setVisibility(View.GONE);
                if (response.body() != null) {
                    list.addAll(response.body());
                    if (list.size() > 0) {
                        setMainPage();
                    }
                }

            }

            @Override
            public void onFailure(int code, @NonNull JSONObject jsonObject) {
//                new Handler().postDelayed(() -> {
                tabProgress.setVisibility(View.GONE);
                container.setVisibility(View.GONE);
                loadingView.setVisibility(View.VISIBLE);
                loadingView.showError(getString(R.string.communicationError));


//                }, 100);
            }
        });
    }

    private void setMainPage() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        ContainerFragment containerFragment = ContainerFragment.newInstance(this);
        containerFragment.setResponse(list);
        containerFragment.setNotification(postId, link);
        containerFragment.setListener(this);
        containerFragment.setScrollListener(this);
        transaction.replace(R.id.container, containerFragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //request
//        if ((TextUtils.isEmpty(postId) || TextUtils.isEmpty(link)) ) {
//            getMenusRequest();
//        }


    }


    private void getMagHomeWithoutCheckVersion() {
        progressBar.setVisibility(View.VISIBLE);
        MagHomeService.getInstance().getMagHome(getResources(), seed, true, new ResponseListener<AppConfigResponse>() {
            @Override
            public void onGetError(String error) {
                progressBar.setVisibility(View.GONE);
            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(MainActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(AppConfigResponse response) {
                progressBar.setVisibility(View.GONE);
                if (response != null) {
                    MainActivity.this.appConfigResponse = response;
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    ContainerFragment containerFragment = ContainerFragment.newInstance(MainActivity.this);
//                    containerFragment.setResponse(response);
                    containerFragment.setListener(MainActivity.this);
                    containerFragment.setScrollListener(MainActivity.this);
                    transaction.replace(R.id.container, containerFragment);
                    transaction.commit();
                }
            }
        });
    }

    private void setSlider() {
        ll.setVisibility(View.GONE);

        view = new SliderView(this);
        adapter = new SliderMagAdapter(this, sliderList, this, readPost != null ? readPost : new ArrayList<>());
        view.setScrollTimeInSec(7);
        view.startAutoCycle();
        view.setSliderAdapter(adapter);
        view.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        view.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        view.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_INHERIT);
        view.setAutoCycle(true);
        view.setCircularHandlerEnabled(true);
        view.setIndicatorSelectedColor(Color.WHITE);
        view.setIndicatorUnselectedColor(Color.GRAY);
        ll.addView(view);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            if (this.doubleBackToExitPressedOnce) {
                super.onBackPressed();
            } else {
                Snackbar snackbar = Snackbar
                        .make(mDrawerLayout, "برای خروج دوباره کلیک کنید. ", Snackbar.LENGTH_SHORT);
                ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.redColor));
                snackbar.show();
                // اگر کاربر یه بار کلیک کرد روی بک توی صفحه ی اصلی ، بهش پیام می دیم که بار دوم هم کلیک کنه . اگه بار دوم زد خارج  می شه . ضمنا اگه تو صفحه اصلی نبود بک زدن باعث می شه که برگردیم تو صفحه خانه
                this.doubleBackToExitPressedOnce = true;
                new Handler().postDelayed(() -> MainActivity.this.doubleBackToExitPressedOnce = false, 2000);
            }
        }

    }

    @Override
    public void setupTabLayout(ViewPager viewPager) {
        tabLayout.setupWithViewPager(viewPager);
        setCustomFont();//for tablaout
    }

    @Override
    public void onListItemClick(String title) {
        Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.imgSearch, R.id.real_estate_sector, R.id.rlRequirement})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgSearch:
                startActivity(new Intent(this, SearchMagActivity.class));
                break;
            case R.id.real_estate_sector:
                Intent intent = new Intent(this, SplashConsultantActivity.class);
                intent.putExtra("isFirst", false);
                startActivity(intent);

                params = new Bundle();
                params.putString("realEstate", "selectRealStateSection");
                mFirebaseAnalytics.logEvent("section_real_estate", params);
                break;
            case R.id.rlRequirement:
                Intent intent1 = new Intent(MainActivity.this, SecondAdsActivity.class);
                intent1.putExtra("isShowButtonEstate", appConfigResponse.isShowButtonEstate());
                startActivity(intent1);
                break;
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_favorite:
                intent = new Intent(this, FavoriteMagListActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_about_delta:
                intent = new Intent(this, AboutMagDeltaActivity.class);
//                intent.putExtra("urlSite", "https://delta.ir/about/mobileabout");
                startActivity(intent);
                break;

            case R.id.nav_ads:
                intent = new Intent(this, OrderAdvertisingActivity.class);
                startActivity(intent);
                params = new Bundle();
                params.putString("register_ads", "RegisterAdsClick");
                mFirebaseAnalytics.logEvent("Register_Ads", params);
                break;
            case R.id.nav_share_application:
                if (BuildConfig.isCounsultant) {
                    Constants.shareTextUrl(this, "https://play.google.com/store/apps/details?id=ir.delta.delta.counsultant");
                } else {
                    Constants.shareTextUrl(this, PreferencesData.getLinDownload(this));
                }

                params = new Bundle();
                params.putString("share_application", "ShareApp");
                mFirebaseAnalytics.logEvent("share_application", params);
                break;
            case R.id.nav_contact_us:
                intent = new Intent(this, ContactDeltaActivity.class);
                startActivity(intent);
                params = new Bundle();
                params.putString("contact_us", "ContactUsClick");
                mFirebaseAnalytics.logEvent("ContactUs", params);
                break;
            case R.id.nav_game:
                if (Constants.getCampaignUser(this) != null) {
                    intent = new Intent(this, CampaignActivity.class);
                    intent.putExtra("mobile", Constants.getCampaignUser(this).getMobile());
                } else {
                    intent = new Intent(this, RegisterCampaignActivity.class);
                }
                startActivity(intent);

                params = new Bundle();
                params.putString("game", "Game_Click");
                mFirebaseAnalytics.logEvent("GameClick", params);
                break;
            case R.id.nav_feedback:
                startActivity(new Intent(this, FeedbackActivity.class));
                break;
            case R.id.nav_requirement:
                ContainerFragment.currentPage();
                break;

        }
        mDrawerLayout.closeDrawer(Gravity.RIGHT);
        return true;
    }

    private void setFont() {
        Menu m = navigationDrawer.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void setCustomFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();

//        Typeface fromAsset = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/IRANSansMobile(FaNum).ttf");
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
                    if (j == 0 && vg.getChildAt(0).getContentDescription().equals(getResources().getString(R.string.essential_requirements))) {
                        int paddingStart = vgTab.getPaddingStart();
                        int paddingTop = vgTab.getPaddingTop();
                        int paddingEnd = vgTab.getPaddingEnd();
                        int paddingBottom = vgTab.getPaddingBottom();
                        ViewCompat.setBackground(vgTab, AppCompatResources.getDrawable(vgTab.getContext(), R.drawable
                                .tabs_black));
                        ViewCompat.setPaddingRelative(vgTab, paddingStart, paddingTop, paddingEnd, paddingBottom);
                    }


                }
            }
        }
    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        if (list != null) {
            Intent intent = new Intent(this, PostContentActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("magPosts", list);
            bundle.putString("termId", list.get(position).getTermId());
            bundle.putInt("selectedIndex", position);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onGetList(ArrayList<MagPost> sliderList) {
        this.sliderList.clear();
        this.sliderList.addAll(sliderList);
        view.removeAllViews();
        setSlider();
    }


    @Override
    public void onScrool(int i) {
        showButton(2);
    }

    private void showButton(int i) {
        if (PreferencesData.isShowRequirementBtnShowInMenu(this)) {
            rlRequirement.setVisibility(View.GONE);
        } else {
            if (i == 0) {
                rlRequirement.setVisibility(View.VISIBLE);
//            slideUp();
            } else if (i == 1) {
                rlRequirement.setVisibility(View.VISIBLE);
//            slideDown();
            } else if (i == 2) {
                rlRequirement.setVisibility(View.GONE);
            }
        }
    }

    private void slideUp() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(rlRequirement, "translationY", 0);
        animation2.setDuration(500);
        animation2.start();
    }

    private void slideDown() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(rlRequirement, "translationY", +rlRequirement.getHeight() + 200);
        animation2.setDuration(500);
        animation2.start();
    }
}
