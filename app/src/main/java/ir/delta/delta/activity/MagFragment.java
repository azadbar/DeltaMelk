package ir.delta.delta.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.favoriteMag.FavoriteMagListActivity;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.PostAdapter;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.mag.SubMenuAdapter;
import ir.delta.delta.mag.main_page.OnRefreshMainPageList;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestCallback;
import ir.delta.delta.service.RequestListener;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.metrix.sdk.Metrix;
import retrofit2.Response;


public class MagFragment extends BaseFragment implements PostAdapter.onItemClick, PostContentActivity.setBackgroundReadItem,
        MagPostContentAdapter.onItemClickListener, OnRefreshMainPageList, SubMenuAdapter.OnItemClickListener {

    @BindView(R.id.rvSubMenu)
    RecyclerView rvSubMenu;
    @BindView(R.id.main_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.btnNewSearch)
    BaseTextView btnNewSearch;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;
    //    @BindView(R.id.real_estate_sector)
//    BaseRelativeLayout realEstateSector;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    private OnListItemClickListener mListItemClickListener;
    private int position;
    private PostAdapter postAdapter;
    private String termId;
    private String termName;
    private ArrayList<Integer> readPost;
    private ArrayList<Integer> favoriteList;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;
    private final ArrayList<MagMenuResponse> menus = new ArrayList<>();
    private final int seed = 30;
    private final ArrayList<MagPost> list = new ArrayList<>();
    private boolean isSwipeRefresh;
    private ContainerFragment.OnGetRecentList listener;
    private LinearLayoutManager layoutManager;
    private ContainerFragment.OnScroolListener scroolListener;
    private FirebaseAnalytics mFirebaseAnalytics;


    public static MagFragment newInstance(int position, String termId, String termName,
                                          ContainerFragment.OnGetRecentList listener,
                                          ContainerFragment.OnScroolListener scrollListener) {
        MagFragment pageFragment = new MagFragment();
        Bundle args = new Bundle();
        args.putSerializable("position", position);
        args.putString("termId", termId);
        args.putString("termName", termName);
        pageFragment.setListener(listener);
        pageFragment.setScrollListener(scrollListener);
        pageFragment.setArguments(args);

        return pageFragment;
    }

    private void setScrollListener(ContainerFragment.OnScroolListener scrollListener) {
        this.scroolListener = scrollListener;
    }

    private void setListener(ContainerFragment.OnGetRecentList listener) {
        this.listener = listener;
    }

    public MagFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_page, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt("position");
            termId = bundle.getString("termId");
            termName = bundle.getString("termName");
        }

        /*if (response !=null && response.getMenus() != null && response.getMenus().get(position).getChildren() != null &&
                response.getMenus().get(position).getChildren().size() > 0) {
            termId = response.getMenus().get(position).getChildren().get(0).getTermId();
            termName = response.getMenus().get(position).getChildren().get(0).getTitle();
            rvSubMenu.setVisibility(View.VISIBLE);
            subAdapter = new SubMenuAdapter(response.getMenus().get(position).getChildren(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
            rvSubMenu.setNestedScrollingEnabled(true);
            rvSubMenu.setLayoutManager(layoutManager);
            rvSubMenu.setAdapter(subAdapter);
        } else {
            rvSubMenu.setVisibility(View.GONE);
        }*/

        //get main request
        getPostsRequest();

        //read post for change background
        readPost = TransactionReadPost.getInstance().getReadPostsId(getContext());

        //read post for change background
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());

//        FavoriteMagListActivity.refreshMainPageListener = this;

        empty.setVisibility(View.GONE);
        if (getActivity() != null) {
            layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
//        recyclerView.setRotationY(180);
//        recyclerView.setRotationY(180);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());
                                getPostsRequest();
                            }
                    }
                }
            });
        }


        //when response not found and try again
        loadingView.setOnClickListener(v -> getPostsRequest());


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
//                    animateSwtich(1);
                    scroolListener.onScrool(1);

                } else {
                    if (!inLoading) {
//                        animateSwtich(0);
                        scroolListener.onScrool(0);
                    }
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        swipeRefresh.setColorSchemeColors(Color.RED);
        swipeRefresh.setOnRefreshListener(this::swipeRefreshMethod);
        return view;
    }

    private void getPostsRequest() {
        Bundle params = new Bundle();
        params.putString("click_menu", termName);
        mFirebaseAnalytics.logEvent("Click_in_menu", params);
        //analytics for metrix
        Metrix.getInstance().newEvent("xdtyo");
        Map<String, String> attributes = new HashMap<>();
        attributes.put("xdtyo", termName);
        Metrix.getInstance().addUserAttributes(attributes);
        if (!inLoading) {
            inLoading = true;
            if (list.isEmpty()) {
                if (isSwipeRefresh) {
                    recyclerView.animate().alpha(0.0f).setStartDelay(700);
                } else {
                    showLoading();
                }
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
        }
        new RequestCallback<>(getActivity(), ApiClient.getClient_MAG().create(ReqInterface.class).getPosts(list.size(), seed, termId), new RequestListener<List<MagPost>>() {
            @Override
            public void onResponse(@NonNull Response<List<MagPost>> response) {
                try {
                    if (response.body() != null) {
                        //when swipe refresh
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.animate().alpha(1.0f);
                        isSwipeRefresh = false;
                        swipeRefresh.setRefreshing(false);
                        rlLoding.setVisibility(View.GONE);
                        stopLoading();
                        if (response.body().size() < offset) {
                            endOfList = true;
                        }
                        inLoading = false;
                        list.addAll(response.body());
                        setDataAdapter();
                    } else {
                        stopLoading();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int code, @NonNull JSONObject jsonObject) {
                stopLoading();
                loadingView.setVisibility(View.VISIBLE);
                onError(getResources().getString(R.string.communicationError));
            }
        });
    }


    private void swipeRefreshMethod() {
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());
        list.clear();
        if (postAdapter != null)
            postAdapter.notifyDataSetChanged();
        isSwipeRefresh = true;
        getPostsRequest();
    }

  /*      private void animateSwtich(int selectedIndex) {
        if (selectedIndex == 0) {
            slideUp();
        } else {
            slideDown();
        }
    }

    private void slideUp() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(realEstateSector, "translationY", 0);
        animation2.setDuration(500);
        animation2.start();
    }

    private void slideDown() {
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(realEstateSector, "translationY", +realEstateSector.getHeight() + 200);
        animation2.setDuration(500);
        animation2.start();
    }*/


    private void setDataAdapter() {
        if (postAdapter == null) {
            postAdapter = new PostAdapter(list, readPost, this, favoriteList);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(postAdapter);
//            postAdapter.setRefreshMainPageListener(this);
        } else {
            postAdapter.notifayList(favoriteList);
        }
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null) {
            rlLoding.setVisibility(View.GONE);
            if (list.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


    private void showLoading() {
        loadingProgress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
//        loadingView.showLoading(true);
    }

    private void stopLoading() {
        loadingProgress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.animate().alpha(1.0f);
        loadingView.stopLoading();
//        if (BuildConfig.isCounsultant) {
//            realEstateSector.setVisibility(View.GONE);
//        } else {
//            realEstateSector.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext());
        try {
            mListItemClickListener = (OnListItemClickListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnListItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListItemClickListener = null;
    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        Intent intent = new Intent(getActivity(), PostContentActivity.class);
        PostContentActivity.magPosts = list;//becuse too large list
        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("magPosts", list);
        bundle.putString("termId", termId);
        bundle.putInt("selectedIndex", position);
        intent.putExtras(bundle);
        PostContentActivity.refreshMainPageListener = this;
//        PostContentActivity.listener = this;
//        list.get(position).setOpen(true);
//        postAdapter.notifyDataSetChanged();
        startActivity(intent);
    }


    @Override
    public void onItemShare(MagPost object) {
        Constants.sharePostLink(getContext(),object.getId(), object.getLink(), object.getTitle(), object.getTerm_name());
    }


    @Override
    public void onItemNext(int position, ArrayList<MagPost> magPosts) {
        if (magPosts != null && magPosts.size() > 0) {
            readPost.add(magPosts.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(getContext(), magPosts.get(position).getId());
            postAdapter.updateReceiptsList(magPosts);
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshMethod();

    }

    public interface OnListItemClickListener {
        void onListItemClick(String title);

    }


    @Override
    public void onItemClick(int position) {


    }

    @Override
    public void onItemSubMenu(int position, MagMenuResponse magMenuResponse, ArrayList<MagMenuResponse> menus) {
        /*if (magMenuResponse != beforMagmenu) {
            if (magMenuResponse.getTermId() == menus.get(position).getTermId()) {
                termId = magMenuResponse.getTermId();
                termName = magMenuResponse.getTitle();
                list.clear();
                recyclerView.stopScroll();
                if (postAdapter != null)
                    postAdapter.notifyDataSetChanged();
                postAdapter = null;
                inLoading = false;
                endOfList = false;
                getPostList();
                if (subAdapter != null)
                    subAdapter.setSubMenu(magMenuResponse);
                beforMagmenu = magMenuResponse;
            }

        }*/


    }

}