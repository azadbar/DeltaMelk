package ir.delta.delta.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import io.sentry.Sentry;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.splash.SplashConsultantActivity;
import ir.delta.delta.splash.SplashMagActivity;

public class StartActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.test);
//        overridePendingTransition(R.anim.pop_enter, R.anim.pop_exit);


        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle params = new Bundle();
        params.putString("open_app", String.valueOf(System.currentTimeMillis()));
        mFirebaseAnalytics.logEvent("open_app_first", params);


        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        if (BuildConfig.isCounsultant) {
            FirebaseMessaging.getInstance().subscribeToTopic("Consultant");
            FirebaseMessaging.getInstance().subscribeToTopic("Consultant-Android");
        } else {
            FirebaseMessaging.getInstance().subscribeToTopic("DeltaMag");
            FirebaseMessaging.getInstance().subscribeToTopic("DeltaMag-Android");
        }

        FirebaseMessaging.getInstance().subscribeToTopic("General");
        FirebaseMessaging.getInstance().subscribeToTopic("General-Android");

        if (!BuildConfig.isCounsultant)
            FirebaseMessaging.getInstance().subscribeToTopic("Test");


//        FirebaseMessaging.getInstance().subscribeToTopic("MAN");

        Intent intent;
        if (BuildConfig.isCounsultant) {
            intent = new Intent(this, SplashConsultantActivity.class);
            if (getIntent().getExtras() != null) {
                intent.putExtra("id", getIntent().getExtras().getString("id"));
                intent.putExtra("link", getIntent().getExtras().getString("link"));
            }
//            intent.putExtra("isFirst", true);
        } else {
            intent = new Intent(this, SplashMagActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            if (getIntent() != null) {                // ATTENTION: This was auto-generated to handle app links.
                if (getIntent().getData() != null && getIntent().getData().getEncodedPath() != null) {
                    String[] split = getIntent().getData().getEncodedPath().split("/");
                    if (split.length > 1 ) {
                        intent.putExtra("id", split[1]);
                    }
                } else {
                    if (getIntent().getExtras() != null) {
                        intent.putExtra("id", getIntent().getExtras().getString("postId"));
                        intent.putExtra("link", getIntent().getExtras().getString("link"));
                    }
                }
            }
        }
        startActivity(intent);
        finish();


    }
//    private void createCustomEventFirebase(FirebaseAnalytics mFirebaseAnalytics, String parameterName, String parameter, String eventName) {
//        Bundle params = new Bundle();
//        params.putString(parameterName, parameter);
//        mFirebaseAnalytics.logEvent(eventName, params);
//    }

}
