package ir.delta.delta.mag;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.service.ResponseModel.mag.Comment;

import static android.view.View.VISIBLE;

public class CommentActivity extends BaseActivity implements CommentAdapter.onItemClick {

    @BindView(R.id.rvComment)
    RecyclerView rvComment;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rlInsertComment)
    BaseRelativeLayout rlInsertComment;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.insert_comment)
    BaseLinearLayout insertComment;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.rlRoot)
    BaseRelativeLayout rlRoot;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;

    private boolean commentStatus;
    private int postId;
    private String myScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);

        seStatusBarColor(getResources().getColor(R.color.magToolbarFore));


        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setVisibility(View.VISIBLE);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.black));
        imgBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black));
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = rlRoot.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            rlRoot.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
        rlRoot.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgSearch.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            ArrayList<Comment> comment = (ArrayList<Comment>) bundle.getSerializable("comments");
            commentStatus = bundle.getBoolean("commentStatus");
            postId = bundle.getInt("postId");
            myScore = bundle.getString("myScore");
            if (comment != null) {
                paperTreeList(comment);
                tvFilterTitle.setText(getResources().getString(R.string.see_all_comment, comment.size()));
            }
        }

        if (commentStatus) {
            rlInsertComment.setVisibility(View.GONE);
            insertComment.setVisibility(View.VISIBLE);
        } else {
            rlInsertComment.setVisibility(View.GONE);
            insertComment.setVisibility(View.GONE);
        }
    }

    private void paperTreeList(ArrayList<Comment> comment) {
        ArrayList<Comment> list = new ArrayList<>();
        int index = 0;
        for (Comment com : comment) {
            if (com.isRoot()) {
                com.setIndex(index);
                com.setLevel(0);//because root
                for (int i = 0; i < comment.size(); i++) {
                    if (com.getId().equals(comment.get(i).getParentId())) {
                        com.setHasChild(true);
                        break;
                    }
                }
                list.add(com);
                setChild(index, 0, com.getId(), list, comment);
                index++;
            }
        }
        setAdapter(list);

    }


    private void setAdapter(ArrayList<Comment> list) {
        CommentAdapter adapter = new CommentAdapter(list, commentStatus, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvComment.setHasFixedSize(true);
        rvComment.setLayoutManager(layoutManager);
        rvComment.setAdapter(adapter);
    }

    private void setChild(int index, int currentLevel, String parentId, ArrayList<Comment> list, ArrayList<Comment> mainList) {
        for (Comment com : mainList) {
            if (TextUtils.equals(com.getParentId(), parentId)) {
                com.setIndex(index);
                com.setLevel(currentLevel + 1);//because root
                list.add(com);
                setChild(index, currentLevel + 1, com.getId(), list, mainList);
            }
        }
    }

    @Override
    public void onItemClick(int position, Comment comment) {
        PostCommentDialog dialog = new PostCommentDialog(this);
        dialog.setPostId(postId);
        dialog.setParentId(Integer.parseInt(comment.getId()));
        dialog.show();
    }

    @OnClick({R.id.rlBack, R.id.rlInsertComment, R.id.insert_comment})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlInsertComment:
            case R.id.insert_comment:
                PostCommentDialog dialog = new PostCommentDialog(this);
                dialog.setPostId(postId);
                dialog.setParentId(0);
                dialog.setMyScore(myScore);
                dialog.show();
                break;
        }

    }
}
