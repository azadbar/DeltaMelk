package ir.delta.delta.mag;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.ViewHolder> {

    private final ArrayList<MagMenuResponse> list;
    private final SubMenuAdapter.OnItemClickListener listener;
    private MagMenuResponse magMenuResponse;

    public SubMenuAdapter(ArrayList<MagMenuResponse> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        magMenuResponse = list.get(0);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_menu, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

     /*   Resources res = holder.itemView.getContext().getResources();
        MagMenuResponse object = list.get(position);
        holder.subMenuTitle.setText(object.getTitle());

        if (magMenuResponse != null && (magMenuResponse.getTermId().equals(object.getTermId()))) {
            setSelcted(res, holder);
        } else {
            setNormal(res, holder);
        }
        holder.bind(position, object,list, listener);*/
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.subMenuTitle)
        BaseTextView subMenuTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, MagMenuResponse magMenuResponse, ArrayList<MagMenuResponse> list, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemSubMenu(position, magMenuResponse, list));
        }

    }

    public void setSubMenu(MagMenuResponse magMenuResponse) {
        this.magMenuResponse = magMenuResponse;
        notifyDataSetChanged();
    }

    private void setSelcted(Resources res, ViewHolder holder) {
        holder.subMenuTitle.setTextColor(res.getColor(R.color.redColor));
        holder.subMenuTitle.setFontType(FontTypeEnum.BOLD);
        holder.subMenuTitle.setTextSize(res.getDimensionPixelSize(R.dimen.text_size_3));
    }

    private void setNormal(Resources res, ViewHolder holder) {
        holder.subMenuTitle.setTextColor(res.getColor(R.color.primaryTextColor));
        holder.subMenuTitle.setFontType(FontTypeEnum.MEDIUM);
        holder.subMenuTitle.setTextSize(res.getDimensionPixelSize(R.dimen.text_size_2));
    }

    public interface OnItemClickListener {
        void onItemSubMenu(int position, MagMenuResponse magMenuResponse, ArrayList<MagMenuResponse> list);
    }
}
