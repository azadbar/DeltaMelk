package ir.delta.delta.mag.searchMag;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.mag.PostAdapter;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestCallback;
import ir.delta.delta.service.RequestListener;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class SearchMagFragment extends BaseFragment implements PostAdapter.onItemClick, PostContentActivity.setBackgroundReadItem {


    @BindView(R.id.edtEstateCode)
    BaseEditText edtEstateCode;
    @BindView(R.id.btnSearch)
    BaseImageView btnSearch;
    @BindView(R.id.recycle)
    RecyclerView recycle;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.rlRoot)
    BaseRelativeLayout rlRoot;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.horizontal_progress)
    ProgressBar horizontalProgress;
    @BindView(R.id.btnClear)
    BaseImageView btnClear;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    private Unbinder unbinder;
    private final ArrayList<MagPost> list = new ArrayList<>();
    private PostAdapter adapter;
    private final int seed = 30;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private Timer timer;
    private ArrayList<Integer> readPost;
    private ArrayList<Integer> favoriteList;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Bundle params;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_search_mag, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = rlRoot.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            rlRoot.setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.search_in_mag);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black));
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));
        rlRoot.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        horizontalProgress.setVisibility(View.GONE);

        readPost = TransactionReadPost.getInstance().getReadPostsId(getContext());

        //read post for change background
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        params = new Bundle();


        edtEstateCode.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 2000; // milliseconds

            @Override
            public void afterTextChanged(Editable s) {
//                horizontalProgress.setVisibility(View.GONE);

                btnClear.setVisibility(VISIBLE);
                btnSearch.setVisibility(View.GONE);
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty() && edtEstateCode.getValueString().length() > 2) {
                    timer.cancel();
                    timer = new Timer();
                    timer.schedule(
                            new TimerTask() {
                                @Override
                                public void run() {
                                    if (getActivity() != null) {
                                        getActivity().runOnUiThread(() -> {
                                            if (!TextUtils.isEmpty(s.toString().trim())) {
                                                list.clear();
                                                endOfList = false;
                                                inLoading = false;
                                                empty.setVisibility(View.GONE);
                                                getList();
                                            } else {
                                                horizontalProgress.setVisibility(View.GONE);
                                                list.clear();
                                                if (adapter != null)
                                                    adapter.notifyDataSetChanged();
                                            }
                                        });
                                    }
                                }
                            },
                            DELAY
                    );
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }


            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtEstateCode.getValueString() != null && !edtEstateCode.getValueString().isEmpty() && edtEstateCode.getValueString().length() > 2) {
                    horizontalProgress.setVisibility(VISIBLE);
                    if (timer != null) {
                        timer.cancel();
                    }
                }
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick({R.id.rlBack, R.id.btnClear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.btnClear:
                edtEstateCode.setText("");
                edtEstateCode.setHint(getResources().getString(R.string.search_mag));
                btnClear.setVisibility(View.GONE);
                btnSearch.setVisibility(VISIBLE);
                list.clear();
                if (adapter != null)
                    adapter.notifyDataSetChanged();
                break;
        }
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (list.isEmpty()) {
                horizontalProgress.setVisibility(VISIBLE);
            } else {
                rlLoding.setVisibility(VISIBLE);
            }
            searchMagRequest();
        }
    }

    private void searchMagRequest() {
        //
//        params.putString("SearchText", edtEstateCode.getValueString());
//        mFirebaseAnalytics.logEvent("Search_in_mag_keyword", params);
        if (edtEstateCode != null) {
            //for databeen
            params = new Bundle();
            params.putString("SearchText", edtEstateCode.getValueString());
            mFirebaseAnalytics.logEvent("Search_in_mag_keyword", params);
        }
        enableDisableViewGroup(root, false);
        new RequestCallback<>(getActivity(), ApiClient.getClient_MAG().create(ReqInterface.class).magSearch(list.size(), seed, edtEstateCode.getValueString()),
                new RequestListener<List<MagPost>>() {
                    @Override
                    public void onResponse(@NonNull Response<List<MagPost>> response) {
                        enableDisableViewGroup(root, true);
                        try {

                            if (getView() != null && isAdded()) {
                                setList(response.body());
                            }
                        } catch (Exception ex) {

                        }
                    }

                    @Override
                    public void onFailure(int code, @NonNull JSONObject jsonObject) {
                        if (getView() != null && isAdded()) {

                        }
                    }
                });
    }


    private void setList(List<MagPost> lists) {
        if (rlLoding == null) {
            return;
        }
        inLoading = false;
        horizontalProgress.setVisibility(View.GONE);
        rlLoding.setVisibility(View.GONE);
        enableDisableViewGroup(root, true);
        if (lists != null && lists.size() > 0) {

            if (lists.size() < seed) {
                endOfList = true;
            }
            list.addAll(lists);
            setAdapter();

            if (list.size() > 0) {
                empty.setVisibility(View.GONE);
            } else {
                empty.setVisibility(VISIBLE);
            }
        } else {
            empty.setVisibility(VISIBLE);
        }
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new PostAdapter(list, readPost, this, favoriteList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            recycle.setHasFixedSize(true);
            recycle.setLayoutManager(layoutManager);
            recycle.setAdapter(adapter);


            recycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();

                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList) {
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                        }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        Intent intent = new Intent(getActivity(), PostContentActivity.class);
        PostContentActivity.magPosts = list;//becuse too large list
        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("magPosts", list);
        bundle.putString("termId", "0");
        bundle.putInt("selectedIndex", position);
        intent.putExtras(bundle);
        PostContentActivity.listener = this;
        if (adapter != null)
            adapter.notifyDataSetChanged();
        startActivity(intent);
    }


    @Override
    public void onItemShare(MagPost object) {
        Constants.sharePostLink(getContext(), object.getId(), object.getLink(), object.getTitle(), object.getTerm_name());
    }

    @Override
    public void onItemNext(int position, ArrayList<MagPost> magPosts) {
        if (magPosts != null && magPosts.size() > 0) {
            readPost.add(magPosts.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(getContext(), magPosts.get(position).getId());
            if (adapter != null)
                adapter.updateReceiptsList(magPosts);
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();


        if (edtEstateCode.getValueString() == null) {
            errorMsgList.add("لطفا یک متن یا کلمه وارد کنید");
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


}
