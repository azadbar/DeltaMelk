package ir.delta.delta.mag.telegram;

public interface OnGetMorePostTelegram {

    void btnNextPost();
    void btnPreviousPost();
}
