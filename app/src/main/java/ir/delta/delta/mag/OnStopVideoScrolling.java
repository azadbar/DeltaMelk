package ir.delta.delta.mag;

import android.app.Activity;

import androidx.fragment.app.FragmentActivity;

public interface OnStopVideoScrolling {
    void onScroll();

    void initVideo(Activity activity);

    void releaseVideo();


}
