package ir.delta.delta.mag.postContentCell;

import android.graphics.Typeface;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindAnim;
import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.service.ResponseModel.mag.MagPostRow;
import ir.delta.delta.service.ResponseModel.mag.NodeImage;

public class RelatedPostHeaderViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.textHeader)
    BaseTextView text;

    public RelatedPostHeaderViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setContent(Object object) {
        String htmlTag = (String) object;
        text.setVisibility(View.VISIBLE);
        text.setText(htmlTag);
//        text.setTextSize(magFont.getSize(text.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.p : row.getHtmlTag()));
//        text.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));
    }


}
