package ir.delta.delta.mag;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomMultiLineEditText;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.mag.postContentCell.ShowCommentsHolder;
import ir.delta.delta.mag.postContentCell.onUpdateRateResponse;
import ir.delta.delta.orderAdvertising.OrderAdvertisingActivity;
import ir.delta.delta.service.Request.PostCommentService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.PostCommentReq;
import ir.delta.delta.service.ResponseModel.mag.CommentResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class PostCommentDialog extends Dialog {


    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;
    @BindView(R.id.edtDesc)
    CustomMultiLineEditText edtDesc;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.btnDone)
    BaseTextView btnDone;
    @BindView(R.id.imageClose)
    BaseImageView imageClose;
    @BindView(R.id.ratingBar)
    SimpleRatingBar ratingBar;
    @BindView(R.id.tvTxtBugReport)
    BaseTextView tvTxtBugReport;

    private int postId;
    private int parenId;
    private int commentsRate;
    private String score;
    private onUpdateRateResponse listener;

    public void setListener(onUpdateRateResponse listener) {
        this.listener = listener;
    }

    public PostCommentDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.post_comment_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(true);

//        User user = Constants.getUser(getContext());
//        if (user != null) {
//            if (!TextUtils.isEmpty(user.getDisplayName())) {
//                edtName.setTextBody(user.getDisplayName());
//            }
//
//            if (!TextUtils.isEmpty(user.getMobile())) {
//                edtMobile.setTextBody(user.getMobile());
//            }
//        }


        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
        if (parenId == 0) {
            ratingBar.setVisibility(View.VISIBLE);
            SpannableStringBuilder desc_one = new SpannableStringBuilder();
            desc_one.append("به این مطلب از ");
            int start = desc_one.length();
            desc_one.append("1");
            int end = desc_one.length();
            desc_one.append(" تا ");
            int start1 = desc_one.length();
            desc_one.append("5");
            int end1 = desc_one.length();
            desc_one.append(" امتیاز دهید");
            desc_one.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_one.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_one.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);

            desc_one.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.redColor)), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_one.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_one.setSpan(new RelativeSizeSpan(1.1f), start1, end1, 0);
            tvTxtBugReport.setText(desc_one);
        } else {
            SpannableStringBuilder desc_one = new SpannableStringBuilder();
            desc_one.append("نظر خود را وارد کنید");
            tvTxtBugReport.setText(desc_one);
            ratingBar.setVisibility(View.GONE);
        }

        if (score != null) {
            ratingBar.setRating(Float.parseFloat(score));
            commentsRate = Integer.parseInt(score);
        }
        ratingBar.setStarCornerRadius(20f, getContext().getResources().getDimensionPixelSize(R.dimen.margin_1));
        ratingBar.setStarBorderWidth(10f, getContext().getResources().getDimensionPixelSize(R.dimen.margin_1));
        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            commentsRate = (int) ratingBar.getRating();
        });
    }


    @OnClick({R.id.btnDone, R.id.imageClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDone:

                if (parenId == 0) {
                    if (commentsRate > 0) {
                        if (edtMobile.getError() == null) {
                            requestPostComment();
                        } else {
                            Toast.makeText(getContext(), edtMobile.getError(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Constants.setToastFont(ratingBar.getContext(),
                                ratingBar.getContext().getResources().getString(R.string.plases_enter_selected_star), 16);
                    }
                } else {
                    if (edtDesc.getTextVal() != null) {
                        requestPostComment();
                    } else {
                        Constants.setToastFont(ratingBar.getContext(),
                                ratingBar.getContext().getResources().getString(R.string.plase_insert_comment), 16);
                    }
                }

                break;
            case R.id.imageClose:
                dismiss();
                break;
        }

    }

    private void requestPostComment() {
        imageClose.setVisibility(View.GONE);
        progressDialog.setVisibility(View.VISIBLE);
        PostCommentReq req = new PostCommentReq();
        req.setToken(PreferencesData.getToken(getContext()));
        req.setScore(commentsRate);
        req.setPostId(postId);
        req.setParentId(parenId);
        req.setContent(edtDesc.getTextVal());
        req.setName(edtName.getValueString());
        req.setMobile(edtMobile.getValueString());
        PostCommentService.getInstance().postComment(getContext().getResources(), req, new ResponseListener<CommentResponse>() {
            @Override
            public void onGetError(String error) {
                progressDialog.setVisibility(View.GONE);
                imageClose.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            public void onSuccess(CommentResponse response) {
                imageClose.setVisibility(View.VISIBLE);
                progressDialog.setVisibility(View.GONE);
                dismiss();
                CustomDialog dialog = new CustomDialog(getContext());
                dialog.setCancelable(true);
                dialog.setOkListener(getContext().getResources().getString(R.string.ok), v -> dialog.dismiss());
                dialog.setColorTitle(getContext().getResources().getColor(R.color.primaryTextColor));
                if (response.isSuccessed()) {
                    dialog.setDialogTitle(getContext().getResources().getString(R.string.success_post_content));
                    dialog.setLottieAnim("tick.json", 0);
                    listener.onUpdateRate(response.getScoreCount(), response.getAverageScore(), String.valueOf(commentsRate));
                } else {
                    dialog.setDialogTitle("لطفا ورودی خود را بررسی نمایید");
                    dialog.setLottieAnim("error.json", 0);
                }
                dialog.show();
            }

            @Override
            public void onAuthorization() {

            }
        });
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public void setParentId(int parentId) {
        this.parenId = parentId;
    }


    public void setMyScore(String score) {
        this.score = score;
    }


}
