package ir.delta.delta.mag.postContentCell;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.customView.PlayerControlView;
import ir.delta.delta.customView.PlayerView;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.OnStopVideoScrolling;
import ir.delta.delta.mag.video.ExoPlayerActivity;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.util.DownloadImageThumbnail;

public class ShowCustomVideoHolder extends RecyclerView.ViewHolder implements PlayerControlView.ControllItemClick, OnStopVideoScrolling {

    //    @BindView(R.id.parent)
//    RelativeLayout parent;
    @BindView(R.id.imageMag)
    BaseImageView imageMag;
    @BindView(R.id.play)
    BaseImageView play;
    @BindView(R.id.progreesBar)
    ProgressBar progreesBar;

    private SimpleExoPlayer player;
    private PlayerView videoVew;
    private Uri uri;
    private boolean playWhenReady;
    private int mResumeWindow;
    private long mResumePosition;
    private Activity activity;
//    private OnStopVideoScrolling stopVideoScrollListener;


    public ShowCustomVideoHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

    }


    public void setContent(Object object, PostDetailResponse response,int id) {
        HtmlTag htmlTag = (HtmlTag) object;
//        uri = Uri.parse("http://172.16.25.110:8888/wordpress/wp-content/uploads/2019/09/تمرین.mp4");
        if (!TextUtils.isEmpty(htmlTag.getSrc())){
            uri = Uri.parse(htmlTag.getSrc());
        }

//        parent.removeAllViews();
//        parent.setBackgroundColor(row.getBackColor(parent.getResources().getColor(R.color.white)));
//        parent.setLayoutParams(new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.WRAP_CONTENT
//                , parent.getContext().getResources().getDimensionPixelSize(R.dimen.height_video_size)));
//        videoVew = new PlayerView(parent.getContext());
//        videoVew.setUseArtwork(true);
//        parent.addView(videoVew);

//        new DownloadImageThumbnail(imageMag,progreesBar,play).execute(row.getSrc());

        if (!TextUtils.isEmpty(response.getImage())) {
            Glide.with(imageMag.getContext()).load(response.getImage()).into(imageMag);
        } else {
            new DownloadImageThumbnail(imageMag, progreesBar, play).execute(htmlTag.getSrc());
        }
//        initVideoPlayer();
        MagPostContentAdapter.stopVideoScrollListener = this;

        imageMag.setOnClickListener(v -> {
            Intent intent = new Intent(imageMag.getContext(), ExoPlayerActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            Bundle bundle = new Bundle();
            bundle.putString("videoUri", htmlTag.getSrc());
            bundle.putString("link", response.getLink());
            bundle.putString("id", String.valueOf(id));
            intent.putExtras(bundle);
            imageMag.getContext().startActivity(intent);
            imageMag.setEnabled(false);
            imageMag.postDelayed(new Runnable() {
                @Override
                public void run() {
                    imageMag.setEnabled(true);
                }
            }, 1000);
        });
    }

//    private void initVideoPlayer() {
//        if (player != null)
//            return;
//
//        try {
//            player = ExoPlayerFactory.newSimpleInstance(
//                    new DefaultRenderersFactory(parent.getContext()),
//                    new DefaultTrackSelector(), new DefaultLoadControl());
//            videoVew.setPlayer(player);
//            MediaSource mediaSource = buildMediaSource(uri);
//            player.prepare(mediaSource, true, false);
//            player.seekTo(100);
//            videoVew.setControllItemClick(ShowCustomVideoHolder.this);
//            player.setPlayWhenReady(playWhenReady);
//            Log.e("Exception", "Try");
//        } catch (Exception ex) {
//            player = ExoPlayerFactory.newSimpleInstance(
//                    new DefaultRenderersFactory(parent.getContext()),
//                    new DefaultTrackSelector(), new DefaultLoadControl());
//            videoVew.setPlayer(player);
//            MediaSource mediaSource = buildMediaSource(uri);
//            player.prepare(mediaSource, true, false);
//            player.seekTo(100);
//            videoVew.setControllItemClick(ShowCustomVideoHolder.this);
//            player.setPlayWhenReady(playWhenReady);
//            Log.e("Exception", "Catch");
//        }


//        player.seekTo(mResumeWindow, mResumePosition);


//    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(uri);
    }

    private void releasePlayer() {
        if (player != null) {
            mResumePosition = player.getCurrentPosition();
            mResumeWindow = player.getCurrentWindowIndex();
            playWhenReady = false;
            player.release();
            player.setPlayWhenReady(false);
            player = null;

        }
    }


    @Override
    public void onFullscreenClick() {

    }

    @Override
    public void onScroll() {
//        releasePlayer();
//        initVideoPlayer();
    }

    @Override
    public void initVideo(Activity activity) {
        this.activity = activity;
    }


    @Override
    public void releaseVideo() {
//        releasePlayer();
    }
}
