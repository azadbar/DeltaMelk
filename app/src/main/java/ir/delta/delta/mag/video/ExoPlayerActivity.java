package ir.delta.delta.mag.video;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.mag.video.hauler.HaulerView;
import ir.delta.delta.pushNotification.MagPostContentPushResponse;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.UpdatePostViewCountService;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

public class ExoPlayerActivity extends BaseActivity implements Player.EventListener {


    @BindView(R.id.videoFullScreenPlayer)
    PlayerView videoFullScreenPlayer;
    @BindView(R.id.spinnerVideoDetails)
    ProgressBar spinnerVideoDetails;
    @BindView(R.id.imageViewExit)
    ImageView imageViewExit;

    String videoUri;
    SimpleExoPlayer player;
    Handler mHandler;
    Runnable mRunnable;
    @BindView(R.id.haulerView)
    HaulerView haulerView;
    @BindView(R.id.title)
    BaseTextView title;
    @BindView(R.id.imageShare)
    ImageView imageShare;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.imageViewExitWeb)
    ImageView imageViewExitWeb;
    @BindView(R.id.imageShareWeb)
    ImageView imageShareWeb;
    @BindView(R.id.rlTitle)
    LinearLayout rlTitle;
    @BindView(R.id.haulerViewWeb)
    HaulerView haulerViewWeb;
    @BindView(R.id.root)
    ConstraintLayout root;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private String titleString;
    private String link;
    private MagPost mag;
    private String id;

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_exo_player);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mag = bundle.getParcelable("magPost");
            videoUri = bundle.getString("videoUri");
            link = bundle.getString("link");
            titleString = bundle.getString("titleString");
            id = bundle.getString("id");
        }

        progressBar.setVisibility(View.GONE);
        haulerView.setVisibility(View.VISIBLE);
        title.setText(titleString);
        setUp();
        shareLink();
        haulerView.setOnDragDismissedListener(dragDirection -> finish());

        //request for vot in video detail
        UpdatePostViewCountService.getInstance().updatePostViewCount(getResources(), mag != null ? mag.getId() : Integer.parseInt(id), PreferencesData.getToken(this), new ResponseListener<MagPostContentPushResponse>() {
            @Override
            public void onGetError(String error) {

            }

            @Override
            public void onSuccess(MagPostContentPushResponse response) {

            }

            @Override
            public void onAuthorization() {

            }
        });

    }



    private void shareLink() {
        imageShare.setOnClickListener(v -> shareText());
    }

    private void shareText() {
        Constants.sharePostLink(this,Integer.parseInt(id) ,link, titleString, "");
    }

    private void setUp() {
        initializePlayer();
        if (videoUri == null) {
            return;
        }
        buildMediaSource(Uri.parse(videoUri));
    }

    @OnClick({R.id.imageViewExit, R.id.imageViewExitWeb})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imageViewExit:
            case R.id.imageViewExitWeb:
                finish();
                break;
        }

    }

    private void initializePlayer() {
        if (player == null) {
            // 1. Create a default TrackSelector
            TrackSelector trackSelector = new DefaultTrackSelector();

            DefaultLoadControl loadControl = new DefaultLoadControl.Builder()
                    .setBufferDurationsMs(32*1024, 64*1024, 1024, 1024)
                    .createDefaultLoadControl();
            player = ExoPlayerFactory.newSimpleInstance(this, trackSelector, loadControl);
            videoFullScreenPlayer.setPlayer(player);
        }
    }

    private void buildMediaSource(Uri mUri) {
        // Measures bandwidth during playback. Can be null if not required.
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, getString(R.string.app_name)), bandwidthMeter);
        // This is the MediaSource representing the media to be played.
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                .createMediaSource(mUri);
        // Prepare the player with the source.
        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(this);
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    private void pausePlayer() {
        if (player != null) {
            player.setPlayWhenReady(false);
            player.getPlaybackState();
        }
    }

    private void resumePlayer() {
        if (player != null) {
            player.setPlayWhenReady(true);
            player.getPlaybackState();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (mRunnable != null) {
            mHandler.removeCallbacks(mRunnable);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        resumePlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releasePlayer();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {

            case Player.STATE_BUFFERING:
                spinnerVideoDetails.setVisibility(View.VISIBLE);
                break;
            case Player.STATE_ENDED:
                // Activate the force enable
                break;
            case Player.STATE_IDLE:

                break;
            case Player.STATE_READY:
                spinnerVideoDetails.setVisibility(View.GONE);

                break;
            default:
                // status = PlaybackStatus.IDLE;
                break;
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }


}
