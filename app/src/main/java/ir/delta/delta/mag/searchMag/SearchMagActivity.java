package ir.delta.delta.mag.searchMag;

import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;

public class SearchMagActivity extends BaseActivity {

    @BindView(R.id.frameLayout_mag)
    LinearLayout frameLayoutMag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_mag);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        SearchMagFragment estateDetailByCodeFragment = new SearchMagFragment();
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frameLayout_mag, estateDetailByCodeFragment);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }
}
