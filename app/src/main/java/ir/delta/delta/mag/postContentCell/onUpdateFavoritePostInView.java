package ir.delta.delta.mag.postContentCell;

public interface onUpdateFavoritePostInView {
    void onUpdateFavoriteInView(boolean isFavorite, int id);
}
