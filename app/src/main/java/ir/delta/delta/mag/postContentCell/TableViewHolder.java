package ir.delta.delta.mag.postContentCell;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.mag.MagPostContentWebViewActivity;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.detail.TablePressRow;
import ir.delta.delta.util.Constants;

public class TableViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imageMag)
    BaseImageView imageMag;
    @BindView(R.id.textView)
    BaseTextView textView;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.parent)
    BaseRelativeLayout parent;

    public TableViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);


    }

    public void setContent(Object object) {
        TablePressRow tablePressRow = (TablePressRow) object;
        if (!TextUtils.isEmpty(tablePressRow.getTitle())) {
            textView.setText(tablePressRow.getTitle().replace("null",""));
//            textView.setTextSize(magFont.getSize(textView.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.p : row.getHtmlTag()));
//            textView.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));
            textView.setTextColor(textView.getResources().getColor(R.color.primaryTextColor));
        } else {
            textView.setText(null);
        }

        if (!TextUtils.isEmpty(tablePressRow.getSrc())) {
            imageMag.setVisibility(View.VISIBLE);
            Glide.with(imageMag.getContext()).load(tablePressRow.getSrc()).placeholder(R.drawable.placeholder).into(imageMag);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageMag.getLayoutParams();
            if (tablePressRow.getHeightFactor() <= 1) {
                params.width = imageMag.getResources().getDimensionPixelSize(R.dimen.height_size_item_search_estate);
                params.height = (int) (params.width * tablePressRow.getHeightFactor());
            } else {
                params.height = imageMag.getResources().getDimensionPixelSize(R.dimen.height_size_item_search_estate);
                params.width = (int) (params.height / tablePressRow.getHeightFactor());
            }


        } else {
            imageMag.setVisibility(View.GONE);
        }


//        textView.setOnClickListener(view -> {
//            Intent intent = new Intent(parent.getContext(), MagPostContentWebViewActivity.class);
//            intent.putExtra("link", row.getHref());//TODO check href
//            Log.e("link", row.getHref());
//            textView.getContext().startActivity(intent);
//        });
    }


}
