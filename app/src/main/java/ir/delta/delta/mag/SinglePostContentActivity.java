package ir.delta.delta.mag;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.PostRowTypeEnum;
import ir.delta.delta.listener.AppBarStateChangeListener;
import ir.delta.delta.mag.postContentCell.onUpdateFavoritePost;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.MagDetialService2;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.TableVisibility;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ResponseModel.detail.PostDetailRow;
import ir.delta.delta.service.ResponseModel.detail.TablePressRow;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

import static ir.delta.delta.enums.PostRowTypeEnum.author;
import static ir.delta.delta.enums.PostRowTypeEnum.script;
import static ir.delta.delta.enums.PostRowTypeEnum.tablePress;
import static ir.delta.delta.favoriteMag.FavoriteMagListActivity.refreshMainPageListener;

public class SinglePostContentActivity extends BaseActivity implements MagPostContentAdapter.onItemClickListener,
        onUpdateFavoritePost {


    @BindView(R.id.recycle)
    RecyclerView recycle;

    Unbinder unbinder;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.imageHeader)
    ImageView imageHeader;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.imgShare)
    BaseImageView imgShare;
    @BindView(R.id.anim_toolbar)
    BaseToolbar animToolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.root)
    NestedScrollView root;
    @BindView(R.id.imgBookMark)
    BaseImageView imgBookMark;
    private MagPostContentAdapter adapter;

    private int recycleWidth;
    private PostDetailResponse response;

    //    private int id;
    private ArrayList<Integer> favoriteList;
    private AppBarStateChangeListener.State state;
    private boolean isFavorite;
    private ArrayList<PostDetailRow> list;
    private int id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_content_mag_single_swipe_fragment);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = rlBack.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            rlBack.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id =  Integer.parseInt(bundle.getString("id"));
        }


        list = new ArrayList<>();
        //read post for change background
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(this);


        if (favoriteList.contains(id)) {
            imgBookMark.setImageResource(R.drawable.ic_bookmark);
            imgBookMark.setColorFilter(imgBookMark.getContext().getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        } else {
            imgBookMark.setImageResource(R.drawable.ic_bookmark_border);
            imgBookMark.setColorFilter(imgBookMark.getContext().getResources().getColor(R.color.gray_300), PorterDuff.Mode.SRC_ATOP);
        }

        rlBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black));
        imgShare.setVisibility(View.VISIBLE);
        imgShare.setColorFilter(getResources().getColor(R.color.black));
        imgBookMark.setVisibility(View.VISIBLE);
        imgBookMark.setColorFilter(getResources().getColor(R.color.black));
        ViewTreeObserver viewTreeObserver = imageHeader.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    imageHeader.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    imageHeader.getLayoutParams().height = (int) (imageHeader.getWidth() / 1.75);
                }
            });
        }



        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            Point size = Constants.getScreenSize(windowManager);
            recycleWidth = size.x;
        }

        appbar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if (state == State.COLLAPSED) {
                    imgBack.setColorFilter(getResources().getColor(R.color.black));
                    imgShare.setColorFilter(getResources().getColor(R.color.black));
                    imgBookMark.setColorFilter(getResources().getColor(R.color.black));
                    imgBack.setBackground(null);
                    imgShare.setBackground(null);
                    imgBookMark.setBackground(null);
                    tvTitle.setVisibility(View.VISIBLE);
                    tvTitle.setText(response != null ? response.getTitle() : "");
                    ellipsizeText(tvTitle);
                } else if (state == State.EXPANDED) {
                    imgBack.setColorFilter(getResources().getColor(R.color.white));
                    imgShare.setColorFilter(getResources().getColor(R.color.white));
                    imgBookMark.setColorFilter(getResources().getColor(R.color.white));
                    imgBack.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.background_transparent_view,null));
                    imgShare.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.background_transparent_view,null));
                    imgBookMark.setBackground(ResourcesCompat.getDrawable(getResources(),R.drawable.background_transparent_view,null));
                    tvTitle.setVisibility(View.GONE);

                }
                SinglePostContentActivity.this.state = state;
            }
        });


        loadingView.setButtonClickListener(view -> getPostContent());
        getPostContent();

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.onScroll();
        }
    }

//    public void changeFont(MagFontEnum fontSize) {
//        this.magFont = fontSize;
//        if (adapter != null) {
//            adapter.setFontSize(fontSize);
//        }
//    }


    private void getPostContent() {
        //imgShare.setVisibility(View.GONE);

        root.setVisibility(View.GONE);
        appbar.setVisibility(View.INVISIBLE);
        loadingView.showLoading(true);
        MagDetialService2.getInstance().magDetail(getResources(), id,
                PreferencesData.getToken(SinglePostContentActivity.this), new ResponseListener<PostDetailResponse>() {
                    @Override
                    public void onGetError(String error) {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(getResources().getString(R.string.communicationError));
                    }

                    @Override
                    public void onAuthorization() {
                        Intent intent = new Intent(SinglePostContentActivity.this, LoginActivity.class);
                        Constants.setCurrentUser(null);
                        TransactionUser.getInstance().deleteUsr(SinglePostContentActivity.this);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onSuccess(PostDetailResponse response) {
                        appbar.setVisibility(View.VISIBLE);
                        root.setVisibility(View.VISIBLE);
                        loadingView.stopLoading();
                        SinglePostContentActivity.this.response = response;
                        createList(response);
                        setAdapter();
                        if ("1609".equals(response.getTermId()) || "28".equals(response.getTermId())) {
                            imageHeader.setVisibility(View.GONE);
                            imgBack.setColorFilter(getResources().getColor(R.color.black));
                            imgShare.setColorFilter(getResources().getColor(R.color.black));
                            imgBack.setBackground(null);
                            imgShare.setBackground(null);
                        } else {
                            imageHeader.setVisibility(View.VISIBLE);
                            Glide.with(SinglePostContentActivity.this).load(response.getImage()).placeholder(R.drawable.placeholder).into(imageHeader);
                        }
                    }

                });
    }

    private void createList(PostDetailResponse response) {
        list.clear();
        if (response.getTags() == null) {
            return;
        }
        list.add(new PostDetailRow(PostRowTypeEnum.header, null));
        for (HtmlTag tag : response.getTags()) {
            PostRowTypeEnum cellType = PostRowTypeEnum.getHtmlTag(tag.getNodeName());
            if (cellType == tablePress) {
                creatPostTableRow(tag.getTable(), tag.getVisibility());
            } else if (cellType == script) {
                tag.fillScriptRow();
                if (TextUtils.isEmpty(tag.getScriptHtml())) {
                    list.add(new PostDetailRow(cellType, tag));
                }
            } else {
                list.add(new PostDetailRow(cellType, tag));
            }
        }
        list.add(new PostDetailRow(author, ""));

        addRelatedPost();
        addComments();

    }


    private void addComments() {
//        if (response.getComments() == null || response.getComments().size() == 0) {
//            return;
//        }
        list.add(new PostDetailRow(PostRowTypeEnum.commentHeader,
                getResources().getString(R.string.comment_user_for_post, 0)));

//        ArrayList<Comment> parentList = new ArrayList<>();
//        for (Comment comment :
//                response.getComments()) {
//            if (comment.getParentId().equals("0")) {
//                parentList.add(comment);
//            }
//        }
//
//        int index = Math.min(2, parentList.size());
//        for (int i = 0; i < index; i++) {
//            list.add(new PostDetailRow(PostRowTypeEnum.comment, parentList.get(i)));
//
//        }
//
//        if (response.getComments().size() > 2) {
//            list.add(new PostDetailRow(PostRowTypeEnum.otherCommentButton, getResources().getString(R.string.more_comment)));
//        }
//
//        if (response.getCommentStatus().equals("open")) {
//            list.add(new PostDetailRow(PostRowTypeEnum.addCommentButton, getResources().getString(R.string.insert_comment_rate)));
//        }

    }


    private void addRelatedPost() {
        if (response.getRelatedPosts() == null || response.getRelatedPosts().size() == 0) {
            return;
        }
        list.add(new PostDetailRow(PostRowTypeEnum.relatedPostTitle, getResources().getString(R.string.also_read)));
        for (MagPost post :
                response.getRelatedPosts()) {
            list.add(new PostDetailRow(PostRowTypeEnum.relatedPost, post));

        }
    }


    private void creatPostTableRow(ArrayList<ArrayList<String>> table, TableVisibility visibility) {
        if (table != null && visibility != null) {
            for (int i = 0; i < table.size(); i++) {
                if (visibility.getRows().size() == 0 ||
                        (i < visibility.getRows().size() && visibility.getRows().get(i) == 1)) {
                    ArrayList<String> row = table.get(i);
                    TablePressRow tablePressRow = new TablePressRow(row, visibility);
                    list.add(new PostDetailRow(tablePress, tablePressRow));
                }
            }
        }
    }

    private void setAdapter() {
        adapter = new MagPostContentAdapter(list, response, id, recycleWidth, this);
        adapter.setOnUpdateFavoritePost(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(SinglePostContentActivity.this, RecyclerView.VERTICAL, false);
        recycle.smoothScrollToPosition(0);//TODO change scroll
        layoutManager.scrollToPositionWithOffset(0, 0);
        recycle.setHasFixedSize(true);
        recycle.setItemViewCacheSize(20);
        recycle.setDrawingCacheEnabled(true);
        recycle.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recycle.setLayoutManager(layoutManager);
        recycle.setAdapter(adapter);

    }


    @Override
    public void onItemClick(int position) {


    }


    @OnClick({R.id.rlBack, R.id.imgShare, R.id.imgBookMark})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.imgShare:
                Constants.sharePostLink(this,id,response.getLink(), response.getTitle(), response.getTermName());
                break;
            case R.id.imgBookMark:
                favoriteItem();
                MagPostContentAdapter.onUpdateFavoritePostInViewListener.onUpdateFavoriteInView(isFavorite,id);
                break;
        }
    }

    private void favoriteItem() {

        if (id == 0)
            return;

        int index = favoriteList.indexOf(id);
        if (index >= 0) {
            favoriteList.remove(index);
            TransactionFavoriteMag.getInstance().delete(imgBookMark.getContext(),id);
            imgBookMark.setImageResource(R.drawable.ic_bookmark_border);
            imgBookMark.setColorFilter(imgBookMark.getContext().getResources()
                            .getColor(state == AppBarStateChangeListener.State.COLLAPSED ? R.color.gray_300 : R.color.white)
                    , PorterDuff.Mode.SRC_ATOP);
            Constants.setToastFont(this, getString(R.string.delete_favorite_mag_text), 16);
            isFavorite = false;
        } else {
            favoriteList.add(id);
            MagPost magPost = new MagPost();
            magPost.setId(id);
            magPost.setTitle(response.getTitle());
            magPost.setAuthor(response.getAuthor());
            magPost.setTerm_name(response.getTermName());
            magPost.setTermId(response.getTermId());
            magPost.setCommentStatus(response.getCommentStatus());
            magPost.setViewCount(response.getViewCount());
            magPost.setImage(response.getImage());
            magPost.setDate(response.getDate());
            TransactionFavoriteMag.getInstance().saveMagPost(imgBookMark.getContext(), magPost);
            imgBookMark.setImageResource(R.drawable.ic_bookmark);
            imgBookMark.setColorFilter(imgBookMark.getContext().getResources()
                            .getColor(state == AppBarStateChangeListener.State.COLLAPSED ? R.color.black : R.color.white)
                    , PorterDuff.Mode.SRC_ATOP);
            Constants.setToastFont(this, getString(R.string.add_favorite_mag_text), 16);
            isFavorite = true;

        }
        if (refreshMainPageListener != null)
            refreshMainPageListener.onRefresh();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.onScroll();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.onScroll();
        }
    }

    @Override
    public void onUpdateFavorite(boolean isFavorite,int id) {
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(this);
        int index = favoriteList.indexOf(id);
        if (isFavorite && (index >=0)) {
            imgBookMark.setImageResource(R.drawable.ic_bookmark);
            imgBookMark.setColorFilter(getResources().getColor(R.color.black));
        } else {
            imgBookMark.setImageResource(R.drawable.ic_bookmark_border);
            imgBookMark.setColorFilter(imgBookMark.getContext().getResources().getColor(R.color.gray_300), PorterDuff.Mode.SRC_ATOP);
        }

        if (refreshMainPageListener != null)
            refreshMainPageListener.onRefresh();
    }
}
