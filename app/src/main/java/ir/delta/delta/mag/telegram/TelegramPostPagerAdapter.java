package ir.delta.delta.mag.telegram;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;

import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class TelegramPostPagerAdapter extends FragmentStatePagerAdapter {

    private final Map<Integer, WeakReference<Fragment>> fragments;
    private final ArrayList<MagPost> magPosts;
    private MagFontEnum fontSize;
    private final ContainerFragment.OnScroolListener scrollListener;
    private final OnGetMorePostTelegram morePostTelegramListener;

    public TelegramPostPagerAdapter(FragmentManager fm, ArrayList<MagPost> magPosts, MagFontEnum fontSize, ContainerFragment.OnScroolListener scrollListener,
                                    OnGetMorePostTelegram morePostTelegramListener) {
        super(fm);
        this.magPosts = magPosts;
        this.fontSize = fontSize;
        this.scrollListener = scrollListener;
        this.morePostTelegramListener = morePostTelegramListener;
        fragments = new WeakHashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        PostContentTelegramFragment postContentTelegramFragment = PostContentTelegramFragment.newInstance(magPosts.get(position), fontSize);
        postContentTelegramFragment.setListener(scrollListener);
        postContentTelegramFragment.setMoreTelegramPostListener(morePostTelegramListener);
        return postContentTelegramFragment;
    }


    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof PostContentTelegramFragment) {
            PostContentTelegramFragment fragment = (PostContentTelegramFragment) object;
            fragments.put(position, new WeakReference<>(fragment));
        }
        return object;
    }


    @Override
    public int getCount() {
        return magPosts.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void setFragment(MagFontEnum fontSize) {
        this.fontSize = fontSize;
        for (WeakReference<Fragment> f : fragments.values()) {
            Fragment fra = f.get();
            if (fra != null) {
                ((PostContentTelegramFragment) fra).changeFont(fontSize);
            }
        }
    }
}