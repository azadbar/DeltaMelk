package ir.delta.delta.mag;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.enums.PostRowTypeEnum;
import ir.delta.delta.mag.postContentCell.AuthorViewHolder;
import ir.delta.delta.mag.postContentCell.GalleryViewHolder;
import ir.delta.delta.mag.postContentCell.HeaderViewHolder;
import ir.delta.delta.mag.postContentCell.ImageViewHolder;
import ir.delta.delta.mag.postContentCell.RelatedPostHeaderViewHolder;
import ir.delta.delta.mag.postContentCell.RelatedPostLinkViewHolder;
import ir.delta.delta.mag.postContentCell.ShowCommentsHolder;
import ir.delta.delta.mag.postContentCell.ShowCustomVideoHolder;
import ir.delta.delta.mag.postContentCell.TableViewHolder;
import ir.delta.delta.mag.postContentCell.TextViewHolder;
import ir.delta.delta.mag.postContentCell.VideoViewHolder;
import ir.delta.delta.mag.postContentCell.onUpdateFavoritePost;
import ir.delta.delta.mag.postContentCell.onUpdateFavoritePostInView;
import ir.delta.delta.mag.telegram.OnVisibleTelegramButton;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ResponseModel.detail.PostDetailRow;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

/**
 * Created by m.azadbar on 9/21/2017.
 */

public class MagPostContentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private PostDetailResponse response;
    private int id;
    private int recycleWidth;
    private final onItemClickListener listener;
    public static OnVisibleTelegramButton magVisibilityState;
    public static OnStopVideoScrolling stopVideoScrollListener;
    public static onUpdateFavoritePostInView onUpdateFavoritePostInViewListener;
    private int selectedIndex;
    private int listSizeTelegram;
    private onUpdateFavoritePost onUpdateFavoritePost;
    private ArrayList<PostDetailRow> list;
    private PostRowTypeEnum tag;


    public MagPostContentAdapter(ArrayList<PostDetailRow> list, PostDetailResponse response, int id, int recycleWidth, onItemClickListener listener) {
        this.list = list;
        this.response = response;
        this.id = id;
        this.recycleWidth = recycleWidth;
        this.listener = listener;
    }


    @Override
    public int getItemViewType(int position) {


        tag = list.get(position == list.size() ? position - 1 : position).getType();
        if (tag == PostRowTypeEnum.header) {
            return 0;
        }

        if (tag == PostRowTypeEnum.htmlTag) {
            return 1;
        }
        if (tag == PostRowTypeEnum.img) {
            return 2;
        }

        if (tag == PostRowTypeEnum.script) {
            return 3;
        }

        if (tag == PostRowTypeEnum.video) {
            return 4;
        }

        if (tag == PostRowTypeEnum.gallery) {
            return 5;
        }

        if (tag == PostRowTypeEnum.tablePress) {
            return 6;
        }

        if (tag == PostRowTypeEnum.author) {
            return 7;
        }

        if (tag == PostRowTypeEnum.relatedPostTitle) {
            return 8;
        }

        if (tag == PostRowTypeEnum.relatedPost) {
            return 9;
        }

        if (tag == PostRowTypeEnum.commentHeader) {
            return 10;
        }

//        if (tag == PostRowTypeEnum.comment) {
//            return 11;
//        }
//
//        if (tag == PostRowTypeEnum.addCommentButton) {
//            return 12;
//        }
//
//        if (tag == PostRowTypeEnum.otherCommentButton) {
//            return 13;
//        }
        return list.get(position).getType().getMethodCode();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case 0:
                View addHeader = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header_post_content, parent, false);
                return new HeaderViewHolder(addHeader);
            case 2:
                View addImageView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_mag_post_content, parent, false);
                return new ImageViewHolder(addImageView);
            case 3:
                View addVideo = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_mag_post_content, parent, false);
                return new VideoViewHolder(addVideo);
            case 4:
                View customVideo = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_video_mag, parent, false);
                return new ShowCustomVideoHolder(customVideo);
            case 5:
                View addGallery = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gallery_mag, parent, false);
                return new GalleryViewHolder(addGallery);
            case 6:
                View addTable = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_table_mag_post_content, parent, false);
                return new TableViewHolder(addTable);
            case 7:
                View author = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_mag_post_content, parent, false);
                return new AuthorViewHolder(author);
            case 8:
                View relatedHeaderPost = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_related_posts, parent, false);
                return new RelatedPostHeaderViewHolder(relatedHeaderPost);
            case 9:
                View relatedPost = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycle_related_post, parent, false);
                return new RelatedPostLinkViewHolder(relatedPost);
            case 10:
                View comments = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_post_in_content, parent, false);
                return new ShowCommentsHolder(comments);
            default:
                View addTextView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_text_mag_post_content, parent, false);
                return new TextViewHolder(addTextView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.setContent(response);
                break;
            case 1:
                TextViewHolder textViewHolder = (TextViewHolder) holder;
                textViewHolder.setTextContent(list.get(position == list.size() ? position - 1 : position).getObject());
                break;
            case 2:
                ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
                imageViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject(), recycleWidth);
                break;
            case 3:
                VideoViewHolder videoViewHolder = (VideoViewHolder) holder;
                videoViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject(), recycleWidth);
                break;
            case 4:
                ShowCustomVideoHolder showCustomVideo = (ShowCustomVideoHolder) holder;
                showCustomVideo.setContent(list.get(position == list.size() ? position - 1 : position).getObject(), response, id);
                break;
            case 5:
                GalleryViewHolder galleryViewHolder = (GalleryViewHolder) holder;
                galleryViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject(), recycleWidth);
                break;
            case 6:
                TableViewHolder tableViewHolder = (TableViewHolder) holder;
                tableViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject());
                break;
            case 7:
                AuthorViewHolder authorViewHolder = (AuthorViewHolder) holder;
                authorViewHolder.setTextContent(id, response, onUpdateFavoritePost);
                break;
            case 8:
                RelatedPostHeaderViewHolder relatedPostHeaderViewHolder = (RelatedPostHeaderViewHolder) holder;
                relatedPostHeaderViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject());
                break;
            case 9:
                RelatedPostLinkViewHolder relatedPostLinkViewHolder = (RelatedPostLinkViewHolder) holder;
                relatedPostLinkViewHolder.setContent(list.get(position == list.size() ? position - 1 : position).getObject());
                break;
            case 10:
                ShowCommentsHolder showCommentsHolder = (ShowCommentsHolder) holder;
                showCommentsHolder.setContent(response, id, false);
                break;

        }

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.onItemClick(position - 1);
            }
        });

    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 1;
    }

    public void setIndexForTelegram(int selectedIndex, int listSize) {
        this.selectedIndex = selectedIndex;
        this.listSizeTelegram = listSize;
    }

    public void setOnUpdateFavoritePost(onUpdateFavoritePost onUpdateFavoritePost) {
        this.onUpdateFavoritePost = onUpdateFavoritePost;
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        BaseTextView text;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


    public void setFontSize(MagFontEnum fontSize) {
        notifyDataSetChanged();
    }

    public interface onItemClickListener {
        void onItemClick(int position);
    }
}
