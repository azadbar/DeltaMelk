package ir.delta.delta.mag;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.WeakHashMap;

import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.mag.main_page.OnRefreshMainPageList;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class PostPagerAdapter extends FragmentStatePagerAdapter {

    private final Map<Integer, WeakReference<Fragment>> fragments;
    private final ArrayList<MagPost> magPosts;
    private OnRefreshMainPageList refreshMainPageListener;

    public PostPagerAdapter(FragmentManager fm, ArrayList<MagPost> magPosts) {
        super(fm);
        this.magPosts = magPosts;
        fragments = new WeakHashMap<>();
    }

    @Override
    public Fragment getItem(int position) {
        return PostContentFragment.newInstance(magPosts.get(position).getId(),  refreshMainPageListener);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof PostContentFragment) {
            Fragment fragment = (Fragment) object;
            fragments.put(position, new WeakReference<>(fragment));
        }
        return object;
    }


    @Override
    public int getCount() {
        if (magPosts != null)
            return magPosts.size();
        else
            return 0;
    }

    public void setFragment(MagFontEnum fontSize) {
//        this.fontSize = fontSize;
//        for (WeakReference<Fragment> f : fragments.values()) {
//            Fragment fra = f.get();
//            if (fra != null) {
//                ((PostContentFragment) fra).changeFont(fontSize);
//            }
//        }
    }

    public void setRefreshList(OnRefreshMainPageList refreshMainPageListener) {
        this.refreshMainPageListener = refreshMainPageListener;
    }
}