package ir.delta.delta.mag.telegram;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.service.Request.MorePostsService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.service.ResponseModel.mag.MagPostResponse;


public class TelegramContainerFragment extends BaseFragment implements OnGetMorePostTelegram {


    @BindView(R.id.loadingView)
    LoadingView loadingView;
    //    @BindView(R.id.pagerTelegram)
    FrameLayout pager;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    private ArrayList<MagPost> magPosts;
    private int selectedIndex;
    private int termId;
    private final MagFontEnum fontSize = MagFontEnum.NORMAL;
    private final int seed = 30;
    TelegramPostPagerAdapter pagerAdapter;
    private ContainerFragment.OnScroolListener scrollListener;
    private ArrayList<MagPost> list;
    private MagPost selectedFragment;

    public void setScrollListener(ContainerFragment.OnScroolListener scrollListener) {
        this.scrollListener = scrollListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_telegram_contianer, container, false);
        ButterKnife.bind(this, view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = loadingView.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            loadingView.setSystemUiVisibility(flags);
            getActivity().getWindow().setStatusBarColor(Color.WHITE);
        }
        list = new ArrayList<>();

        pager = view.findViewById(R.id.frameTelegram);
//        pager = new LockableViewPager(getActivity());
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        pager.setLayoutParams(lp);
//        root.addView(pager);
        getPostList();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        scrollListener.onScrool(2);
    }

    private void getPostList() {
        MorePostsService.getInstance().getMorePost(getResources(), Integer.parseInt("5285"), 0, 30, new ResponseListener<MagPostResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                }
            }

            @Override
            public void onSuccess(MagPostResponse response) {
                if (getView() != null && isAdded()) {
                    if (response.getList() != null && response.getList().size() > 0) {
                        pager.setVisibility(View.VISIBLE);
                        list.addAll(response.getList());
                        selectedFragment = list.get(selectedIndex);
                        loadFra();
//                        if (pagerAdapter == null){
//                            pagerAdapter = new TelegramPostPagerAdapter(getChildFragmentManager(), list, fontSize, scrollListener, TelegramContainerFragment.this);
//                            pager.setOffscreenPageLimit(2);
//                            pager.setAdapter(pagerAdapter);
//                            pager.setOnTouchListener((v, event) -> true);
//                            pager.setSwipeable(false);
//                            pager.setCurrentItem(selectedIndex);
//                            setSelectedIndex();
//                        }

                    }
                }
            }
        });
    }

    private void loadFra() {
        PostContentTelegramFragment fragment = new PostContentTelegramFragment().newInstance(selectedFragment, fontSize);
        fragment.setListener(TelegramContainerFragment.this.scrollListener);
        fragment.setMoreTelegramPostListener(TelegramContainerFragment.this);
        fragment.setIndex(selectedIndex , list.size());
        FragmentManager fragMgr = getActivity().getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frameTelegram, fragment);
        fragTrans.commit();
    }

    @Override
    public void btnNextPost() {
        if (list.size() > 0) {
            if (selectedIndex == list.size() - 1) {
                selectedIndex = list.size() - 1;
            } else {
                selectedIndex += 1;
            }

            selectedFragment = list.get(selectedIndex);
            loadFra();
            setSelectedIndex();
        }
//        if (list.size() > 0) {
//            selectedIndex += 1;
//            pagerAdapter.notifyDataSetChanged();
////            pager.setCurrentItem(selectedIndex);
//            setSelectedIndex();
//
//        }

    }

    private void setSelectedIndex() {
        if (MagPostContentAdapter.magVisibilityState != null)
            MagPostContentAdapter.magVisibilityState.onStateVisibility(selectedIndex, list.size());
    }

    @Override
    public void btnPreviousPost() {

        if (selectedIndex > 0) {
            selectedIndex = 0;
            selectedFragment = list.get(selectedIndex);
            loadFra();
            setSelectedIndex();
        }
//        if (selectedIndex > 0) {
//            selectedIndex = 0;
//            pagerAdapter.notifyDataSetChanged();
////            pager.setCurrentItem(selectedIndex);
//            setSelectedIndex();
//        }
    }

}
