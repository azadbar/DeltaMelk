package ir.delta.delta.mag;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagLink;
import ir.delta.delta.util.Constants;


public class MagLinksDialog extends Dialog implements MagLinksAdapter.OnItemClickListener {


    private ArrayList<MagLink> magLinks;
    private String title;
    @BindView(R.id.tvHeader)
    BaseTextView tvHeader;
    @BindView(R.id.rvLinks)
    RecyclerView rvLinks;
    @BindView(R.id.rlError)
    BaseLinearLayout rlError;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;


    void setMagLinks(ArrayList<MagLink> magLinks) {
        this.magLinks = magLinks;
    }

    void setTitle(String title) {
        this.title = title;
    }

    MagLinksDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.mag_links_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(true);
        if (getWindow() != null) {
            Window window = getWindow();
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int maxHeight = getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_show_info);
                int cellHeight = getContext().getResources().getDimensionPixelSize(R.dimen.defult_height_dilog_info);
                int heightDialog = (cellHeight) * (3 + magLinks.size());
                if (heightDialog > maxHeight) {
                    heightDialog = maxHeight;
                } else if (heightDialog <= (cellHeight * 4)) {
                    heightDialog = cellHeight * 4;
                }
                window.setLayout(width, heightDialog);
                window.setGravity(Gravity.CENTER);
            }

        }

        btnCancel.setText(getContext().getString(R.string.cancel));
        tvHeader.setText(title);
        setAdapter();

    }


    private void setAdapter() {
        MagLinksAdapter adapter = new MagLinksAdapter(magLinks,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvLinks.setLayoutManager(layoutManager);
        rvLinks.setAdapter(adapter);

    }


    @OnClick({R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }


    @Override
    public void onItemClick(int position) {
        if (!TextUtils.isEmpty(magLinks.get(position).getHref())) {
            Intent intent = new Intent(getContext(), MagPostContentWebViewActivity.class);
            intent.putExtra("link", magLinks.get(position).getHref());
            getContext().startActivity(intent);
            dismiss();
        }
    }
}
