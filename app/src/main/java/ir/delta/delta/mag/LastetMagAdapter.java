package ir.delta.delta.mag;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class LastetMagAdapter extends RecyclerView.Adapter<LastetMagAdapter.ViewHolder> {

    private final ArrayList<MagPost> list;
    private final int termId;
    private final onItemClick listener;

    LastetMagAdapter(ArrayList<MagPost> list, int termId, onItemClick listener) {
        this.list = list;
        this.termId = termId;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mag, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        MagPost object = list.get(position);
        holder.tvTitle.setText(object.getTitle());
//        holder.tvDate.setText(object.getDate());
        holder.tvAuthor.setText(res.getString(R.string.author) + " " + object.getAuthor());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
        Date date = null;
        try {
            date = format.parse(object.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd;
            if (datea.getShYear() >= 1400) {
                pd = new PersianDateFormat("j F Y");
            } else {
                pd = new PersianDateFormat("j F y");
            }
            holder.tvDate.setText(pd.format(datea) + "");
        }

        Glide.with(holder.itemView.getContext()).load(object.getImage()).placeholder(R.drawable.placeholder).centerCrop().into(holder.imageMag);
        holder.bind(position, termId, list, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        BaseImageView imageMag;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvAuthor)
        BaseTextView tvAuthor;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, int termId, ArrayList<MagPost> list, final onItemClick listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, termId, list));
        }

    }

    public interface onItemClick {
        void onItemClick(int position, int termId, ArrayList<MagPost> list);
    }
}
