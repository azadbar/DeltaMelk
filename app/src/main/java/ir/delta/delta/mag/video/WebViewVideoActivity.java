package ir.delta.delta.mag.video;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.mag.video.hauler.HaulerView;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.OnSwipeTouchListener;
import ir.delta.delta.util.PreferencesData;

public class WebViewVideoActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.imageViewExit)
    ImageView imageViewExit;
    @BindView(R.id.haulerView)
    HaulerView haulerView;
    @BindView(R.id.title)
    BaseTextView title;
    @BindView(R.id.rlTitle)
    LinearLayout rlTitle;
    @BindView(R.id.root)
    ConstraintLayout root;
    @BindView(R.id.imageShare)
    ImageView imageShare;
    private String videoUri;
    private String titleString;
    private boolean isShowView = true;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_web_view_video);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            videoUri = bundle.getString("videoUri");
            titleString = bundle.getString("title");
            link = bundle.getString("link");
        }
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });

        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.loadData(videoUri, "text/html", null);//TODO handle webView

        title.setText(titleString);
        haulerView.setOnDragDismissedListener(dragDirection -> finish());

        root.setOnTouchListener(new OnSwipeTouchListener(webview.getContext()) {

            @Override
            public void onClick() {
                super.onClick();
                if (isShowView) {
                    rlTitle.setVisibility(View.GONE);
                } else {
                    rlTitle.setVisibility(View.VISIBLE);
                }
                isShowView = !isShowView;

                // your on click here
            }

            @Override
            public void onDoubleClick() {
                super.onDoubleClick();
                // your on onDoubleClick here
            }

            @Override
            public void onLongClick() {
                super.onLongClick();
                // your on onLongClick here
            }

            @Override
            public void onSwipeUp() {
                super.onSwipeUp();
                // your swipe up here
            }

            @Override
            public void onSwipeDown() {
                super.onSwipeDown();
                // your swipe down here.
            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                // your swipe left here.
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                // your swipe right here.
            }
        });

        imageShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.sharePostLink(WebViewVideoActivity.this,0 ,link, titleString, "");
            }
        });
    }

    @OnClick(R.id.imageViewExit)
    public void onViewClicked() {
        finish();
    }


}
