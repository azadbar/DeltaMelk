package ir.delta.delta.mag;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomViewPager;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.mag.main_page.OnRefreshMainPageList;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.MorePostsService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.service.ResponseModel.mag.MagPostResponse;
import ir.delta.delta.util.Constants;

public class PostContentActivity extends BaseActivity {


    PostPagerAdapter pagerAdapter;
    CustomViewPager viewPager;
    @BindView(R.id.rlNext)
    BaseRelativeLayout rlNext;
    @BindView(R.id.rlPrevious)
    BaseRelativeLayout rlPrevious;
    @BindView(R.id.imgIncrease)
    BaseImageView imgIncrease;
    @BindView(R.id.imgDecrease)
    BaseImageView imgDecrease;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.llBtn)
    BaseLinearLayout llBtn;
    @BindView(R.id.tvNext)
    BaseTextView tvNext;
    @BindView(R.id.imgRight)
    BaseImageView imgRight;
    @BindView(R.id.tvPrevious)
    BaseTextView tvPrevious;
    @BindView(R.id.imgPrevious)
    BaseImageView imgPrevious;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private MagFontEnum fontSize = MagFontEnum.NORMAL;
    public static ArrayList<MagPost> magPosts;
    private int selectedIndex;

    private boolean inLoading = false;
    private boolean endOfList = false;

    private final int seed = 30;
    private int termId;
    public static setBackgroundReadItem listener;
    private FirebaseAnalytics mFirebaseAnalytics;
    public static OnRefreshMainPageList refreshMainPageListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mag_post_content_activity);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int flags = root.getSystemUiVisibility();
            flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            root.setSystemUiVisibility(flags);
            getWindow().setStatusBarColor(Color.WHITE);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
//            magPosts = bundle.getParcelableArrayList("magPosts");
            termId = Integer.parseInt(bundle.getString("termId"));
            selectedIndex = bundle.getInt("selectedIndex", 0);
//            String termId = magPosts.get(selectedIndex).getTermId();
        }
        viewPager = findViewById(R.id.pager);

        pagerAdapter = new PostPagerAdapter(getSupportFragmentManager(), magPosts);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnTouchListener((v, event) -> true);
        viewPager.setOffscreenPageLimit(0);
        viewPager.setCurrentItem(selectedIndex);
        pagerAdapter.setRefreshList(refreshMainPageListener);

        setSelectedIndex();
    }


    private void setSelectedIndex() {
        if (selectedIndex == 0) {
            tvPrevious.setTextColor(getResources().getColor(R.color.divider));
            imgPrevious.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
            rlPrevious.setClickable(false);
        } else {
            tvPrevious.setTextColor(getResources().getColor(R.color.primaryTextColor));
            imgPrevious.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
            rlPrevious.setClickable(true);
        }

        if (selectedIndex == magPosts.size() - 1 && endOfList) {
            tvNext.setTextColor(getResources().getColor(R.color.divider));
            imgRight.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
            rlNext.setClickable(false);
        } else {
            tvNext.setTextColor(getResources().getColor(R.color.primaryTextColor));
            imgRight.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
            rlNext.setClickable(true);

        }
    }


    private void setFontSize() {
        switch (fontSize) {
            case SMALL:
                imgDecrease.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
                imgDecrease.setClickable(false);
                imgIncrease.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
                imgIncrease.setClickable(true);
                break;
            case NORMAL:
            case LARGE:
                imgDecrease.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
                imgDecrease.setClickable(true);
                imgIncrease.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
                imgIncrease.setClickable(true);
                break;
            case XLARGE:
                imgDecrease.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
                imgDecrease.setClickable(true);
                imgIncrease.setColorFilter(getResources().getColor(R.color.divider), PorterDuff.Mode.SRC_ATOP);
                imgIncrease.setClickable(false);
                break;
        }
        pagerAdapter.setFragment(fontSize);

    }


    private void getMorePostList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            showLoading();
            getMorePostRequest();
        }
    }

    private void getMorePostRequest() {
        llBtn.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        MorePostsService.getInstance().getMorePost(getResources(), termId, magPosts != null ? magPosts.size() : 0, seed, new ResponseListener<MagPostResponse>() {
            @Override
            public void onGetError(String error) {
                if (viewPager != null) {
                    llBtn.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    Toast.makeText(PostContentActivity.this, error, Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(PostContentActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(tvNext.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(MagPostResponse response) {
                if (viewPager != null) {
                    llBtn.setVisibility(View.VISIBLE);
                    viewPager.setVisibility(View.VISIBLE);
                    setList(response);
                }
            }
        });
    }

    private void setList(MagPostResponse response) {
        if (loadingView == null) {
            return;
        }
        inLoading = false;
        stopLoading();
        if (response.isSuccessed() && response.getList() != null) {
            if (response.getList().size() < seed) {
                endOfList = true;
            }
            magPosts.addAll(response.getList());
            pagerAdapter.notifyDataSetChanged();
            if (selectedIndex < magPosts.size() - 1) {
                selectedIndex += 1;
                viewPager.setCurrentItem(selectedIndex);
            }
            setSelectedIndex();
        }
    }


    @OnClick({R.id.rlNext, R.id.rlPrevious, R.id.imgIncrease, R.id.imgDecrease})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlNext:
                if (magPosts != null && selectedIndex < magPosts.size() - 1) {
                    selectedIndex += 1;
                    if (listener != null)
                        listener.onItemNext(selectedIndex, magPosts);
                    viewPager.setCurrentItem(selectedIndex);
                    setSelectedIndex();
                } else {
                    if (listener != null)
                        listener.onItemNext(selectedIndex, magPosts);
                    getMorePostList();
                    //TODO check end of list;
                }
                if (MagPostContentAdapter.stopVideoScrollListener != null)
                    MagPostContentAdapter.stopVideoScrollListener.releaseVideo();
                break;
            case R.id.rlPrevious:
                if (selectedIndex > 0) {
                    selectedIndex -= 1;
                    viewPager.setCurrentItem(selectedIndex);
                    if (listener != null)
                        listener.onItemNext(selectedIndex, magPosts);
                    if (MagPostContentAdapter.stopVideoScrollListener != null)
                        MagPostContentAdapter.stopVideoScrollListener.releaseVideo();
                    setSelectedIndex();
                }
                break;
            case R.id.imgIncrease:
                fontSize = fontSize.increase();
                setFontSize();
                break;
            case R.id.imgDecrease:
                fontSize = fontSize.decrease();
                setFontSize();
                break;
        }
    }


    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        loadingView.showLoading(true);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
    }


    public interface setBackgroundReadItem {
        void onItemNext(int position, ArrayList<MagPost> magPosts);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        magPosts = null;
    }
}
