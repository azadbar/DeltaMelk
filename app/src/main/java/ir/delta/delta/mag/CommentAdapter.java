package ir.delta.delta.mag;

import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.deltanet.DeltaNetEstate;
import ir.delta.delta.service.ResponseModel.mag.Comment;
import ir.delta.delta.util.Constants;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final ArrayList<Comment> list;
    private final boolean commentStatus;
    private final onItemClick listener;
    private int margin;


//    public void setSelectedIndex(int selectedIndex) {
//        this.selectedIndex = selectedIndex;
//        notifyDataSetChanged();
//    }

    public CommentAdapter(ArrayList<Comment> list, boolean commentStatus, onItemClick listener) {
        this.list = list;
        this.commentStatus = commentStatus;
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (!list.get(position).getParentId().equals("0")) {
            return 2;
        } else {
            return 1;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 2:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_comment, parent, false);
                return new SubViewHolder(itemView1);
            default:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
                return new ViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {

            case 2:

                SubViewHolder viewHolder0 = (SubViewHolder) holder;
                Resources res = viewHolder0.tvName.getResources();
                Comment comments = list.get(position);
                viewHolder0.tvName.setText(comments.getNameAuthor(viewHolder0.tvComments.getResources()));
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
                Date date = null;
                try {
                    date = format.parse(comments.getCommentDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date != null) {
                    PersianDate datea = new PersianDate(date);
                    PersianDateFormat pd;
                    if (datea.getShYear() >= 1400) {
                        pd = new PersianDateFormat("H:i" + " - " + "l j F");
                    } else {
                        pd = new PersianDateFormat("H:i" + " - " + "l j F");
                    }
                    viewHolder0.tvDate.setText(pd.format(datea));
                } else {
                    viewHolder0.tvDate.setText(null);
                }

                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) viewHolder0.card.getLayoutParams();
                marginLayoutParams.setMargins(comments.getLevel() * res.getDimensionPixelOffset(R.dimen.padding_comment),
                        res.getDimensionPixelOffset(R.dimen.margin_1), comments.getLevel() * res.getDimensionPixelOffset(R.dimen.padding_comment), 0);
                viewHolder0.card.setLayoutParams(marginLayoutParams);

                if (!TextUtils.isEmpty(comments.getContent().trim())) {
                    if (comments.getContent().length() >= 150) {
                        String description = "<font color='#000'>" + comments.getContent().substring(0, 150).replace("</br>", "\n").trim() + " </font> " +
                                "... "+ "<font color='#E30427' <strong> "  + res.getString(R.string.more) + "</strong></font>";
                        viewHolder0.tvComments.setText(createHtmlText(description));
                    } else {
                        viewHolder0.tvComments.setText(comments.getContent().replace("</br>", "\n").trim());
                    }
                } else {
                    viewHolder0.tvComments.setText(null);
                }

                if (comments.getParentId().equals("0")) {
                    viewHolder0.btnReplay.setVisibility(View.VISIBLE);
                } else {
                    viewHolder0.btnReplay.setVisibility(View.GONE);
                }

                if (comments.getScore() > 0) {
                    viewHolder0.ratingBar.setRating(comments.getScore());
                    viewHolder0.ratingBar.setVisibility(View.VISIBLE);
                } else {
                    viewHolder0.ratingBar.setVisibility(View.GONE);
                }

                if (commentStatus) {
                    viewHolder0.btnPostComment.setVisibility(View.VISIBLE);
                } else {
                    viewHolder0.btnPostComment.setVisibility(View.GONE);
                }
                viewHolder0.tvComments.setOnClickListener(view -> clickDescription(res, viewHolder0.tvComments, comments));
                break;
            default:

                ViewHolder holder1 = (ViewHolder) holder;
                Resources res1 = holder1.tvName.getResources();
                Comment comments1 = list.get(position);

                holder1.tvName.setText(comments1.getNameAuthor(holder1.tvComments.getResources()));
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
                Date date1 = null;
                try {
                    date1 = format1.parse(comments1.getCommentDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date1 != null) {
                    PersianDate datea = new PersianDate(date1);
                    PersianDateFormat pd;
                    if (datea.getShYear() >= 1400) {
                        pd = new PersianDateFormat("H:i" + " - " + "l j F");
                    } else {
                        pd = new PersianDateFormat("H:i" + " - " + "l j F");
                    }
                    holder1.tvDate.setText(pd.format(datea));
                } else {
                    holder1.tvDate.setText(null);
                }

                if (!TextUtils.isEmpty(comments1.getContent().trim())) {
                    if (comments1.getContent().length() >= 150) {
                        String description = "<font color='#000'>" + comments1.getContent().substring(0, 150).replace("</br>", "\n").trim() + " </font> " +
                                "... " +"<font color='#E30427' <strong> " +  res1.getString(R.string.more) + "</strong></font>";
                        holder1.tvComments.setText(createHtmlText(description));
                    } else {
                        holder1.tvComments.setText(comments1.getContent().replace("</br>", "\n").trim());
                    }
                } else {
                    holder1.tvComments.setText(null);
                }
                holder1.view.setVisibility(comments1.isHasChild() ? View.GONE : View.VISIBLE);
//        holder.root.setBackgroundColor(comments1.getIndex() % 2 == 0 ? res1.getColor(R.color.white) : res1.getColor(R.color.oddWW));

//        if (comments1.getLevel() <= 4) {
//            marginLayoutParams.setMargins(0, 0, comments1.getLevel() * res1.getDimensionPixelOffset(R.dimen.padding_comment), 0);
//            margin = comments1.getLevel() * res1.getDimensionPixelOffset(R.dimen.padding_comment);
//        } else {
//            marginLayoutParams.setMargins(0, 0, margin, 0);
//        }

//        holder.ratingBar.setRating(3);

                if (comments1.getParentId().equals("0")) {
                    holder1.btnReplay.setVisibility(View.VISIBLE);
                } else {
                    holder1.btnReplay.setVisibility(View.GONE);
                }

//        holder.ratingBar.setRating(3);
                if (comments1.getScore() > 0) {
                    holder1.ratingBar.setRating(comments1.getScore());
                    holder1.ratingBar.setVisibility(View.VISIBLE);
                } else {
                    holder1.ratingBar.setVisibility(View.GONE);
                }

                if (commentStatus) {
                    holder1.btnPostComment.setVisibility(View.VISIBLE);
                } else {
                    holder1.btnPostComment.setVisibility(View.GONE);
                }
                holder1.bind(position, comments1, listener);
                holder1.tvComments.setOnClickListener(view -> clickDescription(res1, holder1.tvComments, comments1));
                break;
        }
    }

    private void clickDescription(Resources res, BaseTextView tvDescription, Comment comment) {

        if (comment.getContent().length() >= 150) {
            String description;
            if (comment.isExpanded()) {
                description = "<font color='#000'>" + comment.getContent().replace("</br>", "\n").substring(0, 150) + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
            } else {
                description = "<font color='#000'>" + comment.getContent().replace("</br>", "\n") + " </font> " +
                        "<font color='#E30427' <strong> " + res.getString(R.string.less) + "</strong></font>";
            }
            comment.setExpanded(!comment.isExpanded());
            tvDescription.setText(createHtmlText(description));
        }
    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        BaseTextView tvName;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.tvComments)
        BaseTextView tvComments;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.rl)
        BaseRelativeLayout rl;
        @BindView(R.id.btnPostComment)
        BaseTextView btnPostComment;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.btnReplay)
        BaseRelativeLayout btnReplay;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Comment comments, final onItemClick listener) {
            btnReplay.setOnClickListener(v -> listener.onItemClick(position, comments));
        }
    }

    static class SubViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        BaseTextView tvName;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.tvComments)
        BaseTextView tvComments;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.rl)
        BaseRelativeLayout rl;
        @BindView(R.id.btnPostComment)
        BaseTextView btnPostComment;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.btnReplay)
        BaseRelativeLayout btnReplay;
        @BindView(R.id.card)
        CardView card;

        public SubViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }

    public interface onItemClick {
        void onItemClick(int position, Comment comments);
    }
}
