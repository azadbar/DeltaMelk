package ir.delta.delta.mag.main_page;

import android.app.Activity;

public interface OnRefreshMainPageList {
    void onRefresh();
}
