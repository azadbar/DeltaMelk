package ir.delta.delta.mag.postContentCell;

import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;

public class AuthorViewHolder extends RecyclerView.ViewHolder implements onUpdateFavoritePostInView {


    @BindView(R.id.author)
    BaseTextView author;
    @BindView(R.id.source)
    BaseTextView source;
    @BindView(R.id.parent)
    BaseLinearLayout parent;
    @BindView(R.id.btnFavorite)
    BaseRelativeLayout btnFavorite;
    @BindView(R.id.btnShare)
    BaseRelativeLayout btnShare;
    @BindView(R.id.imgFavorite)
    BaseImageView imgFavorite;

    private ArrayList<Integer> favoriteList;
    private ir.delta.delta.mag.postContentCell.onUpdateFavoritePost onUpdateFavoritePost;

    public AuthorViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setTextContent(int id, PostDetailResponse response,
                               onUpdateFavoritePost onUpdateFavoritePost) {
        this.onUpdateFavoritePost = onUpdateFavoritePost;
        author.setVisibility(View.VISIBLE);
//        parent.setBackgroundColor(row.getBackColor(author.getResources().getColor(R.color.white)));
        author.setMovementMethod(LinkMovementMethod.getInstance());
        SpannableStringBuilder srcauther = new SpannableStringBuilder();
//        author.setTextSize(magFont.getSize(author.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.h1 : row.getHtmlTag()));
        srcauther.append(author.getContext().getResources().getString(R.string.author)).append(":");
        int start1 = srcauther.length();
        srcauther.append(response.getAuthor() !=null ? response.getAuthor() : "");
        int end1 = srcauther.length();
        srcauther.setSpan(new ForegroundColorSpan(source.getContext().getResources().getColor(R.color.redColor)), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        srcauther.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        srcauther.setSpan(new BaseFragment.RoundedBackgroundSpan(source.getContext()), start1, end1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        srcauther.setSpan(new CustomClick(magPost.getSource_link()), start1, end1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        author.setText(srcauther);
//        author.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));


        if (response.getSourceName() != null && !response.getSourceName().isEmpty()) {
            source.setVisibility(View.VISIBLE);
            source.setMovementMethod(LinkMovementMethod.getInstance());
//            source.setTextSize(magFont.getSize(source.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.h1 : row.getHtmlTag()));
            SpannableStringBuilder src = new SpannableStringBuilder();
            src.append(source.getContext().getResources().getString(R.string.source_mag)).append(":");
            int start = src.length();
            src.append("").append(response !=null ? response.getSourceName().trim() : "");
            int end = src.length();
            src.setSpan(new ForegroundColorSpan(source.getContext().getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            src.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            src.setSpan(new BaseFragment.RoundedBackgroundSpan(source.getContext()), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//            src.setSpan(new CustomClick(magPost.getSource_link()), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            source.setText(src);
//            source.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));
        } else {
            source.setVisibility(View.GONE);
        }

        btnShare.setOnClickListener(v -> {
            Constants.sharePostLink(btnShare.getContext(),id ,response.getLink(), response.getTitle(), response.getTermName());
        });

        MagPostContentAdapter.onUpdateFavoritePostInViewListener = this;
        //read post for change background
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(parent.getContext());
        if (favoriteList.contains(id)) {
            imgFavorite.setImageResource(R.drawable.ic_bookmark);
            imgFavorite.setColorFilter(imgFavorite.getContext().getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        } else {
            imgFavorite.setImageResource(R.drawable.ic_bookmark_border);
            imgFavorite.setColorFilter(imgFavorite.getContext().getResources().getColor(R.color.gray_300), PorterDuff.Mode.SRC_ATOP);
        }

        btnFavorite.setOnClickListener(v -> favoriteItem(response, id));
    }

    private void favoriteItem(PostDetailResponse response, int id) {

        int index = favoriteList.indexOf(id);
        if (index >= 0) {
            favoriteList.remove(index);
            TransactionFavoriteMag.getInstance().delete(imgFavorite.getContext(), id);
            imgFavorite.setImageResource(R.drawable.ic_bookmark_border);
            Constants.setToastFont(parent.getContext(), parent.getContext().getString(R.string.delete_favorite_mag_text), 16);
            onUpdateFavoritePost.onUpdateFavorite(false, id);
        } else {
            favoriteList.add(id);
            MagPost magPost = new MagPost();
            magPost.setId(id);
            magPost.setTitle(response.getTitle());
            magPost.setAuthor(response.getAuthor());
            magPost.setTerm_name(response.getTermName());
            magPost.setTermId(response.getTermId());
            magPost.setCommentStatus(response.getCommentStatus());
            magPost.setViewCount(response.getViewCount());
            magPost.setDate(response.getDate());
            TransactionFavoriteMag.getInstance().saveMagPost(imgFavorite.getContext(), magPost);
            imgFavorite.setImageResource(R.drawable.ic_bookmark);
            Constants.setToastFont(parent.getContext(), parent.getContext().getString(R.string.add_favorite_mag_text), 16);
            onUpdateFavoritePost.onUpdateFavorite(true, id);
        }
//        if (refreshMainPageListener != null)
//            refreshMainPageListener.onRefresh();
    }

    @Override
    public void onUpdateFavoriteInView(boolean isFavorite, int id) {
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(parent.getContext());
        if (isFavorite ) {
            imgFavorite.setImageResource(R.drawable.ic_bookmark);
        } else {
            imgFavorite.setImageResource(R.drawable.ic_bookmark_border);
        }
    }
}
