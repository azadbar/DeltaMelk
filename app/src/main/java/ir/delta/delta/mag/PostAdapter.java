package ir.delta.delta.mag;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.mag.main_page.OnRefreshMainPageList;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private ArrayList<MagPost> list;
    private final ArrayList<Integer> readPost;
    private final onItemClick listener;
    private final ArrayList<Integer> favoriteList;
    private boolean isFavorite = false;
    private OnRefreshMainPageList refreshMainPageListener;

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


    public PostAdapter(ArrayList<MagPost> list, ArrayList<Integer> readPost, onItemClick listener, ArrayList<Integer> favoriteList) {
        this.list = list;
        this.readPost = readPost;
        this.listener = listener;
        this.favoriteList = favoriteList;
    }

    public void updateReceiptsList(ArrayList<MagPost> newlist) {
        list.clear();
        list.addAll(newlist);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mag, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        MagPost object = list.get(position);
        holder.tvTitle.setText(object.getTitle());
//        holder.tvDate.setText(object.getDate());
        holder.tvAuthor.setText(object.getAuthor());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
        Date date = null;
        try {
            date = format.parse(object.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd;
            if (datea.getShYear() >= 1400) {
                pd = new PersianDateFormat("H:i" + " - " + "l j F Y");
            } else {
                pd = new PersianDateFormat("H:i" + " - " + "l j F y");
            }
            holder.tvDate.setText(pd.format(datea));
        } else {
            holder.tvDate.setText(null);
        }

        if (favoriteList.contains(object.getId())) {
            holder.icBookMark.setImageResource(R.drawable.ic_bookmark);
            holder.icBookMark.setColorFilter(holder.icBookMark.getContext().getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_ATOP);
        } else {
            holder.icBookMark.setImageResource(R.drawable.ic_bookmark_border);
            holder.icBookMark.setColorFilter(holder.icBookMark.getContext().getResources().getColor(R.color.gray_300), PorterDuff.Mode.SRC_ATOP);
        }

        if ("1609".equals(object.getTermId()) || "28".equals(object.getTermId())) {
            holder.videoIcon.setVisibility(View.VISIBLE);
        } else {
            holder.videoIcon.setVisibility(View.GONE);
        }
        Glide.with(holder.itemView.getContext()).load(object.getImage()).placeholder(R.drawable.placeholder).centerCrop().into(holder.imageMag);
        holder.itemView.setOnClickListener(view -> changeBackground(holder.root.getContext(), position, list, listener));
        holder.icBookMark.setOnClickListener(view -> favoriteItem(holder.icBookMark, object));
        holder.icShare.setOnClickListener(view -> listener.onItemShare(object));
    }

    private void favoriteItem(BaseImageView imageView, MagPost magPost) {

        int index = favoriteList.indexOf(magPost.getId());
        if (index >= 0) {
            favoriteList.remove(index);
            TransactionFavoriteMag.getInstance().delete(imageView.getContext(), magPost.getId());
            imageView.setImageResource(R.drawable.ic_bookmark_border);
            imageView.setColorFilter(imageView.getContext().getResources().getColor(R.color.gray_300), PorterDuff.Mode.SRC_ATOP);
            Constants.setToastFont(imageView.getContext(), imageView.getContext().getString(R.string.delete_favorite_mag_text), 16);
        } else {
            favoriteList.add(magPost.getId());
            TransactionFavoriteMag.getInstance().saveMagPost(imageView.getContext(), magPost);
            imageView.setImageResource(R.drawable.ic_bookmark);
            imageView.setColorFilter(imageView.getContext().getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_ATOP);
            Constants.setToastFont(imageView.getContext(), imageView.getContext().getString(R.string.add_favorite_mag_text), 16);
        }
        if (isFavorite) {
            updateReceiptsList(TransactionFavoriteMag.getInstance().getFavoriteMag(imageView.getContext()));
            if (refreshMainPageListener != null)
                refreshMainPageListener.onRefresh();
        }

    }

    private void changeBackground(Context context, int position, ArrayList<MagPost> list, onItemClick listener) {
        if (list != null && position < list.size()) {
            readPost.add(list.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(context, list.get(position).getId());
            listener.onItemClick(position, list);
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<MagPost> posts) {
        list = posts;
        notifyDataSetChanged();
    }

    public void setRefreshMainPageListener(OnRefreshMainPageList refreshMainPageListener) {
        this.refreshMainPageListener = refreshMainPageListener;
    }

    public void notifayList(ArrayList<Integer> favoriteList) {
        this.favoriteList.clear();
        this.favoriteList.addAll(favoriteList);
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        BaseImageView imageMag;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvAuthor)
        BaseTextView tvAuthor;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.ic_bookMark)
        BaseImageView icBookMark;
        @BindView(R.id.ic_share)
        BaseImageView icShare;
        @BindView(R.id.videoIcon)
        BaseImageView videoIcon;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }


    public interface onItemClick {
        void onItemClick(int position, ArrayList<MagPost> list);


        void onItemShare(MagPost object);
    }
}
