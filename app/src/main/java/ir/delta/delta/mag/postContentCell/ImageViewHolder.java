package ir.delta.delta.mag.postContentCell;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.util.Constants;

public class ImageViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.image)
    BaseImageView image;

    public ImageViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setContent(Object object, int recycleWidth) {
        HtmlTag htmlTag = (HtmlTag) object;
        image.setVisibility(View.VISIBLE);
        image.getLayoutParams().height = (int) (recycleWidth * Constants.getHeightFactr(((HtmlTag) object).getWidth() + "", ((HtmlTag) object).getHeight() + ""));
        Glide.with(image.getContext())
                .load(htmlTag.getSrc())
                .thumbnail(0.5f)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .centerCrop()).into(image);
       /* ArrayList<String> images = new ArrayList<>();
        images.add(row.getSrc());
        image.setOnClickListener(view1 -> {
            Intent intent = new Intent(view1.getContext(), PreviewActivity.class);
            intent.putStringArrayListExtra("images", images);
            view1.getContext().startActivity(intent);
        });*/


    }


}
