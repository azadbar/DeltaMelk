package ir.delta.delta.mag.postContentCell;

import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.activity.MainActivity;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.MyJustifiedTextView;
import ir.delta.delta.mag.SinglePostContentActivity;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.mag.MagPostRow;

import static android.text.Html.FROM_HTML_MODE_COMPACT;
import static android.text.Html.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH;

public class TextViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text)
    MyJustifiedTextView text;


    public TextViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setTextContent(Object object) {
        HtmlTag htmlTag = (HtmlTag) object;
        text.setVisibility(View.VISIBLE);
        text.setMovementMethod(LinkMovementMethod.getInstance());
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            text.setText(Html.fromHtml(htmlTag.getText(), Html.FROM_HTML_MODE_COMPACT));
//        } else {
//            text.setText(Html.fromHtml(htmlTag.getText()));
//        }
//        text.setText(htmlTag.getText());
/*        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(parent.getResources().getDimensionPixelSize(R.dimen.margin_4),
                parent.getResources().getDimensionPixelSize(R.dimen.margin_2),
                parent.getResources().getDimensionPixelSize(R.dimen.margin_4),
                parent.getResources().getDimensionPixelSize(R.dimen.margin_2));
        text.setLayoutParams(params);*/

        CharSequence sequence = processHtmlString(htmlTag.getText());
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);

    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                String[] split = span.getURL().split("/");
                String id = null;
                for (int i = 0; i < split.length; i++) {
                    if (Pattern.matches("[0-9]+", split[i]) && split[i].length() > 3) {
                        id = split[i];
                        if (!TextUtils.isEmpty(id)) {
                            Intent intent = new Intent(text.getContext(), SinglePostContentActivity.class);
                            intent.putExtra("id", id);
                            text.getContext().startActivity(intent);
                        }
                    }else if (split.length < 4) {
                        Intent intent = new Intent(text.getContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        text.getContext().startActivity(intent);
                    }
                }

            }
        };
        strBuilder.setSpan(clickable, start, end, flags);
        strBuilder.removeSpan(span);
    }

    private CharSequence noTrailingwhiteLines(CharSequence text) {

        while (text.charAt(text.length() - 1) == '\n') {
            text = text.subSequence(0, text.length() - 1);
        }
        return text;
    }
    public  Spanned processHtmlString(String htmlString){

        // remove leading <br/>
        while (htmlString.startsWith("<br/>")){

            htmlString = htmlString.replaceFirst("<br/>", "");
        }

        // remove trailing <br/>
        while (htmlString.endsWith("<br/>")){

            htmlString =  htmlString.replaceAll("<br/>$", "");
        }

        // reduce multiple \n in the processed HTML string
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

            return Html.fromHtml(htmlString,  FROM_HTML_MODE_COMPACT);
        }else{

            return Html.fromHtml(htmlString);
        }
    }
}
