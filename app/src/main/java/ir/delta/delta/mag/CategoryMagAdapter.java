package ir.delta.delta.mag;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.CategoryMag;
import ir.delta.delta.service.ResponseModel.FailureBugReport;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CategoryMagAdapter extends RecyclerView.Adapter<CategoryMagAdapter.ViewHolder> {


    private final ArrayList<CategoryMag> list;
    private final onItemClick listener;
    private FailureBugReport failureBugReport;

    CategoryMagAdapter(ArrayList<CategoryMag> list, onItemClick listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_mag, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        CategoryMag object = list.get(position);

        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.row)
        BaseRelativeLayout row;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final onItemClick listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    public interface onItemClick {
        void onItemClick(int position);
    }
}
