package ir.delta.delta.mag.postContentCell;

public interface onUpdateFavoritePost {
    void onUpdateFavorite(boolean isFavorite, int id);
}
