package ir.delta.delta.mag.postContentCell;

import android.content.Intent;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.mag.SinglePostContentActivity;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class RelatedPostLinkViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.parent)
    BaseRelativeLayout parent;
    @BindView(R.id.text)
    BaseTextView text;

    public RelatedPostLinkViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setContent(Object object) {
        MagPost magPost = (MagPost) object;
        text.setVisibility(View.VISIBLE);
        text.setText(magPost.getTitle());
//        text.setTextSize(magFont.getSize(text.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.p : row.getHtmlTag()));
//        text.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));


        text.setOnClickListener(v -> {
            Intent intent = new Intent(parent.getContext(), SinglePostContentActivity.class);
            intent.putExtra("id", String.valueOf(magPost.getId()));
            text.getContext().startActivity(intent);
        });
    }



}
