package ir.delta.delta.mag.video.hauler

import ir.delta.delta.mag.video.hauler.DragDirection

interface OnDragDismissedListener {
    fun onDismissed(dragDirection: DragDirection)
}
