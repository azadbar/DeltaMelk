package ir.delta.delta.mag.video;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.mag.SubMenuAdapter;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestCallback;
import ir.delta.delta.service.RequestListener;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.metrix.sdk.Metrix;
import retrofit2.Response;


public class VideoFragment extends BaseFragment implements VideoPostAdapter.onItemClick,
        PostContentActivity.setBackgroundReadItem,
        MagPostContentAdapter.onItemClickListener, SubMenuAdapter.OnItemClickListener {
    @BindView(R.id.main_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.rvSubMenu)
    RecyclerView rvSubMenu;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.btnNewSearch)
    BaseTextView btnNewSearch;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    private int position;
    private VideoPostAdapter postAdapter;
    private String termId;
    private String termName;
    private ArrayList<Integer> readPost;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;
    private final int seed = 30;
    private final ArrayList<MagPost> list = new ArrayList<>();
    private boolean isSwipeRefresh;
    private ContainerFragment.OnGetRecentList listener;
    private LinearLayoutManager layoutManager;
    private ContainerFragment.OnScroolListener scroolListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean isTelegram;
    private boolean lockClickForItem;
    private SubMenuAdapter subAdapter;
    private MagMenuResponse beforMagmenu;


    public static VideoFragment newInstance(int position, String termId, String termName,
                                            ContainerFragment.OnGetRecentList listener,
                                            ContainerFragment.OnScroolListener scrollListener) {
        VideoFragment pageFragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putSerializable("position", position);
        args.putString("termId", termId);
        args.putString("termName", termName);
        pageFragment.setListener(listener);
        pageFragment.setScrollListener(scrollListener);
        pageFragment.setArguments(args);

        return pageFragment;
    }

    private void setScrollListener(ContainerFragment.OnScroolListener scrollListener) {
        this.scroolListener = scrollListener;
    }

    private void setListener(ContainerFragment.OnGetRecentList listener) {
        this.listener = listener;
    }

    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt("position");
            termId = bundle.getString("termId");
            termName = bundle.getString("termName");
        }

       /* if (response !=null && response.getMenus() != null && response.getMenus().get(position).getChildren() != null &&
                response.getMenus().get(position).getChildren().size() > 0) {
            termId = response.getMenus().get(position).getChildren().get(0).getTermId();
            termName = response.getMenus().get(position).getChildren().get(0).getTitle();
            rvSubMenu.setVisibility(View.VISIBLE);
            subAdapter = new SubMenuAdapter(response.getMenus(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
            rvSubMenu.setNestedScrollingEnabled(true);
            rvSubMenu.setLayoutManager(layoutManager);
            rvSubMenu.setAdapter(subAdapter);
        } else {
            rvSubMenu.setVisibility(View.GONE);
        }*/

        //get main request
        getPostsRequest();


        //read post for change background
        readPost = TransactionReadPost.getInstance().getReadPostsId(getContext());

        empty.setVisibility(View.GONE);
        if (getActivity() != null) {
            layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getPostsRequest();
                            }
                    }
                }
            });
        }


        //when response not found and try again
        loadingView.setOnClickListener(v -> getPostsRequest());

        if (isTelegram) {
            Intent intent = new Intent(getActivity(), PostContentActivity.class);
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable("magPosts", list);
//            bundle1.putString("termId", telegramMenu.getTermId());
            bundle1.putInt("selectedIndex", position);
            intent.putExtras(bundle1);
            startActivity(intent);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
//                    animateSwtich(1);
                    scroolListener.onScrool(2);

                } else {
                    if (!inLoading) {
//                        animateSwtich(0);
                        scroolListener.onScrool(2);
                    }
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        swipeRefresh.setColorSchemeColors(Color.RED);
        swipeRefresh.setOnRefreshListener(() -> {
            list.clear();
            if (postAdapter != null)
                postAdapter.notifyDataSetChanged();
            isSwipeRefresh = true;
            getPostsRequest();
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        lockClickForItem = false;
    }

    private void getPostsRequest() {
        Bundle params = new Bundle();
        params.putString("click_menu", termName);
        mFirebaseAnalytics.logEvent("Click_in_menu", params);
        //analytics for metrix
        Metrix.getInstance().newEvent("xdtyo");
        Map<String, String> attributes = new HashMap<>();
        attributes.put("xdtyo", termName);
        Metrix.getInstance().addUserAttributes(attributes);
        if (!inLoading) {
            inLoading = true;
            if (list.isEmpty()) {
                if (isSwipeRefresh) {
                    recyclerView.animate().alpha(0.0f).setStartDelay(700);
                } else {
                    showLoading();
                }
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
        }
        new RequestCallback<>(getActivity(), ApiClient.getClient_MAG().create(ReqInterface.class).getPosts(list.size(), seed, termId), new RequestListener<List<MagPost>>() {
            @Override
            public void onResponse(@NonNull Response<List<MagPost>> response) {
                try {
                    if (response.body() != null) {
                        //when swipe refresh
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.animate().alpha(1.0f);
                        isSwipeRefresh = false;
                        swipeRefresh.setRefreshing(false);
                        rlLoding.setVisibility(View.GONE);
                        stopLoading();
                        if (response.body().size() < offset) {
                            endOfList = true;
                        }
                        inLoading = false;
                        list.addAll(response.body());
                        setDataAdapter();

                    } else {
                        stopLoading();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int code, @NonNull JSONObject jsonObject) {
                stopLoading();
                loadingView.setVisibility(View.VISIBLE);
                onError(getResources().getString(R.string.communicationError));
            }
        });
    }


    private void setDataAdapter() {
        if (postAdapter == null) {
            postAdapter = new VideoPostAdapter(list, readPost, this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(postAdapter);
        } else {
            postAdapter.notifyDataSetChanged();
        }
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null) {
            rlLoding.setVisibility(View.GONE);
            if (list.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


    private void showLoading() {
        loadingProgress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void stopLoading() {
        loadingProgress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.animate().alpha(1.0f);
        loadingView.stopLoading();

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        if (!this.lockClickForItem) {
            String videoUri = "";
            final String regex = "\\*?mp4=\\\"([^\\\"]+)";
            final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            final Matcher matcher = pattern.matcher(list.get(position).getPost_content() != null ?
                    list.get(position).getPost_content() : "");
            while (matcher.find()) {
                for (int i = 1; i <= matcher.groupCount(); i++) {
                    videoUri = matcher.group(i);
                }
            }
             Intent intent = new Intent(getActivity(), ExoPlayerActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("magPost", list.get(position));
            bundle.putString("videoUri", videoUri);
            bundle.putString("titleString", list.get(position).getTitle());
            bundle.putString("link", Constants.URL_SITE + list.get(position).getId());
            bundle.putString("id", String.valueOf(list.get(position).getId()));
            intent.putExtras(bundle);
            startActivity(intent);
            this.lockClickForItem = true;

        }

    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext());
    }

    @Override
    public void onItemNext(int position, ArrayList<MagPost> magPosts) {
        if (magPosts != null && magPosts.size() > 0) {
            readPost.add(magPosts.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(getContext(), magPosts.get(position).getId());
            postAdapter.updateReceiptsList(magPosts);
        }
    }

    @Override
    public void onItemSubMenu(int position, MagMenuResponse magMenuResponse, ArrayList<MagMenuResponse> menus) {
        if (magMenuResponse != beforMagmenu) {
            if (magMenuResponse.getTermId().equals(menus.get(position).getTermId())) {
                termId = magMenuResponse.getTermId();
                termName = magMenuResponse.getTitle();
                list.clear();
                recyclerView.stopScroll();
                if (postAdapter != null)
                    postAdapter.notifyDataSetChanged();
                postAdapter = null;
                inLoading = false;
                endOfList = false;
                getPostsRequest();
                if (subAdapter != null)
                    subAdapter.setSubMenu(magMenuResponse);
                beforMagmenu = magMenuResponse;
            }
        }


    }


    @Override
    public void onItemClick(int position) {


    }

}
