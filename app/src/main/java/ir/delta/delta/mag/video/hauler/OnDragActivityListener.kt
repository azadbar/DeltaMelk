package ir.delta.delta.mag.video.hauler

interface OnDragActivityListener {
    fun onDrag(elasticOffsetPixels: Float, rawOffset: Float)
}
