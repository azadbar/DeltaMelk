package ir.delta.delta.mag.postContentCell;

import android.content.Intent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.activity.MainActivity;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.mag.CommentActivity;
import ir.delta.delta.mag.CommentAdapter;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.PostCommentDialog;
import ir.delta.delta.mag.telegram.OnGetMorePostTelegram;
import ir.delta.delta.mag.telegram.OnVisibleTelegramButton;
import ir.delta.delta.service.Request.PostCommentService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.PostCommentReq;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ResponseModel.mag.Comment;
import ir.delta.delta.service.ResponseModel.mag.CommentResponse;
import ir.delta.delta.util.PreferencesData;

public class ShowCommentsHolder extends RecyclerView.ViewHolder implements CommentAdapter.onItemClick, onUpdateRateResponse, OnVisibleTelegramButton {

    private String myScore;

    @BindView(R.id.countComment)
    BaseTextView countComment;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.rvCommentPost)
    RecyclerView rvCommentPost;
    @BindView(R.id.btnMoreComment)
    BaseLinearLayout btnMoreComment;
    @BindView(R.id.btnInsertComment)
    BaseTextView btnInsertComment;
    @BindView(R.id.cvInsertComment)
    CardView cvInsertComment;
    @BindView(R.id.txtInsert)
    BaseTextView txtInsert;
    @BindView(R.id.ratingBar)
    SimpleRatingBar ratingBar;
    @BindView(R.id.btnRegister)
    BaseTextView btnRegister;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rlRegister)
    BaseRelativeLayout rlRegister;
    @BindView(R.id.backToHome)
    BaseTextView backToHome;
    @BindView(R.id.commentSection)
    BaseRelativeLayout commentSection;
    @BindView(R.id.btnMore)
    BaseTextView btnMore;
    @BindView(R.id.btnMorePostTelegram)
    BaseRelativeLayout btnMorePostTelegram;
    @BindView(R.id.backToFirstPageTelegram)
    BaseTextView backToFirstPageTelegram;
    @BindView(R.id.telegramSection)
    BaseRelativeLayout telegramSection;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.tvInsertComment)
    BaseTextView tvInsertComment;
    @BindView(R.id.otherComment)
    BaseRelativeLayout otherComment;

    private int commentsRate;
    private OnGetMorePostTelegram moreTelegramPostListener;
    private int id;

    public ShowCommentsHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setContent(PostDetailResponse response,int id ,boolean isTelegram) {
        this.moreTelegramPostListener = moreTelegramPostListener;
        this.id = id;
        backToHome.setVisibility(View.GONE);

        if (isTelegram) {
            telegramSection.setVisibility(View.VISIBLE);
            commentSection.setVisibility(View.GONE);
        } else {
            telegramSection.setVisibility(View.GONE);
            commentSection.setVisibility(View.VISIBLE);
        }

//        if (selectedIndex == 0) {
//            backToFirstPageTelegram.setVisibility(View.GONE);
//            btnMorePostTelegram.setVisibility(View.VISIBLE);
//        } else if (selectedIndex == listSize - 1) {
//            backToFirstPageTelegram.setVisibility(View.VISIBLE);
//            btnMorePostTelegram.setVisibility(View.GONE);
//        } else {
//            backToFirstPageTelegram.setVisibility(View.VISIBLE);
//            btnMorePostTelegram.setVisibility(View.VISIBLE);
//        }

        this.myScore = response.getScore();
        ratingBar.setStarCornerRadius(12f, ratingBar.getContext().getResources().getDimensionPixelSize(R.dimen.margin_1));
        ratingBar.setStarBorderWidth(6f, ratingBar.getContext().getResources().getDimensionPixelSize(R.dimen.margin_1));

        if (myScore != null) {
            ratingBar.setRating(Float.parseFloat(myScore));
            disableView();
        } else {
            enableView();
        }
        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            if (fromUser) {
                commentsRate = (int) ratingBar.getRating();
                PostCommentDialog dialog = new PostCommentDialog(ratingBar.getContext());
                dialog.setPostId(id);
                dialog.setParentId(0);
                dialog.setMyScore(String.valueOf(commentsRate));
                dialog.setListener(this);
                dialog.show();
            }
        });

        tvInsertComment.setOnClickListener(view -> {
            commentsRate = (int) ratingBar.getRating();
            PostCommentDialog dialog = new PostCommentDialog(ratingBar.getContext());
            dialog.setPostId(id);
            dialog.setParentId(0);
            dialog.setMyScore(String.valueOf(commentsRate));
            dialog.setListener(ShowCommentsHolder.this);
            dialog.show();
        });

        btnRegister.setVisibility(View.GONE);
        btnRegister.setOnClickListener(v -> {
            if (commentsRate > 0) {
                requestPostComment(id);
            } else {
                Toast.makeText(ratingBar.getContext(), ratingBar.getContext().getResources().getString(R.string.plases_enter_selected_star), Toast.LENGTH_SHORT).show();
            }
        });

        backToHome.setOnClickListener(v -> {
            Intent intent = new Intent(ratingBar.getContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("isFromPostContentMag", true);
            ratingBar.getContext().startActivity(intent);
        });

        btnMorePostTelegram.setOnClickListener(v -> moreTelegramPostListener.btnNextPost());

        backToFirstPageTelegram.setOnClickListener(v -> moreTelegramPostListener.btnPreviousPost());

        /*countComment.setMovementMethod(LinkMovementMethod.getInstance());
        countComment.setTextSize(magFont.getSize(countComment.getResources(), row.getHtmlTag() == null ? HtmlTagEnum.p : row.getHtmlTag()));
        countComment.setTypeface(magFont.getTypeFace(row.getHtmlTag(), boldFont, mediumFont, regularFont));*/
        cvInsertComment.setVisibility(response.isCommentOpen() ? View.GONE : View.GONE);

        if (response.getComments() != null && response.getComments().size() > 0) {
            countComment.setText(countComment.getContext().getResources().getString(R.string.comment_user_for_post, response.getComments().size()));
            view.setVisibility(View.VISIBLE);
            rvCommentPost.setVisibility(View.VISIBLE);
            otherComment.setVisibility(View.VISIBLE);
            setAdapter(response.getComments(), response.isCommentOpen());
        } else {
            view.setVisibility(View.GONE);
            rvCommentPost.setVisibility(View.GONE);
            otherComment.setVisibility(View.GONE);
        }


        otherComment.setOnClickListener(v -> {
            Intent intent = new Intent(btnMoreComment.getContext(), CommentActivity.class);
            intent.putExtra("comments", response.getComments());
            intent.putExtra("commentStatus", response.isCommentOpen());
            intent.putExtra("postId", id);
            intent.putExtra("myScore", response.getScore());
            btnMoreComment.getContext().startActivity(intent);
        });

        btnInsertComment.setOnClickListener(v -> {
            PostCommentDialog dialog = new PostCommentDialog(btnInsertComment.getContext());
            dialog.setPostId(id);
            dialog.setParentId(0);
            dialog.setMyScore(myScore);
            dialog.setListener(this);
            dialog.show();
        });

        MagPostContentAdapter.magVisibilityState = this;

    }

    private void enableView() {
        ratingBar.setFocusable(true);
        ratingBar.setEnabled(true);
        ratingBar.setClickable(true);
        ratingBar.setIndicator(false);
        ratingBar.setBorderColor(ratingBar.getContext().getResources().getColor(R.color.border_ratingbar));
        ratingBar.setFillColor(ratingBar.getContext().getResources().getColor(R.color.fill_ratingbar));
        btnRegister.setEnabled(true);
        btnRegister.setClickable(true);
        btnRegister.setBackground(ratingBar.getContext().getResources().getDrawable(R.drawable.ripple_red_raduis));
        tvInsertComment.setVisibility(View.VISIBLE);
    }

    private void disableView() {
        ratingBar.setEnabled(false);
        ratingBar.setClickable(false);
        ratingBar.setFocusable(false);
        ratingBar.setIndicator(true);
        btnRegister.setEnabled(false);
        btnRegister.setClickable(false);
        btnRegister.setBackground(ratingBar.getContext().getResources().getDrawable(R.drawable.ripple_disable_radius));
        txtInsert.setText(ratingBar.getResources().getString(R.string.your_rate_this_post));
        tvInsertComment.setVisibility(View.INVISIBLE);
    }

    private void requestPostComment(int id) {
        progressBar.setVisibility(View.VISIBLE);
        PostCommentReq req = new PostCommentReq();
        req.setToken(PreferencesData.getToken(ratingBar.getContext()));
        req.setScore(commentsRate);
        req.setPostId(id);
//        req.setParentId(parenId);
//        req.setContent(edtDesc.getTextVal());
//        req.setName(edtName.getValueString());
//        req.setMobile(edtMobile.getValueString());
        PostCommentService.getInstance().postComment(ratingBar.getContext().getResources(), req, new ResponseListener<CommentResponse>() {
            @Override
            public void onGetError(String error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ratingBar.getContext(), error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(CommentResponse response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessed()) {
                    Toast.makeText(ratingBar.getContext(), ratingBar.getContext().getResources().getString(R.string.success_post_content), Toast.LENGTH_SHORT).show();
                    onUpdateRate(response.getScoreCount(), response.getAverageScore(), String.valueOf(commentsRate));
                    disableView();

                } else {
                    Toast.makeText(ratingBar.getContext(), "لطفا ورودی خود را بررسی نمایید", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {

            }
        });
    }


    private void setAdapter(ArrayList<Comment> list, boolean isCommentOpen) {
        final ArrayList<Comment> comments = new ArrayList<>();
        for (Comment com : list) {
            if (com.isRoot()) {
                comments.add(com);
            }
        }

        //TODO check exeption
        ArrayList<Comment> li;
        if (comments.size() <= 2) {
            li = new ArrayList<>(comments);
        } else {
            li = new ArrayList<>(comments.subList(0, 2));
        }
        CommentAdapter adapter = new CommentAdapter(li, isCommentOpen, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(root.getContext(), RecyclerView.VERTICAL, false);
        rvCommentPost.setLayoutManager(layoutManager);
        rvCommentPost.setAdapter(adapter);
        rvCommentPost.setHasFixedSize(true);

    }

    @Override
    public void onItemClick(int position, Comment comment) {
        PostCommentDialog dialog = new PostCommentDialog(btnInsertComment.getContext());
        dialog.setPostId(id);
        dialog.setParentId(Integer.parseInt(comment.getId()));
        dialog.setListener(this);
        dialog.show();
    }

    @Override
    public void onUpdateRate(String scoreCount, String averageScore, String myScore) {
        this.myScore = myScore;
        disableView();
        ratingBar.setRating(Float.parseFloat(myScore));
    }


    @Override
    public void onStateVisibility(int state, int listSize) {
        if (state == 0) {
            backToFirstPageTelegram.setVisibility(View.GONE);
            btnMorePostTelegram.setVisibility(View.VISIBLE);
        } else if (state == listSize) {
            backToFirstPageTelegram.setVisibility(View.VISIBLE);
            btnMorePostTelegram.setVisibility(View.GONE);
        } else {
            backToFirstPageTelegram.setVisibility(View.VISIBLE);
            btnMorePostTelegram.setVisibility(View.VISIBLE);
        }
    }
}
