package ir.delta.delta.mag.postContentCell;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.service.ResponseModel.mag.MagPostRow;
import ir.delta.delta.util.Constants;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class HeaderViewHolder extends RecyclerView.ViewHolder implements onUpdateRateResponse {

    @BindView(R.id.titr)
    BaseTextView titr;
    @BindView(R.id.tvHeader)
    BaseTextView tvHeader;
    @BindView(R.id.tvDate)
    BaseTextView tvDate;
    @BindView(R.id.imgEye)
    BaseImageView imgEye;
    @BindView(R.id.viewCount)
    BaseTextView viewCount;
    @BindView(R.id.header)
    BaseRelativeLayout header;
    @BindView(R.id.tvLeadTitle)
    BaseTextView tvLeadTitle;
//    @BindView(R.id.featureImage)
//    BaseImageView featureImage;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.viewCountLayout)
    BaseRelativeLayout viewCountLayout;
    @BindView(R.id.averageRating)
    BaseTextView averageRating;
    @BindView(R.id.scoreCount)
    BaseTextView scoreCount;
    @BindView(R.id.scoreLayout)
    BaseRelativeLayout scoreLayout;
    @BindView(R.id.tvGrouping)
    BaseTextView tvGrouping;

    private int viewWidth;

    public HeaderViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void setContent(PostDetailResponse response) {
//        header.setOnClickListener(view -> {
//            Intent intent = new Intent(tvHeader.getContext(), MagPostContentWebViewActivity.class);
//            intent.putExtra("link", magPost.getLink());
//            tvHeader.getContext().startActivity(intent);
//        });


//        ViewTreeObserver viewTreeObserver = featureImage.getViewTreeObserver();
//        if (viewTreeObserver.isAlive()) {
//            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    featureImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    viewWidth = featureImage.getWidth();
//                    featureImage.getLayoutParams().height = (int) (viewWidth / 1.75);
//                }
//            });
//        }

//        if (isTelegram){
//            featureImage.setVisibility(View.GONE);
//        }else {
//            featureImage.setVisibility(View.VISIBLE);
//            Glide.with(featureImage.getContext()).load(magPost.getImage()).placeholder(R.drawable.placeholder).into(featureImage);
//        }


//        tvHeader.setTextSize(fontSize.getSize(tvHeader.getResources(), HtmlTagEnum.h6));
//        titr.setTextSize(fontSize.getSize(tvDate.getResources(), HtmlTagEnum.p));
//        tvLeadTitle.setTextSize(fontSize.getSize(tvLeadTitle.getResources(), HtmlTagEnum.h6));
        tvHeader.setText(response.getTitle());
        if (response.getTitr() != null && !response.getTitr().isEmpty()) {
            titr.setVisibility(View.VISIBLE);
            titr.setText(response.getTitr());
        } else {
            titr.setVisibility(View.GONE);
        }

        if (response.getLeadTitle() != null && !response.getLeadTitle().isEmpty()) {
            tvLeadTitle.setVisibility(View.VISIBLE);
            tvLeadTitle.setText(response.getLeadTitle());
        } else {
            tvLeadTitle.setVisibility(View.GONE);
        }

//        category.setTextSize(fontSize.getSize(category.getResources(), HtmlTagEnum.p));

        tvGrouping.setText(" #" + response.getTermName());

//        tvDate.setTextSize(fontSize.getSize(tvDate.getResources(), HtmlTagEnum.p));
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
        Date date = null;
        try {
            date = format.parse(!TextUtils.isEmpty(response.getDate())? response.getDate() : "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd;
            if (datea.getShYear() >= 1400) {
                pd = new PersianDateFormat("j F Y");
            } else {
                pd = new PersianDateFormat("j F y");
            }
            tvDate.setText(pd.format(datea));
        } else {
            tvDate.setText(null);
        }

//        viewCount.setTextSize(fontSize.getSize(viewCount.getResources(), HtmlTagEnum.p));
        if (!TextUtils.isEmpty(response.getViewCount()) && !TextUtils.equals(response.getViewCount(), "0")) {
            viewCountLayout.setVisibility(View.GONE);
            viewCount.setText(response.getViewCount() + "");
        } else
            viewCountLayout.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(response.getAvrageScore())) {
            scoreLayout.setVisibility(View.VISIBLE);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###.#");
            averageRating.setText(formatter.format(Double.parseDouble(response.getAvrageScore())));
            scoreCount.setText(" از " + response.getScoreCount() + " رای");
        } else {
            scoreLayout.setVisibility(View.GONE);
        }
    }


    @Override
    public void onUpdateRate(String scoreCoun, String averageScore, String myScore) {
        if (!TextUtils.isEmpty(averageScore)) {
            scoreLayout.setVisibility(View.VISIBLE);
            averageRating.setText(averageScore + "");
            scoreCount.setText(" از " + scoreCoun + " رای");
        } else {
            scoreLayout.setVisibility(View.GONE);
        }
    }


}