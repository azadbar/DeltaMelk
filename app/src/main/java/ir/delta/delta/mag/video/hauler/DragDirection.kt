package ir.delta.delta.mag.video.hauler

enum class DragDirection {
    UP, DOWN
}
