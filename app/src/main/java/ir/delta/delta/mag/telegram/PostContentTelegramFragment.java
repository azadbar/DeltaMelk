package ir.delta.delta.mag.telegram;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.pushNotification.MagPostContentPushResponse;
import ir.delta.delta.service.Request.MagDetialService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.mag.Comment;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.service.ResponseModel.mag.MagPostRow;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;


public class PostContentTelegramFragment extends BaseFragment implements MagPostContentAdapter.onItemClickListener {


    @BindView(R.id.recycle)
    RecyclerView recycle;

    Unbinder unbinder;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    private MagPostContentAdapter adapter;
    private MagPost magPost = new MagPost();
    private MagFontEnum magFont;

    private Typeface boldFont;
    private Typeface mediumFont;
    private Typeface regularFont;
    private int recycleWidth;
    private ArrayList<Comment> comments;
    private FirebaseAnalytics mFBAnalytics;
    private static ContainerFragment.OnScroolListener scrollListener;
    private static OnGetMorePostTelegram moreTelegramPostListener;

    private int selectedIndex;
    private int listSizeTelegram;

    public void setListener(ContainerFragment.OnScroolListener listener) {
        scrollListener = listener;
    }

    public static PostContentTelegramFragment newInstance(MagPost magPost, MagFontEnum fontSize) {
        PostContentTelegramFragment swipeFragment = new PostContentTelegramFragment();
        Bundle args = new Bundle();
        args.putParcelable("magPost", magPost);
        args.putSerializable("magFont", fontSize);
        swipeFragment.setArguments(args);
        return swipeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View swipeView = inflater.inflate(R.layout.post_content_mag_swipe_fragment_telegram, container, false);
        unbinder = ButterKnife.bind(this, swipeView);
        mFBAnalytics = FirebaseAnalytics.getInstance(getActivity());
        appbar.setVisibility(View.GONE);//because in telegram not show heder


        if (getContext() != null) {
            boldFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf");
            mediumFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanLightMobile(FaNum).ttf");
            regularFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf");

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                recycleWidth = size.x;
            }
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            magPost = (MagPost) bundle.getSerializable("magPost");
            magFont = (MagFontEnum) bundle.get("magFont");
        }

        if (magPost != null) {
            Bundle params = new Bundle();
            params.putString("termName", "تلگرام");
            mFBAnalytics.logEvent("Categories_Content_View", params);
        }
        recycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (scrollListener != null)
                    scrollListener.onScrool(2);//for GONE realEstate Button
                if (MagPostContentAdapter.stopVideoScrollListener != null) {
                    MagPostContentAdapter.stopVideoScrollListener.onScroll();
                }


            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (scrollListener != null)
                    scrollListener.onScrool(2);//for GONE realEstate Button

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        Log.e("Scrolling", "Scrolling now");

                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        Log.e("Scrolling", "Scroll Settling");
                        if (MagPostContentAdapter.stopVideoScrollListener != null) {
                            MagPostContentAdapter.stopVideoScrollListener.onScroll();
                            MagPostContentAdapter.stopVideoScrollListener.initVideo(getActivity());
                        }
                        break;
                }

                super.onScrollStateChanged(recyclerView, newState);
            }


        });

        loadingView.setButtonClickListener(view -> getPostContent());
        getPostContent();
        return swipeView;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.onScroll();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.releaseVideo();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        if (MagPostContentAdapter.stopVideoScrollListener != null) {
            MagPostContentAdapter.stopVideoScrollListener.releaseVideo();
        }
    }

    public void changeFont(MagFontEnum fontSize) {
        this.magFont = fontSize;
        if (adapter != null) {
            adapter.setFontSize(fontSize);
        }
    }


    private void getPostContent() {
        //imgShare.setVisibility(View.GONE);

        loadingProgress.setVisibility(View.VISIBLE);
        MagDetialService.getInstance().magDetail(getResources(), magPost.getId(), PreferencesData.getToken(getContext()), new ResponseListener<MagPostContentPushResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
//                    loadingView.showError(getResources().getString(R.string.communicationError));
                    loadingProgress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(MagPostContentPushResponse response) {
                if (getActivity() != null && isAdded()) {
                    loadingView.stopLoading();
                    if (response.isSuccessed()) {
//                        loadingView.stopLoading();
                        loadingProgress.setVisibility(View.GONE);
                        structureRows(response);
                    } else {

                        Toast.makeText(getContext(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void structureRows(MagPostContentPushResponse response) {
        ArrayList<MagPostRow> list = response.createMagPostRows(getContext(), boldFont, mediumFont, regularFont, MagFontEnum.NORMAL);
        magPost.setLink(response.getLink());
        magPost.setLeadTitle(response.getLeadTitle());
        magPost.setImage(response.getImage());
        magPost.setTitr(response.getTitr());
        magPost.setSource_name(response.getSource_name());
        magPost.setSource_link(response.getSource_link());
        magPost.setViewCount(response.getViewCount());
        magPost.setAvrageScore(response.getAvrageScore());
        magPost.setScore(response.getScore());
        magPost.setScoreCount(response.getScoreCount());
        magPost.setDate(response.getDate());
        magPost.setTitle(response.getTitle());
        magPost.setCommentStatus(response.getCommentStatus());
        magPost.setTerm_name(response.getTerm_name());

        if (!TextUtils.isEmpty(magPost.getAuthor())) {
            MagPostRow row = new MagPostRow();
            row.setHtmlTag(HtmlTagEnum.author);
            row.setText(magPost.getAuthor());
            list.add(row);
        }

        if (response.getRelatedPosts() != null && response.getRelatedPosts().size() > 0) {
            MagPostRow row = new MagPostRow();
            row.setHtmlTag(HtmlTagEnum.relatedHeader);
            row.setText(getString(R.string.also_read));
            list.add(row);
            for (MagPost mg : response.getRelatedPosts()) {
                MagPostRow row1 = new MagPostRow();
                row1.setHtmlTag(HtmlTagEnum.related);
//                row1.setText(mg.getTitle());
//                row1.setPostId(mg.getId());
                row1.setMagPost(mg);
                list.add(row1);
            }
        }

        this.comments = response.getComments();
        if (comments != null) {
            MagPostRow row = new MagPostRow();
            row.setHtmlTag(HtmlTagEnum.comment);
            list.add(row);
            magPost.setComments(response.getComments());
        }


        if (list.size() > 0) {
            magPost.setList(list);
        }
        setAdapter();
    }

    private void setAdapter() {
//        adapter = new MagPostContentAdapter(magPost, MagFontEnum.NORMAL, recycleWidth, boldFont, mediumFont, regularFont, comments, this, true, moreTelegramPostListener);
        adapter.setIndexForTelegram(selectedIndex, listSizeTelegram);
        LinearLayoutManager layoutManager = new LinearLayoutManager (getActivity(), RecyclerView.VERTICAL, false);
        recycle.smoothScrollToPosition(0);//TODO change scroll
        layoutManager.scrollToPositionWithOffset(0, 0);
        recycle.setFocusable(false);
        recycle.setHasFixedSize(true);
        recycle.setLayoutManager(layoutManager);
        recycle.setItemViewCacheSize(20);
        recycle.setDrawingCacheEnabled(true);
        recycle.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recycle.setAdapter(adapter);


    }


    @Override
    public void onItemClick(int position) {

    }

    public void setMoreTelegramPostListener(OnGetMorePostTelegram moreTelegramPostListener) {
        PostContentTelegramFragment.moreTelegramPostListener = moreTelegramPostListener;
    }

    public void setIndex(int selectedIndex, int listSize) {
        this.listSizeTelegram = listSize;
        this.selectedIndex = selectedIndex;
    }
}
