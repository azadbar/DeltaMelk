package ir.delta.delta.mag.main_page;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.adivery.sdk.Adivery;
import com.adivery.sdk.AdiveryInterstitialCallback;
import com.adivery.sdk.AdiveryLoadedAd;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.activity.ContainerFragment;
import ir.delta.delta.activity.MainActivity;
import ir.delta.delta.ads.AdsActivity;
import ir.delta.delta.ads.AdsAdapter;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionFavoriteMag;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.favoriteMag.FavoriteMagListActivity;
import ir.delta.delta.mag.MagPostContentAdapter;
import ir.delta.delta.mag.MagPostContentWebViewActivity;
import ir.delta.delta.mag.PostAdapter;
import ir.delta.delta.mag.PostContentActivity;
import ir.delta.delta.mag.SinglePostContentActivity;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestCallback;
import ir.delta.delta.service.RequestListener;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import ir.metrix.sdk.Metrix;
import retrofit2.Response;


public class MainPageMagFragment extends BaseFragment implements PostAdapter.onItemClick, PostContentActivity.setBackgroundReadItem,
        MagPostContentAdapter.onItemClickListener, AdsAdapter.OnItemClickListener, OnRefreshMainPageList {

    //    @BindView(R.id.real_estate_sector)
//    BaseRelativeLayout realEstateSector;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.loadingProgress)
    ProgressBar loadingProgress;
    @BindView(R.id.main_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.btnNewSearch)
    BaseTextView btnNewSearch;
    @BindView(R.id.cvBtnNewSearch)
    CardView cvBtnNewSearch;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.empty)
    BaseRelativeLayout empty;
    private int position;
    private PostAdapter postAdapter;
    private String termId;
    private String termName;
    private ArrayList<Integer> readPost;
    private ArrayList<Integer> favoriteList;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;
    private final int seed = 30;
    private final ArrayList<MagPost> list = new ArrayList<>();
    private boolean isSwipeRefresh;
    private ContainerFragment.OnGetRecentList listener;
    private LinearLayoutManager layoutManager;
    private ContainerFragment.OnScroolListener scroolListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String postId;
    private String link;


    public static MainPageMagFragment newInstance(int position, String termId, String termName,
                                                  ContainerFragment.OnGetRecentList listener,
                                                  ContainerFragment.OnScroolListener scrollListener, String postId, String link) {
        MainPageMagFragment pageFragment = new MainPageMagFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        args.putString("termId", termId);
        args.putString("termName", termName);
        args.putString("postId", postId);
        args.putString("link", link);
        pageFragment.setListener(listener);
        pageFragment.setScrollListener(scrollListener);
        pageFragment.setArguments(args);

        return pageFragment;
    }

    private void setScrollListener(ContainerFragment.OnScroolListener scrollListener) {
        this.scroolListener = scrollListener;
    }

    private void setListener(ContainerFragment.OnGetRecentList listener) {
        this.listener = listener;
    }

    public MainPageMagFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_page_mag, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            position = bundle.getInt("position");
            termId = bundle.getString("termId");
            termName = bundle.getString("termName");
            postId = bundle.getString("postId");
            link = bundle.getString("link");
        }

        //get main request
        getPostsRequest();



        //read post for change background
        readPost = TransactionReadPost.getInstance().getReadPostsId(getContext());

        //read post for change background
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());

        FavoriteMagListActivity.refreshMainPageListener = this;


        empty.setVisibility(View.GONE);
        if (getActivity() != null) {
            layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setHasFixedSize(true);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NotNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());
                                getPostsRequest();
                            }
                    }
                }
            });
        }

        scroolListener.onScrool(0);

        //when response not found and try again
        loadingView.setButtonClickListener(v -> getPostsRequest());


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) {
//                    animateSwtich(1);
                    scroolListener.onScrool(1);

                } else {
                    if (!inLoading) {
//                        animateSwtich(0);
                        scroolListener.onScrool(0);
                    }
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        swipeRefresh.setColorSchemeColors(Color.RED);
        swipeRefresh.setOnRefreshListener(this::swipeRefreshMethod);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void getPostsRequest() {
        Bundle params = new Bundle();
        params.putString("click_menu", termName);
        mFirebaseAnalytics.logEvent("Click_in_menu", params);
        //analytics for metrix
        Metrix.getInstance().newEvent("xdtyo");
        Map<String, String> attributes = new HashMap<>();
        attributes.put("xdtyo", termName);
        Metrix.getInstance().addUserAttributes(attributes);
        if (!inLoading) {
            inLoading = true;
            if (list.isEmpty()) {
                if (isSwipeRefresh) {
                    recyclerView.animate().alpha(0.0f).setStartDelay(700);
                } else {
                    showLoading();
                }
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
        }
        new RequestCallback<>(getActivity(), ApiClient.getClient_MAG().create(ReqInterface.class).getPosts(list.size(), seed, termId), new RequestListener<List<MagPost>>() {
            @Override
            public void onResponse(@NonNull Response<List<MagPost>> response) {
                try {
                    if (response.body() != null) {
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.animate().alpha(1.0f);
                        isSwipeRefresh = false;
                        swipeRefresh.setRefreshing(false);
                        rlLoding.setVisibility(View.GONE);
                        stopLoading();
                        if (response.body().size() < offset) {
                            endOfList = true;
                        }
                        inLoading = false;
                        list.addAll(response.body());

                        setDataAdapter();
                        scroolListener.onScrool(0);
                    } else {
                        stopLoading();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(int code, @NonNull JSONObject jsonObject) {
                stopLoading();
                loadingView.setVisibility(View.VISIBLE);
                loadingView.showError(getString(R.string.communicationError));
            }
        });
    }

    private void swipeRefreshMethod() {
        favoriteList = TransactionFavoriteMag.getInstance().getReadPostsId(getContext());
        list.clear();
        if (postAdapter != null)
            postAdapter.notifyDataSetChanged();
        isSwipeRefresh = true;
        getPostsRequest();
    }


    private void setDataAdapter() {
        if (postAdapter == null) {
            postAdapter = new PostAdapter(list, readPost, this, favoriteList);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(postAdapter);
            recyclerView.setNestedScrollingEnabled(true);
        } else {
            postAdapter.notifayList(favoriteList);
        }
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null) {
            rlLoding.setVisibility(View.GONE);
            if (list.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


    private void showLoading() {
        loadingProgress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
//        loadingView.showLoading(true);
    }

    private void stopLoading() {
        loadingProgress.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.animate().alpha(1.0f);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getActivity() != null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(int position, ArrayList<MagPost> list) {
        Intent intent = new Intent(getActivity(), PostContentActivity.class);
        PostContentActivity.magPosts = list;//becuse too large list
        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList("magPosts", list);
        bundle.putString("termId", termId);
        bundle.putInt("selectedIndex", position);
        intent.putExtras(bundle);
        PostContentActivity.refreshMainPageListener = this;
//        PostContentActivity.listener = this;
//        list.get(position).setOpen(true);
//        postAdapter.notifyDataSetChanged();
        startActivity(intent);

    }


    @Override
    public void onItemShare(MagPost object) {
        Constants.sharePostLink(getContext(), object.getId(), object.getLink(), object.getTitle(), object.getTerm_name());
    }


    @Override
    public void onItemNext(int position, ArrayList<MagPost> magPosts) {
        if (magPosts != null && magPosts.size() > 0) {
            readPost.add(magPosts.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(getContext(), magPosts.get(position).getId());
            postAdapter.updateReceiptsList(magPosts);
        }
    }

    @Override
    public void onItemClick(AdsItem adsItem, int position) {
        Intent intent = new Intent(getActivity(), AdsActivity.class);
        intent.putExtra("parentId", adsItem.getId());
        intent.putExtra("groupTitle", adsItem.getTitle());
        intent.putExtra("isMelki", adsItem.isMelki());
//        intent.putExtra("isShowButtonEstate", response.isShowButtonEstate());//for show button estate for cafe bazar payment
        startActivity(intent);
        //analytics for metrix
        Metrix.getInstance().newEvent("ypojp");
        Map<String, String> attributes = new HashMap<>();
        attributes.put("ypojp", adsItem.getTitle());
        Metrix.getInstance().addUserAttributes(attributes);
//        findNavController(R.id.nav_host_fragment).setGraph(R.navigation.nav_graph, intent.getExtras());
    }

    @Override
    public void onRefresh() {
        //read post for change background
        swipeRefreshMethod();
    }


    @Override
    public void onItemClick(int position) {
    }


}