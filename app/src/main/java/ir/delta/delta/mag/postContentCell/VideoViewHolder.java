package ir.delta.delta.mag.postContentCell;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;

public class VideoViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.webview)
    WebView webview;

    public VideoViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

    }


    public void setContent(Object object, int recycleWitdh) {
        HtmlTag htmlTag = (HtmlTag) object;
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getLayoutParams().height = (recycleWitdh * 9 / 16);

        try {
            webview.loadData(htmlTag.getScriptHtml(), "text/html", null);//TODO handle webView
        } catch (Exception e) {
            if (e.getMessage() != null && e.getMessage().contains("Failed to load WebView provider: No WebView installed")) {
                e.printStackTrace();
            }
        }


    }


}
