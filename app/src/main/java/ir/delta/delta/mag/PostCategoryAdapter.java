package ir.delta.delta.mag;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class PostCategoryAdapter extends RecyclerView.Adapter<PostCategoryAdapter.ViewHolder> {


    private final ArrayList<MagMenuResponse> list;
    private final onItemClick listener;

    PostCategoryAdapter(ArrayList<MagMenuResponse> list, onItemClick listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_mag, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MagMenuResponse magMenuResponse = list.get(position);
        holder.tvTitle.setText(magMenuResponse.getTitle());

        holder.bind(position, magMenuResponse, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, MagMenuResponse magMenuResponse, final onItemClick listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, magMenuResponse));
        }

    }

    public interface onItemClick {
        void onItemClick(int position, MagMenuResponse magMenuResponse);
    }
}
