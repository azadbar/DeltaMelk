package ir.delta.delta.mag.video;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionReadPost;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import ir.delta.delta.util.Constants;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VideoPostAdapter extends RecyclerView.Adapter<VideoPostAdapter.ViewHolder> {


    private ArrayList<MagPost> list;
    private final ArrayList<Integer> readPost;
    private final onItemClick listener;

    public VideoPostAdapter(ArrayList<MagPost> list, ArrayList<Integer> readPost, onItemClick listener) {
        this.list = list;
        this.readPost = readPost;
        this.listener = listener;
    }

    public void updateReceiptsList(ArrayList<MagPost> newlist) {
        list.clear();
        list.addAll(newlist);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_mag, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();

        MagPost object = list.get(position);
        holder.tvTitle.setText(object.getTitle());
        holder.tvAuthor.setText(object.getAuthor());
//        holder.tvDate.setText(object.getDate());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Constants.getLanguage().getLocale());
        Date date = null;
        try {
            date = format.parse(object.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd;
            if (datea.getShYear() >= 1400) {
                pd = new PersianDateFormat("H:i" + " - " + "l j F Y");
            } else {
                pd = new PersianDateFormat("H:i" + " - " + "l j F y");
            }
            holder.tvDate.setText(pd.format(datea) + "");
        } else {
            holder.tvDate.setText(null);
        }

//        if (object.isOpen()) {
//            holder.root.setBackground(res.getDrawable(R.drawable.ripple_gray_no_radius));
//        } else {
//            holder.root.setBackground(res.getDrawable(R.drawable.ripple_primary));
//        }


//        if (readPost.contains(object.getId())) {
//            holder.root.setBackground(res.getDrawable(R.drawable.ripple_gray_no_radius));
//        } else {
//            holder.root.setBackground(res.getDrawable(R.drawable.ripple_primary));
//        }

        Glide.with(holder.itemView.getContext()).load(object.getImage()).placeholder(R.drawable.placeholder).into(holder.imageMag);
        holder.itemView.setOnClickListener(view -> changeBackground(holder.root.getContext(), position, list, listener));
    }


    private void changeBackground(Context context, int position, ArrayList<MagPost> list, onItemClick listener) {
        if (list != null && position < list.size()) {
            readPost.add(list.get(position).getId());
            TransactionReadPost.getInstance().saveMagPost(context, list.get(position).getId());
            listener.onItemClick(position, list);
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<MagPost> posts) {
        list = posts;
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        BaseImageView imageMag;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.tvAuthor)
        BaseTextView tvAuthor;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }


    public interface onItemClick {
        void onItemClick(int position, ArrayList<MagPost> list);
    }
}
