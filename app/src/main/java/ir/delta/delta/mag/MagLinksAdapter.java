package ir.delta.delta.mag;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagLink;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class MagLinksAdapter extends RecyclerView.Adapter<MagLinksAdapter.ViewHolder> {


    private final ArrayList<MagLink> list;
    private final OnItemClickListener listener;

    MagLinksAdapter(ArrayList<MagLink> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mag_link, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MagLink object = list.get(position);
        if (object != null) {
            holder.tvTitle.setText(object.getText());
        }

        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.root)
        BaseRelativeLayout root;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}
