package ir.delta.delta.mag;

import android.graphics.Color;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {


    private final ArrayList<MagMenuResponse> list;
    private final onMenuItemClick listener;
    private int selectedIndex;


    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }

    MenuAdapter(ArrayList<MagMenuResponse> list, onMenuItemClick listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_category_horizontal, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        MagMenuResponse magMenuResponse = list.get(position);
        holder.tvTitle.setText(magMenuResponse.getTitle());

        if (position == selectedIndex) {
            holder.tvTitle.setTextColor(holder.tvTitle.getResources().getColor(R.color.primaryBack));
            holder.tvTitle.setTypeface(Typeface.createFromAsset(holder.tvTitle.getContext().getAssets(), "fonts/IRANSansMobile(FaNum)_Bold.ttf"));
            holder.lineView.setBackgroundColor(holder.tvTitle.getResources().getColor(R.color.primaryBack));
        } else {
            holder.tvTitle.setTextColor(holder.tvTitle.getResources().getColor(R.color.secondaryTextColor));
            holder.tvTitle.setTypeface(Typeface.createFromAsset(holder.tvTitle.getContext().getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
            holder.lineView.setBackgroundColor(Color.TRANSPARENT);
        }


        holder.bind(position, magMenuResponse, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.lineView)
        View lineView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, MagMenuResponse magMenuResponse, final onMenuItemClick listener) {
            itemView.setOnClickListener(v -> listener.onMenuItemClick(position, magMenuResponse));
        }

    }

    public interface onMenuItemClick {
        void onMenuItemClick(int position, MagMenuResponse magMenuResponse);
    }
}
