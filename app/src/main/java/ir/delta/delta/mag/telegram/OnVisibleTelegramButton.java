package ir.delta.delta.mag.telegram;

public interface OnVisibleTelegramButton {
    void onStateVisibility(int state, int listSize);
}
