package ir.delta.delta.mag.postContentCell;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.mag.MagPostRow;
import ir.delta.delta.service.ResponseModel.mag.NodeImage;

public class GalleryViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.parent)
    BaseLinearLayout parent;


    public GalleryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void setContent(Object object, int recycleWitdh) {
        HtmlTag htmlTag = (HtmlTag) object;
        parent.removeAllViews();

        if (htmlTag.getColumns() == 1) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) parent.getLayoutParams();
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            parent.setOrientation(LinearLayout.VERTICAL);
            for (NodeImage image : htmlTag.getImages()) {
                ImageView view = new ImageView(parent.getContext());
                LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) (recycleWitdh / image.getRatio()));
                view.setLayoutParams(lp1);
                Glide.with(parent.getContext()).load(image.getSrc()).placeholder(R.drawable.placeholder).into(view);
                parent.addView(view);

            }
        } else if (htmlTag.getColumns() > 1) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) parent.getLayoutParams();
            lp.width = LinearLayout.LayoutParams.MATCH_PARENT;
            lp.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            int imageWidth = recycleWitdh / htmlTag.getImages().size();
            parent.setOrientation(LinearLayout.HORIZONTAL);
            int i = 0;
            for (NodeImage image : htmlTag.getImages()) {
                ImageView view = new ImageView(parent.getContext());
                LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(imageWidth, (int) (imageWidth / image.getRatio()));
                view.setLayoutParams(lp1);
                Glide.with(parent.getContext()).load(image.getSrc()).into(view);
                parent.addView(view);

            }
        }
    }
}
