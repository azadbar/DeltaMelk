package ir.delta.delta.mag.postContentCell;

public interface onUpdateRateResponse {
    void onUpdateRate(String scoreCount, String averageScore, String myScore);
}
