package ir.delta.delta.mag;

import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

/**
 * create by m.azadbar
 */
public class SliderMagAdapter extends PagerAdapter {


    private final ArrayList<MagPost> sliderMags;
    private final int termId;

    public SliderMagAdapter(ArrayList<MagPost> sliderMags, int termId) {
        this.sliderMags = sliderMags;
        this.termId = termId;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return sliderMags.size();
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup view, int position) {
        MagPost sliderMag = sliderMags.get(position);
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        View myImageLayout = inflater.inflate(R.layout.slide_image_mag, view, false);
        ImageView myImage = myImageLayout.findViewById(R.id.image);
        myImage.setBackgroundColor(Color.RED);
        BaseTextView magTitle = myImageLayout.findViewById(R.id.magTitle);
        RelativeLayout row = myImageLayout.findViewById(R.id.row);

        row.setOnClickListener(view1 -> {
            Intent intent = new Intent(magTitle.getContext(), PostContentActivity.class);
            intent.putExtra("magPosts", sliderMags);
            intent.putExtra("selectedIndex", position);
            intent.putExtra("termId", termId);
            magTitle.getContext().startActivity(intent);
        });

        magTitle.setText(sliderMag.getTitle());
        Glide.with(myImage.getContext()).load(sliderMag.getImage()).placeholder(R.drawable.placeholder).into(myImage);
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }
}
