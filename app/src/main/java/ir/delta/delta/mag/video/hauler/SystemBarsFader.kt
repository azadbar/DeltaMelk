package ir.delta.delta.mag.video.hauler

import android.app.Activity
import android.graphics.Color
import android.os.Build
import androidx.annotation.RequiresApi


internal class SystemBarsFader(private val activity: Activity) {

    private val statusBarAlpha: Int  = Color.TRANSPARENT

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun onDrag(elasticOffsetPixels: Float, rawOffset: Float) {
        when {
            elasticOffsetPixels != 0f -> // dragging downward or upward, fade the status bar in proportion
                activity.window.statusBarColor = ColorUtils.modifyAlpha(getStatusBarColor(), getNewAlpha(rawOffset))
            elasticOffsetPixels == 0f ->
                activity.window.statusBarColor = ColorUtils.modifyAlpha(getStatusBarColor(), statusBarAlpha)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun onDismiss() {
        // set transparent window bg and transparent navigation bar
        activity.window.decorView.setBackgroundColor(0)
        activity.window.navigationBarColor = ColorUtils.modifyAlpha(activity.window.navigationBarColor, 0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getStatusBarColor() = activity.window.statusBarColor

    private fun getNewAlpha(rawOffset: Float) = ((1f - rawOffset) * statusBarAlpha).toInt()
}
