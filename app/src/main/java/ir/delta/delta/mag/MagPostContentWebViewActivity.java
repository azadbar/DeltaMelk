package ir.delta.delta.mag;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.util.Constants;
import android.content.pm.PackageManager;

import static android.view.View.VISIBLE;

public class MagPostContentWebViewActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.progreesBar)
    ProgressBar progreesBar;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mag_post_content_web_view);
        ButterKnife.bind(this);
        webview.getSettings().setJavaScriptEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            link = extras.getString("link");
        }
        Constants.setBackgroundProgress(this, progreesBar);
        waitingStart();
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                // Return the app name after finish loading
                if (progress == 100)
                    waitingStop();
            }
        });
//        link = "https://telegram.me/zahraazadbar63";
//        link = "https://deltapayam.com/241390";
        if (link != null && link.contains("instagram")) {
            Uri uri = Uri.parse(link);
            Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
            likeIng.setPackage("com.instagram.android");
            try {
                startActivity(likeIng);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(link)));
            }
        } else if (link != null && link.contains("telegram")) {
            intentMessageTelegram(link);
        } else if (URLUtil.isValidUrl(link)) {
            webview.loadUrl(link);//TODO handle webView
        } else {
            webview.loadData(link, "text/html", null);//TODO handle webView
        }
        waitingStop();
    }

    private void intentMessageTelegram(String id) {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(MagPostContentWebViewActivity.this, appName);
        if (isAppInstalled) {
            Intent telegram = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            startActivity(telegram);
        } else {
            Toast.makeText(MagPostContentWebViewActivity.this, "Telegram not Installed", Toast.LENGTH_SHORT).show();
        }
    }

    private static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void waitingStop() {
        progreesBar.setVisibility(View.GONE);
        webview.setVisibility(VISIBLE);

    }

    private void waitingStart() {
        webview.setVisibility(View.GONE);
        progreesBar.setVisibility(VISIBLE);
    }

}
