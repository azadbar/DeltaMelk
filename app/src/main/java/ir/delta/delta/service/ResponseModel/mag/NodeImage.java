package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NodeImage implements Serializable {

    @SerializedName("src")
    private String src;
    @SerializedName("ratio")
    private float ratio;

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public float getRatio() {
        if (ratio <= 0) {
            return 1;
        }
        return ratio;
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }
}
