package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class OrderAdvertisingReq {

    @SerializedName("senderName")
    private String senderName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("description")
    private String description;
    @SerializedName("logedInUserTokenOrId")
    private String logedInUserTokenOrId;

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogedInUserTokenOrId() {
        return logedInUserTokenOrId;
    }

    public void setLogedInUserTokenOrId(String logedInUserTokenOrId) {
        this.logedInUserTokenOrId = logedInUserTokenOrId;
    }
}
