package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class DeltanetPropertySelectReq {

    @SerializedName("deltanetDepositId")
    private long deltanetDepositId;

    public long getDeltanetDepositId() {
        return deltanetDepositId;
    }

    public void setDeltanetDepositId(long deltanetDepositId) {
        this.deltanetDepositId = deltanetDepositId;
    }
}
