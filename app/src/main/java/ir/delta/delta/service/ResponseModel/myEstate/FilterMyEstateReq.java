package ir.delta.delta.service.ResponseModel.myEstate;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;
import java.util.WeakHashMap;

public class FilterMyEstateReq implements Serializable {


    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("statusLocalId")
    private int statusLocalId;
    @SerializedName("depositId")
    private int depositId;


    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public int getStatusLocalId() {
        return statusLocalId;
    }

    public void setStatusLocalId(int statusLocalId) {
        this.statusLocalId = statusLocalId;
    }

    public int getDepositId() {
        return depositId;
    }

    public void setDepositId(int depositId) {
        this.depositId = depositId;
    }

    public Map<String, Object> getParams(int startIndex, int offset) {
        Map<String, Object> map = new WeakHashMap<>();

        if (propertyTypeLocalId > 0)
            map.put("propertyTypeLocalId", propertyTypeLocalId);

        if (depositId > 0)
            map.put("depositId", depositId);
        map.put("statusLocalId", statusLocalId);
        map.put("startIndex", startIndex);
        map.put("offset", offset);

        return map;
    }

    public static Map<String, Object> getParamsWithoutFilter(int startIndex, int offset, int statusId) {

        Map<String, Object> map = new WeakHashMap<>();
        map.put("statusLocalId", statusId);
        map.put("startIndex", startIndex);
        map.put("offset", offset);

        return map;
    }


}
