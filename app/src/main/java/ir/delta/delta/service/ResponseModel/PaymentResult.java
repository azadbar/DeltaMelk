package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class PaymentResult {

    @SerializedName("resultType")
    private boolean resultType;//
    @SerializedName("invoice")
    private String invoice;//کد پیگیری
    @SerializedName("orderId")
    private String orderId;//foctor
    @SerializedName("enOrderId")
    private String enOrderId;
    @SerializedName("payDate")
    private String payDate;//تاریخ پرداخت
    @SerializedName("price")
    private String price;
    @SerializedName("isWalletPayment")
    private boolean isWalletPayment;


    public boolean isResultType() {
        return resultType;
    }

    public void setResultType(boolean resultType) {
        this.resultType = resultType;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getEnOrderId() {
        return enOrderId;
    }

    public void setEnOrderId(String enOrderId) {
        this.enOrderId = enOrderId;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isWalletPayment() {
        return isWalletPayment;
    }

    public void setWalletPayment(boolean walletPayment) {
        isWalletPayment = walletPayment;
    }
}
