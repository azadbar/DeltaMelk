package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.EmptyReq;
import ir.delta.delta.service.ResponseModel.CheckProfileImageValidResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class CheckProfileImageValidService {


    private static CheckProfileImageValidService chackProfileImageValidService;

    public static CheckProfileImageValidService getInstance() {
        if (chackProfileImageValidService == null) {
            chackProfileImageValidService = new CheckProfileImageValidService();
        }
        return chackProfileImageValidService;
    }


    public void checkProfileImageValid(Context context, final Resources res, EmptyReq req, final ResponseListener<CheckProfileImageValidResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).chakeProfileImageValid(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    CheckProfileImageValidResponse response = gson.fromJson(mJson, CheckProfileImageValidResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
