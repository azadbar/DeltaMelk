package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class CheckUserConsultantReq {

    @SerializedName("activationCode")
    private String activationCode;
    @SerializedName("userToken")
    private String userToken;

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
