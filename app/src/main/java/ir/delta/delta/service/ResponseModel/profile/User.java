package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("isGeneral")
    private boolean isGeneral;
    @SerializedName("userId")
    private int userId;//consultant
    @SerializedName("userToken")
    private String userToken;//normal user
    @SerializedName("activityLocation")
    private String activityLocation;//تهران منطقه 5
    @SerializedName("mobile")
    private String mobile;//09354669385
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("displayName")
    private String displayName;//for cansultant : alias , for general userDisplayName
    @SerializedName("officeTitle")
    private String officeTitle;
    @SerializedName("officeLogo")
    private String officeLogo;
    @SerializedName("imagePath")
    private String imagePath;
    @SerializedName("showDeltaNetMenu")
    private boolean showDeltaNetMenu;
    @SerializedName("locationId")
    private int locationId;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public boolean isGeneral() {
        return isGeneral;
    }

    public void setGeneral(boolean general) {
        isGeneral = general;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getActivityLocation() {
        return activityLocation;
    }

    public void setActivityLocation(String activityLocation) {
        this.activityLocation = activityLocation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public String getOfficeTitle() {
        return officeTitle;
    }

    public void setOfficeTitle(String officeTitle) {
        this.officeTitle = officeTitle;
    }

    public String getOfficeLogo() {
        return officeLogo;
    }

    public void setOfficeLogo(String officeLogo) {
        this.officeLogo = officeLogo;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isShowDeltaNetMenu() {
        return showDeltaNetMenu;
    }

    public void setShowDeltaNetMenu(boolean showDeltaNetMenu) {
        this.showDeltaNetMenu = showDeltaNetMenu;
    }
}
