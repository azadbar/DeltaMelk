package ir.delta.delta.service.ResponseModel.wallet;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class WalletAmountResponse {
    private double amount;
    private ModelStateErrors modelStateErrors;
    private String tomanString;
    private boolean successed;
    private String message;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public ModelStateErrors getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ModelStateErrors modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public String getTomanString() {
        return tomanString;
    }

    public void setTomanString(String tomanString) {
        this.tomanString = tomanString;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
