package ir.delta.delta.service.ResponseModel.mag;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.customView.CustomClick;
import ir.delta.delta.customView.CustomTypeface;
import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.util.Constants;

public class MagPostRow implements Parcelable {

    @SerializedName("htmlTag")
    private HtmlTagEnum htmlTag;
    @SerializedName("magStyles")
    private ArrayList<MagStyle> magStyles;
    @SerializedName("text")
    private String text;
    @SerializedName("src")
    private String src;
    @SerializedName("poster")
    private String poster;
    @SerializedName("alt")
    private String alt;
    @SerializedName("type")
    private String type; // for <script> tag
    @SerializedName("href")
    private String href;

    @SerializedName("heightFactor")
    private float heightFactor = 1; // height / width
    @SerializedName("scriptHtml")
    private String scriptHtml = "";
    //for gallery
    @SerializedName("column")
    private int column;
    @SerializedName("images")
    private ArrayList<NodeImage> images;
    @SerializedName("spannableString")
    private transient SpannableStringBuilder spannableString;
    @SerializedName("postId")
    private int postId;

    @SerializedName("magPost")
    private MagPost magPost;


    public MagPostRow(){

    }

    protected MagPostRow(Parcel in) {
        text = in.readString();
        src = in.readString();
        poster = in.readString();
        alt = in.readString();
        type = in.readString();
        href = in.readString();
        heightFactor = in.readFloat();
        scriptHtml = in.readString();
        column = in.readInt();
        postId = in.readInt();
        magPost = in.readParcelable(MagPost.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeString(src);
        dest.writeString(poster);
        dest.writeString(alt);
        dest.writeString(type);
        dest.writeString(href);
        dest.writeFloat(heightFactor);
        dest.writeString(scriptHtml);
        dest.writeInt(column);
        dest.writeInt(postId);
        dest.writeParcelable(magPost, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MagPostRow> CREATOR = new Creator<MagPostRow>() {
        @Override
        public MagPostRow createFromParcel(Parcel in) {
            return new MagPostRow(in);
        }

        @Override
        public MagPostRow[] newArray(int size) {
            return new MagPostRow[size];
        }
    };

    public MagPost getMagPost() {
        return magPost;
    }

    public void setMagPost(MagPost magPost) {
        this.magPost = magPost;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public void setHeightFactor(float heightFactor) {
        this.heightFactor = heightFactor;
    }

    public String getScriptHtml() {
        return scriptHtml;
    }

    public void setScriptHtml(String scriptHtml) {
        this.scriptHtml = scriptHtml;
    }

    public HtmlTagEnum getHtmlTag() {
        return htmlTag;
    }


    public String getText() {
        return text;
    }

    public String getSrc() {
        return src;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getAlt() {
        return alt;
    }


    public String getType() {
        return type;
    }

    public String getHref() {
        return href;
    }


    public void setHtmlTag(HtmlTagEnum htmlTag) {
        this.htmlTag = htmlTag;
    }

    public void setMagStyles(ArrayList<MagStyle> magStyles) {
        this.magStyles = magStyles;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }


    public void setType(String type) {
        this.type = type;
    }

    public void setHref(String href) {
        this.href = href;
    }


    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public ArrayList<NodeImage> getImages() {
        if (images == null) {
            return new ArrayList<>();
        }
        return images;
    }

    public int getImagesGalleryCount() {
        if (images == null || images.size() == 0) {
            return 1;
        }
        return images.size();
    }

    public SpannableStringBuilder getSpannableString() {
        return spannableString;
    }

    public void setImages(ArrayList<NodeImage> images) {
        this.images = images;
    }

    public float getHeightFactor() {
        if (heightFactor <= 0)
            return 1;
        return heightFactor;
    }


    public void fillHrRow(NodeContent content) {
        htmlTag = HtmlTagEnum.hr;
        if (content != null && content.getStyle() != null) {
            magStyles = getMagStyle(content.getStyle());
        }
    }

    public void fillScriptRow(NodeContent content) {
        htmlTag = HtmlTagEnum.script;
        if (content != null && !TextUtils.isEmpty(content.getSrc()) && !TextUtils.isEmpty(content.getType())) {
            String srcReplace = content.getSrc().replace("[", "").replace("]", "");
            Uri uri = Uri.parse(srcReplace);
            String divId = uri.getQueryParameter("datarnddiv");
            String html = "<div id=\"" + divId + "\">\n" +
                    "<script type=\"text/JavaScript\" src=\"" + content.getSrc() + "\"></script>\n" +
                    "</div>";
            String webData = "<!DOCTYPE \\\">\n" +
                    "    <html>\n" +
                    "      <body id=\"body\">  \n" +
                    html +
                    "      </body>\n" +
                    "    </html>";
            setScriptHtml(webData);
        }
    }

    public void fillGalleryRow(NodeContent content) {
        htmlTag = HtmlTagEnum.gallery;
        column = content.getColumn();
        images = content.getImages();
        if (content.getStyle() != null) {
            magStyles = getMagStyle(content.getStyle());
        }
    }


    public void fillImgRow(NodeContent content) {
        htmlTag = HtmlTagEnum.img;
        text = content.getText();
        src = content.getSrc();
        alt = content.getAlt();
        href = content.getHref();
        heightFactor = Constants.getHeightFactr(content.getWidth(), content.getHeight());
        if (content.getStyle() != null) {
            magStyles = getMagStyle(content.getStyle());
        }
    }


    public void fillVideoRow(NodeContent content) {
        htmlTag = HtmlTagEnum.video;
        src = content.getSrc();
        poster = content.getPoster();
        heightFactor = Constants.getHeightFactr(content.getWidth(), content.getHeight());
        if (content.getStyle() !=null){
            magStyles = getMagStyle(content.getStyle());
        }
    }

    public int getBackColor(int defultColor) {
        if (magStyles != null) {
            for (MagStyle style : magStyles) {
                if (TextUtils.equals(style.getKey(), ("background-color"))) {
                    return Constants.parseColor(style.getValue(), defultColor);
                }
            }
        }
        return defultColor;
    }
//    public void fillRow(NodeContent content, String tag) {
//        setHtmlTag(HtmlTagEnum.getHtmlTag(tag));
//        if (content != null) {
//            setText(content.getText());
//            setHref(content.getHref());
//            setType(content.getType());
//            heightFactor = Constants.getHeightFactr(content.getWidth(), content.getHeight());
//            setSrc(content.getSrc());
//            setAlt(content.getAlt());
//            setColumn(content.getColumn());
//            setImages(content.getImages());
//            if (!TextUtils.isEmpty(content.getStyle())) {
//                setMagStyles(getMagStyle(content.getStyle()));
//            }
//        }
//        if (htmlTag == HtmlTagEnum.script) {
//            fillScriptRow();
//        }
//
//    }


    private ArrayList<MagStyle> getMagStyle(String value) {
        String trimValue = value.replace(" ", "");
        if (!TextUtils.isEmpty(trimValue)) {
            ArrayList<MagStyle> magStyles = new ArrayList<>();
            String[] styles = trimValue.split(";");
            for (String s : styles) {
                String[] arr = s.split(":");
                if (arr.length == 2 && !TextUtils.isEmpty(arr[0]) && !TextUtils.isEmpty(arr[1])) {
                    magStyles.add(new MagStyle((arr[0]), (arr[1])));
                }
            }
            return magStyles;
        }
        return new ArrayList<>();
    }


    public void appendText(String t) {
        if (TextUtils.isEmpty(t)) {
            return;
        }
        if (TextUtils.isEmpty(text)) {
            text = t;
        } else {
            text += "\n" + t;
        }
    }


    public void fillHtmlString(Context context, Typeface boldFont, Typeface
            mediumFont, Typeface regularFont, ArrayList<MagNode> temp, MagFontEnum magFont) {
        if (spannableString == null) {
            spannableString = new SpannableStringBuilder();
        }
        htmlTag = HtmlTagEnum.other;
        int start = 0;
        for (MagNode node : temp) {
            if (node.getContent() != null && node.getContent().getText() != null) {
                String str = node.getContent().getText();
                if (TextUtils.equals(str, "\n") || str.isEmpty()) {
                    spannableString.append(str);
                    start += str.length();
                    continue;
                }
                HtmlTagEnum tagEnum = HtmlTagEnum.getHtmlTag(node.getTag());

                switch (tagEnum) {
                    case h1:
                    case h2:
                    case h3:
                    case h4:
                    case h5:
                    case h6:
                    case div:
                    case p:
                        spannableString.append(str);
                        addSpannable(context, tagEnum, boldFont, mediumFont, regularFont, start, str, node.getContent(), magFont);
                        break;
                    default:
                        str = str + " ";
                        spannableString.append(str);
                        addSpannable(context, tagEnum, boldFont, mediumFont, regularFont, start, str, node.getContent(), magFont);
                        break;
                }
                start += str.length();
            }
        }
    }

    private void addSpannable(Context context, HtmlTagEnum tagEnum, Typeface boldFont, Typeface
            mediumFont, Typeface regularFont, int start, String str, NodeContent
                                      content, MagFontEnum magFont) {
        Layout.Alignment align = Layout.Alignment.ALIGN_NORMAL;
        int color = context.getResources().getColor(R.color.primaryTextColor);

        for (MagStyle style : getMagStyle(content.getStyle())) {
            if (TextUtils.equals(style.getKey(), "text-align")) {
                switch (style.getValue()) {
                    case "left":
                        align = Layout.Alignment.ALIGN_OPPOSITE;
                        break;
                    case "center":
                        align = Layout.Alignment.ALIGN_CENTER;
                        break;
                    default:
                        align = Layout.Alignment.ALIGN_NORMAL;
                        break;
                }
            } else if (TextUtils.equals(style.getKey(), "color")) {
                color = Constants.parseColor(style.getValue(), color);
            }
        }
        spannableString.setSpan(new CustomTypeface(magFont.getTypeFace(tagEnum, boldFont, mediumFont, regularFont), color), start, start + str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AlignmentSpan.Standard(align), start, start + str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        if (tagEnum == HtmlTagEnum.a && !TextUtils.isEmpty(content.getHref())) {
//            spannableString.setSpan(new CustomClick(content.getHref()), start, start + str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        }

    }



}
