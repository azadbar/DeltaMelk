package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class ProfileImageResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("FullImagePath")
    private String FullImagePath;
    @SerializedName("FullThumbnailImagePath")
    private String FullThumbnailImagePath;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public String getFullImagePath() {
        return FullImagePath;
    }

    public String getFullThumbnailImagePath() {
        return FullThumbnailImagePath;
    }
}
