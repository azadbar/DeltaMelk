package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DemandRegisterReq {

    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("locationId")
    private int locationId;
    @SerializedName("regionId")
    private ArrayList<Integer> regionId;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("email")
    private String email;
    @SerializedName("area")
    private int area;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("description")
    private String description;

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }


    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public void setRentOrMetric(long rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public int getLocationId() {
        return locationId;
    }


    public String getFullName() {
        return fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public int getArea() {
        return area;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public String getDescription() {
        return description;
    }

    public ArrayList<Integer> getRegionId() {
        return regionId;
    }

    public void setRegionId(ArrayList<Integer> regionId) {
        this.regionId = regionId;
    }
}
