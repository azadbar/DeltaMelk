package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.DepositDetailByCodeReq;
import ir.delta.delta.service.ResponseModel.DetailEstateResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class DetailEstateByCodeService {


    private static DetailEstateByCodeService detailEstateByCodeService;

    public static DetailEstateByCodeService getInstance() {
        if (detailEstateByCodeService == null) {
            detailEstateByCodeService = new DetailEstateByCodeService();
        }
        return detailEstateByCodeService;
    }


    public void getDepositDetailByCode(final Resources res, DepositDetailByCodeReq req, final ResponseListener<DetailEstateResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).detailEstateByCode(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    DetailEstateResponse response = gson.fromJson(mJson, DetailEstateResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
