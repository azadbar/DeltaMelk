package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DemandRegisterResponse {


    @SerializedName("enOrderId")
    private String enOrderId;
    @SerializedName("orderCode")
    private int orderCode;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;
    @SerializedName("needPayment")
    private boolean needPayment;
    @SerializedName("orderTitle")
    private String orderTitle;
    @SerializedName("orderPrice")
    private String orderPrice;

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public boolean isNeedPayment() {
        return needPayment;
    }

    public void setNeedPayment(boolean needPayment) {
        this.needPayment = needPayment;
    }

    public String getEnOrderId() {
        return enOrderId;
    }

    public void setEnOrderId(String enOrderId) {
        this.enOrderId = enOrderId;
    }

    public int getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(int orderCode) {
        this.orderCode = orderCode;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }
}
