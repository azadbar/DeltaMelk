package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MagStyle implements Serializable {

    @SerializedName("key")
    private final String key;
    @SerializedName("value")
    private final String value;


    public MagStyle(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
