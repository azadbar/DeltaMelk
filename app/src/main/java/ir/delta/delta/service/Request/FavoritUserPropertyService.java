package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.FavoritePropertyReq;
import ir.delta.delta.service.ResponseModel.FavoritePropertyResponse;
import ir.delta.delta.service.ResponseModel.myEstate.EstateActionResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class FavoritUserPropertyService {


    private static FavoritUserPropertyService favoritUserPropertyService;

    public static FavoritUserPropertyService getInstance() {
        if (favoritUserPropertyService == null) {
            favoritUserPropertyService = new FavoritUserPropertyService();
        }
        return favoritUserPropertyService;
    }


    public void favoritUserProperty(Context context, final Resources res, FavoritePropertyReq req, final ResponseListener<FavoritePropertyResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).favoriteUserProperty(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    FavoritePropertyResponse response = gson.fromJson(mJson, FavoritePropertyResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
