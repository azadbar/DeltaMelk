package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

public class ValueItem{

	@SerializedName("value")
	private String value;

	@SerializedName("key")
	private String key;

	@SerializedName("image")
	private String image;

	@SerializedName("isMobile")
	private boolean isMobile;

	@SerializedName("type")
	private String type;

	public String getValue(){
		return value;
	}

	public String getKey(){
		return key;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean getIsMobile() {
		return isMobile;
	}

	public void setIsMobile(boolean isMobile) {
		this.isMobile = isMobile;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}