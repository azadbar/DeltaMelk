package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class EditEstateReq {

    @SerializedName("id")
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
