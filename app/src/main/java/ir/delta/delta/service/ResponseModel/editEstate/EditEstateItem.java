package ir.delta.delta.service.ResponseModel.editEstate;

import com.google.gson.annotations.SerializedName;

public class EditEstateItem {

    @SerializedName("depositId")
    private int depositId;
    @SerializedName("encriptDepositId")
    private String encriptDepositId;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("cityId")
    private int locationId;
    @SerializedName("cityName")
    private String locaionName;
    @SerializedName("regionId")
    private int regionId;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("email")
    private String email;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("area")
    private int area;
    @SerializedName("totalArea")
    private int totalArea;
    @SerializedName("barGround")
    private int barGround;
    @SerializedName("transitWidth")
    private int transitWidth;
    @SerializedName("roomCount")
    private int roomCount;
    @SerializedName("floorNumber")
    private int floorNumber;
    @SerializedName("countFloor")
    private int countFloor;
    @SerializedName("yearBuilt")
    private int yearBuilt;
    @SerializedName("description")
    private String description;
    @SerializedName("address")
    private String address;
    @SerializedName("elevator")
    private boolean elevator;
    @SerializedName("parking")
    private boolean parking;
    @SerializedName("store")
    private boolean store;
    @SerializedName("loan")
    private boolean loan;
    @SerializedName("baseImagePath")
    private String baseImagePath;
    @SerializedName("imagesName")
    private String imagesName;// خرید-فروش-آپارتمان-تهران-منطقه-19-20-100235ca-31f7-465b-bfaf-5e7036679292.jpg,
    @SerializedName("isWithoutImageMode")
    private boolean isWithoutImageMode;

    public int getDepositId() {
        return depositId;
    }

    public void setDepositId(int depositId) {
        this.depositId = depositId;
    }

    public String getEncriptDepositId() {
        return encriptDepositId;
    }

    public void setEncriptDepositId(String encriptDepositId) {
        this.encriptDepositId = encriptDepositId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocaionName() {
        if (locaionName == null) {
            return "";
        }
        return locaionName;
    }

    public void setLocaionName(String locaionName) {
        this.locaionName = locaionName;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public void setRentOrMetric(long rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(int totalArea) {
        this.totalArea = totalArea;
    }

    public int getBarGround() {
        return barGround;
    }

    public void setBarGround(int barGround) {
        this.barGround = barGround;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public int getCountFloor() {
        return countFloor;
    }

    public void setCountFloor(int countFloor) {
        this.countFloor = countFloor;
    }

    public int getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(int yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isElevator() {
        return elevator;
    }

    public void setElevator(boolean elevator) {
        this.elevator = elevator;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public boolean isStore() {
        return store;
    }

    public void setStore(boolean store) {
        this.store = store;
    }

    public boolean isLoan() {
        return loan;
    }

    public void setLoan(boolean loan) {
        this.loan = loan;
    }

    public String getBaseImagePath() {
        return baseImagePath;
    }

    public void setBaseImagePath(String baseImagePath) {
        this.baseImagePath = baseImagePath;
    }

    public String getImagesName() {
        if (imagesName == null) {
            return "";
        }
        return imagesName;
    }

    public void setImagesName(String imagesName) {
        this.imagesName = imagesName;
    }

    public boolean isWithoutImageMode() {
        return isWithoutImageMode;
    }

    public void setWithoutImageMode(boolean withoutImageMode) {
        isWithoutImageMode = withoutImageMode;
    }

    public int getTransitWidth() {
        return transitWidth;
    }
}
