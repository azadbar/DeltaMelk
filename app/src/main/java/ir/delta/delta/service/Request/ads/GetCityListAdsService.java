package ir.delta.delta.service.Request.ads;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.ads.CityResponse;
import ir.delta.delta.service.ResponseModel.ads.ProvinceResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class GetCityListAdsService {


    private static GetCityListAdsService getCityListAdsService;

    public static GetCityListAdsService getInstance() {
        if (getCityListAdsService == null) {
            getCityListAdsService = new GetCityListAdsService();
        }
        return getCityListAdsService;
    }


    public void getCityListAds(final Resources res, int provinceId,int groupId, final ResponseListener<CityResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getCityList(provinceId,groupId), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    CityResponse response = gson.fromJson(mJson, CityResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
