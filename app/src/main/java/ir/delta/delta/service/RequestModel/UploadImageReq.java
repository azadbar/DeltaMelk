package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class UploadImageReq {

    @SerializedName("path")
    private String path; // should be 8
    @SerializedName("preImgName")
    private String preImgName; // should be format -> cityName-ContractTypeName-ثبت-ملک-PropertyName
    @SerializedName("ImageBase64")
    private String ImageBase64;
    @SerializedName("thmbWidth")
    private int thmbWidth;
    @SerializedName("thmbHeight")
    private int thmbHeight;
    @SerializedName("imgNumber")
    private int imgNumber;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPreImgName() {
        return preImgName;
    }

    public void setPreImgName(String preImgName) {
        this.preImgName = preImgName;
    }

    public String getImageBase64() {
        return ImageBase64;
    }

    public void setImageBase64(String imageBase64) {
        ImageBase64 = imageBase64;
    }

    public int getThmbWidth() {
        return thmbWidth;
    }

    public void setThmbWidth(int thmbWidth) {
        this.thmbWidth = thmbWidth;
    }

    public int getThmbHeight() {
        return thmbHeight;
    }

    public void setThmbHeight(int thmbHeight) {
        this.thmbHeight = thmbHeight;
    }

    public int getImgNumber() {
        return imgNumber;
    }

    public void setImgNumber(int imgNumber) {
        this.imgNumber = imgNumber;
    }
}
