package ir.delta.delta.service.ResponseModel.myEstate;

import com.google.gson.annotations.SerializedName;

public class EstateActionResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("depositId")
    private int depositId;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public int getDepositId() {
        return depositId;
    }


}
