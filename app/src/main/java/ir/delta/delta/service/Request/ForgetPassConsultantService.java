package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.ForgetPassReq;
import ir.delta.delta.service.ResponseModel.profile.ForgetPassResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class ForgetPassConsultantService {


    private static ForgetPassConsultantService forgetPassConsultantService;

    public static ForgetPassConsultantService getInstance() {
        if (forgetPassConsultantService == null) {
            forgetPassConsultantService = new ForgetPassConsultantService();
        }
        return forgetPassConsultantService;
    }


    public void getForgetPasswrd(final Resources res, ForgetPassReq req, final ResponseListener<ForgetPassResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).forgetPass(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    ForgetPassResponse response = gson.fromJson(mJson, ForgetPassResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
