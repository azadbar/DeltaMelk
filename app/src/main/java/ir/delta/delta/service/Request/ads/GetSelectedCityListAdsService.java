package ir.delta.delta.service.Request.ads;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.ads.CityResponse;
import ir.delta.delta.service.ResponseModel.ads.SelectedListAdsResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class GetSelectedCityListAdsService {


    private static GetSelectedCityListAdsService selectedGetCityListAdsService;

    public static GetSelectedCityListAdsService getInstance() {
        if (selectedGetCityListAdsService == null) {
            selectedGetCityListAdsService = new GetSelectedCityListAdsService();
        }
        return selectedGetCityListAdsService;
    }


    public void getSelectedCityListAds(final Resources res,  final ResponseListener<SelectedListAdsResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getSelectedList(), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    SelectedListAdsResponse response = gson.fromJson(mJson, SelectedListAdsResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
