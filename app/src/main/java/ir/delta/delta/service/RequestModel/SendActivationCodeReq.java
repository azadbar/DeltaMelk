package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class SendActivationCodeReq {

    @SerializedName("userName")
    private String userName;

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getUserName() {
        return userName;
    }
}
