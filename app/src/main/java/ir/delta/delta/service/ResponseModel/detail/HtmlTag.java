package ir.delta.delta.service.ResponseModel.detail;

import android.net.Uri;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.service.ResponseModel.TableVisibility;
import ir.delta.delta.service.ResponseModel.mag.NodeContent;
import ir.delta.delta.service.ResponseModel.mag.NodeImage;

public class HtmlTag {

    @SerializedName("parentNodeName")
    private String parentNodeName;
    @SerializedName("nodeName")
    private String nodeName;

    @SerializedName("text")
    private String text;
    @SerializedName("src")
    private String src;

    @SerializedName("width")
    private float width;
    @SerializedName("height")
    private float height;
    @SerializedName("table")
    private ArrayList<ArrayList<String>> table;

    @SerializedName("visibility")
    private TableVisibility visibility;

    @SerializedName("columns")
    private int columns;

    @SerializedName("poster")
    private String poster;
    @SerializedName("type")
    private String type;

    @SerializedName("images")
    private  ArrayList<NodeImage>  images;
    // use local

    private String scriptHtml;


    public String getScriptHtml() {
        return scriptHtml;
    }

    public void setScriptHtml(String scriptHtml) {
        this.scriptHtml = scriptHtml;
    }


    public String getParentNodeName() {
        return parentNodeName;
    }

    public void setParentNodeName(String parentNodeName) {
        this.parentNodeName = parentNodeName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public ArrayList<ArrayList<String>> getTable() {
        return table;
    }

    public void setTable(ArrayList<ArrayList<String>> table) {
        this.table = table;
    }

    public TableVisibility getVisibility() {
        return visibility;
    }

    public void setVisibility(TableVisibility visibility) {
        this.visibility = visibility;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<NodeImage> getImages() {
        return images;
    }

    public void setImages(ArrayList<NodeImage> images) {
        this.images = images;
    }


    public void fillScriptRow() {
        if (!TextUtils.isEmpty(src) && !TextUtils.isEmpty(type)) {
            String srcReplace = src.replace("[", "").replace("]", "");
            Uri uri = Uri.parse(srcReplace);
            String divId = uri.getQueryParameter("datarnddiv");
            String html = "<div id=\"" + divId + "\">\n" +
                    "<script type=\"text/JavaScript\" src=\"" + src + "\"></script>\n" +
                    "</div>";
            String webData = "<!DOCTYPE \\\">\n" +
                    "    <html>\n" +
                    "      <body id=\"body\">  \n" +
                    html +
                    "      </body>\n" +
                    "    </html>";
            setScriptHtml(webData);
        }
    }
}
