package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class LoginMobileResponse {

    @SerializedName("useVerificationCode")
    private boolean useVerificationCode;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;

    public boolean isUseVerificationCode() {
        return useVerificationCode;
    }

    public void setUseVerificationCode(boolean useVerificationCode) {
        this.useVerificationCode = useVerificationCode;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
