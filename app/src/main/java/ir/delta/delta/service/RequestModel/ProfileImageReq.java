package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class ProfileImageReq {

    @SerializedName("imageBase64")
    private String imageBase64;
    @SerializedName("thmbWidth")
    private int thmbWidth;
    @SerializedName("thmbHeight")
    private int thmbHeight;

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    public int getThmbWidth() {
        return thmbWidth;
    }

    public void setThmbWidth(int thmbWidth) {
        this.thmbWidth = thmbWidth;
    }

    public int getThmbHeight() {
        return thmbHeight;
    }

    public void setThmbHeight(int thmbHeight) {
        this.thmbHeight = thmbHeight;
    }
}
