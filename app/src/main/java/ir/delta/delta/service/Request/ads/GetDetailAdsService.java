package ir.delta.delta.service.Request.ads;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.ads.DetailResponse;
import ir.delta.delta.service.ResponseModel.ads.SubGroupListResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class GetDetailAdsService {


    private static GetDetailAdsService getDetailAdsService;

    public static GetDetailAdsService getInstance() {
        if (getDetailAdsService == null) {
            getDetailAdsService = new GetDetailAdsService();
        }
        return getDetailAdsService;
    }


    public void getDetailAds(final Resources res, int infoId, int parentGroupId, final ResponseListener<DetailResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getDetailAds(infoId,parentGroupId), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    DetailResponse response = gson.fromJson(mJson, DetailResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
