package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Facility implements Parcelable {

    @SerializedName("typeName")
    private String typeName;
    @SerializedName("contentInfo")
    private ArrayList<ContentInfo> contentInfo;


    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public ArrayList<ContentInfo> getContentInfo() {
        return contentInfo;
    }

    public void setContentInfo(ArrayList<ContentInfo> contentInfo) {
        this.contentInfo = contentInfo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.typeName);
        dest.writeList(this.contentInfo);
    }

    public Facility() {
    }

    protected Facility(Parcel in) {
        this.typeName = in.readString();
        this.contentInfo = new ArrayList<ContentInfo>();
        in.readList(this.contentInfo, ContentInfo.class.getClassLoader());
    }

    public static final Parcelable.Creator<Facility> CREATOR = new Parcelable.Creator<Facility>() {
        @Override
        public Facility createFromParcel(Parcel source) {
            return new Facility(source);
        }

        @Override
        public Facility[] newArray(int size) {
            return new Facility[size];
        }
    };
}
