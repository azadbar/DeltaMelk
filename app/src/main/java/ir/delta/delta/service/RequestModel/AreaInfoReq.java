package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class AreaInfoReq {


    @SerializedName("areaId")
    private int areaId;

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }
}
