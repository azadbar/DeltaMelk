package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DepositReq {

    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("locationId")
    private int locationId;
    @SerializedName("regionId")
    private int regionId;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("email")
    private String email;
    @SerializedName("address")
    private String address;
    @SerializedName("area")
    private int area;
    @SerializedName("totalArea")
    private int totalArea;
    @SerializedName("groundWidth")
    private int groundWidth;
    @SerializedName("transitWidth")
    private int transitWidth;
    @SerializedName("floor")
    private int floor;
    @SerializedName("floorCount")
    private int floorCount;
    @SerializedName("roomCount")
    private int roomCount;
    @SerializedName("yearBuilt")
    private int yearBuilt;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("hasParking")
    private boolean hasParking;
    @SerializedName("hasStore")
    private boolean hasStore;
    @SerializedName("hasElevator")
    private boolean hasElevator;
    @SerializedName("hasLoan")
    private boolean hasLoan;
    @SerializedName("description")
    private String description;
    @SerializedName("withPayment")
    private boolean withPayment;
    @SerializedName("imagesListBase64")
    private ArrayList<String> imagesListBase64 = new ArrayList<>();


    //send to site from deltanet
    @SerializedName("deltanetDepositId")
    private long deltanetDepositId;

    @SerializedName("walletPayment")
    private boolean walletPayment;

    public boolean isWalletPayment() {
        return walletPayment;
    }

    public void setWalletPayment(boolean walletPayment) {
        this.walletPayment = walletPayment;
    }

    public long getDeltanetDepositId() {
        return deltanetDepositId;
    }

    public void setDeltanetDepositId(long deltanetDepositId) {
        this.deltanetDepositId = deltanetDepositId;
    }

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setTotalArea(int totalArea) {
        this.totalArea = totalArea;
    }

    public void setGroundWidth(int groundWidth) {
        this.groundWidth = groundWidth;
    }

    public void setTransitWidth(int transitWidth) {
        this.transitWidth = transitWidth;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public void setFloorCount(int floorCount) {
        this.floorCount = floorCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public void setYearBuilt(int yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public void setRentOrMetric(long rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public void setHasStore(boolean hasStore) {
        this.hasStore = hasStore;
    }

    public void setHasElevator(boolean hasElevator) {
        this.hasElevator = hasElevator;
    }

    public void setHasLoan(boolean hasLoan) {
        this.hasLoan = hasLoan;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setWithPayment(boolean withPayment) {
        this.withPayment = withPayment;
    }

    public void setImagesListBase64(ArrayList<String> imagesListBase64) {
        this.imagesListBase64 = new ArrayList<>();
        this.imagesListBase64.addAll(imagesListBase64);
    }

    public void resetImageList() {
        imagesListBase64.clear();
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public int getLocationId() {
        return locationId;
    }

    public int getRegionId() {
        return regionId;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public int getArea() {
        return area;
    }

    public int getTotalArea() {
        return totalArea;
    }

    public int getGroundWidth() {
        return groundWidth;
    }

    public int getTransitWidth() {
        return transitWidth;
    }

    public int getFloor() {
        return floor;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public int getYearBuilt() {
        return yearBuilt;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public boolean isHasStore() {
        return hasStore;
    }

    public boolean isHasElevator() {
        return hasElevator;
    }

    public boolean isHasLoan() {
        return hasLoan;
    }

    public String getDescription() {
        return description;
    }

    public boolean isWithPayment() {
        return withPayment;
    }

    public ArrayList<String> getImagesListBase64() {
        return imagesListBase64;
    }
}
