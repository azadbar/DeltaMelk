package ir.delta.delta.service.Request.wallet;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.PaymentReq;
import ir.delta.delta.service.ResponseModel.wallet.PaymentSendWalletResponse;
import ir.delta.delta.service.ResponseModel.wallet.RegisterWalletAmountResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class RegisterEsatetWalletService {


    private static RegisterEsatetWalletService registerEsatetWalletService;

    public static RegisterEsatetWalletService getInstance() {
        if (registerEsatetWalletService == null) {
            registerEsatetWalletService = new RegisterEsatetWalletService();
        }
        return registerEsatetWalletService;
    }


    public void registerEstateWallet(Context context, PaymentReq req, final Resources res, final ResponseListener<RegisterWalletAmountResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).registerEstateWallet(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    RegisterWalletAmountResponse response = gson.fromJson(mJson, RegisterWalletAmountResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
