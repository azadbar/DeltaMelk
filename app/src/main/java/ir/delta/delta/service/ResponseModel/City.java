package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class City {
    //شهر
    @SerializedName("id")
    private int id;
    @SerializedName("order")
    private int order;
    @SerializedName("parentId")
    private int parentId;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("isMainCity")
    private boolean isMainCity;
    @SerializedName("isCoastalCity")
    private boolean isCoastalCity;
    @SerializedName("locationFeatureLocalId")
    private int locationFeatureLocalId;

    //for ads section city
    @SerializedName("title")
    private String title;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("imagePath")
    private String imagePath;


    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getId() {
        return id;
    }

    public int getOrder() {
        return order;
    }

    public int getParentId() {
        return parentId;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public boolean isMainCity() {
        return isMainCity;
    }

    public boolean isCoastalCity() {
        return isCoastalCity;
    }

    public int getLocationFeatureLocalId() {
        return locationFeatureLocalId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMainCity(boolean mainCity) {
        isMainCity = mainCity;
    }

    public void setCoastalCity(boolean coastalCity) {
        isCoastalCity = coastalCity;
    }

    public void setLocationFeatureLocalId(int locationFeatureLocalId) {
        this.locationFeatureLocalId = locationFeatureLocalId;
    }
}
