package ir.delta.delta.service.Request.ads;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.ads.CityResponse;
import ir.delta.delta.service.ResponseModel.ads.RegionResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class GetRegionListAdsService {


    private static GetRegionListAdsService getRegionListAdsService;

    public static GetRegionListAdsService getInstance() {
        if (getRegionListAdsService == null) {
            getRegionListAdsService = new GetRegionListAdsService();
        }
        return getRegionListAdsService;
    }


    public void getRegionListAds(final Resources res,  int cityId,int groupId, final ResponseListener<RegionResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getRegionList(cityId,groupId), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    RegionResponse response = gson.fromJson(mJson, RegionResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
