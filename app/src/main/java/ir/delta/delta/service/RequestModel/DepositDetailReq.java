package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class DepositDetailReq {

    @SerializedName("enDeposiId")
    private String enDeposiId;
    @SerializedName("logedInUserTokenOrId")
    private String logedInUserTokenOrId;

    public String getEnDeposiId() {
        return enDeposiId;
    }

    public void setEnDeposiId(String enDeposiId) {
        this.enDeposiId = enDeposiId;
    }

    public String getLogedInUserTokenOrId() {
        return logedInUserTokenOrId;
    }

    public void setLogedInUserTokenOrId(String logedInUserTokenOrId) {
        this.logedInUserTokenOrId = logedInUserTokenOrId;
    }
}
