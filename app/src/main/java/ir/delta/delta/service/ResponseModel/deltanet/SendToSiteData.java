package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

public class SendToSiteData {

    @SerializedName("deltanetDepositId")
    private long deltanetDepositId;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("cityId")
    private int locationId;
    @SerializedName("cityName")
    private String locaionName;
    @SerializedName("regionId")
    private int regionId;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("email")
    private String email;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("area")
    private int area;
    @SerializedName("totalArea")
    private int totalArea;
    @SerializedName("barGround")
    private int barGround;
    @SerializedName("transitWidth")
    private int transitWidth;
    @SerializedName("roomCount")
    private int roomCount;
    @SerializedName("floorNumber")
    private int floorNumber;
    @SerializedName("floorCount")
    private int floorCount;
    @SerializedName("yearBuilt")
    private int yearBuilt;
    @SerializedName("description")
    private String description;
    @SerializedName("address")
    private String address;
    @SerializedName("elevator")
    private boolean elevator;
    @SerializedName("parking")
    private boolean parking;
    @SerializedName("store")
    private boolean store;
    @SerializedName("loan")
    private boolean loan;
    @SerializedName("preImgName")
    private String preImgName;

    public long getDeltanetDepositId() {
        return deltanetDepositId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public int getLocationId() {
        return locationId;
    }

    public String getLocaionName() {
        return locaionName;
    }

    public int getRegionId() {
        return regionId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public int getArea() {
        return area;
    }

    public int getTotalArea() {
        return totalArea;
    }

    public int getBarGround() {
        return barGround;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public int getYearBuilt() {
        return yearBuilt;
    }

    public String getDescription() {
        return description;
    }

    public String getAddress() {
        return address;
    }

    public boolean isElevator() {
        return elevator;
    }

    public boolean isParking() {
        return parking;
    }

    public boolean isStore() {
        return store;
    }

    public boolean isLoan() {
        return loan;
    }

    public String getPreImgName() {
        return preImgName;
    }

    public int getTransitWidth() {
        return transitWidth;
    }
}
