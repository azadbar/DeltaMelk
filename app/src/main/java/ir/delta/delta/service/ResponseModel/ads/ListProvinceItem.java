package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

public class ListProvinceItem{

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("longitude")
	private double longitude;

	public double getLatitude(){
		return latitude;
	}

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}

	public double getLongitude(){
		return longitude;
	}
}