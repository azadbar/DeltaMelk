package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class DepositDetailByCodeReq {

    @SerializedName("id")
    private int id;
    @SerializedName("logedInUserTokenOrId")
    private String logedInUserTokenOrId;

    public String getLogedInUserTokenOrId() {
        return logedInUserTokenOrId;
    }

    public void setLogedInUserTokenOrId(String logedInUserTokenOrId) {
        this.logedInUserTokenOrId = logedInUserTokenOrId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
