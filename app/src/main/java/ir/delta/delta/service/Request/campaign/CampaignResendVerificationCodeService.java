package ir.delta.delta.service.Request.campaign;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.campaign.CampaignResendVerificationCodeReq;
import ir.delta.delta.service.ResponseModel.Campaign.ResendVerificationCodeResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class CampaignResendVerificationCodeService {


    private static CampaignResendVerificationCodeService verificationCampaignService;

    public static CampaignResendVerificationCodeService getInstance() {
        if (verificationCampaignService == null) {
            verificationCampaignService = new CampaignResendVerificationCodeService();
        }
        return verificationCampaignService;
    }


    public void getResendVerificationCode(final Resources res, CampaignResendVerificationCodeReq req, final ResponseListener<ResendVerificationCodeResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).campaignResendVerificationCode(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    ResendVerificationCodeResponse response = gson.fromJson(mJson, ResendVerificationCodeResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
