package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Estate implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("encodedId")
    private String encodedId;
    @SerializedName("srcImage")
    private String srcImage;
    @SerializedName("imageCount")
    private int imageCount;
    @SerializedName("displayDate")
    private String displayDate;//12 روز پیش
    @SerializedName("showOfficeLogo")
    private boolean showOfficeLogo;
    @SerializedName("logoSrc")
    private String logoSrc;
    @SerializedName("propertyType")
    private String propertyType;//آپارتمان
    @SerializedName("contractType")
    private String contractType;//خرید
    @SerializedName("displayText")
    private String displayText;//167 متر، تهران منطقه 2،
    @SerializedName("displayTitle")
    private String displayTitle;//اتاق  / نوساز / ط
    @SerializedName("rentOrMetric")
    private long rentOrMetric;//Rial
    @SerializedName("rentOrMetricDisplayTitle")
    private String rentOrMetricDisplayTitle;//قیمت
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;//Rial
    @SerializedName("mortgageOrTotalDisplayTitle")
    private String mortgageOrTotalDisplayTitle;
    @SerializedName("address")
    private String address;//"گیشا ، گیشا"
    @SerializedName("location")
    private String location;//تهران منطقه 2
    @SerializedName("area")
    private String area;
    @SerializedName("isLiked")
    private boolean isLiked;

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }


//    private String fullName;
//    private String floor;

//    private String length;
//    private String room;
//    private String age;
//    private String totalArea;
//    private String description;


    public int getId() {
        return id;
    }

    public String getEncodedId() {
        if (encodedId == null) {
            return "";
        }
        return encodedId;
    }

    public String getSrcImage() {
        return srcImage;
    }

    public int getImageCount() {

        return imageCount;
    }

    public String getDisplayDate() {
        if (displayDate == null) {
            return "";
        }
        return displayDate;
    }

    public boolean isShowOfficeLogo() {
        return showOfficeLogo;
    }

    public String getLogoSrc() {
        return logoSrc;
    }

    public String getPropertyType() {
        if (propertyType == null) {
            return "";
        }
        return propertyType;
    }

    public String getContractType() {
        if (contractType == null) {
            return "";
        }
        return contractType;
    }

    public String getDisplayText() {
        if (displayText == null) {
            return "";
        }
        return displayText;
    }

    public String getDisplayTitle() {
        if (displayTitle == null) {
            return "";
        }
        return displayTitle;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public String getRentOrMetricDisplayTitle() {
        return rentOrMetricDisplayTitle;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public String getMortgageOrTotalDisplayTitle() {
        if (mortgageOrTotalDisplayTitle == null) {
            return "";
        }
        return mortgageOrTotalDisplayTitle;
    }

    public String getAddress() {
        if (address == null) {
            return "";
        }
        return address;
    }

    public String getLocation() {

        if (location == null) {
            return "";
        }
        return location;
    }

    public String getArea() {
        if (area == null) {
            return "";
        }
        return area;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEncodedId(String encodedId) {
        this.encodedId = encodedId;
    }

    public void setSrcImage(String srcImage) {
        this.srcImage = srcImage;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public void setShowOfficeLogo(boolean showOfficeLogo) {
        this.showOfficeLogo = showOfficeLogo;
    }

    public void setLogoSrc(String logoSrc) {
        this.logoSrc = logoSrc;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    public void setDisplayTitle(String displayTitle) {
        this.displayTitle = displayTitle;
    }

    public void setRentOrMetric(long rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public void setRentOrMetricDisplayTitle(String rentOrMetricDisplayTitle) {
        this.rentOrMetricDisplayTitle = rentOrMetricDisplayTitle;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public void setMortgageOrTotalDisplayTitle(String mortgageOrTotalDisplayTitle) {
        this.mortgageOrTotalDisplayTitle = mortgageOrTotalDisplayTitle;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.encodedId);
        dest.writeString(this.srcImage);
        dest.writeInt(this.imageCount);
        dest.writeString(this.displayDate);
        dest.writeByte(this.showOfficeLogo ? (byte) 1 : (byte) 0);
        dest.writeString(this.logoSrc);
        dest.writeString(this.propertyType);
        dest.writeString(this.contractType);
        dest.writeString(this.displayText);
        dest.writeString(this.displayTitle);
        dest.writeLong(this.rentOrMetric);
        dest.writeString(this.rentOrMetricDisplayTitle);
        dest.writeLong(this.mortgageOrTotal);
        dest.writeString(this.mortgageOrTotalDisplayTitle);
        dest.writeString(this.address);
        dest.writeString(this.location);
        dest.writeString(this.area);
    }

    public Estate() {
    }

    protected Estate(Parcel in) {
        this.id = in.readInt();
        this.encodedId = in.readString();
        this.srcImage = in.readString();
        this.imageCount = in.readInt();
        this.displayDate = in.readString();
        this.showOfficeLogo = in.readByte() != 0;
        this.logoSrc = in.readString();
        this.propertyType = in.readString();
        this.contractType = in.readString();
        this.displayText = in.readString();
        this.displayTitle = in.readString();
        this.rentOrMetric = in.readLong();
        this.rentOrMetricDisplayTitle = in.readString();
        this.mortgageOrTotal = in.readLong();
        this.mortgageOrTotalDisplayTitle = in.readString();
        this.address = in.readString();
        this.location = in.readString();
        this.area = in.readString();
    }

    public static final Parcelable.Creator<Estate> CREATOR = new Parcelable.Creator<Estate>() {
        @Override
        public Estate createFromParcel(Parcel source) {
            return new Estate(source);
        }

        @Override
        public Estate[] newArray(int size) {
            return new Estate[size];
        }
    };
}
