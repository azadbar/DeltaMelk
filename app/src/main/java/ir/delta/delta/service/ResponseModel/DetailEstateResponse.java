package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailEstateResponse {

    @SerializedName("modelStateErrors")
    private String modelStateErrors;
    @SerializedName("failureItemList")
    public ArrayList<FailureBugReport> failureItemList;
    @SerializedName("depositDetailData")
    private DetailEstateData depositDetailData;
    @SerializedName("isFavoriteDeposit")
    private boolean isFavoriteDeposit;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;

    public boolean isFavoriteDeposit() {
        return isFavoriteDeposit;
    }

    public void setFavoriteDeposit(boolean favoriteDeposit) {
        isFavoriteDeposit = favoriteDeposit;
    }

    public void setModelStateErrors(String modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public String getModelStateErrors() {
        return modelStateErrors;
    }

    public void setDetailEstateData(DetailEstateData detailEstateData) {
        this.depositDetailData = detailEstateData;
    }

    public DetailEstateData getDetailEstateData() {
        return depositDetailData;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<FailureBugReport> getFailureItemList() {
        if (failureItemList == null) {
            return new ArrayList<>();
        }
        return failureItemList;
    }

    public void setFailureItemList(ArrayList<FailureBugReport> failureItemList) {
        this.failureItemList = failureItemList;
    }

    @Override
    public String toString() {
        return
                "DetailEstateResponse{" +
                        "modelStateErrors = '" + modelStateErrors + '\'' +
                        ",detailEstateData = '" + depositDetailData + '\'' +
                        ",successed = '" + successed + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }
}
