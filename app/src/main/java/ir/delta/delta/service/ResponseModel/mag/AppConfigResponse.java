package ir.delta.delta.service.ResponseModel.mag;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AppConfigResponse implements Parcelable {


    @SerializedName("hasChanged")
    private boolean hasChanged;
    @SerializedName("isForce")
    private boolean isForce;
    @SerializedName("versionName")
    private String versionName;
    @SerializedName("downloadUrl")
    private String downloadUrl;
    @SerializedName("features")
    private ArrayList<String> features;
    @SerializedName("showButtonEstate")
    private boolean showButtonEstate;
    @SerializedName("isCampaign")
    private boolean isCampaign;

    //check link for share app link
    @SerializedName("shareAppLink")
    private String shareAppLink;

    public AppConfigResponse() {
    }

    public AppConfigResponse(Parcel in) {
        hasChanged = in.readByte() != 0;
        isForce = in.readByte() != 0;
        versionName = in.readString();
        downloadUrl = in.readString();
        features = in.createStringArrayList();
        showButtonEstate = in.readByte() != 0;
        isCampaign = in.readByte() != 0;
        shareAppLink = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (hasChanged ? 1 : 0));
        dest.writeByte((byte) (isForce ? 1 : 0));
        dest.writeString(versionName);
        dest.writeString(downloadUrl);
        dest.writeStringList(features);
        dest.writeByte((byte) (showButtonEstate ? 1 : 0));
        dest.writeByte((byte) (isCampaign ? 1 : 0));
        dest.writeString(shareAppLink);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AppConfigResponse> CREATOR = new Creator<AppConfigResponse>() {
        @Override
        public AppConfigResponse createFromParcel(Parcel in) {
            return new AppConfigResponse(in);
        }

        @Override
        public AppConfigResponse[] newArray(int size) {
            return new AppConfigResponse[size];
        }
    };

    public boolean isHasChanged() {
        return hasChanged;
    }

    public void setHasChanged(boolean hasChanged) {
        this.hasChanged = hasChanged;
    }

    public boolean isForce() {
        return isForce;
    }

    public void setForce(boolean force) {
        isForce = force;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public ArrayList<String> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<String> features) {
        this.features = features;
    }

    public boolean isShowButtonEstate() {
        return showButtonEstate;
    }

    public void setShowButtonEstate(boolean showButtonEstate) {
        this.showButtonEstate = showButtonEstate;
    }

    public boolean isCampaign() {
        return isCampaign;
    }

    public void setCampaign(boolean campaign) {
        isCampaign = campaign;
    }

    public String getShareAppLink() {
        return shareAppLink;
    }

    public void setShareAppLink(String shareAppLink) {
        this.shareAppLink = shareAppLink;
    }
}
