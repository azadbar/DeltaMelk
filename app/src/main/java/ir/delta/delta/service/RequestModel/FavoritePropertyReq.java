package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class FavoritePropertyReq {

    @SerializedName("id")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
