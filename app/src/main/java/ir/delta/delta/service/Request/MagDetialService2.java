package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.ResponseModel.detail.PostDetailResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class MagDetialService2 {


    private static MagDetialService2 magDetialService2;

    public static MagDetialService2 getInstance() {
        if (magDetialService2 == null) {
            magDetialService2 = new MagDetialService2();
        }
        return magDetialService2;
    }

    public void magDetail(final Resources res, int id,String token, final ResponseListener<PostDetailResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient_MAG().create(ReqInterface.class).magDetail2(id,token), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    PostDetailResponse response = gson.fromJson(mJson, PostDetailResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
