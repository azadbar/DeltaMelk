package ir.delta.delta.service;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.grapesnberries.curllogger.CurlLoggerInterceptor;
import com.ihsanbal.logging.LoggingInterceptor;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import ir.delta.delta.BuildConfig;
import ir.delta.delta.util.Constants;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


    public static Retrofit getClient() {
        String username = "DeltaApi";
        String password = "12345";
        final String basicAuth = "Basic " + new String(Base64.encode((username + ":" + password).getBytes(), Base64.NO_WRAP));
        OkHttpClient.Builder unsafeOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        OkHttpClient myClient = unsafeOkHttpClient.addInterceptor(getLoggingInterceptor(basicAuth))
                .addInterceptor(new CurlLoggerInterceptor())
                .connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();

        return new Builder().baseUrl(Constants.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .client(myClient).build();
    }

    public static Retrofit getClient(Context context) {
        String username = "DeltaApi";
        String password = "12345";
        final String basicAuth = "Basic " + new String(Base64.encode((username + ":" + password).getBytes(), Base64.NO_WRAP));
        OkHttpClient.Builder unsafeOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        OkHttpClient myClient = unsafeOkHttpClient.addInterceptor(getLoggingInterceptor(context, basicAuth))
                .addInterceptor(new CurlLoggerInterceptor())
                .connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();
        return new Builder().baseUrl(Constants.URL_API).addConverterFactory(GsonConverterFactory.create()).client(myClient).build();
    }


    public static Retrofit getClient_MAG() {
        String username = "DeltaApi";
        String password = "12345";
        final String basicAuth = "Basic " + new String(Base64.encode((username + ":" + password).getBytes(), Base64.NO_WRAP));
        OkHttpClient.Builder unsafeOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        OkHttpClient myClient = unsafeOkHttpClient.addInterceptor(getLoggingInterceptorMag(basicAuth))
                .addInterceptor(new CurlLoggerInterceptor())
                .connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();
        return new Builder().baseUrl(Constants.URL_API_MAG).addConverterFactory(GsonConverterFactory.create()).client(myClient).build();
    }

    public static Retrofit getClient_MAG_Timeout() {
        String username = "DeltaApi";
        String password = "12345";
        final String basicAuth = "Basic " + new String(Base64.encode((username + ":" + password).getBytes(), Base64.NO_WRAP));
        OkHttpClient.Builder unsafeOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        OkHttpClient myClient = unsafeOkHttpClient.addInterceptor(getLoggingInterceptorMag(basicAuth))
                .addInterceptor(new CurlLoggerInterceptor())
                .connectTimeout(2, TimeUnit.SECONDS).readTimeout(2, TimeUnit.SECONDS).build();
        return new Builder().baseUrl(Constants.URL_API_MAG).addConverterFactory(GsonConverterFactory.create()).client(myClient).build();
    }


    private static LoggingInterceptor getLoggingInterceptor(String basicAuth) {
        return new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(com.ihsanbal.logging.Level.BASIC)
                .log(Platform.INFO)
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("Authorization", basicAuth)
                .addHeader("ApiAgent", BuildConfig.isCounsultant ? "consultant" : "mag")
                .addHeader("Market", BuildConfig.Market)
                .addHeader("GeneralUserResponse-Agent", "ir.delta.delta")
                .request("request")
                .response("response")
                .build();
    }

    private static LoggingInterceptor getLoggingInterceptor(Context context, String basicAuth) {
        if (Constants.getUser(context).isGeneral()) {
            return new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(com.ihsanbal.logging.Level.BASIC)
                    .log(Platform.INFO)
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .addHeader("Authorization", basicAuth)
                    .addHeader("ApiAgent", BuildConfig.isCounsultant ? "consultant" : "mag")
                    .addHeader("Market", BuildConfig.Market)
                    .addHeader("GeneralUserResponse-Agent", "ir.delta.delta")
                    .addHeader("UserToken", Constants.getUser(context).getUserToken())
                    .request("request")
                    .response("response")
                    .build();
        } else {
            return new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(com.ihsanbal.logging.Level.BASIC)
                    .log(Platform.INFO)
                    .addHeader("Content-Type", "application/json; charset=utf-8")
                    .addHeader("Authorization", basicAuth)
                    .addHeader("ApiAgent", BuildConfig.isCounsultant ? "consultant" : "mag")
                    .addHeader("Market", BuildConfig.Market)
                    .addHeader("GeneralUserResponse-Agent", "ir.delta.delta")
                    .addHeader("UserId", String.valueOf(Constants.getUser(context).getUserId()))
                    .request("request")
                    .response("response")
                    .build();
        }

    }


    private static LoggingInterceptor getLoggingInterceptorMag(String basicAuth) {
        return new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(com.ihsanbal.logging.Level.BASIC)
                .log(Platform.INFO)
                .addHeader("Content-Type", "application/json; charset=utf-8")
                .addHeader("WWW-Authenticate", basicAuth)
                .addHeader("Api-Agent", BuildConfig.isCounsultant ? "consultant" : "mag")
                .addHeader("Market", BuildConfig.Market)
                .addHeader("GeneralUserResponse-Agent", "ir.delta.delta")
                .request("request")
                .response("response")
                .build();
    }



}
