package ir.delta.delta.service.ResponseModel;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.Model.EstateSpec;
import ir.delta.delta.Model.Pattern;
import ir.delta.delta.R;
import ir.delta.delta.bottomNavigation.search.LocationInterface;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.InputTypeEnum;
import ir.delta.delta.enums.PropertyTypeEnum;
import ir.delta.delta.enums.RangeEnum;

public class PropertyType extends LocationInterface implements Parcelable {

    //    private int propertyTypeId;
    @SerializedName("propertyTypeLocalId")
    private final int propertyTypeLocalId;
    @SerializedName("propertyTypeName")
    private String propertyTypeName;
    @SerializedName("searchParamInfos")
    private final SearchParamInfo searchParamInfos;

    public void setPropertyTypeName(String propertyTypeName) {
        this.propertyTypeName = propertyTypeName;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public String getPropertyTypeName() {
        if (propertyTypeName == null) {
            return "";
        }
        return propertyTypeName;
    }

    public SearchParamInfo getSearchParamInfos() {
        return searchParamInfos;
    }


    public RangeEnum getPriceRange(SearchParam spValue) {
        if (searchParamInfos == null) {
            return RangeEnum.MIDDLE;
        }
        return getRangeEnum(spValue, searchParamInfos.getObjMortgageOrTotals());
    }


    public RangeEnum getRentRange(SearchParam spValue) {
        if (searchParamInfos == null) {
            return RangeEnum.MIDDLE;
        }
        return getRangeEnum(spValue, searchParamInfos.getObjRentOrMetrics());
    }

    public RangeEnum getAreaRange(SearchParam spValue) {
        if (searchParamInfos == null) {
            return RangeEnum.MIDDLE;
        }
        return getRangeEnum(spValue, searchParamInfos.getObjPropertyAreas());
    }


    private RangeEnum getRangeEnum(SearchParam spValue, ArrayList<SearchParam> searchParams) {
        if (spValue != null) {
            if (searchParams != null) {
                for (int i = 0; i < searchParams.size(); i++) {
                    if (spValue.getValue() == searchParams.get(i).getValue()) {
                        return RangeEnum.getEnum(i, searchParams.size());
                    }
                }
            }
        }
        return RangeEnum.MIDDLE;
    }

    public boolean isShowTotalAreaFilter() {
        return searchParamInfos != null && searchParamInfos.isShowTotalAreaFilter();
    }

    public ArrayList<SearchParam> getPriceList() {
        if (searchParamInfos != null && searchParamInfos.getObjMortgageOrTotals() != null) {
            return searchParamInfos.getObjMortgageOrTotals();
        }
        return new ArrayList<>();
    }

    public ArrayList<SearchParam> getRentList() {
        if (searchParamInfos != null && searchParamInfos.getObjRentOrMetrics() != null) {
            return searchParamInfos.getObjRentOrMetrics();
        }
        return new ArrayList<>();
    }

    public ArrayList<SearchParam> getMeterList() {
        if (searchParamInfos != null && searchParamInfos.getObjPropertyAreas() != null) {
            return searchParamInfos.getObjPropertyAreas();
        }
        return new ArrayList<>();
    }


    public Pattern getPattern(ContractTypeEnum contractTypeEnum, Resources res) {
        Pattern pattern = new Pattern();

        //
        String descHint = "";
        switch (PropertyTypeEnum.propertyTypeEnum(propertyTypeLocalId)) {
            case Apartment:
            case ApartmentOffice:
            case ApartmentBoth:
                descHint = setApartmentPattern(pattern, res);
                break;
//            case Industrial:
//            case Others:
            case Cultivation:
            case Factory:
            case Livestock:
                descHint = setIndustrial(pattern, res);
                break;
            case Garden:
                descHint = setGarden(pattern, res);
                break;
            case Ground:
                descHint = setGround(pattern, res);
                break;
            case MojtamaApatmaniMostaghelat:
                descHint = setComplex(pattern, res);
                break;
            case Shop:
                descHint = setShop(pattern, res);
                break;
            case Villa:
                descHint = setVilla(pattern, res);
                break;
            case KaneVilaKolangi:
                descHint = setClutterVilla(pattern, res);
                break;
        }
        //
        if (contractTypeEnum == ContractTypeEnum.RENT) {
            pattern.addEstateSpec(new EstateSpec(InputTypeEnum.MORTGAGE_PRICE, res.getString(R.string.mortgage_text), res.getString(R.string.toman), false, res.getString(R.string.the_minimum_amount_is_one_thousend, res.getString(R.string.mortgage_text) + " 1000 ")));
            pattern.addEstateSpec(new EstateSpec(InputTypeEnum.RENT_PRICE, res.getString(R.string.rent_text), res.getString(R.string.toman), false, res.getString(R.string.the_minimum_amount_is_one_thousend, res.getString(R.string.rent_text) + " 1000")));
        } else {
            pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_PRICE, res.getString(R.string.price_text), res.getString(R.string.toman), true, res.getString(R.string.the_minimum_amount_is_one_thousend, res.getString(R.string.price_text) + " 1000")));
        }

        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.ADDRESS, res.getString(R.string.main_street), null, true, res.getString(R.string.main_street_hint)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.DESCRIPTION, res.getString(R.string.description_text), null, false, descHint));
        return pattern;
    }


    //آپارتمان مسکونی
    private String setApartmentPattern(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.space_text), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.FLOOR, res.getString(R.string.floor_text), null, true, res.getString(R.string.example_two_floor)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.ROOM_COUNT, res.getString(R.string.count_room_text), null, true, res.getString(R.string.example_two_room)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.YEAR_BUILT, res.getString(R.string.age_building_text), res.getString(R.string.year), true, res.getString(R.string.new_build)));
        pattern.setHasParking(true);
        pattern.setHasElevator(true);
        pattern.setHasWarehouse(true);
        pattern.setHasLoan(true);
        return res.getString(R.string.description_apartmant);
    }

    //الگوی صنعتی و زراعی و ...
    private String setIndustrial(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.land_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.building_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_industrial);
    }

    //الگوی باغ و باغچه
    private String setGarden(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.total_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_garden);
    }

    //الگوی زمین
    private String setGround(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.land_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.GROUND_WIDTH, res.getString(R.string.ground_width), res.getString(R.string.meter), true, res.getString(R.string.meter)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TRANSIT_WIDTH, res.getString(R.string.transit_width), res.getString(R.string.meter), true, res.getString(R.string.meter)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_ground);
    }

    //الگوی مجتمع آپارتمان مسکونی
    private String setComplex(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.land_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.building_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.FLOOR_COUNT, res.getString(R.string.count_floor_text), null, true, res.getString(R.string.example_floor_count)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.YEAR_BUILT, res.getString(R.string.age_building_text), res.getString(R.string.year), true, res.getString(R.string.new_build)));
        pattern.setHasParking(true);
        pattern.setHasElevator(true);
        pattern.setHasWarehouse(true);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_comlex);
    }

    //الگوی تجاری و مغازه
    private String setShop(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.space_text), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.GROUND_WIDTH, res.getString(R.string.shop_width), res.getString(R.string.meter), true, res.getString(R.string.meter)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_shop);
    }

    // الگوی خانه ویلا
    private String setVilla(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.land_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.building_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.ROOM_COUNT, res.getString(R.string.count_room_text), null, true, res.getString(R.string.example_two_room)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.YEAR_BUILT, res.getString(R.string.age_building_text), res.getString(R.string.year), true, res.getString(R.string.new_build)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_villa);
    }

    //الگوی خانه ویلا کلنگی
    private String setClutterVilla(Pattern pattern, Resources res) {
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TOTAL_AREA, res.getString(R.string.land_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.AREA, res.getString(R.string.building_area), res.getString(R.string.meter_square), true, res.getString(R.string.meter_square)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.GROUND_WIDTH, res.getString(R.string.ground_width), res.getString(R.string.meter), true, res.getString(R.string.meter)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.ROOM_COUNT, res.getString(R.string.count_room_text), null, true, res.getString(R.string.example_two_room)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.YEAR_BUILT, res.getString(R.string.age_building_text), res.getString(R.string.year), true, res.getString(R.string.new_build)));
        pattern.addEstateSpec(new EstateSpec(InputTypeEnum.TRANSIT_WIDTH, res.getString(R.string.transit_width), res.getString(R.string.meter), true, res.getString(R.string.meter)));
        pattern.setHasParking(false);
        pattern.setHasElevator(false);
        pattern.setHasWarehouse(false);
        pattern.setHasLoan(false);
        return res.getString(R.string.description_clutter_villa);
    }


    public boolean hasArea() {
        switch (PropertyTypeEnum.propertyTypeEnum(propertyTypeLocalId)) {
            case Cultivation:
            case Factory:
            case Livestock:
            case Garden:
                return false;
            default:
                return true;
        }
    }

    public boolean isImageRequired() {
        switch (PropertyTypeEnum.propertyTypeEnum(propertyTypeLocalId)) {
            case Apartment:
            case ApartmentOffice:
            case ApartmentBoth:
            case MojtamaApatmaniMostaghelat:
                return true;
            default:
                return false;
        }
    }
    @Override
    public String getTitle() {

        return getPropertyTypeName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public int getId() {
        return getPropertyTypeLocalId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.propertyTypeLocalId);
        dest.writeString(this.propertyTypeName);
        dest.writeParcelable(this.searchParamInfos, flags);
    }


    private PropertyType(Parcel in) {
        this.propertyTypeLocalId = in.readInt();
        this.propertyTypeName = in.readString();
        this.searchParamInfos = in.readParcelable(SearchParamInfo.class.getClassLoader());
    }

    public static final Parcelable.Creator<PropertyType> CREATOR = new Parcelable.Creator<PropertyType>() {
        @Override
        public PropertyType createFromParcel(Parcel source) {
            return new PropertyType(source);
        }

        @Override
        public PropertyType[] newArray(int size) {
            return new PropertyType[size];
        }
    };


}
