package ir.delta.delta.service.Request.mag;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.mag.AppConfigResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class MagHomeService {


    private static MagHomeService magHomeCheckVersionService;

    public static MagHomeService getInstance() {
        if (magHomeCheckVersionService == null) {
            magHomeCheckVersionService = new MagHomeService();
        }
        return magHomeCheckVersionService;
    }


    public void getMagHome(final Resources res, int seed,boolean withTelegram, final ResponseListener<AppConfigResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient_MAG().create(ReqInterface.class).magHome2(seed,withTelegram), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    AppConfigResponse response = gson.fromJson(mJson, AppConfigResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
