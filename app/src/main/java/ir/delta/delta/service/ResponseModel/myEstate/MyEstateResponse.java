package ir.delta.delta.service.ResponseModel.myEstate;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyEstateResponse {


    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("searchResultCount")
    private int searchResultCount;
    @SerializedName("depositItems")
    private ArrayList<ConsultantEstate> depositItems;
    @SerializedName("statusList")
    private ArrayList<EstateStatus> statusList;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public int getSearchResultCount() {
        return searchResultCount;
    }

    public ArrayList<ConsultantEstate> getDepositItems() {
        return depositItems;
    }

    public ArrayList<EstateStatus> getStatusList() {
        return statusList;
    }

    public String getStatus(int id) {
        for (EstateStatus status : statusList) {
            if (Integer.parseInt(status.getValue()) == id) {
                return status.getText();
            }
        }
        return "";
    }
}
