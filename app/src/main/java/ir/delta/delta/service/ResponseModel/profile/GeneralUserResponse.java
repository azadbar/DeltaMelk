package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class GeneralUserResponse {

    @SerializedName("userDisplayName")
    private String userDisplayName;
    @SerializedName("locationId")
    private int locationId;
    @SerializedName("userToken")
    private String userToken;
    @SerializedName("successed")
    private boolean successed;

    public String getUserToken() {
        return userToken;
    }

    public boolean isSuccessed() {
        return successed;
    }


    public String getUserDisplayName() {
        return userDisplayName;
    }

    public int getLocationId() {
        return locationId;
    }
}
