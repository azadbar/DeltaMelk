package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class PaymentReq {

    @SerializedName("enOrderId")
    private String enOrderId;
    @SerializedName("atUserId")
    private String atUserId;

    public String getEnOrderId() {
        return enOrderId;
    }

    public void setEnOrderId(String enOrderId) {
        this.enOrderId = enOrderId;
    }

    public String getAtUserId() {
        return atUserId;
    }

    public void setAtUserId(String atUserId) {
        this.atUserId = atUserId;
    }
}
