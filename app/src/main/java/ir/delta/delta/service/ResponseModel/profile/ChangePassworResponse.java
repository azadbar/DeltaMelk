package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class ChangePassworResponse {

    @SerializedName("successed")
    private Boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("error")
    private String error;

    public Boolean getSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }
}
