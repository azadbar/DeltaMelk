package ir.delta.delta.service;

import android.content.Context;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import ir.delta.delta.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCallback<T> implements Callback<T> {

    private RequestListener<T> listener;
    private Context context;
    private Call<T> temCall;


    public RequestCallback(Context context, Call<T> temCall, RequestListener<T> listener) {
        this.listener = listener;
        this.context = context;
        this.temCall = temCall;

        temCall.enqueue(this);
    }

    @Override
    public void onResponse(@NotNull Call<T> call, @NotNull Response<T> response) {

        if (response.code() >= 200 && response.code() < 300) {
            try {
                listener.onResponse(response);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                JSONObject jsonObject = new JSONObject(response.errorBody().string());
                listener.onFailure(response.code(), jsonObject);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(@NotNull Call<T> call, @NotNull Throwable t) {
        if (BuildConfig.DEBUG && !TextUtils.isEmpty(t.getMessage())) {
            listener.onFailure(400, new JSONObject() );
        } else {

        }
    }
}
