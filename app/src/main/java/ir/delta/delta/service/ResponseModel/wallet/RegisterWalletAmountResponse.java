package ir.delta.delta.service.ResponseModel.wallet;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class RegisterWalletAmountResponse {

    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    @SerializedName("successed")
    private boolean successed;

    @SerializedName("message")
    private String message;

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}