package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.AreaInfoReq;
import ir.delta.delta.service.RequestModel.PaymentConfirmReq;
import ir.delta.delta.service.ResponseModel.AreaInfoResponse;
import ir.delta.delta.service.ResponseModel.PaymentConfirmResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class PaymentConfirmService {


    private static PaymentConfirmService paymentConfirmService;

    public static PaymentConfirmService getInstance() {
        if (paymentConfirmService == null) {
            paymentConfirmService = new PaymentConfirmService();
        }
        return paymentConfirmService;
    }


    public void getPaymentConfirm(final Resources res, PaymentConfirmReq req, final ResponseListener<PaymentConfirmResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getPaymentConfirm(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }
            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    PaymentConfirmResponse response = gson.fromJson(mJson, PaymentConfirmResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
