package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DeltanetResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("searchResultCount")
    private int searchResultCount;
    @SerializedName("deltanetDeposits")
    private ArrayList<DeltaNetEstate> deltanetDeposits;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public int getSearchResultCount() {
        return searchResultCount;
    }

    public ArrayList<DeltaNetEstate> getDeltanetDeposits() {
        return deltanetDeposits;
    }
}
