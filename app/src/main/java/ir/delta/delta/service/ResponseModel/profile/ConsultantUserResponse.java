package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class ConsultantUserResponse {

    @SerializedName("successed")
    private Boolean successed;
    @SerializedName("message")
    private String message;

    @SerializedName("logedInUserInfo")
    private ConsultantLogedInUserInfo logedInUserInfo;


    public Boolean getSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }


    public ConsultantLogedInUserInfo getLogedInUserInfo() {
        return logedInUserInfo;
    }
}
