package ir.delta.delta.service.ResponseModel.ads;

public class AllListItem {

    private int id;
    private String title;
    private String LocationName;
    private String baseMultimediaPath;
    private String logo;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public String getBaseMultimediaPath() {
        return baseMultimediaPath;
    }

    public void setBaseMultimediaPath(String baseMultimediaPath) {
        this.baseMultimediaPath = baseMultimediaPath;
    }


}
