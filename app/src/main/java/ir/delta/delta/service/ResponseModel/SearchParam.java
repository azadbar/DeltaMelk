package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SearchParam implements Parcelable {

    @SerializedName("Text")
    private String Text;
    @SerializedName("Value")
    private int Value;


    public String getText() {
        if (Text == null) {
            return "";
        }
        return Text;
    }

    public int getValue() {
        return Value;
    }

    public SearchParam() {
    }

    protected SearchParam(Parcel in) {
        this.Text = in.readString();
        this.Value = in.readInt();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Text);
        dest.writeInt(this.Value);
    }


    public static final Parcelable.Creator<SearchParam> CREATOR = new Parcelable.Creator<SearchParam>() {
        @Override
        public SearchParam createFromParcel(Parcel source) {
            return new SearchParam(source);
        }

        @Override
        public SearchParam[] newArray(int size) {
            return new SearchParam[size];
        }
    };


}
