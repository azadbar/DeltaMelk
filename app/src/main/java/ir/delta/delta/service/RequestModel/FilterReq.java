package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FilterReq {

    @SerializedName("locationId")
    private int locationId;
    @SerializedName("areaIds")
    private ArrayList<Integer> areaIds;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("minMortgageOrTotalId")
    private int minMortgageOrTotalId;
    @SerializedName("maxMortgageOrTotalId")
    private int maxMortgageOrTotalId;
    @SerializedName("maxPropertyTotalAreaId")
    private int maxPropertyTotalAreaId;
    @SerializedName("minPropertyTotalAreaId")
    private int minPropertyTotalAreaId;
    @SerializedName("minRentOrMetricId")
    private int minRentOrMetricId;
    @SerializedName("maxRentOrMetricId")
    private int maxRentOrMetricId;
    @SerializedName("maxPropertyAreaId")
    private int maxPropertyAreaId;
    @SerializedName("minPropertyAreaId")
    private int minPropertyAreaId;
    @SerializedName("regionListFilter")
    private ArrayList<Integer> regionListFilter;
    @SerializedName("hasElevator")
    private boolean hasElevator;
    @SerializedName("hasStore")
    private boolean hasStore;
    @SerializedName("hasLoan")
    private boolean hasLoan;
    @SerializedName("hasParking")
    private boolean hasParking;
    @SerializedName("offset")
    private int offset;
    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("logedInUserTokenOrId")
    private String logedInUserTokenOrId;

    public FilterReq() {

    }


    public String getLogedInUserTokenOrId() {
        return logedInUserTokenOrId;
    }

    public void setLogedInUserTokenOrId(String logedInUserTokenOrId) {
        this.logedInUserTokenOrId = logedInUserTokenOrId;
    }

    public ArrayList<Integer> getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(ArrayList<Integer> areaIds) {
        this.areaIds = areaIds;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public int getMinMortgageOrTotalId() {
        return minMortgageOrTotalId;
    }

    public void setMinMortgageOrTotalId(int minMortgageOrTotalId) {
        this.minMortgageOrTotalId = minMortgageOrTotalId;
    }

    public int getMaxMortgageOrTotalId() {
        return maxMortgageOrTotalId;
    }

    public void setMaxMortgageOrTotalId(int maxMortgageOrTotalId) {
        this.maxMortgageOrTotalId = maxMortgageOrTotalId;
    }

    public int getMaxPropertyTotalAreaId() {
        return maxPropertyTotalAreaId;
    }

    public void setMaxPropertyTotalAreaId(int maxPropertyTotalAreaId) {
        this.maxPropertyTotalAreaId = maxPropertyTotalAreaId;
    }

    public int getMinPropertyTotalAreaId() {
        return minPropertyTotalAreaId;
    }

    public void setMinPropertyTotalAreaId(int minPropertyTotalAreaId) {
        this.minPropertyTotalAreaId = minPropertyTotalAreaId;
    }

    public int getMinRentOrMetricId() {
        return minRentOrMetricId;
    }

    public void setMinRentOrMetricId(int minRentOrMetricId) {
        this.minRentOrMetricId = minRentOrMetricId;
    }

    public int getMaxRentOrMetricId() {
        return maxRentOrMetricId;
    }

    public void setMaxRentOrMetricId(int maxRentOrMetricId) {
        this.maxRentOrMetricId = maxRentOrMetricId;
    }

    public int getMaxPropertyAreaId() {
        return maxPropertyAreaId;
    }

    public void setMaxPropertyAreaId(int maxPropertyAreaId) {
        this.maxPropertyAreaId = maxPropertyAreaId;
    }

    public int getMinPropertyAreaId() {
        return minPropertyAreaId;
    }

    public void setMinPropertyAreaId(int minPropertyAreaId) {
        this.minPropertyAreaId = minPropertyAreaId;
    }

    public ArrayList<Integer> getRegionListFilter() {
        return regionListFilter;
    }

    public void setRegionListFilter(ArrayList<Integer> regionListFilter) {
        this.regionListFilter = regionListFilter;
    }

    public boolean isHasElevator() {
        return hasElevator;
    }

    public void setHasElevator(boolean hasElevator) {
        this.hasElevator = hasElevator;
    }

    public boolean isHasStore() {
        return hasStore;
    }

    public void setHasStore(boolean hasStore) {
        this.hasStore = hasStore;
    }

    public boolean isHasLoan() {
        return hasLoan;
    }

    public void setHasLoan(boolean hasLoan) {
        this.hasLoan = hasLoan;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }


}
