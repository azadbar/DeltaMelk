package ir.delta.delta.service.Request.campaign;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.campaign.CampaignVerifyReq;
import ir.delta.delta.service.ResponseModel.Campaign.VerificationCampaignResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class VerificationCampaignService {


    private static VerificationCampaignService verificationCampaignService;

    public static VerificationCampaignService getInstance() {
        if (verificationCampaignService == null) {
            verificationCampaignService = new VerificationCampaignService();
        }
        return verificationCampaignService;
    }


    public void getVarificarion(final Resources res, CampaignVerifyReq req, final ResponseListener<VerificationCampaignResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).campaignVerificationCode(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    VerificationCampaignResponse response = gson.fromJson(mJson, VerificationCampaignResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
