package ir.delta.delta.service.Request;

public interface ResponseListener<T> {

    void onGetError(String error);

    void onSuccess(T response);

    void onAuthorization();

}
