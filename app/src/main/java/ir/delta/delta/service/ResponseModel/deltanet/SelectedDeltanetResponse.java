package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class SelectedDeltanetResponse implements Serializable {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("searchResultCount")
    private int searchResultCount;
    @SerializedName("deltanetSelectedDeposits")
    private ArrayList<SelectedDeltaNetEstate> deltanetSelectedDeposits;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public int getSearchResultCount() {
        return searchResultCount;
    }

    public ArrayList<SelectedDeltaNetEstate> getDeltanetSelectedDeposits() {
        return deltanetSelectedDeposits;
    }
}
