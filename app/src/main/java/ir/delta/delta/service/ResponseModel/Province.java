package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Province implements Parcelable {
//استان

    @SerializedName("id")
    private int id;
    @SerializedName("order")
    private int order;
    @SerializedName("name")
    private String name;


    public int getOrder() {
        return order;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.order);
        dest.writeString(this.name);
    }

    public Province() {
    }

    protected Province(Parcel in) {
        this.id = in.readInt();
        this.order = in.readInt();
        this.name = in.readString();
    }

    public static final Parcelable.Creator<Province> CREATOR = new Parcelable.Creator<Province>() {
        @Override
        public Province createFromParcel(Parcel source) {
            return new Province(source);
        }

        @Override
        public Province[] newArray(int size) {
            return new Province[size];
        }
    };
}
