package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class DetailEstateDisplayText {

    @SerializedName("text")
    private String text;
    @SerializedName("value")
    private String value;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        if (text == null) {
            return "";
        }
        return text;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        if (value == null) {
            return "";
        }
        return value;
    }

    @Override
    public String toString() {
        return
                "DetailEstateDisplayText{" +
                        "text = '" + text + '\'' +
                        ",value = '" + value + '\'' +
                        "}";
    }
}
