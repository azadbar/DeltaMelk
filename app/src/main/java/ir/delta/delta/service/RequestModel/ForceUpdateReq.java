package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class ForceUpdateReq {


    @SerializedName("appVersionCode")
    private int appVersionCode;
    @SerializedName("jsonCashVersion")
    private int jsonCashVersion;

    public ForceUpdateReq(int appVersionCode, int jsonCashVersion) {
        this.appVersionCode = appVersionCode;
        this.jsonCashVersion = jsonCashVersion;
    }

    public int getAppVersionCode() {
        return appVersionCode;
    }

    public int getJsonCashVersion() {
        return jsonCashVersion;
    }

    public void setAppVersionCode(int appVersionCode) {
        this.appVersionCode = appVersionCode;
    }

    public void setJsonCashVersion(int jsonCashVersion) {
        this.jsonCashVersion = jsonCashVersion;
    }
}
