package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class PostCategory implements Serializable {

    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;
    @SerializedName("termId")
    private int termId;
    @SerializedName("objects")
    private ArrayList<Object> objects;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public ArrayList<Object> getObjects() {
        return objects;
    }

    public int getTermId() {
        return termId;
    }


}
