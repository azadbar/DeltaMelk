package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class VerificationCodeReq {


    @SerializedName("mobile")
    private String mobile;
    @SerializedName("vCode")
    private int vCode;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getvCode() {
        return vCode;
    }

    public void setvCode(int vCode) {
        this.vCode = vCode;
    }
}
