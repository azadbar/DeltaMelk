package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RegionResponse{

	@SerializedName("listRegion")
	private ArrayList<ListRegionItem> listRegion;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public ArrayList<ListRegionItem> getListRegion(){
		return listRegion;
	}

	public boolean isSuccess(){
		return success;
	}

	public String getMessage(){
		return message;
	}
}