package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DepositResponse implements Parcelable {

    @SerializedName("depositId")
    private int depositId;
    @SerializedName("enDepositId")
    private String enDepositId;
    @SerializedName("orderTitle")
    private String orderTitle;
    @SerializedName("orderPrice")
    private String orderPrice;
    @SerializedName("isWalletPaymentNeed")
    private boolean isWalletPaymentNeed;
    @SerializedName("isWalletAmountOk")
    private boolean isWalletAmountOk;
    @SerializedName("walletAmount")
    private String walletAmount;
    @SerializedName("duration")
    private int duration;
    @SerializedName("orderId")
    private int orderId;
    @SerializedName("enOrderId")
    private String enOrderId;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;


    protected DepositResponse(Parcel in) {
        depositId = in.readInt();
        enDepositId = in.readString();
        orderTitle = in.readString();
        orderPrice = in.readString();
        isWalletPaymentNeed = in.readByte() != 0;
        isWalletAmountOk = in.readByte() != 0;
        walletAmount = in.readString();
        duration = in.readInt();
        orderId = in.readInt();
        enOrderId = in.readString();
        successed = in.readByte() != 0;
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(depositId);
        dest.writeString(enDepositId);
        dest.writeString(orderTitle);
        dest.writeString(orderPrice);
        dest.writeByte((byte) (isWalletPaymentNeed ? 1 : 0));
        dest.writeByte((byte) (isWalletAmountOk ? 1 : 0));
        dest.writeString(walletAmount);
        dest.writeInt(duration);
        dest.writeInt(orderId);
        dest.writeString(enOrderId);
        dest.writeByte((byte) (successed ? 1 : 0));
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DepositResponse> CREATOR = new Creator<DepositResponse>() {
        @Override
        public DepositResponse createFromParcel(Parcel in) {
            return new DepositResponse(in);
        }

        @Override
        public DepositResponse[] newArray(int size) {
            return new DepositResponse[size];
        }
    };

    public boolean isWalletAmountOk() {
        return isWalletAmountOk;
    }

    public String getWalletAmount() {
        return walletAmount;
    }

    public void setWalletAmount(String walletAmount) {
        this.walletAmount = walletAmount;
    }

    public void setWalletAmountOk(boolean walletAmountOk) {
        isWalletAmountOk = walletAmountOk;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public boolean isWalletPaymentNeed() {
        return isWalletPaymentNeed;
    }

    public void setWalletPaymentNeed(boolean walletPaymentNeed) {
        isWalletPaymentNeed = walletPaymentNeed;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public int getDepositId() {
        return depositId;
    }

    public void setDepositId(int depositId) {
        this.depositId = depositId;
    }

    public String getEnDepositId() {
        return enDepositId;
    }

    public void setEnDepositId(String enDepositId) {
        this.enDepositId = enDepositId;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getEnOrderId() {
        return enOrderId;
    }

    public void setEnOrderId(String enOrderId) {
        this.enOrderId = enOrderId;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public DepositResponse() {
    }

}
