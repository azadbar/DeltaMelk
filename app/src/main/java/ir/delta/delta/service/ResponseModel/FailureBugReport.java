package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class FailureBugReport {

    @SerializedName("Selected")
    private boolean Selected;
    @SerializedName("Text")
    private String Text;
    @SerializedName("Value")
    private String Value;

    public boolean isSelected() {
        return Selected;
    }

    public void setSelected(boolean selected) {
        Selected = selected;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
