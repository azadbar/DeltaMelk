package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CommunityAdsItem {

	@SerializedName("key")
	private String key;

	@SerializedName("value")
	private ArrayList<ValueItem> value;

	public ArrayList<ValueItem> getValue(){
		return value;
	}

	public String getKey(){
		return key;
	}
}