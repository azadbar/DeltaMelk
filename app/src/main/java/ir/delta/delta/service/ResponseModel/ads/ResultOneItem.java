package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResultOneItem {

	@SerializedName("key")
	private String key;

	@SerializedName("value")
	private ArrayList<DetailAdsItem> value;

	public ArrayList<DetailAdsItem> getValue(){
		return value;
	}

	public String getKey(){
		return key;
	}
}