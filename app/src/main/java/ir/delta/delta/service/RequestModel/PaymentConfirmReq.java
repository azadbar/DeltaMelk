package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class PaymentConfirmReq {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("token")
    private String token;
    @SerializedName("resultCode")
    private String resultCode;
    @SerializedName("referenceId")
    private String referenceId;
    @SerializedName("ExeptionMessage")
    private String ExeptionMessage;

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getExeptionMessage() {
        return ExeptionMessage;
    }

    public void setExeptionMessage(String exeptionMessage) {
        ExeptionMessage = exeptionMessage;
    }
}
