package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class SendToSiteResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("depositInfo")
    private SendToSiteData depositInfo;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SendToSiteData getDepositInfo() {
        return depositInfo;
    }

    public void setDepositInfo(SendToSiteData depositInfo) {
        this.depositInfo = depositInfo;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }
}
