package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

public class MagNode {

    @SerializedName("tag")
    private String tag;
    @SerializedName("content")
    private NodeContent content;

    public String getTag() {
        if (tag == null) {
            return "";
        }
        return tag;
    }

    public void setContent(NodeContent content) {
        this.content = content;
    }

    public NodeContent getContent() {
        return content;
    }

//    private void fillTableRow(String text, MagPostRow magPostRow) {
//        if (!TextUtils.isEmpty(text)) {
//            Document doc = Jsoup.parse(text);
//            Elements childs = doc.getElementsByTag("body").get(0).children();
//            if (childs.size() == 0) {
//                magPostRow.appendText(text.trim());
//            } else {
//                Element element = childs.get(0);
//                HtmlTagEnum htmlTag = HtmlTagEnum.getHtmlTag(element.tagName());
//                for (Node node : element.textNodes()) {
//                    if (node instanceof TextNode) {
//                        magPostRow.appendText(((TextNode) node).text().trim());
//                        break;
//                    }
//                }
//                if (element.attributes() != null) {
//                    if (htmlTag == HtmlTagEnum.a) {
//                        for (Attribute att : element.attributes()) {
//                            if (TextUtils.equals(att.getKey(), "href")) {
//                                magPostRow.setHref(att.getValue());
//                                break;
//                            }
//                        }
//                    } else if (htmlTag == HtmlTagEnum.img) {
//                        String width = "";
//                        String height = "";
//                        for (Attribute att : element.attributes()) {
//                            if (TextUtils.equals(att.getKey(), "src")) {
//                                magPostRow.setSrc(att.getValue());
//                            } else if (TextUtils.equals(att.getKey(), "width")) {
//                                width = att.getValue();
//                            } else if (TextUtils.equals(att.getKey(), "height")) {
//                                height = att.getValue();
//                            }
//                        }
//                        magPostRow.setHeightFactor(Constants.getHeightFactr(width, height));
//                    }
//                }
//            }
//        }
//
//    }


}
