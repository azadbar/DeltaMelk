package ir.delta.delta.service.Request.campaign;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.campaign.CampaignQuizReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignResendVerificationCodeReq;
import ir.delta.delta.service.ResponseModel.Campaign.CampaignQuizFirstPageResponse;
import ir.delta.delta.service.ResponseModel.Campaign.ResendVerificationCodeResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class CampaignGetQuizFirstPageCodeService {


    private static CampaignGetQuizFirstPageCodeService campaignGetQuizFirstPageCodeService;

    public static CampaignGetQuizFirstPageCodeService getInstance() {
        if (campaignGetQuizFirstPageCodeService == null) {
            campaignGetQuizFirstPageCodeService = new CampaignGetQuizFirstPageCodeService();
        }
        return campaignGetQuizFirstPageCodeService;
    }


    public void getQuizFirstPage(final Resources res, CampaignQuizReq req, final ResponseListener<CampaignQuizFirstPageResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).campaignGetQuiz(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    CampaignQuizFirstPageResponse response = gson.fromJson(mJson, CampaignQuizFirstPageResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
