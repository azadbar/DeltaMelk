package ir.delta.delta.service.ResponseModel.Campaign;

import ir.delta.delta.service.ResponseModel.BaseResponseModel;

public class CampaignQuizFirstPageResponse extends BaseResponseModel {
    private boolean isNotRegistered;
    private boolean isNotActivated;
    private boolean hasActiveQuiz;
    private String name;
    private String mobile;
    private String refCode;
    private int friendsCount;
    private int points;

    public boolean isNotRegistered() {
        return isNotRegistered;
    }

    public void setNotRegistered(boolean notRegistered) {
        isNotRegistered = notRegistered;
    }

    public boolean isNotActivated() {
        return isNotActivated;
    }

    public void setNotActivated(boolean notActivated) {
        isNotActivated = notActivated;
    }

    public boolean isHasActiveQuiz() {
        return hasActiveQuiz;
    }

    public void setHasActiveQuiz(boolean hasActiveQuiz) {
        this.hasActiveQuiz = hasActiveQuiz;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }

    public int getFriendsCount() {
        return friendsCount;
    }

    public void setFriendsCount(int friendsCount) {
        this.friendsCount = friendsCount;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
