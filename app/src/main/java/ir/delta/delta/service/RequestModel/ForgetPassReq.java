package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class ForgetPassReq {
    @SerializedName("pass")
    private String pass;
    @SerializedName("confirmPass")
    private String confirmPass;
    @SerializedName("userToken")
    private String userToken;

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
