package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class PaymentConfirmResponse {

    @SerializedName("successed")
    public boolean successed;
    @SerializedName("message")
    public String message;
    @SerializedName("error")
    public String error;
    @SerializedName("title")
    public String title;

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
