package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.EditEstateReq;
import ir.delta.delta.service.RequestModel.EditPropertyReq;
import ir.delta.delta.service.ResponseModel.EditPropertyResponse;
import ir.delta.delta.service.ResponseModel.editEstate.EditEstateResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class EditPropertyService {


    private static EditPropertyService editPropertyService;

    public static EditPropertyService getInstance() {
        if (editPropertyService == null) {
            editPropertyService = new EditPropertyService();
        }
        return editPropertyService;
    }


    public void editProperty(Context context, final Resources res, EditPropertyReq req, final ResponseListener<EditPropertyResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).editProperty(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    EditPropertyResponse response = gson.fromJson(mJson, EditPropertyResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
