package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class ConsultantLogedInUserInfo {

    @SerializedName("userId")
    private int userId;//consultant
    @SerializedName("lastname")
    private String lastname;//آزادبر
    @SerializedName("alias")
    private String alias;
    @SerializedName("mobile")
    private String mobile;//09354669385
    @SerializedName("showDeltaNetMenu")
    private boolean showDeltaNetMenu;
    @SerializedName("activityLocation")
    private String activityLocation;//تهران منطقه 5
    @SerializedName("officeTitle")
    private String officeTitle;
    @SerializedName("officeLogo")
    private String officeLogo;
    @SerializedName("imagePath")
    private String imagePath;
    @SerializedName("locationId")
    private int locationId;

    public int getLocationId() {
        return locationId;
    }

    public int getUserId() {
        return userId;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAlias() {
        return alias;
    }

    public String getMobile() {
        return mobile;
    }

    public boolean isShowDeltaNetMenu() {
        return showDeltaNetMenu;
    }

    public String getActivityLocation() {
        return activityLocation;
    }

    public String getOfficeTitle() {
        return officeTitle;
    }

    public String getOfficeLogo() {
        return officeLogo;
    }

    public String getImagePath() {
        return imagePath;
    }
}
