package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class DeltaNetReq {

    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("offset")
    private int offset;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
