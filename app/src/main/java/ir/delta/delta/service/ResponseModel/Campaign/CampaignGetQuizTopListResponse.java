package ir.delta.delta.service.ResponseModel.Campaign;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.BaseResponseModel;

public class CampaignGetQuizTopListResponse extends BaseResponseModel {

    private ArrayList<QuizTopListItem> QuizTopList;

    public ArrayList<QuizTopListItem> getQuizTopList() {
        return QuizTopList;
    }

    public void setQuizTopList(ArrayList<QuizTopListItem> quizTopList) {
        QuizTopList = quizTopList;
    }
}
