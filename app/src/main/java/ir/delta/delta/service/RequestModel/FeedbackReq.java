package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class FeedbackReq {

    @SerializedName("contactName")
    private String contactName;
    @SerializedName("contactPhone")
    private String contactPhone;
    @SerializedName("context")
    private String context;


    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
