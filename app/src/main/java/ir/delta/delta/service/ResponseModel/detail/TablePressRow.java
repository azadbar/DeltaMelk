package ir.delta.delta.service.ResponseModel.detail;

import android.text.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.TableVisibility;
import ir.delta.delta.util.Constants;

public class TablePressRow {

    private String href;
    private String title;
    private String src;
    private float heightFactor;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public float getHeightFactor() {
        return heightFactor;
    }

    public void setHeightFactor(float heightFactor) {
        this.heightFactor = heightFactor;
    }

    public TablePressRow(ArrayList<String> row, TableVisibility visibility) {
        for (int j = 0; j < row.size(); j++) {
            if (visibility.getColumns().size() == 0 ||
                    (j < visibility.getColumns().size() && visibility.getColumns().get(j) == 1)) {
                String s = fillTableRow(row.get(j));
                if (!TextUtils.isEmpty(s)){
                    title += (TextUtils.isEmpty(title)? s: "\n"+ s);
                }
            }
        }
    }

    private String fillTableRow(String text) {
        if (TextUtils.isEmpty(text)) {
            return text;
        }

        Document doc = Jsoup.parse(text);
        Elements childs = doc.getElementsByTag("body").get(0).children();
        if (childs == null || childs.size() == 0) {
            return text.trim();
        }

        Element element = childs.get(0);
        String str = "";
        for (Node node : element.textNodes()) {
            if (node instanceof TextNode && TextUtils.isEmpty(((TextNode) node).text().trim())) {
                str = ((TextNode) node).text().trim();
                break;
            }
        }

        if (element.attributes() == null) {
            return str;
        }
        Attributes attributes = element.attributes();
        String tagName = element.tagName();

        if (tagName.equals("a")) {
            href = getValue("href", attributes);
        } else if (tagName.equals("img")) {
            String width = "";
            String height = "";
            for (Attribute attribute :
                    attributes) {
                if (attribute.getKey().equals("src")) {
                    src = attribute.getValue();
                } else if (attribute.getKey().equals("width")) {
                    width = attribute.getValue();
                } else if (attribute.getKey().equals("height")) {
                    height = attribute.getValue();
                }
            }
            heightFactor = Constants.getHeightFactr(width, height);
        }
        return str;
    }

    private String getValue(String key, Attributes attributes) {
        for (Attribute attribute :
                attributes) {
            if (attribute.getKey().equals(key)) {
                return attribute.getValue();
            }
        }
        return null;
    }
}
