package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.EstateActionReq;
import ir.delta.delta.service.RequestModel.UploadImageReq;
import ir.delta.delta.service.ResponseModel.UploadImageResponse;
import ir.delta.delta.service.ResponseModel.myEstate.EstateActionResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class ImageUploadService {


    private static ImageUploadService imageUploadService;

    public static ImageUploadService getInstance() {
        if (imageUploadService == null) {
            imageUploadService = new ImageUploadService();
        }
        return imageUploadService;
    }


    public void imageUpload(Context context, final Resources res, UploadImageReq req, final ResponseListener<UploadImageResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).imageUpload(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    UploadImageResponse response = gson.fromJson(mJson, UploadImageResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
