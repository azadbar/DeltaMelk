package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResultZeroA{

	@SerializedName("imageList")
	private ArrayList<String> imageList;

	@SerializedName("title")
	private String title;

	@SerializedName("locationName")
	private String locationName;

	public String getLocationName(){
		return locationName;
	}

	public String getTitle(){
		return title;
	}

	public ArrayList<String> getImageList(){
		return imageList;
	}
}