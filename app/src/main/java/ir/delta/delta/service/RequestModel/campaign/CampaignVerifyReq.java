package ir.delta.delta.service.RequestModel.campaign;

public class CampaignVerifyReq {

    private int vCode;
    private String mobile;

    public int getvCode() {
        return vCode;
    }

    public void setvCode(int vCode) {
        this.vCode = vCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
