package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import ir.delta.delta.bottomNavigation.search.LocationInterface;

public class Area extends LocationInterface implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("order")
    private int order;
    @SerializedName("parentId")
    private int parentId;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("locationFeatureLocalId")
    private int locationFeatureLocalId;

    @Override
    public int getId() {
        return id;
    }

    public int getOrder() {
        return order;
    }

    public int getParentId() {
        return parentId;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    @Override
    public String getTitle() {
        return getName();
    }

    @Override
    public String getDescription() {
        if (description == null) {
            return "";
        }
        return description;
    }

    public int getLocationFeatureLocalId() {
        return locationFeatureLocalId;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLocationFeatureLocalId(int locationFeatureLocalId) {
        this.locationFeatureLocalId = locationFeatureLocalId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.order);
        dest.writeInt(this.parentId);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.locationFeatureLocalId);
    }

    public Area() {
    }

    protected Area(Parcel in) {
        this.id = in.readInt();
        this.order = in.readInt();
        this.parentId = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.locationFeatureLocalId = in.readInt();
    }

    public static final Parcelable.Creator<Area> CREATOR = new Parcelable.Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel source) {
            return new Area(source);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };
}
