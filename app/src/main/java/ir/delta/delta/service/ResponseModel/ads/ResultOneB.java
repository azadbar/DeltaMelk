package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResultOneB{

	@SerializedName("key")
	private String key;

	@SerializedName("value")
	private ArrayList<String> value;

	public ArrayList<String> getValue(){
		return value;
	}

	public String getKey(){
		return key;
	}
}