package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.EditEstateReq;
import ir.delta.delta.service.RequestModel.SendToSiteReq;
import ir.delta.delta.service.ResponseModel.deltanet.SendToSiteResponse;
import ir.delta.delta.service.ResponseModel.editEstate.EditEstateResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class SendToSiteService {


    private static SendToSiteService sendToSiteService;

    public static SendToSiteService getInstance() {
        if (sendToSiteService == null) {
            sendToSiteService = new SendToSiteService();
        }
        return sendToSiteService;
    }


    public void sendTOSiteDeltaNet(Context context, final Resources res, SendToSiteReq req, final ResponseListener<SendToSiteResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).sendToSiteDeltaNet(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    SendToSiteResponse response = gson.fromJson(mJson, SendToSiteResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
