package ir.delta.delta.service.ResponseModel.ads;

import java.io.Serializable;

public class AdsItem implements Serializable {

    private int id;
    private String title;
    private boolean isMelki;
    private String image;
    private String firstImage;
    private int parent_id;
    private boolean showList;

    public boolean isShowList() {
        return showList;
    }

    public void setShowList(boolean showList) {
        this.showList = showList;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public boolean isMelki() {
        return isMelki;
    }

    public void setMelki(boolean melki) {
        isMelki = melki;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFirstImage() {
        return firstImage;
    }

    public void setFirstImage(String firstImage) {
        this.firstImage = firstImage;
    }
}
