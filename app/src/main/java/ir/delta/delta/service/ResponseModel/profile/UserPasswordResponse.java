package ir.delta.delta.service.ResponseModel.profile;

import com.google.gson.annotations.SerializedName;

public class UserPasswordResponse {

    @SerializedName("Successed")
    private boolean Successed;
    @SerializedName("message")
    private String message;


    public boolean isSuccessed() {
        return Successed;
    }

    public void setSuccessed(boolean successed) {
        Successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
