package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ListCityItem implements Serializable{

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	@SerializedName("hasRegion")
	private int hasRegion;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getHasRegion() {
		return hasRegion;
	}

	public void setHasRegion(int hasRegion) {
		this.hasRegion = hasRegion;
	}
}