package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class PostCommentReq {

    @SerializedName("token")
    private String token;
    @SerializedName("score")
    private int score;
    @SerializedName("postId")
    private int postId;
    @SerializedName("parentId")
    private int parentId;
    @SerializedName("content")
    private String content;
    @SerializedName("name")
    private String name;
    @SerializedName("mobile")
    private String mobile;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
