package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ContentInfo implements Parcelable {
    @SerializedName("title")
    private String title;
//    private double latitude;
//    private double longitude;
//    private String baseMultimediaPath;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
    }

    public ContentInfo() {
    }

    protected ContentInfo(Parcel in) {
        this.title = in.readString();
    }

    public static final Parcelable.Creator<ContentInfo> CREATOR = new Parcelable.Creator<ContentInfo>() {
        @Override
        public ContentInfo createFromParcel(Parcel source) {
            return new ContentInfo(source);
        }

        @Override
        public ContentInfo[] newArray(int size) {
            return new ContentInfo[size];
        }
    };
}
