package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.TableVisibility;

public class NodeContent {

    @SerializedName("style")
    private String style;
    @SerializedName("text")
    private String text;
    @SerializedName("src")
    private String src;
    @SerializedName("poster")
    private String poster;
    @SerializedName("alt")
    private String alt;
    @SerializedName("width")
    private String width;
    @SerializedName("height")
    private String height;
    @SerializedName("type")
    private String type; // for <script> tag
    @SerializedName("href")
    private String href;
    @SerializedName("table")
    private ArrayList<ArrayList<String>> table;
    @SerializedName("visibility")
    private TableVisibility visibility;
    //for gallery
    @SerializedName("columns")
    private int columns;
    @SerializedName("images")
    private ArrayList<NodeImage> images;


    public String getStyle() {
        if (style == null) {
            return "";
        }
        return style;
    }


    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getText() {
        return text;
    }

    public String getSrc() {
        return src;
    }

    public String getAlt() {
        return alt;
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    public String getType() {
        return type;
    }

    public String getHref() {
        return href;
    }

    public ArrayList<ArrayList<String>> getTable() {
        return table;
    }

    public TableVisibility getVisibility() {
        return visibility;
    }


    public int getColumn() {
        return columns;
    }

    public ArrayList<NodeImage> getImages() {
        return images;
    }
}
