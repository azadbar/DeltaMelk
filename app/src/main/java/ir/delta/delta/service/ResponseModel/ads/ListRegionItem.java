package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

public class ListRegionItem{

	@SerializedName("id")
	private int id;

	@SerializedName("title")
	private String title;

	public int getId(){
		return id;
	}

	public String getTitle(){
		return title;
	}
}