package ir.delta.delta.service.ResponseModel.wallet;

import com.google.gson.annotations.SerializedName;

public class ComplexRegistrationDate {

    @SerializedName("PersianDate")
    private String persianDate;

    @SerializedName("Time")
    private String time;


    public String getPersianDate() {
        return persianDate;
    }

    public void setPersianDate(String persianDate) {
        this.persianDate = persianDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}