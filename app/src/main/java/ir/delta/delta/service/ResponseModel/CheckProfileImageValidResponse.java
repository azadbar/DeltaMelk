package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class CheckProfileImageValidResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }
}
