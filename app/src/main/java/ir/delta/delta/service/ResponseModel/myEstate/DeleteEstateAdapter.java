package ir.delta.delta.service.ResponseModel.myEstate;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.DeleteOwnerTypeEnum;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class DeleteEstateAdapter extends RecyclerView.Adapter<DeleteEstateAdapter.ViewHolder> {

    private final ArrayList<DeleteOwnerTypeEnum> list;
    private final DeleteEstateAdapter.OnItemClickListener listener;
    private DeleteOwnerTypeEnum selectedIndex;

    public DeleteEstateAdapter(ArrayList<DeleteOwnerTypeEnum> list, DeleteOwnerTypeEnum selected, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        selectedIndex = selected;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bug_report, parent, false);
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        DeleteOwnerTypeEnum object = list.get(position);
        holder.tvTitle.setText(object.getString(res));
        if (list != null && selectedIndex.getCode() == object.getCode()) {
            setSelcted(res, holder);
        } else {
            setNormal(res, holder);
        }
        holder.bind(position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgSelect)
        BaseImageView imgSelect;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.row)
        BaseRelativeLayout row;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickSingle(position));
        }

    }

    public void setDeleteEstate(DeleteOwnerTypeEnum selectedIndex) {
        this.selectedIndex = selectedIndex;
        notifyDataSetChanged();
    }

    private void setSelcted(Resources res, ViewHolder holder) {
        holder.imgSelect.setImageResource(R.drawable.ic_check_circle_red);
        holder.tvTitle.setTextColor(res.getColor(R.color.redColor));
    }

    private void setNormal(Resources res, ViewHolder holder) {
        holder.imgSelect.setImageResource(R.drawable.ic_circle);
        holder.tvTitle.setTextColor(res.getColor(R.color.secondaryTextColor));
    }

    public interface OnItemClickListener {
        void onItemClickSingle(int position);
    }
}
