package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.CheckUserConsultantReq;
import ir.delta.delta.service.ResponseModel.profile.CheckUserConsultantResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class CheckUserCodeConsultantService {


    private static CheckUserCodeConsultantService checkUserCodeService;

    public static CheckUserCodeConsultantService getInstance() {
        if (checkUserCodeService == null) {
            checkUserCodeService = new CheckUserCodeConsultantService();
        }
        return checkUserCodeService;
    }


    public void getCheckUserCode(final Resources res, CheckUserConsultantReq req, final ResponseListener<CheckUserConsultantResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).checkUser(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    CheckUserConsultantResponse response = gson.fromJson(mJson, CheckUserConsultantResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
