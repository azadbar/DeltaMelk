package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

public class DetailAdsItem {

	@SerializedName("key")
	private String key;

	@SerializedName("value")
	private String  value;

	@SerializedName("image")
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getValue(){
		return value;
	}

	public String getKey(){
		return key;
	}
}