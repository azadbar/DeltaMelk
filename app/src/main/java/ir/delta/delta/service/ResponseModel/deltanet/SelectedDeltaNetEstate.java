package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

public class SelectedDeltaNetEstate {


    @SerializedName("dataId")
    private long dataId;//
    @SerializedName("depositId")
    private long depositId;
    @SerializedName("title")
    private String title;//خرید زمین ولیعصر, وليعصر, مردوخي",
    @SerializedName("titleDescription")
    private String titleDescription;//150 متر | 100 اتاق
    @SerializedName("persianDate")
    private String persianDate;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;//4000000000
    @SerializedName("rentOrMetric")
    private long rentOrMetric;//4000000000
    @SerializedName("description")
    private String description;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("isSendToSite")
    private boolean isSendToSite;
    @SerializedName("sendToSiteDate")
    private String sendToSiteDate;

    //local
    private boolean expanded = false;


    public String getSendToSiteDate() {
        return sendToSiteDate;
    }

    public long getDataId() {
        return dataId;
    }

    public long getDepositId() {
        return depositId;
    }

    public String getTitle() {
        return title;
    }

    public String getTitleDescription() {
        return titleDescription;
    }

    public String getPersianDate() {
        return persianDate;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public String getDescription() {
        return description;
    }


    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public boolean isSendToSite() {
        return isSendToSite;
    }
}
