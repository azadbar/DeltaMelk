package ir.delta.delta.service.ResponseModel.wallet;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class WalletListResponse {

    @SerializedName("resultCount")
    private int resultCount;

    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    @SerializedName("successed")
    private boolean successed;

    @SerializedName("walletActionsLists")
    private ArrayList<WalletActionsListsItem> walletActionsLists;

    @SerializedName("message")
    private String message;

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public ArrayList<WalletActionsListsItem> getWalletActionsLists() {
        return walletActionsLists;
    }

    public void setWalletActionsLists(ArrayList<WalletActionsListsItem> walletActionsLists) {
        this.walletActionsLists = walletActionsLists;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}