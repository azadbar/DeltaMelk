package ir.delta.delta.service.ResponseModel.deltanet;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class DeltanetActionResponse {

    @SerializedName("deltanetDepositId")
    private int deltanetDepositId;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public int getDeltanetDepositId() {
        return deltanetDepositId;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }
}
