package ir.delta.delta.service.ResponseModel.wallet;

import ir.delta.delta.service.ResponseModel.ModelStateErrors;

public class IncreaseWalletResponse {
    private ModelStateErrors modelStateErrors;
    private int orderId;
    private String enOrderId;
    private boolean successed;
    private String orderPrice;
    private String orderTitle;
    private String message;

    public ModelStateErrors getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ModelStateErrors modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getEnOrderId() {
        return enOrderId;
    }

    public void setEnOrderId(String enOrderId) {
        this.enOrderId = enOrderId;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderTitle() {
        return orderTitle;
    }

    public void setOrderTitle(String orderTitle) {
        this.orderTitle = orderTitle;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
