package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class MagPostResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("termId")
    private String termId;
    @SerializedName("list")
    private ArrayList<MagPost> list;

    public String getTermId() {
        return termId;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<MagPost> getList() {
        return list;
    }



}
