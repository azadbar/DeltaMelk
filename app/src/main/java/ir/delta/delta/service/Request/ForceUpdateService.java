package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.ForceUpdateReq;
import ir.delta.delta.service.ResponseModel.ForceUpdate;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class ForceUpdateService {


    public void getForceUpdate(final Resources res, ForceUpdateReq req, final ResponseListener<ForceUpdate> responseListener) {
        new ServerTransaction().setCall(res, (ApiClient.getClient().create(ReqInterface.class)).VersionControl(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                try {
                    Gson gson = new Gson();
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    ForceUpdate resp = gson.fromJson(mJson, ForceUpdate.class);
                    responseListener.onSuccess(resp);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
