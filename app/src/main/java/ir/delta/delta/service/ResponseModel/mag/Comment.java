package ir.delta.delta.service.ResponseModel.mag;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import ir.delta.delta.R;

public class Comment implements Parcelable {

    @SerializedName("id")
    private String id;
    @SerializedName("parentId")
    private String parentId;
    @SerializedName("auther")
    private String nameAuthor;
    @SerializedName("commentDate")
    private String commentDate;
    @SerializedName("content")
    private String content;
    @SerializedName("score")
    private int score;

    //local

    private int index;
    private int level;
    private boolean expanded = false;

    private boolean hasChild;

    protected Comment(Parcel in) {
        id = in.readString();
        parentId = in.readString();
        nameAuthor = in.readString();
        commentDate = in.readString();
        content = in.readString();
        score = in.readInt();
        index = in.readInt();
        level = in.readInt();
        expanded = in.readByte() != 0;
        hasChild = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(parentId);
        dest.writeString(nameAuthor);
        dest.writeString(commentDate);
        dest.writeString(content);
        dest.writeInt(score);
        dest.writeInt(index);
        dest.writeInt(level);
        dest.writeByte((byte) (expanded ? 1 : 0));
        dest.writeByte((byte) (hasChild ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    public boolean isHasChild() {
        return hasChild;
    }

    public void setHasChild(boolean hasChild) {
        this.hasChild = hasChild;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getNameAuthor(Resources res) {
        if (nameAuthor == null || nameAuthor.trim().isEmpty()) {
            return res.getString(R.string.un_noun);
        }
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isRoot() {
        return parentId == null || TextUtils.equals(parentId, "0");
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
