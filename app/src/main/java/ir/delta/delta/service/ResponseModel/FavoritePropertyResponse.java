package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FavoritePropertyResponse {

    @SerializedName("depositId")
    private int depositId;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    public int getDepositId() {
        return depositId;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }
}
