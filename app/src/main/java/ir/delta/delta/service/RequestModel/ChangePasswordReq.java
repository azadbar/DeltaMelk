package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordReq {

    @SerializedName("currentPass")
    private String currentPass;
    @SerializedName("newPass")
    private String newPass;
    @SerializedName("confirmNewPass")
    private String confirmNewPass;


    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getConfirmNewPass() {
        return confirmNewPass;
    }

    public void setConfirmNewPass(String confirmNewPass) {
        this.confirmNewPass = confirmNewPass;
    }
}
