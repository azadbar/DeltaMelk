package ir.delta.delta.service.Request.wallet;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.PaymentReq;
import ir.delta.delta.service.RequestModel.wallet.IncreaseWalletReq;
import ir.delta.delta.service.ResponseModel.wallet.IncreaseWalletResponse;
import ir.delta.delta.service.ResponseModel.wallet.PaymentSendWalletResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class PaymentSendService {


    private static PaymentSendService paymentSendService;

    public static PaymentSendService getInstance() {
        if (paymentSendService == null) {
            paymentSendService = new PaymentSendService();
        }
        return paymentSendService;
    }


    public void paymentSendWallet(Context context, PaymentReq req, final Resources res, final ResponseListener<PaymentSendWalletResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).paymentSend(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    PaymentSendWalletResponse response = gson.fromJson(mJson, PaymentSendWalletResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
