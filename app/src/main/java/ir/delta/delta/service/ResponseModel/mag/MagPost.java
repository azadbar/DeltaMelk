package ir.delta.delta.service.ResponseModel.mag;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MagPost implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("date")
    private String date;
    @SerializedName("image")
    private String image;
    @SerializedName("author")
    private String author;
    @SerializedName("term_name")
    private String term_name;
    @SerializedName("termId")
    private String termId;
    @SerializedName("commentStatus")
    private String commentStatus;
    @SerializedName("viewCount")
    private String viewCount;
    @SerializedName("post_content")
    private String post_content;


    //read from server later
    @SerializedName("list")
    private ArrayList<MagPostRow> list;
    @SerializedName("link")
    private String link;
    @SerializedName("leadTitle")
    private String leadTitle;
    @SerializedName("titr")
    private String titr;
    @SerializedName("source_name")
    private String source_name;
    @SerializedName("source_link")
    private String source_link;
    @SerializedName("relatePosts")
    private ArrayList<MagPost> relatePosts;
    @SerializedName("comments")
    private ArrayList<Comment> comments;
    @SerializedName("myScore")
    private String score;
    @SerializedName("averageScore")
    private String avrageScore;
    @SerializedName("scoreCount")
    private String scoreCount;

    //local
    @SerializedName("isFavorite")
    private boolean isFavorite;
    @SerializedName("dateSort")
    private String dateSort;


    public MagPost() {

    }

    protected MagPost(Parcel in) {
        id = in.readInt();
        title = in.readString();
        date = in.readString();
        image = in.readString();
        author = in.readString();
        term_name = in.readString();
        termId = in.readString();
        commentStatus = in.readString();
        viewCount = in.readString();
        post_content = in.readString();
        list = in.createTypedArrayList(MagPostRow.CREATOR);
        link = in.readString();
        leadTitle = in.readString();
        titr = in.readString();
        source_name = in.readString();
        source_link = in.readString();
        relatePosts = in.createTypedArrayList(MagPost.CREATOR);
        comments = in.createTypedArrayList(Comment.CREATOR);
        score = in.readString();
        avrageScore = in.readString();
        scoreCount = in.readString();
        isFavorite = in.readByte() != 0;
        dateSort = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(date);
        dest.writeString(image);
        dest.writeString(author);
        dest.writeString(term_name);
        dest.writeString(termId);
        dest.writeString(commentStatus);
        dest.writeString(viewCount);
        dest.writeString(post_content);
        dest.writeTypedList(list);
        dest.writeString(link);
        dest.writeString(leadTitle);
        dest.writeString(titr);
        dest.writeString(source_name);
        dest.writeString(source_link);
        dest.writeTypedList(relatePosts);
        dest.writeTypedList(comments);
        dest.writeString(score);
        dest.writeString(avrageScore);
        dest.writeString(scoreCount);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
        dest.writeString(dateSort);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MagPost> CREATOR = new Creator<MagPost>() {
        @Override
        public MagPost createFromParcel(Parcel in) {
            return new MagPost(in);
        }

        @Override
        public MagPost[] newArray(int size) {
            return new MagPost[size];
        }
    };


    public String getDateSort() {
        return dateSort;
    }

    public void setDateSort(String dateSort) {
        this.dateSort = dateSort;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getAvrageScore() {
        return avrageScore;
    }

    public void setAvrageScore(String avrageScore) {
        this.avrageScore = avrageScore;
    }

    public String getScoreCount() {
        return scoreCount;
    }

    public void setScoreCount(String scoreCount) {
        this.scoreCount = scoreCount;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public String getTitr() {
        return titr;
    }

    public void setTitr(String titr) {
        this.titr = titr;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getSource_link() {
        return source_link;
    }

    public void setSource_link(String source_link) {
        this.source_link = source_link;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public boolean isCommentOpen() {
        return TextUtils.equals(commentStatus, "open");
    }


    public ArrayList<MagPost> getRelatePosts() {
        return relatePosts;
    }

    public void setRelatePosts(ArrayList<MagPost> relatePosts) {
        this.relatePosts = relatePosts;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean open) {
        isFavorite = open;
    }

    public String getTerm_name() {
        return term_name;
    }

    public ArrayList<MagPostRow> getList() {
        return list;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setList(ArrayList<MagPostRow> list) {
        this.list = list;
    }

    public String getCommentStatus() {
        return commentStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getLeadTitle() {
        return leadTitle;
    }

    public void setLeadTitle(String leadTitle) {
        this.leadTitle = leadTitle;
    }

    public void setTerm_name(String term_name) {
        this.term_name = term_name;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }


}
