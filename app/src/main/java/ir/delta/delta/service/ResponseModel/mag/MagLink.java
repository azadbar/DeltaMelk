package ir.delta.delta.service.ResponseModel.mag;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MagLink implements Parcelable {
    @SerializedName("href")
    private String href;
    @SerializedName("text")
    private String text;

    public MagLink(String href, String text) {
        this.href = href;
        this.text = text;
    }


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.href);
        dest.writeString(this.text);
    }

    protected MagLink(Parcel in) {
        this.href = in.readString();
        this.text = in.readString();
    }

    public static final Parcelable.Creator<MagLink> CREATOR = new Parcelable.Creator<MagLink>() {
        @Override
        public MagLink createFromParcel(Parcel source) {
            return new MagLink(source);
        }

        @Override
        public MagLink[] newArray(int size) {
            return new MagLink[size];
        }
    };
}
