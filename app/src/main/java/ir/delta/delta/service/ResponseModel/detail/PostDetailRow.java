package ir.delta.delta.service.ResponseModel.detail;

import ir.delta.delta.enums.PostRowTypeEnum;

public class PostDetailRow {
    private PostRowTypeEnum type;
    private Object object;

    public PostDetailRow(PostRowTypeEnum type, Object object) {
        this.type = type;
        this.object = object;
    }

    public PostRowTypeEnum getType() {
        return type;
    }

    public void setType(PostRowTypeEnum type) {
        this.type = type;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
