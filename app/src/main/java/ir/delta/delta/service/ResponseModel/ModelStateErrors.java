package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class ModelStateErrors {

    @SerializedName("Field")
    private String Field;
    @SerializedName("Message")
    private String Message;

    public String getField() {
        if (Field == null) {
            return "";
        }
        return Field;
    }

    public void setField(String field) {
        Field = field;
    }

    public String getMessage() {
        if (Message == null) {
            return "";
        }
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

}
