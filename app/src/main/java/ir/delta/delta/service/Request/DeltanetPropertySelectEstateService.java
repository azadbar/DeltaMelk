package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.DeltanetPropertySelectReq;
import ir.delta.delta.service.ResponseModel.deltanet.DeltanetActionResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class DeltanetPropertySelectEstateService {


    private static DeltanetPropertySelectEstateService deltanetPropertySelectEstateService;

    public static DeltanetPropertySelectEstateService getInstance() {
        if (deltanetPropertySelectEstateService == null) {
            deltanetPropertySelectEstateService = new DeltanetPropertySelectEstateService();
        }
        return deltanetPropertySelectEstateService;
    }


    public void deltanetPropertySelectEstate(Context context, final Resources res, DeltanetPropertySelectReq req, final ResponseListener<DeltanetActionResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).deltanetPropertySelectEstate(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    DeltanetActionResponse response = gson.fromJson(mJson, DeltanetActionResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
