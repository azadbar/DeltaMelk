package ir.delta.delta.service;



import androidx.annotation.NonNull;

import org.json.JSONObject;

import retrofit2.Response;

public interface RequestListener<T> {
    void onResponse(@NonNull Response<T> response);

    void onFailure(int code, @NonNull JSONObject jsonObject);

}
