package ir.delta.delta.service.ResponseModel.detail;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.detail.HtmlTag;
import ir.delta.delta.service.ResponseModel.mag.Comment;
import ir.delta.delta.service.ResponseModel.mag.MagPost;

public class PostDetailResponse {

    @SerializedName("link")
    private String link;
    @SerializedName("leadTitle")
    private String leadTitle;
    @SerializedName("titr")
    private String titr;
    @SerializedName("title")
    private String title;
    @SerializedName("termName")
    private String termName;
    @SerializedName("termId")
    private String termId;
    @SerializedName("date")
    private String date;
    @SerializedName("source_name")
    private String sourceName;
    @SerializedName("author")
    private String author;
    @SerializedName("image")
    private String image;
    @SerializedName("postContent")
    private String postContent;
    @SerializedName("source_link")
    private String sourceLink;
    @SerializedName("relatedPosts")
    private ArrayList<MagPost> relatedPosts;
    @SerializedName("comments")
    private ArrayList<Comment> comments;
    @SerializedName("commentStatus")
    private String commentStatus;
    @SerializedName("myScore")
    private String score;
    @SerializedName("averageScore")
    private String avrageScore;
    @SerializedName("scoreCount")
    private String scoreCount;
    @SerializedName("viewCount")
    private String viewCount;
    @SerializedName("tags")
    private ArrayList<HtmlTag> tags;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLeadTitle() {
        return leadTitle;
    }

    public void setLeadTitle(String leadTitle) {
        this.leadTitle = leadTitle;
    }

    public String getTitr() {
        return titr;
    }

    public void setTitr(String titr) {
        this.titr = titr;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSourceLink() {
        return sourceLink;
    }

    public void setSourceLink(String sourceLink) {
        this.sourceLink = sourceLink;
    }

    public ArrayList<MagPost> getRelatedPosts() {
        return relatedPosts;
    }

    public void setRelatedPosts(ArrayList<MagPost> relatedPosts) {
        this.relatedPosts = relatedPosts;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }


    public boolean isCommentOpen() {
        return TextUtils.equals(commentStatus, "open");
    }


    public String getCommentStatus() {
        return commentStatus;
    }

    public void setCommentStatus(String commentStatus) {
        this.commentStatus = commentStatus;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getAvrageScore() {
        return avrageScore;
    }

    public void setAvrageScore(String avrageScore) {
        this.avrageScore = avrageScore;
    }

    public String getScoreCount() {
        return scoreCount;
    }

    public void setScoreCount(String scoreCount) {
        this.scoreCount = scoreCount;
    }

    public String getViewCount() {
        return viewCount;
    }

    public void setViewCount(String viewCount) {
        this.viewCount = viewCount;
    }

    public ArrayList<HtmlTag> getTags() {
        return tags;
    }

    public void setTags(ArrayList<HtmlTag> tags) {
        this.tags = tags;
    }
}
