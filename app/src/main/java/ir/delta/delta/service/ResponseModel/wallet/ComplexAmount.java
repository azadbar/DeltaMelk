package ir.delta.delta.service.ResponseModel.wallet;

import com.google.gson.annotations.SerializedName;

public class ComplexAmount {

    @SerializedName("TomanStringToman")
    private String tomanStringToman;


    public String getTomanStringToman() {
        return tomanStringToman;
    }

    public void setTomanStringToman(String tomanStringToman) {
        this.tomanStringToman = tomanStringToman;
    }
}