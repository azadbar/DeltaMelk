package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class EstateReq {

    @SerializedName("cityId")
    private int cityId;
    @SerializedName("startIndex")
    private int startIndex;
    @SerializedName("offset")
    private int offset;
    @SerializedName("logedInUserTokenOrId")
    private String logedInUserTokenOrId;

    public String getLogedInUserTokenOrId() {
        return logedInUserTokenOrId;
    }

    public void setLogedInUserTokenOrId(String logedInUserTokenOrId) {
        this.logedInUserTokenOrId = logedInUserTokenOrId;
    }

    public EstateReq(){

    }
    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
