package ir.delta.delta.service.ResponseModel.demand;

import com.google.gson.annotations.SerializedName;

public class DemandItem {

    @SerializedName("demandId")
    private int demandId;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("contractType")
    private String contractType;
    @SerializedName("propertyType")
    private String propertyType;
    @SerializedName("location")
    private String location;
    @SerializedName("cityTitle")
    private String cityTitle;
    @SerializedName("regionTitle")
    private String regionTitle;
    @SerializedName("maxMortgageOrTotal")
    private long maxMortgageOrTotal;
    @SerializedName("maxRentOrMetric")
    private long maxRentOrMetric;
    @SerializedName("areaTo")
    private int areaTo;
    @SerializedName("description")
    private String description;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("registrationPersianDate")
    private String registrationPersianDate;
    //local
    @SerializedName("expanded")
    private boolean expanded = false;


    public int getDemandId() {
        return demandId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public String getContractType() {
        return contractType;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getLocation() {
        return location;
    }

    public String getCityTitle() {
        return cityTitle;
    }

    public String getRegionTitle() {
        return regionTitle;
    }

    public long getMaxMortgageOrTotal() {
        return maxMortgageOrTotal;
    }

    public long getMaxRentOrMetric() {
        return maxRentOrMetric;
    }

    public int getAreaTo() {
        return areaTo;
    }

    public String getDescription() {
        return description;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMobile() {
        return mobile;
    }

    public String getRegistrationPersianDate() {
        return registrationPersianDate;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }
}
