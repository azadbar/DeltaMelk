package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.SendActivationCodeReq;
import ir.delta.delta.service.ResponseModel.profile.SendActivationCodeResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class SendActivationCodeService {


    private static SendActivationCodeService sendActivationCodeService;

    public static SendActivationCodeService getInstance() {
        if (sendActivationCodeService == null) {
            sendActivationCodeService = new SendActivationCodeService();
        }
        return sendActivationCodeService;
    }


    public void getSendActivationCode(final Resources res, SendActivationCodeReq req, final ResponseListener<SendActivationCodeResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).sendActivationCode(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    SendActivationCodeResponse response = gson.fromJson(mJson, SendActivationCodeResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
