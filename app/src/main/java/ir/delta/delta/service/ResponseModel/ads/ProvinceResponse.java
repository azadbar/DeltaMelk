package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProvinceResponse{

	@SerializedName("listProvince")
	private ArrayList<ListProvinceItem> listProvince;

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	public ArrayList<ListProvinceItem> getListProvince(){
		return listProvince;
	}

	public boolean isSuccess(){
		return success;
	}

	public String getMessage(){
		return message;
	}
}