package ir.delta.delta.service.Request.wallet;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.wallet.IncreaseWalletReq;
import ir.delta.delta.service.ResponseModel.wallet.IncreaseWalletResponse;
import ir.delta.delta.service.ResponseModel.wallet.WalletAmountResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class IncreaseWalletAmountService {


    private static IncreaseWalletAmountService increaseWalletAmountService;

    public static IncreaseWalletAmountService getInstance() {
        if (increaseWalletAmountService == null) {
            increaseWalletAmountService = new IncreaseWalletAmountService();
        }
        return increaseWalletAmountService;
    }


    public void increaseWalletAmount(Context context, IncreaseWalletReq req , final Resources res, final ResponseListener<IncreaseWalletResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).increaseWalletAmount(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    IncreaseWalletResponse response = gson.fromJson(mJson, IncreaseWalletResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
