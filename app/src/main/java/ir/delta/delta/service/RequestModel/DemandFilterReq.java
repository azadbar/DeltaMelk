package ir.delta.delta.service.RequestModel;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Map;
import java.util.WeakHashMap;

import ir.hamsaa.persiandatepicker.util.PersianCalendar;

public class DemandFilterReq implements Serializable {

    @SerializedName("locationId")
    private int locationId;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("fromDate")
    private String fromDate;
    @SerializedName("toDate")
    private String toDate;
    @SerializedName("demandId")
    private int demandId;

    //local
    private PersianCalendar fromCalender;
    private PersianCalendar toCaledner;

    public PersianCalendar getFromCalender() {
        return fromCalender;
    }

    public void setFromCalender(PersianCalendar fromCalender) {
        this.fromCalender = fromCalender;
    }

    public PersianCalendar getToCaledner() {
        return toCaledner;
    }

    public void setToCaledner(PersianCalendar toCaledner) {
        this.toCaledner = toCaledner;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int getDemandId() {
        return demandId;
    }

    public void setDemandId(int demandId) {
        this.demandId = demandId;
    }


    public Map<String, Object> getParams(int startIndex, int offset) {
        Map<String, Object> map = new WeakHashMap<>();
        if (demandId > 0)
            map.put("demandId", demandId);

        if (contractTypeLocalId > 0)
            map.put("contractTypeLocalId", contractTypeLocalId);

        if (propertyTypeLocalId > 0)
            map.put("propertyTypeLocalId", propertyTypeLocalId);

        if (!TextUtils.isEmpty(fromDate))
            map.put("fromDate", fromDate);

        if (!TextUtils.isEmpty(toDate))
            map.put("toDate", toDate);

        if (locationId > 0)
            map.put("locationId", locationId);

        map.put("startIndex", startIndex);
        map.put("offset", offset);

        return map;
    }

    public static Map<String, Object> getParamsWithoutFilter(int startIndex, int offset) {
        Map<String, Object> map = new WeakHashMap<>();

        map.put("startIndex", startIndex);
        map.put("offset", offset);

        return map;
    }
}
