package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.City;

public class SelectedListAdsResponse {

    private boolean success;
    private String message;
    private ArrayList<City> Cities;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<City> getAdslist() {
        return Cities;
    }

    public void setAdslist(ArrayList<City> adslist) {
        Cities = adslist;
    }
}
