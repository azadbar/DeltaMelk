package ir.delta.delta.service.ResponseModel.myEstate;

import com.google.gson.annotations.SerializedName;

import ir.delta.delta.enums.EstateStatusEnum;

public class ConsultantEstate {

    @SerializedName("id")
    private long id;
    @SerializedName("encodedId")
    private String encodedId;
    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("contractType")
    private String contractType;
    @SerializedName("propertyTypeLocalId")
    private int propertyTypeLocalId;
    @SerializedName("propertyType")
    private String propertyType;
    @SerializedName("location")
    private String location;
    @SerializedName("address")
    private String address;
    @SerializedName("showDetail")
    private String showDetail;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("viewCount")
    private int viewCount;
    @SerializedName("image")
    private String image;
    @SerializedName("durationDate")
    private String durationDate;
    @SerializedName("statusName")
    private String statusName;
    @SerializedName("statusLocalId")
    private int statusLocalId;
    @SerializedName("isExtendable")
    private boolean isExtendable;
    @SerializedName("isSpecial")
    private boolean isSpecial;
    @SerializedName("extendedRedirectToEdit")
    private boolean extendedRedirectToEdit;

    //local
    private EstateStatusEnum statusEnum;


    public boolean isExtendedRedirectToEdit() {
        return extendedRedirectToEdit;
    }

    public EstateStatusEnum getStatusEnum() {
        return statusEnum;
    }

    public void setStatusEnum(EstateStatusEnum statusEnum) {
        this.statusEnum = statusEnum;
    }

    public long getId() {
        return id;
    }

    public String getEncodedId() {
        return encodedId;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public String getContractType() {
        return contractType;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public String getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }

    public String getShowDetail() {
        return showDetail;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public int getViewCount() {
        return viewCount;
    }

    public String getImage() {
        return image;
    }

    public String getDurationDate() {
        return durationDate;
    }

    public String getStatusName() {
        return statusName;
    }

    public int getStatusLocalId() {
        return statusLocalId;
    }

    public boolean isExtendable() {
        return isExtendable;
    }

    public boolean isSpecial() {
        return isSpecial;
    }

    public void setExtendable(boolean extendable) {
        isExtendable = extendable;
    }
}
