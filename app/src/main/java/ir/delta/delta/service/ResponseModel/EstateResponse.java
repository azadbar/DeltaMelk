package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EstateResponse {


    @SerializedName("searchResult")
    private ArrayList<Estate> searchResult;
    @SerializedName("resultCount")
    private int resultCount;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;


    public ArrayList<Estate> getSearchResult() {
        return searchResult;
    }

    public int getResultCount() {
        return resultCount;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        if (message == null) {
            return "";
        }
        return message;
    }
}