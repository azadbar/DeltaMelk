package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.enums.ContractTypeEnum;

/**
 * Created by m.azadbar on 12/24/2017.
 */

public class ContractObject {

    @SerializedName("contractTypeLocalId")
    private int contractTypeLocalId;
    @SerializedName("contractTypeName")
    private String contractTypeName;
    @SerializedName("locationFeatureLocalId")
    private int locationFeatureLocalId;
    @SerializedName("propertyTypes")
    private ArrayList<PropertyType> propertyTypes;
//    private int locationFeatureId;

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public int getLocationFeatureLocalId() {
        return locationFeatureLocalId;
    }

    public ArrayList<PropertyType> getPropertyTypes() {
        return propertyTypes;
    }

    public PropertyType getPropertyTypeBy(int propertTypeLocalId) {
        if (propertyTypes != null) {
            for (PropertyType property : this.propertyTypes) {
                if (property.getPropertyTypeLocalId() == propertTypeLocalId) {
                    return property;
                }
            }
        }
        return null;
    }

    public ContractTypeEnum getType() {
        if (contractTypeLocalId == ContractTypeEnum.RENT.getMethodCode()) {
            return ContractTypeEnum.RENT;
        }
        return ContractTypeEnum.PURCHASE;
    }
}
