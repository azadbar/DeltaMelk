package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;

public class AllListAdsResponse {

    private boolean success;
    private String message;
    private ArrayList<AllListItem> Adslist;
    private String text1;
    private String text2;
    private int count;

    public ArrayList<AllListItem> getAdslist() {
        return Adslist;
    }

    public void setAdslist(ArrayList<AllListItem> adslist) {
        Adslist = adslist;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
