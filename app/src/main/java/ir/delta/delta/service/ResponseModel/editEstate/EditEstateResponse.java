package ir.delta.delta.service.ResponseModel.editEstate;

import com.google.gson.annotations.SerializedName;

public class EditEstateResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("dataItem")
    private EditEstateItem dataItem;


    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EditEstateItem getDataItem() {
        return dataItem;
    }

    public void setDataItem(EditEstateItem dataItem) {
        this.dataItem = dataItem;
    }
}
