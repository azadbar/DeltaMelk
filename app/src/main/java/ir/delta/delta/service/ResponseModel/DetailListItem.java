package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class DetailListItem {

	@SerializedName("path")
	private String path;
	@SerializedName("id")
	private int id;
	@SerializedName("hash")
	private String hash;

	public void setPath(String path){
		this.path = path;
	}

	public String getPath(){
		return path;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setHash(String hash){
		this.hash = hash;
	}

	public String getHash(){
		return hash;
	}

	@Override
 	public String toString(){
		return 
			"DetailListItem{" +
			"path = '" + path + '\'' + 
			",id = '" + id + '\'' + 
			",hash = '" + hash + '\'' + 
			"}";
		}
}
