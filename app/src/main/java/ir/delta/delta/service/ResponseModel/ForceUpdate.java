package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ForceUpdate {

    @SerializedName("hasChanged")
    private boolean hasChanged;
    @SerializedName("versionName")
    private String versionName;
    @SerializedName("features")
    private ArrayList<String> features;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("downloadUrl")
    private String downloadUrl;
    @SerializedName("isForce")
    private boolean isForce;
    @SerializedName("newJsonCashVersion")
    private int newJsonCashVersion;
    @SerializedName("JsonCashInfo")
    private Location JsonCashInfo;
    @SerializedName("depositRegisterPrice")
    private int depositRegisterPrice;
    @SerializedName("demandRegisterPrice")
    private int demandRegisterPrice;
    @SerializedName("depositRegisterImageCount")
    private int depositRegisterImageCount;

    //check link for share app link
    @SerializedName("shareAppLink")
    private String shareAppLink;

    public String getShareAppLink() {
        return shareAppLink;
    }

    public void setShareAppLink(String shareAppLink) {
        this.shareAppLink = shareAppLink;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public Location getLocation() {
        return JsonCashInfo;
    }

    public String getDownloadUrl() {
        if (downloadUrl == null) {
            return "";
        }
        return downloadUrl;
    }

    public boolean isHasChanged() {
        return hasChanged;
    }

    public boolean isForce() {
        return isForce;
    }


    public String getVersionName() {
        if (versionName == null) {
            return "";
        }
        return versionName;
    }


    public ArrayList<String> getFeatures() {
        if (features == null) {
            return new ArrayList<>();
        }
        return features;
    }


    public String getUrl() {
        if (downloadUrl == null) {
            return "";
        }
        return downloadUrl;
    }

    public int getNewJsonCashVersion() {

        return newJsonCashVersion;
    }

    public int getDepositRegisterPrice() {
        return depositRegisterPrice;
    }

    public void setDepositRegisterPrice(int depositRegisterPrice) {
        this.depositRegisterPrice = depositRegisterPrice;
    }

    public int getDemandRegisterPrice() {
        return demandRegisterPrice;
    }

    public void setDemandRegisterPrice(int demandRegisterPrice) {
        this.demandRegisterPrice = demandRegisterPrice;
    }

    public int getDepositRegisterImageCount() {
        return depositRegisterImageCount;
    }

    public void setDepositRegisterImageCount(int depositRegisterImageCount) {
        this.depositRegisterImageCount = depositRegisterImageCount;
    }
}
