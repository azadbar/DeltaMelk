package ir.delta.delta.service.RequestModel.campaign;

public class CampaignResendVerificationCodeReq {

    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
