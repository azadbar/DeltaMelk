package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by R.taghizadeh on 4/7/2018.
 */

public class BugReportReq {
    @SerializedName("enDepositId")
    private String enDepositId;
    @SerializedName("typeId")
    private int typeId;
    @SerializedName("senderName")
    private String senderName;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("description")
    private String description;


    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEnDepositId() {
        return enDepositId;
    }

    public void setEnDepositId(String enDepositId) {
        this.enDepositId = enDepositId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
