package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class LoginMobileReq {

    @SerializedName("mobile")
    private String mobile;
    @SerializedName("locationId")
    private int locationId;

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
