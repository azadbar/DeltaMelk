package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BugReportResponse {
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;
    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;



    public boolean isSuccessed() {
        return successed;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }
}