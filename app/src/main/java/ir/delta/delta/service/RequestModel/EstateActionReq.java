package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class EstateActionReq {

    @SerializedName("id")
    private long id;
    @SerializedName("deleteMode")
    private int deleteMode;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDeleteMode() {
        return deleteMode;
    }

    public void setDeleteMode(int deleteMode) {
        this.deleteMode = deleteMode;
    }
}
