package ir.delta.delta.service.Request;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.ProfileImageReq;
import ir.delta.delta.service.ResponseModel.profile.ProfileImageResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class UploadAgentImageService {


    private static UploadAgentImageService uploadAgentImageService;

    public static UploadAgentImageService getInstance() {
        if (uploadAgentImageService == null) {
            uploadAgentImageService = new UploadAgentImageService();
        }
        return uploadAgentImageService;
    }


    public void getUploadProfileImage(Context context, final Resources res, ProfileImageReq req, final ResponseListener<ProfileImageResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient(context).create(ReqInterface.class).UploadProfileImage(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    ProfileImageResponse response = gson.fromJson(mJson, ProfileImageResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
