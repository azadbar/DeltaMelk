package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CityResponse{

	@SerializedName("success")
	private boolean success;

	@SerializedName("message")
	private String message;

	@SerializedName("listCity")
	private ArrayList<ListCityItem> listCity;

	public ArrayList<ListCityItem> getListCity(){
		return listCity;
	}

	public boolean isSuccess(){
		return success;
	}

	public String getMessage(){
		return message;
	}
}