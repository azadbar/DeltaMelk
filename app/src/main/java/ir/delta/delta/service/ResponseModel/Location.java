package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.Model.ContractObjectList;

public class Location {

    @SerializedName("states")
    private ArrayList<Province> states;
    @SerializedName("cities")
    private ArrayList<City> cities;
    @SerializedName("areas")
    private ArrayList<Area> areas;
    @SerializedName("regions")
    private ArrayList<Region> regions;
    @SerializedName("contractObjects")
    private ContractObjectList contractObjects;


    public ArrayList<Province> getProvinces() {
        if (states == null) {
            return new ArrayList<>();
        }
        return states;
    }


    public ArrayList<City> getCities() {
        if (cities == null) {
            return new ArrayList<>();
        }
        return cities;
    }


    public ArrayList<Region> getRegions() {
        if (regions == null) {
            return new ArrayList<>();
        }
        return regions;
    }


    public ArrayList<Area> getAreas() {
        if (areas == null) {
            return new ArrayList<>();
        }
        return areas;
    }

    public ContractObjectList getContractObjects() {
        if (contractObjects == null) {
            return new ContractObjectList();
        }
        return contractObjects;
    }

    public void convert() {
        if (contractObjects != null) {
            for (ContractObject c : contractObjects) {
                if (c.getPropertyTypes() != null) {
                    for (PropertyType p : c.getPropertyTypes()) {
                        if (p.getPropertyTypeName().equals("آپارتمان")) {
                            p.setPropertyTypeName("آپارتمان مسکونی");
                            break;
                        }
                    }

                    for (int i = c.getPropertyTypes().size() - 1; i >= 0; i--) {
                        for (PropertyType p : c.getPropertyTypes()) {
                            if (p.getPropertyTypeLocalId() == 12 || p.getPropertyTypeLocalId() == 14) {
                                c.getPropertyTypes().remove(p);
                                break;
                            }
                        }
                    }
                }
            }


        }
    }
}

