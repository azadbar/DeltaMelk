package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class EditPropertyReq {
    @SerializedName("id")
    private int id;
    @SerializedName("regionId")
    private int regionId;
    @SerializedName("fullName")
    private String fullName;
    //    private String mobile;
//    private String email;
    @SerializedName("address")
    private String address;
    @SerializedName("area")
    private int area;
    @SerializedName("totalArea")
    private int totalArea;
    @SerializedName("groundWidth")
    private int groundWidth;
    @SerializedName("transitWidth")
    private int transitWidth;
    @SerializedName("floor")
    private int floor;
    @SerializedName("floorCount")
    private int floorCount;
    @SerializedName("roomCount")
    private int roomCount;
    @SerializedName("yearBuilt")
    private int yearBuilt;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private long rentOrMetric;
    @SerializedName("hasParking")
    private boolean hasParking;
    @SerializedName("hasStore")
    private boolean hasStore;
    @SerializedName("hasElevator")
    private boolean hasElevator;
    @SerializedName("hasLoan")
    private boolean hasLoan;
    @SerializedName("description")
    private String description;
    @SerializedName("imagesNameDelete")
    private ArrayList<String> imagesNameDelete;
    @SerializedName("imagesListAdded")
    private List<String> imagesListAdded;
    @SerializedName("longitude")
    private int longitude;
    @SerializedName("latitude")
    private int latitude;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(int totalArea) {
        this.totalArea = totalArea;
    }

    public int getGroundWidth() {
        return groundWidth;
    }

    public void setGroundWidth(int groundWidth) {
        this.groundWidth = groundWidth;
    }

    public int getTransitWidth() {
        return transitWidth;
    }

    public void setTransitWidth(int transitWidth) {
        this.transitWidth = transitWidth;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(int floorCount) {
        this.floorCount = floorCount;
    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public int getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(int yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public long getRentOrMetric() {
        return rentOrMetric;
    }

    public void setRentOrMetric(long rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public boolean isHasStore() {
        return hasStore;
    }

    public void setHasStore(boolean hasStore) {
        this.hasStore = hasStore;
    }

    public boolean isHasElevator() {
        return hasElevator;
    }

    public void setHasElevator(boolean hasElevator) {
        this.hasElevator = hasElevator;
    }

    public boolean isHasLoan() {
        return hasLoan;
    }

    public void setHasLoan(boolean hasLoan) {
        this.hasLoan = hasLoan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getImagesNameDelete() {
        return imagesNameDelete;
    }

    public void setImagesNameDelete(ArrayList<String> imagesNameDelete) {
        this.imagesNameDelete = imagesNameDelete;
    }

    public List<String> getImagesListAdded() {
        return imagesListAdded;
    }

    public void setImagesListAdded(List<String> imagesListAdded) {
        this.imagesListAdded = imagesListAdded;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }
}
