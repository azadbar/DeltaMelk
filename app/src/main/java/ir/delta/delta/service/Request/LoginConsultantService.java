package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.LoginConsultantReq;
import ir.delta.delta.service.ResponseModel.profile.ConsultantUserResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class LoginConsultantService {


    private static LoginConsultantService loginConsultantService;

    public static LoginConsultantService getInstance() {
        if (loginConsultantService == null) {
            loginConsultantService = new LoginConsultantService();
        }
        return loginConsultantService;
    }


    public void getLoginConsultant(final Resources res, LoginConsultantReq req, final ResponseListener<ConsultantUserResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).LoginConsultant(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }


            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }
            @Override
            public void onSuccess(JsonObject jsonObject) {
                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    ConsultantUserResponse response = gson.fromJson(mJson, ConsultantUserResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
