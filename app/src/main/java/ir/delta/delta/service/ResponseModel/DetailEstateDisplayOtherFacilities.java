package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class DetailEstateDisplayOtherFacilities {

    @SerializedName("text")
    private String text;
    @SerializedName("value")
    private boolean value;

    public void setText(String text) {

        this.text = text;
    }

    public String getText() {
        if (text == null) {
            return "";
        }
        return text;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public boolean isValue() {
        return value;
    }

    @Override
    public String toString() {
        return
                "DetailEstateDisplayOtherFacilities{" +
                        "text = '" + text + '\'' +
                        ",value = '" + value + '\'' +
                        "}";
    }
}
