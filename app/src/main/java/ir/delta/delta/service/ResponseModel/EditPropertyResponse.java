package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class EditPropertyResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("field")
    private String field;
    @SerializedName("message")
    private String message;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
