package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.RequestModel.PostCommentReq;
import ir.delta.delta.service.ResponseModel.MagSearchResponse;
import ir.delta.delta.service.ResponseModel.mag.CommentResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class PostCommentService {


    private static PostCommentService postCommentService;

    public static PostCommentService getInstance() {
        if (postCommentService == null) {
            postCommentService = new PostCommentService();
        }
        return postCommentService;
    }

    public void postComment(final Resources res, PostCommentReq req, final ResponseListener<CommentResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient_MAG().create(ReqInterface.class).postComment(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    CommentResponse response = gson.fromJson(mJson, CommentResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
