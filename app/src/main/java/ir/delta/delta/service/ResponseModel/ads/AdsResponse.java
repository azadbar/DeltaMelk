package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;

public class AdsResponse {

    private boolean success;
    private String message;
    private ArrayList<AdsItem> groupList;
    private ArrayList<AdsItem> AdsSlideList;
    private String AdsSlideTitle;
    private ArrayList<AdsItem> AdsScrollList;
    private String AdsScrollTitle;
    private int columnCount;
    private int columnCountGroup;

    public int getColumnCountGroup() {
        return columnCountGroup;
    }

    public void setColumnCountGroup(int columnCountGroup) {
        this.columnCountGroup = columnCountGroup;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public ArrayList<AdsItem> getAdsItems() {
        return groupList;
    }

    public void setAdsItems(ArrayList<AdsItem> adsItems) {
        this.groupList = adsItems;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AdsItem> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<AdsItem> groupList) {
        this.groupList = groupList;
    }

    public ArrayList<AdsItem> getAdsSlideList() {
        return AdsSlideList;
    }

    public void setAdsSlideList(ArrayList<AdsItem> adsSlideList) {
        AdsSlideList = adsSlideList;
    }

    public String getAdsSlideTitle() {
        return AdsSlideTitle;
    }

    public void setAdsSlideTitle(String adsSlideTitle) {
        AdsSlideTitle = adsSlideTitle;
    }

    public ArrayList<AdsItem> getAdsScrollList() {
        return AdsScrollList;
    }

    public void setAdsScrollList(ArrayList<AdsItem> adsScrollList) {
        AdsScrollList = adsScrollList;
    }

    public String getAdsScrollTitle() {
        return AdsScrollTitle;
    }

    public void setAdsScrollTitle(String adsScrollTitle) {
        AdsScrollTitle = adsScrollTitle;
    }
}
