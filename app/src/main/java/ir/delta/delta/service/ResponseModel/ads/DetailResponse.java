package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DetailResponse {

    @SerializedName("success")
    private boolean success;

    @SerializedName("message")
    private String message;

    @SerializedName("resultZeroA")
    private ResultZeroA resultZeroA;


    @SerializedName("resultZeroB")
    private ArrayList<DetailAdsItem> resultZeroB;

    @SerializedName("resultOneA")
    private ResultOneItem resultOneA;

    @SerializedName("resultOneA1")
    private ValueItem resultOneA1;

    @SerializedName("resultOneB")
    private ResultOneB resultOneB;

    @SerializedName("resultTwo")
    private ValueItem resultTwo;

    @SerializedName("resultThree")
    private ResultThreeItem resultThree;

    @SerializedName("resultFour")
    private ResultTwoItem resultFour;

    @SerializedName("resultFive")
    private ResultTwoItem resultFive;

    @SerializedName("resultSix")
    private ValueItem resultSix;

    @SerializedName("resultSeven")
    private ResultSevenItem resultSeven;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultZeroA getResultZeroA() {
        return resultZeroA;
    }

    public void setResultZeroA(ResultZeroA resultZeroA) {
        this.resultZeroA = resultZeroA;
    }

    public ArrayList<DetailAdsItem> getResultZeroB() {
        return resultZeroB;
    }

    public void setResultZeroB(ArrayList<DetailAdsItem> resultZeroB) {
        this.resultZeroB = resultZeroB;
    }

    public ResultOneItem getResultOneA() {
        return resultOneA;
    }

    public void setResultOneA(ResultOneItem resultOneA) {
        this.resultOneA = resultOneA;
    }

    public ResultOneB getResultOneB() {
        return resultOneB;
    }

    public void setResultOneB(ResultOneB resultOneB) {
        this.resultOneB = resultOneB;
    }

    public ValueItem getResultTwo() {
        return resultTwo;
    }

    public void setResultTwo(ValueItem resultTwo) {
        this.resultTwo = resultTwo;
    }


    public ResultThreeItem getResultThree() {
        return resultThree;
    }

    public void setResultThree(ResultThreeItem resultThree) {
        this.resultThree = resultThree;
    }

    public ResultTwoItem getResultFour() {
        return resultFour;
    }

    public void setResultFour(ResultTwoItem resultFour) {
        this.resultFour = resultFour;
    }

    public ResultTwoItem getResultFive() {
        return resultFive;
    }

    public void setResultFive(ResultTwoItem resultFive) {
        this.resultFive = resultFive;
    }

    public ValueItem getResultSix() {
        return resultSix;
    }

    public void setResultSix(ValueItem resultSix) {
        this.resultSix = resultSix;
    }

    public ResultSevenItem getResultSeven() {
        return resultSeven;
    }

    public void setResultSeven(ResultSevenItem resultSeven) {
        this.resultSeven = resultSeven;
    }

    public ValueItem getResultOneA1() {
        return resultOneA1;
    }

    public void setResultOneA1(ValueItem resultOneA1) {
        this.resultOneA1 = resultOneA1;
    }
}