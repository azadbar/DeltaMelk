package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DetailEstateData {

    @SerializedName("deposiId")
    private int deposiId;
    @SerializedName("enDepositId")
    private String enDepositId;
    @SerializedName("displayText")
    private ArrayList<DetailEstateDisplayText> displayText;
    @SerializedName("displayOtherFacilities")
    private ArrayList<DetailEstateDisplayOtherFacilities> displayOtherFacilities;
    @SerializedName("mapLatitude")
    private double mapLatitude;
    @SerializedName("mapLongitude")
    private double mapLongitude;
    @SerializedName("description")
    private String description;
    @SerializedName("mortgageOrTotal")
    private long mortgageOrTotal;
    @SerializedName("rentOrMetric")
    private int rentOrMetric;
    @SerializedName("displayMortgageOrTotal")
    private String displayMortgageOrTotal;
    @SerializedName("displayRentOrMetric")
    private String displayRentOrMetric;
    @SerializedName("officeStarsCnt")
    private int officeStarsCnt;
    @SerializedName("displayDate")
    private String displayDate;//not use in app
    @SerializedName("name")
    private String name;
    @SerializedName("userAgencyName")
    private String userAgencyName;
    @SerializedName("userAgencyLogo")
    private String userAgencyLogo;
    @SerializedName("mobile")
    private String mobile;
    @SerializedName("userAgencyPhone")
    private String userAgencyPhone;
    @SerializedName("isOfficeActive")
    private boolean isOfficeActive;//برای اینکه نشون بدیم مشاور است یا خیر
    @SerializedName("deltaMessage")
    private String deltaMessage;//برای اینکه اگه بعدا هم پیغامی به کاربر نشون بدیم ازش استفاده میکنیم
    @SerializedName("enOfficeId")
    private String enOfficeId;//not use in app
    @SerializedName("imagesList")
    private List<DetailListItem> imagesList;
    @SerializedName("depositTitle")
    private String depositTitle;
    @SerializedName("isMobileHidden")
    private boolean isMobileHidden;


    public String getEnDepositId() {
        return enDepositId;
    }

    public void setEnDepositId(String enDepositId) {
        this.enDepositId = enDepositId;
    }

    public String getUserAgencyLogo() {
        return userAgencyLogo;
    }

    public void setUserAgencyLogo(String userAgencyLogo) {
        this.userAgencyLogo = userAgencyLogo;
    }

    public void setDisplayRentOrMetric(String displayRentOrMetric) {
        this.displayRentOrMetric = displayRentOrMetric;
    }

    public String getDisplayRentOrMetric() {
        if (displayRentOrMetric == null) {
            return "";
        }
        return displayRentOrMetric;
    }

    public void setMapLatitude(double mapLatitude) {
        this.mapLatitude = mapLatitude;
    }

    public double getMapLatitude() {
        return mapLatitude;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        if (mobile == null) {
            return "";
        }
        return mobile;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        if (description == null) {
            return "";
        }
        return description;
    }

    public void setDeposiId(int deposiId) {
        this.deposiId = deposiId;
    }

    public int getDeposiId() {
        return deposiId;
    }

    public void setMortgageOrTotal(long mortgageOrTotal) {
        this.mortgageOrTotal = mortgageOrTotal;
    }

    public long getMortgageOrTotal() {
        return mortgageOrTotal;
    }

    public void setUserAgencyName(String userAgencyName) {
        this.userAgencyName = userAgencyName;
    }

    public String getUserAgencyName() {
        if (userAgencyName == null) {
            return "";
        }
        return userAgencyName;
    }

    public void setUserAgencyPhone(String userAgencyPhone) {
        this.userAgencyPhone = userAgencyPhone;
    }

    public String getUserAgencyPhone() {

        if (userAgencyPhone == null) {
            return "";
        }
        return userAgencyPhone;
    }

    public void setDisplayMortgageOrTotal(String displayMortgageOrTotal) {
        this.displayMortgageOrTotal = displayMortgageOrTotal;
    }

    public String getDisplayMortgageOrTotal() {

        if (displayMortgageOrTotal == null) {
            return "";
        }
        return displayMortgageOrTotal;
    }

    public ArrayList<DetailEstateDisplayText> getDisplayText() {
        if (displayText == null) {
            return new ArrayList<>();
        }
        return displayText;
    }

    public void setDisplayText(ArrayList<DetailEstateDisplayText> displayText) {
        this.displayText = displayText;
    }

    public ArrayList<DetailEstateDisplayOtherFacilities> getDisplayOtherFacilities() {
        if (displayOtherFacilities == null) {
            return new ArrayList<>();
        }
        return displayOtherFacilities;
    }

    public void setDisplayOtherFacilities(ArrayList<DetailEstateDisplayOtherFacilities> displayOtherFacilities) {
        this.displayOtherFacilities = displayOtherFacilities;
    }

    public boolean isOfficeActive() {
        return isOfficeActive;
    }

    public void setOfficeActive(boolean officeActive) {
        isOfficeActive = officeActive;
    }

    public void setOfficeStarsCnt(int officeStarsCnt) {
        this.officeStarsCnt = officeStarsCnt;
    }

    public int getOfficeStarsCnt() {
        return officeStarsCnt;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getDisplayDate() {

        if (displayDate == null) {
            return "";
        }
        return displayDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        if (name == null) {
            return "";
        }
        return name;
    }

    public void setRentOrMetric(int rentOrMetric) {
        this.rentOrMetric = rentOrMetric;
    }

    public int getRentOrMetric() {

        return rentOrMetric;
    }

    public void setIsOfficeActive(boolean isOfficeActive) {
        this.isOfficeActive = isOfficeActive;
    }

    public boolean isIsOfficeActive() {
        return isOfficeActive;
    }

    public void setMapLongitude(double mapLongitude) {
        this.mapLongitude = mapLongitude;
    }

    public double getMapLongitude() {
        return mapLongitude;
    }

    public void setDeltaMessage(String deltaMessage) {
        this.deltaMessage = deltaMessage;
    }

    public String getDeltaMessage() {
        if (deltaMessage == null) {
            return "";
        }
        return deltaMessage;
    }

    public void setEnOfficeId(String enOfficeId) {
        this.enOfficeId = enOfficeId;
    }

    public String getEnOfficeId() {
        if (enOfficeId == null) {
            return "";
        }
        return enOfficeId;
    }

    public void setImagesList(List<DetailListItem> imagesList) {
        this.imagesList = imagesList;
    }

    public List<DetailListItem> getImagesList() {
        return imagesList;
    }

    public void setDepositTitle(String depositTitle) {
        this.depositTitle = depositTitle;
    }

    public String getDepositTitle() {
        if (depositTitle == null) {
            return "";
        }
        return depositTitle;
    }

    public boolean isMobileHidden() {
        return isMobileHidden;
    }

    public void setMobileHidden(boolean mobileHidden) {
        isMobileHidden = mobileHidden;
    }
}