package ir.delta.delta.service.ResponseModel.mag;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MagMenuResponse implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("termId")
    private String termId;
    @SerializedName("children")
    private ArrayList<MagMenuResponse> children;
    @SerializedName("menuType")
    private String menuType;


    protected MagMenuResponse(Parcel in) {
        id = in.readInt();
        title = in.readString();
        termId = in.readString();
        children = in.createTypedArrayList(MagMenuResponse.CREATOR);
        menuType = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(termId);
        dest.writeTypedList(children);
        dest.writeString(menuType);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MagMenuResponse> CREATOR = new Creator<MagMenuResponse>() {
        @Override
        public MagMenuResponse createFromParcel(Parcel in) {
            return new MagMenuResponse(in);
        }

        @Override
        public MagMenuResponse[] newArray(int size) {
            return new MagMenuResponse[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public ArrayList<MagMenuResponse> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<MagMenuResponse> children) {
        this.children = children;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }
}
