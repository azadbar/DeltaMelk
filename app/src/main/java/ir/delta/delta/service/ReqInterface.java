package ir.delta.delta.service;

import com.google.gson.JsonObject;


import java.util.List;
import java.util.Map;

import ir.delta.delta.service.RequestModel.AreaInfoReq;
import ir.delta.delta.service.RequestModel.BugReportReq;
import ir.delta.delta.service.RequestModel.FeedbackReq;
import ir.delta.delta.service.RequestModel.FilterAdsReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignQuizReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignRegisterReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignResendVerificationCodeReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignVerifyReq;
import ir.delta.delta.service.RequestModel.ChangePasswordReq;
import ir.delta.delta.service.RequestModel.CheckUserConsultantReq;
import ir.delta.delta.service.RequestModel.DeltaNetReq;
import ir.delta.delta.service.RequestModel.DeltanetPropertySelectReq;
import ir.delta.delta.service.RequestModel.DemandRegisterReq;
import ir.delta.delta.service.RequestModel.DepositConsultantDetailReq;
import ir.delta.delta.service.RequestModel.DepositDetailByCodeReq;
import ir.delta.delta.service.RequestModel.DepositDetailReq;
import ir.delta.delta.service.RequestModel.DepositReq;
import ir.delta.delta.service.RequestModel.EditEstateReq;
import ir.delta.delta.service.RequestModel.EditProfileUserReq;
import ir.delta.delta.service.RequestModel.EditPropertyReq;
import ir.delta.delta.service.RequestModel.EstateActionReq;
import ir.delta.delta.service.RequestModel.EstateReq;
import ir.delta.delta.service.RequestModel.FavoritePropertyReq;
import ir.delta.delta.service.RequestModel.FilterReq;
import ir.delta.delta.service.RequestModel.ForceUpdateReq;
import ir.delta.delta.service.RequestModel.ForgetPassReq;
import ir.delta.delta.service.RequestModel.GetFavoriteReq;
import ir.delta.delta.service.RequestModel.LoginConsultantReq;
import ir.delta.delta.service.RequestModel.LoginMobileReq;
import ir.delta.delta.service.RequestModel.OrderAdvertisingReq;
import ir.delta.delta.service.RequestModel.PaymentConfirmReq;
import ir.delta.delta.service.RequestModel.PaymentReq;
import ir.delta.delta.service.RequestModel.PostCommentReq;
import ir.delta.delta.service.RequestModel.ProfileImageReq;
import ir.delta.delta.service.RequestModel.SelectedDeltaNetReq;
import ir.delta.delta.service.RequestModel.SendActivationCodeReq;
import ir.delta.delta.service.RequestModel.SendToSiteReq;
import ir.delta.delta.service.RequestModel.StairsReq;
import ir.delta.delta.service.RequestModel.UploadImageReq;
import ir.delta.delta.service.RequestModel.VerificationCodeReq;
import ir.delta.delta.service.RequestModel.wallet.IncreaseWalletReq;
import ir.delta.delta.service.RequestModel.wallet.WalletListReq;
import ir.delta.delta.service.ResponseModel.mag.MagMenuResponse;
import ir.delta.delta.service.ResponseModel.mag.MagPost;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ReqInterface {
    @POST("v2/home/GetNewVersion")
    Call<JsonObject> VersionControl(@Body ForceUpdateReq req);

    @POST("v2/search/GetLastDeposit")
    Call<JsonObject> LastEstate(@Body EstateReq req);

    @POST("v2/search/GetDepositList")
    Call<JsonObject> FilterEstate(@Body FilterReq req);

    @POST("v2/search/GetDepositDetails")
    Call<JsonObject> DepositDetail(@Body DepositDetailReq req);

    @POST("v2/Share/SendFailureDepositReport")
    Call<JsonObject> ReportBug(@Body BugReportReq req);

    @POST("v2/Register/DepositRegister")
    Call<JsonObject> DepositRegister(@Body DepositReq req);

    @POST("v2/Register/DemandRegister")
    Call<JsonObject> DemandRegister(@Body DemandRegisterReq req);

    @POST("v2/Payment/PaymentSend")
    Call<JsonObject> PaymentSend(@Body PaymentReq req);

    @POST("v2/about/aboutArea")
    Call<JsonObject> AreaInfo(@Body AreaInfoReq req);

    @POST("v2/search/GetDepositById")
    Call<JsonObject> detailEstateByCode(@Body DepositDetailByCodeReq req);


    @GET("v2/magHome")
    Call<JsonObject> magHome2(@Query("seed") int item,
                              @Query("withTelegram") boolean withTelegram);

    @GET("v3/appConfig")
    Call<JsonObject> appConfig(@Query("appVersionCode") int appVersionCode);

    @GET("v3/menus")
    Call<List<MagMenuResponse>> getMenus();

    @GET("v3/posts")
    Call<List<MagPost>> getPosts(@Query("from") int from,
                                 @Query("seed") int seed,
                                 @Query("termid") String termId);

    @GET("v3/post")
    Call<JsonObject> magDetail2(@Query("id") int id, @Query("token") String token);

    @GET("v3/posts/search?")
    Call<List<MagPost>> magSearch(
            @Query("from") int from,
            @Query("seed") int seed,
            @Query("searchText") String searchText);


    @GET("v2/post_by_content")
    Call<JsonObject> magDetailPush(@Query("postid") int id, @Query("token") String token);



    @POST("v2/comment")
    Call<JsonObject> postComment(@Body PostCommentReq req);


    @POST("v2/user/loginmobile")
    Call<JsonObject> loginMobile(@Body LoginMobileReq req);

    @POST("v2/user/LoginUseVerificationCode")
    Call<JsonObject> verificationCode(@Body VerificationCodeReq req);

    @POST("v2/account/login")
    Call<JsonObject> LoginConsultant(@Body LoginConsultantReq req);

    @POST("v2/account/ForgetPassActivationCode")
    Call<JsonObject> sendActivationCode(@Body SendActivationCodeReq req);

    @POST("v2/account/ForgetPassCheckUserCode")
    Call<JsonObject> checkUser(@Body CheckUserConsultantReq req);

    @POST("v2/account/ForgetPassChangePass")
    Call<JsonObject> forgetPass(@Body ForgetPassReq req);

    @POST("v2/agentuser/SetProfileImage")
    Call<JsonObject> UploadProfileImage(@Body ProfileImageReq req);

    @POST("v2/account/ChangePassword")
    Call<JsonObject> changePassword(@Body ChangePasswordReq req);


    @POST("v2/AgentUser/DemandList")
    Call<JsonObject> getDemandList(@Body Map<String, Object> list);

    @POST("v2/agentuser/PropertyList")
    Call<JsonObject> getConsultantEstate(@Body Map<String, Object> req);

    @POST("v2/agentuser/SuspendProperty")
    Call<JsonObject> suspendEstate(@Body EstateActionReq req);

    @POST("v2/agentuser/UnSuspendProperty")
    Call<JsonObject> unSuspendEstate(@Body EstateActionReq req);

    @POST("v2/agentuser/DeleteProperty")
    Call<JsonObject> deleteEstate(@Body EstateActionReq req);

    @POST("v2/agentuser/ExtendProperty")
    Call<JsonObject> extendedEstate(@Body EstateActionReq req);

    @POST("v2/agentuser/DeltanetPropertyList")
    Call<JsonObject> deltaNet(@Body DeltaNetReq req);

    @POST("v2/agentuser/DeltanetSelectedPropertyList")
    Call<JsonObject> selectedDeltaNet(@Body SelectedDeltaNetReq req);

    @POST("v2/agentuser/DeltanetPropertySelect")
    Call<JsonObject> deltanetPropertySelectEstate(@Body DeltanetPropertySelectReq req);

    @POST("v2/agentuser/DeltanetPropertyDelete")
    Call<JsonObject> deltanetPropertyDeleteEstate(@Body DeltanetPropertySelectReq req);

    @POST("v2/agentuser/PropertyToEdit")
    Call<JsonObject> editEstate(@Body EditEstateReq req);

    @POST("v2/user/UserPropertiesList")
    Call<JsonObject> getUserEstate(@Body Map<String, Object> req);

    @POST("v2/user/DeleteProperty")
    Call<JsonObject> deleteUserEstate(@Body EstateActionReq req);

    @POST("v2/user/SuspendProperty")
    Call<JsonObject> suspendUserEstate(@Body EstateActionReq req);

    @POST("v2/user/UnSuspendProperty")
    Call<JsonObject> unSuspendUserEstate(@Body EstateActionReq req);

    @POST("v2/user/ExtendProperty")
    Call<JsonObject> extendedEstateUser(@Body EstateActionReq req);


    @GET("v2/update_post_view_manually")
    Call<JsonObject> updatePostViewCount(@Query("postid") int id, @Query("token") String token);

    @POST("DepositRegister/PaymentConfirm")
    Call<JsonObject> getPaymentConfirm(@Body PaymentConfirmReq req);


    @POST("DepositRegister/UploadImage/")
    Call<JsonObject> imageUpload(@Body UploadImageReq req);

    @POST("v2/agentuser/PropertyEdit")
    Call<JsonObject> editProperty(@Body EditPropertyReq req);

    @POST("v2/user/PropertyToEdit")
    Call<JsonObject> editUserEstate(@Body EditEstateReq req);

    @POST("v2/user/PropertyEdit")
    Call<JsonObject> editUserProperty(@Body EditPropertyReq req);

    @POST("v2/user/FavoriteProperty")
    Call<JsonObject> favoriteUserProperty(@Body FavoritePropertyReq req);

    @POST("v2/AgentUser/FavoriteProperty")
    Call<JsonObject> favoriteConsultantProperty(@Body FavoritePropertyReq req);

    @POST("v2/user/UnFavoriteProperty")
    Call<JsonObject> unFavoriteUserProperty(@Body FavoritePropertyReq req);

    @POST("v2/AgentUser/UnFavoriteProperty")
    Call<JsonObject> unFavoriteConsultantProperty(@Body FavoritePropertyReq req);

    @POST("v2/user/GetFavoriteProperty")
    Call<JsonObject> getFavoriteUserProperty(@Body GetFavoriteReq req);

    @POST("v2/AgentUser/GetFavoriteProperty")
    Call<JsonObject> getFavoriteConsultantProperty(@Body GetFavoriteReq req);

    @POST("v2/agentuser/CheckProfileImageStatus")
    Call<JsonObject> chakeProfileImageValid(@Body Object empty);

    @POST("v2/share/SendBannerRequest")
    Call<JsonObject> orderAdvertising(@Body OrderAdvertisingReq req);

    @POST("v2/user/EditProfileInfo")
    Call<JsonObject> editProfileUser(@Body EditProfileUserReq req);

    @POST("v2/agentuser/DeltanetPropertyConvertToSend")
    Call<JsonObject> sendToSiteDeltaNet(@Body SendToSiteReq req);


    @POST("v2/agentuser/PropertyDetail")
    Call<JsonObject> DepositConsultantDetail(@Body DepositConsultantDetailReq req);

    @POST("v2/user/SpecialProperty")
    Call<JsonObject> staireProperty(@Body StairsReq req);


    @POST("v2/user/PropertyDetail")
    Call<JsonObject> DepositUserDetail(@Body DepositConsultantDetailReq req);

    @POST("v2/agentuser/SpecialProperty")
    Call<JsonObject> staireCounsultantProperty(@Body StairsReq req);

    @POST("v2/Campaign/FirstRegister")
    Call<JsonObject> registerGame(@Body CampaignRegisterReq req);

    @POST("v2/Campaign/RegisterVerificationCode")
    Call<JsonObject> campaignVerificationCode(@Body CampaignVerifyReq req);

    @POST("v2/Campaign/ResendVerificationCode")
    Call<JsonObject> campaignResendVerificationCode(@Body CampaignResendVerificationCodeReq req);

    @POST("v2/Campaign/QuizFirstPage")
    Call<JsonObject> campaignGetQuiz(@Body CampaignQuizReq req);

    @GET("v2/Campaign/GetQuizTopList")
    Call<JsonObject> getQuizTopList();

    @GET("v2/ads/group")
    Call<JsonObject> getAdsList();

    @GET("v2/ads/subgroup")
    Call<JsonObject> getSubGroupAds(@Query("groupid") int groupid);

    @GET("v2/ads/list")
    Call<JsonObject> getAllListAds(@Query("groupid") int groupid, @Query("pageNumber") int pageNumber,
                                   @Query("isCountServer") boolean isCountServer,
                                   @Query("beforGroupId") int beforId,
                                   @Query("locationId") int locationId);

    @GET("v2/ads/detail")
    Call<JsonObject> getDetailAds(@Query("infoId") int infoId, @Query("parentGroupId") int parentGroupId);

    @GET("v2/ads/infosearchprovince")
    Call<JsonObject> getProvinceList(@Query("beforGroupId") int groupId);

    @GET("v2/ads/infosearchcity")
    Call<JsonObject> getCityList(@Query("provinceId") int infoId, @Query("beforGroupId") int groupId);

    @GET("v2/ads/infosearchregion")
    Call<JsonObject> getRegionList(@Query("cityId") int infoId, @Query("groupId") int groupId);

    @POST("v2/ads/search")
    Call<JsonObject> getFilterList(@Body FilterAdsReq req);

    @GET("v2/ads/city")
    Call<JsonObject> getSelectedList();

    @POST("v2/ads/feedback")
    Call<JsonObject> feedback(@Body FeedbackReq req);

    @GET("v2/ads/AboutUs")
    Call<JsonObject> getAboutMag();

    @GET("v2/agentuser/WalletAmount")
    Call<JsonObject> getWalletAmount();

    @POST("v2/agentuser/IncreaseWalletAmount")
    Call<JsonObject> increaseWalletAmount(@Body IncreaseWalletReq req);

    @POST("v2/payment/paymentsend")
    Call<JsonObject> paymentSend(@Body PaymentReq req);


    @POST("v2/agentuser/WalletActions")
    Call<JsonObject> getWalletList(@Body WalletListReq req);

    @POST("v2/agentuser/PaymentWallet")
    Call<JsonObject> registerEstateWallet(@Body PaymentReq req);


}
