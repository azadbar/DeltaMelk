package ir.delta.delta.service.ResponseModel;

import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import ir.delta.delta.R;

public class SearchParamInfo implements Parcelable {

    @SerializedName("objPropertyAreas")
    private ArrayList<SearchParam> objPropertyAreas;
    @SerializedName("objMortgageOrTotals")
    private ArrayList<SearchParam> objMortgageOrTotals;
    @SerializedName("objRentOrMetrics")
    private ArrayList<SearchParam> objRentOrMetrics;
    @SerializedName("showTotalAreaFilter")
    private boolean showTotalAreaFilter;


    public ArrayList<SearchParam> getObjPropertyAreas() {
        return objPropertyAreas;
    }

    public ArrayList<SearchParam> getObjMortgageOrTotals() {
        return objMortgageOrTotals;
    }

    public ArrayList<SearchParam> getObjRentOrMetrics() {
        return objRentOrMetrics;
    }

    public boolean isShowTotalAreaFilter() {
        return showTotalAreaFilter;
    }

    public SearchParam getPriceParam(int value) {
        if (objMortgageOrTotals != null) {
            for (SearchParam search : this.objMortgageOrTotals) {
                if (search.getValue() == value) {
                    return search;
                }
            }
        }
        return null;
    }

    public SearchParam getRentParam(int value) {
        if (objRentOrMetrics != null) {
            for (SearchParam search : this.objRentOrMetrics) {
                if (search.getValue() == value) {
                    return search;
                }
            }
        }
        return null;
    }

    public SearchParam getAreaParam(int value) {
        if (objPropertyAreas != null) {
            for (SearchParam search : this.objPropertyAreas) {
                if (search.getValue() == value) {
                    return search;
                }
            }
        }
        return null;
    }

    public SearchParam getTotalParam(int value) {

        if (!showTotalAreaFilter) {
            return null;
        }
        if (objPropertyAreas != null) {
            for (SearchParam search : this.objPropertyAreas) {
                if (search.getValue() == value) {
                    return search;
                }
            }
        }
        return null;
    }

    public Spanned getPriceText(Resources res, String title, int minValue, int maxValue) {
        if (objMortgageOrTotals != null) {
            return getText(objMortgageOrTotals, res, title, minValue, maxValue);
        }
        return null;
    }

    public Spanned getRentText(Resources res, String title, int minValue, int maxValue) {
        if (objRentOrMetrics != null) {
            return getText(objRentOrMetrics, res, title, minValue, maxValue);
        }
        return null;
    }

    public Spanned getMeterText(Resources res, String title, int minValue, int maxValue) {
        if (objPropertyAreas != null) {
            return getText(objPropertyAreas, res, title, minValue, maxValue);
        }
        return null;
    }


    private Spanned getText(ArrayList<SearchParam> list, Resources res, String priceLabel, int minValue, int maxValue) {
        String minText = "";
        int minIndex = -1;
        String maxText = "";
        int maxIndex = -1;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getValue() == minValue) {
                minIndex = i;
                minText = list.get(i).getText();
            }
            if (list.get(i).getValue() == maxValue) {
                maxIndex = i;
                maxText = list.get(i).getText();
            }
        }
        if (minIndex == -1 && maxIndex == -1) {
            return null;
        }
        if (minIndex >= 0 && maxIndex >= 0) {
            return
                    createHtmlText("<font color='#777777'>" + priceLabel + "</font>"
                            + "<font color='#000000'>" + res.getString(R.string.from) + " "
                            + minText + " "
                            + res.getString(R.string.to) + " "
                            + maxText + "</font>");

        }

        if (minIndex >= 0) {
            return
                    createHtmlText("<font color='#777777'>" + priceLabel + "</font>"
                            + "<font color='#000000'>" + res.getString(R.string.from) + " "
                            + minText + " "
                            + res.getString(R.string.to_top) + "</font>");
        }
        return
                createHtmlText("<font color='#777777'>" + priceLabel + "</font>"
                        + "<font color='#000000'>" + res.getString(R.string.to) + " "
                        + maxText + "</font>");
    }

    private Spanned createHtmlText(String text) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.objPropertyAreas);
        dest.writeTypedList(this.objMortgageOrTotals);
        dest.writeTypedList(this.objRentOrMetrics);
        dest.writeByte(this.showTotalAreaFilter ? (byte) 1 : (byte) 0);
    }

    public SearchParamInfo() {
    }

    protected SearchParamInfo(Parcel in) {
        this.objPropertyAreas = in.createTypedArrayList(SearchParam.CREATOR);
        this.objMortgageOrTotals = in.createTypedArrayList(SearchParam.CREATOR);
        this.objRentOrMetrics = in.createTypedArrayList(SearchParam.CREATOR);
        this.showTotalAreaFilter = in.readByte() != 0;
    }

    public static final Parcelable.Creator<SearchParamInfo> CREATOR = new Parcelable.Creator<SearchParamInfo>() {
        @Override
        public SearchParamInfo createFromParcel(Parcel source) {
            return new SearchParamInfo(source);
        }

        @Override
        public SearchParamInfo[] newArray(int size) {
            return new SearchParamInfo[size];
        }
    };
}
