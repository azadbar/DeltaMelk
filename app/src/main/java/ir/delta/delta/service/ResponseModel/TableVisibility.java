package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class TableVisibility implements Serializable {

    @SerializedName("rows")
    private ArrayList<Integer> rows;
    @SerializedName("columns")
    private ArrayList<Integer> columns;


    public ArrayList<Integer> getRows() {
        if (rows == null) {
            return new ArrayList<>();
        }
        return rows;
    }

    public ArrayList<Integer> getColumns() {
        return columns;
    }


}
