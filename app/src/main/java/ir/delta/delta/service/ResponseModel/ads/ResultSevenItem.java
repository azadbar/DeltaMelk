package ir.delta.delta.service.ResponseModel.ads;

import com.google.gson.annotations.SerializedName;

public class ResultSevenItem {

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private double longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}