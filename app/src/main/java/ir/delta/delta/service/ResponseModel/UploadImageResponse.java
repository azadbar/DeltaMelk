package ir.delta.delta.service.ResponseModel;

import com.google.gson.annotations.SerializedName;

public class UploadImageResponse {

    @SerializedName("Successed")
    private boolean Successed;
    @SerializedName("Id")
    private String Id;
    @SerializedName("Name")
    private String Name;
    @SerializedName("Path")
    private String Path;
    @SerializedName("Hash")
    private String Hash;
    @SerializedName("imgNumber")
    private int imgNumber;
    @SerializedName("Error")
    private String Error;

    public boolean isSuccessed() {
        return Successed;
    }

    public void setSuccessed(boolean successed) {
        Successed = successed;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPath() {
        return Path;
    }

    public void setPath(String path) {
        Path = path;
    }

    public String getHash() {
        return Hash;
    }

    public void setHash(String hash) {
        Hash = hash;
    }

    public int getImgNumber() {
        return imgNumber;
    }

    public void setImgNumber(int imgNumber) {
        this.imgNumber = imgNumber;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }
}
