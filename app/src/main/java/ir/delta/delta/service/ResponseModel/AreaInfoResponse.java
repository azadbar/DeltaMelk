package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AreaInfoResponse implements Parcelable {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("title")
    private String title;
    @SerializedName("context")
    private String context;
    @SerializedName("aboutLocationFacilities")
    private ArrayList<Facility> aboutLocationFacilities;
    @SerializedName("aboutLocationAgencyModel")
    private ArrayList<Agency> aboutLocationAgencyModel;
    @SerializedName("modelStateErrors")
    private ArrayList<ModelStateErrors> modelStateErrors;
//  private String summary;//after use in app
//    private String resourceLink;//after use in app //link google


    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public ArrayList<Facility> getAboutLocationFacilities() {
        return aboutLocationFacilities;
    }

    public void setAboutLocationFacilities(ArrayList<Facility> aboutLocationFacilities) {
        this.aboutLocationFacilities = aboutLocationFacilities;
    }

    public ArrayList<Agency> getAboutLocationAgencyModel() {
        return aboutLocationAgencyModel;
    }

    public void setAboutLocationAgencyModel(ArrayList<Agency> aboutLocationAgencyModel) {
        this.aboutLocationAgencyModel = aboutLocationAgencyModel;
    }

    public ArrayList<ModelStateErrors> getModelStateErrors() {
        return modelStateErrors;
    }

    public void setModelStateErrors(ArrayList<ModelStateErrors> modelStateErrors) {
        this.modelStateErrors = modelStateErrors;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.successed ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
        dest.writeString(this.title);
        dest.writeString(this.context);
        dest.writeTypedList(this.aboutLocationFacilities);
        dest.writeTypedList(this.aboutLocationAgencyModel);
        dest.writeList(this.modelStateErrors);
    }

    public AreaInfoResponse() {
    }

    protected AreaInfoResponse(Parcel in) {
        this.successed = in.readByte() != 0;
        this.message = in.readString();
        this.title = in.readString();
        this.context = in.readString();
        this.aboutLocationFacilities = in.createTypedArrayList(Facility.CREATOR);
        this.aboutLocationAgencyModel = in.createTypedArrayList(Agency.CREATOR);
        this.modelStateErrors = new ArrayList<ModelStateErrors>();
        in.readList(this.modelStateErrors, ModelStateErrors.class.getClassLoader());
    }

    public static final Creator<AreaInfoResponse> CREATOR = new Creator<AreaInfoResponse>() {
        @Override
        public AreaInfoResponse createFromParcel(Parcel source) {
            return new AreaInfoResponse(source);
        }

        @Override
        public AreaInfoResponse[] newArray(int size) {
            return new AreaInfoResponse[size];
        }
    };
}
