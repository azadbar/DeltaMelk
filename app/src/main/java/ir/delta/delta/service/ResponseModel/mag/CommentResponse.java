package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

public class CommentResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("code")
    private int code;
    @SerializedName("averageScore")
    private String averageScore;
    @SerializedName("scoreCount")
    private String scoreCount;


    public String getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(String averageScore) {
        this.averageScore = averageScore;
    }

    public String getScoreCount() {
        return scoreCount;
    }

    public void setScoreCount(String scoreCount) {
        this.scoreCount = scoreCount;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public void setSuccessed(boolean successed) {
        this.successed = successed;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
