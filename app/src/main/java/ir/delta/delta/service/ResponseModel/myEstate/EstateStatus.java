package ir.delta.delta.service.ResponseModel.myEstate;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import ir.delta.delta.bottomNavigation.search.LocationInterface;

public class EstateStatus extends LocationInterface implements Parcelable {

    @SerializedName("Selected")
    private boolean selected;
    @SerializedName("Text")
    private String text;
    @SerializedName("Value")
    private String value;

    public boolean isSelected() {
        return selected;
    }

    public String getText() {
        return text;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String getTitle() {
        return text;
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public int getId() {
        return super.getId();
    }




    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.selected ? (byte) 1 : (byte) 0);
        dest.writeString(this.text);
        dest.writeString(this.value);
    }

    public EstateStatus() {
    }

    protected EstateStatus(Parcel in) {
        this.selected = in.readByte() != 0;
        this.text = in.readString();
        this.value = in.readString();
    }

    public static final Parcelable.Creator<EstateStatus> CREATOR = new Parcelable.Creator<EstateStatus>() {
        @Override
        public EstateStatus createFromParcel(Parcel source) {
            return new EstateStatus(source);
        }

        @Override
        public EstateStatus[] newArray(int size) {
            return new EstateStatus[size];
        }
    };
}
