package ir.delta.delta.service.ResponseModel.ads;

import java.io.Serializable;

public class AdsItemTest implements Serializable {

    private int id;
    private String title;
    private boolean isMelki;
    private int image;

    public AdsItemTest(int id, String title, boolean isMelki, int image) {
        this.id = id;
        this.title = title;
        this.isMelki = isMelki;
        this.image = image;
    }

    public boolean isMelki() {
        return isMelki;
    }

    public void setMelki(boolean melki) {
        isMelki = melki;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
