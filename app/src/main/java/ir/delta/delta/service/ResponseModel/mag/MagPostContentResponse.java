package ir.delta.delta.service.ResponseModel.mag;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.io.Serializable;
import java.util.ArrayList;

import ir.delta.delta.enums.HtmlTagEnum;
import ir.delta.delta.enums.MagFontEnum;
import ir.delta.delta.util.Constants;

public class MagPostContentResponse implements Serializable {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("link")
    private String link;
    @SerializedName("leadTitle")
    private String leadTitle;
    @SerializedName("titr")
    private String titr;
    @SerializedName("source_name")
    private String source_name;
    @SerializedName("source_link")
    private String source_link;
    @SerializedName("nodes")
    private ArrayList<MagNode> nodes;
    @SerializedName("relatedPosts")
    private ArrayList<MagPost> relatedPosts;
    @SerializedName("comments")
    private ArrayList<Comment> comments;
    @SerializedName("viewCount")
    private String viewCount;
    @SerializedName("myScore")
    private String score;
    @SerializedName("averageScore")
    private String avrageScore;
    @SerializedName("scoreCount")
    private String scoreCount;

    public String getViewCount() {
        return viewCount;
    }

    public String getScore() {
        return score;
    }

    public String getAvrageScore() {
        return avrageScore;
    }

    public String getScoreCount() {
        return scoreCount;
    }

    public String getTitr() {
        return titr;
    }

    public String getSource_name() {
        return source_name;
    }

    public String getSource_link() {
        return source_link;
    }

    public ArrayList<Comment> getComments() {
        if (comments == null) {
            comments = new ArrayList<>();
        }
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public ArrayList<MagPost> getRelatePosts() {
        return relatedPosts;
    }

    public void setRelatePosts(ArrayList<MagPost> relatePosts) {
        this.relatedPosts = relatePosts;
    }

    public String getLeadTitle() {
        return leadTitle;
    }

    public void setLeadTitle(String leadTitle) {
        this.leadTitle = leadTitle;
    }

    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public String getLink() {
        return link;
    }


    public ArrayList<MagNode> getNodes() {
        if (nodes == null) {
            return new ArrayList<>();
        }
        return nodes;
    }

    public ArrayList<MagPostRow> createMagPostRows(Context context, Typeface boldFont, Typeface mediumFont, Typeface regularFont, MagFontEnum magFont) {
        ArrayList<MagPostRow> list = new ArrayList<>();
        if (nodes != null) {
            ArrayList<MagNode> temp = new ArrayList<>();
            for (MagNode magNode : nodes) {
                switch (magNode.getTag()) {
                    case "hr":
                        createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
                        MagPostRow row = new MagPostRow();
                        row.fillHrRow(magNode.getContent());
                        list.add(row);
                        break;
                    case "script":
                        createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
                        MagPostRow row1 = new MagPostRow();
                        row1.fillScriptRow(magNode.getContent());
                        list.add(row1);
                        break;
                    case "gallery":
                        createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
                        MagPostRow row2 = new MagPostRow();
                        row2.fillGalleryRow(magNode.getContent());
                        list.add(row2);
                        break;
                    case "table":
                        createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
                        creatPostTableRow(magNode.getContent(), list);
                        break;
                    case "img":
                        createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
                        MagPostRow row4 = new MagPostRow();
                        row4.fillImgRow(magNode.getContent());
                        list.add(row4);
                        break;
                    default:
                        temp.add(magNode);
                        break;
                }
            }
            createSpannableString(context, boldFont, mediumFont, regularFont, temp, list, magFont);
        }
        return list;
    }


    private void createSpannableString(Context context, Typeface boldFont, Typeface mediumFont, Typeface regularFont, ArrayList<MagNode> temp, ArrayList<MagPostRow> list, MagFontEnum magFont) {
        MagPostRow row = new MagPostRow();
        row.fillHtmlString(context, boldFont, mediumFont, regularFont, temp, magFont);
        list.add(row);
        temp.clear();

    }

    private void creatPostTableRow(NodeContent content, ArrayList<MagPostRow> list) {
        if (content != null && content.getTable() != null && content.getVisibility() != null) {
            for (int i = 0; i < content.getTable().size(); i++) {
                if (content.getVisibility().getRows().size() == 0 ||
                        (i < content.getVisibility().getRows().size() && content.getVisibility().getRows().get(i) == 1)) {
                    MagPostRow magPostRow = new MagPostRow();
                    magPostRow.setHtmlTag(HtmlTagEnum.table);
                    ArrayList<String> row = content.getTable().get(i);
                    magPostRow.setText("");
                    for (int j = 0; j < row.size(); j++) {
                        if (content.getVisibility().getColumns().size() == 0 ||
                                (j < content.getVisibility().getColumns().size() && content.getVisibility().getColumns().get(j) == 1)) {
                            fillTableRow(row.get(j), magPostRow);
                        }
                    }
                    list.add(magPostRow);
                }
            }
        }
    }

    private void fillTableRow(String text, MagPostRow magPostRow) {
        if (!TextUtils.isEmpty(text)) {
            Document doc = Jsoup.parse(text);
            Elements childs = doc.getElementsByTag("body").get(0).children();
            if (childs.size() == 0) {
                magPostRow.appendText(text.trim());
            } else {
                Element element = childs.get(0);
                HtmlTagEnum htmlTag = HtmlTagEnum.getHtmlTag(element.tagName());
                for (Node node : element.textNodes()) {
                    if (node instanceof TextNode) {
                        magPostRow.appendText(((TextNode) node).text().trim());
                        break;
                    }
                }
                if (element.attributes() != null) {
                    if (htmlTag == HtmlTagEnum.a) {
                        for (Attribute att : element.attributes()) {
                            if (TextUtils.equals(att.getKey(), "href")) {
                                magPostRow.setHref(att.getValue());
                                break;
                            }
                        }
                    } else if (htmlTag == HtmlTagEnum.img) {
                        String width = "";
                        String height = "";
                        for (Attribute att : element.attributes()) {
                            if (TextUtils.equals(att.getKey(), "src")) {
                                magPostRow.setSrc(att.getValue());
                            } else if (TextUtils.equals(att.getKey(), "width")) {
                                width = att.getValue();
                            } else if (TextUtils.equals(att.getKey(), "height")) {
                                height = att.getValue();
                            }
                        }
                        magPostRow.setHeightFactor(Constants.getHeightFactr(width, height));
                    }
                }
            }
        }

    }
}
