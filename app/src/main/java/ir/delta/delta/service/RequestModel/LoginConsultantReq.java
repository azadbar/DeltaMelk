package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class LoginConsultantReq {

    @SerializedName("UserName")
    private String UserName;
    @SerializedName("Password")
    private String Password;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}
