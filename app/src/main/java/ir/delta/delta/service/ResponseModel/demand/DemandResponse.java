package ir.delta.delta.service.ResponseModel.demand;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DemandResponse {

    @SerializedName("successed")
    private boolean successed;
    @SerializedName("message")
    private String message;
    @SerializedName("error")
    private String error;
    @SerializedName("searchResultCount")
    private int searchResultCount;
    @SerializedName("demandSearchList")
    private ArrayList<DemandItem> demandSearchList;
//    private ArrayList<ContractTypeObject> objContractTypes;
//    private ArrayList<PropertyTypeObject> objPropertyTypes;
//    private ArrayList<RegionObject> objRegions;


    public boolean isSuccessed() {
        return successed;
    }

    public String getMessage() {
        return message;
    }

    public String getError() {
        return error;
    }

    public int getSearchResultCount() {
        return searchResultCount;
    }

    public ArrayList<DemandItem> getDemandSearchList() {
        return demandSearchList;
    }
}
