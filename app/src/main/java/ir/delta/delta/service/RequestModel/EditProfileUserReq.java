package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class EditProfileUserReq {

    @SerializedName("fullName")
    private String fullName;
    @SerializedName("locationId")
    private int locationId;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
}
