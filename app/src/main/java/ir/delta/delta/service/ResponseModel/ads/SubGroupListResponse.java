package ir.delta.delta.service.ResponseModel.ads;

import java.util.ArrayList;

public class SubGroupListResponse {

    private boolean success;
    private String message;
    private ArrayList<AdsItem> subGroupList;

    public ArrayList<AdsItem> getAdsItems() {
        return subGroupList;
    }

    public void setAdsItems(ArrayList<AdsItem> adsItems) {
        this.subGroupList = adsItems;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
