package ir.delta.delta.service.Request;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.pushNotification.MagPostContentPushResponse;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class UpdatePostViewCountService {


    private static UpdatePostViewCountService updatePostViewCountService;

    public static UpdatePostViewCountService getInstance() {
        if (updatePostViewCountService == null) {
            updatePostViewCountService = new UpdatePostViewCountService();
        }
        return updatePostViewCountService;
    }

    public void updatePostViewCount(final Resources res, int id, String token, final ResponseListener<MagPostContentPushResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient_MAG().create(ReqInterface.class).updatePostViewCount(id,token), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    MagPostContentPushResponse response = gson.fromJson(mJson, MagPostContentPushResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
