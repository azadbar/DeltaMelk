package ir.delta.delta.service.ResponseModel.mag;

import com.google.gson.annotations.SerializedName;

public class ReadPost {

    @SerializedName("id")
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
