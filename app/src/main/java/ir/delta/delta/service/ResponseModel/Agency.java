package ir.delta.delta.service.ResponseModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Agency implements Parcelable {

    @SerializedName("agencyId")
    private int agencyId;
    @SerializedName("agencyName")
    private String agencyName;
    @SerializedName("agentName")
    private String agentName;
    @SerializedName("phone")
    private String phone;
    @SerializedName("workPlace")
    private String workPlace;//تهران منطقه 1
    @SerializedName("address")
    private String address;
    @SerializedName("agencyStars")
    private int agencyStars;
    @SerializedName("agencyLink")
    private String agencyLink;
    @SerializedName("agencyLogo")
    private String agencyLogo;

    public int getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(int agencyId) {
        this.agencyId = agencyId;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkPlace() {
        return workPlace;
    }

    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAgencyStars() {
        return agencyStars;
    }

    public void setAgencyStars(int agencyStars) {
        this.agencyStars = agencyStars;
    }

    public String getAgencyLink() {
        return agencyLink;
    }

    public void setAgencyLink(String agencyLink) {
        this.agencyLink = agencyLink;
    }

    public String getAgencyLogo() {
        return agencyLogo;
    }

    public void setAgencyLogo(String agencyLogo) {
        this.agencyLogo = agencyLogo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.agencyId);
        dest.writeString(this.agencyName);
        dest.writeString(this.agentName);
        dest.writeString(this.phone);
        dest.writeString(this.workPlace);
        dest.writeString(this.address);
        dest.writeInt(this.agencyStars);
        dest.writeString(this.agencyLink);
        dest.writeString(this.agencyLogo);
    }

    public Agency() {
    }

    protected Agency(Parcel in) {
        this.agencyId = in.readInt();
        this.agencyName = in.readString();
        this.agentName = in.readString();
        this.phone = in.readString();
        this.workPlace = in.readString();
        this.address = in.readString();
        this.agencyStars = in.readInt();
        this.agencyLink = in.readString();
        this.agencyLogo = in.readString();
    }

    public static final Parcelable.Creator<Agency> CREATOR = new Parcelable.Creator<Agency>() {
        @Override
        public Agency createFromParcel(Parcel source) {
            return new Agency(source);
        }

        @Override
        public Agency[] newArray(int size) {
            return new Agency[size];
        }
    };
}
