package ir.delta.delta.service.Request.ads;

import android.content.res.Resources;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ir.delta.delta.R;
import ir.delta.delta.service.ApiClient;
import ir.delta.delta.service.ReqInterface;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.FilterAdsReq;
import ir.delta.delta.service.ResponseModel.ads.AllListAdsResponse;
import ir.delta.delta.service.ResponseModel.ads.RegionResponse;
import ir.delta.delta.service.ServerListener;
import ir.delta.delta.service.ServerTransaction;

public class GetFilterListAdsService {


    private static GetFilterListAdsService getFilterListAdsService;

    public static GetFilterListAdsService getInstance() {
        if (getFilterListAdsService == null) {
            getFilterListAdsService = new GetFilterListAdsService();
        }
        return getFilterListAdsService;
    }


    public void getFilterListAds(final Resources res, FilterAdsReq req, final ResponseListener<AllListAdsResponse> responseListener) {
        new ServerTransaction().setCall(res, ApiClient.getClient().create(ReqInterface.class).getFilterList(req), new ServerListener() {
            @Override
            public void onFailure(String str) {
                responseListener.onGetError(str);
            }

            @Override
            public void onAuthorization() {
                responseListener.onAuthorization();
            }

            @Override
            public void onSuccess(JsonObject jsonObject) {


                Gson gson = new Gson();
                try {
                    String mJsonString = jsonObject.toString();
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(mJsonString);
                    AllListAdsResponse response = gson.fromJson(mJson, AllListAdsResponse.class);
                    responseListener.onSuccess(response);
                } catch (JsonSyntaxException ex) {
                    responseListener.onGetError(res.getString(R.string.jsonError));
                }
            }
        });
    }
}
