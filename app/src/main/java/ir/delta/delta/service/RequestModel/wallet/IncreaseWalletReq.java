package ir.delta.delta.service.RequestModel.wallet;

import com.google.gson.annotations.SerializedName;

public class IncreaseWalletReq {


    @SerializedName("amount")
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
