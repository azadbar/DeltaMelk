package ir.delta.delta.service.ResponseModel.wallet;

import com.google.gson.annotations.SerializedName;

public class WalletActionsListsItem{

	@SerializedName("ActionType")
	private String actionType;

	@SerializedName("isDecrease")
	private boolean isDecrease;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	@SerializedName("ComplexAmount")
	private ComplexAmount complexAmount;

	@SerializedName("peyment_id")
	private int peymentId;

	@SerializedName("order_id")
	private int orderId;

	@SerializedName("ComplexRegistrationDate")
	private ComplexRegistrationDate complexRegistrationDate;

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public boolean isDecrease() {
		return isDecrease;
	}

	public void setDecrease(boolean decrease) {
		isDecrease = decrease;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ComplexAmount getComplexAmount() {
		return complexAmount;
	}

	public void setComplexAmount(ComplexAmount complexAmount) {
		this.complexAmount = complexAmount;
	}

	public int getPeymentId() {
		return peymentId;
	}

	public void setPeymentId(int peymentId) {
		this.peymentId = peymentId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public ComplexRegistrationDate getComplexRegistrationDate() {
		return complexRegistrationDate;
	}

	public void setComplexRegistrationDate(ComplexRegistrationDate complexRegistrationDate) {
		this.complexRegistrationDate = complexRegistrationDate;
	}
}