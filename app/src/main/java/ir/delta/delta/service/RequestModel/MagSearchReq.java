package ir.delta.delta.service.RequestModel;

import com.google.gson.annotations.SerializedName;

public class MagSearchReq {

    @SerializedName("search")
    private String search;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
