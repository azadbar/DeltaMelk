package ir.delta.delta.changePassword;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ChangePasswordService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.ChangePasswordReq;
import ir.delta.delta.service.ResponseModel.profile.ChangePassworResponse;
import ir.delta.delta.util.Constants;

/**
 * Created by a.azadbar on 9/27/2017.
 */

public class ChangePasswordActivity extends BaseActivity {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.edtNowPassword)
    CustomEditText edtNowPassword;
    @BindView(R.id.edtNewPassword)
    CustomEditText edtNewPassword;
    @BindView(R.id.edtConfirmPassword)
    CustomEditText edtConfirmPassword;
    @BindView(R.id.btnRegisterCode)
    BaseTextView btnRegisterCode;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);

        initView();

    }

    private void initView() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.no_anim);
        imgBack.setBackgroundResource(R.drawable.ic_clear_black_24dp);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.change_password));
        Window window = getWindow();
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null && window != null) {
            Point size = Constants.getScreenSize(windowManager);
            if (size.x > getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile)) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) second.getLayoutParams();
                layoutParams.width = getResources().getDimensionPixelSize(R.dimen.width_max_input_mobile);
                second.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
                second.setLayoutParams(layoutParams);

            }
        }
    }

    @OnClick({R.id.btnRegisterCode, R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegisterCode:
                if (validData()) {
                    changePasswordRequest();
                }
                break;
            case R.id.rlBack:
                finish();
                closeAnimation();
                break;
        }

    }

    private void changePasswordRequest() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        ChangePasswordReq req = new ChangePasswordReq();
        req.setCurrentPass(edtNowPassword.getValueString());
        req.setNewPass(edtNewPassword.getValueString());
        req.setConfirmNewPass(edtConfirmPassword.getValueString());
        ChangePasswordService.getInstance().getChangePassword(this, getResources(), req, new ResponseListener<ChangePassworResponse>() {
            @Override
            public void onGetError(String error) {
                if (edtNowPassword != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    showErrorDialog(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }
            @Override
            public void onSuccess(ChangePassworResponse response) {
                if (edtNowPassword != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);

                    if (response.getSuccessed()) {
                        Toast.makeText(ChangePasswordActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                        edtNowPassword.setTextBody("");
                        edtNewPassword.setTextBody("");
                        edtConfirmPassword.setTextBody("");
                    } else {
                        showErrorDialog(response.getMessage());
                    }
                }
            }
        });
    }

    public void showErrorDialog(String description) {

        CustomDialog customDialog = new CustomDialog(this);
        customDialog.setOkListener(getString(R.string.retry_text), view -> {
            customDialog.dismiss();
            changePasswordRequest();
        });
        customDialog.setCancelListener(getString(R.string.cancel), view -> customDialog.dismiss());
//        customDialog.setIcon(R.drawable.ic_bug_repoart, getResources().getColor(R.color.redColor));
        customDialog.setLottieAnim("error.json", 0);

        if (description != null) {
            customDialog.setDescription(description);
        }

        customDialog.setDialogTitle(getString(R.string.communicationError));
        customDialog.show();

    }

    private boolean validData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (edtNowPassword.getError() != null) {
            errorMsgList.add(edtNowPassword.getError());
        }

        if (edtNewPassword.getError() != null) {
            errorMsgList.add(edtNewPassword.getError());
        }

        if (edtConfirmPassword.getError() != null) {
            errorMsgList.add(edtConfirmPassword.getError());
        }

        if (!TextUtils.equals(edtNewPassword.getValueString(), edtConfirmPassword.getValueString())) {
            String message = getResources().getString(R.string.no_match_new_assword_repat_password);
            errorMsgList.add(message);
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }

        return true;
    }

    private void closeAnimation() {
        overridePendingTransition(0, R.anim.slide_out_up);
    }

    @Override
    public void onBackPressed() {
        closeAnimation();
        super.onBackPressed();
    }
}
