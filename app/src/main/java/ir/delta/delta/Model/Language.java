package ir.delta.delta.Model;

import android.util.LayoutDirection;
import android.view.View;

import java.util.Locale;

import ir.delta.delta.enums.DirectionEnum;

public class Language {

    private final String name;
    private final String code;
    private final DirectionEnum direction;


    public Language(String name, String code, DirectionEnum direction) {
        this.name = name;
        this.code = code;
        this.direction = direction;
    }

    public int getLayoutDirection() {
        if (direction == DirectionEnum.LTR) {
            return LayoutDirection.LTR;
        }
        return LayoutDirection.RTL;
    }

    public int setTextDirec() {
        if (direction == DirectionEnum.LTR) {
            return View.TEXT_DIRECTION_LTR;
        }
        return View.TEXT_DIRECTION_RTL;
    }

    public Locale getLocale() {
        Locale locale = new Locale(code);
        Locale.setDefault(locale);
        return locale;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

}
