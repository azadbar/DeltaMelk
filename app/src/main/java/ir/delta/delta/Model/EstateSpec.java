package ir.delta.delta.Model;


import ir.delta.delta.enums.InputTypeEnum;

public class EstateSpec {

    private final InputTypeEnum type;
    private final String title;
    private String unit;
    private final String hint;
    private boolean isRequired;


    public EstateSpec(InputTypeEnum type, String title, String unit, boolean isRequired, String hint) {
        this.type = type;
        this.title = title;
        this.unit = unit;
        this.hint = hint;
        this.isRequired = isRequired;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTitle() {
        return title;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public String getHint() {
        return hint;
    }


    public InputTypeEnum getType() {
        return type;
    }


}
