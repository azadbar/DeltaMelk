package ir.delta.delta.Model;

import java.util.ArrayList;

public class Pattern {
    private boolean hasParking;
    private boolean hasElevator;
    private boolean hasWarehouse;
    private boolean hasLoan;
    private ArrayList<EstateSpec> estateSpecs = new ArrayList<>();


    public void setHasParking(boolean hasParking) {
        this.hasParking = hasParking;
    }

    public void setHasElevator(boolean hasElevator) {
        this.hasElevator = hasElevator;
    }

    public void setHasWarehouse(boolean hasWarehouse) {
        this.hasWarehouse = hasWarehouse;
    }

    public void setHasLoan(boolean hasLoan) {
        this.hasLoan = hasLoan;
    }

    public void setEstateSpecs(ArrayList<EstateSpec> estateSpecs) {
        this.estateSpecs = estateSpecs;
    }

    public boolean isHasParking() {
        return hasParking;
    }

    public boolean isHasElevator() {
        return hasElevator;
    }

    public boolean isHasWarehouse() {
        return hasWarehouse;
    }

    public boolean isHasLoan() {
        return hasLoan;
    }

    public ArrayList<EstateSpec> getEstateSpecs() {
        return estateSpecs;
    }

    public void addEstateSpec(EstateSpec estateSpec) {
        estateSpecs.add(estateSpec);
    }

}
