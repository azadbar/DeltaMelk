package ir.delta.delta.Model;

import android.text.Spanned;

public class OnBoarding {

    public int image;
    public String title;
    public Spanned desc;
    public boolean isShowImage;

    public OnBoarding(int image, String title, Spanned desc, boolean isShowImage) {
        this.image = image;
        this.title = title;
        this.desc = desc;
        this.isShowImage = isShowImage;
    }
}
