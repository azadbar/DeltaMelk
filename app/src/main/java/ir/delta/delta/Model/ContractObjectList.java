package ir.delta.delta.Model;

import java.util.ArrayList;

import ir.delta.delta.service.ResponseModel.ContractObject;

public class ContractObjectList extends ArrayList<ContractObject> {


    public ContractObject getContract(int contractTypeLocalId, int locationFeatureLocalId) {
        for (ContractObject contractObject : this) {
            if (contractObject.getContractTypeLocalId() == contractTypeLocalId && contractObject.getLocationFeatureLocalId() == locationFeatureLocalId) {
                return contractObject;
            }
        }
        return null;
    }

}
