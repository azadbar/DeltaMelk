package ir.delta.delta.Model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;


import java.util.ArrayList;

import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.service.RequestModel.FilterReq;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.util.Constants;

public class Filter implements Parcelable {

    private int cityId;
    private String filterId;//for delete filter
    private ArrayList<Integer> areaIdList;
    private ArrayList<Integer> regionListFilter;
    private int contractTypeLocalId;
    private int propertyTypeLocalId;
    private int minMortgageOrTotalId;
    private int maxMortgageOrTotalId;
    private int maxPropertyTotalAreaId;
    private int minPropertyTotalAreaId;
    private int minRentOrMetricId;
    private int maxRentOrMetricId;
    private int maxPropertyAreaId;
    private int minPropertyAreaId;
    private String date;
    //////////////////*********************////////////////////////موقع خواندن از دیتابیس
    private ContractTypeEnum contractTypeEnum;
    private PropertyType propertyType;
    private ArrayList<String> areaNameList;

    public Filter() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public ArrayList<Integer> getAreaIdList() {
        if (areaIdList == null) {
            return new ArrayList<>();
        }
        return areaIdList;
    }

    public void setAreaIdList(ArrayList<Integer> areaIdList) {
        this.areaIdList = areaIdList;
    }

    public int getContractTypeLocalId() {
        return contractTypeLocalId;
    }

    public void setContractTypeLocalId(int contractTypeLocalId) {
        this.contractTypeLocalId = contractTypeLocalId;
    }

    public int getPropertyTypeLocalId() {
        return propertyTypeLocalId;
    }

    public void setPropertyTypeLocalId(int propertyTypeLocalId) {
        this.propertyTypeLocalId = propertyTypeLocalId;
    }

    public int getMinMortgageOrTotalId() {
        return minMortgageOrTotalId;
    }

    public void setMinMortgageOrTotalId(int minMortgageOrTotalId) {
        this.minMortgageOrTotalId = minMortgageOrTotalId;
    }

    public int getMaxMortgageOrTotalId() {
        return maxMortgageOrTotalId;
    }

    public void setMaxMortgageOrTotalId(int maxMortgageOrTotalId) {
        this.maxMortgageOrTotalId = maxMortgageOrTotalId;
    }

    public int getMaxPropertyTotalAreaId() {
        return maxPropertyTotalAreaId;
    }

    public void setMaxPropertyTotalAreaId(int maxPropertyTotalAreaId) {
        this.maxPropertyTotalAreaId = maxPropertyTotalAreaId;
    }

    public int getMinPropertyTotalAreaId() {
        return minPropertyTotalAreaId;
    }

    public void setMinPropertyTotalAreaId(int minPropertyTotalAreaId) {
        this.minPropertyTotalAreaId = minPropertyTotalAreaId;
    }

    public int getMinRentOrMetricId() {
        return minRentOrMetricId;
    }

    public void setMinRentOrMetricId(int minRentOrMetricId) {
        this.minRentOrMetricId = minRentOrMetricId;
    }

    public int getMaxRentOrMetricId() {
        return maxRentOrMetricId;
    }

    public void setMaxRentOrMetricId(int maxRentOrMetricId) {
        this.maxRentOrMetricId = maxRentOrMetricId;
    }

    public int getMaxPropertyAreaId() {
        return maxPropertyAreaId;
    }

    public void setMaxPropertyAreaId(int maxPropertyAreaId) {
        this.maxPropertyAreaId = maxPropertyAreaId;
    }

    public int getMinPropertyAreaId() {
        return minPropertyAreaId;
    }

    public void setMinPropertyAreaId(int minPropertyAreaId) {
        this.minPropertyAreaId = minPropertyAreaId;
    }

    public ArrayList<Integer> getRegionListFilter() {
        if (regionListFilter == null) {
            return new ArrayList<>();
        }
        return regionListFilter;
    }

    public void setRegionListFilter(ArrayList<Integer> regionListFilter) {
        this.regionListFilter = regionListFilter;
    }

    public String getAreaNames() {
        if (areaNameList == null || areaNameList.size() == 0) {
            return null;
        }
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < areaNameList.size(); i++) {
            text.append(areaNameList.get(i));
            if (i < areaNameList.size() - 1) {
                text.append(",");
            }
        }
        return text.toString();
    }

    public void setAreaNameList(ArrayList<String> areaNameList) {
        this.areaNameList = areaNameList;
    }

    public ContractTypeEnum getContractTypeEnum() {
        return contractTypeEnum;
    }

    public void setContractTypeEnum(ContractTypeEnum contractTypeEnum) {
        this.contractTypeEnum = contractTypeEnum;
    }

    public PropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(PropertyType propertyType) {
        this.propertyType = propertyType;
    }


    public String getFilterId() {
        return filterId;
    }


    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public void setFilterRequest(Context context, FilterReq req, int start, int offset) {
        req.setLocationId(Constants.getCity(context).getId());
        req.setAreaIds(getAreaIdList());
        req.setContractTypeLocalId(getContractTypeLocalId());
        req.setPropertyTypeLocalId(getPropertyTypeLocalId());
        req.setMinMortgageOrTotalId(getMinMortgageOrTotalId());
        req.setMaxMortgageOrTotalId(getMaxMortgageOrTotalId());
        req.setMinRentOrMetricId(getMinRentOrMetricId());
        req.setMaxRentOrMetricId(getMaxRentOrMetricId());
        req.setMinPropertyAreaId(getMinPropertyAreaId());
        req.setMaxPropertyAreaId(getMaxPropertyAreaId());
        req.setMinPropertyTotalAreaId(getMinPropertyTotalAreaId());
        req.setMaxPropertyTotalAreaId(getMaxPropertyTotalAreaId());
        req.setRegionListFilter(getRegionListFilter());
        req.setHasElevator(false);
        req.setHasLoan(false);
        req.setHasParking(false);
        req.setHasStore(false);
        req.setStartIndex(start);
        req.setOffset(offset);
        if (Constants.getUser(context) != null) {
            if (Constants.getUser(context).isGeneral()) {
                req.setLogedInUserTokenOrId(Constants.getUser(context).getUserToken());
            } else {
                req.setLogedInUserTokenOrId(String.valueOf(Constants.getUser(context).getUserId()));
            }
        } else {
            req.setLogedInUserTokenOrId("");
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.cityId);
        dest.writeString(this.filterId);
        dest.writeList(this.areaIdList);
        dest.writeList(this.regionListFilter);
        dest.writeInt(this.contractTypeLocalId);
        dest.writeInt(this.propertyTypeLocalId);
        dest.writeInt(this.minMortgageOrTotalId);
        dest.writeInt(this.maxMortgageOrTotalId);
        dest.writeInt(this.maxPropertyTotalAreaId);
        dest.writeInt(this.minPropertyTotalAreaId);
        dest.writeInt(this.minRentOrMetricId);
        dest.writeInt(this.maxRentOrMetricId);
        dest.writeInt(this.maxPropertyAreaId);
        dest.writeInt(this.minPropertyAreaId);
        dest.writeString(this.date);
        dest.writeInt(this.contractTypeEnum == null ? -1 : this.contractTypeEnum.ordinal());
        dest.writeParcelable(this.propertyType, flags);
        dest.writeStringList(this.areaNameList);
    }

    protected Filter(Parcel in) {
        this.cityId = in.readInt();
        this.filterId = in.readString();
        this.areaIdList = new ArrayList<>();
        in.readList(this.areaIdList, Integer.class.getClassLoader());
        this.regionListFilter = new ArrayList<>();
        in.readList(this.regionListFilter, Integer.class.getClassLoader());
        this.contractTypeLocalId = in.readInt();
        this.propertyTypeLocalId = in.readInt();
        this.minMortgageOrTotalId = in.readInt();
        this.maxMortgageOrTotalId = in.readInt();
        this.maxPropertyTotalAreaId = in.readInt();
        this.minPropertyTotalAreaId = in.readInt();
        this.minRentOrMetricId = in.readInt();
        this.maxRentOrMetricId = in.readInt();
        this.maxPropertyAreaId = in.readInt();
        this.minPropertyAreaId = in.readInt();
        this.date = in.readString();
        int tmpContractTypeEnum = in.readInt();
        this.contractTypeEnum = tmpContractTypeEnum == -1 ? null : ContractTypeEnum.values()[tmpContractTypeEnum];
        this.propertyType = in.readParcelable(PropertyType.class.getClassLoader());
        this.areaNameList = in.createStringArrayList();
    }

    public static final Creator<Filter> CREATOR = new Creator<Filter>() {
        @Override
        public Filter createFromParcel(Parcel source) {
            return new Filter(source);
        }

        @Override
        public Filter[] newArray(int size) {
            return new Filter[size];
        }
    };
}
