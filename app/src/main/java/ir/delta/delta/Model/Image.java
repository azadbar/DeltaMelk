package ir.delta.delta.Model;

import android.net.Uri;

import ir.delta.delta.enums.ImageType;

public class Image {

    private ImageType imageType;
    private Uri uri;
    private String url;

    public Image(ImageType imageType, Uri uri, String url) {
        this.imageType = imageType;
        this.uri = uri;
        this.url = url;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
