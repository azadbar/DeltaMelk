package ir.delta.delta.Model;

import ir.delta.delta.enums.ContactDeltaTypeEnum;

public class ContactDelta {

    private int id;
    private int image;
    private String title;
    private ContactDeltaTypeEnum type;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ContactDeltaTypeEnum getType() {
        return type;
    }

    public void setType(ContactDeltaTypeEnum type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
