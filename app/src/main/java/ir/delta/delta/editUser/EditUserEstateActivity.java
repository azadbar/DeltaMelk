package ir.delta.delta.editUser;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.edit.EditEstateFragment;

public class EditUserEstateActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estate_edit);
        EditUserEstateFragment editEstateFragment = new EditUserEstateFragment();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            long id = bundle.getLong("id");
            String title = bundle.getString("title");
            if (id > 0) {
                Bundle b = new Bundle();
                b.putLong("id", id);
                bundle.putString("title", title);
                editEstateFragment.setArguments(b);
            } else {
                finish();
            }
        }
        FragmentManager frgMgr = getSupportFragmentManager();
        FragmentTransaction frgTrans = frgMgr.beginTransaction();
        frgTrans.replace(R.id.frameLayout, editEstateFragment);
        frgTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        frgTrans.commit();
    }
}
