package ir.delta.delta.editUser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.yalantis.ucrop.UCrop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.Model.EstateSpec;
import ir.delta.delta.Model.Image;
import ir.delta.delta.Model.Pattern;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.registerEstate.GlideImageTarget;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomMultiLineEditText;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.database.TransactionRegion;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.edit.ImageAdapter;
import ir.delta.delta.edit.ImageDeleteAdapter;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.ImageType;
import ir.delta.delta.enums.InputTypeEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.payment.ServerErrorListener;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.EditUserEstateService;
import ir.delta.delta.service.Request.EditUserPropertyService;
import ir.delta.delta.service.Request.ImageUploadService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.EditEstateReq;
import ir.delta.delta.service.RequestModel.EditPropertyReq;
import ir.delta.delta.service.RequestModel.UploadImageReq;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.service.ResponseModel.EditPropertyResponse;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.service.ResponseModel.Region;
import ir.delta.delta.service.ResponseModel.UploadImageResponse;
import ir.delta.delta.service.ResponseModel.editEstate.EditEstateItem;
import ir.delta.delta.service.ResponseModel.editEstate.EditEstateResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.EqualSpacingItemDecoration;
import ir.delta.delta.util.FileReaderUri;
import ir.delta.delta.util.PermissionHandler;
import ir.delta.delta.util.PreferencesData;

import static android.app.Activity.RESULT_OK;

public class EditUserEstateFragment extends BaseFragment implements ImageAdapter.OnItemClickListener, ImageAdapter.DeleteOnItemClickListener, ImageAdapter.AddOriginalImage, ImageDeleteAdapter.onItemClick, ServerErrorListener, GlideImageTarget.OnImageLoader {


    @BindView(R.id.section_one)
    BaseLinearLayout sectionOne;
    @BindView(R.id.tvTextDelete)
    BaseTextView tvTextDelete;
    @BindView(R.id.rvDelete)
    RecyclerView rvDelete;
    private EditEstateResponse response;
    private final String AREAS = "areas";
    private final String PROPERTY = "property";
    private final String REGIONS = "regions";
    Unbinder unbinder;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;

    @BindView(R.id.region_layout)
    LocationView regionLayout;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtEmail)
    CustomEditText edtEmail;
    @BindView(R.id.btnPaking)
    BaseRelativeLayout btnPaking;
    @BindView(R.id.btnElevator)
    BaseRelativeLayout btnElevator;
    @BindView(R.id.btnWarehouse)
    BaseRelativeLayout btnWarehouse;
    @BindView(R.id.btnLoan)
    BaseTextView btnLoan;
    @BindView(R.id.rvSelectedPic)
    RecyclerView rvSelectedPic;
    @BindView(R.id.llSelectPic)
    BaseLinearLayout llSelectPic;
    @BindView(R.id.btnRegisterWithPhotos)
    BaseTextView btnRegisterWithPhotos;
    @BindView(R.id.btnRegisterWithoutPhotos)
    BaseTextView btnRegisterWithoutPhotos;
    @BindView(R.id.llButton)
    BaseLinearLayout llButton;
    @BindView(R.id.llFacilities)
    BaseLinearLayout llFacilities;
    @BindView(R.id.section_three)
    BaseLinearLayout sectionThree;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.tvTxtDescMaxSelectPic)
    BaseTextView tvTxtDescMaxSelectPic;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.scroolView)
    ScrollView scroolView;
    @BindView(R.id.imgCheckParking)
    BaseImageView imgCheckParking;
    @BindView(R.id.tvParking)
    BaseTextView tvParking;
    @BindView(R.id.imgElevator)
    BaseImageView imgElevator;
    @BindView(R.id.tvElevator)
    BaseTextView tvElevator;
    @BindView(R.id.imgWarehouse)
    BaseImageView imgWarehouse;
    @BindView(R.id.tvWarehouse)
    BaseTextView tvWarehouse;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.cvRegisterWithPhotos)
    CardView cvRegisterWithPhotos;
    @BindView(R.id.cvRegisterWithoutPhotos)
    CardView cvRegisterWithoutPhotos;
    @BindView(R.id.viewTopFacility)
    View viewTopFacility;
    @BindView(R.id.viewDownFacility)
    View viewDownFacility;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.title)
    BaseTextView titleText;

    private Region selectedRegion;

    private final ArrayList<Image> deleteImages = new ArrayList<>();
    private final ArrayList<Image> imageList = new ArrayList<>();
    private final ArrayList<String> imageNames = new ArrayList<>();
    private ImageAdapter adapter;
    private int maxImageCount = 0;
    private boolean isParkingSelected;
    private boolean isElevatorSelected;
    private boolean isWarehouseSelected;
    private boolean isLoanSelected;
    private EditPropertyReq req;
    private Uri imageUri;
    private final int REQUEST_CODE_PERMISSION = 2;
    private long id;

    private EditEstateItem editEstate;
    private ContractObject selectedContractType;
    private Area selectedArea;
    private PropertyType selectedPropertyType;
    private ImageDeleteAdapter deleteAdapter;
    private ArrayList<Uri> newImages;
    private String preImageName;
    private int index = 0;

    public EditUserEstateFragment() {
    }


    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_edit_estate, container, false);
        unbinder = ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getLong("id");
        }
        getDataEditView();
        tvTextDelete.setVisibility(View.GONE);
        maxImageCount = PreferencesData.getMaxImageCount(getActivity());
        checkArrowRtlORLtr(imgBack);
        finishFragment(rlBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getResources().getString(R.string.edit_file));
        tvFilterDetail.setVisibility(View.GONE);

        rlChangeCity.setVisibility(View.GONE);
        hideInputKeyboard();
        ///
        ParkingChanged();
        ElevatorChanged();
        WarehouseChanged();
        LoanChanged();
        //
        regionLayout.setImgInfo(R.drawable.ic_location_gray);
        regionLayout.setTxtTitle(getString(R.string.region_text)+getString(R.string.dots));
        regionLayout.setHint(getString(R.string.your_region));
        //
        btnRegisterWithoutPhotos.setText(getString(R.string.register_without_pic) + "(" + Constants.convertNumberToDecimal((double) PreferencesData.getDepositRegisterPrice(getActivity()) / 10) + getString(R.string.toman) + ")");
        //
        tvTxtDescMaxSelectPic.setText(getString(R.string.max_of_select_photos_desc, maxImageCount));
        //
        btnLoan.setVisibility(View.GONE);
        //

        loadingView.setButtonClickListener(view1 -> getDataEditView());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() == null) {
            return;
        }
        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            tvCityTitle.setText(Constants.getCity(getActivity()).getName());
            scroolView.scrollTo(0, 0);
            scroolView.post(() -> {
                if (scroolView != null) {
                    scroolView.fullScroll(ScrollView.FOCUS_UP);
                }
            });
        }

    }

    private void getDataEditView() {
        scroolView.setVisibility(View.GONE);
        loadingView.showLoading(false);
        enableDisableViewGroup(root, false);
        EditEstateReq req = new EditEstateReq();
        req.setId(id);
        EditUserEstateService.getInstance().editUserEstate(getActivity(), getResources(), req, new ResponseListener<EditEstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    loadingView.stopLoading();
                    enableDisableViewGroup(root, true);
                    showErrorDialog(error, 2);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(EditEstateResponse response) {
                if (getView() != null && getActivity() != null) {
                    loadingView.stopLoading();
                    enableDisableViewGroup(root, true);
                    if (!response.isSuccessed() && response.getDataItem() != null) {//TODO check
                        EditUserEstateFragment.this.editEstate = response.getDataItem();
                        fillViews();
                    } else {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    private void fillViews() {
        scroolView.setVisibility(View.VISIBLE);
        edtName.setTextBody(editEstate.getLastName());
        edtMobile.setTextBody(editEstate.getMobile());
        edtMobile.setFocusable(false);
        edtMobile.setEnabled(false);
        edtMobile.setBackgroundColor(Color.TRANSPARENT);
        edtEmail.setTextBody(editEstate.getEmail());
        setImageList();

        Area area = TransactionArea.getInstance().getAreaById(getActivity(), editEstate.getLocationId());
        City city;
        if (area != null) {
            selectedArea = area;
            city = TransactionCity.getInstance().getCityById(getActivity(), area.getParentId());
            regionSelected(TransactionRegion.getInstance().getRegionBy(getActivity(), editEstate.getRegionId()));
        } else {
            city = TransactionCity.getInstance().getCityById(getActivity(), editEstate.getLocationId());
            selectedArea = null;
            regionSelected(null);
        }

        if (city != null) {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(editEstate.getContractTypeLocalId(), city.getLocationFeatureLocalId());
        } else {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(editEstate.getContractTypeLocalId(), 1);
        }

        if (selectedContractType != null) {
            selectedPropertyType = selectedContractType.getPropertyTypeBy(editEstate.getPropertyTypeLocalId());
            if (selectedPropertyType != null) {
                titleText.setText(selectedContractType.getType().getTitle(getResources()) + " " + selectedPropertyType.getTitle() + " " + editEstate.getLocaionName());
                if (editEstate.isWithoutImageMode()) {
                    cvRegisterWithPhotos.setVisibility(View.VISIBLE);
                    btnRegisterWithPhotos.setVisibility(View.VISIBLE);
                    btnRegisterWithPhotos.setText(getResources().getString(R.string.edit));
                    btnRegisterWithoutPhotos.setVisibility(View.GONE);
                    cvRegisterWithoutPhotos.setVisibility(View.GONE);
                } else {
                    btnRegisterWithPhotos.setVisibility(View.VISIBLE);
                    cvRegisterWithPhotos.setVisibility(View.VISIBLE);
                    if (selectedPropertyType.isImageRequired()) {
                        btnRegisterWithoutPhotos.setVisibility(View.VISIBLE);
                        cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
                        btnRegisterWithPhotos.setText(getString(R.string.register_with_pic));
                    } else {
                        btnRegisterWithoutPhotos.setVisibility(View.GONE);
                        cvRegisterWithoutPhotos.setVisibility(View.GONE);
                        btnRegisterWithPhotos.setText(getString(R.string.register_final));
                    }
                }
            } else {
                btnRegisterWithPhotos.setVisibility(View.GONE);
                btnRegisterWithoutPhotos.setVisibility(View.GONE);
                cvRegisterWithoutPhotos.setVisibility(View.GONE);
                cvRegisterWithPhotos.setVisibility(View.GONE);
            }
            setEstateSpace();
        }


    }

    private void setImageList() {
        imageList.clear();
//        if (!TextUtils.isEmpty(editEstate.getBaseImagePath())) {
//            imageList.add(new Image(ImageType.url, null, editEstate.getBaseImagePath()));
//        }

        String[] list = editEstate.getImagesName().split(",");
        for (String imgStr : list) {
            if (!TextUtils.equals(imgStr, editEstate.getBaseImagePath()) && !TextUtils.isEmpty(imgStr)) {
                imageList.add(new Image(ImageType.url, null, imgStr));
            }
        }

        int coulumCount = getResources().getInteger(R.integer.coloum_count_register_estate);
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = (int) (getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate) / Constants.imageRatio);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvSelectedPic.setHasFixedSize(true);
        adapter = new ImageAdapter(getActivity(), imageList, cellWidth, maxImageCount, this, this, this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvSelectedPic.setLayoutManager(layoutManager);
        rvSelectedPic.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvSelectedPic.setAdapter(adapter);
    }


    private void regionSelected(Region region) {
        selectedRegion = region;
        if (selectedRegion != null) {
            regionLayout.setVisibility(View.VISIBLE);
            sectionOne.setVisibility(View.VISIBLE);
            regionLayout.setValue(selectedRegion.getName(), null);
        } else {
            if (selectedArea != null && TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId()) > 0) {
                regionLayout.setVisibility(View.VISIBLE);
                sectionOne.setVisibility(View.VISIBLE);
                regionLayout.reset();
            } else {
                regionLayout.setVisibility(View.GONE);
                sectionOne.setVisibility(View.GONE);
            }
        }
    }

    private void setEstateSpace() {
        sectionThree.removeAllViews();
        if (selectedPropertyType != null && editEstate != null) {
            sectionThree.setVisibility(View.VISIBLE);
            llFacilities.setVisibility(View.VISIBLE);
            viewTopFacility.setVisibility(View.VISIBLE);
            viewDownFacility.setVisibility(View.VISIBLE);
            Pattern pattern = selectedPropertyType.getPattern(selectedContractType.getType(), getResources());
            viewTopFacility.setVisibility(pattern.isHasElevator() ? View.VISIBLE : View.GONE);
            viewDownFacility.setVisibility(pattern.isHasElevator() ? View.VISIBLE : View.GONE);
            llFacilities.setVisibility(pattern.isHasElevator() ? View.VISIBLE : View.GONE);
            btnPaking.setVisibility(pattern.isHasParking() ? View.VISIBLE : View.GONE);
            btnElevator.setVisibility(pattern.isHasElevator() ? View.VISIBLE : View.GONE);
            btnWarehouse.setVisibility(pattern.isHasWarehouse() ? View.VISIBLE : View.GONE);
            btnLoan.setVisibility(pattern.isHasLoan() ? View.GONE : View.GONE);
            isParkingSelected = editEstate.isParking();
            ParkingChanged();
            isElevatorSelected = editEstate.isElevator();
            ElevatorChanged();
            isWarehouseSelected = editEstate.isStore();
            WarehouseChanged();
            isLoanSelected = editEstate.isLoan();
            LoanChanged();

            for (int i = 0; i < pattern.getEstateSpecs().size(); i++) {
                EstateSpec spec = pattern.getEstateSpecs().get(i);
                if (InputTypeEnum.DESCRIPTION == spec.getType()) {
                    addRowMultiEditText(spec, i);
                } else {
                    addRowView(spec, i);
                }
            }
        } else {
            sectionThree.setVisibility(View.GONE);
            llFacilities.setVisibility(View.GONE);
            viewTopFacility.setVisibility(View.GONE);
            viewDownFacility.setVisibility(View.GONE);
        }

    }

    private void addRowMultiEditText(EstateSpec estateSpec, int i) {
        CustomMultiLineEditText customMultiLineEditText = new CustomMultiLineEditText(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.splash_image_size_width));
        if (i == 0) {
            lp.setMargins(0, 1, 0, 1);
        } else {
            lp.setMargins(0, 0, 0, 1);
        }

        customMultiLineEditText.setLayoutParams(lp);
        sectionThree.addView(customMultiLineEditText);
        customMultiLineEditText.setTextsTitle(estateSpec.getTitle());
        customMultiLineEditText.setTextHint(estateSpec.getHint());
        customMultiLineEditText.setInputTypeEnum(estateSpec.getType());
        customMultiLineEditText.setIsRequired(estateSpec.isRequired());
        customMultiLineEditText.setTextValue(editEstate.getDescription());
    }

    private void addRowView(EstateSpec estateSpec, int i) {
        CustomEditText customEditText = new CustomEditText(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.height_edit_text_view));
        if (i == 0) {
            lp.setMargins(0, 1, 0, 1);
        } else {
            lp.setMargins(0, 0, 0, 1);
        }

        customEditText.setLayoutParams(lp);
        sectionThree.addView(customEditText);
        customEditText.setTextsTitle(estateSpec.getTitle());
        customEditText.setTextHint(estateSpec.getHint());
        customEditText.setInputTypeEnum(estateSpec.getType());
        customEditText.setIsRequired(estateSpec.isRequired());
        customEditText.setUnit(estateSpec.getUnit());
        switch (estateSpec.getType()) {
            case RENT_PRICE:
                customEditText.setTextBody(String.valueOf(editEstate.getRentOrMetric() / 10));
                break;
            case MORTGAGE_PRICE:
            case TOTAL_PRICE:
                customEditText.setTextBody(String.valueOf(editEstate.getMortgageOrTotal() / 10));
                break;
            case FLOOR:
                customEditText.setTextBody(String.valueOf(editEstate.getFloorNumber()));
                break;
            case FLOOR_COUNT:
                customEditText.setTextBody(String.valueOf(editEstate.getCountFloor()));
                break;
            case ROOM_COUNT:
                customEditText.setTextBody(String.valueOf(editEstate.getRoomCount()));
                break;
            case YEAR_BUILT:
                customEditText.setTextBody(String.valueOf(editEstate.getYearBuilt()));
                break;
            case AREA:
                customEditText.setTextBody(String.valueOf(editEstate.getArea()));
                break;
            case TOTAL_AREA:
                customEditText.setTextBody(String.valueOf(editEstate.getTotalArea()));
                break;
            case GROUND_WIDTH:
                customEditText.setTextBody(String.valueOf(editEstate.getBarGround()));
                break;
            case ADDRESS:
                customEditText.setTextBody(String.valueOf(editEstate.getAddress()));
                break;
        }
    }

    private void ParkingChanged() {
        if (isParkingSelected) {
            tvParking.setTextColor(getResources().getColor(R.color.primaryBack));
            imgCheckParking.setImageResource(R.drawable.ic_check_box_primary);
            imgCheckParking.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        } else {
            tvParking.setTextColor(getResources().getColor(R.color.secondaryTextColor));
            imgCheckParking.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckParking.setColorFilter(getResources().getColor(R.color.secondaryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void ElevatorChanged() {
        if (isElevatorSelected) {
            tvElevator.setTextColor(getResources().getColor(R.color.primaryBack));
            imgElevator.setImageResource(R.drawable.ic_check_box_primary);
            imgElevator.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        } else {
            tvElevator.setTextColor(getResources().getColor(R.color.secondaryTextColor));
            imgElevator.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgElevator.setColorFilter(getResources().getColor(R.color.secondaryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void WarehouseChanged() {
        if (isWarehouseSelected) {
            tvWarehouse.setTextColor(getResources().getColor(R.color.primaryBack));
            imgWarehouse.setImageResource(R.drawable.ic_check_box_primary);
            imgWarehouse.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        } else {
            tvWarehouse.setTextColor(getResources().getColor(R.color.secondaryTextColor));
            imgWarehouse.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgWarehouse.setColorFilter(getResources().getColor(R.color.secondaryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void LoanChanged() {
//        if (isLoanSelected) {
//            btnLoan.setTextColor(getResources().getColor(R.color.white));
//            btnLoan.setBackground(getResources().getDrawable(R.drawable.faciliteis_backgrand_green));
//        } else {
//            btnLoan.setTextColor(getResources().getColor(R.color.secondryTextColor));
//            btnLoan.setBackground(getResources().getDrawable(R.drawable.faciliteis_backgrand_strok_gray_null));
//        }
    }


    @OnClick({R.id.region_layout,
            R.id.btnRegisterWithPhotos, R.id.btnRegisterWithoutPhotos, R.id.btnPaking, R.id.btnElevator,
            R.id.btnWarehouse, R.id.btnLoan,
            R.id.rlChangeCity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.region_layout:
                if (selectedArea != null) {
                    ArrayList<Region> regions = TransactionRegion.getInstance().getRegionsBy(getActivity(), selectedArea.getId());
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(EditUserEstateFragment.this, Constants.REQUEST_REGION_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", REGIONS);
                    bundle.putSerializable("locationType", LocationTypeEnum.REGION);
                    bundle.putParcelableArrayList("list", new ArrayList<>(regions));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    loadFragment(locationFragment, EditUserEstateFragment.class.getName());
                }
                break;
            case R.id.btnRegisterWithPhotos:
                if (!isValidData()) {
                    return;
                }
                if (imageList.size() == 0 && selectedPropertyType.isImageRequired()) {
                    Toast.makeText(getActivity(), getString(R.string.select_minimum_photo, 1), Toast.LENGTH_SHORT).show();
                } else if (imageList.size() > maxImageCount) {
                    Toast.makeText(getActivity(), getString(R.string.select_minimum_photo, maxImageCount), Toast.LENGTH_SHORT).show();
                } else {
//                    fillDepositReq(false);
//                    fillImageList();
                    index = 0;
                    imageUploadRequest();
                }
                if (getActivity() != null) {
                    Constants.hideKeyboard(getActivity());
                }

                break;
            case R.id.btnRegisterWithoutPhotos:
//                if (!isValidData()) {
//                    return;
//                }
//                fillDepositReq(true);
//                req.resetImageList();
//                sendDepositWithPayment();
//                if (getActivity() != null) {
//                    Constants.hideKeyboard(getActivity());
//                }
                break;
            case R.id.rlChangeCity:
                startCityActivity();
                break;
            case R.id.btnPaking:
                isParkingSelected = !isParkingSelected;
                ParkingChanged();
                break;
            case R.id.btnElevator:
                isElevatorSelected = !isElevatorSelected;
                ElevatorChanged();
                break;
            case R.id.btnWarehouse:
                isWarehouseSelected = !isWarehouseSelected;
                WarehouseChanged();
                break;
            case R.id.btnLoan:
                isLoanSelected = !isLoanSelected;
                LoanChanged();
                break;
        }
    }

    private void imageUploadRequest() {
        newImages = new ArrayList<>();
        for (Image image : imageList) {
            if (image.getImageType() == ImageType.local) {
                newImages.add(image.getUri());
            }
        }
        if (newImages.size() > 0) {
            preImageName = selectedContractType.getType().getTitle(getResources()) + "_" + selectedPropertyType.getTitle() + "_" + editEstate.getLocaionName();
            loadImage(index);
        } else {
            fillEditDepositReq("");
        }
    }

    private void loadImage(int index) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        Glide.with(getContext()).asBitmap().load(newImages.get(index)).into(new CustomTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                String base64 = null;
                if (resource != null) {
                    base64 = Constants.convertImageFileToBase64(resource);
                }

                if (base64 != null) {
                    UploadImageReq req = new UploadImageReq();
                    req.setPath("8");
                    req.setPreImgName(preImageName);
                    req.setImageBase64(base64);
                    req.setThmbWidth(0);
                    req.setThmbHeight(0);
                    req.setImgNumber(index + 1);
                    ImageUploadService.getInstance().imageUpload(getContext(), getResources(), req, new ResponseListener<UploadImageResponse>() {
                        @Override
                        public void onGetError(String error) {
                            roundedLoadingView.setVisibility(View.GONE);
                            enableDisableViewGroup(root, true);
                            Toast.makeText(getContext(), "خطایی پیش آمده است لطفا مجددا تلاش کنید", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onAuthorization() {
                            if (getActivity() != null) {
                                Intent intent = new Intent(getActivity(), LoginActivity.class);
                                Constants.setCurrentUser(null);
                                TransactionUser.getInstance().deleteUsr(getActivity());
                                startActivity(intent);
                                getActivity().finish();
                            }
                        }

                        @Override
                        public void onSuccess(UploadImageResponse response) {
                            roundedLoadingView.setVisibility(View.GONE);
                            enableDisableViewGroup(root, true);
                            if (response.isSuccessed()) {
                                imageNames.add(response.getHash() + "&" + response.getName());
                                if (imageNames.size() == newImages.size()) {
                                    StringBuilder finalImageNames = new StringBuilder();
                                    for (int i = 0; i < imageNames.size(); i++) {
                                        if (i == imageNames.size() - 1) {
                                            finalImageNames.append(imageNames.get(i));
                                        } else {
                                            finalImageNames.append(imageNames.get(i)).append(",");
                                        }
                                    }
                                    fillEditDepositReq(finalImageNames.toString());
                                } else {
                                    loadImage(imageNames.size());
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onLoadCleared(@Nullable Drawable placeholder) {

            }

        });
    }

    public void loadFragment(LocationSelectFragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
            fragTrans.addToBackStack(fragmentTag);
            fragTrans.commit();
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (selectedRegion == null && selectedArea != null && TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId()) > 0) {
            String message = getResources().getString(R.string.select_region);
            regionLayout.setError(message);
            errorMsgList.add(message);
        }
        if (edtName.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtName);
            }
            errorMsgList.add(edtName.getError());
        }

        if (edtMobile.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtMobile);
            }
            errorMsgList.add(edtMobile.getError());
        }

        if (edtEmail.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtEmail);
            }
            errorMsgList.add(edtEmail.getError());
        }

        CustomEditText mortgageView = null;
        CustomEditText rentView = null;
        for (int i = 0; i < sectionThree.getChildCount(); i++) {
            View view = sectionThree.getChildAt(i);
            if (view instanceof CustomEditText) {
                CustomEditText customEditText = (CustomEditText) view;
                if (customEditText.getInputTypeEnum() == InputTypeEnum.MORTGAGE_PRICE) {
                    mortgageView = customEditText;
                } else if (customEditText.getInputTypeEnum() == InputTypeEnum.RENT_PRICE) {
                    rentView = customEditText;
                } else if (customEditText.getError() != null) {
                    if (errorMsgList.size() == 0) {
                        scroolTo(customEditText);
                    }
                    errorMsgList.add(customEditText.getError());
                }
            } else if (view instanceof CustomMultiLineEditText) {
                CustomMultiLineEditText customMultiLineEditText = (CustomMultiLineEditText) view;
                if (customMultiLineEditText.getError() != null) {
                    if (errorMsgList.size() == 0) {
                        scroolTo(customMultiLineEditText);
                    }
                    errorMsgList.add(customMultiLineEditText.getError());
                }
            }
        }
        if (mortgageView != null && rentView != null) {
            checkMortgageAndRent(errorMsgList, mortgageView, rentView);
        }
        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


    private void checkMortgageAndRent(ArrayList<String> errorMsgList, CustomEditText mortgageView, CustomEditText rentView) {
        String mortgageError = mortgageView.getError();
        String rentError = rentView.getError();
        if (rentError == null && mortgageError == null) {
            long priceValue = mortgageView.getValueLong();
            long rentValue = rentView.getValueLong();
            if (priceValue <= 0 && rentValue <= 0) {
                if (errorMsgList.size() == 0) {
                    scroolTo(mortgageView);
                }
                String message = getResources().getString(R.string.select_rent_value);
                errorMsgList.add(message);
            } else {
                if (priceValue < 1000 && priceValue != 0) {
                    if (errorMsgList.size() == 0) {
                        scroolTo(mortgageView);
                    }
                    String txt = getString(R.string.the_minimum_price_and_rent);
                    errorMsgList.add(txt);
                }

                if (rentValue < 1000 && rentValue != 0) {
                    if (errorMsgList.size() == 0) {
                        scroolTo(mortgageView);
                    }
                    String txt = getString(R.string.the_minimum_price_and_rent);
                    errorMsgList.add(txt);
                }
            }
        } else {
            if (mortgageError != null) {
                errorMsgList.add(mortgageError);
            }
            if (rentError != null) {
                errorMsgList.add(rentError);
            }

        }
    }

//    private void fillDepositReq(boolean withPayment) {
//        req = new DepositReq();
//        req.setContractTypeLocalId(selectedContractType.getContractTypeLocalId());
//        req.setPropertyTypeLocalId(selectedPropertyType.getPropertyTypeLocalId());
//        req.setLocationId(selectedArea != null ? selectedArea.getId() : Constants.getCity(getActivity()).getId());
//        req.setRegionId(selectedRegion != null ? selectedRegion.getId() : 0);
//        req.setFullName(edtName.getValueString());
//        req.setMobile(edtMobile.getValueString());
//        req.setEmail(edtEmail.getValueString());
//        req.setTransitWidth(0);
//        req.setRentOrMetric(0);
//        for (int i = 0; i < sectionThree.getChildCount(); i++) {
//            View view = sectionThree.getChildAt(i);
//            if (view instanceof CustomEditText) {
//                CustomEditText customEditText = (CustomEditText) view;
//                switch (customEditText.getInputTypeEnum()) {
//                    case ADDRESS:
//                        req.setAddress(customEditText.getValueString());
//                        break;
//                    case AREA:
//                        req.setArea(customEditText.getValueInt());
//                        break;
//                    case TOTAL_AREA:
//                        req.setTotalArea(customEditText.getValueInt());
//                        break;
//                    case GROUND_WIDTH:
//                        req.setGroundWidth(customEditText.getValueInt());
//                        break;
//                    case FLOOR:
//                        req.setFloor(customEditText.getValueInt());
//                        break;
//                    case FLOOR_COUNT:
//                        req.setFloorCount(customEditText.getValueInt());
//                        break;
//                    case ROOM_COUNT:
//                        req.setRoomCount(customEditText.getValueInt());
//                        break;
//                    case YEAR_BUILT:
//                        req.setYearBuilt(customEditText.getValueInt());
//                        break;
//                    case MORTGAGE_PRICE:
//                    case TOTAL_PRICE:
//                        req.setMortgageOrTotal(customEditText.getValueLong());
//                        break;
//                    case RENT_PRICE:
//                        req.setRentOrMetric(selectedContractType.getType() == ContractTypeEnum.PURCHASE ? 0 : customEditText.getValueLong());
//                        break;
//                }
//
//            } else if (view instanceof CustomMultiLineEditText) {
//                CustomMultiLineEditText customMultiLineEditText = (CustomMultiLineEditText) view;
//                if (customMultiLineEditText.getInputTypeEnum() == InputTypeEnum.DESCRIPTION) {
//                    req.setDescription(customMultiLineEditText.getValue());
//                }
//            }
//        }
//        req.setHasParking(isParkingSelected);
//        req.setHasElevator(isElevatorSelected);
//        req.setHasStore(isWarehouseSelected);
//        req.setHasLoan(isLoanSelected);
//        req.setWithPayment(withPayment);
//    }


    private void fillEditDepositReq(String finalImages) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        req = new EditPropertyReq();
        req.setId(editEstate.getDepositId());
        req.setRegionId(selectedRegion != null ? selectedRegion.getId() : 0);
        req.setFullName(edtName.getValueString());
//        req.setMobile(edtMobile.getValueString());
//        req.setEmail(edtEmail.getValueString());
        req.setTransitWidth(0);
        req.setRentOrMetric(0);
        for (int i = 0; i < sectionThree.getChildCount(); i++) {
            View view = sectionThree.getChildAt(i);
            if (view instanceof CustomEditText) {
                CustomEditText customEditText = (CustomEditText) view;
                switch (customEditText.getInputTypeEnum()) {
                    case ADDRESS:
                        req.setAddress(customEditText.getValueString());
                        break;
                    case AREA:
                        req.setArea(customEditText.getValueInt());
                        break;
                    case TOTAL_AREA:
                        req.setTotalArea(customEditText.getValueInt());
                        break;
                    case GROUND_WIDTH:
                        req.setGroundWidth(customEditText.getValueInt());
                        break;
                    case FLOOR:
                        req.setFloor(customEditText.getValueInt());
                        break;
                    case FLOOR_COUNT:
                        req.setFloorCount(customEditText.getValueInt());
                        break;
                    case ROOM_COUNT:
                        req.setRoomCount(customEditText.getValueInt());
                        break;
                    case YEAR_BUILT:
                        req.setYearBuilt(customEditText.getValueInt());
                        break;
                    case MORTGAGE_PRICE:
                    case TOTAL_PRICE:
                        req.setMortgageOrTotal(customEditText.getValueLong());
                        break;
                    case RENT_PRICE:
                        req.setRentOrMetric(selectedContractType.getType() == ContractTypeEnum.PURCHASE ? 0 : customEditText.getValueLong());
                        break;
                }

            } else if (view instanceof CustomMultiLineEditText) {
                CustomMultiLineEditText customMultiLineEditText = (CustomMultiLineEditText) view;
                if (customMultiLineEditText.getInputTypeEnum() == InputTypeEnum.DESCRIPTION) {
                    req.setDescription(customMultiLineEditText.getValue());
                }
            }
        }
        req.setHasParking(isParkingSelected);
        req.setHasElevator(isElevatorSelected);
        req.setHasStore(isWarehouseSelected);
        req.setHasLoan(isLoanSelected);
        ArrayList<String> deleteImagesNames = new ArrayList<>();
        for (int i = 0; i < deleteImages.size(); i++) {
            if (deleteImages.get(i).getImageType() == ImageType.url) {
                deleteImagesNames.add(deleteImages.get(i).getUrl().split("\\$")[1]);
            }
        }
        req.setImagesNameDelete(deleteImagesNames);

        List<String> myList = null;
        if (!finalImages.isEmpty()) {
            myList = new ArrayList<String>(Arrays.asList(finalImages.split(",")));
            req.setImagesListAdded(myList);
        } else {
            req.setImagesListAdded(new ArrayList<>());
        }

        //TODO check for user
        EditUserPropertyService.getInstance().editUserProperty(getContext(), getResources(), req, new ResponseListener<EditPropertyResponse>() {
            @Override
            public void onGetError(String error) {
                roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(root, true);
                Toast.makeText(getContext(), "خطایی پیش آمده است لطفا مجددا تلاش کنید", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(EditPropertyResponse response) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        if (getActivity() != null) {
                            Intent intent = new Intent();
                            intent.putExtra("result", response.getMessage());
                            getActivity().setResult(Activity.RESULT_OK, intent);
                            getActivity().finish();
                        }
                    } else {
                        ArrayList<String> error = new ArrayList<>();
                        for (ModelStateErrors m : response.getModelStateErrors()) {
                            error.add(m.getMessage());
                        }
                        showInfoDialog("موارد زیر را بررسی کنید", error);
                    }
                }
            }
        });
    }

    private void showDialogForImageSelection() {
        BottomSheetMenuDialog dialog = new BottomSheetBuilder(getActivity(), R.style.AppTheme_BottomSheetDialog)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_bottom_sheet)
                .setItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.btnTakePhoto:
                            openCameraTake();
                            break;
                        case R.id.btnImageGallery:
                            openGalleryPhotos();
                            break;
                    }
                })
                .createDialog();
        dialog.show();
    }


    private void openCameraTake() {
        if (getActivity() != null) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, "دلتا");
            values.put(MediaStore.Images.Media.DESCRIPTION, "عکس خود را انتخاب کنید");
            imageUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(intent, Constants.REQUEST_IMAGE_CAPTURE);
        }
    }

    private void openGalleryPhotos() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/*");
            startActivityForResult(intent, Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY);
        } else {
            if (getActivity() != null) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                getActivity().startActivityForResult(Intent.createChooser(intent, "انتخاب عکس"), Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_REGION_CODE) {
                regionSelected(data.getParcelableExtra(REGIONS));
            } else if (requestCode == Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY) {
                getImageFromGallery(data);
            } else if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
                getImageFromCamera();
            }
        }

        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(data);
            imageList.add(new Image(ImageType.local, resultUri, null));
            adapter.notifyDataSetChanged();
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getImageFromGallery(Intent data) {
        if (getActivity() == null || data == null) {
            return;
        }
        ArrayList<Uri> list = FileReaderUri.getImagePaths(data);
        if (imageList.size() + list.size() > maxImageCount) {
            Toast.makeText(getActivity(), getString(R.string.max_of_select_photos, maxImageCount), Toast.LENGTH_SHORT).show();
            int countImage = Math.min(maxImageCount - imageList.size(), list.size());
            for (int i = 0; i < countImage; i++) {
                imageList.add(new Image(ImageType.local, list.get(i), null));
            }
            adapter.notifyDataSetChanged();
        } else {
            for (int i = 0; i < list.size(); i++) {
                imageList.add(new Image(ImageType.local, list.get(i), null));
            }
            adapter.notifyDataSetChanged();
        }

    }


    private void getImageFromCamera() {
        if (getActivity() == null) {
            return;
        }
        try {
            imageList.add(new Image(ImageType.local, imageUri, null));
            adapter.notifyDataSetChanged();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    private void fillImageList() {
//        req.resetImageList();
//        if (getActivity() != null) {
//            if (imageList.size() > 0) {
//                ArrayList<String> base64List = new ArrayList<>();
//                Glide.with(getActivity()).load(imageList.get(0)).asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).override(600, 600).into(new GlideImageTarget(0, imageList.size(), base64List, this));
//            } else {
//                sendDepositWithOutPayment();
//            }
//        }
//    }

    @Override
    public void onfinish(ArrayList<String> list, int position) {
//        if (position < imageList.size() - 1) {
//            Glide.with(getActivity()).load(imageList.get(position + 1)).asBitmap().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).override(600, 600).into(new GlideImageTarget(position + 1, imageList.size(), list, this));
//        } else {
//            req.setImagesListBase64(list);
//            System.gc();
//            sendDepositWithOutPayment();
//        }


    }

    @Override
    public void onError(int position) {

    }

    private void hideInputKeyboard() {
        if (getActivity() != null) {
            if (this.edtEmail.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            if (this.edtMobile.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            if (this.edtName.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }

    }


    @Override
    public void onItemClick(int position) {
        if (getActivity() != null) {
            if (PermissionHandler.hasAllPermissions(getActivity())) {
                if (imageList.size() < maxImageCount && position == 0) {
                    showDialogForImageSelection();
                }
            } else {
                PermissionHandler.requestPermissions(EditUserEstateFragment.this, REQUEST_CODE_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getActivity() == null) {
            return;
        }
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean hasAllPermission = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    hasAllPermission = false;
                    break;
                }
            }
            if (hasAllPermission) {
                showDialogForImageSelection();
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                PermissionHandler.showSettingPermissionPage(getActivity(), false);
            } else {
                PermissionHandler.requestPermissions(getActivity(), REQUEST_CODE_PERMISSION);
            }
        }
    }

    @Override
    public void onItemClickDelete(int position, Image image) {
        if (imageList.size() == maxImageCount) {
            imageList.remove(position);
        } else {
            imageList.remove(position - 1);
        }
        deleteImages.add(image);
        adapter.notifyDataSetChanged();

        if (deleteAdapter == null) {
            tvTextDelete.setVisibility(View.VISIBLE);
            int coulumCount = getResources().getInteger(R.integer.coloum_count_register_estate);
            int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
            int cellWidth = (getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate));
            if (getContext() != null) {

                WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
                if (windowManager != null) {

                    int screenWidth = Constants.getScreenSize(windowManager).x;
                    cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
                }
            }

            deleteAdapter = new ImageDeleteAdapter(deleteImages, cellWidth, maxImageCount, this);
            LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
            rvDelete.setLayoutManager(layoutManager);
            rvDelete.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
            rvDelete.setAdapter(deleteAdapter);
        } else {
            deleteAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onItemAddOriginalImage(int position, Image image) {

        imageList.remove(image);
        imageList.add(0, image);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void onAddImage(int position, Image image) {
        if (imageList.size() == maxImageCount) {
            Toast.makeText(getActivity(), getResources().getString(R.string.max_of_select_photos_desc, maxImageCount), Toast.LENGTH_SHORT).show();
        } else {
            deleteImages.remove(position);
            imageList.add(image);
            adapter.notifyDataSetChanged();
            deleteAdapter.notifyDataSetChanged();
        }

        if (deleteImages.size() == 0) {
            tvTextDelete.setVisibility(View.GONE);
        } else {
            tvTextDelete.setVisibility(View.VISIBLE);
        }
    }

//    private void sendDepositWithPayment() {
//        if (getActivity() == null) {
//            return;
//        }
//
//        roundedLoadingView.setVisibility(View.VISIBLE);
//        enableDisableViewGroup(root, false);
//
//        DepositRegisterService.getInstance().depositRegister(getResources(), req, new ResponseListener<DepositResponse>() {
//            @Override
//            public void onGetError(String error) {
//                if (getActivity() != null && isAdded()) {
//                    roundedLoadingView.setVisibility(View.GONE);
//                    enableDisableViewGroup(root, true);
//                    showErrorDialog(error, 0);
//                }
//            }
//
//            @Override
//            public void onSuccess(DepositResponse response) {
//                if (getActivity() != null && isAdded()) {
//                    roundedLoadingView.setVisibility(View.GONE);
//                    enableDisableViewGroup(root, true);
//                    if (response.isSuccessed() && !TextUtils.isEmpty(response.getEnOrderId())) {
//                        PaymentDialog paymentDialog = new PaymentDialog(getActivity());
//                        paymentDialog.enOrderId = response.getEnOrderId();
//                        paymentDialog.orderId = response.getOrderId();
//                        paymentDialog.paymentTypeEnum = PaymentTypeEnum.DEPOSIT_REGISTER;
//                        paymentDialog.listener = EditEstateFragment.this;
//                        paymentDialog.show();
//                    } else {
//                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
//                    }
//                }
//            }
//        });
//
//    }


//    private void sendDepositWithOutPayment() {
//
//        if (getActivity() == null) {
//            return;
//        }
//
//        roundedLoadingView.setVisibility(View.VISIBLE);
//        enableDisableViewGroup(root, false);
//
//        DepositRegisterService.getInstance().depositRegister(getResources(), req, new ResponseListener<DepositResponse>() {
//            @Override
//            public void onGetError(String error) {
//                if (getActivity() != null && isAdded()) {
//                    roundedLoadingView.setVisibility(View.GONE);
//                    enableDisableViewGroup(root, true);
//                    showErrorDialog(error, 1);
//                }
//            }
//
//            @Override
//            public void onSuccess(DepositResponse response) {
//                if (getView() != null && getActivity() != null) {
//                    roundedLoadingView.setVisibility(View.GONE);
//                    enableDisableViewGroup(root, true);
//
//                    if (response.isSuccessed()) {
//                        clearView();
//                        CustomDialog successDialog = new CustomDialog(getActivity());
//                        successDialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
//                        successDialog.setDialogTitle(getString(R.string.property_registered_with_the_code, response.getDepositId()));
//                        successDialog.setColorTitle(getResources().getColor(R.color.applyFore));
//                        successDialog.setDescription(getString(R.string.property_renewed_least_day, response.getDuration()));
//                        successDialog.setOkListener(getString(R.string.ok), view -> successDialog.dismiss());
//                        successDialog.show();
//                    } else {
//                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
//                    }
//                }
//            }
//        });
//
//    }


    public void showErrorDialog(String description, int type) {

        if (getActivity() == null) {
            return;
        }
        CustomDialog customDialog = new CustomDialog(getActivity());
        customDialog.setOkListener(getString(R.string.retry_text), view -> {
            customDialog.dismiss();
            if (type == 0) {
//                sendDepositWithPayment();
            } else if (type == 1) {
//                sendDepositWithOutPayment();
            } else if (type == 2) {
                getDataEditView();
            }
        });
        customDialog.setCancelListener(getString(R.string.cancel), view -> customDialog.dismiss());
        customDialog.setIcon(R.drawable.ic_bug_repoart, getResources().getColor(R.color.redColor));
        customDialog.setLottieAnim("error.json", 0);
        if (description != null) {
            customDialog.setDescription(description);
        }

        customDialog.setDialogTitle(getString(R.string.communicationError));
        customDialog.show();

    }


//    private DepositReq getRequest() {
//        DepositReq req = new DepositReq();
//        req.setContractTypeLocalId(1);
//        req.setPropertyTypeLocalId(1);
//        req.setLocationId(14889);
//        req.setFullName("محمد آزادبر");
//        req.setMobile("09354669385");
//        req.setEmail(null);
//        req.setAddress("تست");
//        req.setArea(20);
//        req.setTotalArea(200);
//        req.setGroundWidth(500);
//        req.setTransitWidth(10);
//        req.setFloor(1);
//        req.setFloorCount(10);
//        req.setRoomCount(30);
//        req.setYearBuilt(10);
//        req.setMortgageOrTotal(100000000);
//        req.setRentOrMetric(5000000);
//        req.setHasParking(false);
//        req.setHasElevator(false);
//        req.setHasLoan(false);
//        req.setHasStore(false);
//        req.setDescription(null);
//        req.setWithPayment(true);
//        req.resetImageList();
//        return req;
//    }


    @Override
    public void onShowErrorListener(ArrayList<ModelStateErrors> modelStateErrors, String message) {
        showErrorFromServer(modelStateErrors, message);
    }

    private void clearView() {
        req = null;
        imageList.clear();
        adapter.notifyDataSetChanged();
        isParkingSelected = false;
        tvParking.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        imgCheckParking.setImageResource(R.drawable.ic_check_box_outline_primary);
        isElevatorSelected = false;
        tvElevator.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        imgElevator.setImageResource(R.drawable.ic_check_box_outline_primary);
        isWarehouseSelected = false;
        tvWarehouse.setTextColor(getResources().getColor(R.color.secondaryTextColor));
        imgWarehouse.setImageResource(R.drawable.ic_check_box_outline_primary);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void scroolTo(View view) {
        if (scroolView != null) {
            scroolView.scrollTo(0, (int) ((View) view.getParent()).getY());
        }
    }


}
