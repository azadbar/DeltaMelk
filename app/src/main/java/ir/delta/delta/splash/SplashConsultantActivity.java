package ir.delta.delta.splash;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.BaseTransaction;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.database.TransactionContractObject;
import ir.delta.delta.database.TransactionFilter;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.dialog.ForceUpdateDialog;
import ir.delta.delta.enums.CityEnum;
import ir.delta.delta.listener.OnNetworkResponse;
import ir.delta.delta.location.LocationActivity;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ForceUpdateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.ForceUpdateReq;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ForceUpdate;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PermissionHandler;
import ir.delta.delta.util.PreferencesData;


public class SplashConsultantActivity extends BaseActivity implements OnNetworkResponse {

    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.onBoardingPic)
    BaseImageView onBoardingPic;
    @BindView(R.id.everthing_for_everthing)
    BaseTextView everthingForEverthing;
    private final int REQUEST_CODE_PERMISSION = 500;
    private int versionCode = 27; // TODO
    private boolean dbInUpdate;
    private ForceUpdate response;
//    private boolean isFirstActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

//        setImageOnBoarding();
        Bundle b = getIntent().getExtras();
       /* if (b != null) {
            isFirstActivity = b.getBoolean("isFirst");
        }*/

        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            versionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException ignored) {

        }

        BaseTransaction.openDb(this);
        if (!PreferencesData.isFilterTableUpdate(this)) {
            new ConvertFilterDatabase(this, this).execute();
        } else {
            PermissionHandler.checkNetwork(this, this);
        }

        loadingView.setButtonClickListener(view -> checkServerVersion());
        loadingView.showImage(false);

//        if (BuildConfig.isCounsultant) {
        onBoardingPic.setImageResource(R.drawable.ic_logo_delta);
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        int start = desc_two.length();
        desc_two.append("اولین سایت ملکی در ایران");
        int end = desc_two.length();
        desc_two.append("\nThe First Iranian Real Estate Website");
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.3f), start, end, 0);
        everthingForEverthing.setText(desc_two);
//        }
//        else {
//            onBoardingPic.setImageResource(R.drawable.mag);
//            SpannableStringBuilder desc_two = new SpannableStringBuilder();
//            int start = desc_two.length();
//            desc_two.append("همه چیز در مورد همه چیز\n");
//            int end = desc_two.length();
//            desc_two.append("سلامتی، آشپزی، حقوقی، دکوراسیون\n");
//            desc_two.append("گردشگری و جستجوی هر نوع ملک");
//            desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//            desc_two.setSpan(new RelativeSizeSpan(1.3f), start, end, 0);
//            everthingForEverthing.setText(desc_two);
//        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


//    private void setImageOnBoarding() {
//        Window window = getWindow();
//        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//
//        if (windowManager != null && window != null) {
//            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            Point size = Constants.getScreenSize(windowManager);
//            int height = ((size.y) - ((getStatusBarHeight() + 2 * getResources().getDimensionPixelSize(R.dimen.loading_splash))));
//            if (height > getResources().getDimensionPixelSize(R.dimen.max_onboarding_height)) {
//                height = getResources().getDimensionPixelSize(R.dimen.max_onboarding_height);
//            }
//            int width = (int) (height * (1080 / 2250.0));
//            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) onBoardingPic.getLayoutParams();
//            params.height = height;
//            params.width = width;
//            params.setMargins(0, 0, 0, 0);
//        }
//    }

    private static class ConvertFilterDatabase extends AsyncTask<String, String, String> {
        private final WeakReference<Activity> contextWeak;
        private final WeakReference<OnNetworkResponse> networkWeak;

        ConvertFilterDatabase(Activity context, OnNetworkResponse listener) {
            this.contextWeak = new WeakReference<>(context);
            this.networkWeak = new WeakReference<>(listener);

        }


        @Override
        protected String doInBackground(String... strings) {
            Activity context = contextWeak.get();
            if (context != null) {
                TransactionFilter.getInstance().updateFilterTable(context);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Activity context = contextWeak.get();
            OnNetworkResponse network = networkWeak.get();
            if (context != null && network != null) {
                PermissionHandler.checkNetwork(context, network);
            }

        }
    }

    @Override
    public void onConnectionState(boolean isConnected) {
        if (isConnected) {
            checkServerVersion();
        } else {
            loadingView.setVisibility(View.VISIBLE);
            loadingView.showError(getString(R.string.communicationError));
        }
    }

    private void checkServerVersion() {
        showLoading();
        dbInUpdate = true;//وققتی در حال آپدیت دیتابیس است دکمه بک کار نمی کند
        ForceUpdateReq req = new ForceUpdateReq(versionCode, TransactionContractObject.getInstance().getCacheVersion(this));
        req.setAppVersionCode(versionCode);
        req.setJsonCashVersion(TransactionContractObject.getInstance().getCacheVersion(this));
        new ForceUpdateService().getForceUpdate(getResources(), req, new ResponseListener<ForceUpdate>() {
            @Override
            public void onGetError(String error) {
                if (loadingView != null) {
                    loadingView.showError(null);
                }
                dbInUpdate = false;
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(SplashConsultantActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(loadingView.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(ForceUpdate response) {
                if (loadingView == null) {
                    return;
                }
                if (response.isSuccessed()) {

                    PreferencesData.saveToSharedPreferences(SplashConsultantActivity.this, response.getDepositRegisterPrice(), response.getDemandRegisterPrice(), response.getDepositRegisterImageCount());
                    PreferencesData.saveLinDownload(SplashConsultantActivity.this, !TextUtils.isEmpty(response.getShareAppLink()) ?
                            response.getShareAppLink() : "http://bit.ly/3bXhFBR");
                    if (response.isHasChanged()) {
                        SplashConsultantActivity.this.response = response;
                        checkPermissions();
                    } else {
                        updateDatabase(response);
                    }
                } else {
                    loadingView.showError(getString(R.string.communicationError));
                }
            }
        });
    }

    private void updateDatabase(ForceUpdate response) {

        if (response.getLocation() != null) {
            new Thread(() -> {
                response.getLocation().convert();
                if (BaseTransaction.isDbOpen()) {
                    new BaseTransaction("").updateTable(this, response.getLocation(), response.getNewJsonCashVersion());
                    Constants.setContractObjects(response.getLocation().getContractObjects());
                    runOnUiThread(() -> {
                        checkCurrentCity(1000);
                        dbInUpdate = false;
                    });
                }
            }).start();
        } else {
            Constants.setContractObjects(TransactionContractObject.getInstance().getContractObject(this));
            checkCurrentCity(1000);
            dbInUpdate = false;
        }
    }

    private void checkPermissions() {
        stopLoading();
//        if (PermissionHandler.hasAllPermissions(this)) {
        ForceUpdateDialog forceUpdateDialog = new ForceUpdateDialog(SplashConsultantActivity.this);
        forceUpdateDialog.setForceUpdate(response.getVersionName(), response.isForce(), response.getFeatures()
                , response.getDownloadUrl(), view -> updateDatabase(response), false);
        forceUpdateDialog.show();
//        } else {
//            PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean hasInstall = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    hasInstall = false;
                    break;
                }
            }
            if (hasInstall) {
                PermissionHandler.checkNetwork(this, this);
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                PermissionHandler.showSettingPermissionPage(this, true);
            } else {
                PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
            }
        }
    }

    public void checkCurrentCity(int delay) {
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            stopLoading();
            Intent intent = null;
//            if (BuildConfig.isCounsultant) {
            checkCurrentCity();

//            } else {
//                intent = new Intent(SplashConsultantActivity.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                if (getIntent().getExtras() != null) {
//                    intent.putExtra("postId", getIntent().getExtras().getString("postId"));
//                    intent.putExtra("link", getIntent().getExtras().getString("link"));
//                }
//                startActivity(intent);
//            }
            finish();
        }, delay);

    }


    private void askForPermissionAgainDialog() {
        CustomDialog permissionDialog = new CustomDialog(SplashConsultantActivity.this);
        permissionDialog.setOkListener(getResources().getString(R.string.retry_text), view -> {
            permissionDialog.dismiss();
            checkPermissions();
        });

        permissionDialog.setCancelListener(getResources().getString(R.string.exit_app), view -> {
            permissionDialog.dismiss();
            finish();
        });
        permissionDialog.setIcon(R.drawable.ic_bug_repoart, getResources().getColor(R.color.redColor));
        permissionDialog.setDialogTitle(getResources().getString(R.string.error_premission));
        permissionDialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        permissionDialog.setDescription(getResources().getString(R.string.description_error_permission));
        permissionDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (!dbInUpdate) {
            BaseTransaction.closeDb();
            super.onBackPressed();
        }
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
    }

    private void checkCurrentCity() {
        int cityId = PreferencesData.getCityId(this);
        City city = TransactionCity.getInstance().getCityById(this, cityId);
        if (city != null) {
            Constants.setCurrentCity(city);
            User user = Constants.getUser(this);
            Intent intent = null;
            /*if (isFirstActivity) {//for check from mag or consultant*/
            if (user != null) {
                if (BuildConfig.isCounsultant) {
                    intent = new Intent(this, BottomBarActivity.class);
                    intent.putExtra("itemsIndex", 3);
                } else {
                    intent = new Intent(this, BottomBarActivity.class);
                    intent.putExtra("itemsIndex", 0);
                }

//            } else {
//                intent = new Intent(this, LoginActivity.class);
//                intent.putExtra("isFirstActivity", true);
//            }
            } else {
                intent = new Intent(this, BottomBarActivity.class);
                intent.putExtra("itemsIndex", 0);
            }

            /*if (isFirstActivity)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
            //for notification handle when counsultant
            if (getIntent().getExtras() != null &&
                    (getIntent().getExtras().getString("postId") != null
                            || getIntent().getExtras().getString("link") != null)) {
                intent = new Intent(SplashConsultantActivity.this, SplashMagActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                if (getIntent().getExtras() != null) {
                    intent.putExtra("postId", getIntent().getExtras().getString("postId"));
                    intent.putExtra("link", getIntent().getExtras().getString("link"));
                }
            }
            startActivity(intent);
        } else {
            Intent cityActivity = new Intent(this, LocationActivity.class);
            cityActivity.putExtra("cityEnum", CityEnum.MAINCITY.getId());
            cityActivity.putExtra("isFirstLaunch", true);

            startActivity(cityActivity);
        }
    }
}
