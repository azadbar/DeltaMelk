package ir.delta.delta.splash;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.ForceUpdateDialog;
import ir.delta.delta.listener.OnNetworkResponse;
import ir.delta.delta.activity.MainActivity;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.mag.MagHomeCheckVersionService;
import ir.delta.delta.service.Request.mag.MagHomeService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.ResponseModel.mag.AppConfigResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PermissionHandler;
import ir.delta.delta.util.PreferencesData;


public class SplashMagActivity extends BaseActivity implements OnNetworkResponse {

    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.onBoardingPic)
    BaseImageView onBoardingPic;
    @BindView(R.id.everthing_for_everthing)
    BaseTextView everthingForEverthing;
    private final int REQUEST_CODE_PERMISSION = 500;
    private int appVersionCode;
    private final int seed = 30;
    private AppConfigResponse response;
    private String postId;
    private String link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
//        setImageOnBoarding();
//        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        PermissionHandler.checkNetwork(this, this);
        try {
            PackageManager manager = getPackageManager();
            PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
            appVersionCode = info.versionCode;
        } catch (PackageManager.NameNotFoundException ignored) {

        }


        loadingView.setButtonClickListener(v -> {
            if (BuildConfig.isCounsultant) {
                getMagHomeWithoutCheckVersion();
            } else {
                getMagHome();
            }
        });
        loadingView.showImage(false);

        //for check notification link postId
        Bundle b = getIntent().getExtras();
        if (b != null) {
            postId = b.getString("id");
            link = b.getString("link");
        }


        onBoardingPic.setImageResource(R.drawable.mag);
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        int start = desc_two.length();
        desc_two.append("همه چیز در مورد همه چیز\n");
        int end = desc_two.length();
       /* desc_two.append("سلامتی، آشپزی، حقوقی، دکوراسیون\n");
        desc_two.append("مد و زیبایی، گردشگری و نیازمندی ها");*/
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.4f), start, end, 0);
        everthingForEverthing.setText(desc_two);
    }


    private void getMagHome() {
        //for detected notification

        showLoading();
        MagHomeCheckVersionService.getInstance().appConfig(getResources(), appVersionCode, new ResponseListener<AppConfigResponse>() {
            @Override
            public void onGetError(String error) {
                stopLoading();
                loadingView.showError(getResources().getString(R.string.communicationError));
            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(SplashMagActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(SplashMagActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(AppConfigResponse response) {
                if (response != null) {
                    SplashMagActivity.this.response = response;
                    PreferencesData.saveLinDownload(SplashMagActivity.this, response.getShareAppLink());
                    if (response.isHasChanged()) {
                        checkPermissions();
                    } else {
                        startMainActivity();
                    }

                } else {
                    stopLoading();
                    loadingView.showError(getResources().getString(R.string.communicationError));
                }
            }
        });
    }

    private void getMagHomeWithoutCheckVersion() {
        //for detected notification

        showLoading();
        MagHomeService.getInstance().getMagHome(getResources(), seed, true, new ResponseListener<AppConfigResponse>() {
            @Override
            public void onGetError(String error) {
                stopLoading();
                loadingView.showError(getResources().getString(R.string.communicationError));
            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(SplashMagActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(SplashMagActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(AppConfigResponse response) {
                if (response != null) {
                    SplashMagActivity.this.response = response;
                    PreferencesData.saveLinDownload(SplashMagActivity.this, response.getShareAppLink());
                    if (response.isHasChanged()) {
                        checkPermissions();
                    } else {
                        startMainActivity();
                    }

                } else {
                    stopLoading();
                    loadingView.showError(getResources().getString(R.string.communicationError));
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onConnectionState(boolean isConnected) {
        if (isConnected) {
            try {
                PackageManager manager = getPackageManager();
                PackageInfo info = manager.getPackageInfo(getPackageName(), 0);
                appVersionCode = info.versionCode;
            } catch (PackageManager.NameNotFoundException ignored) {

            }
            if (BuildConfig.isCounsultant) {
                getMagHomeWithoutCheckVersion();
            } else {
                getMagHome();
            }

        } else {
            loadingView.setVisibility(View.VISIBLE);
            loadingView.showError(getString(R.string.communicationError));
        }
    }


    private void checkPermissions() {
        stopLoading();
//        if (PermissionHandler.hasAllPermissions(this)) {
        if (response.isForce()) {
            ForceUpdateDialog forceUpdateDialog = new ForceUpdateDialog(SplashMagActivity.this);
            forceUpdateDialog.setForceUpdate(response.getVersionName(),
                    response.isForce(),
                    response.getFeatures(),
                    response.getDownloadUrl(),
                    v -> startMainActivity(),
                    false);
            forceUpdateDialog.show();
        } else {
            startMainActivity();
        }

//        } else {
//            PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
//        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("appConfigResponse", response);
        bundle.putString("id", postId);
        bundle.putString("link", link);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean hasInstall = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    hasInstall = false;
                    break;
                }
            }
            if (hasInstall) {
                PermissionHandler.checkNetwork(this, this);
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                PermissionHandler.showSettingPermissionPage(this, true);
            } else {
                PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
    }

}
