package ir.delta.delta.wallet;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.registerRequest.RegisterRequestFragment;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.enums.PaymentTypeEnum;
import ir.delta.delta.payment.PaymentDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.wallet.GetWalletAmountService;
import ir.delta.delta.service.Request.wallet.IncreaseWalletAmountService;
import ir.delta.delta.service.RequestModel.wallet.IncreaseWalletReq;
import ir.delta.delta.service.ResponseModel.wallet.IncreaseWalletResponse;
import ir.delta.delta.service.ResponseModel.wallet.WalletAmountResponse;
import ir.delta.delta.util.Constants;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * Created by m.Azadbar on 10/7/2017.
 */

public class WalletDialog extends Dialog {


    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.btnCancel)
    BaseRelativeLayout btnCancel;
    @BindView(R.id.btnPayment)
    BaseRelativeLayout btnPayment;
    @BindView(R.id.edtInsertAmount)
    CustomEditText edtInsertAmount;
    @BindView(R.id.progressDialog)
    ProgressBar progressDialog;

    private View.OnClickListener onClickListener;

    public WalletDialog(@NonNull Context context) {
        super(context);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.wallet_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(true);
        setDialogSize();


    }

    private void setDialogSize() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.80, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }
    }


    @OnClick({R.id.btnCancel, R.id.btnPayment})
    public void onViewClicked(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.btnPayment:
                if (edtInsertAmount.getValueLong() > 49999)
                    increaseWalletRequest();
                else
                    Toast.makeText(getContext(), "لطفا حداقل مبلغ را وارد کنید", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void increaseWalletRequest() {
        progressDialog.setVisibility(View.VISIBLE);
        IncreaseWalletReq req = new IncreaseWalletReq();
        req.setAmount(edtInsertAmount.getValueLong());
        IncreaseWalletAmountService.getInstance().increaseWalletAmount(getContext(), req, getContext().getResources(), new ResponseListener<IncreaseWalletResponse>() {
            @Override
            public void onGetError(String error) {

            }

            @Override
            public void onSuccess(IncreaseWalletResponse response) {
                if (response.isSuccessed()) {
                    progressDialog.setVisibility(View.GONE);
                    dismiss();
                    PaymentDialog paymentDialog = new PaymentDialog(getContext());
                    paymentDialog.enOrderId = response.getEnOrderId();
                    paymentDialog.orderId = response.getOrderId();
                    paymentDialog.orderPrice = response.getOrderPrice();
                    paymentDialog.orderTitle = response.getOrderTitle();
                    paymentDialog.show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                getContext().startActivity(intent);
                dismiss();
            }
        });
    }

}

