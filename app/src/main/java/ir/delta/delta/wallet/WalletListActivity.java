package ir.delta.delta.wallet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.demand.DemandFilterActivity;
import ir.delta.delta.demand.DemandListAdapter;
import ir.delta.delta.estateDetail.ComminucationDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DemandSiteService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.wallet.GetWalletListService;
import ir.delta.delta.service.RequestModel.DemandFilterReq;
import ir.delta.delta.service.RequestModel.wallet.WalletListReq;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;
import ir.delta.delta.service.ResponseModel.demand.DemandResponse;
import ir.delta.delta.service.ResponseModel.wallet.WalletActionsListsItem;
import ir.delta.delta.service.ResponseModel.wallet.WalletListResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.Validation;

public class WalletListActivity extends BaseActivity {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rvWallet)
    RecyclerView rvWallet;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;


    private WalletListAdapter adapter;
    private WalletListReq req;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<WalletActionsListsItem> walletActionsListsItems = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_list);
        ButterKnife.bind(this);

        intView();
        getList();
    }

    private void intView() {
        checkArrowRtlORLtr(imgBack);
        rlBack.setVisibility(View.VISIBLE);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getResources().getString(R.string.financial_transactions));
        tvFilterDetail.setVisibility(View.GONE);
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (walletActionsListsItems.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getWalletListRequest();
        }
    }

    private void getWalletListRequest() {
        WalletListReq req = new WalletListReq();
        req.setOffset(offset);
        req.setStartIndex(walletActionsListsItems.size());

        GetWalletListService.getInstance().getWalletList(this, req, getResources(),  new ResponseListener<WalletListResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    onError(error);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(WalletListActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }
            @Override
            public void onSuccess(WalletListResponse response) {
                if (imgBack != null) {
                    if (rlLoding == null) {
                        return;
                    }
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getWalletActionsLists() != null) {
                        if (response.getWalletActionsLists().size() < offset) {
                            endOfList = true;
                        }
                        walletActionsListsItems.addAll(response.getWalletActionsLists());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }


    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new WalletListAdapter(walletActionsListsItems);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvWallet.setHasFixedSize(true);
            rvWallet.setLayoutManager(layoutManager);
            rvWallet.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvWallet.setAdapter(adapter);
            rvWallet.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    @OnClick({R.id.rlBack, R.id.rlFilterChange})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
        }

    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvWallet.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvWallet.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && walletActionsListsItems != null) {
            rlLoding.setVisibility(View.GONE);
            if (walletActionsListsItems.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


}
