package ir.delta.delta.wallet;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;
import ir.delta.delta.service.ResponseModel.wallet.WalletActionsListsItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class WalletListAdapter extends RecyclerView.Adapter<WalletListAdapter.ViewHolder> {


    private final ArrayList<WalletActionsListsItem> list;


    WalletListAdapter(ArrayList<WalletActionsListsItem> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        WalletActionsListsItem walletActionsListsItem = list.get(position);

        if (walletActionsListsItem.isDecrease()) {
            holder.tvActionTypeValue.setTextColor(res.getColor(R.color.redColor));
            holder.imgActionType.setBackgroundResource(R.drawable.ic_caret_down);
        } else {
            holder.tvActionTypeValue.setTextColor(res.getColor(R.color.green_dark));
            holder.imgActionType.setBackgroundResource(R.drawable.ic_caret_arrow_up);
        }
        if (!TextUtils.isEmpty(walletActionsListsItem.getActionType())) {
            holder.tvActionTypeValue.setText(walletActionsListsItem.getActionType());
        } else {
            holder.tvActionTypeValue.setText("-");
        }

        if (!TextUtils.isEmpty(walletActionsListsItem.getDescription())) {
            holder.tvTitleValue.setText(walletActionsListsItem.getDescription());
        } else {
            holder.tvTitleValue.setText("-");
        }

        if (walletActionsListsItem.getComplexRegistrationDate() != null) {
            holder.tvDateValue.setText(walletActionsListsItem.getComplexRegistrationDate().getPersianDate()
                            + " " +walletActionsListsItem.getComplexRegistrationDate().getTime());
        } else {
            holder.tvDateValue.setText("-");
        }

        if (walletActionsListsItem.getComplexAmount() != null) {
            holder.tvAmountValue.setText(walletActionsListsItem.getComplexAmount().getTomanStringToman());
        } else {
            holder.tvAmountValue.setText("-");
        }

        if (walletActionsListsItem.getOrderId() > 0) {
            holder.tvInvoiceValue.setText(walletActionsListsItem.getOrderId() + "");
        } else {
            holder.tvInvoiceValue.setText("-");
        }

        if (walletActionsListsItem.getPeymentId() > 0) {
            holder.tvOrderIdValue.setText(walletActionsListsItem.getPeymentId() + "");
        } else {
            holder.tvOrderIdValue.setText("-");
        }

        holder.itemView.setBackgroundColor(position % 2 == 0 ? holder.itemView.getResources().getColor(R.color.white) : holder.itemView.getResources().getColor(R.color.oddW));

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.root)
        BaseLinearLayout root;
        @BindView(R.id.tvActionTypeTxt)
        BaseTextView tvActionTypeTxt;
        @BindView(R.id.tvActionTypeValue)
        BaseTextView tvActionTypeValue;
        @BindView(R.id.tvTitleTxt)
        BaseTextView tvTitleTxt;
        @BindView(R.id.tvTitleValue)
        BaseTextView tvTitleValue;
        @BindView(R.id.tvDateTxt)
        BaseTextView tvDateTxt;
        @BindView(R.id.tvDateValue)
        BaseTextView tvDateValue;
        @BindView(R.id.tvAmountTxt)
        BaseTextView tvAmountTxt;
        @BindView(R.id.tvAmountValue)
        BaseTextView tvAmountValue;
        @BindView(R.id.tvInvoiceTxt)
        BaseTextView tvInvoiceTxt;
        @BindView(R.id.tvInvoiceValue)
        BaseTextView tvInvoiceValue;
        @BindView(R.id.tvOrderIdTxt)
        BaseTextView tvOrderIdTxt;
        @BindView(R.id.tvOrderIdValue)
        BaseTextView tvOrderIdValue;
        @BindView(R.id.imgActionType)
        BaseImageView imgActionType;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }

}
