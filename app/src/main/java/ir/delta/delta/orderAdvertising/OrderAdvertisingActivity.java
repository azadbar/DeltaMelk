package ir.delta.delta.orderAdvertising;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomMultiLineEditText;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.OrderAdvertisingService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.OrderAdvertisingReq;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.OrderAdvertisingResponse;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

public class OrderAdvertisingActivity extends BaseActivity {


    Unbinder unbinder;
    @BindView(R.id.text)
    BaseTextView text;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.cardName)
    CardView cardName;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.cardMobile)
    CardView cardMobile;
    @BindView(R.id.edtDesc)
    CustomMultiLineEditText edtDesc;
    @BindView(R.id.btnDone)
    BaseTextView btnDone;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;


    public OrderAdvertisingActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_order_advertising);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.order_ads);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);


        SpannableStringBuilder desc_one = new SpannableStringBuilder();
        int start = desc_one.length();
        desc_one.append("جهت سفارش تبلیغات در مجله دلتا یا معرفی کسب و کار خود " +
                "در بخش نیازمندی ها لطفا  فرم زیر را تکمیل نمایید.");
        int end = desc_one.length();
//        desc_one.append("مجله اینترنتی دلتا با شعار \"همه چیز در مورد همه چیز\" و با مطالب متنوع در بخش های سلامتی، هنر آشپزی، گردشگری، دکوراسیون، نکات حقوقی، خبر و گزارش و صـدها مطلب خوانـدنی دیگر، یکی از پرمخاطب ترین مجلات اینترنتی در سراسر کشور می باشد.\n" +
//                "این مجله هر روز با هزاران بازدیدکننده، بستر مناسبی جهت معرفی کالا و خدمات و همچنین موارد ملکی در سراسر ایران می باشد.\n");
//        int end1 = desc_one.length();
//        desc_one.append("جهت سفارش تبلیغات می توانید از طریق تکمیل فرم زیر درخواست \n" +
//                "خود را ثبت کنید تا همکاران ما در اسرع وقت با شمـا تماس بگیـرنـد.");
//        int start2 = desc_one.length();
        desc_one.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_one.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_one.setSpan(new RelativeSizeSpan(1.3f), start, end, 0);

//
//        desc_one.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.secondaryTextColor)), end, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        desc_one.setSpan(new StyleSpan(Typeface.NORMAL), end, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        desc_one.setSpan(new RelativeSizeSpan(1.2f), end, end1, 0);

//        desc_one.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), end1, start2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        desc_one.setSpan(new StyleSpan(Typeface.BOLD), end1, start2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        desc_one.setSpan(new RelativeSizeSpan(1.3f), end1, start2, 0);

        text.setText(desc_one);
    }


    @OnClick({R.id.btnDone, R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right);
                finish();
                break;
            case R.id.btnDone:
                if (isValidData())
                    orderAdvertisingRequest();
                break;
        }

    }

    private boolean isValidData() {
        ArrayList<String> message = new ArrayList<>();
        if (edtName.getError() != null) {
            message.add(edtName.getError());
        }

        if (edtMobile.getError() != null) {
            message.add(edtMobile.getError());
        }
        if (edtDesc.getError() != null) {
            message.add(edtDesc.getError());
        }

        if (message.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), message);
            return false;
        }

        return true;
    }

    private void orderAdvertisingRequest() {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        OrderAdvertisingReq req = new OrderAdvertisingReq();
        req.setSenderName(edtName.getValueString());
        req.setMobile(edtMobile.getValueString());
        req.setDescription(edtDesc.getValue());
        OrderAdvertisingService.getInstance().orderAdvertising(getResources(), req, new ResponseListener<OrderAdvertisingResponse>() {
            @Override
            public void onGetError(String error) {
                roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(root, true);
                Toast.makeText(OrderAdvertisingActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(OrderAdvertisingActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(OrderAdvertisingActivity.this);
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(OrderAdvertisingResponse response) {
                roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(root, true);
                if (response.isSuccessed()) {
                    emptyView();
                    CustomDialog dialog = new CustomDialog(OrderAdvertisingActivity.this);
                    dialog.setCancelable(true);
                    dialog.setOkListener(getResources().getString(R.string.ok), v -> dialog.dismiss());
                    dialog.setDialogTitle("سفارش شما با موفقیت ثبت شد");
                    dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                    dialog.setDescription(response.getMessage());
                    dialog.setLottieAnim("tick.json",0);
//                    dialog.setIcon(R.drawable.ic_check_circle_green_48dp, getResources().getColor(R.color.secondaryBack1));
                    dialog.show();
                } else {
                    ArrayList<String> error = new ArrayList<>();
                    for (ModelStateErrors m :
                            response.getModelStateErrors()) {
                        error.add(m.getMessage());
                    }
                    showInfoDialog(getResources().getString(R.string.check_below_item), error);
                }
            }
        });
    }

    private void emptyView() {
        edtName.setTextBody("");
        edtName.setTextHint(getResources().getString(R.string.name_sample));
        edtMobile.setTextBody("");
        edtMobile.setTextHint(getResources().getString(R.string.mobile_sample));
        edtDesc.setTextValue("");
        edtDesc.setTextHint(getResources().getString(R.string.enter_description));
    }
}
