package ir.delta.delta.tablayout;

import android.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.service.ResponseModel.AreaInfoResponse;
import ir.delta.delta.util.EqualSpacingItemDecoration;

public class AgencyFragment extends Fragment {


    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rvAgency)
    RecyclerView rvAgency;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;

    private Unbinder unbinder;
    private AreaInfoResponse response;
    private AgencyAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_agency, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle b = this.getArguments();
        if (b != null) {
            response = b.getParcelable("agency");
        }

        int culomnCount = getResources().getInteger(R.integer.coulem_count_recycle_view_agency);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), culomnCount, RecyclerView.VERTICAL, false);
        rvAgency.setLayoutManager(layoutManager);
        rvAgency.addItemDecoration(new EqualSpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_agnecy), EqualSpacingItemDecoration.GRID));

        if (response.getAboutLocationAgencyModel().size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.not_agency));
        } else {
            rootEmptyView.setVisibility(View.GONE);
            setDataAdapter();
        }

        return view;
    }

    private void setDataAdapter() {
        rvAgency.setVisibility(View.VISIBLE);
        if (adapter == null) {
            adapter = new AgencyAdapter(getActivity(), response.getAboutLocationAgencyModel());
            rvAgency.setHasFixedSize(true);
            rvAgency.setAdapter(adapter);

//            rvAgency.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                    if (dy > 0) {
//                        int visibleItemCount = layoutManager.getChildCount();
//                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
//                        int totalItemCount = layoutManager.getItemCount();
//                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
//                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
//                                getEstateList();
//                            }
//                    }
//                }
//            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
