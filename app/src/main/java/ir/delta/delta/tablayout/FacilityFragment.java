package ir.delta.delta.tablayout;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;



import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.CustomFacilityAreaInfo;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.MyJustifiedTextView;
import ir.delta.delta.database.TransactionRegion;
import ir.delta.delta.service.ResponseModel.AreaInfoResponse;
import ir.delta.delta.service.ResponseModel.Facility;
import ir.delta.delta.service.ResponseModel.Region;
import ir.delta.delta.util.EqualSpacingItemDecoration;

public class FacilityFragment extends Fragment {

    @BindView(R.id.cvDescription)
    CardView cvDescription;
    @BindView(R.id.emptyView)
    BaseTextView emptyView;
    @BindView(R.id.scrollView)
    ScrollView scrollView;

    Unbinder unbinder;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.tvDescription)
    MyJustifiedTextView tvDescription;
    @BindView(R.id.llDescription)
    BaseLinearLayout llDescription;
    @BindView(R.id.addRow)
    BaseLinearLayout addRow;
    @BindView(R.id.llRegion)
    BaseLinearLayout llRegion;
    @BindView(R.id.cvRegion)
    CardView cvRegion;
    @BindView(R.id.rvRegions)
    RecyclerView rvRegions;
    @BindView(R.id.imgRegion)
    BaseImageView imgRegion;
    @BindView(R.id.btnRegions)
    BaseRelativeLayout btnRegions;
    @BindView(R.id.viewRegion)
    View viewRegion;
    @BindView(R.id.btnDescription)
    BaseRelativeLayout btnDescription;
    @BindView(R.id.imgDescriprion)
    BaseImageView imgDescriprion;

    private boolean showRegions = true;
    private boolean showDescriptions = true;
    private AreaInfoResponse response;
    private int areaId;
    private Typeface regular;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.fragment_area_info, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle b = this.getArguments();
        if (b != null) {
            response = b.getParcelable("areaInfoResponse");
            areaId = b.getInt("areaId");
        }

        int culomnCount = getResources().getInteger(R.integer.coulem_count_recycle_view_of_facility_agency);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), culomnCount, RecyclerView.VERTICAL, false);
        rvRegions.setLayoutManager(layoutManager);
        rvRegions.addItemDecoration(new EqualSpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_1dp), EqualSpacingItemDecoration.GRID));
        rvRegions.setNestedScrollingEnabled(false);
        scrollView.setVisibility(View.GONE);
        cvDescription.setVisibility(View.VISIBLE);
        cvRegion.setVisibility(View.VISIBLE);
        addRow.setVisibility(View.VISIBLE);
        regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf");

        setDataInView();
        return view;
    }


    private void setDataInView() {
        setContext();

        setRegions();

        setLocationFacilities();
    }


    private void setContext() {
        scrollView.setVisibility(View.VISIBLE);
        if (response.getContext() != null) {
            cvDescription.setVisibility(View.VISIBLE);
            String text = response.getContext().replace("\n", "").replace("\r", "");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvDescription.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tvDescription.setText(Html.fromHtml(text));
            }
            tvDescription.setTypeface(regular);
        } else {
            cvDescription.setVisibility(View.GONE);
        }
    }

    private void setRegions() {
        ArrayList<Region> regions = TransactionRegion.getInstance().getRegionsBy(getActivity(), areaId);
        if (regions.size() == 0) {
            cvRegion.setVisibility(View.GONE);
        } else {
            cvRegion.setVisibility(View.VISIBLE);
            InfoRegionsAdapter adapter = new InfoRegionsAdapter(regions);
            rvRegions.setAdapter(adapter);
        }
    }

    private void setLocationFacilities() {

        if (response.getAboutLocationFacilities().size() > 0) {
            addRow.setVisibility(View.VISIBLE);
            for (Facility f : response.getAboutLocationFacilities()) {
                if (f.getTypeName() != null && f.getContentInfo().size() > 0) {
                    addCardViewInApp(f);
                }
            }
        } else {
            addRow.setVisibility(View.GONE);
        }
    }

    private void addCardViewInApp(Facility facility) {
        if (getActivity() == null) {
            return;
        }
        CustomFacilityAreaInfo customFacilityAreaInfo = new CustomFacilityAreaInfo(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        customFacilityAreaInfo.setLayoutParams(lp);
        addRow.addView(customFacilityAreaInfo);
        customFacilityAreaInfo.setTextTitle(facility.getTypeName());
        customFacilityAreaInfo.setListFacilities(facility.getContentInfo());
    }


    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.btnRegions, R.id.btnDescription})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.btnRegions:
                checkOpenOrClose(showRegions, rvRegions, viewRegion, imgRegion);
                showRegions = !showRegions;
                break;
            case R.id.btnDescription:
                if (showDescriptions) {
                    imgDescriprion.setImageResource(R.drawable.ic_keyboard_arrow_down_whit);
                    imgDescriprion.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
                    tvDescription.setVisibility(View.GONE);
                } else {
                    imgDescriprion.setImageResource(R.drawable.ic_keyboard_arrow_up);
                    imgDescriprion.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
                    tvDescription.setVisibility(View.VISIBLE);
                }

                showDescriptions = !showDescriptions;
                break;
        }
    }


    private void checkOpenOrClose(boolean isShow, RecyclerView recycle, View view, BaseImageView image) {
        if (isShow) {
            recycle.setVisibility(View.GONE);
            image.setImageResource(R.drawable.ic_keyboard_arrow_down_whit);
            image.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        } else {
            recycle.setVisibility(View.VISIBLE);
            view.setVisibility(View.VISIBLE);
            image.setImageResource(R.drawable.ic_keyboard_arrow_up);
            image.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
        }
    }


}
