package ir.delta.delta.tablayout;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dision.android.rtlviewpager.RTLPagerAdapter;
import com.dision.android.rtlviewpager.RTLViewPager;
import com.dision.android.rtlviewpager.Tab;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.bottomNavigation.search.LocationInterface;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.AreaInfoService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.AreaInfoReq;
import ir.delta.delta.service.ResponseModel.AreaInfoResponse;
import ir.delta.delta.util.Constants;


public class TabLayoutActivity extends BaseActivity {

    private static final int TAB_ABOUT_AREA = 1;
    private static final int TAB_ACTIVE_AGENCY = 2;

    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tool)
    AppBarLayout tool;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    RTLViewPager viewpager;
    private LocationInterface area;
    private AreaInfoResponse response;
    private Tab[] mTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout);
        ButterKnife.bind(this);
        toolbar = findViewById(R.id.toolbar);
        checkArrowRtlORLtr(imgBack);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            area = b.getParcelable("area");
        }

        loadingView.setButtonClickListener(view -> getAreaInfoData());
        getAreaInfoData();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }


    private void initTabs() {
        if (response.getAboutLocationAgencyModel().size() == 0) {
            mTabs = new Tab[]{
                    new Tab(TAB_ABOUT_AREA, getString(R.string.about_area)) {
                        @Override
                        public Fragment getFragment() {
                            FacilityFragment facilityFragment = new FacilityFragment();
                            Bundle bundle1 = new Bundle();
                            bundle1.putParcelable("areaInfoResponse", response);
                            bundle1.putInt("areaId", area.getId());
                            facilityFragment.setArguments(bundle1);
                            return facilityFragment;
                        }
                    }
            };
        } else {
            mTabs = new Tab[]{
                    new Tab(TAB_ABOUT_AREA, getString(R.string.about_area)) {
                        @Override
                        public Fragment getFragment() {
                            FacilityFragment facilityFragment = new FacilityFragment();
                            Bundle bundle1 = new Bundle();
                            bundle1.putParcelable("areaInfoResponse", response);
                            bundle1.putInt("areaId", area.getId());
                            facilityFragment.setArguments(bundle1);
                            return facilityFragment;
                        }
                    },
                    new Tab(TAB_ACTIVE_AGENCY, getString(R.string.active_agency)) {

                        @Override
                        public Fragment getFragment() {
                            AgencyFragment agencyFragment = new AgencyFragment();
                            Bundle bundle1 = new Bundle();
                            bundle1.putParcelable("agency", response);
                            agencyFragment.setArguments(bundle1);
                            return agencyFragment;
                        }
                    }
            };
        }

    }

    private void setAdapters() {
        RTLPagerAdapter mTabsAdapter = new RTLPagerAdapter(getFragmentManager(), mTabs, true);
        viewpager.setAdapter(mTabsAdapter);
        viewpager.setRtlOriented(true);
        tabs.setupWithViewPager(viewpager);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public AppBarLayout getToolbar() {
        return tool;
    }

    private void getAreaInfoData() {
        loadingView.showLoading(false);
        AreaInfoReq req = new AreaInfoReq();
        req.setAreaId(area.getId());
        AreaInfoService areaInfoService = new AreaInfoService();
        areaInfoService.getGetAreaInfo(getResources(), req, new ResponseListener<AreaInfoResponse>() {
            @Override
            public void onGetError(String error) {
                if (loadingView != null) {
                    loadingView.showError(null);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(TabLayoutActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(loadingView.getContext());
                startActivity(intent);
                finish();
            }

            @Override
            public void onSuccess(AreaInfoResponse response) {
                if (loadingView == null) {
                    return;
                }
                if (response.isSuccessed() && response.getAboutLocationFacilities() != null) {
                    loadingView.stopLoading();
                    tvTitle.setText(response.getTitle());
                    TabLayoutActivity.this.response = response;
                    initTabs();
                    setAdapters();
                    setCustomFont();
                } else {

                    loadingView.showError(null);
                }
            }
        });
    }


    @OnClick(R.id.imgBack)
    public void onViewClicked() {
        finish();
    }

    public void setCustomFont() {

        ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);

            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    //Put your font in assests folder
                    //assign name of the font here (Must be case sensitive)
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
                }
            }
        }
    }

}