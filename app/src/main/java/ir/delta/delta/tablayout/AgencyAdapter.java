package ir.delta.delta.tablayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.Agency;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class AgencyAdapter extends RecyclerView.Adapter<AgencyAdapter.ViewHolder> {


    private final Context context;

    private ArrayList<Agency> list;
//    private final OnItemBtnKnow listener;

    public AgencyAdapter(Context context, ArrayList<Agency> list) {
        this.context = context;
        this.list = list;
//        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_agency_info, parent, false);
        return new ViewHolder(itemView);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        Agency object = list.get(position);

        if (!TextUtils.isEmpty(object.getAgencyName())) {
            holder.tvAgencyName.setText(object.getAgencyName().trim());
        } else {
            holder.tvAgencyName.setText(null);
        }
        if (!TextUtils.isEmpty(object.getAgentName())) {
            createHtmlText(holder.tvManagerAgency, "<font color='#000000'> <b>" + res.getString(R.string.manager) + "</b>:</font> " + "<font color='#777777'>" + object.getAgentName().trim() + "</font>");
        } else {
            holder.tvManagerAgency.setText(null);
        }

        if (!TextUtils.isEmpty(object.getPhone())) {
            createHtmlText(holder.tvPhoneAgency, "<font color='#000000'> <b>" + res.getString(R.string.phone) + "</b>:</font> " + "<font color='#777777'>" + object.getPhone().trim() + "</font>");
        } else {
            holder.tvManagerAgency.setText(null);
        }

        if (!TextUtils.isEmpty(object.getAddress())) {
            createHtmlText(holder.tvAddressAgency, "<font color='#000000'> <b>" + res.getString(R.string.address) + "</b>:</font> " + "<font color='#777777'>" + object.getAddress().trim() + "</font>");
        } else {
            holder.tvManagerAgency.setText(null);
        }

        if (object.getAgencyLogo() != null) {
            Glide.with(context).load(object.getAgencyLogo()).placeholder(R.drawable.placeholder).into(holder.imageAgency);
        } else {
            holder.imageAgency.setImageResource(R.drawable.placeholder);
        }

        if (object.getAgencyStars() > 0) {
            holder.ratingBar.setVisibility(View.VISIBLE);
            holder.ratingBar.setRating(object.getAgencyStars());
        } else {
            holder.ratingBar.setRating(object.getAgencyStars());
        }

//        holder.bind(position, object, listener);
    }

    private void createHtmlText(BaseTextView textView, String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textView.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
        } else {
            textView.setText(Html.fromHtml(text));
        }
    }

//    private void createHtmlText(String text) {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            tvDescPayment.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY));
//        } else {
//            tvDescPayment.setText(Html.fromHtml(text));
//        }
//    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<Agency> searchResult) {
        this.list = searchResult;
        notifyDataSetChanged();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageAgency)
        BaseImageView imageAgency;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.tvAgencyName)
        BaseTextView tvAgencyName;
        @BindView(R.id.tvManagerAgency)
        BaseTextView tvManagerAgency;
        @BindView(R.id.tvPhoneAgency)
        BaseTextView tvPhoneAgency;
        @BindView(R.id.tvAddressAgency)
        BaseTextView tvAddressAgency;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

//        public void bind(int position, Agency agency, final OnItemBtnKnow listener) {
//            itemView.setOnClickListener(v -> listener.onItemClick(position, agency));
//        }

    }

//    public interface OnItemBtnKnow {
//        void onItemClick(int position, Agency agency);
//    }
}
