package ir.delta.delta.onBoarding;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingAdapter.ViewHolder> {


    private final ArrayList<OnBoarding2> list;


    OnBoardingAdapter(ArrayList<OnBoarding2> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.slide_layout_onboarding_one, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        OnBoarding2 onBoarding = list.get(position);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);


        holder.tvStatus.setText(onBoarding.getTitle());
        holder.tvDescription.setText(onBoarding.getDesc());
        if (onBoarding.isShowImage()) {
            holder.image.setVisibility(View.VISIBLE);
            Glide.with(holder.tvStatus.getContext()).load(onBoarding.getImage()).into(holder.image);
        } else {
            holder.image.setVisibility(View.GONE);
        }

        if (onBoarding.isMargin()) {
            params.setMargins(0, -20, 0, 0);
            holder.root.setLayoutParams(params);
        } else {
            params.setMargins(0, 0, 0, 0);
            holder.root.setLayoutParams(params);
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvStatus)
        BaseTextView tvStatus;
        @BindView(R.id.image)
        BaseImageView image;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.root)
        BaseRelativeLayout root;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


    }


}
