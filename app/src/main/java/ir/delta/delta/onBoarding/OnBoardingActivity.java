package ir.delta.delta.onBoarding;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.splash.SplashConsultantActivity;
import ir.delta.delta.util.PreferencesData;

public class OnBoardingActivity extends BaseActivity {


    @BindView(R.id.recycle)
    RecyclerView recycle;
    @BindView(R.id.btnStart)
    BaseTextView btnStart;

    @BindView(R.id.root)
    BaseRelativeLayout root;
    private ArrayList<OnBoarding2> list;
    private Typeface bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding2);
        ButterKnife.bind(this);

        createList();
        setAdapter();

        bold = Typeface.createFromAsset(getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf");
        btnStart.setOnClickListener(view -> {
            PreferencesData.saveIsOnBoarding(OnBoardingActivity.this, true);
            Intent splashIntent = new Intent(OnBoardingActivity.this, SplashConsultantActivity.class);
            startActivity(splashIntent);
            finish();
        });
        animate();
    }


    private void createList() {
        list = new ArrayList<>();
        list.add(new OnBoarding2(R.drawable.mag, getString(R.string.first), onboarding_desc_1(), false, false, false));
        list.add(new OnBoarding2(R.drawable.mag, getString(R.string.then), onboarding_desc_2(), false, false, true));
        list.add(new OnBoarding2(R.drawable.mag, getString(R.string.and_finally), onboarding_desc_3(), true, false, false));
    }


    private void setAdapter() {
        OnBoardingAdapter adapter = new OnBoardingAdapter(list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycle.setLayoutManager(layoutManager);
        recycle.setAdapter(adapter);
    }

    private Spanned onboarding_desc_1() {
        SpannableStringBuilder desc_one = new SpannableStringBuilder();
        desc_one.append("راه اندازی آژانس های املاک سازمان یافته\n");
        desc_one.append("با رویکردی جدید و بهره گیری از\n");
        desc_one.append("پیشرفته ترین سایت ملکی\n");
        int start = desc_one.length();
        desc_one.append("DELTA.ir");
        desc_one.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, desc_one.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_one.setSpan(new StyleSpan(Typeface.BOLD), start, desc_one.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_one.setSpan(new RelativeSizeSpan(1.5f), start, desc_one.length(), 0);


//        String text = "";
//        text += "<font color='#000000'>" + "راه اندازی آژانس های املاک سازمان یافته" + "</font>";
//        text += "<br/><font color='#000000'>" + "با رویکردی جدید و بهره گیری از " + "</font>";
//        text += "<br/><font  color='#000000'>" + "پیشرفته ترین سایت ملکی " + "</font>";
//        text += "<br/><font size='6' color='#E30427'><b>" + getString(R.string.delta_ir) + "</b></font>";
//        text += "<br/><font color='#E30427'> <big><b>DELTA.ir</b></big></font>";
        return desc_one;
    }

    private Spanned onboarding_desc_2() {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append("تخصیص سایت دلتا برای همگان و\n");
        int start = desc_two.length();
        desc_two.append("آژانس های املاک سراسر کشور\n");
        int end = desc_two.length();
        desc_two.append("جهت ثبت و جستجوی هر نوع ملک");

        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.NORMAL), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1f), start, desc_two.length(), 0);


//        String text = "";
//        text += "<font color='#000000'>" + "تخصیص سایت دلتا برای همگان و" + "</font>";
//        text += "<br/><font color='#E30427'>" + "آژانس های املاک سراسر کشور" + "</font>";
//        text += "<br/><font color='#000000'>" + "جهت ثبت و جستجوی هر نوع ملک" + "</font>";
        return desc_two;
    }

    private Spanned onboarding_desc_3() {

        SpannableStringBuilder desc_three = new SpannableStringBuilder();
        desc_three.append("با مطالبی آنچنان مفید که\n");
        int start = desc_three.length();
        desc_three.append("ثبت و جستجوی ملک\n");
        int end = desc_three.length();
        int start1 = desc_three.length();
        desc_three.append("جزء کوچکی ");
        int end1 = desc_three.length();
        desc_three.append("از آن است، رونمایی شد.\n");
        desc_three.append("از ");
        int start2 = desc_three.length();
        desc_three.append("سلامتی، گردشگری، موارد حقوقی، دکوراسیون\n");
        int end2 = desc_three.length();

        desc_three.append("تا ");
        int start3 = desc_three.length();
        desc_three.append("جستجوی ملک و مطالب دیگر را در آن خواهید یافت.");
        int end3 = desc_three.length();

        desc_three.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        desc_three.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new StyleSpan(Typeface.BOLD), start1, end1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        desc_three.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new StyleSpan(Typeface.NORMAL), start2, end2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new RelativeSizeSpan(0.9f), start2, end2, 0);

        desc_three.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start3, end3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new StyleSpan(Typeface.NORMAL), start3, end3, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_three.setSpan(new RelativeSizeSpan(0.9f), start3, end3, 0);
//        String text = "";
//        text += "<font color='#000000'>" + "با مطالبی آنچنان مفید که" + "</font>";
//        text += "<br/><font color='#E30427'><b>" + "ثبت و جستجوی ملک" + "</b></font>";
//        text += "<br/><font color='#000000'><b>" + "جزء کوچکی " + "</b></font>";
//        text += "<font color='#000000'>" + "از آن است، رونمایی شد." + "</font>";
//        text += "<br/><font color='#000000'>" + "از " + "</font>" + "<font color='#E30427'> سلامتی، گردشگری، موارد حقوقی، دکوراسیون </font>";
//        text += "<br/><font color='#000000'>تا </font>";
//        text += "<font color='#E30427'>صــدهـا مطلــب دیگــر را در آن خـواهیـد یـافـــت. </font>";
        return desc_three;
    }
//
//    public void animate(){
//        ObjectAnimator a = ObjectAnimator.ofInt(btnStart, "textColor", Color.GREEN, Color.RED);
//        a.setInterpolator(new LinearInterpolator());
//        a.setDuration(900);
//        a.setRepeatCount(ValueAnimator.INFINITE);
//        a.setRepeatMode(ValueAnimator.REVERSE);
//        a.setEvaluator(new ArgbEvaluator());
//        AnimatorSet t = new AnimatorSet();
//        t.play(a);
//        t.start();
//    }

//    private void animate() {
////        btnStart.setBackground(getResources().getDrawable(R.drawable.strock_red));
//        final GradientDrawable gd = (GradientDrawable) btnStart.getBackground();
//        int primaryColor = getResources().getColor(R.color.white);
//        int secondaryColor = getResources().getColor(R.color.colorAccent);
//        final ValueAnimator animator2 = ValueAnimator.ofInt(primaryColor, secondaryColor);
//        animator2.setEvaluator(new ArgbEvaluator());
//        animator2.addUpdateListener(animation -> gd.setColor((int) animation.getAnimatedValue()));
//        animator2.setRepeatCount(ValueAnimator.INFINITE);
//        animator2.setRepeatMode(ValueAnimator.REVERSE);
//        AnimatorSet set = new AnimatorSet();
//        set.playTogether(animator2);
//        set.setInterpolator(new AccelerateDecelerateInterpolator());
//        set.setDuration(2000);
//        set.start();
//
//        ObjectAnimator a = ObjectAnimator.ofInt(btnStart, "textColor",
//                getResources().getColor(R.color.colorAccent), getResources().getColor(R.color.white));
//        a.setInterpolator(new LinearInterpolator());
//        a.setDuration(2000);
//        a.setRepeatCount(ValueAnimator.INFINITE);
//        a.setRepeatMode(ValueAnimator.REVERSE);
//        a.setEvaluator(new ArgbEvaluator());
//        AnimatorSet t = new AnimatorSet();
//        t.play(a);
//        t.start();
//    }
//
    private void animate() {
        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(btnStart,
                PropertyValuesHolder.ofFloat("scaleX", 1.1f),
                PropertyValuesHolder.ofFloat("scaleY", 1f));
        scaleDown.setDuration(1100);
        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);
        scaleDown.start();
    }
//
//    private void animate() {
//        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
//        btnStart.startAnimation(pulse);
//    }

//        private void animate() {
//        btnStart.setBackground(getResources().getDrawable(R.drawable.strock_red));
//        final GradientDrawable gd = (GradientDrawable) btnStart.getBackground();
//
//        int buttonHeight = getResources().getDimensionPixelOffset(R.dimen.button_height);
//        int primaryColor = getResources().getColor(R.color.primaryBack);
//        int secondaryColor = getResources().getColor(R.color.redColor);
//        int strokeWidth = getResources().getDimensionPixelOffset(R.dimen.margin_2dp);
//        //================
//        // MARK: - anim 1
//        //================
////        final ValueAnimator animator1 = ValueAnimator.ofFloat(buttonHeight / 2.5f, buttonHeight / 2.0f);
////        animator1.addUpdateListener(animation -> gd.setCornerRadius((float) animation.getAnimatedValue()));
////        animator1.setRepeatCount(ValueAnimator.INFINITE);
////        animator1.setRepeatMode(ValueAnimator.REVERSE);
//
//        //================
//        // MARK: - anim 2
//        //================
//        final ValueAnimator animator2 = ValueAnimator.ofInt(primaryColor, secondaryColor);
//        animator2.setEvaluator(new ArgbEvaluator());
//        animator2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                gd.setColor((int) animation.getAnimatedValue());
//            }
//        });
//        animator2.setRepeatCount(ValueAnimator.INFINITE);
//        animator2.setRepeatMode(ValueAnimator.REVERSE);
//
//        //================
//        // MARK: - anim 3
//        //================
//        final ValueAnimator animator3 = ValueAnimator.ofInt(secondaryColor, primaryColor);
//        animator3.setEvaluator(new ArgbEvaluator());
//        animator3.addUpdateListener(animation -> gd.setStroke(strokeWidth, (int) animation.getAnimatedValue()));
//        animator3.setRepeatCount(ValueAnimator.INFINITE);
//        animator3.setRepeatMode(ValueAnimator.REVERSE);
//
//        AnimatorSet set = new AnimatorSet();
//        set.playTogether(animator2 , animator3);
//        set.setInterpolator(new AccelerateDecelerateInterpolator());
//        set.setDuration(800);
//        set.start();
//    }

}
