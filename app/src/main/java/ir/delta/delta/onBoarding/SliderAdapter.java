package ir.delta.delta.onBoarding;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ir.delta.delta.Model.OnBoarding;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;

public class SliderAdapter extends PagerAdapter {

    private final ArrayList<OnBoarding> list;

    SliderAdapter(ArrayList<OnBoarding> list) {
        this.list = list;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        ViewGroup currentView;

        if (position == 0) {
            currentView = (ViewGroup) LayoutInflater.from(container.getContext()).inflate(R.layout.slide_layout_onboarding_one, container, false);
            OnBoarding item = list.get(position);
            BaseTextView title = currentView.findViewById(R.id.tvStatus);
            BaseTextView tvDescription = currentView.findViewById(R.id.tvDescription);
            title.setText(item.title);
            tvDescription.setText(item.desc);
        } else {
            currentView = (ViewGroup) LayoutInflater.from(container.getContext()).inflate(R.layout.slide_layout, container, false);
            BaseImageView iamge = currentView.findViewById(R.id.image);
            BaseTextView tvDescription = currentView.findViewById(R.id.tvDescription);
            BaseTextView title = currentView.findViewById(R.id.tvStatus);
            OnBoarding item = list.get(position);
            title.setText(item.title);

            if (item.isShowImage) {
                iamge.setVisibility(View.VISIBLE);
                iamge.setImageResource(item.image);
            } else
                iamge.setVisibility(View.GONE);

            tvDescription.setText(item.desc);
        }


        container.addView(currentView);
        return currentView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((BaseRelativeLayout) object);
    }
}
