package ir.delta.delta.onBoarding;

import android.text.Spanned;

public class OnBoarding2 {

    private int image;
    private String title;
    private Spanned desc;
    private boolean isShowImage;
    private boolean isShowLogo;
    private boolean isMargin;

    public OnBoarding2(int image, String title, Spanned desc, boolean isShowImage, boolean isShowLogo, boolean isMargin) {
        this.image = image;
        this.title = title;
        this.desc = desc;
        this.isShowImage = isShowImage;
        this.isShowLogo = isShowLogo;
        this.isMargin = isMargin;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Spanned getDesc() {
        return desc;
    }

    public void setDesc(Spanned desc) {
        this.desc = desc;
    }

    public boolean isShowImage() {
        return isShowImage;
    }

    public void setShowImage(boolean showImage) {
        isShowImage = showImage;
    }

    public boolean isShowLogo() {
        return isShowLogo;
    }

    public void setShowLogo(boolean showLogo) {
        isShowLogo = showLogo;
    }

    public boolean isMargin() {
        return isMargin;
    }

    public void setMargin(boolean margin) {
        isMargin = margin;
    }
}
