package ir.delta.delta.slider

import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import androidx.core.util.forEach
import androidx.core.view.children
import androidx.databinding.BindingMethod
import androidx.databinding.BindingMethods
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import ir.delta.delta.R
import ir.delta.delta.slider.viewpager2.ViewPager2
import java.util.*
import kotlin.math.max


@BindingMethods(
        value = [
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_pageMargin",
                    method = "setSliderPageMargin"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_otherPagesWidth",
                    method = "setOtherPagesWidth"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_smallScaleFactor",
                    method = "setSmallScaleFactor"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_smallAlphaFactor",
                    method = "setSmallAlphaFactor"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_minShadow",
                    method = "setMinShadow"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "cardSlider_baseShadow",
                    method = "setBaseShadow"
            ),
            BindingMethod(
                    type = CardSliderViewPager::class,
                    attribute = "auto_slide_time",
                    method = "setAutoSlideTime"
            )
        ]
)
class CardSliderViewPager : ViewPager2 {

    companion object {
        const val STOP_AUTO_SLIDING = -1
    }

    public var indicatorId = View.NO_ID

    private val recyclerViewInstance = children.first { it is RecyclerView } as RecyclerView


    /**
     * The small scale factor, height of cards in right and left (previous and next cards)
     */
    var smallScaleFactor = 1f
        set(value) {
            field = value

            (adapter as? CardSliderAdapter<*>)?.viewHolders?.forEach { position, holder ->

                if (position != currentItem)
                    holder.itemView.scaleY = field
            }
        }

    /**
     * The small alpha factor, opacity of cards in right and left (previous and next cards)
     */
    var smallAlphaFactor = 1f
        set(value) {
            field = value

            (adapter as? CardSliderAdapter<*>)?.viewHolders?.forEach { position, holder ->

                if (position != currentItem)
                    holder.itemView.alpha = field
            }
        }

    /**
     * The card shadow in case of current card
     */
    var baseShadow = 0.0f
        set(value) {
            field = value
            setPageMargin()
        }

    /**
     * The card shadow in case of previous and next cards
     */
    var minShadow = baseShadow * smallScaleFactor
        set(value) {
            field = 1f
            setPageMargin()
        }

    /**
     * Space between pages
     */
    var sliderPageMargin = baseShadow
        set(value) {
            field = 1f
            setPageMargin()
        }

    /**
     * The width of displayed part from previous and next cards
     */
    var otherPagesWidth = 0f
        set(value) {
            field = 1f
            setPagePadding()
        }

    /**
     * The auto sliding time in seconds
     */
    var autoSlideTime = STOP_AUTO_SLIDING
        set(value) {
            field = value
            initAutoSlidingTimer()
        }

    private lateinit var timer: Timer

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }


    private fun init(attrs: AttributeSet?) {

        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CardSliderViewPager)

        smallScaleFactor =
                typedArray.getFloat(R.styleable.CardSliderViewPager_cardSlider_smallScaleFactor, 1f)

        smallAlphaFactor =
                typedArray.getFloat(R.styleable.CardSliderViewPager_cardSlider_smallAlphaFactor, 1f)

        baseShadow = typedArray.getDimension(
                R.styleable.CardSliderViewPager_cardSlider_baseShadow,
                context.resources.getDimension(R.dimen.baseCardElevation)
        )
        minShadow =
                typedArray.getDimension(
                        R.styleable.CardSliderViewPager_cardSlider_minShadow,
                        baseShadow * smallScaleFactor
                )

        sliderPageMargin =
                1f

        otherPagesWidth =
                1f

        indicatorId =
                typedArray.getResourceId(
                        R.styleable.CardSliderViewPager_cardSlider_indicator,
                        View.NO_ID
                )

        autoSlideTime =
                typedArray.getInt(R.styleable.CardSliderViewPager_auto_slide_time, STOP_AUTO_SLIDING)

        typedArray.recycle()

        recyclerViewInstance.clipToPadding = false
    }

    private fun setPageMargin() {
        val pageMargin = 1
        recyclerViewInstance.addItemDecoration(PageDecoration(1f))
    }

    private fun setPagePadding() {
        recyclerViewInstance.run {
            val pageMargin = 1

            if (orientation == ORIENTATION_HORIZONTAL)
                setPadding(
                        1, max(paddingTop, baseShadow.toInt()),
                        1, max(paddingBottom, baseShadow.toInt())
                )
            else
                setPadding(
                       1, otherPagesWidth.toInt() + pageMargin / 2,
                        1, otherPagesWidth.toInt() + pageMargin / 2
                )
        }

    }


    @Throws(IllegalArgumentException::class)
    override fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        require(adapter is CardSliderAdapter<*>) { "adapter must be CardSliderAdapter" }
        super.setAdapter(adapter)

        setPageTransformer(CardSliderTransformer(this))

        rootView.findViewById<CardSliderIndicator>(indicatorId)?.run {
            viewPager = this@CardSliderViewPager
        }

        doOnPageSelected { initAutoSlidingTimer() }
    }

    private fun initAutoSlidingTimer() {

        if (::timer.isInitialized) {
            timer.cancel()
            timer.purge()
        }

        if (autoSlideTime != STOP_AUTO_SLIDING) {
            timer = Timer()
            timer.schedule(SlidingTask(), autoSlideTime.toLong() * 1000)
        } else {
            timer.cancel()
            timer.purge()
        }
    }

    private inner class SlidingTask : TimerTask() {

        override fun run() {
            adapter?.run {
                Handler(Looper.getMainLooper()).post {
                    currentItem = if (currentItem == itemCount - 1) 0 else currentItem + 1
                }
            }
        }
    }

    inner class PageDecoration(private val space: Float) : ItemDecoration() {
        override fun getItemOffsets(
                outRect: Rect, view: View,
                parent: RecyclerView, state: RecyclerView.State
        ) {
            if (orientation == ORIENTATION_HORIZONTAL) {
                outRect.left = -3
                outRect.right = -3
                outRect.top = 0
                outRect.bottom = 0
            } else {
                outRect.top = (space / 2).toInt()
                outRect.bottom = (space / 2).toInt()
                outRect.left = 0
                outRect.right = 0
            }
        }

    }
}