package ir.delta.delta.bottomNavigation.moreItems;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ir.delta.delta.baseView.BaseActivity;

/**
 * Created by m.azadbar on 9/27/2017.
 */

public class AddressDeltaWebViewActivity extends BaseActivity {

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WebView mWebview = new WebView(this);
        mWebview.getSettings().setJavaScriptEnabled(true); // exitFromArchive javascript
        mWebview.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            }
        });

        mWebview.loadUrl("https://www.google.com/maps/place/Delta+Real+Estate+Group+%DA%AF%D8%B1%D9%88%D9%87+%D8%A7%D9%85%D9%84%D8%A7%D9%83+%D8%AF%D9%84%D8%AA%D8%A7%E2%80%AD/@35.7374973,51.362688,13z/data=!4m8!1m2!2m1!1z2q_YsdmI2Ycg2KfZhdmE2KfaqSDYr9mE2KrYqA!3m4!1s0x0:0x5b32fecc83e561af!8m2!3d35.7485063!4d51.4138806");
        setContentView(mWebview);
    }

}

