package ir.delta.delta.bottomNavigation.search;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

public class LocationInterface implements Parcelable , Comparable<LocationInterface> {

    public static final Creator<LocationInterface> CREATOR = new Creator<LocationInterface>() {
        @Override
        public LocationInterface createFromParcel(Parcel in) {
            return new LocationInterface(in);
        }

        @Override
        public LocationInterface[] newArray(int size) {
            return new LocationInterface[size];
        }
    };

    public String getTitle() {
        return null;
    }

    public String getDescription() {
        return null;
    }

    public int getId() {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public LocationInterface() {
    }

    private LocationInterface(Parcel in) {
    }

    @Override
    public int compareTo(@NonNull LocationInterface other) {
        return this.getId() - other.getId();
    }
}