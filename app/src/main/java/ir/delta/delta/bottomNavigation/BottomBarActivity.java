package ir.delta.delta.bottomNavigation;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.bottomNavigation.moreItems.LikeEstateFragment;
import ir.delta.delta.bottomNavigation.moreItems.MoreItemsFragment;
import ir.delta.delta.bottomNavigation.moreItems.RecentVisitFragment;
import ir.delta.delta.bottomNavigation.registerEstate.RegisterEstateFragment;
import ir.delta.delta.bottomNavigation.registerRequest.RegisterRequestFragment;
import ir.delta.delta.bottomNavigation.search.FilterFragment;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.bottomNavigation.search.SearchEstateFragment;
import ir.delta.delta.util.BottomNavigationViewHelper;
import ir.delta.delta.util.CustomTypefaceSpan;
import ir.delta.delta.util.PreferencesData;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.FullscreenPromptBackground;
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal;

public class BottomBarActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    private int tabIndex = 0;
    private Typeface fontSelected;
    private Typeface fontNormal;
    private SearchEstateFragment searchFragment;
    private RegisterEstateFragment registerEsateFragment;
    private RegisterRequestFragment registerRequestFragment;
    private MoreItemsFragment moreItemFragment;
    public static final String saearchDetailTag = "SearchDetailTag";
    private static final String moreItemDetailTag = "MoreItemDetailTag";
    public static final String registerEsateDetailTag = "registerEsateDetailTag";
    public static final String registerRequestDetailTag = "registerRequestDetailTag";
    private boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_bar);
        ButterKnife.bind(this);
        navigation.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.disableShiftMode(navigation);
        fontSelected = Typeface.createFromAsset(getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf");
        fontNormal = Typeface.createFromAsset(getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            tabIndex = bundle.getInt("itemsIndex");
        }

        navigation.getMenu().getItem(tabIndex).setChecked(true);
        onNavigationItemSelected(navigation.getMenu().getItem(tabIndex));
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        if (PreferencesData.isBottomBarOpened(this, "isBottomBar")) {
            new Handler().postDelayed(() -> guideView(), 2000);
            PreferencesData.setBottomBarOpend(this, "isBottomBar", false);
        }

    }

    private void guideView() {

        new MaterialTapTargetPrompt.Builder(this)
                .setTarget(R.id.navigation)
                .setPrimaryText(getResources().getString(R.string.guide_title_bottom_bar))
                .setPrimaryTextTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"))
                .setPrimaryTextColour(getResources().getColor(R.color.white))
                .setSecondaryTextColour(getResources().getColor(R.color.white))
                .setSecondaryText(getTextDescription(this, getResources().getString(R.string.giud_desc_bottom_bar)))
                .setSecondaryTextGravity(Gravity.LEFT)
                .setSecondaryTextSize(R.dimen.text_size_2)
                .setPromptBackground(new FullscreenPromptBackground())
                .setPromptFocal(new RectanglePromptFocal())
                .setSecondaryTextTypeface(Typeface.createFromAsset(getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"))
                .setBackgroundColour(getResources().getColor(R.color.transparentGuide))
                .setPromptStateChangeListener((prompt, state) -> {
                    if (BuildConfig.isCounsultant) {
                        if (state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED || state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                            if (moreItemFragment != null)
                                moreItemFragment.showGuid();
                        }
                    }

                })
                .show();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        boolean isSelected = false;
        switch (item.getItemId()) {
            case R.id.tab_search:
                tabIndex = 0;
                searchEstateClicked();
                isSelected = true;
                break;
            case R.id.tab_register_estate:
                tabIndex = 1;
                registerEstateClicked();
                isSelected = true;
                break;
            case R.id.tab_register_request:
                tabIndex = 2;
                registerRequestClicked();
                isSelected = true;
                break;
            case R.id.tab_more_items:
                tabIndex = 3;
                moreItemsClicked();
                isSelected = true;
                break;
        }
        setFont();
        return isSelected;
    }

    private void setFont() {
        Menu m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem item = m.getItem(i);
            Typeface font = (i == tabIndex) ? fontSelected : fontNormal;
            SpannableString mNewTitle = new SpannableString(item.getTitle());
            mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            item.setTitle(mNewTitle);
        }
    }

    private void searchEstateClicked() {
        FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        boolean isAdd = false;
        if (searchFragment == null) {
            searchFragment = new SearchEstateFragment();
            isAdd = true;
        }
        loadFragment(fragTrans, searchFragment, SearchEstateFragment.class.getName(), isAdd);

    }

    private void registerEstateClicked() {
        FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        boolean isAdd = false;
        if (registerEsateFragment == null) {
            registerEsateFragment = new RegisterEstateFragment();
            isAdd = true;
        }
        loadFragment(fragTrans, registerEsateFragment, RegisterEstateFragment.class.getName(), isAdd);
    }

    private void registerRequestClicked() {
        FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        boolean isAdd = false;
        if (registerRequestFragment == null) {
            registerRequestFragment = new RegisterRequestFragment();
            isAdd = true;
        }
        loadFragment(fragTrans, registerRequestFragment, RegisterRequestFragment.class.getName(), isAdd);
    }

    private void moreItemsClicked() {
        BaseFragment fragmentByTag = (BaseFragment) getSupportFragmentManager().findFragmentByTag(moreItemDetailTag);
        FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
        boolean isAdd = false;
        if (BuildConfig.isCounsultant) {
            if (moreItemFragment == null) {
                moreItemFragment = new MoreItemsFragment();
                isAdd = true;
            }
        } else {
//            if (fragmentByTag != null && fragmentByTag.isAdded() && fragmentByTag.getView() != null) {
//                fragTrans.show(fragmentByTag);
//                hideOtherFragment(fragTrans, fragmentByTag);
//                fragmentByTag.resume();
//            } else {
            if (fragmentByTag != null) {
                fragTrans.remove(fragmentByTag);
            }

            if (moreItemFragment == null) {
                moreItemFragment = new MoreItemsFragment();
                isAdd = true;
            }
//            }
        }
        loadFragment(fragTrans, moreItemFragment, MoreItemsFragment.class.getName(), isAdd);
    }

    private void loadFragment(FragmentTransaction fragTrans, BaseFragment fragment, String tag, boolean isAdd) {
        if (isAdd) {
            fragTrans.add(R.id.frameLayout, fragment, tag);
            hideOtherFragment(fragTrans, fragment);
        } else {
            fragTrans.show(fragment);
            hideOtherFragment(fragTrans, fragment);
            if (fragment == searchFragment) {
                searchFragment.resume();
            }

            if (fragment == moreItemFragment) {
                moreItemFragment.resume();
            }
        }


    }

    private void hideOtherFragment(FragmentTransaction fragTrans, Fragment currentFragment) {
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            if (fragment != currentFragment)
                fragTrans.hide(fragment);
        }
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }


    private void loadFragmentChild(Fragment fragment, String fragmentTag) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
        fragTrans.addToBackStack(fragmentTag);
        fragTrans.commit();
    }

    public void likeItemClick() {
        loadFragmentChild(new LikeEstateFragment(), moreItemDetailTag);
    }

    public void recentVisitItemClick() {
        loadFragmentChild(new RecentVisitFragment(), moreItemDetailTag);
    }

    @Override
    public void onBackPressed() {
        if (BuildConfig.isCounsultant) {
            if (this.doubleBackToExitPressedOnce) {
                finish();
            } else {
                Snackbar snackbar = Snackbar
                        .make(root, "برای خروج دوباره کلیک کنید. ", Snackbar.LENGTH_SHORT);
                ViewCompat.setLayoutDirection(snackbar.getView(), ViewCompat.LAYOUT_DIRECTION_RTL);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ContextCompat.getColor(BottomBarActivity.this, R.color.redColor));
                snackbar.show();
                // اگر کاربر یه بار کلیک کرد روی بک توی صفحه ی اصلی ، بهش پیام می دیم که بار دوم هم کلیک کنه . اگه بار دوم زد خارج  می شه . ضمنا اگه تو صفحه اصلی نبود بک زدن باعث می شه که برگردیم تو صفحه خانه
                this.doubleBackToExitPressedOnce = true;
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        BottomBarActivity.this.doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        } else {
            if (tabIndex == 0) {
                BaseFragment f = (BaseFragment) getSupportFragmentManager().findFragmentByTag(saearchDetailTag);
                if (f != null && f.onPopBackStack())
                    return;
                if (searchFragment != null && searchFragment.isHidden()) {
                    FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
                    fragTrans.show(searchFragment);
                    hideOtherFragment(fragTrans, searchFragment);
                    navigation.setSelectedItemId(R.id.tab_search);
                } else {
                    finish();
                }
            } else if (tabIndex == 1) {
                BaseFragment f = (BaseFragment) getSupportFragmentManager().findFragmentByTag(registerEsateDetailTag);
                if (f != null && f.onPopBackStack())
                    return;
                if (registerEsateFragment != null && registerEsateFragment.isHidden()) {
                    FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
                    fragTrans.show(registerEsateFragment);
                    hideOtherFragment(fragTrans, registerEsateFragment);
                    navigation.setSelectedItemId(R.id.tab_register_estate);
                } else {
                    finish();
                }
            } else if (tabIndex == 2) {
                BaseFragment f = (BaseFragment) getSupportFragmentManager().findFragmentByTag(registerRequestDetailTag);
                if (f != null && f.onPopBackStack())
                    return;
                if (registerRequestFragment != null && registerRequestFragment.isHidden()) {
                    FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
                    fragTrans.show(registerRequestFragment);
                    hideOtherFragment(fragTrans, registerRequestFragment);
                    navigation.setSelectedItemId(R.id.tab_register_request);
                } else {
                    finish();
                }
            } else if (tabIndex == 3) {

                BaseFragment f = (BaseFragment) getSupportFragmentManager().findFragmentByTag(moreItemDetailTag);
                if (f != null && f.onPopBackStack())
                    return;
                if (moreItemFragment != null && moreItemFragment.isHidden()) {
                    FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
                    fragTrans.show(moreItemFragment);
                    hideOtherFragment(fragTrans, moreItemFragment);
                    navigation.setSelectedItemId(R.id.tab_more_items);
                } else {
                    finish();
                }
            }
        }

    }

    @Override
    public void onBackStackChanged() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            String fragmentTag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
            BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(fragmentTag);
            if (currentFragment.isHiddenNavigation()) {
                navigation.setVisibility(View.GONE);
            } else {
                navigation.setVisibility(View.VISIBLE);
            }
        } else {
            navigation.setVisibility(View.VISIBLE);
        }
    }

    public void rectentVisitToSearchTab(Filter filter, boolean isRecent) {
        tabIndex = 0;
        navigation.getMenu().getItem(tabIndex).setChecked(true);
        FragmentManager frg = getSupportFragmentManager();
        FragmentTransaction beginFrag = frg.beginTransaction();
        boolean isSearch = false;
        if (searchFragment == null) {
            searchFragment = new SearchEstateFragment();
            beginFrag.add(R.id.frameLayout, searchFragment, SearchEstateFragment.class.getName());
        } else {
            beginFrag.show(searchFragment);
            isSearch = true;
        }
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(saearchDetailTag);
        if (fragmentByTag != null) {
            beginFrag.remove(fragmentByTag);
            frg.popBackStack(fragmentByTag.getTag(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        hideOtherFragment(beginFrag, searchFragment);
        if (isSearch) {
            searchFragment.resume();
            searchFragment.isRecent = isRecent;
            searchFragment.setFilter(filter);
        }
    }

    public void filterToSearchTab(Fragment filterFragment, boolean isBack) {
        if (searchFragment == null) {
            tabIndex = 0;
            navigation.getMenu().getItem(tabIndex).setChecked(true);

            FragmentManager frg = getSupportFragmentManager();
            FragmentTransaction fragTrans = frg.beginTransaction();
            fragTrans.remove(filterFragment);
            frg.popBackStack(saearchDetailTag, 0);
            if (searchFragment == null) {
                searchFragment = new SearchEstateFragment();
                fragTrans.add(R.id.frameLayout, searchFragment, SearchEstateFragment.class.getName());

                hideOtherFragment(fragTrans, searchFragment);
            } else {
                fragTrans.show(searchFragment);
                hideOtherFragment(fragTrans, searchFragment);
                if (!isBack) {
                    searchFragment.refreshLikeList();
                }
            }
        } else {
            FragmentManager frg = getSupportFragmentManager();
            FragmentTransaction beginFrag = frg.beginTransaction();
            beginFrag.remove(filterFragment);
            beginFrag.show(searchFragment);
            frg.popBackStack();
//            hideOtherFragment(beginFrag, searchFragment);
            if (!isBack) {
                searchFragment.changeFilterList();
            }

        }
    }

    public void loadFilterFragment(Filter filter) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction beginFrag = fragMgr.beginTransaction();
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(saearchDetailTag);
        beginFrag.setCustomAnimations(R.anim.slide_in_up, 0, 0, R.anim.slide_out_up);
        FilterFragment filterFragment;
        if (fragmentByTag != null && fragmentByTag.isAdded() && fragmentByTag.getView() != null) {
            if (fragmentByTag instanceof FilterFragment) {
                filterFragment = (FilterFragment) fragmentByTag;
                filterFragment.filter = filter;
                beginFrag.show(fragmentByTag);
                beginFrag.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                beginFrag.commit();
                filterFragment.resume();
            } else {
                beginFrag.remove(fragmentByTag);
                fragMgr.popBackStack(saearchDetailTag, 0);
                filterFragment = new FilterFragment();
                filterFragment.filter = filter;
                beginFrag.add(R.id.frameLayout, filterFragment, saearchDetailTag);
                beginFrag.addToBackStack(saearchDetailTag);
                beginFrag.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                beginFrag.commit();
            }
        } else {
            filterFragment = new FilterFragment();
            filterFragment.filter = filter;
            beginFrag.add(R.id.frameLayout, filterFragment, saearchDetailTag);
            beginFrag.addToBackStack(saearchDetailTag);
            beginFrag.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            beginFrag.commit();
        }
    }

    public void backToMoreItemTab(Fragment fragment) {
        FragmentManager frg = getSupportFragmentManager();
        FragmentTransaction beginFrag = frg.beginTransaction();
        beginFrag.remove(fragment);
        beginFrag.show(moreItemFragment);
        frg.popBackStack(moreItemDetailTag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        hideOtherFragment(beginFrag, moreItemFragment);
    }


    public void loadFragment(LocationSelectFragment fragment, String fragmentTag) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
        fragTrans.addToBackStack(fragmentTag);
        fragTrans.commit();
    }


    public interface onClickGuide {
        void onItemClickGiude();
    }
}