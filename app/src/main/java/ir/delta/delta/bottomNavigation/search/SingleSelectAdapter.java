package ir.delta.delta.bottomNavigation.search;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.LocationTypeEnum;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SingleSelectAdapter extends RecyclerView.Adapter<SingleSelectAdapter.ViewHolder> {


    private final boolean isSingleLine;
    private final LocationTypeEnum locationType;

    private final ArrayList<? extends LocationInterface> list;
    private final OnItemClickListener listener;

    SingleSelectAdapter(ArrayList<? extends LocationInterface> list, OnItemClickListener listener, boolean isSingleLine, LocationTypeEnum locationTypeEnum) {
        this.list = list;
        this.listener = listener;
        this.isSingleLine = isSingleLine;
        this.locationType = locationTypeEnum;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_area, parent, false);

        return new ViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LocationInterface object = list.get(position);
        if (isSingleLine) {
            holder.tvTitleSingle.setVisibility(View.VISIBLE);
            holder.tvTitleSingle.setText(object.getTitle());
        } else {
            holder.tvTitle.setText(object.getTitle());
            holder.tvDescription.setText(object.getDescription().replace("\n", "").replace("\r", ""));
        }

        if (locationType == LocationTypeEnum.AREA && object.getTitle()!= null && object.getTitle().contains("تهران") ) {
            holder.btnAboutArea.setVisibility(View.VISIBLE);
        } else {
            holder.btnAboutArea.setVisibility(View.GONE);
        }
        holder.bind(position, object, listener);

    }

    @Override
    public int getItemCount() {
        if (list == null) {
            return 0;
        }
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.tvTitleSingle)
        BaseTextView tvTitleSingle;
        @BindView(R.id.root)
        BaseRelativeLayout root;
        @BindView(R.id.btnAboutArea)
        BaseTextView btnAboutArea;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, LocationInterface area, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickSingle(position));
            btnAboutArea.setOnClickListener(v -> listener.onItemAreaInfo(position, area));
        }

    }


    public interface OnItemClickListener {
        void onItemClickSingle(int position);

        void onItemAreaInfo(int position, LocationInterface area);
    }
}
