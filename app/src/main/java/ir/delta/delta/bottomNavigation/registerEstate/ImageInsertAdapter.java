package ir.delta.delta.bottomNavigation.registerEstate;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImageInsertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int cellWidth;
    private final int maxImageCount;
    private final Context context;
    private final ArrayList<Uri> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;


    public ImageInsertAdapter(Context context, ArrayList<Uri> list, int cellWidth, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener) {
        this.list = list;
        this.listener = listener;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_image, parent, false);
                itemView.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new addImageViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_estate, parent, false);
                itemView1.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new ViewHolderImageList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addImageViewHolder viewHolder0 = (addImageViewHolder) holder;
                ((addImageViewHolder) holder).bind(position, listener);
                break;

            default:
                ViewHolderImageList viewHolder2 = (ViewHolderImageList) holder;
                ((ViewHolderImageList) holder).bind(position, deleteOnItemClickListener);
                Uri imagPath;
                if (list.size() == maxImageCount) {
                    imagPath = list.get(position);
                } else {
                    imagPath = list.get(position - 1);
                }

                Glide.with(context).load(imagPath)
                        .placeholder(R.drawable.placeholder)
                        .centerCrop()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .override(200, 200)
                        .into(((ViewHolderImageList) holder).imgAdd);

                break;
        }

    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position);
    }

    static class addImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;

        addImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderImageList extends RecyclerView.ViewHolder {
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;

        ViewHolderImageList(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position));
        }
    }

}
