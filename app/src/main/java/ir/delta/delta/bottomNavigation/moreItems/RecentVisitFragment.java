package ir.delta.delta.bottomNavigation.moreItems;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.database.TransactionFilter;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 5/28/2018.
 */

public class RecentVisitFragment extends BaseFragment implements RecentVisitAdapter.OnItemClickListener{

    Unbinder unbinder;
    @BindView(R.id.rvRecentVisit)
    RecyclerView rvRecentVisit;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgLocation)
    BaseImageView imgLocation;
    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;

    ArrayList<Filter> filters = new ArrayList<>();

    public RecentVisitFragment() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_recent_visit, container, false);
        unbinder = ButterKnife.bind(this, view);
        checkArrowRtlORLtr(imgBack);
        rlChangeCity.setVisibility(View.GONE);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getResources().getString(R.string.recent_history));
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            tvCityTitle.setText(Constants.getCity(getActivity()).getName());
            tvFilterDetail.setText(Constants.getCity(getActivity()).getName());
            tvFilterDetail.setTextColor(getResources().getColor(R.color.toolbarForeAlpha));
        }
        resume();
    }

    @Override
    public void resume() {
        filters = TransactionFilter.getInstance().getListOfFilters(getActivity(), Constants.getCity(getActivity()).getId());
        if (filters.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.not_found_search_in_city));
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
        setDataAdapter();
    }

    private void setDataAdapter() {
        RecentVisitAdapter adapter = new RecentVisitAdapter(getResources(), filters, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvRecentVisit.setHasFixedSize(true);
        rvRecentVisit.setLayoutManager(layoutManager);
        rvRecentVisit.setAdapter(adapter);
    }

    @Override
    public void onItemClick(Filter filter) {
        if (getActivity() != null) {
            TransactionFilter.getInstance().updateFilterDate(getActivity(), filter.getFilterId());
            if (getActivity() instanceof BottomBarActivity)
                ((BottomBarActivity) getActivity()).rectentVisitToSearchTab(filter, true);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rlBack, R.id.rlChangeCity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((BottomBarActivity) getActivity()).backToMoreItemTab(this);
                }
                break;
            case R.id.rlChangeCity:
                startCityActivity();
                break;
        }
    }

    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            ((BottomBarActivity) getActivity()).backToMoreItemTab(this);
        }
        return true;
    }
}
