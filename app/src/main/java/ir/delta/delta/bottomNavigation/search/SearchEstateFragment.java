package ir.delta.delta.bottomNavigation.search;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionFilter;
import ir.delta.delta.database.TransactionLikeEstate;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.estateDetail.EstateDetailActivity;
import ir.delta.delta.favorite.Favorite;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.EstateService;
import ir.delta.delta.service.Request.FilterEstateService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.EstateReq;
import ir.delta.delta.service.RequestModel.FilterReq;
import ir.delta.delta.service.ResponseModel.Estate;
import ir.delta.delta.service.ResponseModel.EstateResponse;
import ir.delta.delta.service.ResponseModel.FavoritePropertyResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class SearchEstateFragment extends BaseFragment implements SearchEstateAdapter.OnItemClickListener, Favorite.OnFavoriteListener {


    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.btnNewSearch)
    BaseTextView btnNewSearch;
    @BindView(R.id.cvBtnNewSearch)
    CardView cvBtnNewSearch;
    @BindView(R.id.rvSearchEstate)
    RecyclerView rvSearchEstate;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgLocation)
    BaseImageView imgLocation;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    private Unbinder unbinder;
    private FilterReq filterReq;
    private EstateReq req;
    private Filter filter;
    private final ArrayList<Estate> estates = new ArrayList<>();
    private ArrayList<Integer> likeEstateList;
    private SearchEstateAdapter adapter;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;
    public boolean isRecent;
    private boolean disLike;
    private int estateId;
    private int estatePosition;
    private int selectedPosition;

    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    public SearchEstateFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_estate, container, false);
        unbinder = ButterKnife.bind(this, view);
        Constants.setBackgroundProgress(getActivity(), rlProgress);
        rlFilterChange.setVisibility(View.VISIBLE);
        rlChangeCity.setVisibility(View.GONE);
        tvFilterTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
        tvFilterDetail.setTextColor(getResources().getColor(R.color.toolbarForeAlpha));
        checkArrowRtlORLtr(imgBack);
        finishFragment(rlBack);
        if (BuildConfig.isCounsultant) {
            tvTitle.setText(null);
            rlBack.setVisibility(View.GONE);
        } else {
            tvTitle.setText(getResources().getString(R.string.mag_delta));
            rlBack.setVisibility(View.VISIBLE);
        }
        loadingView.setButtonClickListener(view1 -> getList());
        resume();


        return view;
    }

    private void guideView() {
        new MaterialTapTargetPrompt.Builder(this)
                .setTarget(R.id.rlFilterChange)
                .setPrimaryText(getResources().getString(R.string.guide_title_change_search))
                .setPrimaryTextColour(getResources().getColor(R.color.white))
                .setSecondaryTextColour(getResources().getColor(R.color.white))
                .setPrimaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"))
                .setSecondaryText(getTextDescription(getContext(), getResources().getString(R.string.giud_desc_change_search)))
                .setSecondaryTextGravity(Gravity.LEFT)

                .setSecondaryTextSize(R.dimen.text_size_2)
                .setSecondaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"))
                .setBackgroundColour(getResources().getColor(R.color.transparentGuide))
                .setPromptStateChangeListener((prompt, state) -> {
                })
                .show();

    }


    @Override
    public void resume() {
        likeEstateList = TransactionLikeEstate.getInstance().getEstateIdList(getActivity());
//        currentCityId = Constants.getCity(getActivity()).getId();
//        filter = TransactionFilter.getInstance().getLastFilter(getActivity(), Constants.getCity(getActivity()).getId());
//        refreshList();


        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            filter = TransactionFilter.getInstance().getLastFilter(getActivity(), Constants.getCity(getActivity()).getId());
            if (filter == null) {
//                changeFilter();
                getList();
            } else {
                refreshList();
            }
        } else if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        if (isRecent) {
            filter = TransactionFilter.getInstance().getLastFilter(getActivity(), Constants.getCity(getActivity()).getId());
            refreshList();
            isRecent = false;
        }
        /*if (filter == null) {
            changeFilter();
        }*/

    }


    public void changeFilterList() {
        likeEstateList = TransactionLikeEstate.getInstance().getEstateIdList(getActivity());
        currentCityId = Constants.getCity(getActivity()).getId();
        filter = TransactionFilter.getInstance().getLastFilter(getActivity(), Constants.getCity(getActivity()).getId());
        refreshList();

    }

    public void refreshLikeList() {
//        if (adapter != null) {
//            likeEstateList = TransactionLikeEstate.getInstance().getEstateIdList(getActivity());
//            adapter.setLikeEstateList(likeEstateList);
//        }
    }


    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new SearchEstateAdapter(getContext(), estates, this, filter == null);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            rvSearchEstate.setHasFixedSize(true);
            rvSearchEstate.setLayoutManager(layoutManager);
            rvSearchEstate.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvSearchEstate.setAdapter(adapter);
            rvSearchEstate.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.setSuggationEstate(filter == null);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void shakeAnimate() {
        int count = PreferencesData.getSearchTextAnimationTime(getActivity());
        if (count < 4) {
            rlFilterChange.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.shake));
            PreferencesData.setSearchTextAnimationTime(getActivity(), count + 1);
        }
    }

    @Override
    public void onItemClick(int position, Estate estate) {
        selectedPosition = position;
        Intent intent = new Intent(getContext(), EstateDetailActivity.class);
        intent.putExtra("encodedId", estate.getEncodedId());
        startActivityForResult(intent, 2);
    }

    @Override
    public void onLikeItem(int position, Estate estate) {
        estateId = estate.getId();
        estatePosition = position;
        Favorite favorite = new Favorite(getContext(), this);
        if (estate.isLiked()) {
            if (Constants.getUser(getActivity()) != null) {
                roundedLoadingView.setVisibility(View.VISIBLE);
                enableDisableViewGroup(root, false);
                if (Constants.getUser(getActivity()).isGeneral()) {
                    favorite.unFavoriteUserRequest(estateId);
                } else {
                    favorite.unFavoriteConsultantProperty(estateId);
                }
            } else {
                login();
                disLike = true;
            }

        } else {
            if (Constants.getUser(getActivity()) != null) {
                roundedLoadingView.setVisibility(View.VISIBLE);
                enableDisableViewGroup(root, false);
                if (Constants.getUser(getActivity()).isGeneral()) {
                    favorite.favoriteUserRequest(estateId);
                } else {
                    favorite.favoriteConsultantProperty(estateId);
                }
            } else {
                login();
                disLike = false;
            }
        }


    }

    private void login() {
        if (getActivity() != null) {
            CustomDialog dialog = new CustomDialog(getActivity());
            dialog.setIcon(R.drawable.ic_lock, getResources().getColor(R.color.redColor));
            dialog.setDialogTitle(getResources().getString(R.string.login_account));
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setDescription(getString(R.string.desc__login_to_account_for_like));
            dialog.setOkListener(getString(R.string.login), v -> {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivityForResult(intent, Constants.LOGIN_LIKE);
                dialog.dismiss();
            });
            dialog.setCancelListener(getString(R.string.cancel), v -> {
                dialog.dismiss();
            });
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    @OnClick({R.id.rlFilterChange, R.id.btnNewSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlFilterChange:
            case R.id.btnNewSearch:
                changeFilter();
                break;
        }
    }

    private void changeFilter() {
        if (getActivity() != null && getActivity() instanceof BottomBarActivity) {
            ((BottomBarActivity) getActivity()).loadFilterFragment(filter);
        }
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvSearchEstate.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvSearchEstate.setVisibility(View.VISIBLE);
    }

    private void refreshList() {
        tvFilterTitle.setText("");
        tvFilterDetail.setText("");
        estates.clear();
        if (adapter != null)
            adapter.notifyDataSetChanged();
        endOfList = false;
        inLoading = false;
        rootEmptyView.setVisibility(View.GONE);
        getList();
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (estates.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            if (filter != null) {
                getFilterList();
            } else {
                getLastEstates();
            }
        }
    }

    private void getLastEstates() {

        if (req == null) {
            req = new EstateReq();
        }
        req.setCityId(Constants.getCity(getActivity()).getId());
        req.setOffset(offset);
        req.setStartIndex(estates.size());
        if (Constants.getUser(getContext()) != null) {
            if (Constants.getUser(getContext()).isGeneral()) {
                req.setLogedInUserTokenOrId(Constants.getUser(getContext()).getUserToken());
            } else {
                req.setLogedInUserTokenOrId(String.valueOf(Constants.getUser(getContext()).getUserId()));
            }
        } else {
            req.setLogedInUserTokenOrId("");
        }
        EstateService.getInstance().getLastEstate(getResources(), req, new ResponseListener<EstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded())
                    onError(getResources().getString(R.string.communicationError));
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }

            }

            @Override
            public void onSuccess(EstateResponse response) {
                if (getActivity() != null && isAdded())
                    setList(response);
            }
        });

    }


    private void getFilterList() {
        if (filterReq == null) {
            filterReq = new FilterReq();
        }

        filter.setFilterRequest(getActivity(), filterReq, estates.size(), offset);
        FilterEstateService.getInstance().getFilterEstate(getResources(), filterReq, new ResponseListener<EstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded())
                    onError(getResources().getString(R.string.communicationError));
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(EstateResponse response) {
                if (getActivity() != null && isAdded())
                    setList(response);
            }
        });
    }

    private void setList(EstateResponse response) {
        if (rlLoding == null) {
            return;
        }
        inLoading = false;
        stopLoading();
        rlLoding.setVisibility(View.GONE);
        if (response.isSuccessed() && response.getSearchResult() != null) {
            if (response.getSearchResult().size() < offset) {
                endOfList = true;
            }
            handleToolbarSearchFragment(response, response.getResultCount() == 0);
            estates.addAll(response.getSearchResult());
            setDataAdapter();
            if (PreferencesData.isFirstSearchOpened(getActivity(), "isSearchOpened") && filter != null) {
                new Handler().postDelayed(() -> guideView(), 2000);
                PreferencesData.setFirstSearchOpened(getActivity(), "isSearchOpened", false);
            }
        } else {
            loadingView.setVisibility(View.VISIBLE);
            loadingView.showError(response.getMessage());
        }
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && estates != null) {
            rlLoding.setVisibility(View.GONE);
            if (estates.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }

    public void handleToolbarSearchFragment(EstateResponse response, boolean isListEmpty) {
        imgLogo.setVisibility(View.GONE);
        rlChangeCity.setVisibility(View.GONE);
        if (response.getSearchResult().size() > 0 || estates.size() > 0) {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setVisibility(View.VISIBLE);
            tvFilterTitle.setVisibility(View.VISIBLE);

            rootEmptyView.setVisibility(View.GONE);
            Estate estate = response.getSearchResult().get(0);
            if (filter == null) {
                tvFilterTitle.setText(getResources().getString(R.string.sugestion_home));
            } else {
                tvFilterTitle.setText(estate.getPropertyType());
            }
            tvFilterDetail.setText("برای " + estate.getContractType() + " - " + response.getResultCount() + " " + getResources().getString(R.string.advertis));
            rlFilterChange.setVisibility(View.VISIBLE);
            if (isListEmpty) {
                shakeAnimate();
            }
        } else {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterTitle.setText(Constants.getCity(getActivity()).getName());
//            tvFilterDetail.setText(getString(R.string.not_advertis));
            tvFilterDetail.setVisibility(View.GONE);
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.not_found_info));
            cvBtnNewSearch.setVisibility(View.VISIBLE);
            rlFilterChange.setVisibility(View.GONE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constants.LOGIN_LIKE) {
                if (Constants.getUser(getActivity()) != null) {
                    Favorite favorite = new Favorite(getContext(), this);
                    roundedLoadingView.setVisibility(View.VISIBLE);
                    enableDisableViewGroup(root, false);
                    if (Constants.getUser(getActivity()).isGeneral()) {
                        if (disLike) {
                            favorite.unFavoriteUserRequest(estateId);
                        } else {
                            favorite.favoriteUserRequest(estateId);
                        }
                    } else {
                        if (disLike) {
                            favorite.unFavoriteConsultantProperty(estateId);
                        } else {
                            favorite.favoriteConsultantProperty(estateId);
                        }
                    }
                }
            } else if (requestCode == 2) {
                boolean isLike = data.getBooleanExtra("isLike", false);
                if (selectedPosition < estates.size())
                    estates.get(selectedPosition).setLiked(isLike);
                adapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void favoriteUserGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void favoriteUserOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), getResources().getString(R.string.estate_to_favorite), Toast.LENGTH_SHORT).show();
                adapter.updateViewInRecycle(estatePosition, true);
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));
                adapter.updateViewInRecycle(estatePosition, true);
            }
//            isFavoriteDeposit = true;
        }
    }

    @Override
    public void unFavoriteUserGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void unFavoriteUserOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), getResources().getString(R.string.remove_estate_to_favorite), Toast.LENGTH_SHORT).show();
                adapter.updateViewInRecycle(estatePosition, false);
//                isFavoriteDeposit = false;
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));

            }
        }
    }

    @Override
    public void favoriteConsultantPropertyGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void favoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), getResources().getString(R.string.estate_to_favorite), Toast.LENGTH_SHORT).show();
                adapter.updateViewInRecycle(estatePosition, true);
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));
                adapter.updateViewInRecycle(estatePosition, true);
            }
//            isFavoriteDeposit = true;
        }
    }

    @Override
    public void unFavoriteConsultantPropertyGetError(String error) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void unFavoriteConsultantPropertyOnSuccess(FavoritePropertyResponse response) {
        if (getView() != null && isAdded()) {
            roundedLoadingView.setVisibility(View.GONE);
            enableDisableViewGroup(root, true);
            if (response.isSuccessed()) {
                Toast.makeText(getActivity(), getResources().getString(R.string.remove_estate_to_favorite), Toast.LENGTH_SHORT).show();
                adapter.updateViewInRecycle(estatePosition, false);
//                isFavoriteDeposit = false;
            } else {
                showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));
                adapter.updateViewInRecycle(estatePosition, true);
            }
        }
    }


}
