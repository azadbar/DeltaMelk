package ir.delta.delta.bottomNavigation.registerRequest;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomMultiLineEditText;
import ir.delta.delta.customView.CustomSwitchButton;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionRegion;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.enums.PaymentTypeEnum;
import ir.delta.delta.payment.PaymentDialog;
import ir.delta.delta.payment.ServerErrorListener;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DemandRegisterService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.DemandRegisterReq;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.service.ResponseModel.DemandRegisterResponse;
import ir.delta.delta.service.ResponseModel.ModelStateErrors;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.service.ResponseModel.Region;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

import static android.app.Activity.RESULT_OK;

public class RegisterRequestFragment extends BaseFragment implements ServerErrorListener {

    Unbinder unbinder;
    private final String AREAS = "areas";
    private final String PROPERTY = "property";
    private final String REGIONS = "regions";
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.propertyType_layout)
    LocationView propertyTypeLayout;
    @BindView(R.id.area_layout)
    LocationView areaLayout;
    @BindView(R.id.region_layout)
    LocationView regionLayout;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtEmail)
    CustomEditText edtEmail;
    @BindView(R.id.edtRangeSquare)
    CustomEditText edtRangeSquare;
    @BindView(R.id.edtRangePrice)
    CustomEditText edtRangePrice;
    @BindView(R.id.edtRangeRent)
    CustomEditText edtRangeRent;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;

    @BindView(R.id.btnRegisterPayment)
    BaseTextView btnRegisterPayment;
    @BindView(R.id.edtMortgagePrice)
    CustomEditText edtMortgagePrice;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.scroolView)
    ScrollView scroolView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.tvTxtDescPayment)
    BaseTextView tvTxtDescPayment;
    @BindView(R.id.card_first)
    CustomSwitchButton cardFirst;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;


    private ContractObject selectedContractType;
    private Area selectedArea;
    private ArrayList<Region> selectedRegions;
    private PropertyType selectedPropertyType;
    private ContractTypeEnum contractTypeEnum;
    private DemandRegisterReq req;
    private final int maxSelectedRegions = 3;


    public RegisterRequestFragment() {
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register_request, container, false);
        unbinder = ButterKnife.bind(this, view);


        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.register_request));
        checkArrowRtlORLtr(imgBack);
        finishFragment(rlBack);
        if (BuildConfig.isCounsultant) {
            tvTitle.setText(null);
            rlBack.setVisibility(View.GONE);
        } else {
            rlBack.setVisibility(View.VISIBLE);
            tvTitle.setText(getResources().getString(R.string.mag_delta));
        }
        rlFilterChange.setVisibility(View.GONE);
        rlChangeCity.setVisibility(View.VISIBLE);
        hideInputKeyboard();

//        btnRegisterPayment.setText(getString(R.string.register_and_payment, Constants.convertNumberToDecimal(PreferencesData.getDemandRegisterPrice(getContext()) / 10.0) + " " + getString(R.string.toman)));
        tvTxtDescPayment.setText(getString(R.string.description_payment_request));

        areaLayout.setImgInfo(R.drawable.ic_location_gray);
        areaLayout.setTxtTitle(getString(R.string.area_text));
        regionLayout.setImgInfo(R.drawable.ic_location_gray);
        regionLayout.setTxtTitle(getString(R.string.region_text) + getString(R.string.dots));
        propertyTypeLayout.setImgInfo(R.drawable.ic_location_gray);
        propertyTypeLayout.setTxtTitle(getString(R.string.property_text));

        propertyTypeLayout.setHint(getString(R.string.click_to_select));
        areaLayout.setHint(getString(R.string.your_estat));
        regionLayout.setHint(getString(R.string.your_region));
        //
        cardFirst.setSections(getString(R.string.sale_text), getString(R.string.rent_txt));
        cardFirst.setListener(index -> {
            if (index == 0) {
                contractTypeSelected(ContractTypeEnum.PURCHASE);
            } else if (index == 1) {
                contractTypeSelected(ContractTypeEnum.RENT);
            }
        });

        //
        edtRangePrice.setTextHint(getString(R.string.the_minimum_amount_is_one_thousend, getString(R.string.price_txt) + " 1000"));
        edtMortgagePrice.setTextHint(getString(R.string.the_minimum_amount_is_one_thousend, getString(R.string.mortgage_text) + " 1000"));
        edtRangeRent.setTextHint(getString(R.string.the_minimum_amount_is_one_thousend, getString(R.string.rent_text) + " 1000"));

        edtRangeSquare.setUnit(getString(R.string.meter_square));
        edtRangePrice.setUnit(getString(R.string.toman));
        edtRangeRent.setUnit(getString(R.string.toman));
        edtMortgagePrice.setUnit(getString(R.string.toman));

        User user = Constants.getUser(getActivity());
        if (user != null) {
            if (!TextUtils.isEmpty(user.getDisplayName())) {
                edtName.setTextBody(user.getDisplayName());
            } else {
                edtName.setTextBody(user.getLastName());
            }

            if (!TextUtils.isEmpty(user.getMobile())) {
                edtMobile.setTextBody(user.getMobile());
            }
        }

//        paymentInBillingBazar();

        if (PreferencesData.isChangeCity(getActivity(), "isChangeCity")) {
            new Handler().postDelayed(this::guideView, 2000);
            PreferencesData.setChangeCity(getActivity(), "isChangeCity", false);
        }
        return view;
    }

    private void guideView() {
        rlChangeCity.setVisibility(View.VISIBLE);
        new MaterialTapTargetPrompt.Builder(getActivity())
                .setTarget(rlChangeCity)
                .setPrimaryText(getResources().getString(R.string.guide_title_change_city))
                .setPrimaryTextColour(getResources().getColor(R.color.white))
                .setSecondaryTextColour(getResources().getColor(R.color.white))
                .setPrimaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"))
                .setSecondaryText(getTextDescription(getContext(), getResources().getString(R.string.giud_desc_change_city)))
                .setSecondaryTextGravity(Gravity.LEFT)
                .setSecondaryTextSize(R.dimen.text_size_2)
                .setSecondaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"))
                .setBackgroundColour(getResources().getColor(R.color.transparentGuide))
                .setPromptStateChangeListener((prompt, state) -> {
                })
                .show();
    }


//    private void paymentInBillingBazar() {
//        if (getActivity() != null) {
//            PackageManager pm = getActivity().getPackageManager();
//            hasBazar = Constants.isPackageInstalled(pm);
//            if (hasBazar) {
//                mHelper = new IabHelper(getActivity(), base64EncodedPublicKey);
//                mGotInventoryListener = (result, inventory) -> {
//                    Log.d(TAG, "Query inventory finished.");
//                    if (result.isFailure()) {
//                        Log.d(TAG, "Failed to query inventory: " + result);
//                        return;
//                    } else {
//                        Log.d(TAG, "Query inventory was successful.");
//                        mIsPremium = inventory.hasPurchase(SKU_PREMIUM);
//                        Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
//                    }
//                    Log.d(TAG, "Initial inventory query finished; enabling main UI.");
//                };
//
//                mPurchaseFinishedListener = (result, purchase) -> {
//                    if (result.isFailure()) {
//                        Log.d(TAG, "Error purchasing: " + result);
//                        return;
//                    } else if (purchase.getSku().equals(SKU_PREMIUM)) {
//                        //calBack from bazar and call PaymentUrl
//                        PaymentConfirm(purchase);
//                    }
//                };
//
//                Log.d(TAG, "Starting setup.");
//                mHelper.startSetup(result -> {
//                    Log.d(TAG, "Setup finished.");
//                    if (!result.isSuccess()) {
//                        // Oh noes, there was a problem.
//                        Log.d(TAG, "Problem setting up In-app Billing: " + result);
//                    }
//                    // Hooray, IAB is fully set up!
//                    mHelper.queryInventoryAsync(mGotInventoryListener);
//                });
//            } else {
//                showBazarDialog();
//            }
//
//        }
//    }

//    private void PaymentConfirm(Purchase purchase) {
//        kala = purchase;
//        Constants.hideKeyboard(getActivity());
//        roundedLoadingView.showLoading();
//        enableDisableViewGroup(root, false);
//        //TODO send
//        PaymentConfirmReq req = new PaymentConfirmReq();
//        req.setSuccessed(true);
//        req.setToken(token);
//        req.setExeptionMessage("");
//        req.setResultCode("200");
//        req.setReferenceId("");
//        PaymentConfirmService.getInstance().getPaymentConfirm(getResources(), req, new ResponseListener<PaymentConfirmResponse>() {
//            @Override
//            public void onGetError(String error) {
//                if (getActivity() != null && isAdded()) {
//                    roundedLoadingView.setVisibility(View.GONE);
//                    enableDisableViewGroup(root, true);
//                    showErrorDialog(error);
//                }
//            }
//
//            @Override
//            public void onSuccess(PaymentConfirmResponse response) {
//                if (getActivity() != null && isAdded()) {
//                    Intent intent = new Intent(getActivity(), ActivityListenerResultPayment.class);
//                    intent.putExtra("data", dataPayment);
//                    intent.putExtra("from", "req");
//                    getActivity().startActivity(intent);
//                    getActivity().getSupportFragmentManager();
//                    MasrafSeke(kala);
//                    Toast.makeText(getActivity(), "toast", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//
//    }

//    private void showBazarDialog() {
//        if (getActivity() != null) {
//            CustomDialog dialog = new CustomDialog(getActivity());
//            dialog.setCancelListener(getResources().getString(R.string.install), v -> Toast.makeText(getActivity(), "نصب بازار", Toast.LENGTH_SHORT).show());
//            dialog.setCancelListener(getResources().getString(R.string.cancel), v -> dialog.dismiss());
//            dialog.setDialogTitle(getResources().getString(R.string.install_bazar));
//            dialog.setDescription(getResources().getString(R.string.please_install_bazar));
//            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
//        }
//
//    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            tvCityTitle.setText(Constants.getCity(getActivity()).getName());
            contractTypeSelected(ContractTypeEnum.PURCHASE);
            propertyTypeSelected(null);
            areaSelected(null);
            scroolView.scrollTo(0, 0);

            scroolView.post(() -> {
                if (scroolView != null) {
                    scroolView.fullScroll(ScrollView.FOCUS_UP);
                }
            });
        }

    }

    private void contractTypeSelected(ContractTypeEnum type) {
        if (contractTypeEnum != type) {
            contractTypeEnum = type;
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractTypeEnum.getMethodCode(), Constants.getCity(getActivity()).getLocationFeatureLocalId());
            propertyTypeSelected(null);
            setEstateSpace();
        }
    }


    private void propertyTypeSelected(PropertyType propertyType) {
        selectedPropertyType = propertyType;
        if (selectedPropertyType != null) {
            propertyTypeLayout.setValue(propertyType.getTitle(), null);
        } else {
            propertyTypeLayout.reset();
        }
    }

    private void areaSelected(Area area) {
        areaLayout.setVisibility(View.VISIBLE);
        selectedArea = area;
        if (selectedArea != null) {
            areaLayout.setValue(selectedArea.getName(), selectedArea.getDescription());
        } else {
            if (TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) > 0) {
                areaLayout.reset();
            } else {
                areaLayout.setVisibility(View.GONE);
//                areaLayout.notExist();
            }
        }
        regionSelected(null);
    }

    private void regionSelected(ArrayList<Region> region) {
        regionLayout.setVisibility(View.VISIBLE);
        selectedRegions = region;
        if (selectedRegions != null) {
            int allRegionCount = TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId());
            if (selectedRegions.size() == allRegionCount) {
                regionLayout.setValue(getString(R.string.all_regions), null);
            } else {
                StringBuilder text = new StringBuilder();
                for (int i = 0; i < selectedRegions.size(); i++) {
                    text.append(selectedRegions.get(i).getName());
                    if (i < selectedRegions.size() - 1) {
                        text.append(",");
                    }
                }

                if (text.length() == 0) {
                    regionLayout.setValue(getString(R.string.all_regions), null);
                } else {
                    regionLayout.setValue(text.toString(), null);
                }

            }
        } else {
            regionLayout.setValue(getString(R.string.all_regions), null);
            if (selectedArea != null) {
                if (TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId()) == 0) {
                    regionLayout.setVisibility(View.GONE);
                }
            } else {
                regionLayout.setVisibility(View.GONE);
            }

        }

    }

    private void setEstateSpace() {
        if (selectedContractType.getType() == ContractTypeEnum.RENT) {
            edtRangePrice.setVisibility(View.GONE);
            edtMortgagePrice.setVisibility(View.VISIBLE);
            edtRangeRent.setVisibility(View.VISIBLE);
        } else {
            edtRangePrice.setVisibility(View.VISIBLE);
            edtMortgagePrice.setVisibility(View.GONE);
            edtRangeRent.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnRegisterPayment,
            R.id.propertyType_layout,
            R.id.area_layout, R.id.region_layout, R.id.rlChangeCity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.propertyType_layout:
                if (selectedContractType != null && selectedContractType.getPropertyTypes() != null && selectedContractType.getPropertyTypes().size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(RegisterRequestFragment.this, Constants.REQUEST_PROPERTYTYPE_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_property));
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", PROPERTY);
                    bundle.putSerializable("locationType", LocationTypeEnum.PROPERTYTYPE);
                    bundle.putParcelableArrayList("list", new ArrayList<>(selectedContractType.getPropertyTypes()));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.registerRequestDetailTag);
                    }
                }
                break;
            case R.id.area_layout:
                ArrayList<Area> areas = TransactionArea.getInstance().getAreasBy(getActivity(), Constants.getCity(getActivity()).getId());
                if (areas.size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(RegisterRequestFragment.this, Constants.REQUEST_AREA_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_area));
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", false);
                    bundle.putString("extraName", AREAS);
                    bundle.putSerializable("locationType", LocationTypeEnum.AREA);
                    bundle.putParcelableArrayList("list", new ArrayList<>(areas));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.registerRequestDetailTag);
                    }
                }
                break;
            case R.id.region_layout:
                if (selectedArea != null) {
                    ArrayList<Region> regions = TransactionRegion.getInstance().getRegionsBy(getActivity(), selectedArea.getId());
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(RegisterRequestFragment.this, Constants.REQUEST_REGION_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_region));
                    bundle.putBoolean("isMultiSelect", true);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", REGIONS);
                    bundle.putSerializable("locationType", LocationTypeEnum.REGION);
                    bundle.putParcelableArrayList("list", new ArrayList<>(regions));
                    bundle.putInt("maxSelectedArea", maxSelectedRegions);
                    bundle.putParcelableArrayList("selectedList", selectedRegions == null ? new ArrayList<>() : new ArrayList<>(selectedRegions));
                    locationFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.registerRequestDetailTag);
                    }
                }
                break;
            case R.id.btnRegisterPayment:
                if (!isValidData()) {
                    return;
                }
                fillDemandReq();
                sendDemand();
                if (getActivity() != null) {
                    Constants.hideKeyboard(getActivity());
                }
                break;
            case R.id.rlChangeCity:
                startCityActivity();
                break;
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();
        if (selectedPropertyType == null) {
            String message = getResources().getString(R.string.select_property_type);
            propertyTypeLayout.setError(message);
            errorMsgList.add(message);
            scroolTo(propertyTypeLayout);
        }
        if (selectedArea == null && TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) > 0) {
            String message = getResources().getString(R.string.select_area);
            areaLayout.setError(message);
            if (errorMsgList.size() == 0) {
                scroolTo(areaLayout);
            }
            errorMsgList.add(message);
        }

        if (selectedRegions == null && selectedArea != null && TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId()) > 0) {
            String message = getResources().getString(R.string.select_region);
            regionLayout.setError(message);
            if (errorMsgList.size() == 0) {
                scroolTo(regionLayout);
            }
            errorMsgList.add(message);
        }
        if (edtName.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtName);
            }
            errorMsgList.add(edtName.getError());
        }

        if (edtMobile.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtMobile);
            }
            errorMsgList.add(edtMobile.getError());
        }

        if (edtEmail.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtEmail);
            }
            errorMsgList.add(edtEmail.getError());
        }

        if (edtRangeSquare.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtRangeSquare);
            }
            errorMsgList.add(edtRangeSquare.getError());
        }

        if (selectedContractType.getType() == ContractTypeEnum.PURCHASE) {
            if (edtRangePrice.getError() != null) {
                if (errorMsgList.size() == 0) {
                    scroolTo(edtRangePrice);
                }
                errorMsgList.add(edtRangePrice.getError());
            }
        } else {
            String rentError = edtRangeRent.getError();
            String mortgageError = edtMortgagePrice.getError();
            if (rentError == null && mortgageError == null) {
                long priceValue = edtMortgagePrice.getValueLong();
                long rentValue = edtRangeRent.getValueLong();
                if (priceValue <= 0 && rentValue <= 0) {
                    if (errorMsgList.size() == 0) {
                        scroolTo(edtMortgagePrice);
                    }
                    String message = getResources().getString(R.string.select_rent_value);
                    errorMsgList.add(message);
                } else {
                    if (priceValue < 1000 && priceValue != 0) {
                        if (errorMsgList.size() == 0) {
                            scroolTo(edtMortgagePrice);
                        }
                        String txt = getString(R.string.the_minimum_price_and_rent);
                        errorMsgList.add(txt);
                    }

                    if (rentValue < 1000 && rentValue != 0) {
                        if (errorMsgList.size() == 0) {
                            scroolTo(edtRangeRent);
                        }
                        String txt = getString(R.string.the_minimum_price_and_rent);
                        errorMsgList.add(txt);
                    }
                }
            } else {
                if (mortgageError != null) {
                    errorMsgList.add(mortgageError);
                }
                if (rentError != null) {
                    errorMsgList.add(rentError);
                }

            }
        }
        if (edtDescription.getError() != null) {
            if (errorMsgList.size() == 0) {
                scroolTo(edtDescription);
            }
            errorMsgList.add(edtDescription.getError());
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


    private void fillDemandReq() {
        req = new DemandRegisterReq();
        req.setContractTypeLocalId(selectedContractType.getContractTypeLocalId());
        req.setPropertyTypeLocalId(selectedPropertyType.getPropertyTypeLocalId());
        req.setLocationId(selectedArea != null ? selectedArea.getId() : Constants.getCity(getActivity()).getId());
//        req.setRegionId(selectedRegions != null ? selectedRegions.getId() : 0);
        if (selectedRegions != null && selectedRegions.size() > 0) {
            int allRegionsCount = TransactionRegion.getInstance().getRegionsCount(getActivity(), selectedArea.getId());
            if (selectedRegions.size() == allRegionsCount) {
                req.setRegionId(new ArrayList<>());
            } else {
                ArrayList<Integer> list = new ArrayList<>();
                for (int i = 0; i < selectedRegions.size(); i++) {
                    list.add(selectedRegions.get(i).getId());
                }
                req.setRegionId(list);
            }
        } else {
            req.setRegionId(new ArrayList<>());
        }
        req.setFullName(edtName.getValueString());
        req.setMobile(edtMobile.getValueString());
        req.setEmail(edtEmail.getValueString());
        req.setArea(edtRangeSquare.getValueInt());
        req.setDescription(edtDescription.getValue());

        if (selectedContractType.getType() == ContractTypeEnum.RENT) {
            req.setMortgageOrTotal(edtMortgagePrice.getValueLong());
            req.setRentOrMetric(edtRangeRent.getValueLong());
        } else {
            req.setMortgageOrTotal(edtRangePrice.getValueLong());
            req.setRentOrMetric(0);
        }

    }


    private void sendDemand() {
        if (getActivity() == null) {
            return;
        }

        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        DemandRegisterService.getInstance().demandRegister(getResources(), req, new ResponseListener<DemandRegisterResponse>() {
            @Override
            public void onGetError(String error) {
                if (getActivity() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    showErrorDialog(error);
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(DemandRegisterResponse response) {
                if (getView() != null && getActivity() != null) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        clearView();
                        if (response.getOrderCode() > 0 && !response.isNeedPayment()) {
                            PaymentDialog paymentDialog = new PaymentDialog(getActivity());
                            paymentDialog.enOrderId = response.getEnOrderId();
                            paymentDialog.orderId = response.getOrderCode();
                            paymentDialog.orderPrice = response.getOrderPrice();
                            paymentDialog.orderTitle = response.getOrderTitle();
                            paymentDialog.paymentTypeEnum = PaymentTypeEnum.DEMAND_REGISTER;
                            paymentDialog.listener = RegisterRequestFragment.this;
                            paymentDialog.show();
                        } else {
                            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
//                        Toast.makeText(getContext(), !TextUtils.isEmpty(response.getMessage()) ? response.getMessage() : getResources().getString(R.string.jsonError), Toast.LENGTH_SHORT).show();
                        showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                    }
                }
            }
        });
    }


    private void scroolTo(View view) {
        scroolView.scrollTo(0, (int) ((View) view.getParent()).getY());
    }

    public void showErrorDialog(String description) {
        if (getActivity() == null) {
            return;
        }
        CustomDialog customDialog = new CustomDialog(getActivity());
        customDialog.setOkListener(getString(R.string.retry_text), view -> {
            customDialog.dismiss();
            sendDemand();
        });
        customDialog.setCancelListener(getString(R.string.cancel), view -> customDialog.dismiss());
//        customDialog.setIcon(R.drawable.ic_bug_repoart, getResources().getColor(R.color.redColor));
        customDialog.setLottieAnim("error.json", 0);
        customDialog.setDescription(description);
        customDialog.setDialogTitle(getString(R.string.communicationError));
        customDialog.show();

    }

    private void hideInputKeyboard() {
        if (getActivity() != null) {
            if (this.edtEmail.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            if (this.edtMobile.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
            if (this.edtName.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_AREA_CODE) {
                areaSelected(data.getParcelableExtra(AREAS));
            } else if (requestCode == Constants.REQUEST_REGION_CODE) {
                regionSelected(data.getParcelableArrayListExtra(REGIONS));
            } else if (requestCode == Constants.REQUEST_PROPERTYTYPE_CODE) {
                propertyTypeSelected(data.getParcelableExtra(PROPERTY));
            }
//            else if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//                super.onActivityResult(requestCode, resultCode, data);
//            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onShowErrorListener(ArrayList<ModelStateErrors> modelStateErrors, String
            message) {
        showErrorFromServer(modelStateErrors, message);
    }

//    @Override
//    public void payemntInBillingBazar(String referralCode) {
//        token = referralCode;
//        mHelper.flagEndAsync();
//        mHelper.launchPurchaseFlow(getActivity(), SKU_PREMIUM, RC_REQUEST, mPurchaseFinishedListener, "payload-string");
//
//    }

    public static void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup) {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        if (mHelper != null) mHelper.dispose();
//        mHelper = null;
//    }
//
//    @Override
//    public void getJson(String json) {
//        dataPayment = json;
//    }
//
//    private void MasrafSeke(Purchase kala) {
//        // برای اینکه کاربر فقط یکبار بتواند از کالای فروشی استفاده کند
//        // باید بعد از خرید آن کالا را مصرف کنیم
//        // در غیر اینصورت کاربر با یکبار خرید محصول می تواند چندبار از آن استفاده کند
//        mHelper.consumeAsync(kala, new IabHelper.OnConsumeFinishedListener() {
//            @Override
//            public void onConsumeFinished(Purchase purchase, IabResult result) {
//                if (result.isSuccess())
//                    Log.d(TAG, "NATIJE masraf: " + result.getMessage() + result.getResponse());
//
//            }
//        });
//    }

    private void clearView() {
        req = null;
        edtRangePrice.setTextBody("");
        edtRangeRent.setTextBody("");
        edtRangeSquare.setTextBody("");
    }

}
