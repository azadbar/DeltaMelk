package ir.delta.delta.bottomNavigation.moreItems;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

/**
 * Created by m.azadbar on 9/27/2017.
 */

public class MagDeltaActivity extends BaseActivity {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.progreesBar)
    ProgressBar progreesBar;
    @BindView(R.id.webview)
    WebView webview;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mag_delta);
        ButterKnife.bind(this);
        Constants.setBackgroundProgress(this, progreesBar);
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getResources().getString(R.string.mag_delta));
        tvFilterDetail.setVisibility(View.GONE);

        waitingStart();
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                // Return the app name after finish loading
                if (progress == 100)
                    waitingStop();
            }
        });
        webview.loadUrl("https://delta.ir/ir.delta.delta.mag/");
        rlBack.setOnClickListener(view -> finish());
    }


    public void waitingStop() {
        progreesBar.setVisibility(View.GONE);
        webview.setVisibility(VISIBLE);

    }

    private void waitingStart() {
        webview.setVisibility(View.GONE);
        progreesBar.setVisibility(VISIBLE);
    }
}

