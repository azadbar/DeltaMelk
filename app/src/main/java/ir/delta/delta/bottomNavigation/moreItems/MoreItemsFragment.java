package ir.delta.delta.bottomNavigation.moreItems;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.deltanet.DeltaNetActivity;
import ir.delta.delta.demand.DemandListActivity;
import ir.delta.delta.enums.DirectionEnum;
import ir.delta.delta.estateDetail.estateByCode.EstateDetailByCodeActivity;
import ir.delta.delta.myEstate.ConsultantEstateActivity;
import ir.delta.delta.myEstate.UserEstateActivity;
import ir.delta.delta.profile.ConsultantProfileActivity;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.profile.UserProfileActivity;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.wallet.GetWalletAmountService;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.service.ResponseModel.wallet.WalletAmountResponse;
import ir.delta.delta.splash.SplashMagActivity;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import ir.delta.delta.wallet.WalletDialog;
import ir.delta.delta.wallet.WalletListActivity;
import ir.delta.delta.wallet.WalletListAdapter;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;


/**
 * Created by m.azadbar on 5/28/2018.
 */

public class MoreItemsFragment extends BaseFragment {


    @BindView(R.id.imgFavoritNextPage)
    BaseImageView imgFavoritNextPage;
    @BindView(R.id.rlFavoriteMyEstate)
    BaseRelativeLayout rlFavoriteMyEstate;
    @BindView(R.id.imgRecentVisits)
    BaseImageView imgRecentVisits;
    @BindView(R.id.imgRecentVisitsNext)
    BaseImageView imgRecentVisitsNext;
    @BindView(R.id.rlRecentVisits)
    BaseRelativeLayout rlRecentVisits;

    Unbinder unbinder;

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgAccountNextPage)
    BaseImageView imgAccountNextPage;
    @BindView(R.id.rlAccount)
    BaseRelativeLayout rlAccount;
    @BindView(R.id.imgMyAdsPage)
    BaseImageView imgMyAdsPage;
    @BindView(R.id.rlMyAds)
    BaseRelativeLayout rlMyAds;
    @BindView(R.id.imgSearchEstateByCodeNext)
    BaseImageView imgSearchEstateByCodeNext;
    @BindView(R.id.rlSearchEstateByCode)
    BaseRelativeLayout rlSearchEstateByCode;

    @BindView(R.id.imgAccount)
    BaseImageView imgAccount;
    @BindView(R.id.tvAccount)
    BaseTextView tvAccount;
    @BindView(R.id.rlDeltaNet)
    BaseRelativeLayout rlDeltaNet;
    @BindView(R.id.imgDeltaNetNext)
    BaseImageView imgDeltaNetNext;
    @BindView(R.id.ivPerson)
    CircleImageView ivPerson;
    @BindView(R.id.card_second)
    BaseLinearLayout cardSecond;
    @BindView(R.id.rlRequest)
    BaseRelativeLayout rlRequest;
    @BindView(R.id.tvRequest)
    BaseTextView tvRequest;
    @BindView(R.id.tvMyAds)
    BaseTextView tvMyAds;
    @BindView(R.id.card_third)
    BaseLinearLayout cardThird;
    @BindView(R.id.imgRequestPage)
    BaseImageView imgRequestPage;
    @BindView(R.id.imgFinancialTransactionsNext)
    BaseImageView imgFinancialTransactionsNext;
    @BindView(R.id.settingProfile)
    BaseTextView settingProfile;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.rlMagSection)
    BaseRelativeLayout rlMagSection;
    @BindView(R.id.card_five)
    BaseLinearLayout cardFive;
    @BindView(R.id.imgMagSectionNext)
    BaseImageView imgMagSectionNext;
    @BindView(R.id.cardWallet)
    CardView cardWallet;
    @BindView(R.id.inventoryValue)
    BaseTextView inventoryValue;
    @BindView(R.id.inventoryText)
    BaseTextView inventoryText;
    @BindView(R.id.rlIncreaseWallet)
    BaseRelativeLayout rlIncreaseWallet;
    @BindView(R.id.progressWallet)
    ProgressBar progressWallet;
    @BindView(R.id.rlFinancialTransactions)
    BaseRelativeLayout rlFinancialTransactions;

    public MoreItemsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_more_items, container, false);

        unbinder = ButterKnife.bind(this, view);

        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getResources().getString(R.string.profile));
        checkArrowRtlORLtr(imgBack);
        finishFragment(rlBack);
        if (BuildConfig.isCounsultant) {
            tvTitle.setText(null);
            cardFive.setVisibility(View.VISIBLE);
            rlBack.setVisibility(View.GONE);
        } else {
            tvTitle.setText(getResources().getString(R.string.mag_delta));
            cardFive.setVisibility(View.GONE);
            rlBack.setVisibility(View.VISIBLE);
        }
        rlFilterChange.setVisibility(View.GONE);
        rlChangeCity.setVisibility(View.VISIBLE);
        if (Constants.getLanguage().getDirection() == DirectionEnum.LTR) {
            imgRecentVisitsNext.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgFavoritNextPage.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgAccountNextPage.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgMyAdsPage.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgSearchEstateByCodeNext.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgDeltaNetNext.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgRequestPage.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgFinancialTransactionsNext.setBackgroundResource(R.drawable.ic_keyboard_english);
            imgMagSectionNext.setBackgroundResource(R.drawable.ic_keyboard_english);

        } else {
            imgRecentVisitsNext.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgFavoritNextPage.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgAccountNextPage.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgMyAdsPage.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgSearchEstateByCodeNext.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgDeltaNetNext.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgRequestPage.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgFinancialTransactionsNext.setBackgroundResource(R.drawable.ic_keyboard_arrow);
            imgMagSectionNext.setBackgroundResource(R.drawable.ic_keyboard_arrow);
        }
//
//        if (PreferencesData.isChangeCity(getActivity(), "isChangeCity")) {
//            new Handler().postDelayed(() -> showGuid(), 2000);
//            PreferencesData.setChangeCity(getActivity(), "isChangeCity", false);
//        }
        return view;

    }


    public void showGuid() {

        if (PreferencesData.isChangeCity(getActivity(), "isChangeCity")) {
            new Handler().postDelayed(() -> guidView(), 2000);
            PreferencesData.setChangeCity(getActivity(), "isChangeCity", false);
        }
    }

    private void guidView() {
        if (getActivity() != null) {
            new MaterialTapTargetPrompt.Builder(getActivity())
                    .setTarget(rlChangeCity)
                    .setPrimaryText(getResources().getString(R.string.guide_title_change_city))
                    .setPrimaryTextColour(getResources().getColor(R.color.white))
                    .setSecondaryTextColour(getResources().getColor(R.color.white))
                    .setPrimaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"))
                    .setSecondaryText(getTextDescription(getContext(), getResources().getString(R.string.giud_desc_change_city)))
                    .setSecondaryTextGravity(Gravity.LEFT)
                    .setSecondaryTextSize(R.dimen.text_size_2)
                    .setSecondaryTextTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"))
                    .setBackgroundColour(getResources().getColor(R.color.transparentGuide))
                    .setPromptStateChangeListener((prompt, state) -> {
                    })
                    .show();
        }
    }


    @Override
    public void resume() {
        User user = Constants.getUser(getActivity());
        if (user != null) {
            checkWalletUi(user);
        }
    }

    private void checkWalletUi(User user) {
        if (!user.isGeneral()) {
            cardWallet.setVisibility(View.VISIBLE);
            getWalletBalance();
        } else {
            cardWallet.setVisibility(View.GONE);
            rlFinancialTransactions.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        City city = Constants.getCity(getActivity());
        if (city != null && city.getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            tvCityTitle.setText(Constants.getCity(getActivity()).getName());
        }
        User user = Constants.getUser(getActivity());
        if (user != null) {
            checkWalletUi(user);
            settingProfile.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(user.getLastName())) {
                if (!TextUtils.isEmpty(user.getDisplayName())) {
                    tvAccount.setText(user.getLastName() + " (" + user.getDisplayName() + ")");
                } else {
                    tvAccount.setText(user.getLastName());
                }
            } else if (!TextUtils.isEmpty(user.getMobile())) {
                tvAccount.setText(user.getMobile());
            }

            if (user.isGeneral() || TextUtils.isEmpty(user.getImagePath())) {
                imgAccount.setVisibility(View.VISIBLE);
                imgAccount.setImageResource(R.drawable.ic_proflie_white);
                imgAccount.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
                imgAccount.setBackgroundResource(R.drawable.background_image_view_account);

            } else {
                imgAccount.setVisibility(View.INVISIBLE);
                Glide.with(getActivity()).load(user.getImagePath()).error(R.drawable.ic_profile).into(ivPerson);
            }

            cardThird.setVisibility(View.VISIBLE);
            if (user.isGeneral()) {
                rlDeltaNet.setVisibility(View.GONE);
                rlRequest.setVisibility(View.GONE);
                tvMyAds.setText(getResources().getString(R.string.my_file));
            } else {
                if (user.isShowDeltaNetMenu()) {
                    rlDeltaNet.setVisibility(View.VISIBLE);
                } else {
                    rlDeltaNet.setVisibility(View.GONE);
                }
                rlRequest.setVisibility(View.VISIBLE);
                tvMyAds.setText(getResources().getString(R.string.my_file));
            }
            rlMyAds.setVisibility(View.VISIBLE);

        } else {
            logout();
        }
    }

    private void getWalletBalance() {
        inventoryValue.setVisibility(View.GONE);
        progressWallet.setVisibility(View.VISIBLE);

        GetWalletAmountService.getInstance().getAmountWallet(getActivity(), getResources(), new ResponseListener<WalletAmountResponse>() {
            @Override
            public void onGetError(String error) {

            }

            @Override
            public void onSuccess(WalletAmountResponse response) {
                if (response.isSuccessed()) {
                    inventoryText.setText(getResources().getString(R.string.inventory) + ": ");
                    inventoryValue.setVisibility(View.VISIBLE);
                    progressWallet.setVisibility(View.GONE);
                    inventoryValue.setText((!TextUtils.isEmpty(response.getTomanString()) ? response.getTomanString() : "0") + " " + getResources().getString(R.string.toman));

                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }
        });
    }

    private void logout() {
        imgAccount.setVisibility(View.VISIBLE);
        tvAccount.setText(getResources().getString(R.string.login_account));
        imgAccount.setImageResource(R.drawable.ic_proflie_white);
        imgAccount.setBackgroundResource(R.drawable.background_image_view_account);
        cardThird.setVisibility(View.GONE);
        settingProfile.setVisibility(View.GONE);
        cardWallet.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.rlFavoriteMyEstate, R.id.rlRecentVisits, R.id.rlAccount,
            R.id.rlMyAds, R.id.rlSearchEstateByCode,
            R.id.rlChangeCity, R.id.rlRequest, R.id.rlDeltaNet,
            R.id.rlMagSection, R.id.rlIncreaseWallet, R.id.rlFinancialTransactions})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.rlFavoriteMyEstate:
                if (getActivity() == null) {
                    return;
                }
                if (getActivity() != null) {
                    if (getActivity() != null && getActivity() instanceof BottomBarActivity) {
                        BottomBarActivity bottomBarActivity = (BottomBarActivity) getActivity();
                        bottomBarActivity.likeItemClick();
                    }
                }
                break;
            case R.id.rlRecentVisits:
                if (getActivity() == null) {
                    return;
                }
                if (getActivity() != null) {
                    if (getActivity() != null && getActivity() instanceof BottomBarActivity) {
                        BottomBarActivity bottomBarActivity = (BottomBarActivity) getActivity();
                        bottomBarActivity.recentVisitItemClick();
                    }
                }
                break;

            case R.id.rlChangeCity:
                startCityActivity();
                break;

            case R.id.rlAccount:
                if (Constants.getUser(getActivity()) != null) {
                    if (Constants.getUser(getActivity()).isGeneral()) {
                        startActivity(new Intent(getActivity(), UserProfileActivity.class));
                    } else {
                        intent = new Intent(getActivity(), ConsultantProfileActivity.class);
                        startActivity(intent);
                    }

                } else {
                    intent = new Intent(getActivity(), LoginActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                break;
            case R.id.rlMyAds:
                if (Constants.getUser(getActivity()).isGeneral()) {
                    intent = new Intent(getActivity(), UserEstateActivity.class);
                    startActivity(intent);
                } else {
                    intent = new Intent(getActivity(), ConsultantEstateActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.rlSearchEstateByCode:
                intent = new Intent(getActivity(), EstateDetailByCodeActivity.class);
                startActivity(intent);
                break;
            case R.id.rlRequest:
                intent = new Intent(getActivity(), DemandListActivity.class);
                startActivity(intent);
                break;
            case R.id.rlDeltaNet:
                intent = new Intent(getActivity(), DeltaNetActivity.class);
                startActivity(intent);
                break;
            case R.id.rlMagSection:
                intent = new Intent(getActivity(), SplashMagActivity.class);
                startActivity(intent);
                break;
            case R.id.rlIncreaseWallet:
                WalletDialog dialog = new WalletDialog(getActivity());
                dialog.setCancelable(true);
                dialog.show();
                break;
            case R.id.rlFinancialTransactions:
                intent = new Intent(getActivity(), WalletListActivity.class);
                startActivity(intent);
                break;
        }
    }


}
