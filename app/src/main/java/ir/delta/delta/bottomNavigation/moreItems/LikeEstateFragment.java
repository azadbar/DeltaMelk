package ir.delta.delta.bottomNavigation.moreItems;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.customView.RoundedLoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.estateDetail.EstateDetailActivity;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.GetFavoriteConsultantPropertyService;
import ir.delta.delta.service.Request.GetFavoriteUserPropertyService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.UnFavoriteConsultantPropertyService;
import ir.delta.delta.service.Request.UnFavoriteUserPropertyService;
import ir.delta.delta.service.RequestModel.FavoritePropertyReq;
import ir.delta.delta.service.RequestModel.GetFavoriteReq;
import ir.delta.delta.service.ResponseModel.Estate;
import ir.delta.delta.service.ResponseModel.EstateResponse;
import ir.delta.delta.service.ResponseModel.FavoritePropertyResponse;
import ir.delta.delta.service.ResponseModel.myEstate.ConsultantEstate;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 5/28/2018.
 */

public class LikeEstateFragment extends BaseFragment implements LikeEstateAdapter.OnItemClickListener {

    Unbinder unbinder;
    @BindView(R.id.rvLikeItem)
    RecyclerView rvLikeItem;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.tvCityTitle)
    BaseTextView tvCityTitle;
    @BindView(R.id.rlProgress)
    ProgressBar rlProgress;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private LikeEstateAdapter adapter;
    private final ArrayList<Estate> estates = new ArrayList<>();
    private final int offset = 30;

    public LikeEstateFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_like_item, container, false);
        unbinder = ButterKnife.bind(this, view);
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getResources().getString(R.string.favorite_my_estate));
        tvFilterDetail.setVisibility(View.GONE);
        rlChangeCity.setVisibility(View.GONE);
        Constants.setBackgroundProgress(getActivity(), rlProgress);
        loadingView.setButtonClickListener(v -> getList());
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        resume();
    }

    @Override
    public void resume() {
        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            currentCityId = Constants.getCity(getActivity()).getId();
            tvCityTitle.setText(Constants.getCity(getActivity()).getName());
            tvFilterDetail.setText(Constants.getCity(getActivity()).getName());
        }

        if (Constants.getUser(getContext()) != null) {
            endOfList = false;
            estates.clear();
            getList();
        } else {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getResources().getString(R.string.login_account_or_like));
        }


    }

    private void getList() {
        if (!inLoading && !endOfList) {
            rootEmptyView.setVisibility(View.GONE);
            inLoading = true;
            if (estates.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            if (Constants.getUser(getContext()).isGeneral()) {
                getUserFavorite();
            } else {
                getConsultantProperty();
            }
        }
    }

    private void getConsultantProperty() {
        GetFavoriteReq req = new GetFavoriteReq();
        req.setStartIndex(estates.size());
        req.setOffset(offset);

        GetFavoriteConsultantPropertyService.getInstance().getFavoriteConsultantProperty(getContext(), getResources(), req, new ResponseListener<EstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    onError(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getContext());
                startActivity(intent);
                getActivity().finish();
            }

            @Override
            public void onSuccess(EstateResponse response) {
                if (getView() != null && isAdded()) {
                    enableDisableViewGroup(root, true);
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getSearchResult() != null) {
                        if (response.getSearchResult().size() < offset) {
                            endOfList = true;
                        }
                        estates.addAll(response.getSearchResult());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    private void getUserFavorite() {
        GetFavoriteReq req = new GetFavoriteReq();
        req.setStartIndex(estates.size());
        req.setOffset(offset);

        GetFavoriteUserPropertyService.getInstance().getFavoriteUserProperty(getContext(), getResources(), req, new ResponseListener<EstateResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    onError(getResources().getString(R.string.communicationError));
                }
            }

            @Override
            public void onAuthorization() {
                if (getActivity() != null) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                    Constants.setCurrentUser(null);
                    TransactionUser.getInstance().deleteUsr(getActivity());
                    startActivity(intent);
                    getActivity().finish();
                }
            }

            @Override
            public void onSuccess(EstateResponse response) {
                if (getView() != null && isAdded()) {
                    enableDisableViewGroup(root, true);
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getSearchResult() != null) {
                        if (response.getSearchResult().size() < offset) {
                            endOfList = true;
                        }
                        estates.addAll(response.getSearchResult());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new LikeEstateAdapter(getContext(), estates, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvLikeItem.setHasFixedSize(true);
            rvLikeItem.setLayoutManager(layoutManager);
            rvLikeItem.setItemAnimator(new DefaultItemAnimator());
            rvLikeItem.setAdapter(adapter);
            rvLikeItem.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvLikeItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.setList(estates);
        }

        if (adapter.getItemCount() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getResources().getString(R.string.not_result));
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onItemClick(Estate estate, int position) {
        Intent intent = new Intent(getContext(), EstateDetailActivity.class);
        intent.putExtra("encodedId", estate.getEncodedId());
        startActivity(intent);
    }

    @Override
    public void likeViewClicked(Estate estate) {
        if (getActivity() != null) {
            CustomDialog dialog = new CustomDialog(getActivity());
            dialog.setOkListener(getResources().getString(R.string.ok), view -> {
                if (Constants.getUser(getActivity()).isGeneral()) {
                    unFavoriteUserRequest(estate.getId());
                } else {
                    unFavoriteConsultantProperty(estate.getId());
                }
                if (adapter.getItemCount() == 0) {
                    rootEmptyView.setVisibility(View.VISIBLE);
                } else {
                    rootEmptyView.setVisibility(View.GONE);
                }
                dialog.dismiss();
            });
            dialog.setIcon(R.drawable.ic_cancel, getResources().getColor(R.color.redColor));
            dialog.setCancelListener(getResources().getString(R.string.cancel), view -> dialog.dismiss());
            dialog.setDialogTitle(getResources().getString(R.string.are_you_sure_you_want_to_delete));
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.show();
        }
    }

    public void unFavoriteUserRequest(int id) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);

        UnFavoriteUserPropertyService.getInstance().unFavoritUserProperty(getContext(), getContext().getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getActivity());
                getActivity().startActivity(intent);
            }

            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
//                        endOfList = false;
//                        estates.clear();
//                        getUserFavorite();
                        removeFromList(response.getDepositId());
                    } else {
                        showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));

                    }
                }
            }
        });
    }


    public void unFavoriteConsultantProperty(int id) {
        roundedLoadingView.setVisibility(View.VISIBLE);
        enableDisableViewGroup(root, false);
        FavoritePropertyReq req = new FavoritePropertyReq();
        req.setId(id);

        UnFavoriteConsultantPropertyService.getInstance().unFavoriteConsultantProperty(getActivity(), getActivity().getResources(), req, new ResponseListener<FavoritePropertyResponse>() {
            @Override
            public void onGetError(String error) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(getActivity());
                getActivity().startActivity(intent);
            }

            @Override
            public void onSuccess(FavoritePropertyResponse response) {
                if (getView() != null && isAdded()) {
                    roundedLoadingView.setVisibility(View.GONE);
                    enableDisableViewGroup(root, true);
                    if (response.isSuccessed()) {
                        Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                        removeFromList(response.getDepositId());
//                        endOfList = false;
//                        estates.clear();
//                        getConsultantProperty();
//                isFavoriteDeposit = false;
                    } else {
                        showErrorFromServer(response.getModelStateErrors(), getString(R.string.fill_following));

                    }
                }
            }
        });

    }

    private void removeFromList(int id) {
        for (Estate estate : estates) {
            if (estate.getId() == id) {
                estates.remove(estate);
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (adapter != null && adapter.getItemCount() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getResources().getString(R.string.not_result));
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }


    @OnClick({R.id.rlBack, R.id.rlChangeCity})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                if (getActivity() != null) {
                    ((BottomBarActivity) getActivity()).backToMoreItemTab(this);
                }
                break;
            case R.id.rlChangeCity:
                startCityActivity();
                break;
        }
    }


    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            ((BottomBarActivity) getActivity()).backToMoreItemTab(this);
        }
        return true;
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && estates != null) {
            rlLoding.setVisibility(View.GONE);
            if (estates.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }


    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvLikeItem.setVisibility(View.VISIBLE);
    }


    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvLikeItem.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }


}
