package ir.delta.delta.bottomNavigation.search;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.tablayout.TabLayoutActivity;

import static android.app.Activity.RESULT_OK;

public class LocationSelectFragment extends BaseFragment implements MultiSelectAdapter.OnItemClickListener, SingleSelectAdapter.OnItemClickListener {


    @BindView(R.id.rvSelectLocation)
    RecyclerView rvSelectLocation;
    @BindView(R.id.llSelectLocation)
    BaseLinearLayout llSelectLocation;
    @BindView(R.id.btnDone)
    BaseTextView btnDone;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.btnAllArea)
    BaseRelativeLayout btnAllArea;
    @BindView(R.id.tvTitleAllArea)
    BaseTextView tvTitleAllArea;
    @BindView(R.id.imgSelected)
    BaseImageView imgSelected;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.cvDone)
    CardView cvDone;
    private MultiSelectAdapter multiSelectAdapter;
    private Unbinder unbinder;
    private boolean isSingleLine;
    private String extraName;
    private LocationTypeEnum locationType;
    private int maxSelectedArea;
    private boolean isMultiSelect = false;
    private ArrayList<LocationInterface> list;
    private ArrayList<LocationInterface> selectedList;
    private String title;

    public LocationSelectFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_select_location, container, false);

        unbinder = ButterKnife.bind(this, view);
        rlBack.setVisibility(View.VISIBLE);
        imgLogo.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);

        Bundle b = getArguments();
        if (b != null) {
            isMultiSelect = b.getBoolean("isMultiSelect");
            isSingleLine = b.getBoolean("isSingleLine");
            maxSelectedArea = b.getInt("maxSelectedArea");
            extraName = b.getString("extraName");
            locationType = (LocationTypeEnum) b.getSerializable("locationType");
            list = b.getParcelableArrayList("list");
            selectedList = b.getParcelableArrayList("selectedList");
            title = b.getString("title");
        }

        if (!TextUtils.isEmpty(title)) {
            tvCenterTitle.setVisibility(View.VISIBLE);
            tvCenterTitle.setText(title);
            tvCenterTitle.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
        }

        if (isMultiSelect) {
            btnAllArea.setVisibility(View.VISIBLE);
            if (locationType ==LocationTypeEnum.REGION){
                tvTitleAllArea.setText(getString(R.string.all_regions));
            }else {
                tvTitleAllArea.setText(getString(R.string.all_areas));
            }

            if (selectedList.size() == 0 || selectedList.size() == list.size()) {
                imgSelected.setImageResource(R.drawable.ic_check_circle_green);
                imgSelected.setColorFilter(getResources().getColor(R.color.redColor));
            } else {
                imgSelected.setImageResource(R.drawable.ic_circle);
                imgSelected.setColorFilter(getResources().getColor(R.color.secondaryTextColor));
            }
            multiSelectAdapter = new MultiSelectAdapter(list, selectedList, locationType, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            rvSelectLocation.setLayoutManager(layoutManager);
            rvSelectLocation.setAdapter(multiSelectAdapter);
            cvDone.setVisibility(View.VISIBLE);
        } else {
            btnAllArea.setVisibility(View.GONE);
            SingleSelectAdapter singleAdapter = new SingleSelectAdapter(list, this, isSingleLine, locationType);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            rvSelectLocation.setLayoutManager(layoutManager);
            rvSelectLocation.setAdapter(singleAdapter);
            cvDone.setVisibility(View.GONE);
        }
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onItemClickMultiSelected(int position) {
        LocationInterface object = list.get(position);
        if (isMultiSelect) {
            int index = multiSelectAdapter.isSelected(object);
            if (index >= 0) {
                selectedList.remove(index);
            } else {
                if (selectedList.size() < 3) {
                    selectedList.add(object);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.max_of_select_area, maxSelectedArea), Toast.LENGTH_SHORT).show();
                }
            }
            multiSelectAdapter.notifyDataSetChanged();
            checkSelectedListStatus();
        }
    }


    @Override
    public void onItemAreaInfo(int position, LocationInterface area) {
        Intent tabLayout = new Intent(getActivity(), TabLayoutActivity.class);
        tabLayout.putExtra("area", area);
        startActivity(tabLayout);
    }

    public void checkSelectedListStatus() {
        if (selectedList.size() == 0) {
            imgSelected.setImageResource(R.drawable.ic_check_circle_green);
            imgSelected.setColorFilter(getResources().getColor(R.color.redColor));
        } else {
            imgSelected.setImageResource(R.drawable.ic_circle);
            imgSelected.setColorFilter(getResources().getColor(R.color.secondaryTextColor));
        }

    }

    @Override
    public void onItemClickSingle(int position) {
        Intent intent = new Intent(getContext(), LocationSelectFragment.class);
        if (isMultiSelect) {
            intent.putParcelableArrayListExtra(extraName, new ArrayList<Parcelable>(selectedList));
        } else {
            intent.putExtra(extraName, list.get(position));
        }
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
        }
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }

    }

    @OnClick({R.id.btnDone, R.id.btnAllArea, R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnDone:
                if (selectedList.size() <= 3) {
                    Collections.sort(selectedList);
                    Intent intent = new Intent(getContext(), LocationSelectFragment.class);
                    intent.putExtra(extraName, selectedList);
                    if (getTargetFragment() != null) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                    }
                    if (getActivity() != null) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                } else {
                    ArrayList<String> errorMsg = new ArrayList<>();
                    errorMsg.add(getResources().getString(R.string.max_of_select_area_or_all_area, maxSelectedArea));
                    showInfoDialog(getString(R.string.attetion), errorMsg);
                }
                break;
            case R.id.btnAllArea:
                selectedList.clear();
                imgSelected.setImageResource(R.drawable.ic_check_circle_green);
                imgSelected.setColorFilter(getResources().getColor(R.color.redColor));
                multiSelectAdapter.notifyDataSetChanged();
//                checkSelectedListStatus();
                break;
            case R.id.rlBack:
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                break;
        }
    }


    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return true;
    }

    @Override
    public boolean isHiddenNavigation() {
        return true;
    }
}
