package ir.delta.delta.bottomNavigation.search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.Estate;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SearchEstateAdapter extends RecyclerView.Adapter<SearchEstateAdapter.ViewHolder> {


    private ArrayList<Estate> list;
    private final OnItemClickListener listener;
    private final Context context;
    private boolean isSuggationEstate;
    private boolean isFavoriteDeposit;

    void setSuggationEstate(boolean suggationEstate) {
        isSuggationEstate = suggationEstate;
    }


    SearchEstateAdapter(Context context, ArrayList<Estate> list, OnItemClickListener listener, boolean isSuggationEstate) {
        this.list = list;
        this.listener = listener;
        this.context = context;
        this.isSuggationEstate = isSuggationEstate;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_estate, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        Estate object = list.get(position);

        if (isSuggationEstate) {
            holder.tvTitleEstate.setText(object.getContractType() + " " + object.getPropertyType() + "،" + object.getLocation() + "، " + object.getAddress());
        } else {
            holder.tvTitleEstate.setText(object.getDisplayText());
        }
        holder.tvDetailsEstate.setText(object.getDisplayTitle());


        holder.tvDuration.setText(object.getDisplayDate());
        if (!TextUtils.isEmpty(object.getMortgageOrTotalDisplayTitle()) && object.getMortgageOrTotal() >= 1000) {
            holder.tvBuyPrice.setText(createHtmlText("<font color='#999999'>" + object.getMortgageOrTotalDisplayTitle() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(object.getMortgageOrTotal(), res) + "</strong></font>"));
        } else {
            holder.tvBuyPrice.setText(null);
        }

        if (!TextUtils.isEmpty(object.getRentOrMetricDisplayTitle()) && object.getRentOrMetric() >= 1000) {
            holder.tvRentPrice.setText(createHtmlText("<font color='#999999'>" + object.getRentOrMetricDisplayTitle() + ": " + "</font>" +
                    "<font color='#000000'><strong>" + Constants.priceConvertor(object.getRentOrMetric(), res) + "</strong></font>"));
        } else {
            holder.tvRentPrice.setText(null);
        }

        int width = res.getDimensionPixelOffset(R.dimen.height_size_item_search_estate);
        ViewGroup.LayoutParams lp = holder.imageEstate.getLayoutParams();
        lp.height = (int) (width / Constants.imageRatio);
        holder.imageEstate.setLayoutParams(lp);

        Glide.with(context).load(object.getSrcImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .into(holder.imageEstate);


        if (object.isShowOfficeLogo()) {
            holder.logoRealEstate.setVisibility(View.VISIBLE);
            Glide.with(context).load(object.getLogoSrc())
                    .placeholder(R.drawable.placeholder)
                    .into(holder.logoRealEstate);
        } else {
            holder.logoRealEstate.setVisibility(View.GONE);
        }

//        holder.imgFavorite.setVisibility(View.GONE);//delete favorite image in search estate list
        if (object.isLiked()) {
            setLike(holder);
        } else {
            setUnLike(holder);
        }

        holder.bind(object, position, listener);

        holder.imgFavorite.setOnClickListener(view -> listener.onLikeItem(position, object));
    }

    private void setLike(ViewHolder holder) {
        holder.imgFavorite.setImageResource(R.drawable.ic_bookmark);
        holder.imgFavorite.setColorFilter(holder.imgFavorite.getContext().getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
    }

    private void setUnLike(ViewHolder holder) {
        holder.imgFavorite.setImageResource(R.drawable.ic_bookmark_border);
        holder.imgFavorite.setColorFilter(holder.imgFavorite.getContext().getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
    }


    public Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateViewInRecycle(int estatePosition, boolean isFavoriteDeposit) {
        list.get(estatePosition).setLiked(isFavoriteDeposit);
        notifyDataSetChanged();
    }

    public void adapternotify(ArrayList<Estate> estates) {
        list.clear();
        this.list = estates;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageEstate)
        BaseImageView imageEstate;
        @BindView(R.id.tvDuration)
        BaseTextView tvDuration;
        @BindView(R.id.imgFavorite)
        BaseImageView imgFavorite;
        @BindView(R.id.tvTitleEstate)
        BaseTextView tvTitleEstate;
        @BindView(R.id.tvDetailsEstate)
        BaseTextView tvDetailsEstate;
        @BindView(R.id.tvBuyPrice)
        BaseTextView tvBuyPrice;
        @BindView(R.id.tvRentPrice)
        BaseTextView tvRentPrice;
        @BindView(R.id.logoRealEstate)
        BaseImageView logoRealEstate;
        @BindView(R.id.framelayout)
        FrameLayout framelayout;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(Estate estate, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, estate));
        }


        public void likeViewClicked(int position, Estate object, OnItemClickListener listener) {
            imgFavorite.setOnClickListener(v -> listener.onLikeItem(position, object));
        }
    }


    public void setList(ArrayList<Estate> searchResult) {
        this.list = searchResult;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(int position, Estate estate);

        void onLikeItem(int position, Estate estate);
    }

}
