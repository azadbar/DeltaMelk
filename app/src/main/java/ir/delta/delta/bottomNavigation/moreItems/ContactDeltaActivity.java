package ir.delta.delta.bottomNavigation.moreItems;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.Model.ContactDelta;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.baseView.BaseToolbar;
import ir.delta.delta.enums.ContactDeltaTypeEnum;
import ir.delta.delta.enums.TypeCommunityAdsEnum;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

/**
 * Created by m.Azadbar on 9/27/2017.
 */

public class ContactDeltaActivity extends BaseActivity implements ContactAdapter.OnItemClickListener {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.imgMag)
    BaseImageView imgMag;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgSearch)
    BaseImageView imgSearch;
    @BindView(R.id.toolbar_main)
    BaseToolbar toolbarMain;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.rvWaysOfCommunication)
    RecyclerView rvWaysOfCommunication;
    @BindView(R.id.rvDeltaMagazineInMedia)
    RecyclerView rvDeltaMagazineInMedia;
    @BindView(R.id.tvVersion)
    BaseTextView tvVersion;
    private ContactDelta contactDelta;
    private ArrayList<ContactDelta> list;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);

        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(getResources().getColor(R.color.grayHeaderPram));
        rlBack.setVisibility(VISIBLE);
        imgBack.setImageResource(R.drawable.ic_back);
        imgBack.setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_ATOP);
        tvTitle.setText(getResources().getString(R.string.back));
        tvTitle.setTextColor(getResources().getColor(R.color.black));

        rlContacrt.setVisibility(VISIBLE);
        tvFilterTitle.setText(R.string.contact_us);
        tvFilterTitle.setTextColor(getResources().getColor(R.color.magToolbarFore));
        tvFilterDetail.setVisibility(View.GONE);
        toolbarMain.setBackgroundColor(getResources().getColor(R.color.magToolbarBack));
        imgMag.setVisibility(View.GONE);
        imgSearch.setVisibility(View.GONE);
        webView.scrollTo(Constants.getScreenSize(this).x/2, (int) (getResources().getDimension(R.dimen.width_linear_tired_four)/2));

        setMapView();
        setRelationWay();
        setMagInMedia();
        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getPackageName(), 0);
            tvVersion.setText(getResources().getString(R.string.version)+" "+info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void setMagInMedia() {
        createListMagInMediaDelta();

        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {

            int screenWidth = Constants.getScreenSize(windowManager).x;
            cellWidth = (int) ((screenWidth - (4 + 1) * (offset * 2)) / (double) (4));
        }
        ContactAdapter adapter = new ContactAdapter(this, cellWidth, createListMagInMediaDelta(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvDeltaMagazineInMedia.setLayoutManager(layoutManager);
        rvDeltaMagazineInMedia.setAdapter(adapter);
    }

    private ArrayList<ContactDelta> createListMagInMediaDelta() {
        list = new ArrayList<>();
        contactDelta = new ContactDelta();
        contactDelta.setId(3);
        contactDelta.setTitle(getResources().getString(R.string.instagram));
        contactDelta.setImage(R.drawable.instagram);
        contactDelta.setType(ContactDeltaTypeEnum.INSTAGRAM);
        contactDelta.setValue(getResources().getString(R.string.instagram_id));
        list.add(contactDelta);
        contactDelta = new ContactDelta();
        contactDelta.setId(4);
        contactDelta.setTitle(getResources().getString(R.string.linkdin));
        contactDelta.setImage(R.drawable.linkedin);
        contactDelta.setType(ContactDeltaTypeEnum.LINKEDEN);
        contactDelta.setValue(getResources().getString(R.string.linkeden_id));
        list.add(contactDelta);
        contactDelta = new ContactDelta();
        contactDelta.setId(5);
        contactDelta.setTitle(getResources().getString(R.string.aparat));
        contactDelta.setImage(R.drawable.aparat);
        contactDelta.setType(ContactDeltaTypeEnum.APARAT);
        contactDelta.setValue("https://www.aparat.com/Deltamagazin");
        list.add(contactDelta);
        contactDelta = new ContactDelta();
        contactDelta.setId(6);
        contactDelta.setTitle(getResources().getString(R.string.web));
        contactDelta.setImage(R.drawable.web);
        contactDelta.setType(ContactDeltaTypeEnum.WEB);
        contactDelta.setValue("https://deltapayam.com");
        list.add(contactDelta);
        return list;
    }

    private void setMapView() {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(Constants.IMAGE_URL_ADS + "map.html?lat=" + 35.7485983
                + "&lng=" + 51.4136342 + "&isMelki=" + 2);
    }

    private void setRelationWay() {
        createListRelationDelta();

        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {

            int screenWidth = Constants.getScreenSize(windowManager).x;
            cellWidth = (int) ((screenWidth - (4 + 1) * (offset * 2)) / (double) (4));
        }
        ContactAdapter adapter = new ContactAdapter(this, cellWidth, createListRelationDelta(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvWaysOfCommunication.setLayoutManager(layoutManager);
        rvWaysOfCommunication.setAdapter(adapter);
    }

    private ArrayList<ContactDelta> createListRelationDelta() {
        list = new ArrayList<>();
        contactDelta = new ContactDelta();
        contactDelta.setId(0);
        contactDelta.setTitle(getResources().getString(R.string.phone));
        contactDelta.setImage(R.drawable.ic_call_white);
        contactDelta.setType(ContactDeltaTypeEnum.PHONE);
        contactDelta.setValue(getResources().getString(R.string.support_delta_phone_number));
        list.add(contactDelta);
        contactDelta = new ContactDelta();
        contactDelta.setId(1);
        contactDelta.setTitle(getResources().getString(R.string.email));
        contactDelta.setImage(R.drawable.ic_email_white);
        contactDelta.setType(ContactDeltaTypeEnum.EMAIL);
        contactDelta.setValue(getString(R.string.support_email_delta));
        list.add(contactDelta);
        contactDelta = new ContactDelta();
        contactDelta.setId(2);
        contactDelta.setTitle(getResources().getString(R.string.watsapp));
        contactDelta.setImage(R.drawable.whatsapp);
        contactDelta.setType(ContactDeltaTypeEnum.WATS_APP);
        contactDelta.setValue("98"+getResources().getString(R.string.support_delta_phone_number));
        list.add(contactDelta);
        return list;

    }


    @Override
    public void onItemClick(ContactDelta object, int position) {
        Intent intent = null;
        if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.PHONE)) {
            openCallIntent(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.EMAIL)) {
            openEmail(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.WATS_APP)) {
            openWatsapp(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.INSTAGRAM)) {
            openInstagram(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.LINKEDEN)) {
            openLinkeden(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.APARAT)) {
            openWebSite(object.getValue());
        } else if (object.getType() != null && object.getType().equals(ContactDeltaTypeEnum.WEB)) {
            openWebSite(object.getValue());
        }
    }

    private void openLinkeden(String value) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://add/%@" + value));
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.isEmpty()) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/profile/view?id=" + value));
        }
        startActivity(intent);
    }

    private void openInstagram(String value) {
        Uri uri = Uri.parse("http://instagram.com/_u/" + value);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
        likeIng.setPackage("com.instagram.android");
        try {
            startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + value)));
        }
    }
    private void openWatsapp(String value) {
        String url = "https://api.whatsapp.com/send?phone="+value;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openEmail(String value) {
        try {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{value});
            intent.putExtra(Intent.EXTRA_SUBJECT, "App feedback");
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email client installed on your device.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openCallIntent(String call) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + call));
        startActivity(intent);
    }


    private void openWebSite(String value) {
        if (URLUtil.isValidUrl(value)) {
            Intent intent = new Intent(this, WebSiteDeltaActivity.class);
            intent.putExtra("url", value);
            startActivity(intent);
        } else {
            Toast.makeText(this, "متاسفانه خطایی رخ داده است", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.rlBack)
    public void onViewClicked() {
        finish();
    }
}
