package ir.delta.delta.bottomNavigation.search;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.SearchParam;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SearchMaxParamAdapter extends RecyclerView.Adapter<SearchMaxParamAdapter.ViewHolder> {


    private final ArrayList<SearchParam> list;
    private final OnItemClickListener listener;
    private SearchParam searchParam;

    private final Resources resources;
    private SearchParam minSearchParam;

    public SearchMaxParamAdapter(Resources resources, ArrayList<SearchParam> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        this.resources = resources;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_value_price_meter, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SearchParam object = list.get(position);
        holder.tvTitle.setText(object.getText());

        if (minSearchParam != null && minSearchParam.getValue() >= object.getValue()) {
            setDisableColor(holder);
        } else if (searchParam != null && searchParam.getValue() == object.getValue()) {
            setSelcted(holder);
        } else {
            setNormal(holder);
        }

        holder.bind(position, listener);

    }



    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgSelect)
        BaseImageView imgSelect;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.row)
        BaseRelativeLayout row;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickMax(position));
        }

    }

    public interface OnItemClickListener {
        void onItemClickMax(int position);
    }

    public void setSearchParam(SearchParam searchParam) {
        this.searchParam = searchParam;
        notifyDataSetChanged();
    }

    public void setMinSearchParam(SearchParam minSearchParam) {
        this.minSearchParam = minSearchParam;
        notifyDataSetChanged();
    }

    private void setNormal(ViewHolder holder) {
        holder.tvTitle.setTextColor(resources.getColor(R.color.primaryTextColor));
        holder.imgSelect.setImageResource(R.drawable.ic_circle);
        holder.imgSelect.setColorFilter(resources.getColor(R.color.secondaryTextColor));
    }

    private void setSelcted(ViewHolder holder) {
        holder.tvTitle.setTextColor(resources.getColor(R.color.redColor));
        holder.imgSelect.setImageResource(R.drawable.ic_check_circle_secondary);
        holder.imgSelect.setColorFilter(resources.getColor(R.color.redColor));
    }

    private void setDisableColor(ViewHolder holder) {
        holder.tvTitle.setTextColor(resources.getColor(R.color.divider));
        holder.imgSelect.setImageResource(R.drawable.ic_circle_white);
    }
}
