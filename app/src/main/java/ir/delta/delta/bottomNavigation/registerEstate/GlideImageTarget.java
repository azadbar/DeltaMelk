package ir.delta.delta.bottomNavigation.registerEstate;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class GlideImageTarget extends CustomTarget<Bitmap> {
    private final int listSize;
    private final ArrayList<String> list;
    private final int position;
    private final OnImageLoader onImageLoader;

    public GlideImageTarget(int position, int listSize, ArrayList<String> list, OnImageLoader onImageLoader) {
        this.position = position;
        this.listSize = listSize;
        this.list = list;
        this.onImageLoader = onImageLoader;
    }

    @Override
    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
        list.add(convertBitmapToBase64(resource));
        if (onImageLoader != null) {
            onImageLoader.onfinish(list, position);
        }

    }

    @Override
    public void onLoadCleared(@Nullable Drawable placeholder) {
        if (onImageLoader != null) {
            onImageLoader.onError(position);
        }
    }

   /* @Override
    public void onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//        super.onLoadFailed(errorDrawable);
        if (onImageLoader != null) {
            onImageLoader.onError(position);
        }
    }*/


    private String convertBitmapToBase64(Bitmap bitmap) {
        System.gc();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        try {
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (OutOfMemoryError e) {
            System.gc();
            byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
            bitmap.recycle();
            byteArray = byteArrayOutputStream.toByteArray();
            try {
                return Base64.encodeToString(byteArray, Base64.DEFAULT);
            } catch (OutOfMemoryError ee) {
                System.gc();
                byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
                bitmap.recycle();
                byteArray = byteArrayOutputStream.toByteArray();
                return Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
        } finally {
            bitmap.recycle();
        }
    }



    public interface OnImageLoader {
        void onfinish(ArrayList<String> list, int position);

        void onError(int position);
    }

}