package ir.delta.delta.bottomNavigation.search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.BottomBarActivity;
import ir.delta.delta.customView.CustomSwitchButton;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.customView.RangeView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionFilter;
import ir.delta.delta.dialog.SearchParamDialog;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.enums.RangeEnum;
import ir.delta.delta.enums.SearchParamType;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.service.ResponseModel.Region;
import ir.delta.delta.service.ResponseModel.SearchParam;
import ir.delta.delta.service.ResponseModel.SearchParamInfo;
import ir.delta.delta.util.Constants;

import static android.app.Activity.RESULT_OK;

public class FilterFragment extends BaseFragment implements BottomBarActivity.onClickGuide {
    @BindView(R.id.area_layout)
    LocationView areaLayout;
    @BindView(R.id.region_layout)
    LocationView regionLayout;
    @BindView(R.id.propertyType_layout)
    LocationView propertyTypeLayout;
    @BindView(R.id.location_layout)
    BaseLinearLayout locationLayout;
    @BindView(R.id.price_layout)
    RangeView priceLayout;
    @BindView(R.id.rent_layout)
    RangeView rentLayout;
    @BindView(R.id.meter_layout)
    RangeView meterLayout;
    @BindView(R.id.total_meter_layout)
    RangeView totalMeterLayout;
    @BindView(R.id.value_layout)
    BaseLinearLayout valueLayout;
    @BindView(R.id.btnDone)
    BaseTextView btnDone;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.rlChangeCity)
    BaseRelativeLayout rlChangeCity;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.imgLocation)
    BaseImageView imgLocation;
    @BindView(R.id.card_first)
    CustomSwitchButton cardFirst;
    @BindView(R.id.tvFill)
    BaseTextView tvFill;

    private Unbinder unbinder;
    private static final int REQUEST_AREA_CODE = 100;
    private static final int REQUEST_REGION_CODE = 101;
    private static final int REQUEST_PROPERTYTYPE_CODE = 102;
    private static final int REQUEST_SEARCH_PARAM = 110;
    private static final int maxSelectedArea = 3;
    private final String AREAS = "areas";
    private final String PROPERTY = "property";
    private final String REGINS = "regions";
    private ArrayList<Region> selectedRegions;
    private ContractObject selectedContractType;
    private ArrayList<Area> selectedAreas;
    private PropertyType selectedPropertyType;
    private SearchParam selectedMinPrice;
    private SearchParam selectedMaxPrice;
    private SearchParam selectedMinRent;
    private SearchParam selectedMaxRent;
    private SearchParam selectedMinMeter;//متراژ یا متراژ بنا
    private SearchParam selectedMaxMeter;//متراژ یا متراژ بنا
    private SearchParam selectTotalMinMeter;//مساحت زمین
    private SearchParam selectTotalMaxMeter;//مساحت زمین
    private boolean isRentSlected = false;
    public Filter filter;
    private boolean notMeter;
    private boolean notPrice;
    private boolean notRent;

    public FilterFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            filter = bundle.getParcelable("filter");
        }
        unbinder = ButterKnife.bind(this, view);
        rlBack.setVisibility(View.VISIBLE);
        imgBack.setBackgroundResource(R.drawable.ic_clear_black_24dp);
        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.VISIBLE);
        tvFilterTitle.setText(getString(R.string.search_home));
        tvFilterDetail.setText(Constants.getCity(getActivity()).getName());


//        rlChangeCity.setVisibility(View.GONE);
//        rlChangeCity.setBackground(null);
//        imgLocation.setVisibility(View.GONE);
//        tvCityTitle.setText(Constants.getCity(getActivity()).getName());
        areaLayout.setImgInfo(R.drawable.ic_location_gray);
        areaLayout.setTxtTitle(getString(R.string.area_text));
        cardFirst.setSections(getString(R.string.buy_text), getString(R.string.rent_txt));
        cardFirst.setListener(index -> {
            if (index == 0) {
                chanageContractType(false);
            } else if (index == 1) {
                chanageContractType(true);
            }
        });
        priceLayout.setActivity(getActivity());
        rentLayout.setActivity(getActivity());
        meterLayout.setActivity(getActivity());
        totalMeterLayout.setActivity(getActivity());

        regionLayout.setImgInfo(R.drawable.ic_location_gray);
        regionLayout.setTxtTitle(getString(R.string.region_text)+getString(R.string.dots));

        propertyTypeLayout.setImgInfo(R.drawable.ic_location_gray);
        propertyTypeLayout.setTxtTitle(getString(R.string.property_text));
        propertyTypeLayout.setHint(getString(R.string.click_to_select));
        areaLayout.setHint(getString(R.string.your_estat));
        regionLayout.setHint(getString(R.string.your_region));
        priceLayout.setHint(getString(R.string.your_region));
        rentLayout.setHint(getString(R.string.your_region));
        meterLayout.setHint(getString(R.string.your_region));
        totalMeterLayout.setHint(getString(R.string.your_region));

        priceLayout.setImgInfo(R.drawable.ic_location_gray);
        rentLayout.setImgInfo(R.drawable.ic_location_gray);
        meterLayout.setImgInfo(R.drawable.ic_location_gray);
        totalMeterLayout.setImgInfo(R.drawable.ic_location_gray);

        priceLayout.setButtonClickListener(v -> {
            selectedMaxPrice = null;
            selectedMinPrice = null;
            notPrice = !notPrice;
            purchaseFillHint(notMeter, notPrice, notRent);
        });

        rentLayout.setButtonClickListener(v -> {
            selectedMaxRent = null;
            selectedMinRent = null;
            rentLayout.reset();
            notRent = !notRent;
            purchaseFillHint(notMeter, notPrice, notRent);
        });

        meterLayout.setButtonClickListener(v -> {
            selectedMaxMeter = null;
            selectedMinMeter = null;
            notMeter = !notMeter;
            purchaseFillHint(notMeter, notPrice, notRent);
        });


//        if (PreferencesData.isFirstFilterOpened(getActivity(), "isFilterOpened")) {
//            guideView();
//            PreferencesData.setFirstFilterOpened(getActivity(), "isFilterOpened", false);
//        }
        setFilter();
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        if (Constants.getCity(getActivity()).getId() != currentCityId) {
            resume();
        }
    }

    @Override
    public void resume() {
        currentCityId = Constants.getCity(getActivity()).getId();
        tvFilterDetail.setText(Constants.getCity(getActivity()).getName());
        filter = TransactionFilter.getInstance().getLastFilter(getActivity(), Constants.getCity(getActivity()).getId());
        setFilter();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void setFilter() {
        if (filter != null) {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(filter.getContractTypeLocalId(), Constants.getCity(getActivity()).getLocationFeatureLocalId());
            chanageContractType(selectedContractType.getType() == ContractTypeEnum.RENT);
            if (Constants.getCity(getActivity()).getId() == filter.getCityId()) {
                if (filter.getAreaIdList() != null) {
                    selectedAreas = TransactionArea.getInstance().getAreas(getActivity(), filter.getAreaIdList());
                }
                areaSelected(selectedAreas);
//                if (selectedArea != null) {
//                    selectedRegions = TransactionRegion.getInstance().getRegions(filter.getRegionListFilter());
//                } else {
//                    selectedRegions = null;
//                }
//                regionSelected(selectedRegions);
                if (selectedContractType != null) {
                    selectedPropertyType = selectedContractType.getPropertyTypeBy(filter.getPropertyTypeLocalId());
                }
                propertyTypeSelected(selectedPropertyType);

                if (selectedPropertyType != null && selectedPropertyType.getSearchParamInfos() != null) {
                    SearchParamInfo searchParamInfo = selectedPropertyType.getSearchParamInfos();

                    selectedMinPrice = searchParamInfo.getPriceParam(filter.getMinMortgageOrTotalId());
                    selectedMaxPrice = searchParamInfo.getPriceParam(filter.getMaxMortgageOrTotalId());

                    selectedMinRent = searchParamInfo.getRentParam(filter.getMinRentOrMetricId());
                    selectedMaxRent = searchParamInfo.getRentParam(filter.getMaxRentOrMetricId());

                    selectedMinMeter = searchParamInfo.getAreaParam(filter.getMinPropertyAreaId());
                    selectedMaxMeter = searchParamInfo.getAreaParam(filter.getMaxPropertyAreaId());

                    selectTotalMinMeter = searchParamInfo.getTotalParam(filter.getMinPropertyTotalAreaId());
                    selectTotalMaxMeter = searchParamInfo.getTotalParam(filter.getMaxPropertyTotalAreaId());
                    setSection2();
                }
            } else {
                filter = null;
                areaSelected(null);
                regionSelected(null);
                selectedContractType = Constants.getContractObjects(getActivity()).getContract(ContractTypeEnum.PURCHASE.getMethodCode(), Constants.getCity(getActivity()).getLocationFeatureLocalId());
                setContractType(selectedContractType.getType());
                chanageContractType(selectedContractType.getType() == ContractTypeEnum.RENT);
            }
        } else {
            areaSelected(null);
            regionSelected(null);
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(ContractTypeEnum.PURCHASE.getMethodCode(), Constants.getCity(getActivity()).getLocationFeatureLocalId());
            setContractType(selectedContractType.getType());
            chanageContractType(selectedContractType.getType() == ContractTypeEnum.RENT);
        }

    }


    private void setSection2() {
        if (selectedPropertyType != null) {
            fillSection2();
        } else {
            selectedMinPrice = null;
            selectedMaxPrice = null;
            selectedMinMeter = null;
            selectedMaxMeter = null;
            selectedMinRent = null;
            selectedMaxRent = null;
            selectTotalMinMeter = null;
            selectTotalMaxMeter = null;
            priceLayout.setVisibility(View.GONE);
            rentLayout.setVisibility(View.GONE);
            meterLayout.setVisibility(View.GONE);
            totalMeterLayout.setVisibility(View.GONE);
        }
    }

    private void fillSection2() {

        if (selectedPropertyType.getPriceList().size() == 0 || selectedPropertyType.getMeterList().size() == 0) {
            tvFill.setVisibility(View.GONE);
        } else {
            tvFill.setVisibility(View.VISIBLE);
        }
        //set price
        if (selectedPropertyType.getPriceList().size() > 0) {
            priceLayout.setVisibility(View.VISIBLE);
            if (selectedContractType.getType() == ContractTypeEnum.RENT) {
                priceLayout.setTxtTitle(getString(R.string.deposit_text));
            } else {
                priceLayout.setTxtTitle(getString(R.string.price_text));
            }
            RangeEnum fromIndex = selectedPropertyType.getPriceRange(selectedMinPrice);
            RangeEnum toIndex = selectedPropertyType.getPriceRange(selectedMaxPrice);
            priceLayout.setRange(selectedMinPrice, selectedMaxPrice, fromIndex, toIndex);
        } else {
            priceLayout.setVisibility(View.GONE);
        }

        ///////////////////////////////////////////////set rent///////////////////
        if (selectedPropertyType.getRentList().size() > 0) {
            if (selectedContractType.getType() == ContractTypeEnum.RENT) {
                rentLayout.setVisibility(View.VISIBLE);
                rentLayout.setTxtTitle(getString(R.string.rent_text) + "(" + getString(R.string.toman) + ")");
                RangeEnum fromRentIndex = selectedPropertyType.getRentRange(selectedMinRent);
                RangeEnum toRentIndex = selectedPropertyType.getRentRange(selectedMaxRent);
                rentLayout.setRange(selectedMinRent, selectedMaxRent, fromRentIndex, toRentIndex);
            } else {
                rentLayout.setVisibility(View.GONE);
            }
        } else {
            rentLayout.setVisibility(View.GONE);
        }
////////////////////////////set meter ///////////////////////////////
        if (selectedPropertyType.getMeterList().size() > 0) {
            totalMeterLayout.setVisibility(View.GONE);
            if (selectedPropertyType.isShowTotalAreaFilter()) {
                //meter
                meterLayout.setTxtTitle(getString(R.string.building_area));
                //total area
            } else {
                meterLayout.setTxtTitle(getString(R.string.meter_text));
            }
            meterLayout.setVisibility(View.VISIBLE);
            RangeEnum fromAreaIndex = selectedPropertyType.getAreaRange(selectedMinMeter);
            RangeEnum toAreaIndex = selectedPropertyType.getAreaRange(selectedMaxPrice);
            meterLayout.setRange(selectedMinMeter, selectedMaxMeter, fromAreaIndex, toAreaIndex);
        } else {
            meterLayout.setVisibility(View.GONE);
            totalMeterLayout.setVisibility(View.GONE);
        }

        notMeter = selectedPropertyType.getMeterList().size() > 0 && selectedMinMeter == null && selectedMaxMeter == null;
        notPrice = selectedPropertyType.getPriceList().size() > 0 && selectedMinPrice == null && selectedMaxPrice == null;
        notRent = selectedPropertyType.getPriceList().size() > 0 && selectedMinRent == null && selectedMaxRent == null;
        purchaseFillHint(notMeter, notPrice, notRent);

    }

    private void purchaseFillHint(boolean notMeter, boolean notPrice, boolean notRent) {
        if (selectedContractType.getType() == ContractTypeEnum.PURCHASE) {
            if (notMeter && notPrice) {
                priceLayout.setHint(getResources().getString(R.string.selected));
                priceLayout.reset();
                meterLayout.setHint(getResources().getString(R.string.selected));
                meterLayout.reset();
                tvFill.setVisibility(View.VISIBLE);
            } else if (notMeter || notPrice) {
                if (selectedMinMeter == null && selectedMaxMeter == null) {
                    meterLayout.setHint(getResources().getString(R.string.optional));
                    meterLayout.reset();
                }

//                if (selectedMinMeter == null){
//                    meterLayout.setHint(getResources().getString(R.string.optional));
//                    meterLayout.reset();
//                }
//
//                if (selectedMaxMeter == null){
//                    meterLayout.setHint(getResources().getString(R.string.optional));
//                    meterLayout.reset();
//                }

                if (selectedMinPrice == null && selectedMaxPrice == null) {
                    priceLayout.setHint(getResources().getString(R.string.optional));
                    priceLayout.reset();
                }

//                if (selectedMinPrice == null){
//                    priceLayout.setHint(getResources().getString(R.string.optional));
//                    priceLayout.reset();
//                }
//
//                if (selectedMaxPrice == null){
//                    priceLayout.setHint(getResources().getString(R.string.optional));
//                    priceLayout.reset();
//                }


                tvFill.setVisibility(View.INVISIBLE);
            } else {
                tvFill.setVisibility(View.INVISIBLE);
            }
        } else {
            if (notMeter && notPrice && notRent) {
                priceLayout.setHint(getResources().getString(R.string.selected));
                priceLayout.reset();
                rentLayout.setHint(getResources().getString(R.string.selected));
                rentLayout.reset();
                meterLayout.setHint(getResources().getString(R.string.selected));
                meterLayout.reset();
                tvFill.setVisibility(View.VISIBLE);
            } else if (notMeter || notPrice || notRent) {
                if (selectedMinMeter == null && selectedMaxMeter == null) {
                    meterLayout.setHint(getResources().getString(R.string.optional));
                    meterLayout.reset();
                }

//                if (selectedMinMeter == null){
//                    meterLayout.setHint(getResources().getString(R.string.optional));
//                    meterLayout.reset();
//                }
//
//                if (selectedMaxMeter == null){
//                    meterLayout.setHint(getResources().getString(R.string.optional));
//                    meterLayout.reset();
//                }

                if (selectedMinPrice == null && selectedMaxPrice == null) {
                    priceLayout.setHint(getResources().getString(R.string.optional));
                    priceLayout.reset();
                }

//                if (selectedMinPrice == null){
//                    priceLayout.setHint(getResources().getString(R.string.optional));
//                    priceLayout.reset();
//                }
//
//                if (selectedMaxPrice == null){
//                    priceLayout.setHint(getResources().getString(R.string.optional));
//                    priceLayout.reset();
//                }

                if (selectedMinRent == null && selectedMaxRent == null) {
                    rentLayout.setHint(getResources().getString(R.string.optional));
                    rentLayout.reset();
                }

//                if (selectedMinRent == null){
//                    rentLayout.setHint(getResources().getString(R.string.optional));
//                    rentLayout.reset();
//                }
//
//                if (selectedMaxRent == null){
//                    rentLayout.setHint(getResources().getString(R.string.optional));
//                    rentLayout.reset();
//                }

                tvFill.setVisibility(View.INVISIBLE);
            } else {
                tvFill.setVisibility(View.INVISIBLE);
            }
        }
    }


    private void areaSelected(ArrayList<Area> areaIdList) {
        areaLayout.setVisibility(View.VISIBLE);
        selectedAreas = areaIdList;
        if (selectedAreas != null) {
            int allAreaCount = TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId());
            if (selectedAreas.size() == allAreaCount) {
                areaLayout.setValue(getString(R.string.all_areas), null);
            } else {
                StringBuilder text = new StringBuilder();
                for (int i = 0; i < selectedAreas.size(); i++) {
                    text.append(selectedAreas.get(i).getName());
                    if (i < selectedAreas.size() - 1) {
                        text.append(",");
                    }
                }

                if (text.length() == 0) {
                    areaLayout.setValue(getString(R.string.all_areas), null);
                } else {
                    areaLayout.setValue(text.toString(), null);
                }
            }

        } else {
            areaLayout.setValue(getString(R.string.all_areas), null);
            if (TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) == 0) {
                areaLayout.setVisibility(View.GONE);
            }
        }
        regionSelected(null);
    }

    private void regionSelected(ArrayList<Region> locationInterfaces) {
        regionLayout.setVisibility(View.GONE);
//        regionLayout.setVisibility(View.VISIBLE);
//        selectedRegions = locationInterfaces;
//        if (selectedRegions != null) {
//            String text = "";
//            for (int i = 0; i < selectedRegions.size(); i++) {
//                text += selectedRegions.get(i).getName();
//                if (i < selectedRegions.size() - 1) {
//                    text += ",";
//                }
//            }
//            if (text.isEmpty()){
//                regionLayout.setValue(getString(R.string.all_region), null);
//            }else {
//                regionLayout.setValue(text, null);
//            }
//
//        } else {
//            if (selectedArea != null) {
//                if (TransactionRegion.getInstance().getRegionsCount(selectedArea.getId()) > 0) {
//                    regionLayout.reset();
//                } else {
//                    regionLayout.setVisibility(View.GONE);
////                    regionLayout.notExist();
//                }
//            } else {
//                if (TransactionArea.getInstance().getAreasCount(Constants.currentCity.getId()) > 0) {
//                    regionLayout.selectParent(getString(R.string.select_parent));
//                    regionLayout.setVisibility(View.GONE);
//                } else {
//                    regionLayout.setVisibility(View.GONE);
////                    regionLayout.notExist();
//                }
//            }
//        }
    }

    private void setContractType(ContractTypeEnum contractType) {
        selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractType.getMethodCode(), Constants.getCity(getActivity()).getLocationFeatureLocalId());
        propertyTypeSelected(null);

    }

    private void propertyTypeSelected(PropertyType propertyType) {
        if (propertyTypeLayout == null) {
            return;
        }
        selectedPropertyType = propertyType;
        if (selectedPropertyType != null) {
            propertyTypeLayout.setValue(propertyType.getTitle(), null);
            if (!propertyType.hasArea()) {
                areaSelected(null);
                areaLayout.setVisibility(View.GONE);
            } else {
                if (TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId()) > 0) {
                    areaLayout.setVisibility(View.VISIBLE);
                } else {
                    areaLayout.setVisibility(View.GONE);
//                areaLayout.notExist();
                }
            }
        } else {
            propertyTypeLayout.reset();
        }
        selectedMinPrice = null;
        selectedMaxPrice = null;
        selectedMinMeter = null;
        selectedMaxMeter = null;
        selectedMinRent = null;
        selectedMaxRent = null;
        selectTotalMinMeter = null;
        selectTotalMaxMeter = null;
        tvFill.setVisibility(View.GONE);
        setSection2();
    }


    @OnClick({R.id.area_layout, R.id.region_layout,
            R.id.price_layout, R.id.rent_layout, R.id.meter_layout,
            R.id.total_meter_layout, R.id.value_layout,
            R.id.btnDone, R.id.propertyType_layout, R.id.rlBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.area_layout:
                ArrayList<Area> areas = TransactionArea.getInstance().getAreasBy(getActivity(), Constants.getCity(getActivity()).getId());
                if (areas.size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(FilterFragment.this, REQUEST_AREA_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_area));
                    bundle.putBoolean("isMultiSelect", true);
                    bundle.putBoolean("isSingleLine", false);
                    bundle.putInt("maxSelectedArea", maxSelectedArea);
                    bundle.putString("extraName", AREAS);
                    bundle.putSerializable("locationType", LocationTypeEnum.AREA);
                    bundle.putParcelableArrayList("list", new ArrayList<>(areas));
                    bundle.putParcelableArrayList("selectedList", selectedAreas == null ? new ArrayList<>() : new ArrayList<>(selectedAreas));
                    locationFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.saearchDetailTag);
                    }
                }
                break;
            case R.id.region_layout:
//                if (selectedArea != null) {
//                    ArrayList<Region> regions = TransactionRegion.getInstance().getRegionsBy(selectedArea.getId());
//                    LocationSelectFragment locationFragment = new LocationSelectFragment();
//                    locationFragment.setTargetFragment(FilterFragment.this, REQUEST_REGION_CODE);
//                    locationFragment.isMultiSelect = true;
//                    locationFragment.locationType = LocationType.REGION;
//                    locationFragment.list = new ArrayList<>(regions);
//                    locationFragment.selectedList = selectedRegions == null ? new ArrayList<>() : new ArrayList<>(selectedRegions);
//                     if (getActivity() != null){
//                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.saearchDetailTag);
//                    }
//                }
                break;
            case R.id.propertyType_layout:
                if (selectedContractType != null && selectedContractType.getPropertyTypes() != null && selectedContractType.getPropertyTypes().size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(FilterFragment.this, REQUEST_PROPERTYTYPE_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_property));
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", PROPERTY);
                    bundle.putSerializable("locationType", LocationTypeEnum.PROPERTYTYPE);
                    bundle.putParcelableArrayList("list", new ArrayList<>(selectedContractType.getPropertyTypes()));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    if (getActivity() != null) {
                        ((BottomBarActivity) getActivity()).loadFragment(locationFragment, BottomBarActivity.saearchDetailTag);
                    }
                }
                break;
            case R.id.price_layout:
                if (selectedPropertyType != null && getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    SearchParamDialog searchParamDialog = new SearchParamDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_price));
                    bundle.putParcelableArrayList("searchParams", selectedPropertyType.getPriceList());
                    bundle.putParcelable("minSearchParam", selectedMinPrice);
                    bundle.putParcelable("maxSearchParam", selectedMaxPrice);
                    if (selectedContractType != null && selectedContractType.getType() == ContractTypeEnum.RENT) {
                        bundle.putString("minTitle", getResources().getString(R.string.min_vadie));
                        bundle.putString("maxTitle", getResources().getString(R.string.max_vadie));
                    } else {
                        bundle.putString("minTitle", getResources().getString(R.string.min_price));
                        bundle.putString("maxTitle", getResources().getString(R.string.max_price));
                    }
                    bundle.putSerializable("searchParamType", SearchParamType.PRICE);
                    searchParamDialog.setArguments(bundle);
                    searchParamDialog.setTargetFragment(FilterFragment.this, REQUEST_SEARCH_PARAM);
                    searchParamDialog.show(fm, SearchParamDialog.class.getName());
                }

                break;
            case R.id.rent_layout:
                if (selectedPropertyType != null && getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    SearchParamDialog searchParamDialog = new SearchParamDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_price));
                    bundle.putParcelableArrayList("searchParams", selectedPropertyType.getRentList());
                    bundle.putParcelable("minSearchParam", selectedMinRent);
                    bundle.putParcelable("maxSearchParam", selectedMaxRent);
                    if (selectedContractType != null && selectedContractType.getType() == ContractTypeEnum.RENT) {
                        bundle.putString("minTitle", getResources().getString(R.string.min_rent));
                        bundle.putString("maxTitle", getResources().getString(R.string.max_rent));
                    } else {
                        bundle.putString("minTitle", getResources().getString(R.string.min_price));
                        bundle.putString("maxTitle", getResources().getString(R.string.max_price));
                    }
                    bundle.putSerializable("searchParamType", SearchParamType.RENT);
                    searchParamDialog.setArguments(bundle);
                    searchParamDialog.setTargetFragment(FilterFragment.this, REQUEST_SEARCH_PARAM);
                    searchParamDialog.show(fm, SearchParamDialog.class.getName());
                }

                break;
            case R.id.meter_layout:
                if (selectedPropertyType != null && getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    SearchParamDialog searchParamDialog = new SearchParamDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_meter));
                    bundle.putParcelableArrayList("searchParams", selectedPropertyType.getMeterList());
                    bundle.putParcelable("minSearchParam", selectedMinMeter);
                    bundle.putParcelable("maxSearchParam", selectedMaxMeter);
                    bundle.putString("minTitle", getResources().getString(R.string.min_meter));
                    bundle.putString("maxTitle", getResources().getString(R.string.max_meter));
                    bundle.putSerializable("searchParamType", SearchParamType.METER);
                    searchParamDialog.setArguments(bundle);
                    searchParamDialog.setTargetFragment(FilterFragment.this, REQUEST_SEARCH_PARAM);
                    searchParamDialog.show(fm, SearchParamDialog.class.getName());
                }
                break;
            case R.id.total_meter_layout:
                if (selectedPropertyType != null && getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    SearchParamDialog searchParamDialog = new SearchParamDialog();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", getString(R.string.selection_meter));
                    bundle.putParcelableArrayList("searchParams", selectedPropertyType.getMeterList());
                    bundle.putParcelable("minSearchParam", selectTotalMinMeter);
                    bundle.putParcelable("maxSearchParam", selectTotalMaxMeter);
                    bundle.putString("minTitle", getResources().getString(R.string.min_meter));
                    bundle.putString("maxTitle", getResources().getString(R.string.max_meter));
                    bundle.putSerializable("searchParamType", SearchParamType.TOTALMETER);
                    searchParamDialog.setArguments(bundle);
                    searchParamDialog.setTargetFragment(FilterFragment.this, REQUEST_SEARCH_PARAM);
                    searchParamDialog.show(fm, SearchParamDialog.class.getName());
                }
                break;
            case R.id.value_layout:
                break;
            case R.id.btnDone:
                if (isValidData()) {
                    saveFilter();
                }
                break;
            case R.id.rlBack:
                onPopBackStack();
                break;
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();
        if (selectedPropertyType == null) {
            String message = getResources().getString(R.string.select_property_type);
            propertyTypeLayout.setError(message);
            errorMsgList.add(message);
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }


        if (selectedContractType.getType() == ContractTypeEnum.PURCHASE) {
            boolean notMeter = selectedPropertyType.getMeterList().size() > 0 && selectedMinMeter == null && selectedMaxMeter == null;
            boolean notPrice = selectedPropertyType.getPriceList().size() > 0 && selectedMinPrice == null && selectedMaxPrice == null;
            if (notMeter && notPrice) {
                String message = getResources().getString(R.string.fill_one_item_from);
                meterLayout.setError(message);
                priceLayout.setError(message);
                errorMsgList.add(message);
            }

        } else {
            boolean notMeter = selectedPropertyType.getMeterList().size() > 0 && selectedMinMeter == null && selectedMaxMeter == null;
            boolean notPrice = selectedPropertyType.getPriceList().size() > 0 && selectedMinPrice == null && selectedMaxPrice == null;
            boolean notRent = selectedPropertyType.getRentList().size() > 0 && selectedMinRent == null && selectedMaxRent == null;
            if (notMeter && notPrice && notRent) {
                String message = getResources().getString(R.string.fill_one_item_from);
                meterLayout.setError(message);
                priceLayout.setError(message);
                rentLayout.setError(message);
                errorMsgList.add(message);
            }
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


    private void saveFilter() {
        if (filter == null) {
            filter = new Filter();
        }
        filter.setCityId(Constants.getCity(getActivity()).getId());
        if (selectedPropertyType.hasArea() && selectedAreas != null && selectedAreas.size() > 0) {
            int allAreaCount = TransactionArea.getInstance().getAreasCount(getActivity(), Constants.getCity(getActivity()).getId());
            if (selectedAreas.size() == allAreaCount) {
                filter.setAreaIdList(new ArrayList<>());
            } else {
                ArrayList<Integer> list = new ArrayList<>();
                for (int i = 0; i < selectedAreas.size(); i++) {
                    list.add(selectedAreas.get(i).getId());
                }
                filter.setAreaIdList(list);
            }
        } else {
            filter.setAreaIdList(new ArrayList<>());
        }
        if (selectedRegions != null) {
            ArrayList<Integer> list = new ArrayList<>();
            for (int i = 0; i < selectedRegions.size(); i++) {
                list.add(selectedRegions.get(i).getId());
            }
            filter.setRegionListFilter(list);
        }
        filter.setContractTypeLocalId(selectedContractType != null ? selectedContractType.getContractTypeLocalId() : 1);
        filter.setPropertyTypeLocalId(selectedPropertyType != null ? selectedPropertyType.getId() : 0);
        filter.setMinMortgageOrTotalId(selectedMinPrice != null ? selectedMinPrice.getValue() : 0);
        filter.setMaxMortgageOrTotalId(selectedMaxPrice != null ? selectedMaxPrice.getValue() : 0);
        filter.setMinRentOrMetricId(selectedMinRent != null ? selectedMinRent.getValue() : 0);
        filter.setMaxRentOrMetricId(selectedMaxRent != null ? selectedMaxRent.getValue() : 0);
        filter.setMinPropertyAreaId(selectedMinMeter != null ? selectedMinMeter.getValue() : 0);
        filter.setMaxPropertyAreaId(selectedMaxMeter != null ? selectedMaxMeter.getValue() : 0);
        filter.setMinPropertyTotalAreaId(selectTotalMinMeter != null ? selectTotalMinMeter.getValue() : 0);
        filter.setMaxPropertyTotalAreaId(selectTotalMaxMeter != null ? selectTotalMaxMeter.getValue() : 0);

        Filter lastFilter = TransactionFilter.getInstance().getSameFilter(getActivity(), filter);
        if (lastFilter != null) {
            TransactionFilter.getInstance().updateFilter(getActivity(), filter, lastFilter.getFilterId());
        } else {
            TransactionFilter.getInstance().saveFilter(getActivity(), filter);
        }
        if (getActivity() != null) {
            ((BottomBarActivity) getActivity()).filterToSearchTab(this, false);
        }
    }


    private void chanageContractType(boolean isRent) {
        if (isRentSlected != isRent) {
            isRentSlected = isRent;
            if (isRentSlected) {
                cardFirst.changeSection(1);
                setContractType(ContractTypeEnum.RENT);
            } else {
                cardFirst.changeSection(0);
                setContractType(ContractTypeEnum.PURCHASE);
            }
        }
    }

    private void setSearchParam(SearchParam minSearchParam, SearchParam
            maxSearchParam, SearchParamType searchParamType) {
        switch (searchParamType) {
            case PRICE:
                selectedMinPrice = minSearchParam;
                selectedMaxPrice = maxSearchParam;
                rentLayout.setError(null);
                break;
            case RENT:
                selectedMinRent = minSearchParam;
                selectedMaxRent = maxSearchParam;
                priceLayout.setError(null);
                break;
            case METER:
                selectedMinMeter = minSearchParam;
                selectedMaxMeter = maxSearchParam;
                break;
            case TOTALMETER:
                selectTotalMinMeter = minSearchParam;
                selectTotalMaxMeter = maxSearchParam;
                break;
        }
        setSection2();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_AREA_CODE && data != null) {
                ArrayList<Area> areaIdList = new ArrayList<>();
//                areaIdList.add(data.getParcelableArrayListExtra(AREAS));
                areaSelected(data.getParcelableArrayListExtra(AREAS));
            } else if (requestCode == REQUEST_REGION_CODE) {
                regionSelected(data.getParcelableArrayListExtra(REGINS));
            } else if (requestCode == REQUEST_PROPERTYTYPE_CODE) {
                propertyTypeSelected(data.getParcelableExtra(PROPERTY));
            } else if (requestCode == REQUEST_SEARCH_PARAM) {
                setSearchParam(data.getParcelableExtra("minSearchParam"), data.getParcelableExtra("maxSearchParam"),
                        (SearchParamType) data.getSerializableExtra("searchParamType"));
            }
        }
    }

    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            if (filter == null) {
                getActivity().finish();
            } else {
                FragmentManager frg = getActivity().getSupportFragmentManager();
                Fragment searchEstateFragment = frg.findFragmentByTag(SearchEstateFragment.class.getName());
                if (searchEstateFragment == null) {
                    getActivity().finish();
                } else {
                    ((BottomBarActivity) getActivity()).filterToSearchTab(this, true);
                }
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isHiddenNavigation() {
        return false;
    }


    @Override
    public void onItemClickGiude() {
        Toast.makeText(getContext(), "ok", Toast.LENGTH_SHORT).show();
    }
}

