package ir.delta.delta.bottomNavigation.moreItems;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.ContactDelta;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.ads.AdsItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {


    private final int cellWidth;
    private int coulumCount;
    private final ArrayList<ContactDelta> list;
    private final OnItemClickListener listener;
    private final Context context;

    public ContactAdapter(Context context, int cellWidth, ArrayList<ContactDelta> list, OnItemClickListener listener) {
        this.cellWidth = cellWidth;
        this.list = list;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_delta, parent, false);
        return new ViewHolder(itemView, cellWidth);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Resources res = holder.itemView.getContext().getResources();
        ContactDelta object = list.get(position);
        Glide.with(context).load(object.getImage()).into(holder.imgAdd);
        holder.title.setText(object.getTitle());
        holder.bind(object, position, listener);

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.title)
        BaseTextView title;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;

        ViewHolder(View view, int cellWidth) {
            super(view);
            ButterKnife.bind(this, view);
            LinearLayout.LayoutParams layoutParams =
                    new LinearLayout.LayoutParams(cellWidth, cellWidth * 3 / 4);
            layoutParams.gravity = Gravity.CENTER;
            layoutParams.setMargins(0, 0 , view.getResources().getDimensionPixelOffset(R.dimen.margin_1), 0);
            itemView.setLayoutParams(layoutParams);
        }

        public void bind(ContactDelta addItem, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(addItem, position));
        }


    }


    public interface OnItemClickListener {
        void onItemClick(ContactDelta adsItem, int position);

    }


}
