package ir.delta.delta.bottomNavigation.moreItems;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.Model.Filter;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseLinearLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.enums.ContractTypeEnum;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class RecentVisitAdapter extends RecyclerView.Adapter<RecentVisitAdapter.ViewHolder> {


    private final Resources res;
    private final ArrayList<Filter> list;
    private final RecentVisitAdapter.OnItemClickListener listener;

    RecentVisitAdapter(Resources res, ArrayList<Filter> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        this.res = res;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recent_visit, parent, false);
        return new ViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Filter filter = list.get(position);


        String priceTitleText;
        String text;
        if (filter.getContractTypeEnum() == ContractTypeEnum.RENT) {
            priceTitleText = res.getString(R.string.mortgage_text) + "(" + res.getString(R.string.toman) + "): ";
            holder.imgContaract.setImageResource(R.drawable.ic_circle_filled_primary);
            text = "  " + res.getString(R.string.rent_text) + " " + (filter.getPropertyType() != null ? filter.getPropertyType().getPropertyTypeName() : "") + " در ";
        } else {
            priceTitleText = res.getString(R.string.price_text) + "(" + res.getString(R.string.toman) + "): ";
            holder.imgContaract.setImageResource(R.drawable.ic_circle_filled_green);
            text = "  " + res.getString(R.string.buy_text) + " " + (filter.getPropertyType() != null ? filter.getPropertyType().getPropertyTypeName() : "") + " در ";
        }

        if (filter.getAreaNames() != null) {
            text += filter.getAreaNames();
        } else {
            if (TransactionArea.getInstance().getAreasCount(holder.tvContractType.getContext(), filter.getCityId()) > 0) {
                text += res.getString(R.string.all_areas);
            } else {
                text += "";
            }
        }
        holder.tvContractType.setText(text);
        holder.tvContractType.setTextColor(res.getColor(R.color.primaryTextColor));

        if (filter.getPropertyType() != null && filter.getPropertyType().getSearchParamInfos() != null) {
            Spanned priceText = filter.getPropertyType().getSearchParamInfos().getPriceText(res, priceTitleText, filter.getMinMortgageOrTotalId(), filter.getMaxMortgageOrTotalId());
            if (priceText != null) {
                holder.tvPrice.setText(priceText);
                holder.tvPrice.setVisibility(View.VISIBLE);
            } else {
                holder.tvPrice.setVisibility(View.GONE);
            }

            String rentTitle = res.getString(R.string.rent_text) + "(" + res.getString(R.string.toman) + "): ";
            Spanned rentText = filter.getPropertyType().getSearchParamInfos().getRentText(res, rentTitle, filter.getMinRentOrMetricId(), filter.getMaxRentOrMetricId());
            if (rentText != null) {
                holder.tvRent.setText(rentText);
                holder.tvRent.setVisibility(View.VISIBLE);
            } else {
                holder.tvRent.setVisibility(View.GONE);
            }

            String meterTitle;
            if (filter.getPropertyType().getSearchParamInfos().isShowTotalAreaFilter()) {
                meterTitle = res.getString(R.string.building_area) + ": ";
                String meterTotalTitle = res.getString(R.string.land_area) + ": ";
                Spanned meterTotalString = filter.getPropertyType().getSearchParamInfos().getMeterText(res, meterTotalTitle, filter.getMinPropertyTotalAreaId(), filter.getMaxPropertyTotalAreaId());
                if (meterTotalString != null) {
                    holder.tvTotalMeter.setText(meterTotalString);
                    holder.tvTotalMeter.setVisibility(View.VISIBLE);
                } else {
                    holder.tvTotalMeter.setVisibility(View.GONE);
                }

            } else {
                holder.tvTotalMeter.setVisibility(View.GONE);
                meterTitle = res.getString(R.string.meter_text) + ": ";
            }

            Spanned meterString = filter.getPropertyType().getSearchParamInfos().getMeterText(res, meterTitle, filter.getMinPropertyAreaId(), filter.getMaxPropertyAreaId());
            if (meterString != null) {
                holder.tvMeter.setText(meterString);
                holder.tvMeter.setVisibility(View.VISIBLE);
            } else {
                holder.tvMeter.setVisibility(View.GONE);
            }
        } else {
            holder.tvPrice.setVisibility(View.GONE);
            holder.tvRent.setVisibility(View.GONE);
            holder.tvMeter.setVisibility(View.GONE);
            holder.tvTotalMeter.setVisibility(View.GONE);
        }
        holder.bind(filter, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgContaract)
        BaseImageView imgContaract;
        @BindView(R.id.tvContractType)
        BaseTextView tvContractType;
        @BindView(R.id.tvPrice)
        BaseTextView tvPrice;
        @BindView(R.id.tvRent)
        BaseTextView tvRent;
        @BindView(R.id.tvMeter)
        BaseTextView tvMeter;
        @BindView(R.id.tvTotalMeter)
        BaseTextView tvTotalMeter;
        @BindView(R.id.row)
        BaseLinearLayout row;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(Filter filter, final OnItemClickListener listener) {
            itemView.setOnClickListener(view -> listener.onItemClick(filter));
        }

    }

    public interface OnItemClickListener {
        void onItemClick(Filter position);
    }
}
