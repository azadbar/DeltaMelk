package ir.delta.delta.bottomNavigation.moreItems;

import android.content.Context;
import android.content.pm.PackageManager;

public class ContactDeltaPresenter {

    private final MakePage makePage;

    ContactDeltaPresenter(MakePage makePage) {
        this.makePage = makePage;
    }

    public void initView() {
        makePage.initView();
    }


    public boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public interface MakePage {
        void initView();
    }
}
