package ir.delta.delta.bottomNavigation.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.LocationTypeEnum;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class MultiSelectAdapter extends RecyclerView.Adapter<MultiSelectAdapter.ViewHolder> {


    private final LocationTypeEnum locationType;
    private final ArrayList<? extends LocationInterface> list;
    private final ArrayList<? extends LocationInterface> selectedList;
    private final OnItemClickListener listener;


    MultiSelectAdapter(ArrayList<? extends LocationInterface> list, ArrayList<? extends LocationInterface> selectedList, LocationTypeEnum locationType, OnItemClickListener listener) {
        this.list = list;
        this.selectedList = selectedList;
        this.listener = listener;
        this.locationType = locationType;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_region_property, parent, false);

        return new ViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        LocationInterface object = list.get(position);

        if (locationType == LocationTypeEnum.REGION) {
            holder.tvRegions.setVisibility(View.VISIBLE);
            holder.tvRegions.setText(object.getTitle());
        } else {
            holder.tvRegions.setVisibility(View.GONE);
            holder.tvTitle.setText(object.getTitle());
            holder.tvDescription.setText(object.getDescription());
        }

        if (isSelected(object) >= 0) {
            holder.imgSelected.setImageResource(R.drawable.ic_check_circle_green);
            holder.imgSelected.setColorFilter(holder.imgSelected.getContext().getResources().getColor(R.color.redColor));
        } else {
            holder.imgSelected.setImageResource(R.drawable.ic_circle);
            holder.imgSelected.setColorFilter(holder.imgSelected.getContext().getResources().getColor(R.color.secondaryTextColor));
        }

        if (object.getTitle().contains("تهران") && locationType == LocationTypeEnum.AREA) {
            holder.btnAboutArea.setVisibility(View.VISIBLE);
        } else {
            holder.btnAboutArea.setVisibility(View.GONE);
        }
        holder.bind(position, object, listener);
    }

    public int isSelected(LocationInterface locationInterface) {
        if (selectedList == null) {
            return -1;
        }
        for (int i = 0; i < selectedList.size(); i++) {
            if (selectedList.get(i).getId() == locationInterface.getId()) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.imgSelected)
        BaseImageView imgSelected;
        @BindView(R.id.btnAboutArea)
        BaseTextView btnAboutArea;
        @BindView(R.id.tvRegions)
        BaseTextView tvRegions;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, LocationInterface area, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickMultiSelected(position));
            btnAboutArea.setOnClickListener(view -> listener.onItemAreaInfo(position, area));
        }

    }


    public interface OnItemClickListener {
        void onItemClickMultiSelected(int position);

        void onItemAreaInfo(int position, LocationInterface area);
    }
}
