package ir.delta.delta.bottomNavigation.moreItems;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.util.Constants;

import static android.view.View.VISIBLE;

/**
 * Created by a.azadbar on 9/27/2017.
 */

public class WebSiteDeltaActivity extends BaseActivity {


    @BindView(R.id.progreesBar)
    ProgressBar progreesBar;
    @BindView(R.id.webview)
    WebView webview;
    private String url;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mag_delta);
        ButterKnife.bind(this);
        Constants.setBackgroundProgress(this, progreesBar);
        webview.getSettings().setJavaScriptEnabled(true);
        waitingStart();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString("url");
        }
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //Make the bar disappear after URL is loaded, and changes string to Loading...
                // Return the app name after finish loading
                if (progress == 100)
                    waitingStop();
            }
        });
        webview.loadUrl(url);

    }

    public void waitingStop() {
        progreesBar.setVisibility(View.GONE);
        webview.setVisibility(VISIBLE);

    }

    private void waitingStart() {
        webview.setVisibility(View.GONE);
        progreesBar.setVisibility(VISIBLE);
    }
}
