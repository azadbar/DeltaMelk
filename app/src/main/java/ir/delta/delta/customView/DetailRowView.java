package ir.delta.delta.customView;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

public class DetailRowView extends LinearLayout {

    @BindView(R.id.tvTxtTitle)
    BaseTextView tvTxtTitle;
    @BindView(R.id.tvValue)
    BaseTextView tvValue;

    public DetailRowView(Context context) {
        this(context, null);
    }

    public DetailRowView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_row_detail_estate, this, true);
        tvTxtTitle = findViewById(R.id.tvTxtTitle);
        tvValue = findViewById(R.id.tvValue);
//        tvValue.setTextColor(getResources().getColor(R.color.primaryTextColor));
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setTextTitle(String title, String value) {
        tvTxtTitle.setText(title);
        tvValue.setText(value);
    }
}
