package ir.delta.delta.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.util.Constants;

public class RoundedLoadingView extends RelativeLayout {

    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.root)
    BaseRelativeLayout root;

    public RoundedLoadingView(Context context) {
        this(context, null);
    }

    public RoundedLoadingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundedLoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_rounded_loading_view, this, true);
        progress = findViewById(R.id.progress);
        root = findViewById(R.id.root);
        Constants.setBackgroundProgress(getContext(), progress);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }


    public void showLoading() {
        root.setVisibility(VISIBLE);
        progress.setVisibility(VISIBLE);
    }

    public void stopLoading() {
        root.setVisibility(GONE);
        progress.setVisibility(GONE);
    }

    public void showError(String error) {
        progress.setVisibility(GONE);
    }
}
