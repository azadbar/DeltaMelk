package ir.delta.delta.customView;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.search.LocationInterface;
import ir.delta.delta.tablayout.FacilityAdapter;
import ir.delta.delta.service.ResponseModel.ContentInfo;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.EqualSpacingItemDecoration;

public class CustomFacilityAreaInfo extends LinearLayout {


    @BindView(R.id.tvHeader)
    BaseTextView tvHeader;
    @BindView(R.id.image)
    BaseImageView image;
    @BindView(R.id.btnHeaders)
    BaseRelativeLayout btnHeaders;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.rvFacility)
    RecyclerView rvFacility;
    @BindView(R.id.cvFacility)
    CardView cvFacility;
    private LocationInterface area;
    private boolean isShow = true;

    public CustomFacilityAreaInfo(Context context) {
        this(context, null);
    }

    public CustomFacilityAreaInfo(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomFacilityAreaInfo(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }


    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_facility_area_info, this, true);
        tvHeader = findViewById(R.id.tvHeader);
        image = findViewById(R.id.image);
        btnHeaders = findViewById(R.id.btnHeaders);
        view = findViewById(R.id.view);
        rvFacility = findViewById(R.id.rvFacility);
        cvFacility = findViewById(R.id.cvFacility);
        btnHeaders.setOnClickListener(view -> {
            if (isShow) {
                rvFacility.setVisibility(View.GONE);
                view.setVisibility(View.VISIBLE);
                image.setImageResource(R.drawable.ic_keyboard_arrow_down_whit);
                image.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
            } else {
                rvFacility.setVisibility(View.VISIBLE);
                view.setVisibility(View.VISIBLE);
                image.setImageResource(R.drawable.ic_keyboard_arrow_up);
                image.setColorFilter(getResources().getColor(R.color.primaryBack), PorterDuff.Mode.SRC_ATOP);
            }
            isShow = !isShow;
        });
//        tvValue.setTextColor(getResources().getColor(R.color.primaryTextColor));
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setTextTitle(String title) {
        tvHeader.setText(title);
    }

    public void setListFacilities(ArrayList<ContentInfo> contentInfo) {
        int culomnCount = getResources().getInteger(R.integer.coulem_count_recycle_view_of_facility_agency);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), culomnCount, GridLayoutManager.VERTICAL, false);
        rvFacility.setLayoutManager(layoutManager);
        rvFacility.addItemDecoration(new EqualSpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_1dp), EqualSpacingItemDecoration.GRID));
        FacilityAdapter adapter = new FacilityAdapter(contentInfo);
        rvFacility.setAdapter(adapter);
    }


    public void setBtnHeaders(View.OnClickListener clickListener) {
        this.btnHeaders.setOnClickListener(clickListener);
    }


}
