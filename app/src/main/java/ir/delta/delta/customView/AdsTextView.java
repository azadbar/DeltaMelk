package ir.delta.delta.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.FontTypeEnum;
import ir.delta.delta.util.Constants;

public class AdsTextView extends LinearLayout {


    @BindView(R.id.text)
    BaseTextView text;


    public AdsTextView(Context context) {
        this(context, null);
    }

    public AdsTextView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AdsTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.item_text_ads, this, true);
        text = findViewById(R.id.text);
//        tvValue.setTextColor(getResources().getColor(R.color.primaryTextColor));
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setTextTitle(String txt) {
        text.setText(txt);
//        tvValue.setText(value);
    }

    private SpannableStringBuilder setTextSpannable(String title, long value) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(title + ": ");
        int start = desc_two.length();
        desc_two.append(Constants.convertNumberToDecimal(value));
        int end = desc_two.length();
//        desc_two.append(" "+getResources().getString(R.string.toman));
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);
        return desc_two;
    }

    public void setStyleText(int color, FontTypeEnum fontTypeEnum) {
        text.setTextColor(color);
        text.setFontType(fontTypeEnum);
    }
}
