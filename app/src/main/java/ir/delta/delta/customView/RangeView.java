package ir.delta.delta.customView;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.Nullable;

import android.graphics.Typeface;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.RangeEnum;
import ir.delta.delta.service.ResponseModel.SearchParam;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;
import uk.co.samuelwall.materialtaptargetprompt.extras.backgrounds.FullscreenPromptBackground;
import uk.co.samuelwall.materialtaptargetprompt.extras.focals.RectanglePromptFocal;

public class RangeView extends LinearLayout {

    private String title;
    private BaseTextView tvValue;
    private BaseImageView imgInfo, icon;
    private String hint;
    private BaseImageView clear;
    private Activity activity;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setButtonClickListener(OnClickListener onClickListener) {
        clear.setOnClickListener(onClickListener);
    }

    public RangeView(Context context) {
        this(context, null);
    }

    public RangeView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RangeView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView(context, attrs);
    }

    private void initView(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_param_range_view, this, true);
        tvValue = findViewById(R.id.tvValue);
        imgInfo = findViewById(R.id.imgInfo);
        icon = findViewById(R.id.icon);
        clear = findViewById(R.id.clear);
//        imgInfo.setImageResource(INVISIBLE);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setImgInfo(int icon) {
        this.imgInfo.setImageResource(icon);
    }

    public void setError(String error) {
        if (error != null) {
            icon.setImageResource(R.drawable.ic_asterisk);
            icon.setVisibility(VISIBLE);
        } else {
            icon.setVisibility(INVISIBLE);
        }
    }

    public void setTxtTitle(String title) {
        this.title = title;
    }

    public void setRange(SearchParam from, SearchParam to, RangeEnum fromIndex, RangeEnum toIndex) {
        setError(null);
        if (from == null && to == null) {
            reset();
        } else if (from != null && to != null) {
            String htmlRange = "<font color='#333333'>" + title + "</font>:\t"
                    + "<font color='#757575'>" + getResources().getString(R.string.from) + "</font> "
                    + "<font color='#000000'><strong> " + from.getText() + "</strong></font> "
                    + "<font color='#757575'>" + getResources().getString(R.string.to) + "</font> "
                    + "<font color='#000000'><strong>" + to.getText() + "</strong></font> ";
            createHtmlText(htmlRange);
            clear.setVisibility(VISIBLE);
            if (PreferencesData.isClear(getContext(), "isClear")) {
                new Handler().postDelayed(() -> guideView(), 200);
                PreferencesData.setClear(getContext(), "isClear", false);
            }
        } else if (from != null) {
            if (fromIndex == RangeEnum.FIRST) {
                setFirstRang(from);
            } else if (fromIndex == RangeEnum.END) {
                setEndRang(from);
            } else {
                String htmlRange = "<font color='#333333'>" + title + ":</font>\t "
                        + "<font color='#757575'>" + getResources().getString(R.string.from) + "</font> "
                        + "<font color='#000000'><strong>" + from.getText() + "</strong></font> "
                        + "<font color='#757575'>" + getResources().getString(R.string.to_top) + "</font> ";
                createHtmlText(htmlRange);
            }
            clear.setVisibility(VISIBLE);
            if (PreferencesData.isClear(getContext(), "isClear")) {
                new Handler().postDelayed(() -> guideView(), 200);
                PreferencesData.setClear(getContext(), "isClear", false);
            }
        } else {
            if (toIndex == RangeEnum.FIRST) {
                setFirstRang(to);
            } else if (toIndex == RangeEnum.END) {
                setEndRang(to);
            } else {
                String htmlRange = "<font color='#333333'>" + title + ":</font>\t "
                        + "<font color='#757575'>" + getResources().getString(R.string.to) + "</font> "
                        + "<font color='#000000'><strong>" + to.getText() + "</strong></font> ";
                createHtmlText(htmlRange);
            }
            clear.setVisibility(VISIBLE);
            if (PreferencesData.isClear(getContext(), "isClear")) {
                new Handler().postDelayed(() -> guideView(), 200);
                PreferencesData.setClear(getContext(), "isClear", false);
            }
        }

    }

    public void guideView() {
        new MaterialTapTargetPrompt.Builder(activity)
                .setTarget(clear)
                .setPrimaryText(getResources().getString(R.string.guide_title_clear_filter))
                .setPrimaryTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANYekanMobileBold(FaNum).ttf"))
                .setSecondaryText(getTextDescription(activity, getResources().getString(R.string.giud_desc_clear_filter)))
                .setSecondaryTextSize(R.dimen.text_size_2)
                .setSecondaryTextTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANYekanRegularMobile(FaNum).ttf"))
                .setSecondaryTextGravity(Gravity.LEFT)
                .setSecondaryTextGravity(Gravity.TOP)
                .setPromptBackground(new FullscreenPromptBackground())
                .setPromptFocal(new RectanglePromptFocal())
                .setBackgroundColour(getResources().getColor(R.color.transparentGuide))
                .setPromptStateChangeListener((prompt, state) -> {
                    if (state == MaterialTapTargetPrompt.STATE_NON_FOCAL_PRESSED) {

                    }
                })
                .show();

    }

    private void setEndRang(SearchParam from) {
        String htmlRange = "<font color='#333333'>" + title + ":</font>\t "
                + "<font color='#000000'><strong>" + from.getText() + "</strong></font> ";
        createHtmlText(htmlRange);
    }


    public void setFirstRang(SearchParam from) {
        String htmlRange = "<font color='#333333'>" + title + ":</font>\t "
                + "<font color='#000000'>" + from.getText() + "</font> ";
        createHtmlText(htmlRange);
    }

    public void reset() {
        clear.setVisibility(GONE);
        String htmlText = "<font color='#333333'>" + title + ":</font>\t "
                + "<font color='#757575'>" + hint + "</font>";
        createHtmlText(htmlText);
        setError(null);
    }

    private void createHtmlText(String text) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            tvValue.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvValue.setText(Html.fromHtml(text));
        }
    }

    public void setHint(String hint) {
        if (!hint.isEmpty()) {
            this.hint = hint;
        } else {
            this.hint = getResources().getString(R.string.your_region);
        }
    }

    protected SpannableStringBuilder getTextDescription(Context context, String desc) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(desc + "\n\n");
        int start = desc_two.length();
        desc_two.append("متوجه شدم");
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new BaseFragment.RoundedBackgroundSpan(context), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return desc_two;
    }


}
