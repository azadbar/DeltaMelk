package ir.delta.delta.customView;

import android.content.Intent;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Toast;

import ir.delta.delta.mag.MagPostContentWebViewActivity;

public class CustomClick extends ClickableSpan {

    private final String link;

    public CustomClick(String link) {
        this.link = link;
    }

    @Override
    public void onClick(View widget) {
            if (!TextUtils.isEmpty(link)) {
                Intent intent = new Intent(widget.getContext(), MagPostContentWebViewActivity.class);
                intent.putExtra("link", link);
                widget.getContext().startActivity(intent);
        }
    }
}
