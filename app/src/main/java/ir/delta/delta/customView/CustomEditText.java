package ir.delta.delta.customView;

import android.content.Context;
import android.content.res.TypedArray;

import androidx.annotation.Nullable;

import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.listener.OnEditTextChangeListener;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.InputTypeEnum;
import ir.delta.delta.util.Constants;

public class CustomEditText extends LinearLayout implements OnEditTextChangeListener {
    private BaseTextView tvTitle;
    private BaseEditText edtBody;
    private BaseImageView image;
    private BaseRelativeLayout root;
    private String error;
    private InputTypeEnum inputTypeEnum;
    private boolean isVisibile;
    private boolean isShowTitle;


    public CustomEditText(Context context) {
        this(context, null);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_edit_text, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        edtBody = findViewById(R.id.edtBody);
        image = findViewById(R.id.image);
        root = findViewById(R.id.root);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
            String titleText = a.getString(R.styleable.CustomEditText_tvTitle);
//            String bodyValue = a.getString(R.styleable.CustomEditText_edtBody);
            String hint = a.getString(R.styleable.CustomEditText_hint);
            Boolean isRequired = a.getBoolean(R.styleable.CustomEditText_isRequired, false);
            isVisibile = a.getBoolean(R.styleable.CustomEditText_isVisibility, false);
            isShowTitle = a.getBoolean(R.styleable.CustomEditText_isShowTitle, true);
            if (a.hasValue(R.styleable.CustomEditText_inputType)) {
                int value = a.getInt(R.styleable.CustomEditText_inputType, 0);
                if (value >= 0 && value < InputTypeEnum.values().length) {
                    setInputTypeEnum(InputTypeEnum.values()[value]);
                }
            }
            a.recycle();
            setTextsTitle(titleText);
//            setTextValue(bodyValue);
            setTextHint(hint);
            setIsRequired(isRequired);
            setIsShowTitle(isShowTitle);
//            setImageVisibility(isVisibile);
        }
        edtBody.onEditTextChangeListener = this;

    }

    private void setIsShowTitle(boolean isShowTitle) {
        if (isShowTitle){
            tvTitle.setVisibility(VISIBLE);
        }else {
            tvTitle.setVisibility(GONE);
            edtBody.setGravity(Gravity.CENTER);
            image.setVisibility(GONE);
            isVisibile = true;
        }
    }

//    private void setImageVisibility(boolean isVisibile) {
//        if (!isVisibile) {
//            image.setVisibility(GONE);
//        } else {
//            image.setVisibility(VISIBLE);
//        }
//    }

    public void setUnit(String unit) {
        this.edtBody.unit = unit;
    }

    public void setTextHint(String hint) {
        edtBody.setHint(hint);
    }

    public void setInputTypeEnum(InputTypeEnum inputTypeEnum) {
        this.inputTypeEnum = inputTypeEnum;
        this.edtBody.setInputTypeEnum(inputTypeEnum);

    }

    public InputTypeEnum getInputTypeEnum() {
        return inputTypeEnum;
    }

    public void setIsRequired(Boolean isRequired) {
        if (isRequired) {
            image.setVisibility(VISIBLE);
            error = getResources().getString(R.string.enter_title, tvTitle.getText().toString().trim());
            image.setImageResource(R.drawable.ic_asterisk);
        } else {
            image.setVisibility(GONE);
            error = null;
        }
        this.edtBody.setIsRequired(isRequired);
    }

    public void setTextsTitle(String title) {
        if (title != null && !title.trim().isEmpty()) {
            title = title.trim();
            tvTitle.setVisibility(VISIBLE);
            tvTitle.setText(title);
            edtBody.title = title;
        } else {
            tvTitle.setVisibility(GONE);
        }
    }

//    public void setTextValue(String value) {
//        edtBody.setText(value);
//    }
//
//    public String getTextVal() {
//        if (edtBody.getText() == null) {
//            return "";
//        }
//        return edtBody.getText().toString().trim();
//    }

    @Override
    public void onGetError(String error) {
        setError(error);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
        if (error != null) {
            image.setVisibility(VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_asterisk));
        } else if (!"".equals(edtBody.getTrimedText()) && inputTypeEnum != InputTypeEnum.MORTGAGE_PRICE && inputTypeEnum != InputTypeEnum.RENT_PRICE) {
            image.setVisibility(VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
        } else {
            image.setVisibility(GONE);
        }

        //for Code of verification code
        if (isVisibile){
            image.setVisibility(GONE);
            tvTitle.setVisibility(GONE);
            edtBody.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
        }else {
            image.setVisibility(VISIBLE);
            tvTitle.setVisibility(VISIBLE);
        }
    }

    public String getValueString() {
        return edtBody.getValueString();
    }

    public int getValueInt() {
        return edtBody.getValueInt();
    }

    public long getValueLong() {
        return edtBody.getValueLong();
    }

    public void setTextBody(String text) {
        if (text != null) {
            edtBody.setText(text);
            if (this.edtBody.getText() != null)
                this.edtBody.setSelection(this.edtBody.getText().length());
        }
    }

    public void disableView() {
        edtBody.setEnabled(false);
        edtBody.setClickable(false);
        edtBody.setFocusable(false);
        tvTitle.setClickable(false);
        tvTitle.setClickable(false);
        tvTitle.setFocusable(false);
        root.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    public BaseEditText getEdtBody() {
        return edtBody;
    }

}
