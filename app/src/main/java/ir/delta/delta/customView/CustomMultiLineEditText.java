package ir.delta.delta.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;

import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseEditText;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.InputTypeEnum;
import ir.delta.delta.listener.OnEditTextChangeListener;
import ir.delta.delta.util.Constants;

public class CustomMultiLineEditText extends LinearLayout implements OnEditTextChangeListener {
    private BaseTextView tvTitle;
    private BaseEditText edtBody;
    private BaseImageView image;
    private String error;
    private InputTypeEnum inputTypeEnum;


    public CustomMultiLineEditText(Context context) {
        this(context, null);
    }

    public CustomMultiLineEditText(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CustomMultiLineEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_multiline_edit_text, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        edtBody = findViewById(R.id.edtBody);
        image = findViewById(R.id.image);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomMultiLineEditText, 0, 0);
            String titleText = a.getString(R.styleable.CustomMultiLineEditText_tvTitleMulti);
            String bodyValue = a.getString(R.styleable.CustomMultiLineEditText_edtBodyMulti);
            String hint = a.getString(R.styleable.CustomMultiLineEditText_hintMulti);
            Boolean isRequired = a.getBoolean(R.styleable.CustomMultiLineEditText_isRequiredMulti, false);
            if (a.hasValue(R.styleable.CustomMultiLineEditText_inputTypeMulti)) {
                int value = a.getInt(R.styleable.CustomMultiLineEditText_inputTypeMulti, 0);

                if (value >= 0 && value < InputTypeEnum.values().length) {
                    setInputTypeEnum(InputTypeEnum.values()[value]);
                }
            }
            a.recycle();
            setTextsTitle(titleText);
            setTextValue(bodyValue);
            setTextHint(hint);
            setIsRequired(isRequired);
            edtBody.setCursorVisible(true);
        }

        //for screoll in srcrolView
        edtBody.setOnTouchListener((v, event) -> {
            if (edtBody.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_SCROLL:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }
            }
            return false;
        });

        edtBody.onEditTextChangeListener = this;

    }

    public void setTextHint(String hint) {
        edtBody.setHint(hint);
    }

    public void setInputTypeEnum(InputTypeEnum inputTypeEnum) {
        this.edtBody.setInputTypeEnum(inputTypeEnum);
        this.inputTypeEnum = inputTypeEnum;
    }

    public InputTypeEnum getInputTypeEnum() {
        return inputTypeEnum;
    }

    public void setIsRequired(Boolean isRequired) {
        if (isRequired) {
            image.setVisibility(VISIBLE);
            error = getResources().getString(R.string.enter_title, tvTitle.getText().toString().trim());
            image.setImageResource(R.drawable.ic_asterisk);
        }
        this.edtBody.setIsRequired(isRequired);
    }

    public void setTextsTitle(String title) {
        if (title != null && !title.trim().isEmpty()) {
            tvTitle.setVisibility(VISIBLE);
            tvTitle.setText(title);
            edtBody.title = title;
        } else {
            tvTitle.setVisibility(GONE);

        }
    }

    public void setTextValue(String value) {
        edtBody.setText(value);
    }

    public String getTextVal() {
        if (edtBody.getText() == null) {
            return "";
        }
        return edtBody.getText().toString().trim();
    }

    @Override
    public void onGetError(String error) {
        this.error = error;
        if (error != null) {
            image.setVisibility(VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_asterisk));
        } else if (!"".equals(edtBody.getTrimedText())) {
            image.setVisibility(VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_check));
        } else {
            image.setVisibility(GONE);
        }
    }

    public String getError() {
        return error;
    }

    public String getValue() {
        return edtBody.getValueString();
    }

}
