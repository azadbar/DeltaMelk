package ir.delta.delta.customView;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Toast;

import ir.delta.delta.mag.MagPostContentWebViewActivity;

public class CustomClickCall extends ClickableSpan {

    private final String link;

    public CustomClickCall(String link) {
        this.link = link;
    }

    @Override
    public void onClick(View widget) {
        if (!TextUtils.isEmpty(link)) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + link));
            widget.getContext().startActivity(intent);
        }
    }
}
