package ir.delta.delta.customView;


import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import ir.delta.delta.baseView.BaseImageView;

public class AspectRatioImageView extends BaseImageView {

    public AspectRatioImageView(Context context) {
        super(context);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (getDrawable() == null)
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = (int)(displayMetrics.heightPixels - 24 * getContext().getResources().getDisplayMetrics().density);
            int width = displayMetrics.widthPixels;
            setMeasuredDimension(width, height);
        }
    }
}