package ir.delta.delta.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import butterknife.BindView;
import ir.delta.delta.BuildConfig;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

public class LoadingView extends RelativeLayout {

    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.magImage)
    BaseImageView magImage;


    public void setButtonClickListener(OnClickListener onClickListener) {
        btnOk.setOnClickListener(onClickListener);
    }

    public LoadingView(Context context) {
        this(context, null);
    }

    public LoadingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_loading_view, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        progress = findViewById(R.id.progress);
        btnOk = findViewById(R.id.btnOk);
        root = findViewById(R.id.root);
        magImage = findViewById(R.id.magImage);
        Constants.setBackgroundProgress(getContext(), progress);
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }


    public void showLoading(boolean isMag) {
        this.setVisibility(View.VISIBLE);
        btnOk.setVisibility(GONE);
        root.setVisibility(VISIBLE);
        tvTitle.setVisibility(VISIBLE);
        progress.setVisibility(VISIBLE);
        if (isMag) {
            magImage.setVisibility(VISIBLE);
            SpannableStringBuilder desc_two = new SpannableStringBuilder();
            int start = desc_two.length();
            desc_two.append("همه چیز در مورد همه چیز\n");
            int end = desc_two.length();
            /*desc_two.append("سلامتی، آشپزی، حقوقی، دکوراسیون\n");
            desc_two.append("مد و زیبایی، گردشگری و نیازمندی ها");*/
            desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            desc_two.setSpan(new RelativeSizeSpan(1.4f), start, end, 0);
            tvTitle.setText(desc_two);
        } else {
            magImage.setVisibility(GONE);
            tvTitle.setText(getResources().getString(R.string.receiving_information));
        }


    }

    public void stopLoading() {
        this.setVisibility(GONE);
        progress.setVisibility(GONE);
        btnOk.setVisibility(GONE);
    }

    public void showError(String error) {
        this.setVisibility(View.VISIBLE);
        if (error != null) {
            tvTitle.setText(error);
        } else {
            tvTitle.setText(getResources().getString(R.string.communicationError));
        }
        btnOk.setVisibility(VISIBLE);
        progress.setVisibility(GONE);
        magImage.setVisibility(GONE);
    }

    public void showImage(boolean isShow) {
        if (isShow) {
            magImage.setVisibility(VISIBLE);
        } else {
            magImage.setVisibility(GONE);
        }
    }
}
