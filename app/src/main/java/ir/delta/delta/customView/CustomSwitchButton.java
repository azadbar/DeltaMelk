package ir.delta.delta.customView;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;

import androidx.annotation.Nullable;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.FontTypeEnum;

public class CustomSwitchButton extends LinearLayout {


    private View selectedView;
    private BaseTextView oneSection;
    private BaseTextView secondSection;
    private onClickCustomButton listener;
    private int selectedWidth;
    private int padding = 20;
    private int width;
    private int selectedTextColor;
    private int selectedBackColor;
    private int BackColor;
    private int selectedIndex = 0;
    private int textColor;

    public void setListener(onClickCustomButton listener) {
        this.listener = listener;
    }

    public CustomSwitchButton(Context context) {
        this(context, null);
    }

    public CustomSwitchButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomSwitchButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_switch_button, this, true);
        selectedView = findViewById(R.id.selectedView);
        oneSection = findViewById(R.id.sale);
        secondSection = findViewById(R.id.buy);
        FrameLayout frameLayout = findViewById(R.id.frameLayout);
//        selectedView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT));

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomSwitchButton, 0, 0);
            padding = a.getDimensionPixelOffset(R.styleable.CustomSwitchButton_padding, 0);
//            String bodyValue = a.getString(R.styleable.CustomEditText_edtBody);
            textColor = a.getColor(R.styleable.CustomSwitchButton_textColor, 0);
            selectedTextColor = a.getColor(R.styleable.CustomSwitchButton_selectedTextColor, 0);
            selectedBackColor = a.getColor(R.styleable.CustomSwitchButton_selectedBackColor, 0);
            BackColor = a.getColor(R.styleable.CustomSwitchButton_backColor, 0);
            if (a.hasValue(R.styleable.CustomSwitchButton_fontTypeSwitch)) {
                int value = a.getInt(R.styleable.CustomSwitchButton_fontTypeSwitch, 0);
                if (value >= 0 && value < FontTypeEnum.values().length) {
                    FontTypeEnum fontType = FontTypeEnum.values()[value];
                }
            }
            a.recycle();
        }

        oneSection.setTextColor(selectedTextColor);
        secondSection.setTextColor(selectedBackColor);

        oneSection.setOnClickListener(view -> {
            selectedIndex = 0;
            animateSwtich();
        });
        secondSection.setOnClickListener(view -> {
            selectedIndex = 1;
            animateSwtich();
        });

    }

    private void animateSwtich() {
        if (selectedIndex == 0) {
            ObjectAnimator animation2 = ObjectAnimator.ofFloat(selectedView, "translationX", 0);
            animation2.setDuration(200);
            animation2.start();
            listener.onIndexSelected(0);
            oneSection.setTextColor(selectedTextColor);
            secondSection.setTextColor(textColor);
            oneSection.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum)_Medium.ttf"));
            secondSection.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
        } else {
            int x = width - (selectedWidth + padding * 2);
            ObjectAnimator animation = ObjectAnimator.ofFloat(selectedView, "translationX", -x);
            animation.setDuration(200);
            animation.start();
            listener.onIndexSelected(1);
            oneSection.setTextColor(textColor);
            secondSection.setTextColor(selectedTextColor);
            secondSection.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum)_Medium.ttf"));
            oneSection.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/IRANSansMobile(FaNum).ttf"));
        }
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = View.MeasureSpec.getSize(widthMeasureSpec);
        int height = getLayoutParams().height;
        selectedWidth = (width - padding * 2 + height) / 2;
        selectedView.getLayoutParams().width = selectedWidth;
        setD(this, BackColor, padding, height / 2);
        setD(selectedView, selectedBackColor, 0, height / 2);
        animateSwtich();
    }


    public interface onClickCustomButton {
        void onIndexSelected(int index);
    }

    public void setSections(String oneSection, String secondSection) {
        if (oneSection != null && secondSection != null) {
            this.oneSection.setText(oneSection);
            this.secondSection.setText(secondSection);
        } else {
            this.oneSection.setText(null);
            this.secondSection.setText(null);
        }
    }

    public void changeSection(int type) {
//        if (type == 0) {
//            oneSectionSelected();
//        } else if (type == 1) {
//            secondSectionSelected();
//        }
        selectedIndex = type;
    }

    private void setD(View view, int color, int padding, int radius) {
        float[] outerRadii = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};
        ShapeDrawable backgroundShape = new ShapeDrawable(new RoundRectShape(outerRadii, null, null));
        backgroundShape.getPaint().setColor(color);
        backgroundShape.getPaint().setStyle(Paint.Style.FILL);
        backgroundShape.getPaint().setAntiAlias(true);
        backgroundShape.setPadding(padding, padding, padding, padding);
        Drawable[] drawables = new Drawable[]{backgroundShape};
        view.setBackground(new LayerDrawable(drawables));
    }
}
