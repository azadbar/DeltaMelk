package ir.delta.delta.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

public class DetailRowAdsView extends LinearLayout {

    @BindView(R.id.tvTxtTitle)
    BaseTextView tvTxtTitle;
    @BindView(R.id.tvValue)
    BaseTextView tvValue;

    public DetailRowAdsView(Context context) {
        this(context, null);
    }

    public DetailRowAdsView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailRowAdsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_row_detail_ads, this, true);
        tvTxtTitle = findViewById(R.id.tvTxtTitle);
        tvValue = findViewById(R.id.tvValue);
//        tvValue.setTextColor(getResources().getColor(R.color.primaryTextColor));
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setTextTitle(String title, String value, String postfix, boolean isConvertNumberToDecimal) {
        tvTxtTitle.setText(setTextSpannable(title.trim(), value, postfix, isConvertNumberToDecimal));

//        tvValue.setText(value);
    }

    private SpannableStringBuilder setTextSpannable(String title, String value, String postfix, boolean isConvertNumberToDecimal) {
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append(title).append(": ");
        int start = desc_two.length();
        if (isConvertNumberToDecimal)
            desc_two.append(Constants.convertNumberToDecimal(Double.parseDouble(value)));
        else
            desc_two.append(String.valueOf(value));
        int end = desc_two.length();
        desc_two.append(" ").append(postfix);
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primaryTextColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);
        return desc_two;
    }

}
