package ir.delta.delta.customView

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RoundRectShape
import androidx.core.content.ContextCompat
import ir.delta.delta.R
import java.util.*

class CustomDrawable(
    pStartColor: Int,
    pCenterColor: Int,
    pEndColor: Int,
    pStrokeWidth: Int,
    pStrokeColor: Int,
    cornerRadius: Float
) : GradientDrawable(Orientation.BOTTOM_TOP, intArrayOf(pStartColor, pCenterColor, pEndColor)) {

    init {
        setStroke(pStrokeWidth, pStartColor)
        shape = RECTANGLE
        setCornerRadius(cornerRadius)
    }

    fun getRipple(
        context: Context,
        drawable: CustomDrawable,
        height:Int
    ): RippleDrawable {
        val floatArray = FloatArray(8)
        Arrays.fill(floatArray, height.toFloat())
        var stateList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.white))
        val shape: RoundRectShape = RoundRectShape(floatArray, null, null)
        val mask: ShapeDrawable = ShapeDrawable(shape)
        return RippleDrawable(stateList, drawable, mask)

    }
}