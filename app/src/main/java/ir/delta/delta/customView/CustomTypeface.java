package ir.delta.delta.customView;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.Log;

public class CustomTypeface extends MetricAffectingSpan {

    private final Typeface typeface;
    private final int color;


    public CustomTypeface(Typeface typeface, int color) {
        this.typeface = typeface;
        this.color = color;
    }


    @Override
    public void updateMeasureState(TextPaint textPaint) {

        applyCustom(textPaint);
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        applyCustom(tp);
    }


    private void applyCustom(TextPaint tp) {
        tp.setTypeface(typeface);
        tp.setColor(color);
        tp.setUnderlineText(false);
    }
}

