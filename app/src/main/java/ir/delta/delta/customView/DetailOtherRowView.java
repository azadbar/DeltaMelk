package ir.delta.delta.customView;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import butterknife.BindView;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.util.Constants;

public class DetailOtherRowView extends LinearLayout {

    @BindView(R.id.tvTxtTitle)
    BaseTextView tvTxtTitle;
    @BindView(R.id.imgValue)
    BaseImageView imgValue;
    @BindView(R.id.tvStatus)
    BaseTextView tvStatus;

    public DetailOtherRowView(Context context) {
        this(context, null);
    }

    public DetailOtherRowView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DetailOtherRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_row_other_detail_estate, this, true);
        tvTxtTitle = findViewById(R.id.tvTxtTitle);
        imgValue = findViewById(R.id.imgValue);
        tvStatus = findViewById(R.id.tvStatus);
//        tvValue.setTextColor(getResources().getColor(R.color.primaryTextColor));
        this.setLayoutDirection(Constants.getLanguage().getLayoutDirection());
    }

    public void setTextTitle(String title) {
        tvTxtTitle.setText(title);
    }

    public void setImageIcon(int icon, int color) {
//        int icon1 = icon;
        this.imgValue.setImageResource(icon);
        imgValue.setColorFilter(getResources().getColor(color), PorterDuff.Mode.SRC_ATOP);

    }

    public void setTextStatus(String status) {
        this.tvStatus.setText(status);
    }

    public void setTextStatusColor(int statusColor) {
        this.tvStatus.setTextColor(statusColor);
    }

}
