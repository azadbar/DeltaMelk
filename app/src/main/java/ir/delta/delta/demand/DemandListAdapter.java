package ir.delta.delta.demand;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Build;

import androidx.annotation.NonNull;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;
import ir.delta.delta.util.Constants;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class DemandListAdapter extends RecyclerView.Adapter<DemandListAdapter.ViewHolder> {


    private final ArrayList<DemandItem> list;
    private final OnItemClickListener listener;


    DemandListAdapter(ArrayList<DemandItem> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_demand, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        DemandItem demand = list.get(position);


        if (!TextUtils.isEmpty(demand.getRegionTitle())) {
            holder.tvTitle.setText(demand.getContractType() + " " + demand.getPropertyType() + " در "  + demand.getRegionTitle());
        } else {
            holder.tvTitle.setText(demand.getContractType() + " " + demand.getPropertyType() + " در " + demand.getCityTitle());
        }
        if (demand.getContractTypeLocalId() == ContractTypeEnum.PURCHASE.getMethodCode()) {
            holder.tvRent.setVisibility(View.GONE);
            holder.tvPrice.setVisibility(View.VISIBLE);
            if (demand.getMaxMortgageOrTotal() >= 1000) {
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.range_price) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(demand.getMaxMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setText(null);
            }
        } else {
            if (demand.getMaxMortgageOrTotal() >= 1000) {
                holder.tvPrice.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.range_mortgage) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(demand.getMaxMortgageOrTotal(), res) + "</strong></font>"));
            } else {
                holder.tvPrice.setText(res.getString(R.string.no_deposit));
            }

            if (demand.getMaxRentOrMetric() >= 1000)
                holder.tvRent.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.rent_text) + ": " + "</font>" +
                        "<font color='#000000'><strong>" + Constants.priceConvertor(demand.getMaxRentOrMetric(), res) + "</strong></font>"));
            else
                holder.tvRent.setText(res.getString(R.string.no_rent));
        }
        if (demand.getAreaTo() >= 10)
            holder.tvArea.setText(createHtmlText("<font color='#999999'>" + res.getString(R.string.range_square) + ": " + "</font>" +
                    "<font color='#000000'><strong>" + demand.getAreaTo() + " " + res.getString(R.string.meter) + "</strong></font>"));
        else
            holder.tvArea.setText(null);

        if (!TextUtils.isEmpty(demand.getDescription())) {
            if (demand.getDescription().length() >= 100) {
                String description = "<font color='#777777'>" + demand.getDescription().substring(0, 30) + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
                holder.tvDescription.setText(createHtmlText(description));
            } else {
                holder.tvDescription.setText(demand.getDescription());
            }
        } else {
            holder.tvDescription.setText(null);
        }


        if (!TextUtils.isEmpty(demand.getFullName())) {
            String advertiser = "<font color='#777777'>" + res.getString(R.string.demandant) + ": </font> " +
                    "<font color='#000000' <strong> " + demand.getFullName() + "</strong></font>";
            holder.tvAdvertiser.setText(createHtmlText(advertiser));
        } else {
            holder.tvAdvertiser.setText(null);
        }

        if (demand.getDemandId() > 0) {
            String codeRequest = "<font color='#777777'>" + res.getString(R.string.code_request) + ": </font> " +
                    "<font color='#000000' <strong> " + demand.getDemandId() + "</strong></font>";
            holder.tvRequestCode.setText(createHtmlText(codeRequest));
        } else {
            holder.tvRequestCode.setText(null);
        }

        if (!TextUtils.isEmpty(demand.getRegistrationPersianDate())) {
            String date = "<font color='#777777'>" + res.getString(R.string.date) + ": </font> " +
                    "<font color='#000000' <strong> " + demand.getRegistrationPersianDate() + "</strong></font>";
            holder.tvDate.setText(createHtmlText(date));
        } else {
            holder.tvDate.setText(null);
        }

        if (!TextUtils.isEmpty(demand.getMobile())) {
            holder.btnContact.setVisibility(View.VISIBLE);
        } else {
            holder.btnContact.setVisibility(View.GONE);
        }
        holder.communication(demand, position, listener);
        holder.share(demand, position, listener);
        holder.itemView.setOnClickListener(view -> clickDescription(res, holder.tvDescription, demand));
    }

    private void clickDescription(Resources res, BaseTextView tvDescription, DemandItem demand) {

        if (demand.getDescription().length() >= 100) {
            String description;
            if (demand.isExpanded()) {
                description = "<font color='#777777'>" + demand.getDescription().substring(0, 20) + " </font> " +
                        "<font color='#E30427' <strong> " + "... " + res.getString(R.string.more) + "</strong></font>";
            } else {
                description = "<font color='#777777'>" + demand.getDescription() + " </font> " +
                        "<font color='#E30427' <strong> " + res.getString(R.string.less) + "</strong></font>";
            }
            demand.setExpanded(!demand.isExpanded());
            tvDescription.setText(createHtmlText(description));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvPrice)
        BaseTextView tvPrice;
        @BindView(R.id.tvRent)
        BaseTextView tvRent;
        @BindView(R.id.tvArea)
        BaseTextView tvArea;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.tvAdvertiser)
        BaseTextView tvAdvertiser;
        @BindView(R.id.tvRequestCode)
        BaseTextView tvRequestCode;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.btnContact)
        FloatingActionButton btnContact;
        @BindView(R.id.btnShare)
        FloatingActionButton btnShare;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void communication(DemandItem demand, int position, final OnItemClickListener listener) {
            btnContact.setOnClickListener(v -> listener.onItemCommunication(position, demand));
        }

        public void share(DemandItem demand, int position, final OnItemClickListener listener) {
            btnShare.setOnClickListener(v -> listener.onItemShare(position, demand));
        }


    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public interface OnItemClickListener {
        void onItemCommunication(int position, DemandItem demand);

        void onItemShare(int position, DemandItem demand);
    }

}
