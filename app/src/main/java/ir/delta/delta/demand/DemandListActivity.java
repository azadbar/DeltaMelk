package ir.delta.delta.demand;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.customView.LoadingView;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.estateDetail.ComminucationDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.DemandSiteService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.DemandFilterReq;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;
import ir.delta.delta.service.ResponseModel.demand.DemandResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.Validation;

public class DemandListActivity extends BaseActivity implements DemandListAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.loadingView)
    LoadingView loadingView;
    @BindView(R.id.rvDemandList)
    RecyclerView rvDemandList;
    @BindView(R.id.rlLoding)
    BaseRelativeLayout rlLoding;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;

    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;

    private DemandListAdapter adapter;
    private DemandFilterReq filter;
    private boolean inLoading = false;
    private boolean endOfList = false;
    private final int offset = 30;

    private final ArrayList<DemandItem> demands = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demand_list);
        ButterKnife.bind(this);

        intView();
        getList();
    }

    private void intView() {
        checkArrowRtlORLtr(imgBack);
        rlContacrt.setVisibility(View.VISIBLE);
        rlFilterChange.setVisibility(View.VISIBLE);
        tvFilterChnage.setText(getResources().getString(R.string.filter));
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tvFilterChnage.getLayoutParams();
        params.width = getResources().getDimensionPixelSize(R.dimen.minwidth);
        tvFilterChnage.setLayoutParams(params);
        tvFilterChnage.setTextSize(getResources().getDimensionPixelSize(R.dimen.text_size_2));
        tvFilterChnage.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        loadingView.setButtonClickListener(view -> getList());
    }

    private void getList() {
        if (!inLoading && !endOfList) {
            inLoading = true;
            if (demands.isEmpty()) {
                showLoading();
            } else {
                rlLoding.setVisibility(View.VISIBLE);
            }
            getDemandListRequest();
        }
    }

    private void getDemandListRequest() {
        Map<String, Object> map;
        if (filter == null) {
            map = DemandFilterReq.getParamsWithoutFilter(demands.size(), offset);
        } else {
            map = filter.getParams(demands.size(), offset);
        }

        DemandSiteService.getInstance().getDemandSiteList(this, getResources(), map, new ResponseListener<DemandResponse>() {
            @Override
            public void onGetError(String error) {
                if (imgBack != null) {
                    onError(error);
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(DemandListActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(imgBack.getContext());
                startActivity(intent);
                finish();
            }
            @Override
            public void onSuccess(DemandResponse response) {
                if (imgBack != null) {
                    if (rlLoding == null) {
                        return;
                    }
                    inLoading = false;
                    stopLoading();
                    rlLoding.setVisibility(View.GONE);
                    if (response.isSuccessed() && response.getDemandSearchList() != null) {
                        if (response.getDemandSearchList().size() < offset) {
                            endOfList = true;
                        }
                        handleToolbarFragment(response);
                        demands.addAll(response.getDemandSearchList());
                        setDataAdapter();
                    } else {
                        loadingView.setVisibility(View.VISIBLE);
                        loadingView.showError(response.getMessage());
                    }
                }
            }
        });
    }

    public void handleToolbarFragment(DemandResponse response) {
        if (response.getDemandSearchList().size() > 0 || demands.size() > 0) {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterDetail.setVisibility(View.GONE);
            tvFilterTitle.setVisibility(View.VISIBLE);
            tvFilterTitle.setText(response.getSearchResultCount() + " " + getResources().getString(R.string.found_request));

        } else {
            rlContacrt.setVisibility(View.VISIBLE);
            tvFilterTitle.setText(getResources().getString(R.string.no_found_request));
//            tvFilterDetail.setText(getString(R.string.not_advertis));
            tvFilterDetail.setVisibility(View.GONE);
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.not_found_info));
        }
    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new DemandListAdapter(demands, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvDemandList.setHasFixedSize(true);
            rvDemandList.setLayoutManager(layoutManager);
            rvDemandList.smoothScrollToPosition(0);//TODO change scroll
            layoutManager.scrollToPositionWithOffset(0, 0);
            rvDemandList.setAdapter(adapter);
            rvDemandList.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    if (dy > 0) {
                        int visibleItemCount = layoutManager.getChildCount();
                        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                        int totalItemCount = layoutManager.getItemCount();
                        if (firstVisibleItem + visibleItemCount + 5 > totalItemCount && !inLoading && !endOfList)
                            if (firstVisibleItem != 0 || visibleItemCount != 0) {
                                getList();
                            }
                    }
                }
            });
        } else {
            adapter.notifyDataSetChanged();
        }
    }


    @OnClick({R.id.rlBack, R.id.rlFilterChange})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
                finish();
                break;
            case R.id.rlFilterChange:
                Intent intent = new Intent(this, DemandFilterActivity.class);
                intent.putExtra("filter", filter);
                startActivityForResult(intent, Constants.FILTER_DEMAND_LIST);
                break;
        }

    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
        rvDemandList.setVisibility(View.GONE);
        loadingView.showLoading(false);
    }

    private void stopLoading() {
        loadingView.setVisibility(View.GONE);
        rvDemandList.setVisibility(View.VISIBLE);
    }

    private void onError(String error) {
        inLoading = false;
        if (rlLoding != null && demands != null) {
            rlLoding.setVisibility(View.GONE);
            if (demands.isEmpty()) {
                loadingView.showError(error);
            }
        }
    }

    @Override
    public void onItemCommunication(int position, DemandItem demand) {
        if (!TextUtils.isEmpty(demand.getMobile())) {
            if (TextUtils.isEmpty(Validation.getMobileError(demand.getMobile(), true, getResources()))) {
                ComminucationDialog comminucationDialog = new ComminucationDialog(this);
                comminucationDialog.setName(demand.getFullName());
                comminucationDialog.setMobile(demand.getMobile());
                comminucationDialog.setIsOfficeActive(false);
                comminucationDialog.setUserAgencyName("");
                comminucationDialog.setLogoSrc("");
                comminucationDialog.show();
            } else {
                Toast.makeText(this, "شماره تماس اشتباه است", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, getResources().getString(R.string.not_find_mobile), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.FILTER_DEMAND_LIST) {
                filter = (DemandFilterReq) data.getSerializableExtra("demandFilter");
                tvFilterTitle.setText("");
                tvFilterDetail.setText("");
                demands.clear();
                if (adapter != null)
                    adapter.notifyDataSetChanged();
                endOfList = false;
                inLoading = false;
                rootEmptyView.setVisibility(View.GONE);
                getList();

            }
        }
    }

    @Override
    public void onItemShare(int position, DemandItem demand) {

        Constants.shareTextUrl(this, "در خواست " + " " + demand.getContractType() + " " + demand.getPropertyType()
                + " " + demand.getCityTitle() + " " + demand.getRegionTitle() + " " + "\n\n" + demand.getDescription() + "\n\n" + "درخواست کننده : "
                + demand.getFullName() + "\n\n " + "شماره تماس :" + demand.getMobile() + "\n\n" + "کد درخواست : "
                + demand.getDemandId() + "\n\n" + "سایت دلتا:"
                + "http://www.delta.ir");

    }
}
