package ir.delta.delta.demand;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.service.RequestModel.DemandFilterReq;

public class DemandFilterActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demand_filter);
        DemandFilterFragment demandFilterFragment = new DemandFilterFragment();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            DemandFilterReq demandFilter = (DemandFilterReq) bundle.getSerializable("filter");
            if (demandFilter != null) {
                Bundle b = new Bundle();
                b.putSerializable("filter", demandFilter);
                demandFilterFragment.setArguments(b);
            }
        }
        FragmentManager frgMgr = getSupportFragmentManager();
        FragmentTransaction frgTrans = frgMgr.beginTransaction();
        frgTrans.replace(R.id.frameLayout, demandFilterFragment);
        frgTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        frgTrans.commit();
    }
}
