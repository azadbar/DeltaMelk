package ir.delta.delta.demand;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseFragment;
import ir.delta.delta.baseView.BaseImageView;
import ir.delta.delta.baseView.BaseRelativeLayout;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.bottomNavigation.search.LocationSelectFragment;
import ir.delta.delta.customView.CustomEditText;
import ir.delta.delta.customView.CustomSwitchButton;
import ir.delta.delta.customView.LocationView;
import ir.delta.delta.database.TransactionArea;
import ir.delta.delta.database.TransactionCity;
import ir.delta.delta.enums.ContractTypeEnum;
import ir.delta.delta.enums.LocationTypeEnum;
import ir.delta.delta.service.RequestModel.DemandFilterReq;
import ir.delta.delta.service.ResponseModel.Area;
import ir.delta.delta.service.ResponseModel.City;
import ir.delta.delta.service.ResponseModel.ContractObject;
import ir.delta.delta.service.ResponseModel.PropertyType;
import ir.delta.delta.util.Constants;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;

import static android.app.Activity.RESULT_OK;

public class DemandFilterFragment extends BaseFragment {

    private final String AREAS = "areas";
    private final String PROPERTY = "property";
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.rlBack)
    BaseRelativeLayout rlBack;
    @BindView(R.id.tvFilterTitle)
    BaseTextView tvFilterTitle;
    @BindView(R.id.tvFilterDetail)
    BaseTextView tvFilterDetail;
    @BindView(R.id.rlContacrt)
    BaseRelativeLayout rlContacrt;
    @BindView(R.id.edtCodeRequest)
    CustomEditText edtCodeRequest;
    @BindView(R.id.propertyType_layout)
    LocationView propertyTypeLayout;
    @BindView(R.id.area_layout)
    LocationView areaLayout;
    @BindView(R.id.from_date_layout)
    LocationView fromDateLayout;
    @BindView(R.id.to_date_layout)
    LocationView toDateLayout;
    @BindView(R.id.btnSearch)
    BaseTextView btnSearch;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.card_first)
    CustomSwitchButton cardFirst;
    @BindView(R.id.tvFilterChnage)
    BaseTextView tvFilterChnage;
    @BindView(R.id.rlFilterChange)
    BaseRelativeLayout rlFilterChange;
    private boolean isRentSelected = false;
    private ContractObject selectedContractType;
    private PropertyType selectedPropertyType;
    private Area selectedArea;
    private DemandFilterReq demandFilter;
    private City ConsultantCity;
    private PersianCalendar calendarFromDate;
    private PersianCalendar calendarToDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragmnet_demand_filter, container, false);
        ButterKnife.bind(this, view);
        hideInputKeyboard();
        initView();
        Bundle b = getArguments();
        if (b != null) {
            demandFilter = (DemandFilterReq) b.getSerializable("filter");

        }
        setDemandFilter();
        return view;
    }

    private void setDemandFilter() {
        Area ConsultantArea = TransactionArea.getInstance().getAreaById(getActivity(), Constants.getUser(getActivity()).getLocationId());

        if (ConsultantArea == null) {
            ConsultantCity = TransactionCity.getInstance().getCityById(getActivity(), Constants.getUser(getActivity()).getLocationId());
        } else {
            ConsultantCity = TransactionCity.getInstance().getCityById(getActivity(), ConsultantArea.getParentId());
        }

        if (demandFilter != null && demandFilter.getDemandId() > 0) {
            edtCodeRequest.setTextBody(demandFilter.getDemandId() + "");
        } else {
            edtCodeRequest.setTextsTitle(getResources().getString(R.string.code_request));
        }

        if (demandFilter != null && demandFilter.getContractTypeLocalId() > 0) {
            setContractType(ContractTypeEnum.getContractType(demandFilter.getContractTypeLocalId()));

        } else {
            setContractType(ContractTypeEnum.getContractType(ContractTypeEnum.PURCHASE.getMethodCode()));
        }

        changeContractType(selectedContractType.getType() == ContractTypeEnum.RENT);


        if (demandFilter != null && demandFilter.getPropertyTypeLocalId() > 0 && selectedContractType != null) {
            propertyTypeSelected(selectedContractType.getPropertyTypeBy(demandFilter.getPropertyTypeLocalId()));
        } else {
            propertyTypeLayout.setTxtTitle(getString(R.string.property_text));
            propertyTypeLayout.setHint(getString(R.string.click_to_select));
            propertyTypeLayout.reset();
        }

        if (ConsultantCity != null && TransactionArea.getInstance().hasArea(getActivity(), ConsultantCity.getId())) {
            areaLayout.setVisibility(View.VISIBLE);
            if (demandFilter != null && demandFilter.getLocationId() > 0) {
                areaSelected(TransactionArea.getInstance().getAreaById(getActivity(), demandFilter.getLocationId()));
            } else {
                areaLayout.setTxtTitle(getString(R.string.area_text));
                areaLayout.setHint(getString(R.string.your_estat));
                areaLayout.reset();
            }
        } else {
            areaLayout.setVisibility(View.GONE);
        }

        if (demandFilter != null && demandFilter.getFromCalender() != null) {
            calendarFromDate = demandFilter.getFromCalender();
            fromDateLayout.setValue(demandFilter.getFromCalender().getPersianYear() + "/" + demandFilter.getFromCalender().getPersianMonth() + "/" + demandFilter.getFromCalender().getPersianDay(), null);
        } else {
            fromDateLayout.setTxtTitle(getString(R.string.from_data));
            fromDateLayout.setHint(getString(R.string.choose_from_date));
            fromDateLayout.reset();
        }

        if (demandFilter != null && demandFilter.getToCaledner() != null) {
            calendarToDate = demandFilter.getToCaledner();
            toDateLayout.setValue(demandFilter.getToCaledner().getPersianYear() + "/" + demandFilter.getToCaledner().getPersianMonth() + "/" + demandFilter.getToCaledner().getPersianDay(), null);
        } else {
            toDateLayout.setTxtTitle(getString(R.string.to_date));
            toDateLayout.setHint(getString(R.string.choose_to_date));
            toDateLayout.reset();
        }

    }


    private void initView() {
        rlBack.setVisibility(View.VISIBLE);
        checkArrowRtlORLtr(imgBack);
        rlFilterChange.setVisibility(View.VISIBLE);
        tvFilterChnage.setText(getResources().getString(R.string.delete_filter));

        rlContacrt.setVisibility(View.VISIBLE);
        tvFilterDetail.setVisibility(View.GONE);
        tvFilterTitle.setText(getString(R.string.search_request));

        propertyTypeLayout.setTxtTitle(getString(R.string.property_text));
        propertyTypeLayout.setHint(getString(R.string.click_to_select));
        propertyTypeLayout.reset();

        areaLayout.setTxtTitle(getString(R.string.area_text));
        areaLayout.setHint(getString(R.string.click_to_select));
        areaLayout.reset();

        fromDateLayout.setTxtTitle(getString(R.string.from_data));
        fromDateLayout.setHint(getString(R.string.choose_from_date));
        fromDateLayout.reset();

        toDateLayout.setTxtTitle(getString(R.string.to_date));
        toDateLayout.setHint(getString(R.string.choose_to_date));
        toDateLayout.reset();

        cardFirst.setSections(getString(R.string.buy_text), getString(R.string.rent_text));

        cardFirst.setListener(index -> {
            if (index == 0) {
                changeContractType(false);
            } else if (index == 1) {
                changeContractType(true);
            }
        });
    }

    private void changeContractType(boolean isRent) {
        if (isRentSelected != isRent) {
            isRentSelected = isRent;
            if (isRentSelected) {
                cardFirst.changeSection(1);
                setContractType(ContractTypeEnum.RENT);
            } else {
                cardFirst.changeSection(0);
                setContractType(ContractTypeEnum.PURCHASE);
            }
        }
    }

    private void setContractType(ContractTypeEnum contractType) {
        if (ConsultantCity != null) {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractType.getMethodCode(), ConsultantCity.getLocationFeatureLocalId());
        } else {
            selectedContractType = Constants.getContractObjects(getActivity()).getContract(contractType.getMethodCode(), 1);
        }
        propertyTypeSelected(null);

    }

    private void propertyTypeSelected(PropertyType propertyType) {
        if (propertyTypeLayout == null) {
            return;
        }
        selectedPropertyType = propertyType;
        if (selectedPropertyType != null) {
            propertyTypeLayout.setValue(selectedPropertyType.getTitle(), null);
        } else {
            propertyTypeLayout.reset();
        }
    }

    private void areaSelected(Area area) {
        areaLayout.setVisibility(View.VISIBLE);
        selectedArea = area;
        if (selectedArea != null) {
            areaLayout.setValue(selectedArea.getTitle(), selectedArea.getDescription());
        } else {
            if (ConsultantCity != null && TransactionArea.getInstance().getAreasCount(getActivity(), ConsultantCity.getId()) > 0) {
                areaLayout.reset();
            } else {
                areaLayout.setVisibility(View.GONE);
            }
        }
    }

    @OnClick({R.id.rlBack, R.id.btnSearch, R.id.btnCancel, R.id.propertyType_layout, R.id.area_layout,
            R.id.to_date_layout, R.id.from_date_layout, R.id.rlFilterChange})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlBack:
            case R.id.btnCancel:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.btnSearch:
                fillDemandFilter();
                if (getActivity() != null) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", Constants.FILTER_DEMAND_LIST);
                    returnIntent.putExtra("demandFilter", demandFilter);
                    getActivity().setResult(RESULT_OK, returnIntent);
                    getActivity().finish();
                }

                break;
            case R.id.propertyType_layout:
                if (selectedContractType != null && selectedContractType.getPropertyTypes() != null && selectedContractType.getPropertyTypes().size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(DemandFilterFragment.this, Constants.REQUEST_PROPERTYTYPE_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", true);
                    bundle.putString("extraName", PROPERTY);
                    bundle.putSerializable("locationType", LocationTypeEnum.PROPERTYTYPE);
                    bundle.putParcelableArrayList("list", new ArrayList<>(selectedContractType.getPropertyTypes()));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    loadFragment(locationFragment, LocationSelectFragment.class.getName());
                }
                break;
            case R.id.area_layout:
                ArrayList<Area> areas = TransactionArea.getInstance().getAreasBy(getActivity(), ConsultantCity.getId());
                if (areas.size() > 0) {
                    LocationSelectFragment locationFragment = new LocationSelectFragment();
                    locationFragment.setTargetFragment(DemandFilterFragment.this, Constants.REQUEST_AREA_CODE);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isMultiSelect", false);
                    bundle.putBoolean("isSingleLine", false);
                    bundle.putString("extraName", AREAS);
                    bundle.putSerializable("locationType", LocationTypeEnum.AREA);
                    bundle.putParcelableArrayList("list", new ArrayList<>(areas));
                    bundle.putParcelableArrayList("selectedList", null);
                    locationFragment.setArguments(bundle);
                    loadFragment(locationFragment, LocationSelectFragment.class.getName());
                }

                break;

            case R.id.from_date_layout:
                if (getActivity() == null) {
                    return;
                }
                PersianCalendar initDate = null;
                if (demandFilter != null && demandFilter.getFromCalender() != null) {
                    initDate = new PersianCalendar();
                    initDate.setPersianDate(demandFilter.getFromCalender().getPersianYear(), demandFilter.getFromCalender().getPersianMonth(), demandFilter.getFromCalender().getPersianDay());
                }
                PersianDatePickerDialog picker = new PersianDatePickerDialog(getActivity())
                        .setPositiveButtonString("تایید")
                        .setNegativeButton("بیخیال")
                        .setTodayButton("امروز")
                        .setTodayButtonVisible(true)
                        .setInitDate(initDate)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                        .setMinYear(1390)
                        .setTypeFace(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANSansMobile(FaNum).ttf"))
                        .setActionTextColor(Color.GRAY)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar fromDate) {
                                calendarFromDate = fromDate;
                                fromDateLayout.setValue(fromDate.getPersianYear() + "/" + fromDate.getPersianMonth() + "/" + fromDate.getPersianDay(), null);
                                toDateLayout.reset();
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
                break;

            case R.id.to_date_layout:
                if (getActivity() == null) {
                    return;
                }
                PersianCalendar initDate1 = null;
                if (demandFilter != null && demandFilter.getToCaledner() != null) {
                    initDate1 = new PersianCalendar();
                    initDate1.setPersianDate(demandFilter.getToCaledner().getPersianYear(), demandFilter.getToCaledner().getPersianMonth(), demandFilter.getToCaledner().getPersianDay());
                }
                picker = new PersianDatePickerDialog(getActivity())
                        .setPositiveButtonString("تایید")
                        .setNegativeButton("بیخیال")
                        .setTodayButton("امروز")
                        .setTodayButtonVisible(true)
                        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                        .setMinYear(1390)
                        .setTypeFace(Typeface.createFromAsset(getActivity().getAssets(), "fonts/IRANSansMobile(FaNum).ttf"))
                        .setInitDate(initDate1)
                        .setActionTextColor(Color.GRAY)
                        .setListener(new Listener() {
                            @Override
                            public void onDateSelected(PersianCalendar toDate) {
                                calendarToDate = toDate;
                                if (calendarFromDate != null && calendarToDate != null && calendarFromDate.after(calendarToDate)) {
                                    Toast.makeText(getActivity(), "تاریخ پایان جلوتر از تاریخ شروع می باشد", Toast.LENGTH_SHORT).show();
                                } else {
                                    toDateLayout.setValue(toDate.getPersianYear() + "/" + toDate.getPersianMonth() + "/" + toDate.getPersianDay(), null);

                                }
                            }

                            @Override
                            public void onDismissed() {

                            }
                        });

                picker.show();
                break;
            case R.id.rlFilterChange:
                if (getActivity() != null) {
                    demandFilter = null;
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", Constants.FILTER_DEMAND_LIST);
                    returnIntent.putExtra("demandFilter", demandFilter);
                    getActivity().setResult(RESULT_OK, returnIntent);
                    getActivity().finish();
                }
                break;

        }
    }

    private void fillDemandFilter() {

        if (demandFilter == null)
            demandFilter = new DemandFilterReq();


        demandFilter.setDemandId(edtCodeRequest.getValueInt());

        if (selectedContractType != null)
            demandFilter.setContractTypeLocalId(selectedContractType.getContractTypeLocalId());

        if (selectedPropertyType != null)
            demandFilter.setPropertyTypeLocalId(selectedPropertyType.getPropertyTypeLocalId());

        if (selectedArea != null)
            demandFilter.setLocationId(selectedArea.getId());


        if (calendarFromDate != null)
            demandFilter.setFromDate(calendarFromDate.get(PersianCalendar.YEAR) + "/" + ((calendarFromDate.get(PersianCalendar.MONTH)) + 1) + "/" + calendarFromDate.get(PersianCalendar.DATE));

        if (calendarToDate != null)
            demandFilter.setToDate(calendarToDate.get(PersianCalendar.YEAR) + "/" + ((calendarToDate.get(PersianCalendar.MONTH)) + 1) + "/" + calendarToDate.get(PersianCalendar.DATE));


        demandFilter.setFromCalender(calendarFromDate);

        demandFilter.setToCaledner(calendarToDate);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_PROPERTYTYPE_CODE) {
                propertyTypeSelected(data.getParcelableExtra(PROPERTY));
            } else if (requestCode == Constants.REQUEST_AREA_CODE) {
                areaSelected(data.getParcelableExtra(AREAS));
            }
        }
    }

    public void loadFragment(LocationSelectFragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.add(R.id.frameLayout, fragment, fragmentTag);
            fragTrans.addToBackStack(fragmentTag);
            fragTrans.commit();
        }
    }

    private void hideInputKeyboard() {
        if (getActivity() != null) {
            if (this.edtCodeRequest.requestFocus()) {
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            }
        }
    }


}
