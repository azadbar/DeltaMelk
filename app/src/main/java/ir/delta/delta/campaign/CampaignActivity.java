package ir.delta.delta.campaign;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.database.TransactionCampaignUser;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.databinding.ActivityCampaignBinding;
import ir.delta.delta.dialog.CustomDialog;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.pushNotification.MagPostContentPushResponse;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.campaign.CampaignGetQuizFirstPageCodeService;
import ir.delta.delta.service.Request.campaign.CampaignGetQuizTopListService;
import ir.delta.delta.service.RequestModel.campaign.CampaignQuizReq;
import ir.delta.delta.service.ResponseModel.Campaign.CampaignGetQuizTopListResponse;
import ir.delta.delta.service.ResponseModel.Campaign.CampaignQuizFirstPageResponse;
import ir.delta.delta.service.ResponseModel.Campaign.CampaignUser;
import ir.delta.delta.service.ResponseModel.Campaign.QuizTopListItem;
import ir.delta.delta.service.ResponseModel.profile.ConsultantUserResponse;
import ir.delta.delta.service.ResponseModel.profile.User;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

import static android.view.View.VISIBLE;

public class CampaignActivity extends BaseActivity {

    private CampaignQuizFirstPageResponse response;
    ActivityCampaignBinding binding;
    private String mobile;
    private final ArrayList<QuizTopListItem> quizTopList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_campaign);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mobile = bundle.getString("mobile");
        }

        initView();
        onClick();
        quizFirstPageRequest();
    }

    private void initView() {
        seStatusBarColor(binding.rlBack, getResources().getColor(R.color.redColor));
        binding.background.setImageResource(R.drawable.background);
        Glide.with(this).load(R.drawable.app_header).into(binding.imageHeader);
        Glide.with(this).load(R.drawable.coins_bg).into(binding.coinImage);

    }

    private void quizFirstPageRequest() {
        hideKeyboard();
        binding.roundedLoadingView.setVisibility(VISIBLE);
        enableDisableViewGroup(binding.root, false);
        CampaignQuizReq req = new CampaignQuizReq();
        req.setMobile(mobile);

        CampaignGetQuizFirstPageCodeService.getInstance().getQuizFirstPage(getResources(), req, new ResponseListener<CampaignQuizFirstPageResponse>() {
            @Override
            public void onGetError(String error) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                Toast.makeText(CampaignActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(CampaignQuizFirstPageResponse response) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                if (response.isSuccessed()) {
                    CampaignActivity.this.response = response;
                    fillView();
                    saveUser(response);
                    getListQuizRequest();
                } else {
                    if (response.isNotRegistered() || response.isNotActivated()) {
                        Intent intent = new Intent(CampaignActivity.this, RegisterCampaignActivity.class);
                        startActivity(intent);
                        finish();
                        Constants.setCurrentCampaignUser(null);
                        TransactionCampaignUser.getInstance().deleteUsr(CampaignActivity.this);
                    }
                    showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(CampaignActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(CampaignActivity.this);
                startActivity(intent);
                finish();
            }
        });
    }

    private void fillView() {
        binding.name.setText(response.getName());
        binding.mobile.setText(response.getMobile());
        binding.point.setText(String.valueOf(response.getPoints()));
        binding.friend.setText(String.valueOf(response.getFriendsCount()));
        if (response.isHasActiveQuiz()) {
            binding.startCampaign.setEnabled(true);
            binding.startCampaign.setBackground(getResources().getDrawable(R.drawable.ripple_red_raduis));
            binding.tvStartCampaign.setTextColor(getResources().getColor(R.color.white));
        } else {
            binding.startCampaign.setEnabled(false);
            binding.startCampaign.setBackground(getResources().getDrawable(R.drawable.ripple_disable_radius));
            binding.tvStartCampaign.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void saveUser(CampaignQuizFirstPageResponse response) {
        CampaignUser user = new CampaignUser();
        user.setName(response.getName());
        user.setMobile(response.getMobile());
        user.setRefCode(response.getRefCode());
        user.setFriendsCount(response.getFriendsCount());
        user.setPoints(response.getPoints());
        user.setNotRegistered(response.isNotRegistered());
        user.setNotActivated(response.isNotActivated());
        user.setHasActiveQuiz(response.isHasActiveQuiz());
        TransactionCampaignUser.getInstance().save(this, user);
        Constants.setCurrentCampaignUser(user);
    }

    private void getListQuizRequest() {

        binding.progressBar.setVisibility(VISIBLE);
        binding.rvPersonTop.setVisibility(View.GONE);
        if (quizTopList != null && quizTopList.size() > 0)
            quizTopList.clear();

        CampaignGetQuizTopListService.getInstance().getQuizTopList(getResources(), new ResponseListener<CampaignGetQuizTopListResponse>() {
            @Override
            public void onGetError(String error) {
                binding.progressBar.setVisibility(View.GONE);
                Toast.makeText(CampaignActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(CampaignGetQuizTopListResponse response) {
                binding.progressBar.setVisibility(View.GONE);
                binding.rvPersonTop.setVisibility(VISIBLE);
                if (response.isSuccessed()) {
                    if (response.getQuizTopList() != null && response.getQuizTopList().size() > 0) {
                        quizTopList.addAll(response.getQuizTopList());
                        setAdapter();
                    }
                } else {
                    showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(CampaignActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(CampaignActivity.this);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setAdapter() {
        binding.myScroll.scrollTo(0,0);
        CampaginQuizAdapter adapter = new CampaginQuizAdapter(quizTopList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        binding.rvPersonTop.setHasFixedSize(true);
        binding.rvPersonTop.setLayoutManager(layoutManager);
        binding.rvPersonTop.setAdapter(adapter);
    }

    private void onClick() {
        binding.rlBack.setOnClickListener(v -> finish());

        binding.introduceToFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog dialog = new CustomDialog(CampaignActivity.this);
                dialog.setIcon(R.drawable.ic_share_gray, getResources().getColor(R.color.redColor));
                dialog.setDialogTitle(getResources().getString(R.string.share_reference_code_txt));
                dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog.setDescription(response.getRefCode());
                dialog.setCancelable(true);
                dialog.setOkListener(getResources().getString(R.string.share), v1 -> {
                    Constants.shareTextUrl(CampaignActivity.this, PreferencesData.getLinDownload(CampaignActivity.this) + "\n"
                            + "کد معرف شما :" + response.getRefCode());
                });
                dialog.setCancelListener(getResources().getString(R.string.copy), v12 -> {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", response.getRefCode());
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(CampaignActivity.this, "کد کپی شد", Toast.LENGTH_SHORT).show();
                });
                dialog.show();

            }
        });

        binding.startCampaign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CampaignActivity.this, "start campaign", Toast.LENGTH_SHORT).show();
            }
        });

        binding.winnersPrizes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CampaignActivity.this, "این بخش به زودی در دسترس قرار خواهد گرفت", Toast.LENGTH_SHORT).show();
            }
        });

        binding.refreshList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getListQuizRequest();
            }
        });
    }

}
