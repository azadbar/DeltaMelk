package ir.delta.delta.campaign;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.databinding.ActivityRegisterCampaignBinding;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.campaign.RegisterCampaignService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.RequestModel.campaign.CampaignRegisterReq;
import ir.delta.delta.service.ResponseModel.Campaign.RegisterCampaignResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

import static android.view.View.VISIBLE;

public class RegisterCampaignActivity extends BaseActivity {

    ActivityRegisterCampaignBinding binding;
    private boolean changeMobileNumber;
    private boolean isRetryCode;
    private int mLastContentHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_campaign);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            changeMobileNumber = bundle.getBoolean(Constants.isChangeMobileNumber, false);
            isRetryCode = bundle.getBoolean(Constants.isRetryCode);
        }

        mLastContentHeight = findViewById(Window.ID_ANDROID_CONTENT).getHeight();
        binding.rlRoot.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
        initView();
        onClick();

    }


    private void initView() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(binding.rlBack, getResources().getColor(R.color.redColor));
        binding.background.setImageResource(R.drawable.background);
        Handler h = new Handler();
        h.postDelayed(() -> {
            binding.myScroll.smoothScrollTo(0, 0);
            binding.myScroll.smoothScrollBy(0, 0);
            binding.myScroll.scrollTo(0, 0);
        }, 100);
//        binding.myScroll.setEnableScrolling(false);
//        binding.myScroll.scrollTo(0,0);



        Glide.with(this).load(R.drawable.app_header).into(binding.imageHeader);
        Glide.with(this).load(R.drawable.coins_bg).into(binding.coinImage);

        if ((changeMobileNumber || isRetryCode) && !TextUtils.isEmpty(PreferencesData.getMobileNumber(this))) {
            binding.edtMobile.setTextBody(PreferencesData.getMobileNumber(this));
            binding.edtName.setTextBody(PreferencesData.getNameCampaign(this));
        }
    }


    private void onClick() {
        binding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidData()) {
                    sendRequestMobile();
                }

            }
        });

        binding.rlBack.setOnClickListener(v -> finish());
    }

    private void sendRequestMobile() {
        hideKeyboard();
        binding.roundedLoadingView.setVisibility(VISIBLE);
        enableDisableViewGroup(binding.root, false);
        CampaignRegisterReq req = new CampaignRegisterReq();
        req.setName(binding.edtName.getValueString());
        req.setMobile(binding.edtMobile.getValueString());
        req.setRefererCode(binding.edtRefrenceCode.getValueString());
        RegisterCampaignService.getInstance().getRegisterCode(getResources(), req, new ResponseListener<RegisterCampaignResponse>() {
            @Override
            public void onGetError(String error) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                Toast.makeText(RegisterCampaignActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(RegisterCampaignResponse response) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                if (response.isSuccessed()) {
                    PreferencesData.setMobileNumber(RegisterCampaignActivity.this, binding.edtMobile.getValueString());
                    PreferencesData.setlongParam(RegisterCampaignActivity.this, Constants.SEND_VERIFICATION_TIME, System.currentTimeMillis());
                    Intent intent = new Intent(RegisterCampaignActivity.this, VerificationCodeCampaignActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(RegisterCampaignActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(RegisterCampaignActivity.this);
                startActivity(intent);
                finish();
            }
        });

    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (binding.edtName.getError() != null) {
            errorMsgList.add(binding.edtName.getError());
        }

        if (binding.edtMobile.getError() != null) {
            errorMsgList.add(binding.edtMobile.getError());
        }

        if (binding.edtRefrenceCode.getError() != null) {
            errorMsgList.add(binding.edtRefrenceCode.getError());
        }
        if (errorMsgList.size() > 0) {
            showInfoDialog(getResources().getString(R.string.attetion), errorMsgList);
            return false;
        }

        return true;
    }

    private final ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int currentContentHeight = findViewById(Window.ID_ANDROID_CONTENT).getHeight();

            if (mLastContentHeight > currentContentHeight + 100) {
                Log.d("Keyboard is open", "OPEN");
                mLastContentHeight = currentContentHeight;
                binding.myScroll.setEnableScrolling(true);
            } else if (currentContentHeight > mLastContentHeight + 100) {
                Log.d("Keyboard is closed", "CLOSE");
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                mLastContentHeight = currentContentHeight;
            }
        }
    };

}
