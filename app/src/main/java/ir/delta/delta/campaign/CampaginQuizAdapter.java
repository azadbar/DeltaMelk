package ir.delta.delta.campaign;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseTextView;
import ir.delta.delta.service.ResponseModel.Campaign.QuizTopListItem;
import ir.delta.delta.service.ResponseModel.demand.DemandItem;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class CampaginQuizAdapter extends RecyclerView.Adapter<CampaginQuizAdapter.ViewHolder> {



    private final ArrayList<QuizTopListItem> list;


    public CampaginQuizAdapter(ArrayList<QuizTopListItem> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quiz, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        QuizTopListItem quiz = list.get(position);

        holder.name.setText(quiz.getName());

        holder.points.setText(String.valueOf(quiz.getPoint()));
        holder.mobile.setText(quiz.getMobile());


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.name)
        BaseTextView name;
        @BindView(R.id.points)
        BaseTextView points;
        @BindView(R.id.mobile)
        BaseTextView mobile;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }

    public interface OnItemClickListener {
        void onItemCommunication(int position, DemandItem demand);

        void onItemShare(int position, DemandItem demand);
    }

}
