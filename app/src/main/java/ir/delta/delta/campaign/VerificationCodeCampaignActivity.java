package ir.delta.delta.campaign;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;

import java.util.Timer;
import java.util.TimerTask;

import ir.delta.delta.R;
import ir.delta.delta.baseView.BaseActivity;
import ir.delta.delta.database.TransactionUser;
import ir.delta.delta.databinding.ActivityVerificationCodeCampaignBinding;
import ir.delta.delta.profile.LoginActivity;
import ir.delta.delta.service.Request.campaign.CampaignResendVerificationCodeService;
import ir.delta.delta.service.Request.ResponseListener;
import ir.delta.delta.service.Request.campaign.VerificationCampaignService;
import ir.delta.delta.service.RequestModel.campaign.CampaignResendVerificationCodeReq;
import ir.delta.delta.service.RequestModel.campaign.CampaignVerifyReq;
import ir.delta.delta.service.ResponseModel.Campaign.ResendVerificationCodeResponse;
import ir.delta.delta.service.ResponseModel.Campaign.VerificationCampaignResponse;
import ir.delta.delta.util.Constants;
import ir.delta.delta.util.PreferencesData;

import static android.view.View.VISIBLE;

public class VerificationCodeCampaignActivity extends BaseActivity {

    ActivityVerificationCodeCampaignBinding binding;
    private String mobileNumber;
    private long sendVerificationCodeTime;
    private final long ONE_MINUTE = 60000;
    private String key;

    private int mLastContentHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code_campaign);

        mLastContentHeight = findViewById(Window.ID_ANDROID_CONTENT).getHeight();
        binding.rlRoot.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
        initView();
        onClick();
    }

    private void initView() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
        seStatusBarColor(binding.rlBack, getResources().getColor(R.color.redColor));
        binding.background.setImageResource(R.drawable.background);
        binding.myScroll.setEnableScrolling(false);
        Glide.with(this).load(R.drawable.app_header).into(binding.imageHeader);
        Glide.with(this).load(R.drawable.coins_bg).into(binding.coinImage);

        sendVerificationCodeTime = PreferencesData.getLongParam(this, Constants.SEND_VERIFICATION_TIME, System.currentTimeMillis());
        mobileNumber = PreferencesData.getMobileNumber(this);
        if (!TextUtils.isEmpty(mobileNumber))
            binding.tvTxtCodeSendTo.setText(getString(R.string.send_code_to_mobile, mobileNumber));
        setTimer();
        setCodeFocusHandler();

    }


    private void onClick() {
        binding.okRegister.setOnClickListener(v -> {
            if (isValidData()) {
                sendVerificationCode();
            }
        });

        binding.btnChangeMobile.setOnClickListener(v -> {
            Intent intent = new Intent(VerificationCodeCampaignActivity.this, RegisterCampaignActivity.class);
            intent.putExtra(Constants.isChangeMobileNumber, true);
            startActivity(intent);
            finish();
        });

        binding.btnRetryCode.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(mobileNumber)) {
                resendVerificationCode();
            }
        });

        binding.rlBack.setOnClickListener(v -> finish());
    }

    private void resendVerificationCode() {
        PreferencesData.setlongParam(VerificationCodeCampaignActivity.this, Constants.SEND_VERIFICATION_TIME, System.currentTimeMillis());
        binding.btnRetryCode.setEnabled(false);
        binding.btnChangeMobile.setEnabled(false);
        hideKeyboard();
        binding.roundedLoadingView.setVisibility(VISIBLE);
        enableDisableViewGroup(binding.root, false);
        CampaignResendVerificationCodeReq req = new CampaignResendVerificationCodeReq();
        req.setMobile(mobileNumber);

        CampaignResendVerificationCodeService.getInstance().getResendVerificationCode(getResources(), req, new ResponseListener<ResendVerificationCodeResponse>() {
            @Override
            public void onGetError(String error) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                Toast.makeText(VerificationCodeCampaignActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(ResendVerificationCodeResponse response) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                sendVerificationCodeTime = PreferencesData.getLongParam(VerificationCodeCampaignActivity.this, Constants.SEND_VERIFICATION_TIME, System.currentTimeMillis());
                setTimer();
                if (response.isSuccessed()) {
                    Toast.makeText(VerificationCodeCampaignActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(VerificationCodeCampaignActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(VerificationCodeCampaignActivity.this);
                startActivity(intent);
                finish();
            }
        });
    }

    private void sendVerificationCode() {
        hideKeyboard();
        binding.roundedLoadingView.setVisibility(VISIBLE);
        enableDisableViewGroup(binding.root, false);
        CampaignVerifyReq req = new CampaignVerifyReq();
        req.setMobile(mobileNumber);
        req.setvCode(Integer.parseInt(key));

        VerificationCampaignService.getInstance().getVarificarion(getResources(), req, new ResponseListener<VerificationCampaignResponse>() {
            @Override
            public void onGetError(String error) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                Toast.makeText(VerificationCodeCampaignActivity.this, error, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(VerificationCampaignResponse response) {
                binding.roundedLoadingView.setVisibility(View.GONE);
                enableDisableViewGroup(binding.root, true);
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                if (response.isSuccessed()) {
                    Intent intent = new Intent(VerificationCodeCampaignActivity.this, CampaignActivity.class);
                    intent.putExtra("mobile", mobileNumber);
                    startActivity(intent);
                    finish();
                } else {
                    showErrorFromServer(response.getModelStateErrors(), response.getMessage());
                }
            }

            @Override
            public void onAuthorization() {
                Intent intent = new Intent(VerificationCodeCampaignActivity.this, LoginActivity.class);
                Constants.setCurrentUser(null);
                TransactionUser.getInstance().deleteUsr(VerificationCodeCampaignActivity.this);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean isValidData() {
        if (TextUtils.isEmpty(binding.edtOne.getValueString())
                || TextUtils.isEmpty(binding.edtTwo.getValueString())
                || TextUtils.isEmpty(binding.edtThree.getValueString())
                || TextUtils.isEmpty(binding.edtFour.getValueString())
                || TextUtils.isEmpty(binding.edtFive.getValueString())) {
            Toast.makeText(this, getString(R.string.please_enter_verification_code), Toast.LENGTH_SHORT).show();
            return false;
        }
        key = binding.edtOne.getValueString() + binding.edtTwo.getValueString() + binding.edtThree.getValueString() +
                binding.edtFour.getValueString() + binding.edtFive.getValueString();
        return true;
    }

//    private void sendRequestMobile() {
//        hideKeyboard();
//        binding.roundedLoadingView.setVisibility(VISIBLE);
//        enableDisableViewGroup(binding.root, false);
//        CampaignRegisterReq req = new CampaignRegisterReq();
//        req.setName(binding.edtName.getValueString());
//        req.setMobile(binding.edtMobile.getValueString());
//        req.setReferenceCode(binding.edtRefrenceCode.getValueInt());
//        RegisterCampaignService.getInstance().getRegisterCode(getResources(), req, new ResponseListener<RegisterCampaignResponse>() {
//            @Override
//            public void onGetError(String error) {
//                binding.roundedLoadingView.setVisibility(View.GONE);
//                enableDisableViewGroup(binding.root, true);
//                Toast.makeText(VerificationCodeCampaignActivity.this, error, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onSuccess(RegisterCampaignResponse response) {
//                binding.roundedLoadingView.setVisibility(View.GONE);
//                enableDisableViewGroup(binding.root, true);
//                if (response.isSuccessed()) {
//                    PreferencesData.setMobileNumber(VerificationCodeCampaignActivity.this, binding.edtMobile.getValueString());
////                    Intent intent = new Intent(InputMobileActivity.this, VerificationCodeActivity.class);
////                    intent.putExtra(Constants.isChangePassword, isChangePassword);
////                    startActivity(intent);
////                    finish();
//                } else {
//                    if (!TextUtils.isEmpty(response.getMessage())) {
//                        Toast.makeText(VerificationCodeCampaignActivity.this, response.getMessage(), Toast.LENGTH_SHORT).show();
//                    } else {
//                        Toast.makeText(VerificationCodeCampaignActivity.this, getResources().getString(R.string.communicationError), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onAuthorization() {
//                Intent intent = new Intent(VerificationCodeCampaignActivity.this, LoginActivity.class);
//                Constants.setCurrentUser(null);
//                TransactionUser.getInstance().deleteUsr(VerificationCodeCampaignActivity.this);
//                startActivity(intent);
//                finish();
//            }
//        });
//
//    }
//
//    private boolean isValidData() {
//        ArrayList<String> errorMsgList = new ArrayList<>();
//
//        if (binding.edtName.getError() != null) {
//            errorMsgList.add(binding.edtName.getError());
//        }
//
//        if (binding.edtMobile.getError() != null) {
//            errorMsgList.add(binding.edtMobile.getError());
//        }
//
//        if (binding.edtRefrenceCode.getError() != null) {
//            errorMsgList.add(binding.edtRefrenceCode.getError());
//        }
//        if (errorMsgList.size() > 0) {
//            showInfoDialog(getResources().getString(R.string.attetion), errorMsgList);
//            return false;
//        }
//
//        return true;
//    }

    private void setTimer() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(() -> {
                    long diffTime = System.currentTimeMillis() - sendVerificationCodeTime;

                    if (diffTime > ONE_MINUTE) {
                        binding.btnRetryCode.setEnabled(true);
                        binding.btnChangeMobile.setEnabled(true);
                    } else {
                        diffTime = (ONE_MINUTE - diffTime) / 1000;
                        if (diffTime > 0) {
                            binding.btnRetryCode.setTextColor(ContextCompat.getColor(VerificationCodeCampaignActivity.this, R.color.grayDark));
                            binding.btnChangeMobile.setTextColor(ContextCompat.getColor(VerificationCodeCampaignActivity.this, R.color.grayDark));
                            binding.btnRetryCode.setText(getString(R.string.retry_again_code, String.valueOf(diffTime)));
                        } else {
                            binding.btnRetryCode.setText(getString(R.string.retry_again_code_without_second));
                            binding.btnRetryCode.setTextColor(ContextCompat.getColor(VerificationCodeCampaignActivity.this, R.color.primaryTextColor));
                            binding.btnChangeMobile.setTextColor(ContextCompat.getColor(VerificationCodeCampaignActivity.this, R.color.orange));
                        }
                    }
                });

            }
        }, 0, 1000);
    }

    private void setCodeFocusHandler() {
        binding.edtOne.setFocusableInTouchMode(true);
        binding.edtOne.setFocusable(true);
        binding.edtOne.requestFocus();
        binding.edtOne.getEdtBody().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int count, int after) {
                if (after > 0)
                    binding.edtTwo.getEdtBody().requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.edtTwo.getEdtBody().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int count, int after) {
                if (after > 0)
                    binding.edtThree.getEdtBody().requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        binding.edtThree.getEdtBody().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int count, int after) {
                if (after > 0)
                    binding.edtFour.getEdtBody().requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.edtFour.getEdtBody().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int count, int after) {
                if (after > 0)
                    binding.edtFive.getEdtBody().requestFocus();

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        binding.edtFive.getEdtBody().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isValidData())
                    sendVerificationCode();
            }
        });

    }

    private final ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int currentContentHeight = findViewById(Window.ID_ANDROID_CONTENT).getHeight();

            if (mLastContentHeight > currentContentHeight + 100) {
                Log.d("Keyboard is open", "OPEN");
                mLastContentHeight = currentContentHeight;
                binding.myScroll.setEnableScrolling(true);
            } else if (currentContentHeight > mLastContentHeight + 100) {
                Log.d("Keyboard is closed", "CLOSE");
                binding.myScroll.scrollTo(0, 0);
                binding.myScroll.setEnableScrolling(false);
                mLastContentHeight = currentContentHeight;
            }
        }
    };

}
