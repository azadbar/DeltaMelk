# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
#-keep class org.apache.http.**
#-keep interface org.apache.http.**
#-dontwarn org.apache.**

-keep interface java.**
-keep class java.**
-dontwarn java.**

-keep interface  java. { *; }
-keep interface  javax. { *; }


-keep interface retrofit2.**
-keep class retrofit2.**
-dontwarn retrofit2.**



-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions
-keepattributes Annotation



-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault


#
-keep class org.apache.http.* { *; }
-dontwarn org.apache.http.

#
-dontwarn org.kobjects.
-dontwarn org.ksoap2.
-dontwarn org.kxml2.
-dontwarn org.xmlpull.v1.

-keep class org.kobjects. { *; }
-keep class org.ksoap2. { *; }
-keep class org.kxml2. { *; }
-keep class org.xmlpull. { *; }

-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions
-keepattributes Annotation


-keep class com.google. {*;}
-keepclassmembers class com.google. {*;}
-dontwarn com.google.**

-keep class *{
    public private *;
}


-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }

-dontwarn com.squareup.okhttp.**
-dontwarn okio.**

-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }

-dontwarn okhttp3.**

-keepattributes *Annotation*

-keepclassmembers enum * { *; }

-keep class com.bumptech.glide.GeneratedAppGlideModuleImpl { *; }

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}